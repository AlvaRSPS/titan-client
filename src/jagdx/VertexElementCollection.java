/* VertexElementCollection - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;

import jaclib.peer.jaa;
import jaclib.peer.mfa;

public class VertexElementCollection extends mfa {
	public VertexElementCollection(jaa var_jaa) {
		super(var_jaa);
		try {
			init();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final native void addElement(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_);

	public final native void finish();

	private final native void init();

	public final native void reset();
}

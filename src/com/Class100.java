/* Class100 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Archive;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.VolumePreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class Class100 {
	public static float			aFloat845;
	public static Js5Archive[]	js5Archives	= new Js5Archive[37];

	public static final boolean method1688(int i, int i_0_, int i_1_, Class246_Sub3_Sub3 class246_sub3_sub3, byte i_2_) {
		try {
			if (!RtMouseEvent.occlusion || !Js5Client.aBoolean1052) {
				return false;
			}
			if (Class4.pixelsOccludedCount < 100) {
				return false;
			}
			if (!Class76_Sub5.method758((byte) -78, i, i_1_, i_0_)) {
				return false;
			}
			int i_4_ = i_0_ << Class151_Sub8.tileScale;
			int i_5_ = i_1_ << Class151_Sub8.tileScale;
			int i_6_ = Class78.aSArray594[i].getTileHeight(i_1_, -12639, i_0_) - 1;
			int i_7_ = i_6_ + class246_sub3_sub3.method2990(0);
			if ((class246_sub3_sub3.aShort6153 ^ 0xffffffff) == -2) {
				if (!Class254.method3187(i_7_, i_4_, i_5_, (byte) 82, i_6_, NativeShadow.anInt6333 + i_5_, i_7_, i_5_, i_4_, i_4_)) {
					return false;
				}
				if (!Class254.method3187(i_7_, i_4_, i_5_, (byte) 82, i_6_, NativeShadow.anInt6333 + i_5_, i_6_, i_5_ + NativeShadow.anInt6333, i_4_, i_4_)) {
					return false;
				}
				Class98_Sub16.wallsOccludedCount++;
				return true;
			}
			if (class246_sub3_sub3.aShort6153 == 2) {
				if (!Class254.method3187(i_7_, i_4_, NativeShadow.anInt6333 + i_5_, (byte) 82, i_6_, NativeShadow.anInt6333 + i_5_, i_7_, NativeShadow.anInt6333 + i_5_, i_4_ - -NativeShadow.anInt6333, i_4_)) {
					return false;
				}
				if (!Class254.method3187(i_6_, NativeShadow.anInt6333 + i_4_, i_5_ + NativeShadow.anInt6333, (byte) 82, i_6_, i_5_ + NativeShadow.anInt6333, i_7_, i_5_ - -NativeShadow.anInt6333, i_4_ - -NativeShadow.anInt6333, i_4_)) {
					return false;
				}
				Class98_Sub16.wallsOccludedCount++;
				return true;
			}
			if (class246_sub3_sub3.aShort6153 == 4) {
				if (!Class254.method3187(i_7_, i_4_ - -NativeShadow.anInt6333, i_5_, (byte) 82, i_6_, NativeShadow.anInt6333 + i_5_, i_7_, i_5_, NativeShadow.anInt6333 + i_4_, NativeShadow.anInt6333 + i_4_)) {
					return false;
				}
				if (!Class254.method3187(i_7_, NativeShadow.anInt6333 + i_4_, i_5_, (byte) 82, i_6_, i_5_ + NativeShadow.anInt6333, i_6_, NativeShadow.anInt6333 + i_5_, i_4_ - -NativeShadow.anInt6333, NativeShadow.anInt6333 + i_4_)) {
					return false;
				}
				Class98_Sub16.wallsOccludedCount++;
				return true;
			}
			if ((class246_sub3_sub3.aShort6153 ^ 0xffffffff) == -9) {
				if (!Class254.method3187(i_7_, i_4_, i_5_, (byte) 82, i_6_, i_5_, i_7_, i_5_, NativeShadow.anInt6333 + i_4_, i_4_)) {
					return false;
				}
				if (!Class254.method3187(i_6_, i_4_ + NativeShadow.anInt6333, i_5_, (byte) 82, i_6_, i_5_, i_7_, i_5_, NativeShadow.anInt6333 + i_4_, i_4_)) {
					return false;
				}
				Class98_Sub16.wallsOccludedCount++;
				return true;
			}
			if ((class246_sub3_sub3.aShort6153 ^ 0xffffffff) == -17) {
				if (!OpenGlGround.method3427(i_6_, Js5.anInt1577, i_7_, Js5.anInt1577, (byte) 16, Js5.anInt1577 + i_5_, i_4_)) {
					return false;
				}
				Class98_Sub16.wallsOccludedCount++;
				return true;
			}
			if (class246_sub3_sub3.aShort6153 == 32) {
				if (!OpenGlGround.method3427(i_6_, Js5.anInt1577, i_7_, Js5.anInt1577, (byte) 16, i_5_ - -Js5.anInt1577, Js5.anInt1577 + i_4_)) {
					return false;
				}
				Class98_Sub16.wallsOccludedCount++;
				return true;
			}
			if (class246_sub3_sub3.aShort6153 == 64) {
				if (!OpenGlGround.method3427(i_6_, Js5.anInt1577, i_7_, Js5.anInt1577, (byte) 16, i_5_, i_4_ - -Js5.anInt1577)) {
					return false;
				}
				Class98_Sub16.wallsOccludedCount++;
				return true;
			}
			if (class246_sub3_sub3.aShort6153 == 128) {
				if (!OpenGlGround.method3427(i_6_, Js5.anInt1577, i_7_, Js5.anInt1577, (byte) 16, i_5_, i_4_)) {
					return false;
				}
				Class98_Sub16.wallsOccludedCount++;
				return true;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gea.A(" + i + ',' + i_0_ + ',' + i_1_ + ',' + (class246_sub3_sub3 != null ? "{...}" : "null") + ',' + i_2_ + ')');
		}
	}

	public static void method1691(int i) {

	}

	public static final void method1692(int i, int i_9_, int i_10_, int i_11_, int i_12_) {
		try {
			if (i_12_ != 64) {
				method1688(-20, 71, 72, null, (byte) 127);
			}
			if ((i ^ 0xffffffff) == -9 || (i ^ 0xffffffff) == -17) {
				for (int i_13_ = 0; (i_13_ ^ 0xffffffff) > (RSToolkit.anInt936 ^ 0xffffffff); i_13_++) {
					Class155 class155 = AwtGraphicsBuffer.aClass155Array5889[i_13_];
					if (i == class155.aByte1242 && (i_10_ ^ 0xffffffff) == (class155.aShort1236 ^ 0xffffffff) && class155.aShort1239 == i_11_ || i_10_ == class155.aShort1243 && (class155.aShort1239 ^ 0xffffffff) == (i_11_ ^ 0xffffffff)) {
						if (RSToolkit.anInt936 != i_13_) {
							ArrayUtils.method2892(AwtGraphicsBuffer.aClass155Array5889, 1 + i_13_, AwtGraphicsBuffer.aClass155Array5889, i_13_, AwtGraphicsBuffer.aClass155Array5889.length - (1 + i_13_));
						}
						RSToolkit.anInt936--;
						break;
					}
				}
			} else {
				Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i_9_][i_10_][i_11_];
				if (class172 != null) {
					if ((i ^ 0xffffffff) == -2) {
						class172.aShort1335 = (short) 0;
					} else if (i == 2) {
						class172.aShort1328 = (short) 0;
					}
				}
				VolumePreferenceField.method644(i_12_ + -107);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gea.H(" + i + ',' + i_9_ + ',' + i_10_ + ',' + i_11_ + ',' + i_12_ + ')');
		}
	}

	public static final Class172 method1693(int i, int i_14_, int i_15_) {
		if (QuickChatCategory.aClass172ArrayArrayArray5948[i][i_14_][i_15_] == null) {
			boolean bool = QuickChatCategory.aClass172ArrayArrayArray5948[0][i_14_][i_15_] != null && QuickChatCategory.aClass172ArrayArrayArray5948[0][i_14_][i_15_].aClass172_1330 != null;
			if (bool && i >= OpenGLTexture2DSource.anInt3103 - 1) {
				return null;
			}
			Class224_Sub2_Sub1.method2839(i, i_14_, i_15_);
		}
		return QuickChatCategory.aClass172ArrayArrayArray5948[i][i_14_][i_15_];
	}

	private Queue		aClass215_842		= new Queue();

	private HashTable	aClass377_846;

	private Cacheable	aClass98_Sub46_839	= new Cacheable();

	private int			anInt841;

	private int			anInt843;

	Class100(int i) {
		try {
			anInt843 = i;
			anInt841 = i;
			int i_18_;
			for (i_18_ = 1; i_18_ + i_18_ < i; i_18_ += i_18_) {
				/* empty */
			}
			aClass377_846 = new HashTable(i_18_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gea.<init>(" + i + ')');
		}
	}

	public final void method1689(long l, byte i) {
		do {
			try {
				Cacheable class98_sub46 = (Cacheable) aClass377_846.get(l, -1);
				if (class98_sub46 == null) {
					break;
				}
				class98_sub46.unlink(98);
				class98_sub46.uncache((byte) -90);
				anInt843++;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "gea.D(" + l + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1690(int i) {
		try {
			aClass215_842.clear(16711680);
			if (i == 1) {
				aClass377_846.clear(-84);
				aClass98_Sub46_839 = new Cacheable();
				anInt843 = anInt841;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gea.C(" + i + ')');
		}
	}

	public final Cacheable method1694(byte i, long l) {
		try {
			Cacheable class98_sub46 = (Cacheable) aClass377_846.get(l, -1);
			if (class98_sub46 != null) {
				aClass215_842.insert(class98_sub46, -54);
			}
			return class98_sub46;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gea.E(" + i + ',' + l + ')');
		}
	}

	public final void method1695(int i, Cacheable class98_sub46, long l) {
		do {
			try {
				do {
					if ((anInt843 ^ 0xffffffff) == -1) {
						Cacheable class98_sub46_17_ = aClass215_842.remove(-16711936);
						class98_sub46_17_.unlink(i + -26312);
						class98_sub46_17_.uncache((byte) -90);
						if (aClass98_Sub46_839 != class98_sub46_17_) {
							break;
						}
						class98_sub46_17_ = aClass215_842.remove(-16711936);
						class98_sub46_17_.unlink(116);
						class98_sub46_17_.uncache((byte) -90);
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					anInt843--;
				} while (false);
				aClass377_846.put(class98_sub46, l, i + -26405);
				aClass215_842.insert(class98_sub46, -101);
				if (i == 26404) {
					break;
				}
				method1688(-123, -119, 55, null, (byte) 68);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "gea.G(" + i + ',' + (class98_sub46 != null ? "{...}" : "null") + ',' + l + ')');
			}
			break;
		} while (false);
	}
}

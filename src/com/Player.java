/* Class246_Sub3_Sub4_Sub2_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.MemoryCacheNodeFactory;
import com.jagex.game.client.definition.*;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Player extends Mob {
	public static boolean			menuOpen		= false;
	public static byte[][]			aByteArrayArray6533;
	public static IncomingOpcode	CLAN_CHAT_DATA	= new IncomingOpcode(28, -2);

	public static final void method3065(int xChunk, int yChunk, boolean bool) {

		Class28 class28 = Class76.aClass28ArrayArray586[xChunk][yChunk];
		if (bool == true) {
			if (class28 != null) {
				Class48.anInt410 = class28.anInt284;
				InputStream_Sub2.anInt29 = class28.anInt282;
				OutputStream_Sub2.anInt40 = class28.anInt292;
			}
			Class230.renderAmbientAndSunSettings((byte) -74);
		}

	}

	boolean				aBoolean6520;
	boolean				aBoolean6532;
	boolean				clanmate;
	private byte titleGroup;
	String				accountName;
	int					nextDirection	= -1;
	int					anInt6514;
	int					anInt6517		= -1;
	int					path_x;
	int					anInt6522;
	int					anInt6524;
	int					anInt6525;
	int					anInt6527;
	int					path_z;
	PlayerAppearence	appearence;
	int					boosted;
	int					combatLevel;
	int					combatRange;
	String				displayName;
	private byte		gender;
	boolean				hasDisplayName;
	int					headIconId;
	boolean				invisible;
	private byte		nameTitleId;
	private int			renderId;

	int					skillRating;

	int					skullId			= -1;

	int					team;
	boolean				cprgbe;
	int					intensity;
	int					ambient;
	ParticleDescriptor	pDescriptor;
	protected int		red;
	protected int		green;
	protected int		blue;
	protected float		hue;
	protected boolean	che;

	public Player() {
		titleGroup = (byte) 0;
		combatLevel = 0;
		anInt6524 = -1;
		aBoolean6532 = false;
		combatRange = -1;
		invisible = false;
		headIconId = -1;
		skillRating = 0;
		nameTitleId = (byte) 0;
		anInt6514 = 255;
		anInt6525 = 0;
		clanmate = false;
		hasDisplayName = false;
		team = 0;
		anInt6522 = -1;
		anInt6527 = -1;
		gender = (byte) 0;
		aBoolean6520 = false;
		boosted = 0;
		cprgbe = false;
		red = 0;
		green = 0;
		blue = 0;
		intensity = 0;
		ambient = 0;
		hue = 0.0F;
		che = false;
		pDescriptor = new ParticleDescriptor((byte) 0, (byte) 0, (byte) 0);
		pDescriptor.m = false;
	}

	public final void decodeAppearance(RSByteBuffer buffer, byte i) {
		do {
			buffer.position = 0;
			int flags = buffer.readUnsignedByte((byte) -101);
			gender = (byte) (0x1 & flags);
			boolean hasDisplayName = this.hasDisplayName;
			this.hasDisplayName = (~flags & 0x2) != -1;
			boolean combatColoured = (0x4 & flags) != 0;
			int size = super.getSize(0);
			setSize((byte) 68, 1 + (0x7 & flags >> 3));
			titleGroup = (byte) (flags >> 6 & 0x3);
			this.boundExtentsX += -size + getSize(i + -73) << 1992716520;
			this.boundExtentsZ += getSize(0) - size << 1346950216;
			nameTitleId = buffer.readSignedByte((byte) -19);
			skullId = buffer.readSignedByte((byte) -19);
			headIconId = buffer.readSignedByte((byte) -19);
			invisible = buffer.readSignedByte((byte) -19) == 1;
			// System.out.println("X: " + ((Char) this).boundExtentsX + " Y: " +
			// ((Char) this).boundExtentsY);
			// System.out.println("Gender: " + gender + " HasDisplayName: " +
			// hasDisplayName + " Name Title Id: " + nameTitleId + " SkullId: "
			// + skullId + " headIcon: " + headIconId + " Invisible: " +
			// invisible);
			if (client.buildLocation == BuildLocation.LIVE && LoadingScreenSequence.rights >= 2) {
				invisible = false;
			}
			int npcId = -1;
			team = 0;

			int[] equips = new int[12];
			ItemAppearanceOverride[] overrides = new ItemAppearanceOverride[12];
			for (int slot = 0; slot < 12; slot++) {
				int index = buffer.readUnsignedByte((byte) -123);
				if (index == 0) {
					equips[slot] = 0;
				} else {
					int value = buffer.readUnsignedByte((byte) -111);
					int itemId = (index << -30617816) + value;
					//System.out.println("Slot " + slot + " Model: " + itemId);

					if (slot == 0 && (itemId ^ 0xffffffff) == -65536) {
						npcId = buffer.readShort((byte) 127);
						team = buffer.readUnsignedByte((byte) -114);

						break;
					}
					if (itemId >= 32768) {
						itemId = Class255.anIntArray3207[-32768 + itemId];
						equips[slot] = Class41.or(1073741824, itemId);
						int val = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -119).team;
						if (val != 0) {
							team = val;
						}

					} else {
						equips[slot] = Class41.or(-2147483648, itemId + -256);
						//System.out.println("Model: " + itemId);
					}
				}
			}
			if (-1 == npcId) {
				int count = buffer.readShort((byte) 127);
				System.out.println("Short: " + count);
				int current = 0;

				for (int index = 0; index < 12; index++) {
					if (0 != (count & 1 << current)) {
						overrides[index] = ItemAppearanceOverride.create(buffer, Class98_Sub46_Sub19.itemDefinitionList.get(index, (byte) -119));
						System.out.println("Int: " + index);
					}

					current++;
				}
			}

			int[] colours = new int[5];
			for (int index = 0; index < 5; index++) {
				int value = buffer.readUnsignedByte((byte) -108);
				if ((HashTableIterator.colourToReplace.length ^ 0xffffffff) > -2 || value < 0 || HashTableIterator.colourToReplace[0][index].length <= value) {
					value = 0;
				}
				colours[index] = value;
			}
			renderId = buffer.readShort((byte) 127);
			displayName = buffer.readString((byte) 84);
			accountName = displayName;
			if (this == Class87.localPlayer) {
				Class256_Sub1.userName = displayName;
			}
			combatLevel = buffer.readUnsignedByte((byte) 80);
			if (!combatColoured) {
				skillRating = 0;
				boosted = buffer.readUnsignedByte((byte) -125);
				combatRange = buffer.readUnsignedByte((byte) 31);
				if ((combatRange ^ 0xffffffff) == -256) {
					combatRange = -1;
				}
			} else {
				skillRating = buffer.readShort((byte) 127);
				combatRange = -1;
				boosted = combatLevel;
				if ((skillRating ^ 0xffffffff) == -65536) {
					skillRating = -1;
				}
			}

			int i_60_ = anInt6525;
			anInt6525 = buffer.readUnsignedByte((byte) -122);
			if ((anInt6525 ^ 0xffffffff) == -1) {
				Huffman.method2778(true, this);
			} else {
				int i_61_ = anInt6527;
				int i_62_ = anInt6524;
				int i_63_ = anInt6522;
				int i_64_ = anInt6517;
				int i_65_ = anInt6514;
				anInt6527 = buffer.readShort((byte) 127);
				anInt6524 = buffer.readShort((byte) 127);
				anInt6522 = buffer.readShort((byte) 127);
				anInt6517 = buffer.readShort((byte) 127);
				anInt6514 = buffer.readUnsignedByte((byte) -115);
				if (!hasDisplayName != !this.hasDisplayName || i_60_ != anInt6525 || i_61_ != anInt6527 || i_62_ != anInt6524 || i_63_ != anInt6522 || i_64_ != anInt6517 || anInt6514 != i_65_) {
					OpenGlElementBufferPointer.method3674(0, this);
				}
			}

            if(buffer.position < buffer.payload.length) {
                // Server sent us a particle effect, not part of the standard protocol
                cprgbe = buffer.readUnsignedByte((byte) -122) == 1;
                if (cprgbe) {
                    if (pDescriptor == null)
                        pDescriptor = new ParticleDescriptor((byte) 0, (byte) 0, (byte) 0);
                    pDescriptor.red = (byte) buffer.readUnsignedByte((byte) -122);
                    pDescriptor.green = (byte) buffer.readUnsignedByte((byte) -122);
                    pDescriptor.blue = (byte) buffer.readUnsignedByte((byte) -122);
                    intensity = buffer.readUnsignedByte((byte) -122);
                    ambient = buffer.readUnsignedByte((byte) -122);
                    if (pDescriptor.red == -105 && pDescriptor.green == 50 && pDescriptor.blue == -123)
                        pDescriptor.m = true;
                    else
                        pDescriptor.m = false;
                }
            }

			System.out.println("RenderId: " + renderId + " Name: " + displayName + " CombatLevel: " + combatLevel + " Boosted: " + boosted + " CombatRange: " + combatRange + " Unknown: " + anInt6525);
			System.out.println("Unknown: " + anInt6527 + " Unknown: " + anInt6524 + " Unknown: " + anInt6522 + " Unknown: " + anInt6517 + " Unknown: " + anInt6514);

			if (appearence == null) {
				appearence = new PlayerAppearence();
			}
			int i_66_ = appearence.npc;
			int[] is_67_ = appearence.colours;
			appearence.update(equips, overrides, -59, (gender ^ 0xffffffff) == -2, colours, getRenderId((byte) 70), npcId);
			if (i == 73) {
				if ((npcId ^ 0xffffffff) != (i_66_ ^ 0xffffffff)) {
					this.boundExtentsX = (this.pathX[0] << -1791062743) - -(getSize(0) << 2009592264);
					this.boundExtentsZ = (this.pathZ[0] << 845342377) - -(getSize(i ^ 0x49) << 1847977800);
				}
				if ((this.index ^ 0xffffffff) == (OpenGLHeap.localPlayerIndex ^ 0xffffffff) && is_67_ != null) {
					for (int i_68_ = 0; colours.length > i_68_; i_68_++) {
						if ((is_67_[i_68_] ^ 0xffffffff) != (colours[i_68_] ^ 0xffffffff)) {
							Class98_Sub46_Sub19.itemDefinitionList.method2717(i + -9);
							break;
						}
					}
				}
				if (this.aClass246_Sub5_6439 != null) {
					this.aClass246_Sub5_6439.method3127();
				}
				if (this.anInt6385 == -1 || !this.aBoolean6359) {
					break;
				}
				RenderAnimDefinition class294 = method3039(1);
				if (class294.method3480((byte) 119, this.anInt6385)) {
					break;
				}
				this.aBoolean6359 = false;
				this.anInt6385 = -1;
			}
			break;
		} while (false);
	}

	public final String formattedName(int i, boolean hasDisplayName) {
		String string = "";
		if (Class116.aStringArray966 != null) {
			string += Class116.aStringArray966[titleGroup];
		}
		int[] titleIds;
		if ((gender ^ 0xffffffff) == -2 && Class35.anIntArray333 != null) {
			titleIds = Class35.anIntArray333;
		} else {
			titleIds = Class272.anIntArray2036;
		}
		if (titleIds != null && (titleIds[titleGroup] ^ 0xffffffff) != 0) {
			EnumDefinition titles = Class98_Sub10_Sub16.enumsDefinitionList.read(titleIds[titleGroup], 1028602529);
			if ((titles.aChar2567 ^ 0xffffffff) == -116) {
				string += titles.get(nameTitleId & 0xff, (byte) 37);
			} else {
				Class305_Sub1.reportError(new Throwable(), i + -126, "gdn1");
				titleIds[titleGroup] = -1;
			}
		}
		if (!hasDisplayName) {
			string += accountName;
		} else {
			string += displayName;
		}
		if (Class84.aStringArray636 != null) {
			string += Class84.aStringArray636[titleGroup];
		}
		return string;
	}

	@Override
	public final int getArmyIcon(int i) {
		return -1;
	}

	public final String getName(int i, boolean hasDisplayName) {
		if (hasDisplayName) {
			return displayName;
		}
		return accountName;
	}

	@Override
	public final int getRenderId(byte i) {
		return renderId;
	}

	@Override
	public final int getSize(int i) {
		if (appearence != null && (appearence.npc ^ 0xffffffff) != 0) {
			return Class4.npcDefinitionList.get(i + 5, appearence.npc).boundSize;
		}
		return super.getSize(0);
	}

	public final boolean hasAppearance(byte i) {
		return appearence != null;
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		return null;
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		if (appearence == null || !method3058((byte) -122, var_ha, 2048)) {
			return null;
		}
		Matrix class111 = var_ha.method1793();
		int i_79_ = this.yaw.value((byte) 116);
		class111.method2101(i_79_);
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[this.plane][this.boundExtentsX >> Class151_Sub8.tileScale][this.boundExtentsZ >> Class151_Sub8.tileScale];
		if (class172 == null || class172.aClass246_Sub3_Sub1_1332 == null) {
			this.anInt6351 -= this.anInt6351 / 10.0F;
		} else {
			int i_80_ = this.anInt6351 - class172.aClass246_Sub3_Sub1_1332.aShort6149;
			this.anInt6351 -= i_80_ / 10.0F;
		}
		class111.translate(this.boundExtentsX, -this.anInt6351 + this.anInt5089 + -20, this.boundExtentsZ);
		Class246_Sub1 class246_sub1 = null;
		if (i > -12) {
			return null;
		}
		this.aBoolean6442 = false;
		if ((client.preferences.characterShadows.getValue((byte) 120) ^ 0xffffffff) == -2) {
			RenderAnimDefinition class294 = method3039(1);
			if (class294.aBoolean2400 && (appearence.npc == -1 || Class4.npcDefinitionList.get(5, appearence.npc).aBoolean1130)) {
				AnimationDefinition class97 = (this.anInt6413 ^ 0xffffffff) != 0 && this.anInt6400 == 0 ? Class151_Sub7.animationDefinitionList.method2623(this.anInt6413, 16383) : null;
				AnimationDefinition class97_81_ = (this.anInt6385 ^ 0xffffffff) == 0 || aBoolean6520 || this.aBoolean6359 && class97 != null ? null : Class151_Sub7.animationDefinitionList.method2623(this.anInt6385, 16383);
				ModelRenderer class146 = PrefetchObject.method1703(class97_81_ == null ? class97 : class97_81_, this.anInt6377, 1, 240, this.aClass146Array6441[0], this.anInt6388, i_79_, 124, 160, 0, class97_81_ == null ? this.anInt6393 : this.anInt6350,
						var_ha, 0, this.anInt6416);
				if (class146 != null) {
					class246_sub1 = Class94.method915(1 + this.aClass146Array6441.length, (byte) -47, true);
					this.aBoolean6442 = true;
					var_ha.setDepthWriteMask(false);
					if (VarClientStringsDefinitionParser.aBoolean1839) {
						class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[this.aClass146Array6441.length], Class16.anInt197, 0);
					} else {
						class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[this.aClass146Array6441.length], 0);
					}
					var_ha.setDepthWriteMask(true);
				}
			}
		}
		if (this == Class87.localPlayer) {
			class111.method2100(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
			for (int i_82_ = -1 + OpenGlArrayBufferPointer.aClass36Array903.length; (i_82_ ^ 0xffffffff) <= -1; i_82_--) {
				Class36 class36 = OpenGlArrayBufferPointer.aClass36Array903[i_82_];
				if (class36 != null && class36.anInt339 != -1) {
					if ((class36.anInt346 ^ 0xffffffff) == -2) {
						NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(class36.anInt345, -1);
						if (class98_sub39 != null) {
							NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
							int i_83_ = -Class87.localPlayer.boundExtentsX + class246_sub3_sub4_sub2_sub1.boundExtentsX;
							int i_84_ = class246_sub3_sub4_sub2_sub1.boundExtentsZ - Class87.localPlayer.boundExtentsZ;
							if (!VarClientStringsDefinitionParser.aBoolean1839) {
								method3066(i_84_, class111, i_83_, false, this.aClass146Array6441[0], 92160000, var_ha, class36.anInt339);
							} else {
								method3061(var_ha, i_84_, Class16.anInt197, i_83_, true, class111, 92160000, class36.anInt339, this.aClass146Array6441[0]);
							}
						}
					}
					if (class36.anInt346 == 2) {
						int i_85_ = -Class87.localPlayer.boundExtentsX + class36.anInt338 - -256;
						int i_86_ = -Class87.localPlayer.boundExtentsZ + 256 + class36.anInt347;
						int i_87_ = class36.anInt340 << 556190985;
						i_87_ *= i_87_;
						if (VarClientStringsDefinitionParser.aBoolean1839) {
							method3061(var_ha, i_86_, Class16.anInt197, i_85_, true, class111, i_87_, class36.anInt339, this.aClass146Array6441[0]);
						} else {
							method3066(i_86_, class111, i_85_, false, this.aClass146Array6441[0], i_87_, var_ha, class36.anInt339);
						}
					}
					if ((class36.anInt346 ^ 0xffffffff) == -11 && (class36.anInt345 ^ 0xffffffff) <= -1 && class36.anInt345 < Class151_Sub9.players.length) {
						Player player_88_ = Class151_Sub9.players[class36.anInt345];
						if (player_88_ != null) {
							int i_89_ = player_88_.boundExtentsX - Class87.localPlayer.boundExtentsX;
							int i_90_ = -Class87.localPlayer.boundExtentsZ + player_88_.boundExtentsZ;
							if (!VarClientStringsDefinitionParser.aBoolean1839) {
								method3066(i_90_, class111, i_89_, false, this.aClass146Array6441[0], 92160000, var_ha, class36.anInt339);
							} else {
								method3061(var_ha, i_90_, Class16.anInt197, i_89_, true, class111, 92160000, class36.anInt339, this.aClass146Array6441[0]);
							}
						}
					}
				}
			}
			class111.method2101(i_79_);
			class111.translate(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
		}
		class111.method2101(i_79_);
		class111.translate(this.boundExtentsX, -this.anInt6351 + -5 + this.anInt5089, this.boundExtentsZ);
		if (class246_sub1 == null) {
			class246_sub1 = Class94.method915(this.aClass146Array6441.length, (byte) -47, true);
		}
		method3036(var_ha, false, (byte) -124, class111, this.aClass146Array6441);
		if (!VarClientStringsDefinitionParser.aBoolean1839) {
			for (int i_91_ = 0; (this.aClass146Array6441.length ^ 0xffffffff) < (i_91_ ^ 0xffffffff); i_91_++) {
				if (this.aClass146Array6441[i_91_] != null) {
					this.aClass146Array6441[i_91_].method2325(class111, class246_sub1.aClass246_Sub6Array5067[i_91_], Class87.localPlayer == this ? 1 : 0);
				}
			}
		} else {
			for (int i_92_ = 0; (i_92_ ^ 0xffffffff) > (this.aClass146Array6441.length ^ 0xffffffff); i_92_++) {
				if (this.aClass146Array6441[i_92_] != null) {
					this.aClass146Array6441[i_92_].method2329(class111, class246_sub1.aClass246_Sub6Array5067[i_92_], Class16.anInt197, Class87.localPlayer != this ? 0 : 1);
				}
			}
		}
		if (this.aClass246_Sub5_6439 != null) {
			Class242 class242 = this.aClass246_Sub5_6439.method3116();
			if (cprgbe)
				var_ha.method1820_cp(class242, pDescriptor, intensity, ambient);
			else
				var_ha.method1820(class242);
			/*if (VarClientStringsDefinitionParser.aBoolean1839) {
				var_ha.method1785(class242, Class16.anInt197);
			} else {
				var_ha.method1820(class242);
			}*/
		}
		for (ModelRenderer element : this.aClass146Array6441) {
			if (element != null) {
				this.aBoolean6442 |= element.F();
			}
		}
		this.anInt6417 = RemoveRoofsPreferenceField.anInt3676;
		((Mob) this).aClass146Array6441[0] = ((Mob) this).aClass146Array6441[1] = ((Mob) this).aClass146Array6441[2] = null;
		return class246_sub1;
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_73_, int i_74_) {
		if (appearence == null || !method3058((byte) -105, var_ha, 131072)) {
			return false;
		}
		Matrix class111 = var_ha.method1793();
		int i_75_ = this.yaw.value((byte) 116);
		class111.method2101(i_75_);
		class111.translate(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
		boolean bool = false;
		for (ModelRenderer element : this.aClass146Array6441) {
			if (element != null && (VarClientStringsDefinitionParser.aBoolean1839 ? element.method2333(i, i_74_, class111, true, 0, Class16.anInt197) : element.method2339(i, i_74_, class111, true, 0))) {
				bool = true;
				break;
			}
		}
		if (i_73_ < 59) {
			clanmate = true;
		}
		((Mob) this).aClass146Array6441[0] = ((Mob) this).aClass146Array6441[1] = ((Mob) this).aClass146Array6441[2] = null;
		return bool;
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_69_, RSToolkit var_ha, int i_70_, int i_71_) {
		throw new IllegalStateException();
	}

	@Override
	public final boolean method2982(byte i) {
		return false;
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		if (appearence != null && (this.aBoolean6440 || method3058((byte) 35, var_ha, 0))) {
			Matrix class111 = var_ha.method1793();
			class111.method2101(this.yaw.value((byte) 116));
			class111.translate(this.boundExtentsX, this.anInt5089 + -5, this.boundExtentsZ);
			method3036(var_ha, this.aBoolean6440, (byte) -126, class111, this.aClass146Array6441);
			((Mob) this).aClass146Array6441[0] = ((Mob) this).aClass146Array6441[1] = ((Mob) this).aClass146Array6441[2] = null;
		}
	}

	@Override
	public final void method2992(byte i) {
		throw new IllegalStateException();
	}

	public final boolean method3058(byte i, RSToolkit var_ha, int i_7_) {
		int i_8_ = i_7_;
		RenderAnimDefinition class294 = method3039(1);
		AnimationDefinition class97 = this.anInt6413 == -1 || this.anInt6400 != 0 ? null : Class151_Sub7.animationDefinitionList.method2623(this.anInt6413, 16383);
		AnimationDefinition class97_10_ = this.anInt6385 == -1 || aBoolean6520 || this.aBoolean6359 && class97 != null ? null : Class151_Sub7.animationDefinitionList.method2623(this.anInt6385, 16383);
		int i_11_ = class294.anInt2362;
		int i_12_ = class294.anInt2382;
		if ((i_11_ ^ 0xffffffff) != -1 || (i_12_ ^ 0xffffffff) != -1 || (class294.anInt2393 ^ 0xffffffff) != -1 || (class294.anInt2363 ^ 0xffffffff) != -1) {
			i_7_ |= 0x7;
		}
		int i_13_ = this.yaw.value((byte) 116);
		boolean bool = (this.aByte6422 ^ 0xffffffff) != -1 && Queue.timer >= this.anInt6403 && Queue.timer < this.anInt6349;
		if (bool) {
			i_7_ |= 0x80000;
		}
		ModelRenderer class146 = ((Mob) this).aClass146Array6441[0] = appearence.method3628(Class370.renderAnimDefinitionList, class97, class97_10_, Class151_Sub7.animationDefinitionList, this.anInt6393, ParamDefinition.identikitDefinitionList, this.anIntArray6370,
				Class98_Sub46_Sub19.itemDefinitionList, true, this.anInt6409, this.aClass226Array6387, Class4.npcDefinitionList, StartupStage.varValues, true, i_7_, this.anInt6419, this.anInt6366, this.anInt6361, this.anInt6350, i_13_, var_ha);
		int i_14_ = NewsLSEConfig.method488(true);
		if (GameShell.maxHeap < 96 && (i_14_ ^ 0xffffffff) < -51) {
			MemoryCacheNodeFactory.method2727(89);
		}
		if (BuildLocation.LIVE == client.buildLocation || (i_14_ ^ 0xffffffff) <= -51) {
			if (client.buildLocation != BuildLocation.LIVE) {
				Class98_Sub48.anInt4281 = 0;
				Class76.aByteArrayArray590 = new byte[50][];
			}
		} else {
			int i_15_;
			for (i_15_ = -i_14_ + 50; (i_15_ ^ 0xffffffff) < (Class98_Sub48.anInt4281 ^ 0xffffffff); Class98_Sub48.anInt4281++) {
				Class76.aByteArrayArray590[Class98_Sub48.anInt4281] = new byte[102400];
			}
			while ((Class98_Sub48.anInt4281 ^ 0xffffffff) < (i_15_ ^ 0xffffffff)) {
				Class98_Sub48.anInt4281--;
				Class76.aByteArrayArray590[Class98_Sub48.anInt4281] = null;
			}
		}
		if (class146 == null) {
			return false;
		}
		this.anInt6352 = class146.fa();
		this.anInt6354 = class146.ma();
		method3046(758, class146);
		if ((i_11_ ^ 0xffffffff) != -1 || i_12_ != 0) {
			method3040(false, class294.anInt2360, i_11_, i_12_, i_13_, class294.anInt2391);
			if ((this.anInt6388 ^ 0xffffffff) != -1) {
				class146.FA(this.anInt6388);
			}
			if (this.anInt6377 != 0) {
				class146.VA(this.anInt6377);
			}
			if ((this.anInt6416 ^ 0xffffffff) != -1) {
				class146.H(0, this.anInt6416, 0);
			}
		} else {
			method3040(false, 0, getSize(0) << -1003342711, getSize(0) << -31149111, i_13_, 0);
		}
		if (bool) {
			class146.method2337(this.aByte6404, this.aByte6381, this.aByte6368, 0xff & this.aByte6422);
		}
		if (!aBoolean6520 && this.anInt6379 != -1 && (this.anInt6376 ^ 0xffffffff) != 0) {
			GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, this.anInt6379);
			boolean bool_16_ = class107.aByte923 == 3 && ((i_11_ ^ 0xffffffff) != -1 || i_12_ != 0);
			int i_17_ = i_8_;
			if (!bool_16_) {
				if (this.anInt6389 != 0) {
					i_17_ |= 0x5;
				}
				if ((this.anInt6382 ^ 0xffffffff) != -1) {
					i_17_ |= 0x2;
				}
				if (this.anInt6410 >= 0) {
					i_17_ |= 0x7;
				}
			} else {
				i_17_ |= 0x7;
			}
			ModelRenderer class146_18_ = ((Mob) this).aClass146Array6441[1] = class107.method1728(this.anInt6376, Class151_Sub7.animationDefinitionList, i_17_, this.anInt6396, (byte) -95, this.anInt6367, var_ha);
			if (class146_18_ != null) {
				if ((this.anInt6410 ^ 0xffffffff) <= -1 && class294.anIntArrayArray2366 != null && class294.anIntArrayArray2366[this.anInt6410] != null) {
					int i_19_ = 0;
					int i_20_ = 0;
					int i_21_ = 0;
					if (class294.anIntArrayArray2366 != null && class294.anIntArrayArray2366[this.anInt6410] != null) {
						i_21_ += class294.anIntArrayArray2366[this.anInt6410][2];
						i_19_ += class294.anIntArrayArray2366[this.anInt6410][0];
						i_20_ += class294.anIntArrayArray2366[this.anInt6410][1];
					}
					if (class294.anIntArrayArray2364 != null && class294.anIntArrayArray2364[this.anInt6410] != null) {
						i_21_ += class294.anIntArrayArray2364[this.anInt6410][2];
						i_19_ += class294.anIntArrayArray2364[this.anInt6410][0];
						i_20_ += class294.anIntArrayArray2364[this.anInt6410][1];
					}
					if (i_21_ != 0 || (i_19_ ^ 0xffffffff) != -1) {
						int i_22_ = i_13_;
						if (this.anIntArray6370 != null && (this.anIntArray6370[this.anInt6410] ^ 0xffffffff) != 0) {
							i_22_ = this.anIntArray6370[this.anInt6410];
						}
						int i_23_ = 0x3fff & i_22_ + this.anInt6389 * 2048 + -i_13_;
						if (i_23_ != 0) {
							class146_18_.rotateYaw(i_23_);
						}
						int i_24_ = Class284_Sub2_Sub2.SINE[i_23_];
						int i_25_ = Class284_Sub2_Sub2.COSINE[i_23_];
						int i_26_ = i_25_ * i_19_ + i_24_ * i_21_ >> 1204025262;
						i_21_ = i_21_ * i_25_ + -(i_19_ * i_24_) >> -1923367154;
						i_19_ = i_26_;
					}
					class146_18_.H(i_19_, i_20_, i_21_);
				} else if (this.anInt6389 != 0) {
					class146_18_.rotateYaw(2048 * this.anInt6389);
				}
				if ((this.anInt6382 ^ 0xffffffff) != -1) {
					class146_18_.H(0, -this.anInt6382 << -849373150, 0);
				}
				if (bool_16_) {
					if (this.anInt6388 != 0) {
						class146_18_.FA(this.anInt6388);
					}
					if ((this.anInt6377 ^ 0xffffffff) != -1) {
						class146_18_.VA(this.anInt6377);
					}
					if (this.anInt6416 != 0) {
						class146_18_.H(0, this.anInt6416, 0);
					}
				}
			}
		} else {
			((Mob) this).aClass146Array6441[1] = null;
		}
		if (aBoolean6520 || this.anInt6365 == -1 || (this.anInt6428 ^ 0xffffffff) == 0) {
			((Mob) this).aClass146Array6441[2] = null;
		} else {
			GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, this.anInt6365);
			boolean bool_27_ = (class107.aByte923 ^ 0xffffffff) == -4 && (i_11_ != 0 || (i_12_ ^ 0xffffffff) != -1);
			int i_28_ = i_8_;
			if (!bool_27_) {
				if ((this.anInt6360 ^ 0xffffffff) != -1) {
					i_28_ |= 0x5;
				}
				if (this.anInt6363 != 0) {
					i_28_ |= 0x2;
				}
				if (this.anInt6353 >= 0) {
					i_28_ |= 0x7;
				}
			} else {
				i_28_ |= 0x7;
			}
			ModelRenderer class146_29_ = ((Mob) this).aClass146Array6441[2] = class107.method1721(var_ha, this.anInt6421, 21945, i_28_, Class151_Sub7.animationDefinitionList, this.anInt6427, this.anInt6428);
			if (class146_29_ != null) {
				if ((this.anInt6353 ^ 0xffffffff) > -1 || class294.anIntArrayArray2366 == null || class294.anIntArrayArray2366[this.anInt6353] == null) {
					if ((this.anInt6360 ^ 0xffffffff) != -1) {
						class146_29_.rotateYaw(2048 * this.anInt6360);
					}
				} else {
					int i_30_ = 0;
					int i_31_ = 0;
					int i_32_ = 0;
					if (class294.anIntArrayArray2366 != null && class294.anIntArrayArray2366[this.anInt6353] != null) {
						i_32_ += class294.anIntArrayArray2366[this.anInt6353][2];
						i_31_ += class294.anIntArrayArray2366[this.anInt6353][1];
						i_30_ += class294.anIntArrayArray2366[this.anInt6353][0];
					}
					if (class294.anIntArrayArray2364 != null && class294.anIntArrayArray2364[this.anInt6353] != null) {
						i_32_ += class294.anIntArrayArray2364[this.anInt6353][2];
						i_31_ += class294.anIntArrayArray2364[this.anInt6353][1];
						i_30_ += class294.anIntArrayArray2364[this.anInt6353][0];
					}
					if ((i_32_ ^ 0xffffffff) != -1 || i_30_ != 0) {
						int i_33_ = i_13_;
						if (this.anIntArray6370 != null && (this.anIntArray6370[this.anInt6353] ^ 0xffffffff) != 0) {
							i_33_ = this.anIntArray6370[this.anInt6353];
						}
						int i_34_ = 0x3fff & -i_13_ + this.anInt6360 * 2048 - -i_33_;
						if ((i_34_ ^ 0xffffffff) != -1) {
							class146_29_.rotateYaw(i_34_);
						}
						int i_35_ = Class284_Sub2_Sub2.SINE[i_34_];
						int i_36_ = Class284_Sub2_Sub2.COSINE[i_34_];
						int i_37_ = i_30_ * i_36_ + i_35_ * i_32_ >> 1334277742;
						i_32_ = i_36_ * i_32_ - i_35_ * i_30_ >> 1208318926;
						i_30_ = i_37_;
					}
					class146_29_.H(i_30_, i_31_, i_32_);
				}
				if ((this.anInt6363 ^ 0xffffffff) != -1) {
					class146_29_.H(0, -this.anInt6363 << -623157630, 0);
				}
				if (bool_27_) {
					if ((this.anInt6388 ^ 0xffffffff) != -1) {
						class146_29_.FA(this.anInt6388);
					}
					if (this.anInt6377 != 0) {
						class146_29_.VA(this.anInt6377);
					}
					if ((this.anInt6416 ^ 0xffffffff) != -1) {
						class146_29_.H(0, this.anInt6416, 0);
					}
				}
			}
		}
		return true;
	}

	private final void method3061(RSToolkit var_ha, int i, int i_41_, int i_42_, boolean bool, Matrix class111, int i_43_, int i_44_, ModelRenderer class146) {
		do {
			int i_45_ = i * i + i_42_ * i_42_;
			if ((i_45_ ^ 0xffffffff) <= -262145 && (i_43_ ^ 0xffffffff) <= (i_45_ ^ 0xffffffff)) {
				if (bool != true) {
					getRenderId((byte) 44);
				}
				int i_46_ = 0x3fff & (int) (2607.5945876176133 * Math.atan2(i_42_, i));
				ModelRenderer class146_47_ = Class98_Sub10_Sub16.method1052(this.anInt6377, i_46_, this.anInt6388, var_ha, i_44_, 106, this.anInt6416);
				if (class146_47_ == null) {
					break;
				}
				var_ha.setDepthWriteMask(false);
				class146_47_.method2329(class111, null, i_41_, 0);
				var_ha.setDepthWriteMask(true);
			}
			break;
		} while (false);
	}

	private final void method3066(int i, Matrix class111, int i_94_, boolean bool, ModelRenderer class146, int i_95_, RSToolkit var_ha, int i_96_) {
		do {
			int i_97_ = i * i + i_94_ * i_94_;
			if (i_97_ >= 262144 && (i_97_ ^ 0xffffffff) >= (i_95_ ^ 0xffffffff) && bool == false) {
				int i_98_ = (int) (Math.atan2(i_94_, i) * 2607.5945876176133) & 0x3fff;
				ModelRenderer class146_99_ = Class98_Sub10_Sub16.method1052(this.anInt6377, i_98_, this.anInt6388, var_ha, i_96_, 94, this.anInt6416);
				if (class146_99_ == null) {
					break;
				}
				var_ha.setDepthWriteMask(false);
				class146_99_.method2325(class111, null, 0);
				var_ha.setDepthWriteMask(true);
			}
			break;
		} while (false);
	}

	public final void move(int x, int z, byte speed, int i_6_) {
		if ((this.anInt6413 ^ 0xffffffff) != 0 && Class151_Sub7.animationDefinitionList.method2623(this.anInt6413, 16383).priority == 1) {
			this.anInt6413 = -1;
			this.anIntArray6373 = null;
		}
		if (this.anInt6379 != i_6_) {
			GraphicsDefinition animation = BuildLocation.gfxDefinitionList.method3564(i_6_ + 3, this.anInt6379);
			if (animation.aBoolean909 && animation.animation != -1 && (Class151_Sub7.animationDefinitionList.method2623(animation.animation, 16383).priority ^ 0xffffffff) == -2) {
				this.anInt6379 = -1;
			}
		}
		if ((this.anInt6365 ^ 0xffffffff) != 0) {
			GraphicsDefinition animation = BuildLocation.gfxDefinitionList.method3564(2, this.anInt6365);
			if (animation.aBoolean909 && animation.animation != -1 && Class151_Sub7.animationDefinitionList.method2623(animation.animation, 16383).priority == 1) {
				this.anInt6365 = -1;
			}
		}
		nextDirection = -1;
		if (x < 0 || (Class165.mapWidth ^ 0xffffffff) >= (x ^ 0xffffffff) || z < 0 || z >= Class98_Sub10_Sub7.mapLength) {
			move(z, x, 1470);
		} else if ((this.pathX[0] ^ 0xffffffff) <= -1 && (this.pathX[0] ^ 0xffffffff) > (Class165.mapWidth ^ 0xffffffff) && this.pathZ[0] >= 0 && (this.pathZ[0] ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff)) {
			if ((speed ^ 0xffffffff) == -3) {
				NewsFetcher.method3930(false, (byte) 2, this, z, x);
			}
			step(speed, 1, z, x);
		} else {
			move(z, x, 1470);
		}
	}

	public final void move(int z, int x, int i_39_) {
		do {
			pathX[0] = x;
			anInt6433 = 0;
			anInt6436 = 0;
			pathLength = 0;
			pathZ[0] = z;
			int size = getSize(0);
			boundExtentsX = 512 * this.pathX[0] + 256 * size;
			boundExtentsZ = size * 256 + this.pathZ[0] * 512;
			if (Class87.localPlayer == this) {
				WhirlpoolGenerator.method3980((byte) 126);
			}
			if (aClass246_Sub5_6439 == null) {
				break;
			}
			aClass246_Sub5_6439.method3127();
			break;
		} while (false);
	}

	public final void step(byte speed, int i_0_, int z, int x) {
		if (this.pathLength < 9) {
			this.pathLength++;
		}
		for (int count = this.pathLength; (count ^ 0xffffffff) < -1; count--) {
			((Mob) this).pathX[count] = this.pathX[-1 + count];
			((Mob) this).pathZ[count] = this.pathZ[count - 1];
			((Mob) this).pathSpeed[count] = this.pathSpeed[count - 1];
		}
		((Mob) this).pathX[0] = x;
		((Mob) this).pathZ[0] = z;
		((Mob) this).pathSpeed[0] = speed;
	}
}

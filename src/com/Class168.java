/* Class168 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class168 {
	public static HashTable aClass377_1287 = new HashTable(512);

	public static void method2532(byte i) {
		try {
			if (i == -6) {
				client.asyncCache = null;
				aClass377_1287 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "le.A(" + i + ')');
		}
	}

	public static final void method2533(int i, int i_0_, int i_1_, int i_2_, byte i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		try {
			if (i_3_ > 79) {
				if ((i_0_ ^ 0xffffffff) > -513 || i_4_ < 512 || (Class165.mapWidth * 512 + -1024 ^ 0xffffffff) > (i_0_ ^ 0xffffffff) || (Class98_Sub10_Sub7.mapLength * 512 + -1024 ^ 0xffffffff) > (i_4_ ^ 0xffffffff)) {
					Class259.anIntArray1957[0] = Class259.anIntArray1957[1] = -1;
				} else {
					int i_9_ = StrongReferenceMCNode.getHeight(i_6_, i_4_, i_0_, 24111) + -i_8_;
					if (OpenGLHeap.aBoolean6079) {
						Class98_Sub46_Sub14.method1604(true, (byte) 88);
					} else {
						SunDefinition.aClass111_1986.translate(i_2_, 0, 0);
						client.graphicsToolkit.a(SunDefinition.aClass111_1986);
					}
					if (VarClientStringsDefinitionParser.aBoolean1839) {
						client.graphicsToolkit.HA(i_0_, i_9_, i_4_, Class16.anInt197, Class259.anIntArray1957);
					} else {
						client.graphicsToolkit.da(i_0_, i_9_, i_4_, Class259.anIntArray1957);
					}
					if (OpenGLHeap.aBoolean6079) {
						Js5.method2765((byte) 38);
					} else {
						SunDefinition.aClass111_1986.translate(-i_2_, 0, 0);
						client.graphicsToolkit.a(SunDefinition.aClass111_1986);
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "le.C(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	public static final boolean method2534(byte i) {
		try {
			if (i > -126) {
				client.asyncCache = null;
			}
			return RenderAnimDefinitionParser.anInt1948 != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "le.B(" + i + ')');
		}
	}

	public byte[]	aByteArray1289;

	public short[]	aShortArray1288;

	public short[]	aShortArray1291;

	public short[]	aShortArray1292;
}

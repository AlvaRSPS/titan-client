/* Class98_Sub10_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.Launcher;
import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.BConfigDefinitionParser;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class98_Sub10_Sub3 extends Class98_Sub10 {

	public static boolean	isSnow	= Launcher.DISABLE_CONSOLE_SNOW;
	public static int[]		anIntArray1283;
	public static int[]		anIntArray10356;
	public static int[]		anIntArray1289;
	public static int[]		anIntArray10273;

	public static final void renderDevelopersConsole(RSToolkit toolkit, byte i) {
		do {
			int x = 0;
			int y = 0;
			if (OpenGLHeap.aBoolean6079) {
				x = Class189.method2642((byte) 42);
				y = MapScenesDefinitionParser.method3765(false);
			}
			toolkit.setClip(x, y, GameShell.frameWidth - -x, 350 - -y);
			toolkit.fillRectangle(x, y, GameShell.frameWidth, 350, 0x332277 | Class261.fadeInCounter << 24, 1);// Message
			// screen
			AnimatedProgressBarLSEConfig.method908(y + 350, y, false, x, x + GameShell.frameWidth);
			if (!isSnow) {//Snow
				for (int i_41_ = 0; i_41_ < 100; i_41_++) {
					int _x = anIntArray1283[i_41_] >> 4;
					int _y = anIntArray10273[i_41_] >> 4;
					_x += Class284_Sub2_Sub2.COSINE[64 * ((_x & 0x40) + _y) & 0x3fff] >> 10;
					if (_x >= 0 && _x >> 1 < anIntArray10356.length && anIntArray10356[_x >> 1] >> 2 < 350 - _y)
						toolkit.fillRectangle(_x, _y, 2, 2, anIntArray1289[i_41_] << 24 | 0xffffff, 1);
				}
				for (int i_44_ = 0; i_44_ < anIntArray10356.length; i_44_++) {
					int i_45_ = anIntArray10356[i_44_] >> 2;
					toolkit.fillRectangle(i_44_ << 1, 350 - i_45_, 2, i_45_, -2130706433, 1);
				}
			}
			int i_3_ = 350 / AsyncCache.anInt1934;
			if (Class98_Sub28.anInt4080 > 0) {
				int i_4_ = 346 + -AsyncCache.anInt1934 + -4;
				int i_5_ = i_3_ * i_4_ / (Class98_Sub28.anInt4080 + i_3_ + -1);
				int i_6_ = 4;
				if (Class98_Sub28.anInt4080 > 1) {
					i_6_ += (-i_5_ + i_4_) * (-NativeProgressMonitor.anInt3395 + Class98_Sub28.anInt4080 + -1) / (-1 + Class98_Sub28.anInt4080);
				}
				toolkit.fillRectangle(-16 + x + GameShell.frameWidth, y - -i_6_, 12, i_5_, Class261.fadeInCounter << 2108767992 | 0x332277, 2);// Scrollbar
				for (int i_7_ = NativeProgressMonitor.anInt3395; i_7_ < i_3_ + NativeProgressMonitor.anInt3395 && (i_7_ ^ 0xffffffff) > (Class98_Sub28.anInt4080 ^ 0xffffffff); i_7_++) {
					String[] strings = Class112.splitString(Class98_Sub46_Sub20.aStringArray6073[i_7_], '\010', false);
					int i_8_ = (-16 + GameShell.frameWidth - 8) / strings.length;
					for (int i_9_ = 0; (strings.length ^ 0xffffffff) < (i_9_ ^ 0xffffffff); i_9_++) {
						int i_10_ = 8 - -(i_8_ * i_9_);
						toolkit.setClip(x - -i_10_, y, i_8_ + i_10_ + x - 8, 350 + y);
						Class195.p12Full.drawString((byte) 85, -(AsyncCache.anInt1934 * (-NativeProgressMonitor.anInt3395 + i_7_)) + -Class98_Sub46_Sub10.p12FullMetrics.anInt1514 + 348 + y + -Class98_Sub10_Sub12.anInt5598, Class224_Sub1.method2834(0, strings[i_9_]), -1, -16777216, x - -i_10_);
					}
				}
			}
			Class69_Sub2.p11Full.drawStringRightAnchor(-1, 0, x - -GameShell.frameWidth + -25, -16777216, "Titan Build: 637", -20 + y - -350);
			toolkit.setClip(x, y, x - -GameShell.frameWidth, y + 350);
			toolkit.method1753(22294, GameShell.frameWidth, -1, y + 350 - Class98_Sub10_Sub12.anInt5598, x);
			Class98_Sub10_Sub34.p13Full.drawString((byte) -97, -Class42_Sub1.p13FullMetrics.anInt1514 + y + 349, "--> " + Class224_Sub1.method2834(0, ParticleManager.inputBuffer), -1, -16777216, 10 + x);
			if (!GameShell.focus) {
				break;
			}
			int i_11_ = -1;
			if (Queue.timer % 30 > 15) {
				i_11_ = 16777215;
			}
			toolkit.method1755(8479, -11 + -Class42_Sub1.p13FullMetrics.anInt1514 + 350 + y, Class42_Sub1.p13FullMetrics.method2674("--> " + Class224_Sub1.method2834(0, ParticleManager.inputBuffer).substring(0, BConfigDefinitionParser.anInt1524), 127) + 10 + x, i_11_, 12);
			break;
		} while (false);
	}

	public Class98_Sub10_Sub3() {
		super(0, true);
	}

	@Override
	public final int[] method990(int i, int i_0_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_0_);
			if (this.aClass16_3863.aBoolean198) {
				ArrayUtils.method2896(is, 0, Class25.anInt268, GameObjectDefinition.anIntArray3001[i_0_]);
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aja.G(" + i + ',' + i_0_ + ')');
		}
	}
}

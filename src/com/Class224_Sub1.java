
/* Class224_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

public final class Class224_Sub1 extends Class224 {
	public static IncomingOpcode	aClass58_5032;
	public static int				anInt5031		= 0;
	public static int[]				anIntArray5033;
	public static int[]				anIntArray5034	= { 3, 7, 15 };

	static {
		anIntArray5033 = new int[1];
		aClass58_5032 = new IncomingOpcode(33, -2);

	}

	public static final String method2834(int i, String string) {
		String string_0_ = null;
		int i_1_ = string.indexOf("--> ");
		if ((i_1_ ^ 0xffffffff) <= -1) {
			string_0_ = string.substring(0, i_1_ + 4);
			string = string.substring(i_1_ - -4);
		}
		if (string.startsWith("directlogin ")) {
			int i_2_ = string.indexOf(" ", "directlogin ".length());
			if ((i_2_ ^ 0xffffffff) <= -1) {
				int i_3_ = string.length();
				string = string.substring(0, i_2_) + " ";
				for (int i_4_ = i_2_ - -1; (i_3_ ^ 0xffffffff) < (i_4_ ^ 0xffffffff); i_4_++) {
					string += "*";
				}
			}
		}
		if (string_0_ != null) {
			return string_0_ + string;
		}
		return string;
	}
}


/* Class98_Sub43_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.toolkit.font.Font;

import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;
import jagtheora.theora.DecoderContext;
import jagtheora.theora.Frame;
import jagtheora.theora.GranulePos;
import jagtheora.theora.SetupInfo;
import jagtheora.theora.TheoraComment;
import jagtheora.theora.TheoraInfo;

public final class Class98_Sub43_Sub3 extends Class98_Sub43 {
	public static Class245[]	aClass245Array5922;
	public static Class65		aClass65_5926;
	public static int[]			anIntArray5919;

	static {
		anIntArray5919 = new int[128];
		for (int i = 0; anIntArray5919.length > i; i++) {
			anIntArray5919[i] = -1;
		}
		for (int i = 65; i <= 90; i++) {
			anIntArray5919[i] = i - 65;
		}
		for (int i = 97; (i ^ 0xffffffff) >= -123; i++) {
			anIntArray5919[i] = i + -71;
		}
		for (int i = 48; (i ^ 0xffffffff) >= -58; i++) {
			anIntArray5919[i] = 52 + -48 + i;
		}
		anIntArray5919[45] = anIntArray5919[47] = 63;
		anIntArray5919[42] = anIntArray5919[43] = 62;
		aClass65_5926 = new Class65();
	}

	public static void method1497(byte i) {
		try {
			aClass245Array5922 = null;
			anIntArray5919 = null;
			if (i <= 74) {
				aClass65_5926 = null;
			}
			aClass65_5926 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.A(" + i + ')');
		}
	}

	public static final boolean method1499(byte i, char c) {
		try {
			if (Character.isISOControl(c)) {
				return false;
			}
			if (Class114.method2147(c, 115)) {
				return true;
			}
			char[] cs = Font.aCharArray376;
			for (char c_4_ : cs) {
				if (c_4_ == c) {
					return true;
				}
			}
			char[] cs_5_ = aa_Sub3.aCharArray3572;
			int i_6_ = 0;
			if (i != 105) {
				method1497((byte) 7);
			}
			for (/**/; i_6_ < cs_5_.length; i_6_++) {
				char c_7_ = cs_5_[i_6_];
				if (c == c_7_) {
					return true;
				}
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.F(" + i + ',' + c + ')');
		}
	}

	private boolean			aBoolean5912;
	private boolean			aBoolean5913;
	private boolean			aBoolean5916;
	private boolean			aBoolean5927;
	private Sprite			aClass332_5917;
	private DecoderContext	aDecoderContext5923;
	private double			aDouble5915;
	private Frame			aFrame5911;
	private GranulePos		aGranulePos5918;
	private long			aLong5920;
	private int				anInt5925;
	private int				anInt5929;

	private SetupInfo		aSetupInfo5928;

	private TheoraComment	aTheoraComment5921;

	private TheoraInfo		aTheoraInfo5914;

	Class98_Sub43_Sub3(OggStreamState oggstreamstate) {
		super(oggstreamstate);
		try {
			aSetupInfo5928 = new SetupInfo();
			aTheoraInfo5914 = new TheoraInfo();
			aTheoraComment5921 = new TheoraComment();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.<init>(" + (oggstreamstate != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method1482(OggPacket oggpacket, boolean bool) {
		do {
			try {
				do {
					if (!aBoolean5927) {
						int i = aSetupInfo5928.decodeHeader(aTheoraInfo5914, aTheoraComment5921, oggpacket);
						if ((i ^ 0xffffffff) == -1) {
							aBoolean5927 = true;
							if (aTheoraInfo5914.frameWidth > 2048 || aTheoraInfo5914.frameHeight > 1024) {
								throw new IllegalStateException();
							}
							aDecoderContext5923 = new DecoderContext(aTheoraInfo5914, aSetupInfo5928);
							aGranulePos5918 = new GranulePos();
							aFrame5911 = new Frame(aTheoraInfo5914.frameWidth, aTheoraInfo5914.frameHeight);
							anInt5925 = aDecoderContext5923.getMaxPostProcessingLevel();
							method1495(anInt5929, -98);
							if (!GameShell.cleanedStatics) {
								break;
							}
						}
						if ((i ^ 0xffffffff) > -1) {
							throw new IllegalStateException(String.valueOf(i));
						}
					} else {
						aLong5920 = TimeTools.getCurrentTime(-47);
						int i = aDecoderContext5923.decodePacketIn(oggpacket, aGranulePos5918);
						if ((i ^ 0xffffffff) > -1) {
							throw new IllegalStateException(String.valueOf(i));
						}
						aDecoderContext5923.granuleFrame(aGranulePos5918);
						aDouble5915 = aDecoderContext5923.granuleTime(aGranulePos5918);
						if (aBoolean5916) {
							boolean bool_2_ = (oggpacket.isKeyFrame() ^ 0xffffffff) == -2;
							if (!bool_2_) {
								return;
							}
							aBoolean5916 = false;
						}
						if (!aBoolean5913 || (oggpacket.isKeyFrame() ^ 0xffffffff) == -2) {
							if ((aDecoderContext5923.decodeFrame(aFrame5911) ^ 0xffffffff) != -1) {
								throw new IllegalStateException(String.valueOf(i));
							}
							aBoolean5912 = true;
						}
					}
				} while (false);
				if (bool == false) {
					break;
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sb.J(" + (oggpacket != null ? "{...}" : "null") + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method1487(int i) {
		try {
			if (aFrame5911 != null) {
				aFrame5911.a();
			}
			if (aDecoderContext5923 != null) {
				aDecoderContext5923.a();
				aDecoderContext5923 = null;
			}
			if (aGranulePos5918 != null) {
				aGranulePos5918.a();
				aGranulePos5918 = null;
			}
			aTheoraInfo5914.a();
			aTheoraComment5921.a();
			if (i != -1128) {
				aTheoraComment5921 = null;
			}
			aSetupInfo5928.a();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.C(" + i + ')');
		}
	}

	private final void method1495(int i, int i_0_) {
		do {
			try {
				anInt5929 = i;
				if (!aBoolean5927) {
					break;
				}
				if ((anInt5925 ^ 0xffffffff) > (anInt5929 ^ 0xffffffff)) {
					anInt5929 = anInt5925;
				}
				if ((anInt5929 ^ 0xffffffff) > -1) {
					anInt5929 = 0;
				}
				aDecoderContext5923.setPostProcessingLevel(anInt5929);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sb.B(" + i + ',' + i_0_ + ')');
			}
			break;
		} while (false);
	}

	public final float method1496(int i) {
		try {
			if (i != -1) {
				return -0.24629752F;
			}
			if (!aBoolean5927 || aTheoraInfo5914.b()) {
				return 0.0F;
			}
			return (float) aTheoraInfo5914.fpsNumerator / (float) aTheoraInfo5914.fpsDenominator;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.H(" + i + ')');
		}
	}

	public final boolean method1498(int i) {
		try {
			if (i != 1024) {
				method1500(true);
			}
			return aBoolean5927;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.G(" + i + ')');
		}
	}

	public final long method1500(boolean bool) {
		try {
			if (bool != false) {
				return -59L;
			}
			return aLong5920;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.E(" + bool + ')');
		}
	}

	public final Sprite method1501(RSToolkit var_ha, int i) {
		try {
			if (aFrame5911 == null) {
				return null;
			}
			if (!aBoolean5912 && aClass332_5917 != null) {
				return aClass332_5917;
			}
			if (i != 11242) {
				return null;
			}
			aClass332_5917 = var_ha.createSprite(aFrame5911.pixels, 0, aFrame5911.a, aFrame5911.a, aFrame5911.b, false);
			aBoolean5912 = false;
			return aClass332_5917;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.D(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final double method1502(int i) {
		try {
			if (i != 0) {
				method1500(false);
			}
			return aDouble5915;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sb.I(" + i + ')');
		}
	}
}

/* Class192 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;

public final class NativeDestructorManager {
	private static boolean		disallowedAdd			= false;
	private static LinkedList	nativeDestructorList	= new LinkedList();
	private static int			nativeUsageCounter		= 0;

	public static final synchronized void add(boolean bool, NativeDestructor nativeDestructor) {
		do {
			if (!disallowedAdd) {
				if (nativeUsageCounter <= 0) {
					nativeDestructor.w(false);// w = cleanUp
				} else {
					NativeDestructorNode nativeDestructorNode = new NativeDestructorNode();
					nativeDestructorNode.nativeDestructor = nativeDestructor;
					nativeDestructorList.addLast(nativeDestructorNode, -20911);
				}
				if (bool == false) {
					break;
				}
				nativeDestructorList = null;
			}
			break;
		} while (false);
	}

	public static final synchronized void addUsage(int i) {
		do {
			nativeUsageCounter++;
			call(-76);
			break;
		} while (false);
	}

	public static final synchronized void call(int i) {
		do {
			for (;;) {
				NativeDestructorNode nativeDestructorNode = (NativeDestructorNode) nativeDestructorList.removeFirst(6494);
				if (nativeDestructorNode == null) {
					break;
				}
				nativeDestructorNode.nativeDestructor.w(true);// w = cleanUp
				nativeDestructorNode.unlink(94);
			}
			removeUsage(11);
			break;
		} while (false);
	}

	public static final synchronized void removeUsage(int i) {
		do {
			if (i <= -24) {
				nativeUsageCounter--;
				if (nativeUsageCounter != 0) {
					break;
				}
				call(0);
			}
			break;
		} while (false);
	}

	public static final synchronized void setAddAllowed(int i, boolean setAddAllowed) {
		if (i >= -41) {
			setAddAllowed(-3, false);
		}
		disallowedAdd = setAddAllowed;
	}
}

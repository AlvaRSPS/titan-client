/* Class191 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;

public final class Class191 {// TODO: Not sure if this is correct.
	public static LinkedList	aClass148_1478	= new LinkedList();
	public static int			anInt1477;
	public static int			anInt1480		= 0;
	public static Class191		JS5_FILE		= new Class191();
	public static Class191		JS5_GROUP		= new Class191();
	public static Class191		JS5_STORE		= new Class191();
	public static Class191		NATIVE_LIBRARY	= new Class191();

	public static final void method2650(byte i) {
		if (i > -118) {
			GameShell.topMargin = -121;
		}
		for (int i_0_ = 0; (i_0_ ^ 0xffffffff) > -101; i_0_++) {
			Class98_Sub46_Sub3.buffer[i_0_] = null;
		}
		RectangleLoadingScreenElement.chatLinesCount = 0;
	}

	public static final boolean method2651(int i, byte i_1_) {
		if (i_1_ >= -5) {
			return true;
		}
		return !(i != 3 && (i ^ 0xffffffff) != -6 && (i ^ 0xffffffff) != -7);
	}

	public Class191() {
		/* empty */
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

/* Class246_Sub3_Sub3_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class246_Sub3_Sub3_Sub1 extends Class246_Sub3_Sub3 implements Interface19 {
	public static boolean[]	loadedInterface;
	public static float		aFloat6257	= 1.0F;

	public static void method3017(byte i) {
		do {
			try {
				loadedInterface = null;
				if (i >= 19) {
					break;
				}
				method3017((byte) 70);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "tp.R(" + i + ')');
			}
			break;
		} while (false);
	}

	private boolean		aBoolean6253;
	private boolean		aBoolean6255	= false;
	private Class228	aClass228_6254;

	Class359			aClass359_6258;

	Class246_Sub3_Sub3_Sub1(RSToolkit var_ha, GameObjectDefinition class352, int i, int i_6_, int i_7_, int i_8_, int i_9_, boolean bool, int i_10_, int i_11_, int i_12_) {
		super(i_7_, i_8_, i_9_, i, i_6_, Class1.method160(i_11_, (byte) -101, i_10_));
		try {
			aClass359_6258 = new Class359(var_ha, class352, i_10_, i_11_, ((Char) this).plane, i_6_, this, bool, i_12_);
			aBoolean6253 = (class352.anInt2998 ^ 0xffffffff) != -1 && !bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.<init>(" + (var_ha != null ? "{...}" : "null") + ',' + (class352 != null ? "{...}" : "null") + ',' + i + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + i_9_ + ',' + bool + ',' + i_10_ + ',' + i_11_ + ',' + i_12_ + ')');
		}
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (i != -53) {
				aBoolean6255 = false;
			}
			return aClass228_6254;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = aClass359_6258.method3897(-1, false, 2048, var_ha, true);
			if (class146 == null) {
				return null;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
			Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, aBoolean6253);
			int i_17_ = this.boundExtentsX >> -600721783;
			int i_18_ = this.boundExtentsZ >> 1261366377;
			aClass359_6258.method3895(class146, i_17_, i_18_, class111, true, i_17_, var_ha, false, i_18_);
			if (VarClientStringsDefinitionParser.aBoolean1839) {
				class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
			} else {
				class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
			}
			if (i > -12) {
				aFloat6257 = 0.4871182F;
			}
			if (aClass359_6258.aClass246_Sub5_3062 != null) {
				Class242 class242 = aClass359_6258.aClass246_Sub5_3062.method3116();
				if (!VarClientStringsDefinitionParser.aBoolean1839) {
					var_ha.method1820(class242);
				} else {
					var_ha.method1785(class242, Class16.anInt197);
				}
			}
			aBoolean6255 = class146.F() || aClass359_6258.aClass246_Sub5_3062 != null;
			if (aClass228_6254 == null) {
				aClass228_6254 = Class48_Sub2_Sub1.method472(this.anInt5089, this.boundExtentsX, class146, this.boundExtentsZ, 4);
			} else {
				Class283.method3350(this.anInt5089, this.boundExtentsX, 18, this.boundExtentsZ, class146, aClass228_6254);
			}
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_0_, int i_1_) {
		try {
			ModelRenderer class146 = aClass359_6258.method3897(-1, false, 131072, var_ha, false);
			if (class146 == null) {
				return false;
			}
			if (i_0_ <= 59) {
				aBoolean6253 = false;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
			if (!VarClientStringsDefinitionParser.aBoolean1839) {
				return class146.method2339(i, i_1_, class111, false, 0);
			}
			return class146.method2333(i, i_1_, class111, false, 0, Class16.anInt197);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.H(" + i + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_2_, RSToolkit var_ha, int i_3_, int i_4_) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_2_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_3_ + ',' + i_4_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i > -70) {
				method61((byte) 39);
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.HA(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				return -32;
			}
			return aClass359_6258.method3903((byte) -102);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (i != 6540) {
				return false;
			}
			return aBoolean6255;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		do {
			try {
				ModelRenderer class146 = aClass359_6258.method3897(-1, true, 262144, var_ha, true);
				if (class146 == null) {
					break;
				}
				int i_14_ = this.boundExtentsX >> -1240765751;
				int i_15_ = this.boundExtentsZ >> -1493666103;
				Matrix class111 = var_ha.method1793();
				class111.method2100(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
				aClass359_6258.method3895(class146, i_14_, i_15_, class111, false, i_14_, var_ha, false, i_15_);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "tp.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				method62(null, 53);
			}
			return aClass359_6258.method3899((byte) 127);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.J(" + i + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		try {
			if (i != -73) {
				aFloat6257 = 1.3506109F;
			}
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.DA(" + i + ')');
		}
	}

	public final void method3016(int i, Class185 class185) {
		try {
			aClass359_6258.method3901(class185, i ^ ~0x878);
			if (i != 2048) {
				aClass228_6254 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.P(" + i + ',' + (class185 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method61(byte i) {
		try {
			if (i != -96) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.Q(" + i + ')');
		}
	}

	@Override
	public final void method62(RSToolkit var_ha, int i) {
		do {
			try {
				aClass359_6258.method3892(var_ha, i + -24343);
				if (i == 24447) {
					break;
				}
				method2981(null, (byte) 16, true, -102, null, 75, -43);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "tp.G(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int method63(byte i) {
		try {
			if (i != 20) {
				aClass228_6254 = null;
			}
			return aClass359_6258.anInt3038;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.B(" + i + ')');
		}
	}

	@Override
	public final int method64(int i) {
		try {
			if (i != 30472) {
				return -48;
			}
			return aClass359_6258.anInt3052;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.C(" + i + ')');
		}
	}

	@Override
	public final boolean method65(boolean bool) {
		try {
			if (bool != true) {
				method2990(-31);
			}
			return aClass359_6258.method3898(35);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.A(" + bool + ')');
		}
	}

	@Override
	public final int method66(int i) {
		try {
			if (i != 4657) {
				return -76;
			}
			return aClass359_6258.anInt3059;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.N(" + i + ')');
		}
	}

	@Override
	public final void method67(int i, RSToolkit var_ha) {
		try {
			if (i != -25163) {
				method2987(-89);
			}
			aClass359_6258.method3894((byte) -72, var_ha);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tp.E(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}
}

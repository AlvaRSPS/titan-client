/* Class246_Sub3_Sub5_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.FlickeringEffectsPreferenceField;
import com.jagex.game.client.preferences.ScreenSizePreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.shadow.Shadow;

public final class Class246_Sub3_Sub5_Sub2 extends Class246_Sub3_Sub5 implements Interface19 {
	public static boolean				aBoolean6272	= true;
	public static Class246_Sub3_Sub4[]	aClass246_Sub3_Sub4Array6273;
	public static int					anInt6268;

	public static final boolean method3096(int i, int i_29_, int i_30_) {
		if ((0x800 & i_30_ ^ 0xffffffff) == -1 || (0x37 & i_29_ ^ 0xffffffff) == -1) {
			return false;
		}
		return true;
	}

	public static final void method3098(Class98_Sub31_Sub2 class98_sub31_sub2, boolean bool, int i) {
		do {
			SystemInformation.aClass268_1173.method3252(0, class98_sub31_sub2);
			if (!bool) {
				break;
			}
			aa_Sub3.method159(Class119_Sub2.sound3Js5, -25233, class98_sub31_sub2, SystemInformation.aClass268_1173, BuildLocation.midiInstrumentsJs5, Class76_Sub2.soundEffectsJs5);
			break;
		} while (false);
	}

	public static final void renderInterface(int clipBottom, int i_2_, int clipTop, int interfaceId, int i_5_, int x, int clipRight, byte i_8_, int clipLeft) {
		do {
			if (!Class85.loadInterface(interfaceId, 68)) {
				if ((i_2_ ^ 0xffffffff) == 0) {
					for (int count = 0; (count ^ 0xffffffff) > -101; count++) {
						aa_Sub3.isDirty[count] = true;
					}
				} else {
					aa_Sub3.isDirty[i_2_] = true;
				}
			} else {
				int i_11_ = 0;
				int i_12_ = 0;
				int i_13_ = 0;
				int i_14_ = 0;
				int i_16_ = 0;
				if (OpenGLHeap.aBoolean6079) {
					i_13_ = ReflectionRequest.anInt3956;
					i_16_ = Class98_Sub10_Sub5.anInt5554;
					i_14_ = Class82.anInt629;
					i_11_ = ScreenSizePreferenceField.anInt3716;
					i_12_ = GameShell.anInt2;
					Class98_Sub10_Sub5.anInt5554 = 1;
				}
				if (FlickeringEffectsPreferenceField.interfaceOverride[interfaceId] == null) {
					Class98_Sub10_Sub24.renderInterface(-1, true, x, clipLeft, clipTop, i_2_, Class159.interfaceStore[interfaceId], clipRight, clipBottom, (i_2_ ^ 0xffffffff) > -1, i_5_);
				} else {
					Class98_Sub10_Sub24.renderInterface(-1, true, x, clipLeft, clipTop, i_2_, FlickeringEffectsPreferenceField.interfaceOverride[interfaceId], clipRight, clipBottom, i_2_ < 0, i_5_);
				}
				if (!OpenGLHeap.aBoolean6079) {
					break;
				}
				if (i_2_ >= 0 && Class98_Sub10_Sub5.anInt5554 == 2) {
					AnimatedProgressBarLSEConfig.method908(Class82.anInt629, GameShell.anInt2, false, ScreenSizePreferenceField.anInt3716, ReflectionRequest.anInt3956);
				}
				Class98_Sub10_Sub5.anInt5554 = i_16_;
				GameShell.anInt2 = i_12_;
				Class82.anInt629 = i_14_;
				ScreenSizePreferenceField.anInt3716 = i_11_;
				ReflectionRequest.anInt3956 = i_13_;
			}
			break;
		} while (false);
	}

	private boolean			aBoolean6269;
	private boolean			aBoolean6270;
	private boolean			aBoolean6278;
	private byte			aByte6267;
	private byte			aByte6275;

	private ModelRenderer	aClass146_6277;

	private Class228		aClass228_6274;

	private Shadow			aR6266;

	private short			aShort6276;

	Class246_Sub3_Sub5_Sub2(RSToolkit var_ha, GameObjectDefinition class352, int i, int i_20_, int i_21_, int i_22_, int i_23_, boolean bool, int i_24_, int i_25_, int i_26_, int i_27_) {
		super(i_21_, i_22_, i_23_, i, i_20_, i_24_, i_25_);
		((Char) this).boundExtentsX = i_21_;
		((Char) this).boundExtentsZ = i_23_;
		aByte6275 = (byte) i_26_;
		aByte6267 = (byte) i_27_;
		aBoolean6278 = bool;
		aShort6276 = (short) class352.id;
		aBoolean6270 = class352.anInt2998 != 0 && !bool;
		aBoolean6269 = var_ha.method1771() && class352.aBoolean2935 && !aBoolean6278 && client.preferences.sceneryShadows.getValue((byte) 120) != 0;
		Class298 class298 = method3094(2048, 0, aBoolean6269, var_ha);
		aClass146_6277 = class298.aClass146_2477;
		aR6266 = class298.aR2479;
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		if (aClass228_6274 == null) {
			aClass228_6274 = Class48_Sub2_Sub1.method472(((Char) this).anInt5089, ((Char) this).boundExtentsX, method3095(0, 0, var_ha), ((Char) this).boundExtentsZ, 4);
		}
		return aClass228_6274;
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		if (aClass146_6277 == null) {
			return null;
		}
		Matrix class111 = var_ha.method1793();
		class111.method2100(((Char) this).boundExtentsX - -((Class246_Sub3_Sub5) this).aShort6165, ((Char) this).anInt5089, ((Class246_Sub3_Sub5) this).aShort6163 + ((Char) this).boundExtentsZ);
		Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, aBoolean6270);
		do {
			if (VarClientStringsDefinitionParser.aBoolean1839) {
				aClass146_6277.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			aClass146_6277.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
		} while (false);
		return class246_sub1;
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_0_, int i_1_) {
		ModelRenderer class146 = method3095(0, 131072, var_ha);
		if (class146 != null) {
			Matrix class111 = var_ha.method1793();
			class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
			if (!VarClientStringsDefinitionParser.aBoolean1839) {
				return class146.method2339(i, i_1_, class111, false, 0);
			}
			return class146.method2333(i, i_1_, class111, false, 0, Class16.anInt197);
		}
		return false;
	}

	@Override
	public final boolean method2978(int i) {
		if (aClass146_6277 != null) {
			if (aClass146_6277.r()) {
				return false;
			}
			return true;
		}
		return true;
	}

	@Override
	public final int method2985(boolean bool) {
		if (bool != false) {
			aByte6275 = (byte) 100;
		}
		if (aClass146_6277 == null) {
			return 0;
		}
		return aClass146_6277.ma();
	}

	@Override
	public final boolean method2987(int i) {
		if (aClass146_6277 != null) {
			return aClass146_6277.F();
		}
		return false;
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
	}

	@Override
	public final int method2990(int i) {
		if (aClass146_6277 == null) {
			return 0;
		}
		return aClass146_6277.fa();
	}

	private final Class298 method3094(int i, int i_17_, boolean bool, RSToolkit var_ha) {
		GameObjectDefinition objectDefinition = Class130.gameObjectDefinitionList.get(aShort6276 & 0xffff, (byte) 119);
		if (i_17_ != 0) {
			return null;
		}
		Ground var_s;
		Ground var_s_18_;
		if (!aBoolean6278) {
			var_s = StrongReferenceMCNode.aSArray6298[((Char) this).collisionPlane];
			if ((((Char) this).collisionPlane ^ 0xffffffff) <= -4) {
				var_s_18_ = null;
			} else {
				var_s_18_ = StrongReferenceMCNode.aSArray6298[1 + ((Char) this).collisionPlane];
			}
		} else {
			var_s = Class81.aSArray618[((Char) this).collisionPlane];
			var_s_18_ = StrongReferenceMCNode.aSArray6298[0];
		}
		return objectDefinition.method3851(((Char) this).boundExtentsZ, false, var_s_18_, aByte6267, ((Char) this).anInt5089, bool, ((Char) this).boundExtentsX, i, null, var_s, var_ha, aByte6275);
	}

	private final ModelRenderer method3095(int i, int i_28_, RSToolkit var_ha) {
		if (aClass146_6277 != null && var_ha.c(aClass146_6277.functionMask(), i_28_) == 0) {
			return aClass146_6277;
		}
		Class298 class298 = method3094(i_28_, i, false, var_ha);
		if (class298 == null) {
			return null;
		}
		return class298.aClass146_2477;
	}

	@Override
	public final void method61(byte i) {
		if (aClass146_6277 != null) {
			aClass146_6277.method2326();
		}
	}

	@Override
	public final void method62(RSToolkit var_ha, int i) {
		Shadow var_r;
		if (aR6266 != null || !aBoolean6269) {
			var_r = aR6266;
			aR6266 = null;
		} else {
			Class298 class298 = method3094(262144, 0, true, var_ha);
			var_r = class298 == null ? null : class298.aR2479;
		}
		if (var_r != null) {
			Class184.method2626(var_r, ((Char) this).collisionPlane, ((Char) this).boundExtentsX, ((Char) this).boundExtentsZ, null);
		}
	}

	@Override
	public final int method63(byte i) {
		return aByte6275;

	}

	@Override
	public final int method64(int i) {
		return aShort6276 & 0xffff;
	}

	@Override
	public final boolean method65(boolean bool) {
		return aBoolean6269;
	}

	@Override
	public final int method66(int i) {
		return aByte6267;
	}

	@Override
	public final void method67(int i, RSToolkit var_ha) {
		do {
			Shadow var_r;
			if (aR6266 != null || !aBoolean6269) {
				var_r = aR6266;
				aR6266 = null;
			} else {
				Class298 class298 = method3094(262144, 0, true, var_ha);
				var_r = class298 != null ? class298.aR2479 : null;
			}
			if (var_r == null) {
				break;
			}
			Class268.method3254(var_r, ((Char) this).collisionPlane, ((Char) this).boundExtentsX, ((Char) this).boundExtentsZ, null);
			break;
		} while (false);
	}
}

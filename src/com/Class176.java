/* Class176 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.preferences.FogPreferenceField;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessage;
import com.jagex.game.client.ui.loading.impl.elements.config.ProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.NewsLoadingScreenElement;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class Class176 {
	public static Class204 aClass204_1372 = new Class204();

	public static final void addFriend(int i, String inputName) {
		if (inputName != null) {
			if ((Class314.friendListSize ^ 0xffffffff) <= -201 && !client.isMembers1 || Class314.friendListSize >= 200) {
				OpenGLHeap.addChatMessage(TextResources.FRIENDS_LIST_IS_FULL.getText(client.gameLanguage, (byte) 25), 4, (byte) 49);
				String germanLine = TextResources.DEUTSCH.getText(client.gameLanguage, (byte) 25);
				if (germanLine != null) {
					OpenGLHeap.addChatMessage(germanLine, 4, (byte) 127);
				}
			} else {
				String filteredInputName = Class353.filterName(-1, inputName);
				if (filteredInputName != null && i == 4) {
					for (int count = 0; (Class314.friendListSize ^ 0xffffffff) < (count ^ 0xffffffff); count++) {
						String friend = Class353.filterName(-1, Class98_Sub25.friends[count]);
						if (friend != null && friend.equals(filteredInputName)) {
							OpenGLHeap.addChatMessage(inputName + TextResources.FRIEND_ALREADY_ADDED.getText(client.gameLanguage, (byte) 25), 4, (byte) -32);
							return;
						}
						if (TextLSEConfig.friendsDisplayNames[count] != null) {
							String filteredDisplayName = Class353.filterName(i + -5, TextLSEConfig.friendsDisplayNames[count]);
							if (filteredDisplayName != null && filteredDisplayName.equals(filteredInputName)) {
								OpenGLHeap.addChatMessage(inputName + TextResources.FRIEND_ALREADY_ADDED.getText(client.gameLanguage, (byte) 25), 4, (byte) 119);
								return;
							}
						}
					}
					for (int count = 0; count < Class248.ignoreListSize; count++) {
						String ignore = Class353.filterName(i ^ ~0x4, FriendLoginUpdate.ignores[count]);
						if (ignore != null && ignore.equals(filteredInputName)) {
							OpenGLHeap.addChatMessage(TextResources.PLEASE_REMOVE.getText(client.gameLanguage, (byte) 25) + inputName + TextResources.FROM_YOUR_IGNORE_LIST_FIRST.getText(client.gameLanguage, (byte) 25), 4, (byte) -96);
							return;
						}
						if (ItemDeque.ignoreDisplayNames[count] != null) {
							String filteredDisplayName = Class353.filterName(-1, ItemDeque.ignoreDisplayNames[count]);
							if (filteredDisplayName != null && filteredDisplayName.equals(filteredInputName)) {
								OpenGLHeap.addChatMessage(TextResources.PLEASE_REMOVE.getText(client.gameLanguage, (byte) 25) + inputName + TextResources.FROM_YOUR_IGNORE_LIST_FIRST.getText(client.gameLanguage, (byte) 25), 4, (byte) -103);
								return;
							}
						}
					}
					if (Class353.filterName(-1, Class87.localPlayer.accountName).equals(filteredInputName)) {
						OpenGLHeap.addChatMessage(TextResources.CANT_ADD_YOURSELF_TO_FRIENDS_LIST.getText(client.gameLanguage, (byte) 25), 4, (byte) -100);
					} else {
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class246_Sub3_Sub4_Sub3.aClass171_6446, Class331.aClass117_2811);
						class98_sub11.packet.writeByte(NativeShadow.encodedStringLength(inputName, (byte) 88), i + 81);
						class98_sub11.packet.writePJStr1(inputName, (byte) 113);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					}
				}
			}
		}
	}

	public static final void method2579(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		do {
			try {
				if (i_3_ == i_2_ && i_7_ == i && i_5_ == i_6_ && i_0_ == i_1_) {
					OpenGLDisplayList.method890(i_4_, i, i_1_, i_6_, (byte) -105, i_3_);
				} else {
					int i_9_ = i_3_;
					int i_10_ = i;
					int i_11_ = 3 * i_3_;
					int i_12_ = i * 3;
					int i_13_ = i_2_ * 3;
					int i_14_ = 3 * i_7_;
					int i_15_ = i_5_ * 3;
					int i_16_ = 3 * i_0_;
					int i_17_ = i_13_ + i_6_ + -i_15_ - i_3_;
					int i_18_ = -i + i_1_ - i_16_ + i_14_;
					int i_19_ = -i_13_ + i_15_ - (i_13_ - i_11_);
					int i_20_ = i_12_ + -i_14_ + -i_14_ + i_16_;
					int i_21_ = -i_11_ + i_13_;
					int i_22_ = i_14_ - i_12_;
					for (int i_23_ = 128; i_23_ <= 4096; i_23_ += 128) {
						int i_24_ = i_23_ * i_23_ >> -1499707220;
						int i_25_ = i_24_ * i_23_ >> -1727145204;
						int i_26_ = i_17_ * i_25_;
						int i_27_ = i_18_ * i_25_;
						int i_28_ = i_19_ * i_24_;
						int i_29_ = i_20_ * i_24_;
						int i_30_ = i_23_ * i_21_;
						int i_31_ = i_22_ * i_23_;
						int i_32_ = i_3_ + (i_26_ - -i_28_ - -i_30_ >> 390504332);
						int i_33_ = i + (i_29_ + i_27_ + i_31_ >> 1323025196);
						OpenGLDisplayList.method890(i_4_, i_10_, i_33_, i_32_, (byte) -36, i_9_);
						i_9_ = i_32_;
						i_10_ = i_33_;
					}
				}
				if (i_8_ == 22024) {
					break;
				}
				aClass204_1372 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "lt.A(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ')');
			}
			break;
		} while (false);
	}

	public static void method2581(int i) {
		try {
			if (i > 59) {
				aClass204_1372 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lt.B(" + i + ')');
		}
	}

	public static final void method2582(byte i) {
		try {
			QuickChatMessage.aClass332Array6032 = null;
			Class119_Sub4.aClass332Array4739 = null;
			GameObjectDefinition.aClass332Array3000 = null;
			EnumDefinition.aClass332Array2557 = null;
			NewsLoadingScreenElement.aClass332_3471 = null;
			Class287_Sub2.aClass332Array3275 = null;
			Class69_Sub2.p11Full = null;
			Billboard.aClass332Array1382 = null;
			Class98_Sub10_Sub34.p13Full = null;
			Class254.aClass332Array1943 = null;
			OrthoZoomPreferenceField.nameIcons = null;
			FogPreferenceField.aClass332Array3675 = null;
			Class284_Sub2_Sub2.aClass332_6199 = null;
			Class195.p12Full = null;
			Class340.mapFlag = null;
			Class76_Sub7.aClass332Array3764 = null;
			Class2.aClass332Array72 = null;
			if (i >= 11) {
				ProgressBarLSEConfig.mapDots = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lt.D(" + i + ')');
		}
	}
}


/* Class98_Sub46_Sub17 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.Node;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.ui.loading.impl.elements.config.ScalingSpriteLSEConfig;

public final class Class98_Sub46_Sub17 extends Cacheable {
	public static int[][]	regionXteaKey;

	public int				anInt6053;
	public int				anInt6054;

	String					dialogueText;

	public int				headId;

	Class98_Sub46_Sub17(int i, int i_16_) {
		this.hash = i_16_ | (long) i << -706964768;
	}

	public final long method1620(byte i) {
		if (i > -10) {
			method1626((byte) -40);
		}
		return 0x7fffffffffffffffL & this.cachedKey;
	}

	public final void method1621(int i) {
		this.cachedKey = 500L + TimeTools.getCurrentTime(-47) | ~0x7fffffffffffffffL & this.cachedKey;
		if (i != 0) {
			anInt6053 = -123;
		}
		Class98_Sub10_Sub34.aClass215_5728.insert(this, i + -43);
	}

	public final int method1623(int i) {
		if (i >= -89) {
			anInt6054 = 2;
		}
		return (int) this.hash;
	}

	public final int method1625(byte i) {
		if (i != -108) {
			anInt6053 = 116;
		}
		return (int) (this.hash >>> -1338273504 & 0xffL);
	}

	public final void method1626(byte i) {
		this.cachedKey |= ~0x7fffffffffffffffL;
		if (i == -103) {
			ScalingSpriteLSEConfig.aClass215_3545.insert(this, -71);
		}
	}
}

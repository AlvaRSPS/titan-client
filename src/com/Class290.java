/* Class290 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.preferences.TexturesPreferenceField;

public final class Class290 {
	public static int[] anIntArray2198 = new int[14];

	public static final void method3411(int i, byte i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		for (Class246_Sub7 class246_sub7 = (Class246_Sub7) TexturesPreferenceField.aClass218_3694.getFirst((byte) 15); class246_sub7 != null; class246_sub7 = (Class246_Sub7) TexturesPreferenceField.aClass218_3694.getNext(false)) {
			if (class246_sub7.anInt5118 <= Queue.timer) {
				class246_sub7.unlink((byte) 5);
			} else {
				Class42_Sub1.method385(i_3_, class246_sub7.anInt5120, 256 + (class246_sub7.anInt5116 << 1991702313), i_0_ ^ ~0x7, i_4_, class246_sub7.anInt5122 * 2, 256 + (class246_sub7.anInt5123 << 999372329), i_2_ >> -1188035487, i_5_ >> 717151425);
				Class98_Sub10_Sub34.p13Full.drawStringCenterAligned(~0xffffff | class246_sub7.anInt5117, class246_sub7.aString5121, i - -Class259.anIntArray1957[0], 0, (byte) -67, i_1_ - -Class259.anIntArray1957[1]);
			}
		}
	}

	public static final void method3413(int i, int i_6_) {
		Class98_Sub46_Sub17 dialogue = Class185.method2628(i_6_, i + -72, 8);
		dialogue.method1621(i + i);

	}
}

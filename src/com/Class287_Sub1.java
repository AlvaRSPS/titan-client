
/* Class287_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.crypto.CRC32;
import com.jagex.game.client.archive.Js5Exception;

import jaclib.memory.Buffer;

public final class Class287_Sub1 extends Class287 implements ArrayBuffer {
	public static Class232	aClass232_3420;
	public static int[]		anIntArray3421;

	static {
		for (int i = 0; i < 256; i++) {
			int i_5_ = i;
			for (int i_6_ = 0; (i_6_ ^ 0xffffffff) > -9; i_6_++) {
				if ((i_5_ & 0x1) == 1) {
					i_5_ = ~0x12477cdf ^ i_5_ >>> 1453156353;
				} else {
					i_5_ >>>= 1;
				}
			}
			CRC32.crcTable[i] = i_5_;
		}
		aClass232_3420 = new Class232();
	}

	public static void method3390(int i) {
		do {
			try {
				anIntArray3421 = null;
				aClass232_3420 = null;
				CRC32.crcTable = null;
				if (i == -28614) {
					break;
				}
				aClass232_3420 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "iv.A(" + i + ')');
			}
			break;
		} while (false);
	}

	private int anInt3418;

	Class287_Sub1(OpenGlToolkit var_ha_Sub1, int i, Buffer buffer, int i_0_, boolean bool) {
		super(var_ha_Sub1, 34962, buffer, i_0_, bool);
		try {
			anInt3418 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iv.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + (buffer != null ? "{...}" : "null") + ',' + i_0_ + ',' + bool + ')');
		}
	}

	Class287_Sub1(OpenGlToolkit var_ha_Sub1, int i, byte[] is, int i_4_, boolean bool) {
		super(var_ha_Sub1, 34962, is, i_4_, bool);
		try {
			anInt3418 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iv.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + (is != null ? "{...}" : "null") + ',' + i_4_ + ',' + bool + ')');
		}
	}

	@Override
	public final void method3384(int i) {
		do {
			try {
				this.aHa_Sub1_2185.method1887(this, i + 34962);
				if (i == 0) {
					break;
				}
				method53(-28);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "iv.D(" + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final long method52(int i) {
		try {
			if (i != 24582) {
				anIntArray3421 = null;
			}
			return 0L;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iv.E(" + i + ')');
		}
	}

	@Override
	public final int method53(int i) {
		try {
			if (i != -14112) {
				return 57;
			}
			return this.anInt2191;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iv.B(" + i + ')');
		}
	}

	@Override
	public final void method54(int i, int i_1_, byte[] is, int i_2_) {
		do {
			try {
				method3389(0, i, is);
				anInt3418 = i_2_;
				if (i_1_ == 7896) {
					break;
				}
				anInt3418 = -39;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "iv.F(" + i + ',' + i_1_ + ',' + (is != null ? "{...}" : "null") + ',' + i_2_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int method55(int i) {
		try {
			return anInt3418;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iv.C(" + i + ')');
		}
	}
}


/* Class282 - Decompiled by JODE
 */ package com; /*
					*/

import java.util.Random;

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.loading.impl.config.LoadingScreenConfiguration;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public final class LoadingScreenSequence {
	public static RtInterface	aClass293_2129	= null;
	public static int			anInt2128;
	public static int			anInt2132;
	public static int[]			anIntArray2130	= { 1, 4, 1, 2 };
	public static int			rights			= 0;

	public static final void method3334(byte i, int i_0_, Mob mob) {
		if (mob.anIntArray6373 != null) {
			int i_1_ = mob.anIntArray6373[i_0_ + 1];
			if ((mob.anInt6413 ^ 0xffffffff) != (i_1_ ^ 0xffffffff)) {
				mob.anInt6413 = i_1_;
				mob.anInt6366 = 0;
				mob.anInt6436 = mob.pathLength;
				mob.anInt6361 = 1;
				mob.anInt6405 = 0;
				mob.anInt6393 = 0;
				if (mob.anInt6413 != -1) {
					Class349.method3840((byte) -127, mob, mob.anInt6393, Class151_Sub7.animationDefinitionList.method2623(mob.anInt6413, i + 16346));
				}
			}
		}
	}

	private int[][]		loadingScreenFileIds;
	private boolean[]	multipleScreens;

	private int			sequenceLength;

	private Js5			store;

	LoadingScreenSequence(GameDefinition game, int i, Js5 store) {
		this.store = store;
		this.store.getFileCount(0, 1);
		RSByteBuffer buffer = new RSByteBuffer(this.store.getFile(0, 0, false));
		int i_9_ = buffer.readUnsignedByte((byte) 67);
		if ((i_9_ ^ 0xffffffff) >= -4) {
			int i_10_ = buffer.readUnsignedByte((byte) 37);
			LoadingScreenElementType[] types = LoadingScreenElementType.getValues(false);
			boolean valid = true;
			if ((types.length ^ 0xffffffff) != (i_10_ ^ 0xffffffff)) {
				valid = false;
			} else {
				for (LoadingScreenElementType type2 : types) {
					int eRead = buffer.readUnsignedByte((byte) 9);
					if ((eRead ^ 0xffffffff) != (type2.id ^ 0xffffffff)) {
						valid = false;
						break;
					}
				}
			}
			if (!valid) {
				sequenceLength = -1;
				multipleScreens = new boolean[0];
				loadingScreenFileIds = new int[0][];
			} else {
				int entryCount = buffer.readUnsignedByte((byte) -110);
				int sequenceCount = buffer.readUnsignedByte((byte) 49);
				if (i_9_ <= 2) {
					sequenceLength = -1;
				} else {
					sequenceLength = buffer.readUShort(false);
				}
				multipleScreens = new boolean[1 + sequenceCount];
				loadingScreenFileIds = new int[1 + sequenceCount][];
				for (int i_15_ = 0; (i_15_ ^ 0xffffffff) > (entryCount ^ 0xffffffff); i_15_++) {
					int sequencePtr = buffer.readUnsignedByte((byte) -108);
					multipleScreens[sequencePtr] = (buffer.readUnsignedByte((byte) -120) ^ 0xffffffff) == -2;
					int screenCount = buffer.readShort((byte) 127);
					if (sequenceLength == -1) {
						loadingScreenFileIds[sequencePtr] = new int[screenCount];
						for (int screenPtr = 0; (screenPtr ^ 0xffffffff) > (screenCount ^ 0xffffffff); screenPtr++) {
							loadingScreenFileIds[sequencePtr][screenPtr] = buffer.readShort((byte) 127);
						}
					} else {
						loadingScreenFileIds[sequencePtr] = new int[screenCount + 1];
						loadingScreenFileIds[sequencePtr][0] = sequenceLength;
						for (int i_19_ = 0; screenCount > i_19_; i_19_++) {
							loadingScreenFileIds[sequencePtr][1 + i_19_] = buffer.readShort((byte) 127);
						}
					}
				}
				for (int sequencePtr = 0; sequenceCount + 1 > sequencePtr; sequencePtr++) {
					if (loadingScreenFileIds[sequencePtr] == null) {
						if (sequenceLength != -1) {
							loadingScreenFileIds[sequencePtr] = new int[] { sequenceLength };
						} else {
							loadingScreenFileIds[sequencePtr] = new int[0];
						}
					}
				}
			}
		} else {
			multipleScreens = new boolean[0];
			loadingScreenFileIds = new int[0][];
			sequenceLength = -1;
		}
	}

	public final int[] getSequence(int sequenceId, byte i_2_) {
		if (sequenceId < 0 || (sequenceId ^ 0xffffffff) <= (loadingScreenFileIds.length ^ 0xffffffff)) {
			if (sequenceLength != -1) {
				return new int[] { sequenceLength };
			}
			return new int[0];
		}
		if (!multipleScreens[sequenceId] || loadingScreenFileIds[sequenceId].length <= 1) {
			return loadingScreenFileIds[sequenceId];
		}
		int startOffset = (sequenceLength ^ 0xffffffff) == 0 ? 0 : 1;
		Random random = new Random();
		int[] loadingScreenFileIds = new int[this.loadingScreenFileIds[sequenceId].length];
		ArrayUtils.arrayCopy(this.loadingScreenFileIds[sequenceId], 0, loadingScreenFileIds, 0, loadingScreenFileIds.length);
		for (int ptr = startOffset; (ptr ^ 0xffffffff) > (loadingScreenFileIds.length ^ 0xffffffff); ptr++) {
			int targetPtr = HorizontalAlignment.randomNumber(-28737, -startOffset + loadingScreenFileIds.length, random) - -startOffset;
			int swapVal = loadingScreenFileIds[ptr];
			loadingScreenFileIds[ptr] = loadingScreenFileIds[targetPtr];
			loadingScreenFileIds[targetPtr] = swapVal;
		}
		return loadingScreenFileIds;
	}

	public final boolean isLoaded(int i) {
		return sequenceLength != -1;
	}

	public final LoadingScreenConfiguration unpackScreen(int i, int fileId) {
		byte[] data = store.getFile(fileId, 1, false);
		LoadingScreenConfiguration loadingScreenConfiguration = new LoadingScreenConfiguration();
		loadingScreenConfiguration.unpack(0, new RSByteBuffer(data));
		return loadingScreenConfiguration;
	}
}

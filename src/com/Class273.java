
/* Class273 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.File;

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.EnumDefinition;

public final class Class273 {
	public static int anInt2039 = 0;

	public static final void method3280(byte i) {
		try {
			for (int i_0_ = 0; (EnumDefinition.anInt2566 ^ 0xffffffff) < (i_0_ ^ 0xffffffff); i_0_++) {
				Sound class338 = Class245.aClass338Array1865[i_0_];
				if ((class338.aByte2840 ^ 0xffffffff) == -4) {
					if (class338.aClass98_Sub31_Sub5_2836 != null) {
						Class81.aClass98_Sub31_Sub3_619.method1374(class338.aClass98_Sub31_Sub5_2836);
					} else {
						class338.delay = -2147483648;
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qv.A(" + i + ')');
		}
	}

	public static final byte[] method3281(int i, File file) {
		try {
			return Class375.method3988(file, (byte) 78, (int) file.length());
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qv.B(" + i + ',' + (file != null ? "{...}" : "null") + ')');
		}
	}

	private OpenGLXToolkit	aHa_Sub3_Sub2_2041;

	int						anInt2040;

	public Class273(OpenGLXToolkit var_ha_Sub3_Sub2, int i, int i_2_) {
		try {
			anInt2040 = i_2_;
			aHa_Sub3_Sub2_2041 = var_ha_Sub3_Sub2;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qv.<init>(" + (var_ha_Sub3_Sub2 != null ? "{...}" : "null") + ',' + i + ',' + i_2_ + ')');
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			aHa_Sub3_Sub2_2041.method2085(true, anInt2040);
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qv.finalize(" + ')');
		}
	}
}

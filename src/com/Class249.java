/* Class249 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class249 {
	public static Class128	aClass128_1903	= new Class128();
	public static long		occludeCalculationTime;

	public static final String escapeHtml(String string, int i) {
		int i_0_ = string.length();
		int i_1_ = 0;
		for (int i_2_ = 0; i_0_ > i_2_; i_2_++) {
			int i_3_ = string.charAt(i_2_);
			if (i_3_ == 60 || (i_3_ ^ 0xffffffff) == -63) {
				i_1_ += 3;
			}
		}
		StringBuffer stringbuffer = new StringBuffer(i_1_ + i_0_);
		for (int i_4_ = 0; i_0_ > i_4_; i_4_++) {
			char c = string.charAt(i_4_);
			if ((c ^ 0xffffffff) != -61) {
				if (c != 62) {
					stringbuffer.append(c);
				} else {
					stringbuffer.append("<gt>");
				}
			} else {
				stringbuffer.append("<lt>");
			}
		}
		return stringbuffer.toString();
	}

	public static final int method3161(int i, int i_5_, boolean bool, int i_6_) {
		try {
			ClientInventory class98_sub3 = WaterDetailPreferenceField.get(i, bool, 6);
			if (class98_sub3 == null) {
				return 0;
			}
			if ((i_6_ ^ 0xffffffff) == 0) {
				return 0;
			}
			int i_7_ = 0;
			for (int i_9_ = 0; (i_9_ ^ 0xffffffff) > (class98_sub3.anIntArray3823.length ^ 0xffffffff); i_9_++) {
				if ((i_6_ ^ 0xffffffff) == (class98_sub3.anIntArray3824[i_9_] ^ 0xffffffff)) {
					i_7_ += class98_sub3.anIntArray3823[i_9_];
				}
			}
			return i_7_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pk.D(" + i + ',' + i_5_ + ',' + bool + ',' + i_6_ + ')');
		}
	}

	public static final void method3162() {
		for (Class174 element : Class98_Sub46_Sub5.aClass174Array5970) {
			element.method2565();
		}
		Class98_Sub46_Sub5.aClass174Array5970 = null;
	}

	public static void method3163(byte i) {
		try {
			if (i <= 34) {
				method3161(-115, -43, false, -122);
			}
			aClass128_1903 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pk.E(" + i + ')');
		}
	}

	public static final void method3164(int i, int i_10_, RSToolkit var_ha, int i_11_, int i_12_, int i_13_, int i_14_) {
		do {
			try {
				if (i_13_ != -22275) {
					aClass128_1903 = null;
				}
				if ((Class98_Sub47.aClass332_4273 == null || Class98_Sub10_Sub28.aClass332_5704 == null || ParticleManager.aClass332_383 == null) && client.spriteJs5.isFileCached(-69, Class25.anInt267) && client.spriteJs5.isFileCached(i_13_ + 22240, Class95.anInt799) && client.spriteJs5
						.isFileCached(-39, ModelRenderer.anInt1183)) {
					Image class324 = Image.loadImage(client.spriteJs5, Class95.anInt799, 0);
					Class98_Sub10_Sub28.aClass332_5704 = var_ha.createSprite(class324, true);
					class324.flipHorizontal();
					Class221.aClass332_1666 = var_ha.createSprite(class324, true);
					Class98_Sub47.aClass332_4273 = var_ha.createSprite(Image.loadImage(client.spriteJs5, Class25.anInt267, 0), true);
					Image class324_15_ = Image.loadImage(client.spriteJs5, ModelRenderer.anInt1183, 0);
					ParticleManager.aClass332_383 = var_ha.createSprite(class324_15_, true);
					class324_15_.flipHorizontal();
					Class98_Sub50.aClass332_4287 = var_ha.createSprite(class324_15_, true);
				}
				if (Class98_Sub47.aClass332_4273 == null || Class98_Sub10_Sub28.aClass332_5704 == null || ParticleManager.aClass332_383 == null) {
					break;
				}
				int i_16_ = (i + -(2 * ParticleManager.aClass332_383.getWidth())) / Class98_Sub47.aClass332_4273.getWidth();
				for (int i_17_ = 0; i_17_ < i_16_; i_17_++) {
					Class98_Sub47.aClass332_4273.draw(i_12_ + ParticleManager.aClass332_383.getWidth() + Class98_Sub47.aClass332_4273.getWidth() * i_17_, -Class98_Sub47.aClass332_4273.getHeight() + i_11_ + i_14_);
				}
				int i_18_ = (-i_10_ + i_14_ - ParticleManager.aClass332_383.getHeight()) / Class98_Sub10_Sub28.aClass332_5704.getHeight();
				for (int i_19_ = 0; i_19_ < i_18_; i_19_++) {
					Class98_Sub10_Sub28.aClass332_5704.draw(i_12_, i_10_ + i_11_ + Class98_Sub10_Sub28.aClass332_5704.getHeight() * i_19_);
					Class221.aClass332_1666.draw(-Class221.aClass332_1666.getWidth() + i + i_12_, i_19_ * Class98_Sub10_Sub28.aClass332_5704.getHeight() + i_10_ + i_11_);
				}
				ParticleManager.aClass332_383.draw(i_12_, -ParticleManager.aClass332_383.getHeight() + i_11_ - -i_14_);
				Class98_Sub50.aClass332_4287.draw(i + i_12_ - ParticleManager.aClass332_383.getWidth(), i_11_ - (-i_14_ + ParticleManager.aClass332_383.getHeight()));
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "pk.B(" + i + ',' + i_10_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_11_ + ',' + i_12_ + ',' + i_13_ + ',' + i_14_ + ')');
			}
			break;
		} while (false);
	}

	public boolean	aBoolean1904;
	public byte		aByte1906;
	public int		anInt1899;
	public int		anInt1900;
	public int		anInt1905;

	public int		anInt1907;

	public int		anInt1909;

	public short	aShort1901;

	public short	aShort1902;

	public short	aShort1908;

	public Class249(int i, int i_20_, int i_21_, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_, boolean bool, boolean bool_28_, int i_29_) {
		try {
			anInt1899 = i_29_;
			anInt1909 = i_20_;
			aBoolean1904 = bool_28_;
			anInt1905 = i;
			anInt1907 = i_22_;
			anInt1900 = i_21_;
			aShort1908 = (short) i_23_;
			aShort1901 = (short) i_24_;
			aShort1902 = (short) i_25_;
			aByte1906 = (byte) i_27_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pk.<init>(" + i + ',' + i_20_ + ',' + i_21_ + ',' + i_22_ + ',' + i_23_ + ',' + i_24_ + ',' + i_25_ + ',' + i_26_ + ',' + i_27_ + ',' + bool + ',' + bool_28_ + ',' + i_29_ + ')');
		}
	}
}

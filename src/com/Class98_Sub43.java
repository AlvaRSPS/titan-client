
/* Class98_Sub43 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.preferences.ParticlesPreferenceField;

import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;

public abstract class Class98_Sub43 extends Node {
	public static boolean	aBoolean4243	= true;
	public static int		anInt4242;

	public static final void method1483(int i, NPC class246_sub3_sub4_sub2_sub1, int i_1_, int[] is) {
		do {
			try {
				if (class246_sub3_sub4_sub2_sub1.anIntArray6373 != null) {
					boolean bool = true;
					for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > (class246_sub3_sub4_sub2_sub1.anIntArray6373.length ^ 0xffffffff); i_2_++) {
						if (class246_sub3_sub4_sub2_sub1.anIntArray6373[i_2_] != is[i_2_]) {
							bool = false;
							break;
						}
					}
					if (bool && class246_sub3_sub4_sub2_sub1.anInt6413 != -1) {
						AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class246_sub3_sub4_sub2_sub1.anInt6413, 16383);
						int i_3_ = class97.anInt819;
						if (i_3_ == 1) {
							class246_sub3_sub4_sub2_sub1.anInt6405 = 0;
							class246_sub3_sub4_sub2_sub1.anInt6400 = i;
							class246_sub3_sub4_sub2_sub1.anInt6393 = 0;
							class246_sub3_sub4_sub2_sub1.anInt6366 = 0;
							class246_sub3_sub4_sub2_sub1.anInt6361 = 1;
							if (!class246_sub3_sub4_sub2_sub1.aBoolean6371) {
								Class349.method3840((byte) -126, class246_sub3_sub4_sub2_sub1, class246_sub3_sub4_sub2_sub1.anInt6393, class97);
							}
						}
						if (i_3_ == 2) {
							class246_sub3_sub4_sub2_sub1.anInt6405 = 0;
						}
					}
				}
				if (i_1_ != 1) {
					method1483(58, null, 36, null);
				}
				boolean bool = true;
				for (int i_4_ = 0; (is.length ^ 0xffffffff) < (i_4_ ^ 0xffffffff); i_4_++) {
					if (is[i_4_] != -1) {
						bool = false;
					}
					if (class246_sub3_sub4_sub2_sub1.anIntArray6373 == null || class246_sub3_sub4_sub2_sub1.anIntArray6373[i_4_] == -1 || (Class151_Sub7.animationDefinitionList.method2623(is[i_4_], 16383).anInt829 ^ 0xffffffff) <= (Class151_Sub7.animationDefinitionList.method2623(
							class246_sub3_sub4_sub2_sub1.anIntArray6373[i_4_], i_1_ ^ 0x3ffe).anInt829 ^ 0xffffffff)) {
						class246_sub3_sub4_sub2_sub1.anIntArray6373 = is;
						class246_sub3_sub4_sub2_sub1.anInt6436 = class246_sub3_sub4_sub2_sub1.pathLength;
						class246_sub3_sub4_sub2_sub1.anInt6400 = i;
					}
				}
				if (!bool) {
					break;
				}
				class246_sub3_sub4_sub2_sub1.anInt6400 = i;
				class246_sub3_sub4_sub2_sub1.anInt6436 = class246_sub3_sub4_sub2_sub1.pathLength;
				class246_sub3_sub4_sub2_sub1.anIntArray6373 = is;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rd.P(" + i + ',' + (class246_sub3_sub4_sub2_sub1 != null ? "{...}" : "null") + ',' + i_1_ + ',' + (is != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public static final void method1485(int i) {
		try {
			if (!JavaNetworkWriter.aBoolean2575) {
				JavaNetworkWriter.aBoolean2575 = true;
				ParticlesPreferenceField.aBoolean3656 = true;
				MapRegion.aFloat2545 += (-12.0F - MapRegion.aFloat2545) / 2.0F;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rd.O(" + i + ')');
		}
	}

	public static final void setAllDirty(int i) {
		do {
			try {
				for (int i_0_ = 0; (i_0_ ^ 0xffffffff) > -101; i_0_++) {
					aa_Sub3.isDirty[i_0_] = true;
				}
				if (i == 2) {
					break;
				}
				aBoolean4243 = true;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rd.N(" + i + ')');
			}
			break;
		} while (false);
	}

	int				anInt4240;

	OggStreamState	anOggStreamState4241;

	Class98_Sub43(OggStreamState oggstreamstate) {
		try {
			anOggStreamState4241 = oggstreamstate;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rd.<init>(" + (oggstreamstate != null ? "{...}" : "null") + ')');
		}
	}

	public abstract void method1482(OggPacket oggpacket, boolean bool);

	public final void method1486(OggPacket oggpacket, int i) {
		try {
			method1482(oggpacket, false);
			if (i != 21000) {
				aBoolean4243 = false;
			}
			anInt4240++;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rd.L(" + (oggpacket != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public abstract void method1487(int i);
}

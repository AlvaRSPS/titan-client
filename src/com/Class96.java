
/* Class96 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5Manager;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;
import com.jagex.game.client.preferences.LightningDetailPreferenceField;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.elements.impl.TextLoadingScreenElement;
import com.jagex.game.input.impl.AwtMouseListener;
import com.jagex.game.toolkit.font.Font;

public final class Class96 {
	/* synthetic */ static Class	aClass805;
	public static int				anInt801;
	public static int				anInt802;
	public static int				anInt803				= 0;
	public static int				CONTENT_TYPE_COMPASS	= 1339;

	public static final void method922(boolean bool, int i) {
		do {
			try {
				if (bool) {
					if ((client.topLevelInterfaceId ^ 0xffffffff) != 0) {
						RtInterfaceAttachment.uncacheInterface(false, client.topLevelInterfaceId);
					}
					for (RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(122); class98_sub18 != null; class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.iterateNext(-1)) {
						if (!class98_sub18.isLinked((byte) 78)) {
							class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(104);
							if (class98_sub18 == null) {
								break;
							}
						}
						RtInterfaceAttachment.detachInterface(i + 16399, false, class98_sub18, true);
					}
					client.topLevelInterfaceId = -1;
					Class116.attachmentMap = new HashTable(8);
					Class76_Sub9.method768(118);
					client.topLevelInterfaceId = TextLoadingScreenElement.lobbyInterfaceId;
					Class40.calculateLayout(39, false);
					Class98_Sub43.setAllDirty(2);
					ClientScript2Runtime.sendWindowPane(client.topLevelInterfaceId);
				}
				aa_Sub3.aBoolean3569 = true;
				if (i == -1) {
					break;
				}
				CONTENT_TYPE_COMPASS = 122;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fv.B(" + bool + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method923(int i, int i_0_) {
		try {
			RtInterfaceAttachment.anInt3951 = -1;
			RenderAnimDefinitionParser.anInt1948 = 1;
			Class1.aBoolean66 = false;
			Class76_Sub8.anInt3770 = -1;
			QuickChatMessageType.anInt2911 = i_0_;
			Class224_Sub3.anInt5037 = 0;
			LightIntensityDefinitionParser.aClass207_2025 = null;
			Class116.aClass98_Sub31_Sub2_965 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fv.D(" + i + ',' + i_0_ + ')');
		}
	}

	public static final void method924(byte i, int i_2_, int i_3_, int i_4_, int i_5_) {
		try {
			int i_6_ = 0;
			int i_7_ = i_3_;
			if (i == -109) {
				int i_8_ = -i_3_;
				int i_9_ = -1;
				int i_10_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_4_ + i_3_);
				int i_11_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_4_ - i_3_);
				Class333.method3761(i_5_, AnimationDefinition.anIntArrayArray814[i_2_], i_11_, i_10_, (byte) -125);
				while (i_7_ > i_6_) {
					i_9_ += 2;
					i_8_ += i_9_;
					if (i_8_ > 0) {
						i_7_--;
						i_8_ -= i_7_ << 1928097217;
						int i_12_ = -i_7_ + i_2_;
						int i_13_ = i_2_ + i_7_;
						if (i_13_ >= Class98_Sub10_Sub38.anInt5753 && (SceneGraphNodeList.anInt1635 ^ 0xffffffff) <= (i_12_ ^ 0xffffffff)) {
							int i_14_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_4_ - -i_6_);
							int i_15_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_4_ + -i_6_);
							if ((i_13_ ^ 0xffffffff) >= (SceneGraphNodeList.anInt1635 ^ 0xffffffff)) {
								Class333.method3761(i_5_, AnimationDefinition.anIntArrayArray814[i_13_], i_15_, i_14_, (byte) -127);
							}
							if (Class98_Sub10_Sub38.anInt5753 <= i_12_) {
								Class333.method3761(i_5_, AnimationDefinition.anIntArrayArray814[i_12_], i_15_, i_14_, (byte) -125);
							}
						}
					}
					int i_16_ = -++i_6_ + i_2_;
					int i_17_ = i_6_ + i_2_;
					if ((Class98_Sub10_Sub38.anInt5753 ^ 0xffffffff) >= (i_17_ ^ 0xffffffff) && SceneGraphNodeList.anInt1635 >= i_16_) {
						int i_18_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_7_ + i_4_);
						int i_19_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_4_ - i_7_);
						if (i_17_ <= SceneGraphNodeList.anInt1635) {
							Class333.method3761(i_5_, AnimationDefinition.anIntArrayArray814[i_17_], i_19_, i_18_, (byte) -124);
						}
						if (Class98_Sub10_Sub38.anInt5753 <= i_16_) {
							Class333.method3761(i_5_, AnimationDefinition.anIntArrayArray814[i_16_], i_19_, i_18_, (byte) -125);
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fv.F(" + i + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ')');
		}
	}

	public static final int method925(int i, int i_20_, int i_21_, boolean bool) {
		try {
			ClientInventory class98_sub3 = WaterDetailPreferenceField.get(i, bool, 6);
			if (class98_sub3 == null) {
				return -1;
			}
			if (i_21_ < 0 || (class98_sub3.anIntArray3824.length ^ 0xffffffff) >= (i_21_ ^ 0xffffffff)) {
				return -1;
			}
			return class98_sub3.anIntArray3824[i_21_];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fv.A(" + i + ',' + i_20_ + ',' + i_21_ + ',' + bool + ')');
		}
	}

	public static final void method926(int i, RSToolkit var_ha) {
		do {
			try {
				if ((i <= Class359.actionCount || Class98_Sub10_Sub9.aBoolean5585) && Class255.aClass293_3208 == null) {
					String string;
					if (!Class98_Sub10_Sub9.aBoolean5585 || (Class359.actionCount ^ 0xffffffff) <= -3) {
						if (Js5Manager.shiftClickEnabled && client.keyListener.isKeyDown(81, 5503) && Class359.actionCount > 2) {
							string = HitmarksDefinition.getMenuText((byte) -124, SunDefinition.aClass98_Sub46_Sub8_1994);
						} else {
							ActionQueueEntry class98_sub46_sub8 = SunDefinition.aClass98_Sub46_Sub8_1994;
							if (class98_sub46_sub8 == null) {
								break;
							}
							string = HitmarksDefinition.getMenuText((byte) -124, class98_sub46_sub8);
							int[] is = null;
							if (AwtMouseListener.method3526(127, class98_sub46_sub8.actionId)) {
								is = Class98_Sub46_Sub19.itemDefinitionList.get((int) class98_sub46_sub8.aLong5987, (byte) -120).campaigns;
							} else if ((class98_sub46_sub8.anInt5988 ^ 0xffffffff) != 0) {
								is = Class98_Sub46_Sub19.itemDefinitionList.get(class98_sub46_sub8.anInt5988, (byte) -128).campaigns;
							} else if (!Class36.method340(class98_sub46_sub8.actionId, (byte) -61)) {
								if (Class98_Sub10_Sub21.method1064(class98_sub46_sub8.actionId, false)) {
									GameObjectDefinition class352;
									if (class98_sub46_sub8.actionId == 1009) {
										class352 = Class130.gameObjectDefinitionList.get((int) class98_sub46_sub8.aLong5987, (byte) 119);
									} else {
										class352 = Class130.gameObjectDefinitionList.get((int) (class98_sub46_sub8.aLong5987 >>> -584445664 & 0x7fffffffL), (byte) 119);
									}
									if (class352.transformIDs != null) {
										class352 = class352.get(StartupStage.varValues, (byte) -48);
									}
									if (class352 != null) {
										is = class352.anIntArray2934;
									}
								}
							} else {
								NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get((int) class98_sub46_sub8.aLong5987, i + -3);
								if (class98_sub39 != null) {
									NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
									NPCDefinition class141 = class246_sub3_sub4_sub2_sub1.definition;
									if (class141.anIntArray1109 != null) {
										class141 = class141.method2300(StartupStage.varValues, (byte) 72);
									}
									if (class141 != null) {
										is = class141.anIntArray1152;
									}
								}
							}
							if (is != null) {
								string += GroundBlendingPreferenceField.method653(0, is);
							}
						}
					} else {
						string = Class287_Sub2.aString3272 + TextResources.WHITE_SPACE.getText(client.gameLanguage, (byte) 25) + Class246_Sub3_Sub3.applyMenuText + " ->";
					}
					if (Class359.actionCount > 2) {
						string += "<col=ffffff> / " + (Class359.actionCount + -2) + TextResources.aClass309_2611.getText(client.gameLanguage, (byte) 25);
					}
					if (LoadingScreenSequence.aClass293_2129 != null) {
						Font class43 = LoadingScreenSequence.aClass293_2129.getFont(-47, var_ha);
						if (class43 == null) {
							class43 = Class98_Sub10_Sub34.p13Full;
						}
						class43.method416(WorldMapInfoDefinitionParser.anInt2858, LoadingScreenSequence.aClass293_2129.defaultColour, Class39_Sub1.anInt3591, LoadingScreenSequence.aClass293_2129.backgroundColour, LoadingScreenSequence.aClass293_2129.renderHeight, Class76_Sub8.aRandom3767,
								LoadingScreenSequence.aClass293_2129.horizontalTextAlign, Class284_Sub1_Sub2.anIntArray6193, LoadingScreenSequence.aClass293_2129.renderWidth, string, LoadingScreenSequence.aClass293_2129.verticalTextAlign, Class64_Sub5.anIntArray3652, -121,
								GroupProgressMonitor.aClass332Array3408, Class314.anInt2690);
						Class246_Sub3_Sub4_Sub3.method3071(Class284_Sub1_Sub2.anIntArray6193[3], -1, Class284_Sub1_Sub2.anIntArray6193[2], Class284_Sub1_Sub2.anIntArray6193[0], Class284_Sub1_Sub2.anIntArray6193[1]);
					} else {
						if (Class265.aClass293_1979 == null || GameDefinition.RUNESCAPE != client.game) {
							break;
						}
						int i_23_ = Class98_Sub10_Sub34.p13Full.method407(0, 16777215, 16 + LightningDetailPreferenceField.anInt3664, string, Class76_Sub8.aRandom3767, Class64_Sub5.anIntArray3652, Class39_Sub1.anInt3591, Class98_Sub10_Sub17.anInt5623 + 4, GroupProgressMonitor.aClass332Array3408,
								-70);
						Class246_Sub3_Sub4_Sub3.method3071(16, -1, Class42_Sub1.p13FullMetrics.method2674(string, i + 106) - -i_23_, Class98_Sub10_Sub17.anInt5623 + 4, LightningDetailPreferenceField.anInt3664);
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fv.E(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public static final void method928(byte i) {
		OpenGlPointLight.aHa4185.applyFog(Class284_Sub1_Sub2.layerColour, (client.preferences.fog.getValue((byte) 122) ^ 0xffffffff) == -2 ? GZipDecompressor.anInt1965 - -256 << 1972322498 : -1, 0);
		// System.out.println("Fog value: " + (GZipDecompressor.anInt1965 - -256
		// << 1972322498));
	}

	/* synthetic */ static Class method929(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}
}

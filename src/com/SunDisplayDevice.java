
/* Class68 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.DisplayMode;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.lang.reflect.Field;

public final class SunDisplayDevice {
	private DisplayMode		displayMode;
	private GraphicsDevice	graphicsDevice;

	public SunDisplayDevice() throws Exception {
		try {
			GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
			graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
			if (!graphicsDevice.isFullScreenSupported()) {
				GraphicsDevice[] graphicsDevices = graphicsEnvironment.getScreenDevices();
				GraphicsDevice[] graphics_devices = graphicsDevices;
				for (int devicesCount = 0; (graphics_devices.length ^ 0xffffffff) < (devicesCount ^ 0xffffffff); devicesCount++) {
					GraphicsDevice graphics_device = graphics_devices[devicesCount];
					if (null != graphics_device && graphics_device.isFullScreenSupported()) {
						graphicsDevice = graphics_device;
						return;
					}
				}
				throw new Exception();
			}
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final void enter(Frame frame, int width, int height, int bitDepth, int refreshRate) {
		try {
			displayMode = graphicsDevice.getDisplayMode();
			if (null == displayMode) {
				throw new NullPointerException();
			}
			frame.setUndecorated(true);
			frame.enableInputMethods(false);
			method691((byte) -92, frame);
			if (-1 == (refreshRate ^ 0xffffffff)) {
				int i_4_ = displayMode.getRefreshRate();
				DisplayMode[] displaymodes = graphicsDevice.getDisplayModes();
				boolean bool = false;
				for (int modeId = 0; (modeId ^ 0xffffffff) > (displaymodes.length ^ 0xffffffff); modeId++) {
					if (displaymodes[modeId].getWidth() == width && height == displaymodes[modeId].getHeight() && displaymodes[modeId].getBitDepth() == bitDepth) {
						int i_6_ = displaymodes[modeId].getRefreshRate();
						if (!bool || Math.abs(-i_4_ + i_6_) < Math.abs(-i_4_ + refreshRate)) {
							refreshRate = i_6_;
							bool = true;
						}
					}
				}
				if (!bool) {
					refreshRate = i_4_;
				}
			}
			graphicsDevice.setDisplayMode(new DisplayMode(width, height, bitDepth, refreshRate));
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final void exit() {
		try {
			if (null != displayMode) {
				graphicsDevice.setDisplayMode(displayMode);
				if (!graphicsDevice.getDisplayMode().equals(displayMode)) {
					throw new RuntimeException("Did not return to correct resolution!");
				}
				displayMode = null;
			}
			method691((byte) -92, null);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final int[] listModes() {
		int[] is;
		try {
			DisplayMode[] displaymodes = graphicsDevice.getDisplayModes();
			int[] modes = new int[displaymodes.length << 754645378];
			for (int i = 0; i < displaymodes.length; i++) {
				modes[i << 670030594] = displaymodes[i].getWidth();
				modes[1 + (i << 697150050)] = displaymodes[i].getHeight();
				modes[2 + (i << -399684990)] = displaymodes[i].getBitDepth();
				modes[(i << -1626028094) - -3] = displaymodes[i].getRefreshRate();
			}
			is = modes;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return is;
	}

	private final void method691(byte i, Frame frame) {
		try {
			boolean bool = false;
			try {
				Field field = Class.forName("sun.awt.Win32GraphicsDevice").getDeclaredField("valid");
				field.setAccessible(true);
				boolean bool_0_ = ((Boolean) field.get(graphicsDevice)).booleanValue();
				if (bool_0_) {
					field.set(graphicsDevice, Boolean.FALSE);
					bool = true;
				}
				if (i != -92) {
					enter(null, -99, -10, 13, 34);
				}
			} catch (Throwable throwable) {
				/* empty */
			}
			try {
				graphicsDevice.setFullScreenWindow(frame);
			} catch (Throwable t) {
				if (bool) {
					try {
						Field field = Class.forName("sun.awt.Win32GraphicsDevice").getDeclaredField("valid");
						field.set(graphicsDevice, Boolean.TRUE);
					} catch (Throwable throwable) {
						/* empty */
					}
				}
			}
			if (bool) {
				try {
					Field field = Class.forName("sun.awt.Win32GraphicsDevice").getDeclaredField("valid");
					field.set(graphicsDevice, Boolean.TRUE);
				} catch (Throwable throwable) {
					/* empty */
				}
			}
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}
}

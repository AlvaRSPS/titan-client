
/* client - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.Launcher;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.core.crypto.ISAACPseudoRNG;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.core.timetools.timebase.FrameTimeBase;
import com.jagex.game.client.archive.*;
import com.jagex.game.client.definition.*;
import com.jagex.game.client.definition.parser.*;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.*;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.loading.LoadingScreen;
import com.jagex.game.client.ui.loading.impl.AwtLoadingScreen;
import com.jagex.game.client.ui.loading.impl.ConfigurableLoadingScreen;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementFactory;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.config.*;
import com.jagex.game.client.ui.loading.impl.elements.impl.*;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.constants.BuildType;
import com.jagex.game.input.RtKeyEvent;
import com.jagex.game.input.RtKeyListener;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.input.impl.AwtMouseEvent;
import com.jagex.game.input.impl.AwtMouseListener;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.heap.Heap;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;
import com.jagex.game.toolkit.matrix.NativeMatrix;
import com.jagex.game.toolkit.matrix.OpenGlMatrix;
import com.jagex.game.toolkit.model.NativeModelRenderer;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;
import com.jagex.game.toolkit.shadow.OpenGlShadow;
import jagex3.jagmisc.jagmisc;

import java.applet.Applet;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.Socket;
import java.util.GregorianCalendar;
import java.util.Vector;

public final class client extends GameShell {
	public static client				activeClient;
	public static String				additionalInfo				= null;
	public static int					affiliateId;
	public static int					anInt3548;
	private static int					anInt3550;
	public static AsyncCache			asyncCache;
	public static BuildLocation			buildLocation;
	public static BuildType				buildType;
	public static int					clientHeight				= 503;
	public static int					clientState					= 0;
	public static int					clientWidth					= 765;
	public static int					colourId					= 0;
	/* synthetic */ static Class<?>		component;
	public static int					countryId;
	public static SeekableFile			dataFile;
	public static boolean				displayFps					= false;
	public static boolean				fontCacheSelector;
	public static Js5					fontJs5;
	public static Js5					fontMetricsJs5;
	public static boolean				force64Mb					= false;
	public static boolean				forceBilling				= false;
	public static GameDefinition		game;
	public static int					gameLanguage;
	public static Server				gameServer;
	public static RSToolkit				graphicsToolkit;
	public static boolean				HideGUI;
	public static boolean				hideInfo					= true;
	public static DataFs				indexDataFs;
	public static SeekableFile[]		indexFiles					= new SeekableFile[37];
	public static boolean				isMembers1					= false;
	public static boolean				isMembers2					= false;
	public static BigInteger			JS5_MODULUS					= new BigInteger(
			"974b219bf31b50bc1a89e45f6a0a59f21c9a83e19478ae976391cb262dc9b5f8ecfb2e864356ecbf7ca3cdbacd54ea298db5a159b0a64c25e03330fd744fe0a7181244f6ba5f3025afcb3f4122f5fbda4036b9a1b586eb33e732c5c3fd9a210ac8d42df187317c681060a4de30d9391e0b29ce9a6de27470fa3050f8b0868146efc885c62cf88378c9c19414ed3bbf98a4b8e28a8c9266654eccf773ab928973acc6aeec5648a9781463c5802379e7212fb5c480116a349840c5f04bb4b60f78e79d13a4e64b8843ef9d1273d8f80b198468051a93e6540e2510ffb8003046e10dc9bddd7bc30653936d6b178c2c6d086eeedd0a096878accbd80e6b5919806246167992dfad464b8a83e0b2f2cb3263a12b9937c206238a46ec1aea440d75af5a313de6324ac51a24bcaafeab1181b457145eea7a8ddc4009d1b62b118644ec363c521c6c491a879ace7fe9910b5084f8260855c86667c107164aba828126a12a95913507ae5519fa40452e66213918cc12e99430ffd8284b78bf6c827a4b0fbaeaa838dd9cd2e98bb825ff707df9e4465b51be3601dde7de933191328e1a36b0b950a71b681d35d6103a0d7d451cb32dc4f8dd196722a975a391d0048185e05d3ac876af0f15b3f72cbc9ff8132d4d2f29a315e0ad505e183d87c6a6022699c74038803e879bdd124b6f2f5106ea7d703abaaa2dfa86e5b4db14fadf34694b",
			16);
	public static BigInteger			JS5_PRIVATE_KEY				= new BigInteger("10001", 16);
	public static Js5Client				js5Client;
	public static SignLinkRequest		js5ConnectRequest;
	public static int					js5ConnectStage				= 0;
	public static int					js5ConnectSucceededTries	= 0;
	public static String				js5ErrorString				= null;
	public static long					js5LastConnectTime;
	public static Js5Manager			js5Manager;
	public static int					js5ReconnectTimer			= 0;
	public static ClientStream			js5Stream;
	public static boolean				jsEnabled					= false;
	public static RtKeyListener			keyListener;
	public static int					keyPressCount				= 0;
	public static RtKeyEvent[]			keyPressEvents				= new RtKeyEvent[128];
	public static long					lastGc						= 0L;
	public static long					lastRamCheck				= 0L;
	public static StartupStage			LOAD_JS5_INDICES;
	public static Js5					loadingScreenJs5;
	public static Server				lobbyServer;
	public static SeekableFile			masterIndexFile;
	public static Js5					miscellaneousJs5;
	public static RtMouseListener		mouseListener;
	public static final boolean			OFFLINE_MODE				= true;
	public static GamePreferences		preferences;
	public static SeekableFile			randomFile;
	public static boolean				safeMode					= false;
	public static Server				secondLobbyServer;
	public static Server				server;
	public static String				settingsString				= null;
	public static Js5					shaderJs5;
	public static Js5					spriteJs5;
	public static String				ssKey						= null;
	public static StartupStage[]		startupStages;
	public static Clipboard				systemClipboard;
	public static TextureMetricsList	textureList;
	public static volatile int			topLevelInterfaceId			= -1;
	public static byte[][]				unknown;
	public static boolean				useObjectTag				= false;
	public static long					userFlow					= 0L;
	public static int					worldFlags					= 0;
	public static int					zoom						= 132;

	public static final void clearAwtGraphics(int i) {
		do {
			if (fullScreenFrame != null) {
				break;
			}
			int left = leftMargin;
			int top = topMargin;
			int i_2_ = width + -frameWidth + -left;
			int i_3_ = height + -frameHeight + -top;
			if (left > 0 || i_2_ > 0 || (top ^ 0xffffffff) < -1 || i_3_ > 0) {
				try {
					java.awt.Container container;
					if (frame != null) {
						container = frame;
					} else if (applet != null) {
						container = applet;
					} else {
						container = activeGameShell;
					}
					int x = 0;
					int y = 0;
					if (frame == container) {
						Insets insets = frame.getInsets();
						x = insets.left;
						y = insets.top;
					}
					Graphics graphics = container.getGraphics();
					graphics.setColor(Color.black);
					if ((left ^ 0xffffffff) < -1) {
						graphics.fillRect(x, y, left, height);
					}
					if (top > 0) {
						graphics.fillRect(x, y, width, top);
					}
					if (i_2_ > 0) {
						graphics.fillRect(width + x + -i_2_, y, i_2_, height);
					}
					if (i_3_ <= 0) {
						break;
					}
					graphics.fillRect(x, -i_3_ + y - -height, width, i_3_);
				} catch (Exception exception) {
					/* empty */
				}
				break;
			}
		} while (false);
	}

	public static final Js5 createJs5(int i, boolean bool, int i_1_, int i_2_) {
		DataFs class17 = null;
		if (dataFile != null) {
			class17 = new DataFs(i_2_, dataFile, indexFiles[i_2_], 1000000);
		}
		Class100.js5Archives[i_2_] = js5Manager.createArchive(72, indexDataFs, class17, i_2_);
		Class100.js5Archives[i_2_].method3793(107);
		return new Js5(Class100.js5Archives[i_2_], bool, i_1_);
	}

	/* synthetic */ static Class<?> getClassByName(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public static final void handleResize(byte i) {
		synchronized (activeClient) {
			if (fullScreenFrame != null) {
				/* empty */
			} else {
				java.awt.Container container;
				if (frame == null) {
					if (applet != null) {
						container = applet;
					} else {
						container = activeGameShell;
					}
				} else {
					container = frame;
				}
				width = container.getSize().width;
				height = container.getSize().height;
				if (frame == container) {
					Insets insets = frame.getInsets();
					height -= insets.bottom + insets.top;
					width -= insets.left - -insets.right;
				}
				if (OpenGlModelRenderer.getWindowMode((byte) 118) == 1) {
					topMargin = 0;
					frameWidth = clientWidth;
					frameHeight = clientHeight;
					leftMargin = (width + -clientWidth) / 2;
				} else {
					RtMouseListener.updateMargins(false);
				}
				if (BuildLocation.LIVE != buildLocation) {
					if ((frameWidth ^ 0xffffffff) > -1025 && frameHeight < 768) {
						/* empty */
					}
				}
				canvas.setSize(frameWidth, frameHeight);
				if (graphicsToolkit != null) {
					if (!OpenGLHeap.aBoolean6079) {
						graphicsToolkit.resize(canvas, frameWidth, frameHeight);
					} else {
						RtInterface.method3471(canvas, 0);
					}
				}
				if (container == frame) {
					Insets insets = frame.getInsets();
					canvas.setLocation(insets.left + leftMargin, topMargin + insets.top);
				} else {
					canvas.setLocation(leftMargin, topMargin);
				}
				if ((topLevelInterfaceId ^ 0xffffffff) != 0) {
					Class40.calculateLayout(-1, true);
				}
				clearAwtGraphics(14059);
			}
		}
	}

	public static final void initializeToolkit(byte i, String infoString, int toolkit, boolean bool) {
		HashTableIterator.method540((byte) -51);
		if (toolkit == 0) {
			graphicsToolkit = RSToolkit.createToolkit(preferences.multiSample.getValue((byte) 120) * 2, canvas, i ^ 0x27, textureList, 0, shaderJs5);
			if (infoString != null) {
				graphicsToolkit.clearImage(0);
				FontSpecifications fontMetrics = FontSpecifications.load((byte) 120, 0, HitmarksDefinitionParser.p12FullIndex, fontJs5);
				Font font = graphicsToolkit.createFont(fontMetrics, Image.loadImages(spriteJs5, HitmarksDefinitionParser.p12FullIndex, 0), true);
				clearAwtGraphics(14059);
				Class246_Sub2.draw(-127, fontMetrics, font, true, graphicsToolkit, infoString);
			}
		} else {
			RSToolkit rsToolkit = null;
			if (infoString != null) {
				rsToolkit = RSToolkit.createToolkit(0, canvas, i + -21, textureList, 0, shaderJs5);
				rsToolkit.clearImage(0);
				FontSpecifications fontMetrics = FontSpecifications.load((byte) 115, 0, HitmarksDefinitionParser.p12FullIndex, fontJs5);
				Font font = rsToolkit.createFont(fontMetrics, Image.loadImages(spriteJs5, HitmarksDefinitionParser.p12FullIndex, 0), true);
				clearAwtGraphics(14059);
				Class246_Sub2.draw(-99, fontMetrics, font, true, rsToolkit, infoString);
			}
			try {
				graphicsToolkit = RSToolkit.createToolkit(preferences.multiSample.getValue((byte) 127) * 2, canvas, 79, textureList, toolkit, shaderJs5);
				if (infoString != null) {
					rsToolkit.clearImage(0);
					FontSpecifications fontMetrics = FontSpecifications.load((byte) 124, 0, HitmarksDefinitionParser.p12FullIndex, fontJs5);
					Font font = rsToolkit.createFont(fontMetrics, Image.loadImages(spriteJs5, HitmarksDefinitionParser.p12FullIndex, 0), true);
					clearAwtGraphics(14059);
					Class246_Sub2.draw(-127, fontMetrics, font, true, rsToolkit, infoString);
				}
				if (graphicsToolkit.needsNativeHeap()) {
					boolean heapSize = true;
					try {
						heapSize = Exception_Sub1.platformInformation.totalPhysicMemory > 256;
					} catch (Throwable throwable) {
						/* empty */
					}
					Heap heap;
					if (!heapSize) {
						heap = graphicsToolkit.createHeap(104857600);
					} else {
						heap = graphicsToolkit.createHeap(146800640);
					}
					graphicsToolkit.a(heap);
				}
			} catch (Throwable throwable) {
				int currentToolkit = preferences.currentToolkit.getValue((byte) 120);
				if ((currentToolkit ^ 0xffffffff) == -3) {
					Class223.aBoolean1679 = true;
				}
				preferences.setPreference((byte) -13, 0, preferences.currentToolkit);
				initializeToolkit((byte) 69, infoString, currentToolkit, bool);
				return;
			} finally {
				if (rsToolkit != null) {
					try {
						rsToolkit.destroy(-1);
					} catch (Throwable throwable) {
						/* empty */
					}
				}
			}
		}
		preferences.currentToolkit.setLive(!bool, false);
		preferences.setPreference((byte) -13, toolkit, preferences.currentToolkit);
		Class39.method353((byte) -79);
		graphicsToolkit.method1798(10000);
		graphicsToolkit.X(32);
		SunDefinition.aClass111_1986 = graphicsToolkit.createMatrix();
		Class76_Sub5.aClass111_3745 = graphicsToolkit.createMatrix();
		Class98_Sub10_Sub34.method1104(110);
		graphicsToolkit.method1749((preferences.unknown.getValue((byte) 122) ^ 0xffffffff) == -2);
		if (graphicsToolkit.method1767()) {
			Class98_Sub5_Sub1.method966(29089, (preferences.unknown3.getValue((byte) 122) ^ 0xffffffff) == -2);
		}
		Class159.method2508(Class165.mapWidth >> 1270556451, Class98_Sub10_Sub7.mapLength >> 1930762467, (byte) -50, graphicsToolkit);
		WhirlpoolGenerator.method3980((byte) 125);
		Class33.aBoolean316 = true;
		PlayerUpdateMask.aClass259Array527 = null;
		OpenGlGround.aBoolean5207 = false;
		Class230.method2871(i + -134);
	}

	public static final void main(String[] args) {
		try {
			if (args.length != 6) {
				printUsage(1, "Argument count");
			}

			gameServer = new Server();
			gameServer.index = Integer.parseInt(args[0]);
			lobbyServer = new Server();
			lobbyServer.index = Integer.parseInt(args[1]);
			secondLobbyServer = new Server();
			secondLobbyServer.index = Integer.parseInt(args[2]);

			buildLocation = BuildLocation.LOCAL;
			if (!args[3].equals("live")) {
				if (!args[3].equals("rc")) {
					if (!args[3].equals("wip")) {
						printUsage(1, "modewhat");
					} else {
						buildType = BuildType.WIP;
					}
				} else {
					buildType = BuildType.RC;
				}
			} else {
				buildType = BuildType.LIVE;
			}

			gameLanguage = Class76_Sub4.getId(false, args[4]);// Language.getId(...)
			if (gameLanguage == -1) {
				if (args[4].equals("english")) {
					gameLanguage = 0;
				} else if (!args[4].equals("german")) {
					printUsage(1, "language");
				} else {
					gameLanguage = 1;
				}
			}
			useObjectTag = false;
			jsEnabled = false;

			if (!args[5].equals("game0")) {
				if (!args[5].equals("game1")) {
					if (!args[5].equals("game2")) {
						if (!args[5].equals("game3")) {
							printUsage(1, "game");
						} else {
							game = GameDefinition.GAME4;
						}
					} else {
						game = GameDefinition.GAME3;
					}
				} else {
					game = GameDefinition.STELLAR_DAWN;
				}
			} else {
				game = GameDefinition.RUNESCAPE;
			}

			settingsString = "";
			ssKey = null;
			countryId = 0;
			worldFlags = 0;
			forceBilling = false;
			userFlow = 0L;
			isMembers1 = isMembers2 = true;
			additionalInfo = null;
			force64Mb = false;
			affiliateId = 0;
			colourId = game.id;
			client var_client = new client();
			activeClient = var_client;
			var_client.startWindowed(false, game.code, 37, 768, buildType.getId(-106) + 32, true, 637, 1024);
			frame.setLocation(40, 40);
		} catch (Exception exception) {
			Class305_Sub1.reportError(exception, -124, null);
		}
	}

	public static void method100(int i) {
		unknown = null;
		if (i >= -80) {
			anInt3550 = 10;
		}
		miscellaneousJs5 = null;
	}

	public static final void method101() {
		for (int i = 0; i < Class165.mapWidth; i++) {
			int[] is = Class372.anIntArrayArray3149[i];
			for (int i_10_ = 0; i_10_ < Class98_Sub10_Sub7.mapLength; i_10_++) {
				is[i_10_] = 0;
			}
		}
	}

	public static final RtInterface method102(RtInterface class293) {
		int i = method116(class293).method1663(1);
		if (i == 0) {
			return null;
		}
		for (int i_11_ = 0; i_11_ < i; i_11_++) {
			class293 = RtInterface.getInterface(class293.parentId, -9820);
			if (class293 == null) {
				return null;
			}
		}
		return class293;
	}

	public static final void method103(int i, RSToolkit toolkit) {
		do {
			if (Canvas_Sub1.anInt26 != Class87.localPlayer.plane && QuickChatCategory.aClass172ArrayArrayArray5948 != null) {
				if (!Class98_Sub10_Sub5.method1015(toolkit, Class87.localPlayer.plane, 8939)) {
					break;
				}
				Canvas_Sub1.anInt26 = Class87.localPlayer.plane;
			}
			break;
		} while (false);
	}

	public static final void method104(RtInterface[] components, int parentId, int clipLeft, int clipTop, int clipRight, int clipBottom, int x, int y, int i_18_, int i_19_, int i_20_, int i_21_) {
		for (int componentId = 0; componentId < components.length; componentId++) {
			RtInterface compId = components[componentId];
			if (compId != null && compId.parentId == parentId) {
				int compX = compId.renderX + x;
				int compY = compId.renderY + y;
				int clipL;
				int clipT;
				int clipR;
				int clipB;
				if (compId.type == 2) {
					clipL = clipLeft;
					clipT = clipTop;
					clipR = clipRight;
					clipB = clipBottom;
				} else {
					int compR = compX + compId.renderWidth;
					int compB = compY + compId.renderHeight;
					if (compId.type == 9) {
						compR++;
						compB++;
					}
					clipL = compX > clipLeft ? compX : clipLeft;
					clipT = compY > clipTop ? compY : clipTop;
					clipR = compR < clipRight ? compR : clipRight;
					clipB = compB < clipBottom ? compB : clipBottom;
				}
				if (compId.type != 0 && !compId.aBoolean2209 && method116(compId).anInt4284 == 0 && compId != Class189.aClass293_1457 && compId.contentType != Orientation.CONTENT_TYPE_MINIMAP && compId.contentType != SimpleProgressBarLoadingScreenElement.anInt5471) {
					if (clipL < clipR && clipT < clipB) {
						Class98_Sub10_Sub21.method1066(108, compId);
					}
				} else if (!method111(compId)) {
					int offX = 0;
					int offY = 0;
					if (OpenGLHeap.aBoolean6079) {
						offX = Class189.method2642((byte) 42);
						offY = MapScenesDefinitionParser.method3765(false);
					}
					if (compId == Class255.aClass293_3208 && ParamDefinitionParser.method3939(4456, Class255.aClass293_3208) != null) {
						Class166.aBoolean1278 = true;
						VarPlayerDefinition.anInt1286 = compX;
						Class259.anInt1960 = compY;
					}
					if (compId.aBoolean2222 || clipL < clipR && clipT < clipB) {
						if (compId.aBoolean2286 && i_20_ >= clipL && i_21_ >= clipT && i_20_ < clipR && i_21_ < clipB) {
							for (ClientScript2Event class98_sub21 = (ClientScript2Event) Class151_Sub3.aClass148_4977.getFirst(32); class98_sub21 != null; class98_sub21 = (ClientScript2Event) Class151_Sub3.aClass148_4977.getNext(103)) {
								if (class98_sub21.aBoolean3980) {
									class98_sub21.unlink(118);
									class98_sub21.component.mouseOver = false;
								}
							}
							if (Class105.anInt3417 == 0) {
								Class255.aClass293_3208 = null;
								Class189.aClass293_1457 = null;
							}
							Class156_Sub2.anInt3423 = 0;
							AsyncCache.aBoolean1930 = false;
							PrefetchObject.aBoolean889 = false;
							if (!Player.menuOpen) {
								Class46.method435((byte) 103);
							}
						}
						boolean mouseOver;
						mouseOver = mouseListener.getMouseX(124) + offX >= clipL && mouseListener.getMouseY((byte) 60) + offY >= clipT && mouseListener.getMouseX(70) + offX < clipR && mouseListener.getMouseY((byte) 49) + offY < clipB;
						if (!Class98_Sub10_Sub9.aBoolean5585 && mouseOver) {
							if (compId.anInt2317 >= 0) {
								Class21_Sub2.cursorId = compId.anInt2317;
							} else if (compId.aBoolean2286) {
								Class21_Sub2.cursorId = -1;
							}
						}
						if (!Player.menuOpen && i_20_ >= clipL && i_21_ >= clipT && i_20_ < clipR && i_21_ < clipB) {
							InventoriesDefinitionParser.method186(59, compId, i_21_ - compY, i_20_ - compX);
						}
						boolean mouseButtonOneDownOverComp = false;
						if (mouseListener.method3506((byte) 121) && mouseOver) {
							mouseButtonOneDownOverComp = true;
						}
						boolean mousePressedOnComp = false;
						RtMouseEvent mousePressedEvent = (RtMouseEvent) VarPlayerDefinition.aClass148_1284.getFirst(32);
						if (mousePressedEvent != null && mousePressedEvent.getEventType(-5) == 0 && mousePressedEvent.getMouseX(124) + offX >= clipL && mousePressedEvent.getMouseY(123) + offY >= clipT && mousePressedEvent.getMouseX(125) + offX < clipR && mousePressedEvent.getMouseY(80)
								+ offY < clipB) {
							mousePressedOnComp = true;
						}
						if (compId.keyCodes != null && !GroupProgressMonitor.isOpen(-8001)) {
							for (int code = 0; code < compId.keyCodes.length; code++) {
								if (!keyListener.isKeyDown(compId.keyCodes[code], 5503)) {
									if (compId.keyRepeatTimer != null) {
										compId.keyRepeatTimer[code] = 0;
									}
								} else if (compId.keyRepeatTimer == null || Queue.timer >= compId.keyRepeatTimer[code]) {
									byte reqdModifiers = compId.keyModifiers[code];
									if (reqdModifiers == 0 || ((reqdModifiers & 0x8) == 0 || !keyListener.isKeyDown(86, 5503) && !keyListener.isKeyDown(82, 5503) && !keyListener.isKeyDown(81, 5503)) && ((reqdModifiers & 0x2) == 0 || keyListener.isKeyDown(86, 5503)) && ((reqdModifiers & 0x1) == 0
											|| keyListener.isKeyDown(82, 5503)) && ((reqdModifiers & 0x4) == 0 || keyListener.isKeyDown(81, 5503))) {
										if (code < 10) {
											Class303.handleKeysPressed(code + 1, -1, "", -120, compId.idDword);
										} else if (code == 10) {
											Class98_Sub10_Sub32.method1098((byte) -118);
											Class98_Sub49 class98_sub49 = method116(compId);
											Class98_Sub5_Sub2.method970(class98_sub49.anInt4285, compId, class98_sub49.method1668(-1), -6838);
											Class287_Sub2.aString3272 = Class170.method2538(-1, compId);
											if (Class287_Sub2.aString3272 == null) {
												Class287_Sub2.aString3272 = "Null";
											}
											Class246_Sub3_Sub3.applyMenuText = compId.applyText + "<col=ffffff>";
										}
										int kRepeat = compId.keyRepeat[code];
										if (compId.keyRepeatTimer == null) {
											compId.keyRepeatTimer = new int[compId.keyCodes.length];
										}
										if (kRepeat != 0) {
											compId.keyRepeatTimer[code] = Queue.timer + kRepeat;
										} else {
											compId.keyRepeatTimer[code] = 2147483647;
										}
									}
								}
							}
						}
						if (mousePressedOnComp) {
							NativeModelRenderer.method2405(compId, (byte) 52, offX + mousePressedEvent.getMouseX(-89) - compX, offY + mousePressedEvent.getMouseY(47) - compY);
						}
						if (Class255.aClass293_3208 != null && Class255.aClass293_3208 != compId && mouseOver && method116(compId).method1664(-1)) {
							Class162.aClass293_1272 = compId;
						}
						if (compId == Class189.aClass293_1457) {
							VarClientStringsDefinitionParser.aBoolean1840 = true;
							NPC.anInt6500 = compX;
							LoginOpcode.anInt1670 = compY;
						}
						if (compId.aBoolean2209 || compId.contentType != 0) {
							if (mouseOver && Class319.mouseScrollDelta != 0 && compId.anObjectArray2277 != null) {
								ClientScript2Event event = new ClientScript2Event();
								event.aBoolean3980 = true;
								event.component = compId;
								event.mouseY = Class319.mouseScrollDelta;
								event.param = compId.anObjectArray2277;
								Class151_Sub3.aClass148_4977.addLast(event, -20911);
							}
							if (Class255.aClass293_3208 != null || Player.menuOpen || compId.contentType != Class87.anInt673 && Class156_Sub2.anInt3423 > 0) {
								mousePressedOnComp = false;
								mouseButtonOneDownOverComp = false;
								mouseOver = false;
							}
							if (compId.contentType != 0) {
								if (compId.contentType == Class22.CONTENT_TYPE_LOGIN_BG || compId.contentType == TexturesPreferenceField.CONTENT_TYPE_GAME_VIEW) {
									GraphicsBuffer.aClass293_4107 = compId;
									if (OpenGlGround.aClass346_5202 != null) {
										OpenGlGround.aClass346_5202.method3828(compId.renderHeight, 6, graphicsToolkit);
									}
									if (compId.contentType == Class22.CONTENT_TYPE_LOGIN_BG) {
										if (!Player.menuOpen && i_20_ >= clipL && i_21_ >= clipT && i_20_ < clipR && i_21_ < clipB) {
											Class39.method350(0, i_19_, i_18_, graphicsToolkit);
											for (Class246_Sub2 class246_sub2 = (Class246_Sub2) Class151_Sub2.aClass218_4973.getFirst((byte) 15); class246_sub2 != null; class246_sub2 = (Class246_Sub2) Class151_Sub2.aClass218_4973.getNext(false)) {
												if (i_20_ >= class246_sub2.anInt5074 && i_20_ < class246_sub2.anInt5073 && i_21_ >= class246_sub2.anInt5071 && i_21_ < class246_sub2.anInt5075) {
													Class46.method435((byte) 101);
													Class246_Sub3_Sub3.method3012(class246_sub2.aClass246_Sub3_Sub4_Sub2_5076, (byte) -124);
												}
											}
										}
										continue;
									}
								}
								if (compId.contentType == Orientation.CONTENT_TYPE_MINIMAP) {
									if (compId.getClip(graphicsToolkit, 115) != null && (Class333.anInt3386 == 0 || Class333.anInt3386 == 3) && !Player.menuOpen && i_20_ >= clipL && i_21_ >= clipT && i_20_ < clipR && i_21_ < clipB) {
										int i_38_ = i_20_ - compX;
										int i_39_ = i_21_ - compY;
										int i_40_ = compId.anIntArray2217[i_39_];
										if (i_38_ >= i_40_ && i_38_ <= i_40_ + compId.anIntArray2298[i_39_]) {
											i_38_ -= compId.renderWidth / 2;
											i_39_ -= compId.renderHeight / 2;
											int i_41_;
											if (Class98_Sub46_Sub20_Sub2.cameraMode == 4) {
												i_41_ = (int) RsFloatBuffer.aFloat5794 & 0x3fff;
											} else {
												i_41_ = (int) RsFloatBuffer.aFloat5794 + Class204.anInt1553 & 0x3fff;
											}
											int i_42_ = Class284_Sub2_Sub2.SINE[i_41_];
											int i_43_ = Class284_Sub2_Sub2.COSINE[i_41_];
											if (Class98_Sub46_Sub20_Sub2.cameraMode != 4) {
												i_42_ = i_42_ * (Class151.anInt1213 + 256) >> 8;
												i_43_ = i_43_ * (Class151.anInt1213 + 256) >> 8;
											}
											int i_44_ = i_39_ * i_42_ + i_38_ * i_43_ >> 14;
											int i_45_ = i_39_ * i_43_ - i_38_ * i_42_ >> 14;
											int i_46_;
											int i_47_;
											if (Class98_Sub46_Sub20_Sub2.cameraMode == 4) {
												i_46_ = (StrongReferenceMCNode.anInt6295 >> 9) + (i_44_ >> 2);
												i_47_ = (Js5Client.anInt1051 >> 9) - (i_45_ >> 2);
											} else {
												int i_48_ = (Class87.localPlayer.getSize(0) - 1) * 256;
												i_46_ = (Class87.localPlayer.boundExtentsX - i_48_ >> 9) + (i_44_ >> 2);
												i_47_ = (Class87.localPlayer.boundExtentsZ - i_48_ >> 9) - (i_45_ >> 2);
											}
											if (Class98_Sub10_Sub9.aBoolean5585 && (Class98_Sub4.anInt3826 & 0x40) != 0) {
												RtInterface class293_49_ = Class246_Sub9.getDynamicComponent((byte) 72, Class187.anInt1450, Class310.anInt2652);
												if (class293_49_ != null) {
													ActionQueueEntry.addAction(false, true, 1L, Class336.anInt2823, i_46_, " ->", true, i_47_, 46, compId.componentIndex << 32 | compId.idDword, compId.unknown, false, Class287_Sub2.aString3272);
												} else {
													Class98_Sub10_Sub32.method1098((byte) 111);
												}
											} else {
												if (game == GameDefinition.STELLAR_DAWN) {
													ActionQueueEntry.addAction(false, true, 1L, -1, i_46_, "", true, i_47_, 60, 0L, -1, false, TextResources.FACE_HERE.getText(gameLanguage, (byte) 25));
												}
												ActionQueueEntry.addAction(false, true, 1L, FloorOverlayDefinition.anInt1541, i_46_, "", true, i_47_, 6, 0L, -1, false, SceneGraphNodeList.optionText);
											}
										}
									}
									continue;
								}
								if (compId.contentType == Class87.anInt673) {
									CursorDefinitionParser.aClass293_120 = compId;
									if (mouseOver) {
										AsyncCache.aBoolean1930 = true;
									}
									if (mousePressedOnComp) {
										int i_50_ = (int) ((offX + mousePressedEvent.getMouseX(19) - compX - compId.renderWidth / 2) * 2.0 / WorldMap.aFloat2064);
										int i_51_ = (int) -((offY + mousePressedEvent.getMouseY(69) - compY - compId.renderHeight / 2) * 2.0 / WorldMap.aFloat2064);
										int i_52_ = Class42_Sub4.anInt5371 + i_50_ + WorldMap.anInt2075;
										int i_53_ = NodeShort.anInt4197 + i_51_ + WorldMap.anInt2078;
										Class98_Sub46_Sub10 class98_sub46_sub10 = Class98_Sub10_Sub8.method1026(-3);
										if (class98_sub46_sub10 != null) {
											int[] coords = new int[3];
											class98_sub46_sub10.method1563(31960, i_52_, coords, i_53_);
											if (coords != null) {
												if (keyListener.isKeyDown(82, 5503) && LoadingScreenSequence.rights > 0) {
													Class351.handleClickTeleport(coords[2], coords[1], coords[0], -52);
													continue;
												}
												PrefetchObject.aBoolean889 = true;
												Class333.xTele = coords[0];
												NameHashTable.yTele = coords[1];
												Class375.planeTele = coords[2];
											}
											Class156_Sub2.anInt3423 = 1;
											Class81.aBoolean621 = false;
											BaseModel.mouseX = mouseListener.getMouseX(106);
											PlayerUpdateMask.mouseY = mouseListener.getMouseY((byte) 46);
										}
									} else if (mouseButtonOneDownOverComp && Class156_Sub2.anInt3423 > 0) {
										if (Class156_Sub2.anInt3423 == 1 && (BaseModel.mouseX != mouseListener.getMouseX(64) || PlayerUpdateMask.mouseY != mouseListener.getMouseY((byte) 89))) {
											anInt3550 = Class42_Sub4.anInt5371;
											Class265.anInt1975 = NodeShort.anInt4197;
											Class156_Sub2.anInt3423 = 2;
										}
										if (Class156_Sub2.anInt3423 == 2) {
											Class81.aBoolean621 = true;
											Class92.method891(anInt3550 + (int) ((BaseModel.mouseX - mouseListener.getMouseX(27)) * 2.0 / WorldMap.zoom), -94);
											LoadingScreenElementType.method2144(-4365, Class265.anInt1975 - (int) ((PlayerUpdateMask.mouseY - mouseListener.getMouseY((byte) 92)) * 2.0 / WorldMap.zoom));
										}
									} else {
										if (Class156_Sub2.anInt3423 > 0 && !Class81.aBoolean621) {
											if ((Class305_Sub1.anInt5303 == 1 || OpenGlShadow.method1642((byte) 127)) && Class359.actionCount > 2) {
												Class353.method3869(PlayerUpdateMask.mouseY, BaseModel.mouseX, -2);
											} else if (AwtMouseEvent.method1160(90)) {
												Class353.method3869(PlayerUpdateMask.mouseY, BaseModel.mouseX, -2);
											}
										}
										Class156_Sub2.anInt3423 = 0;
									}
									continue;
								}
								if (compId.contentType == VarClientStringsDefinitionParser.anInt1841) {
									if (mouseButtonOneDownOverComp) {
										Class202.method2698((byte) 48, offY + mouseListener.getMouseY((byte) 29) - compY, compId.renderWidth, offX + mouseListener.getMouseX(18) - compX, compId.renderHeight);
									}
									continue;
								}
								if (compId.contentType == SimpleProgressBarLoadingScreenElement.anInt5471) {
									GraphicsDefinitionParser.method3563(compY, compId, compX, 60);
									continue;
								}
							}
							if (!compId.mousePressed && mousePressedOnComp) {
								compId.mousePressed = true;
								if (compId.mousePressedHandler != null) {
									ClientScript2Event event = new ClientScript2Event();
									event.aBoolean3980 = true;
									event.component = compId;
									event.mouseX = offX + mousePressedEvent.getMouseX(-25) - compX;
									event.mouseY = offY + mousePressedEvent.getMouseY(54) - compY;
									event.param = compId.mousePressedHandler;
									Class151_Sub3.aClass148_4977.addLast(event, -20911);
								}
							}
							if (compId.mousePressed && mouseButtonOneDownOverComp && compId.mouseDraggedHandler != null) {
								ClientScript2Event event = new ClientScript2Event();
								event.aBoolean3980 = true;
								event.component = compId;
								event.mouseX = offX + mouseListener.getMouseX(118) - compX;
								event.mouseY = offY + mouseListener.getMouseY((byte) 47) - compY;
								event.param = compId.mouseDraggedHandler;
								Class151_Sub3.aClass148_4977.addLast(event, -20911);
							}
							if (compId.mousePressed && !mouseButtonOneDownOverComp) {
								compId.mousePressed = false;
								if (compId.mouseReleasedHandler != null) {
									ClientScript2Event event = new ClientScript2Event();
									event.aBoolean3980 = true;
									event.component = compId;
									event.mouseX = offX + mouseListener.getMouseX(47) - compX;
									event.mouseY = offY + mouseListener.getMouseY((byte) 72) - compY;
									event.param = compId.mouseReleasedHandler;
									Class98_Sub46_Sub10.aClass148_6018.addLast(event, -20911);
								}
							}
							if (mouseButtonOneDownOverComp && compId.mouseDragPassHandler != null) {
								ClientScript2Event event = new ClientScript2Event();
								event.aBoolean3980 = true;
								event.component = compId;
								event.mouseX = offX + mouseListener.getMouseX(51) - compX;
								event.mouseY = offY + mouseListener.getMouseY((byte) 85) - compY;
								event.param = compId.mouseDragPassHandler;
								Class151_Sub3.aClass148_4977.addLast(event, -20911);
							}
							if (!compId.mouseOver && mouseOver) {
								compId.mouseOver = true;
								if (compId.mouseEnterHandler != null) {
									ClientScript2Event event = new ClientScript2Event();
									event.aBoolean3980 = true;
									event.component = compId;
									event.mouseX = offX + mouseListener.getMouseX(10) - compX;
									event.mouseY = offY + mouseListener.getMouseY((byte) 124) - compY;
									event.param = compId.mouseEnterHandler;
									Class151_Sub3.aClass148_4977.addLast(event, -20911);
								}
							}
							if (compId.mouseOver && mouseOver && compId.mouseMotionHandler != null) {
								ClientScript2Event event = new ClientScript2Event();
								event.aBoolean3980 = true;
								event.component = compId;
								event.mouseX = offX + mouseListener.getMouseX(45) - compX;
								event.mouseY = offY + mouseListener.getMouseY((byte) 99) - compY;
								event.param = compId.mouseMotionHandler;
								Class151_Sub3.aClass148_4977.addLast(event, -20911);
							}
							if (compId.mouseOver && !mouseOver) {
								compId.mouseOver = false;
								if (compId.mouseExitHandler != null) {
									ClientScript2Event event = new ClientScript2Event();
									event.aBoolean3980 = true;
									event.component = compId;
									event.mouseX = offX + mouseListener.getMouseX(116) - compX;
									event.mouseY = offY + mouseListener.getMouseY((byte) 77) - compY;
									event.param = compId.mouseExitHandler;
									Class98_Sub46_Sub10.aClass148_6018.addLast(event, -20911);
								}
							}
							if (compId.updateHandler != null) {
								ClientScript2Event event = new ClientScript2Event();
								event.component = compId;
								event.param = compId.updateHandler;
								Class181.aClass148_1430.addLast(event, -20911);
							}
							if (compId.anObjectArray2212 != null && Class246_Sub4_Sub2.anInt6181 > compId.anInt2223) {
								if (compId.anIntArray2297 == null || Class246_Sub4_Sub2.anInt6181 - compId.anInt2223 > 32) {
									ClientScript2Event event = new ClientScript2Event();
									event.component = compId;
									event.param = compId.anObjectArray2212;
									Class151_Sub3.aClass148_4977.addLast(event, -20911);
								} else {
									while_39_: for (int i_54_ = compId.anInt2223; i_54_ < Class246_Sub4_Sub2.anInt6181; i_54_++) {
										int i_55_ = ItemDeque.anIntArray4257[i_54_ & 0x1f];
										for (int i_56_ = 0; i_56_ < compId.anIntArray2297.length; i_56_++) {
											if (compId.anIntArray2297[i_56_] == i_55_) {
												ClientScript2Event event = new ClientScript2Event();
												event.component = compId;
												event.param = compId.anObjectArray2212;
												Class151_Sub3.aClass148_4977.addLast(event, -20911);
												break while_39_;
											}
										}
									}
								}
								compId.anInt2223 = Class246_Sub4_Sub2.anInt6181;
							}
							if (compId.anObjectArray2320 != null && Class96.anInt803 > compId.anInt2282) {
								if (compId.anIntArray2342 == null || Class96.anInt803 - compId.anInt2282 > 32) {
									ClientScript2Event event = new ClientScript2Event();
									event.component = compId;
									event.param = compId.anObjectArray2320;
									Class151_Sub3.aClass148_4977.addLast(event, -20911);
								} else {
									while_40_: for (int i_57_ = compId.anInt2282; i_57_ < Class96.anInt803; i_57_++) {
										int i_58_ = LinkedList.anIntArray1196[i_57_ & 0x1f];
										for (int i_59_ = 0; i_59_ < compId.anIntArray2342.length; i_59_++) {
											if (compId.anIntArray2342[i_59_] == i_58_) {
												ClientScript2Event event = new ClientScript2Event();
												event.component = compId;
												event.param = compId.anObjectArray2320;
												Class151_Sub3.aClass148_4977.addLast(event, -20911);
												break while_40_;
											}
										}
									}
								}
								compId.anInt2282 = Class96.anInt803;
							}
							if (compId.anObjectArray2269 != null && VarClientStringsDefinitionParser.anInt1844 > compId.anInt2328) {
								if (compId.anIntArray2284 == null || VarClientStringsDefinitionParser.anInt1844 - compId.anInt2328 > 32) {
									ClientScript2Event class98_sub21 = new ClientScript2Event();
									class98_sub21.component = compId;
									class98_sub21.param = compId.anObjectArray2269;
									Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
								} else {
									while_41_: for (int i_60_ = compId.anInt2328; i_60_ < VarClientStringsDefinitionParser.anInt1844; i_60_++) {
										int i_61_ = OpenGlMatrix.anIntArray4682[i_60_ & 0x1f];
										for (int i_62_ = 0; i_62_ < compId.anIntArray2284.length; i_62_++) {
											if (compId.anIntArray2284[i_62_] == i_61_) {
												ClientScript2Event class98_sub21 = new ClientScript2Event();
												class98_sub21.component = compId;
												class98_sub21.param = compId.anObjectArray2269;
												Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
												break while_41_;
											}
										}
									}
								}
								compId.anInt2328 = VarClientStringsDefinitionParser.anInt1844;
							}
							if (compId.inventoryUpdateHandler != null && SystemInformation.anInt1172 > compId.anInt2323) {
								if (compId.anIntArray2249 == null || SystemInformation.anInt1172 - compId.anInt2323 > 32) {
									ClientScript2Event class98_sub21 = new ClientScript2Event();
									class98_sub21.component = compId;
									class98_sub21.param = compId.inventoryUpdateHandler;
									Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
								} else {
									while_42_: for (int i_63_ = compId.anInt2323; i_63_ < SystemInformation.anInt1172; i_63_++) {
										int i_64_ = Class78.anIntArray597[i_63_ & 0x1f];
										for (int i_65_ = 0; i_65_ < compId.anIntArray2249.length; i_65_++) {
											if (compId.anIntArray2249[i_65_] == i_64_) {
												ClientScript2Event class98_sub21 = new ClientScript2Event();
												class98_sub21.component = compId;
												class98_sub21.param = compId.inventoryUpdateHandler;
												Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
												break while_42_;
											}
										}
									}
								}
								compId.anInt2323 = SystemInformation.anInt1172;
							}
							if (compId.anObjectArray2278 != null && DecoratedProgressBarLSEConfig.anInt5477 > compId.anInt2276) {
								if (compId.anIntArray2271 == null || DecoratedProgressBarLSEConfig.anInt5477 - compId.anInt2276 > 32) {
									ClientScript2Event class98_sub21 = new ClientScript2Event();
									class98_sub21.component = compId;
									class98_sub21.param = compId.anObjectArray2278;
									Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
								} else {
									while_43_: for (int i_66_ = compId.anInt2276; i_66_ < DecoratedProgressBarLSEConfig.anInt5477; i_66_++) {
										int i_67_ = Class98_Sub16.anIntArray3928[i_66_ & 0x1f];
										for (int i_68_ = 0; i_68_ < compId.anIntArray2271.length; i_68_++) {
											if (compId.anIntArray2271[i_68_] == i_67_) {
												ClientScript2Event class98_sub21 = new ClientScript2Event();
												class98_sub21.component = compId;
												class98_sub21.param = compId.anObjectArray2278;
												Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
												break while_43_;
											}
										}
									}
								}
								compId.anInt2276 = DecoratedProgressBarLSEConfig.anInt5477;
							}
							if (ClientInventory.timestampChatboxUpdate > compId.timestampLastProcess && compId.chatboxUpdateHandler != null) {
								ClientScript2Event class98_sub21 = new ClientScript2Event();
								class98_sub21.component = compId;
								class98_sub21.param = compId.chatboxUpdateHandler;
								Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
							}
							if (NewsFetcher.anInt3099 > compId.timestampLastProcess && compId.anObjectArray2215 != null) {
								ClientScript2Event class98_sub21 = new ClientScript2Event();
								class98_sub21.component = compId;
								class98_sub21.param = compId.anObjectArray2215;
								Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
							}
							if (VolumePreferenceField.anInt3705 > compId.timestampLastProcess && compId.anObjectArray2292 != null) {
								ClientScript2Event class98_sub21 = new ClientScript2Event();
								class98_sub21.component = compId;
								class98_sub21.param = compId.anObjectArray2292;
								Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
							}
							if (GroundDecorationPreferenceField.anInt3668 > compId.timestampLastProcess && compId.anObjectArray2294 != null) {
								ClientScript2Event class98_sub21 = new ClientScript2Event();
								class98_sub21.component = compId;
								class98_sub21.param = compId.anObjectArray2294;
								Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
							}
							if (Class98_Sub36.anInt4161 > compId.timestampLastProcess && compId.anObjectArray2340 != null) {
								ClientScript2Event class98_sub21 = new ClientScript2Event();
								class98_sub21.component = compId;
								class98_sub21.param = compId.anObjectArray2340;
								Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
							}
							compId.timestampLastProcess = WorldMapInfoDefinition.logicTimer;
							if (compId.anObjectArray2274 != null) {
								for (int index = 0; index < keyPressCount; index++) {
									ClientScript2Event class98_sub21 = new ClientScript2Event();
									class98_sub21.component = compId;
									class98_sub21.anInt3977 = keyPressEvents[index].getKeyCode(true);
									class98_sub21.anInt3976 = keyPressEvents[index].getKeyChar(13313);
									class98_sub21.param = compId.anObjectArray2274;
									Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
								}
							}
							if (Class187.aBoolean1451 && compId.anObjectArray2220 != null) {
								ClientScript2Event class98_sub21 = new ClientScript2Event();
								class98_sub21.component = compId;
								class98_sub21.param = compId.anObjectArray2220;
								Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
							}
						}
						if (compId.type == 5 && compId.objId != -1) {
							compId.method3467(0, GrandExchangeOffer.sunDefinitionList, SimpleProgressBarLoadingScreenElement.skyboxDefinitionList).method3828(compId.renderHeight, 6, graphicsToolkit);
						}
						Class98_Sub10_Sub21.method1066(124, compId);
						if (compId.type == 0) {
							method104(components, compId.idDword, clipL, clipT, clipR, clipB, compX - compId.scrollX, compY - compId.scrollY, i_18_, i_19_, i_20_, i_21_);
							if (compId.dynamicComponents != null) {
								method104(compId.dynamicComponents, compId.idDword, clipL, clipT, clipR, clipB, compX - compId.scrollX, compY - compId.scrollY, i_18_, i_19_, i_20_, i_21_);
							}
							RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(compId.idDword, -1);
							if (class98_sub18 != null) {
								if (game == GameDefinition.RUNESCAPE && class98_sub18.anInt3947 == 0 && !Player.menuOpen && mouseOver && !Class15.qaOpTest) {
									Class46.method435((byte) 117);
								}
								Class62.method544(i_20_, clipT, class98_sub18.attachedInterfaceId, clipR, 0, clipB, compX, i_18_, i_19_, i_21_, compY, clipL);
							}
						}
					}
				}
			}
		}
	}

	public static final void method105(int i) {
		int playerCount = Class2.playerCount;
		int[] playerIndices = Class319.playerIndices;
		for (int count = 0; count < playerCount + Class150.npcCount; count++) {
			Mob mob;
			if (count < playerCount) {
				mob = Class151_Sub9.players[playerIndices[count]];
			} else {
				mob = ((NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[count - playerCount], -1)).npc;
			}
			if (mob.plane == i && mob.anInt6372 >= 0) {
				int size = mob.getSize(0);
				if ((size & 0x1) == 0) {
					if ((mob.boundExtentsX & 0x1ff) != 0 || (mob.boundExtentsZ & 0x1ff) != 0) {
						continue;
					}
				} else if ((mob.boundExtentsX & 0x1ff) != 256 || (mob.boundExtentsZ & 0x1ff) != 256) {
					continue;
				}
				if (size == 1) {
					int i_73_ = mob.boundExtentsX >> 9;
					int i_74_ = mob.boundExtentsZ >> 9;
					if (mob.anInt6372 > Class372.anIntArrayArray3149[i_73_][i_74_]) {
						Class372.anIntArrayArray3149[i_73_][i_74_] = mob.anInt6372;
						WaterDetailPreferenceField.anIntArrayArray3719[i_73_][i_74_] = 1;
					} else if (mob.anInt6372 == Class372.anIntArrayArray3149[i_73_][i_74_]) {
						WaterDetailPreferenceField.anIntArrayArray3719[i_73_][i_74_]++;
					}
				} else {
					int i_75_ = (size - 1) * 256 + 60;
					int i_76_ = mob.boundExtentsX - i_75_ >> 9;
					int i_77_ = mob.boundExtentsZ - i_75_ >> 9;
					int i_78_ = mob.boundExtentsX + i_75_ >> 9;
					int i_79_ = mob.boundExtentsZ + i_75_ >> 9;
					for (int i_80_ = i_76_; i_80_ <= i_78_; i_80_++) {
						for (int i_81_ = i_77_; i_81_ <= i_79_; i_81_++) {
							if (mob.anInt6372 > Class372.anIntArrayArray3149[i_80_][i_81_]) {
								Class372.anIntArrayArray3149[i_80_][i_81_] = mob.anInt6372;
								WaterDetailPreferenceField.anIntArrayArray3719[i_80_][i_81_] = 1;
							} else if (mob.anInt6372 == Class372.anIntArrayArray3149[i_80_][i_81_]) {
								WaterDetailPreferenceField.anIntArrayArray3719[i_80_][i_81_]++;
							}
						}
					}
				}
			}
		}
	}

	public static final void method110(int i) {
		int i_93_ = Class2.playerCount;
		int[] is = Class319.playerIndices;
		int i_94_ = Class237_Sub1.noNpcs ? i_93_ : i_93_ + Class150.npcCount;
		for (int i_95_ = 0; i_95_ < i_94_; i_95_++) {
			Mob mob;
			if (i_95_ < i_93_) {
				mob = Class151_Sub9.players[is[i_95_]];
			} else {
				mob = ((NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[i_95_ - i_93_], -1)).npc;
			}
			if (mob.plane == i) {
				mob.anInt6358 = 0;
				if (mob.anInt6372 < 0) {
					mob.aBoolean6371 = false;
				} else {
					int i_96_ = mob.getSize(0);
					if ((i_96_ & 0x1) == 0) {
						if ((mob.boundExtentsX & 0x1ff) != 0 || (mob.boundExtentsZ & 0x1ff) != 0) {
							mob.aBoolean6371 = false;
							continue;
						}
					} else if ((mob.boundExtentsX & 0x1ff) != 256 || (mob.boundExtentsZ & 0x1ff) != 256) {
						mob.aBoolean6371 = false;
						continue;
					}
					if (i_96_ == 1) {
						int i_97_ = mob.boundExtentsX >> 9;
						int i_98_ = mob.boundExtentsZ >> 9;
						if (mob.anInt6372 != Class372.anIntArrayArray3149[i_97_][i_98_]) {
							mob.aBoolean6371 = true;
							continue;
						}
						if (WaterDetailPreferenceField.anIntArrayArray3719[i_97_][i_98_] > 1) {
							WaterDetailPreferenceField.anIntArrayArray3719[i_97_][i_98_]--;
							mob.aBoolean6371 = true;
							continue;
						}
					} else {
						int i_99_ = (i_96_ - 1) * 256 + 252;
						int i_100_ = mob.boundExtentsX - i_99_ >> 9;
						int i_101_ = mob.boundExtentsZ - i_99_ >> 9;
						int i_102_ = mob.boundExtentsX + i_99_ >> 9;
						int i_103_ = mob.boundExtentsZ + i_99_ >> 9;
						if (!AnimationDefinition.method935(i_103_, i_102_, i_100_, 50, i_101_, mob.anInt6372)) {
							for (int i_104_ = i_100_; i_104_ <= i_102_; i_104_++) {
								for (int i_105_ = i_101_; i_105_ <= i_103_; i_105_++) {
									if (mob.anInt6372 == Class372.anIntArrayArray3149[i_104_][i_105_]) {
										WaterDetailPreferenceField.anIntArrayArray3719[i_104_][i_105_]--;
									}
								}
							}
							mob.aBoolean6371 = true;
							continue;
						}
					}
					mob.aBoolean6371 = false;
					mob.anInt5089 = StrongReferenceMCNode.getHeight(mob.plane, mob.boundExtentsZ, mob.boundExtentsX, 24111);
					LoginOpcode.method2826(mob, true);
				}
			}
		}
	}

	public static final boolean method111(RtInterface interf) {
		if (Class15.qaOpTest) {
			if (method116(interf).anInt4284 != 0) {
				return false;
			}
			if (interf.type == 0) {
				return false;
			}
		}
		return interf.aBoolean2295;
	}

	public static final void method113() {
		ReflectionRequest.anInt3955 = 0;
		for (int i = 0; i < Class150.npcCount; i++) {
			NPC npc = ((NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[i], -1)).npc;
			if (npc.aBoolean6371 && npc.getArmyIcon(28213) != -1) {
				int i_120_ = (npc.getSize(0) - 1) * 256 + 252;
				int i_121_ = npc.boundExtentsX - i_120_ >> 9;
				int i_122_ = npc.boundExtentsZ - i_120_ >> 9;
				Mob mob = CursorDefinition.method2874(npc.plane, i_121_, i_122_, 64);
				if (mob != null) {
					int i_123_ = mob.index;
					if (mob instanceof NPC) {
						i_123_ += 2048;
					}
					if (mob.anInt6358 == 0 && mob.getArmyIcon(28213) != -1) {
						Class151_Sub1.anIntArray4969[ReflectionRequest.anInt3955] = i_123_;
						Class119_Sub2.anIntArray4727[ReflectionRequest.anInt3955] = i_123_;
						ReflectionRequest.anInt3955++;
						mob.anInt6358++;
					}
					Class151_Sub1.anIntArray4969[ReflectionRequest.anInt3955] = i_123_;
					Class119_Sub2.anIntArray4727[ReflectionRequest.anInt3955] = npc.index + 2048;
					ReflectionRequest.anInt3955++;
					mob.anInt6358++;
				}
			}
		}
		Class221.method2823((byte) -125, Class151_Sub1.anIntArray4969, Class119_Sub2.anIntArray4727, 0, ReflectionRequest.anInt3955 - 1);
	}

	public static final void method114() {
		int playerCount = Class2.playerCount;
		int[] playerIndices = Class319.playerIndices;
		int idleAnimations = preferences.idleAnimations.getValue((byte) 127);
		boolean bool = idleAnimations == 1 && playerCount > 200 || idleAnimations == 0 && playerCount > 50;
		for (int playerIndex = 0; playerIndex < playerCount; playerIndex++) {
			Player player = Class151_Sub9.players[playerIndices[playerIndex]];
			if (!player.hasAppearance((byte) 106)) {
				player.anInt6372 = -1;
			} else if (player.invisible) {
				player.anInt6372 = -1;
			} else {
				player.method3022(-8675);
				if (player.aShort6158 < 0 || player.aShort6157 < 0 || player.aShort6160 >= Class165.mapWidth || player.aShort6159 >= Class98_Sub10_Sub7.mapLength) {
					player.anInt6372 = -1;
				} else {
					player.aBoolean6520 = player.aBoolean6359 && bool;
					if (player == Class87.localPlayer) {
						player.anInt6372 = 2147483647;
					} else {
						int i_126_ = 0;
						if (!player.aBoolean6371) {
							i_126_++;
						}
						if (player.anInt6418 > Queue.timer) {
							i_126_ += 2;
						}
						i_126_ += 5 - player.getSize(0) << 2;
						if (player.clanmate) {
							i_126_ += 512;
						} else {
							if (Class103.anInt896 == 0) {
								i_126_ += 32;
							} else {
								i_126_ += 128;
							}
							i_126_ += 256;
						}
						player.anInt6372 = i_126_ + 1;
					}
				}
			}
		}
		for (int i_127_ = 0; i_127_ < Class150.npcCount; i_127_++) {
			NPC npc = ((NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[i_127_], -1)).npc;
			if (!npc.method3052((byte) 106) || !npc.definition.method2296(StartupStage.varValues, (byte) -118)) {
				npc.anInt6372 = -1;
			} else {
				npc.method3022(-8675);
				if (npc.aShort6158 < 0 || npc.aShort6157 < 0 || npc.aShort6160 >= Class165.mapWidth || npc.aShort6159 >= Class98_Sub10_Sub7.mapLength) {
					npc.anInt6372 = -1;
				} else {
					int i_128_ = 0;
					if (!npc.aBoolean6371) {
						i_128_++;
					}
					if (npc.anInt6418 > Queue.timer) {
						i_128_ += 2;
					}
					i_128_ += 5 - npc.getSize(0) << 2;
					if (Class103.anInt896 == 0) {
						if (npc.definition.aBoolean1153) {
							i_128_ += 64;
						} else {
							i_128_ += 128;
						}
					} else if (Class103.anInt896 == 1) {
						if (npc.definition.aBoolean1153) {
							i_128_ += 32;
						} else {
							i_128_ += 64;
						}
					}
					if (npc.definition.aBoolean1106) {
						i_128_ += 1024;
					} else if (!npc.definition.aBoolean1149) {
						i_128_ += 256;
					}
					npc.anInt6372 = i_128_ + 1;
				}
			}
		}
		for (Class36 class36 : OpenGlArrayBufferPointer.aClass36Array903) {
			if (class36 != null) {
				if (class36.anInt346 == 1) {
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(class36.anInt345, -1);
					if (class98_sub39 != null) {
						NPC npc = class98_sub39.npc;
						if (npc.anInt6372 >= 0) {
							npc.anInt6372 += 2048;
						}
					}
				} else if (class36.anInt346 == 10) {
					Player player = Class151_Sub9.players[class36.anInt345];
					if (player != null && player != Class87.localPlayer && player.anInt6372 >= 0) {
						player.anInt6372 += 2048;
					}
				}
			}
		}
	}

	public static final void method115() {
		int i = Class2.playerCount;
		int[] is = Class319.playerIndices;
		int i_131_ = Class237_Sub1.noNpcs ? i : i + Class150.npcCount;
		for (int i_132_ = 0; i_132_ < i_131_; i_132_++) {
			Mob mob;
			if (i_132_ < i) {
				mob = Class151_Sub9.players[is[i_132_]];
			} else {
				mob = ((NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[i_132_ - i], -1)).npc;
			}
			if (mob.anInt6372 >= 0) {
				int i_133_ = mob.getSize(0);
				if ((i_133_ & 0x1) == 0) {
					if ((mob.boundExtentsX & 0x1ff) == 0 && (mob.boundExtentsZ & 0x1ff) == 0) {
						continue;
					}
				} else if ((mob.boundExtentsX & 0x1ff) == 256 && (mob.boundExtentsZ & 0x1ff) == 256) {
					continue;
				}
				mob.anInt5089 = StrongReferenceMCNode.getHeight(mob.plane, mob.boundExtentsZ, mob.boundExtentsX, 24111);
				LoginOpcode.method2826(mob, true);
			}
		}
	}

	public static final Class98_Sub49 method116(RtInterface class293) {
		Class98_Sub49 class98_sub49 = (Class98_Sub49) Class168.aClass377_1287.get(((long) class293.idDword << 32) + class293.componentIndex, -1);
		if (class98_sub49 != null) {
			return class98_sub49;
		}
		return class293.aClass98_Sub49_2348;
	}

	public static final void printUsage(int i, String string) {
		System.exit(i);
	}

	public static final int processStartup(int i) {
		if ((preferences.safeMode.getValue((byte) 123) ^ 0xffffffff) == -1) {
			for (int eventPtr = 0; (keyPressCount ^ 0xffffffff) < (eventPtr ^ 0xffffffff); eventPtr++) {
				if (keyPressEvents[eventPtr].getKeyChar(13313) == 's' || keyPressEvents[eventPtr].getKeyChar(13313) == 'S') {
					preferences.setPreference((byte) -13, 1, preferences.safeMode);
					safeMode = true;
					break;
				}
			}
		}
		if (LOAD_JS5_INDICES == StartupStage.WAIT_FOR_MEMORY) {
			Runtime runtime = Runtime.getRuntime();
			int usedKbs = (int) ((runtime.totalMemory() - runtime.freeMemory()) / 1024L);
			long currentTime = TimeTools.getCurrentTime(-47);
			if ((lastRamCheck ^ 0xffffffffffffffffL) == -1L) {
				lastRamCheck = currentTime;
			}
			if (usedKbs > 16384 && (currentTime + -lastRamCheck ^ 0xffffffffffffffffL) > -5001L) {
				if (currentTime - lastGc > 1000L) {
					System.gc();
					lastGc = currentTime;
				}
				return 0;
			}
		}
		if (LOAD_JS5_INDICES == StartupStage.INITIALIZE_TOOLKIT) {
			if (js5Manager == null) {
				js5Manager = new Js5Manager(js5Client, asyncCache, JS5_PRIVATE_KEY, JS5_MODULUS);
			}
			if (js5Manager.isMasterIndexReady() == false) {
				return 0;
			}
			initializeToolkit((byte) 69, null, 0, true);
			fontCacheSelector = !tryDecompressTitleImage(false);
			fontMetricsJs5 = createJs5(-79, false, 1, !fontCacheSelector ? 32 : 34);
			loadingScreenJs5 = createJs5(-50, false, 1, 33);
			fontJs5 = createJs5(-67, false, 1, 13);

		}
		if (LOAD_JS5_INDICES == StartupStage.LOAD_LOADING_SCREEN_DEFINITIONS) {
			boolean haveGameTips = loadingScreenJs5.isFullyCached((byte) 108);
			int progress = Class100.js5Archives[33].getInitializationProgress(-90);

			progress = progress + Class100.js5Archives[fontCacheSelector ? 34 : 32].getInitializationProgress(-38);
			progress += Class100.js5Archives[13].getInitializationProgress(-26);
			progress = progress + (haveGameTips ? 100 : loadingScreenJs5.getProgress((byte) -49));
			if (progress != 400) {
				return progress / 4;// TODO: From this line on relocate fields
			}

			// etc...
			Class284.titleFontsIndexCrc = fontMetricsJs5.getIndexCrc32((byte) -50);
			OpenGLHeightMapToNormalMapConverter.gameTipsIndexCrc = loadingScreenJs5.getIndexCrc32((byte) -48);
			Class84.loadFontIds(fontMetricsJs5, 78);
			int sequenceId = preferences.loadingScreenSequence.getValue((byte) 121);
			Class3.loadingScreenSequence = new LoadingScreenSequence(game, gameLanguage, loadingScreenJs5);
			int[] loadingScreenFileIds = Class3.loadingScreenSequence.getSequence(sequenceId, (byte) 94);
			if (loadingScreenFileIds.length == 0) {
				loadingScreenFileIds = Class3.loadingScreenSequence.getSequence(0, (byte) 90);
			}
			LoadingScreenElementFactory factory = new LoadingScreenElementFactory(fontMetricsJs5, fontJs5);
			if (loadingScreenFileIds.length > 0) {
				Class39_Sub1.loadingScreens = new LoadingScreen[loadingScreenFileIds.length];
				for (int loadingScreenPtr = 0; (loadingScreenPtr ^ 0xffffffff) > (Class39_Sub1.loadingScreens.length ^ 0xffffffff); loadingScreenPtr++) {
					Class39_Sub1.loadingScreens[loadingScreenPtr] = new ConfigurableLoadingScreen(Class3.loadingScreenSequence.unpackScreen(-123, loadingScreenFileIds[loadingScreenPtr]), factory);
				}
			}

		}
		if (LOAD_JS5_INDICES == StartupStage.REQUEST_LOADING_SCREEN_FONTS) {
			RtKeyListener.initialize(3, fontJs5, HitmarksDefinitionParser.getFontIds(), fontMetricsJs5);
		}
		if (LOAD_JS5_INDICES == StartupStage.LOAD_LOADING_SCREEN_FONTS) {
			int progress = ClientStream.getProgress(-13);
			int maxProgress = Class35.getMaxProgress(2);
			if (maxProgress > progress) {
				return 100 * progress / maxProgress;
			}
		}
		if (StartupStage.LOAD_LOADING_SCREEN_MEDIA == LOAD_JS5_INDICES) {
			if (Class39_Sub1.loadingScreens != null && (Class39_Sub1.loadingScreens.length ^ 0xffffffff) < -1) {
				if ((Class39_Sub1.loadingScreens[0].getCacheProgress(-794) ^ 0xffffffff) > -101) {
					return 0;
				}
				if ((Class39_Sub1.loadingScreens.length ^ 0xffffffff) < -2 && Class3.loadingScreenSequence.isLoaded(1) && (Class39_Sub1.loadingScreens[1].getCacheProgress(-794) ^ 0xffffffff) > -101) {
					return 0;
				}
			}
			Class98_Sub10_Sub19.loadFonts((byte) -128, graphicsToolkit);
			SpriteProgressBarLSEConfig.loadFonts(graphicsToolkit, (byte) -116);
			HashTableIterator.setClientState(1, Launcher.DISABLE_LOADING_SCREEN_IMAGES == true);
		}
		if (LOAD_JS5_INDICES == StartupStage.CREATE_CLIP_MAPS) {
			for (int hl = 0; hl < 4; hl++) {
				VarPlayerDefinition.clipMaps[hl] = ClipMap.create(2742, Class98_Sub10_Sub7.mapLength, Class165.mapWidth);
			}
		}
		if (LOAD_JS5_INDICES == StartupStage.CREATE_JS_ARCHIVES) {
			spriteJs5 = createJs5(-119, false, 1, 8);
			Class94.animationFramesJs5 = createJs5(-114, false, 1, 0);
			Class323.animationFrameBasesJs5 = createJs5(-115, false, 1, 1);
			miscellaneousJs5 = createJs5(-116, false, 1, 2);
			TexturesPreferenceField.interfacesJs5 = createJs5(-126, false, 1, 3);
			Class76_Sub2.soundEffectsJs5 = createJs5(-92, false, 1, 4);
			Class234.mapsJs5 = createJs5(-54, true, 1, 5);
			Class98_Sub10_Sub1.musicJs5 = createJs5(-115, true, 1, 6);
			Class76_Sub9.modelsJs5 = createJs5(-92, false, 1, 7);
			RSByteBuffer.texturesJs5 = createJs5(-121, false, 1, 9);
			NodeShort.huffmanJs5 = createJs5(-93, false, 1, 10);
			HashTableIterator.music2Js5 = createJs5(-100, false, 1, 11);
			NewsLSEConfig.clientScriptJs5 = createJs5(-120, false, 1, 12);
			BuildLocation.midiInstrumentsJs5 = createJs5(-91, false, 1, 14);
			Class119_Sub2.sound3Js5 = createJs5(-92, false, 1, 15);
			Class375.objectsJs5 = createJs5(-87, false, 1, 16);
			Class98_Sub10_Sub24.enumsJs5 = createJs5(-49, false, 1, 17);
			Class234.npcJs5 = createJs5(-90, false, 1, 18);
			Class208.itemsJs5 = createJs5(-62, false, 1, 19);
			Char.animationJs5 = createJs5(-111, false, 1, 20);
			PlayerUpdateMask.graphicJs5 = createJs5(-123, false, 1, 21);
			Class98_Sub46_Sub19.bConfigJs5 = createJs5(-124, false, 1, 22);
			RenderAnimDefinitionParser.worldMapJs5 = createJs5(-113, true, 1, 23);
			Class81.quickChatMessagesJs5 = createJs5(-81, false, 1, 24);
			OpenGlElementBufferPointer.quickChatMenuJs5 = createJs5(-82, false, 1, 25);
			TexturesPreferenceField.materialsJs5 = createJs5(-111, true, 1, 26);
			Class245.particlesJs5 = createJs5(-96, false, 1, 27);
			AsyncCache.defaultsJs5 = createJs5(-80, true, 1, 28);
			NativeMatrix.billboardsJs5 = createJs5(-119, false, 1, 29);
			Class245.nativesJs5 = createJs5(-102, true, 1, 30);
			shaderJs5 = createJs5(-108, true, 1, 31);
			ParticleManager.vorbisJs5 = createJs5(-121, true, 2, 36);
		}
		if (StartupStage.aClass75_567 == LOAD_JS5_INDICES) {
			int js5Progress = 0;
			for (int index = 0; index < 37; index++) {
				if (Class100.js5Archives[index] != null) {
					js5Progress += Class100.js5Archives[index].getInitializationProgress(-51) * Class98_Sub10_Sub10.archiveProgressWeight[index] / 100;
				}
			}
			if ((js5Progress ^ 0xffffffff) != -101) {
				if (Class53_Sub1.js5InitializationProgress < 0) {
					Class53_Sub1.js5InitializationProgress = js5Progress;
				}
				return 100 * (js5Progress + -Class53_Sub1.js5InitializationProgress) / (100 - Class53_Sub1.js5InitializationProgress);
			}
			Class1.loadSpriteIds((byte) -107, spriteJs5);
			RtKeyListener.initialize(3, fontJs5, HitmarksDefinitionParser.getFontIds(), spriteJs5);
		}
		if (LOAD_JS5_INDICES == StartupStage.INITIALIZE_MUSIC) {
			if ((Class94.anInt795 ^ 0xffffffff) == 0) {
				Class94.anInt795 = Class98_Sub10_Sub1.musicJs5.getGroupId((byte) -109, "scape main");
			}
			AwtMouseEvent.method1157(-120);
			HashTableIterator.setClientState(2, Launcher.DISABLE_LOADING_SCREEN_IMAGES == true);
		}
		if (LOAD_JS5_INDICES == StartupStage.INITIALIZE_NATIVE_MANAGER) {
			ParamDefinitionParser.initialize(Class245.nativesJs5, signLink, -1);
		}
		if (StartupStage.PREFETCH == LOAD_JS5_INDICES) {
			int prefetchProgress = PrefetchManager.process((byte) -128);
			if ((prefetchProgress ^ 0xffffffff) > -101) {
				return prefetchProgress;
			}
			Class276.method3284(AsyncCache.defaultsJs5.getFile(1, 14), -65536);
			Class155.method2495(AsyncCache.defaultsJs5.getFile(3, 82), (byte) -25);
		}
		if (StartupStage.CREATE_CONFIG_LISTS == LOAD_JS5_INDICES) {
			if ((LightningDetailPreferenceField.anInt3666 ^ 0xffffffff) != 0 && !Class76_Sub9.modelsJs5.isFileCached(0, LightningDetailPreferenceField.anInt3666, -6329)) {
				return 99;
			}
			textureList = new ProceduralTextureSource(TexturesPreferenceField.materialsJs5, RSByteBuffer.texturesJs5, spriteJs5);
			Class98_Sub43_Sub1.paramDefinitionList = new ParamDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class370.renderAnimDefinitionList = new RenderAnimDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class18.cursorDefinitionList = new CursorDefinitionParser(game, gameLanguage, miscellaneousJs5, spriteJs5);
			Class98_Sub10_Sub16.enumsDefinitionList = new EnumDefinitionParser(game, gameLanguage, Class98_Sub10_Sub24.enumsJs5);
			FloorOverlayDefinition.floorOverlayDefinitionList = new FloorOverlayDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class82.floorUnderDefinitionList = new FloorUnderlayDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class246_Sub3_Sub1.hitmarksDefinitionList = new HitmarksDefinitionParser(game, gameLanguage, miscellaneousJs5, spriteJs5);
			ParamDefinition.identikitDefinitionList = new IdentikitDefinitionParser(game, gameLanguage, miscellaneousJs5, Class76_Sub9.modelsJs5);
			Class98_Sub46_Sub14.inventoriesDefinitionList = new InventoriesDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class21_Sub1.lightIntensityDefinitionList = new LightIntensityDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class130.gameObjectDefinitionList = new GameObjectDefinitionParser(game, gameLanguage, true, Class375.objectsJs5, Class76_Sub9.modelsJs5);
			Class216.worldMapInfoDefinitionList = new WorldMapInfoDefinitionParser(game, gameLanguage, miscellaneousJs5, spriteJs5);
			Class98_Sub10_Sub23.mapScenesDefinitionList = new MapScenesDefinitionParser(game, gameLanguage, miscellaneousJs5, spriteJs5);
			Class4.npcDefinitionList = new NPCDefinitionParser(game, gameLanguage, true, Class234.npcJs5, Class76_Sub9.modelsJs5);
			Class98_Sub46_Sub19.itemDefinitionList = new ItemDefinitionParser(game, gameLanguage, true, Class98_Sub43_Sub1.paramDefinitionList, Class208.itemsJs5, Class76_Sub9.modelsJs5);
			Class303.questDefinitionList = new QuestDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class151_Sub7.animationDefinitionList = new AnimationDefinitionParser(game, gameLanguage, Char.animationJs5, Class94.animationFramesJs5, Class323.animationFrameBasesJs5);
			SimpleProgressBarLoadingScreenElement.skyboxDefinitionList = new SkyboxDefinitionParser(game, gameLanguage, miscellaneousJs5);
			GrandExchangeOffer.sunDefinitionList = new SunDefinitionParser(game, gameLanguage, miscellaneousJs5);
			BuildLocation.gfxDefinitionList = new GraphicsDefinitionParser(game, gameLanguage, PlayerUpdateMask.graphicJs5, Class76_Sub9.modelsJs5);
			Class62.structsDefinitionList = new StructsDefinitionParser(game, gameLanguage, miscellaneousJs5);
			OpenGlMatrix.varClientStringsDefinitionList = new VarClientStringsDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class345.varClientDefinitionList = new VarClientDefinitionParser(game, gameLanguage, miscellaneousJs5);
			DataFs.bConfigDefinitionList = new BConfigDefinitionParser(game, gameLanguage, Class98_Sub46_Sub19.bConfigJs5);
			SpriteLoadingScreenElement.varPlayerDefinitionList = new VarPlayerDefinitionParser(game, gameLanguage, miscellaneousJs5);
			Class94.method916(-99, TexturesPreferenceField.interfacesJs5, fontJs5, spriteJs5, Class76_Sub9.modelsJs5);
			AsyncCache.method3181(NativeMatrix.billboardsJs5, -1);
			SceneGraphNodeList.quickChatCategoryList = new QuickChatCategoryParser(gameLanguage, Class81.quickChatMessagesJs5, OpenGlElementBufferPointer.quickChatMenuJs5);
			NewsLSEConfig.quickChatMessageList = new QuickChatMessageParser(gameLanguage, Class81.quickChatMessagesJs5, OpenGlElementBufferPointer.quickChatMenuJs5, new Class255());
			PlayerAppearence.method3625(-1439);
			Class130.gameObjectDefinitionList.method3552((preferences.aClass64_Sub3_4076.getValue((byte) 123) ^ 0xffffffff) == -1, 60);
			StartupStage.varValues = new Class140();
			Js5FileRequest.method1593((byte) 54);
			AnimatedLoadingBarLoadingScreenElement.method3970(Class245.particlesJs5, (byte) -98);
			Class250.method3168(Class76_Sub9.modelsJs5, true, textureList);
			Huffman huffman = new Huffman(NodeShort.huffmanJs5.method2739("huffman", "", -32734));
			VolumePreferenceField.method643(-256, huffman);
			try {
				jagmisc.init();
			} catch (Throwable throwable) {
				/* empty */
			}
			frameTimeBase = FrameTimeBase.create(72);
			Exception_Sub1.platformInformation = new PlatformInformation(true, signLink);
		}
		if (LOAD_JS5_INDICES == StartupStage.aClass75_573) {
			int i_11_ = Class98_Sub10_Sub33.getCachedSpriteCount(-11286, spriteJs5) - -Class98_Sub46_Sub15.getProgress(true, (byte) 90);
			int i_12_ = Class98_Sub48.method1660(84) - -Class35.getMaxProgress(2);
			if ((i_11_ ^ 0xffffffff) > (i_12_ ^ 0xffffffff)) {
				return i_11_ * 100 / i_12_;
			}
		}
		if (LOAD_JS5_INDICES == StartupStage.aClass75_574) {
			WorldMap.method3298(RenderAnimDefinitionParser.worldMapJs5, FloorOverlayDefinition.floorOverlayDefinitionList, Class82.floorUnderDefinitionList, Class130.gameObjectDefinitionList, Class216.worldMapInfoDefinitionList, Class98_Sub10_Sub23.mapScenesDefinitionList,
					StartupStage.varValues);
		}
		if (StartupStage.aClass75_575 == LOAD_JS5_INDICES) {
			Class140.boolGlobalConfigs = new boolean[Class345.varClientDefinitionList.anInt1047];
			Class151_Sub1.strGlobalConfigs = new String[OpenGlMatrix.varClientStringsDefinitionList.anInt1838];
			Class76_Sub5.intGlobalConfigs = new int[Class345.varClientDefinitionList.anInt1047];
			for (int index = 0; Class345.varClientDefinitionList.anInt1047 > index; index++) {
				if (Class345.varClientDefinitionList.method2237(index, 58).anInt718 == 0) {
					Class140.boolGlobalConfigs[index] = true;
					CpuUsagePreferenceField.anInt3700++;
				}
				Class76_Sub5.intGlobalConfigs[index] = -1;
			}
			Class78.method791((byte) 102);
			ItemDefinition.loginInterfaceId = TexturesPreferenceField.interfacesJs5.getGroupId((byte) -106, "loginscreen");
			TextLoadingScreenElement.lobbyInterfaceId = TexturesPreferenceField.interfacesJs5.getGroupId((byte) -121, "lobbyscreen");
			Class234.mapsJs5.discardNames(false, true, (byte) 95);
			Class98_Sub10_Sub1.musicJs5.discardNames(true, true, (byte) -120);
			spriteJs5.discardNames(true, true, (byte) 66);
			fontJs5.discardNames(true, true, (byte) -78);
			NodeShort.huffmanJs5.discardNames(true, true, (byte) -70);
			TexturesPreferenceField.interfacesJs5.discardNames(true, true, (byte) 125);
			MapScenesDefinitionParser.aBoolean2817 = true;
			miscellaneousJs5.discardUnpacked = 2;
			Class98_Sub10_Sub24.enumsJs5.discardUnpacked = 2;
			Class375.objectsJs5.discardUnpacked = 2;
			Class234.npcJs5.discardUnpacked = 2;
			Class208.itemsJs5.discardUnpacked = 2;
			Char.animationJs5.discardUnpacked = 2;
			PlayerUpdateMask.graphicJs5.discardUnpacked = 2;
		}
		if (StartupStage.LOAD_LOGIN_INTERFACE == LOAD_JS5_INDICES) {
			if (!Class85.loadInterface(ItemDefinition.loginInterfaceId, 102)) {
				return 0;
			}
			boolean bool = true;
			for (int index = 0; (Class159.interfaceStore[ItemDefinition.loginInterfaceId].length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				RtInterface interf = Class159.interfaceStore[ItemDefinition.loginInterfaceId][index];
				if ((interf.type ^ 0xffffffff) == -6 && (interf.defaultImage ^ 0xffffffff) != 0 && !spriteJs5.isFileCached(0, interf.defaultImage, -6329)) {
					bool = false;
				}
			}
			if (!bool) {
				return 0;
			}
		}
		if (LOAD_JS5_INDICES == StartupStage.aClass75_577) {
			LightIntensityDefinitionParser.method3269(true, 0);
		}
		if (StartupStage.aClass75_578 == LOAD_JS5_INDICES) {
			Class140.loadingScreenRenderer.shutDown(false);
			try {
				Class76_Sub9.aThread3783.join();
			} catch (InterruptedException interruptedexception) {
				return 0;
			}
			Class39_Sub1.loadingScreens = null;
			Class76_Sub9.aThread3783 = null;
			fontMetricsJs5 = null;
			Class140.loadingScreenRenderer = null;
			loadingScreenJs5 = null;
			Class3.loadingScreenSequence = null;
			Sound.clearCache((byte) 55);
			InventoriesDefinition.aBoolean6056 = preferences.safeMode.getValue((byte) 127) == 1;
			preferences.setPreference((byte) -13, 1, preferences.safeMode);
			if (InventoriesDefinition.aBoolean6056) {
				preferences.setPreference((byte) -13, 0, preferences.desiredToolkit);
			} else if (preferences.desiredToolkit.defaultValue && (Exception_Sub1.platformInformation.totalPhysicMemory ^ 0xffffffff) > -513 && Exception_Sub1.platformInformation.totalPhysicMemory != 0) {
				preferences.setPreference((byte) -13, 0, preferences.desiredToolkit);
			}
			Class310.method3618(-5964);
			if (InventoriesDefinition.aBoolean6056) {
				Class76_Sub4.method754(0, false, 1);
			} else {
				Class76_Sub4.method754(preferences.desiredToolkit.getValue((byte) 123), false, 101);
			}
			Class98_Sub16.setWindowMode(preferences.screenSize.getValue((byte) 122), -1, 3, -1, false);
			Class98_Sub10_Sub19.loadFonts((byte) -112, graphicsToolkit);
			SpriteProgressBarLSEConfig.loadFonts(graphicsToolkit, (byte) -88);
			NPCDefinition.loadSprites(graphicsToolkit, (byte) -75, spriteJs5);
			Class303.setImageTagSprites(OrthoZoomPreferenceField.nameIcons, 0);
		}
		return ClientStream.nextStartupStage(-5133);
	}

	public static final void screenZoom(int i, int i_2_, int i_3_) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i, -113, i_3_);
		class98_sub46_sub17.method1626((byte) -103);
		class98_sub46_sub17.anInt6054 = i_2_;
	}

	public static final boolean stateIsLoadingScreen(int i, int i_23_) {
		return !((i ^ 0xffffffff) != -1 && (i ^ 0xffffffff) != -2 && (i ^ 0xffffffff) != -3);
	}

	public static final boolean tryDecompressTitleImage(boolean bool) {
		try {
			if (bool != false) {
				return true;
			}
			GZipDecompressor gZip = new GZipDecompressor();
			byte[] is = gZip.decompress((byte) 126, Class74.aByteArray546);
			Class271.loadRgbImageJpeg(is, 1);
			return true;
		} catch (Exception exception) {
			return true;
		}
	}

	@Override
	public final synchronized void addCanvas(int i) {
		if (applet != null && canvas == null && !signLink.msJava) {
			try {
				Class<? extends Applet> var_class = applet.getClass();
				Field field = var_class.getDeclaredField("canvas");
				canvas = (Canvas) field.get(applet);
				field.set(applet, null);
				if (canvas != null) {
					return;
				}
			} catch (Exception exception) {
				/* empty */
			}
		}
		super.addCanvas(i);
	}

	@Override
	public final String generateDebugInfoString(int i) {
		String string = null;
		try {
			string = "[1)" + Class272.gameSceneBaseX + "," + aa_Sub2.gameSceneBaseY + "," + Class165.mapWidth + "," + Class98_Sub10_Sub7.mapLength + "|";
			if (Class87.localPlayer != null) {
				string += "2)" + Font.localPlane + "," + (Class272.gameSceneBaseX + Class87.localPlayer.pathX[0]) + "," + (Class87.localPlayer.pathZ[0] + aa_Sub2.gameSceneBaseY) + "|";
			}
			string += "3)" + preferences.currentToolkit.getValue((byte) 124) + "|4)" + preferences.antiAliasing.getValue((byte) 122) + "|5)" + OpenGlModelRenderer.getWindowMode((byte) -59) + "|6)" + frameWidth + "," + frameHeight + "|";
			string += "7)" + preferences.lightningDetail.getValue((byte) 127) + "|";
			string += "8)" + preferences.sceneryShadows.getValue((byte) 126) + "|";
			string += "9)" + preferences.waterDetail.getValue((byte) 121) + "|";
			string += "10)" + preferences.textures.getValue((byte) 120) + "|";
			string += "11)" + preferences.unknown3.getValue((byte) 127) + "|";
			string += "12)" + preferences.aClass64_Sub3_4076.getValue((byte) 126) + "|";
			string += "13)" + maxHeap + "|";
			string += "14)" + clientState;
			if (Exception_Sub1.platformInformation != null) {
				string += "|15)" + Exception_Sub1.platformInformation.totalPhysicMemory;
			}
			try {
				if (preferences.currentToolkit.getValue((byte) 125) == 2) {
					Class<?> var_class = Class.forName("java.lang.ClassLoader");
					Field field = var_class.getDeclaredField("nativeLibraries");
					Class<?> var_class_88_ = Class.forName("java.lang.reflect.AccessibleObject");
					Method method = var_class_88_.getDeclaredMethod("setAccessible", Boolean.TYPE);
					method.invoke(field, Boolean.TRUE);
					Vector<?> vector = (Vector<?>) field.get((component != null ? component : (component = getClassByName("com.client"))).getClassLoader());
					for (int i_89_ = 0; (vector.size() ^ 0xffffffff) < (i_89_ ^ 0xffffffff); i_89_++) {
						try {
							Object object = vector.elementAt(i_89_);
							Field field_90_ = object.getClass().getDeclaredField("name");
							method.invoke(field_90_, Boolean.TRUE);
							try {
								String string_91_ = (String) field_90_.get(object);
								if (string_91_ != null && (string_91_.indexOf("sw3d.dll") ^ 0xffffffff) != 0) {
									Field field_92_ = object.getClass().getDeclaredField("handle");
									method.invoke(field_92_, Boolean.TRUE);
									string += "|16)" + Long.toHexString(field_92_.getLong(object));
									method.invoke(field_92_, Boolean.FALSE);
								}
							} catch (Throwable throwable) {
								/* empty */
							}
							method.invoke(field_90_, Boolean.FALSE);
						} catch (Throwable throwable) {
							/* empty */
						}
					}
				}
			} catch (Throwable throwable) {
				/* empty */
			}
			string += "]";
		} catch (Throwable throwable) {
			/* empty */
		}
		return string;
	}

	@Override
	public final void init() {
		try {
			if (method89(31)) {
				gameServer = new Server();
				gameServer.index = Integer.parseInt(getParameter("worldid"));
				lobbyServer = new Server();
				lobbyServer.index = Integer.parseInt(getParameter("lobbyid"));
				lobbyServer.host = getParameter("lobbyaddress");
				secondLobbyServer = new Server();
				secondLobbyServer.index = Integer.parseInt(getParameter("demoid"));
				secondLobbyServer.host = getParameter("demoaddress");
				buildLocation = BuildLocation.get(103, Integer.parseInt(getParameter("modewhere")));
				if (buildLocation != BuildLocation.LOCAL) {
					if (!BuildLocation.isInHouseTest(true, buildLocation) && buildLocation != BuildLocation.LIVE) {
						buildLocation = BuildLocation.LIVE;
					}
				} else {
					buildLocation = BuildLocation.WTWIP;
				}
				buildType = BuildType.get(Integer.parseInt(getParameter("modewhat")), 108);
				if (BuildType.WIP != buildType && buildType != BuildType.RC && BuildType.LIVE != buildType) {
					buildType = BuildType.LIVE;
				}
				try {
					gameLanguage = Integer.parseInt(getParameter("lang"));
				} catch (Exception exception) {
					gameLanguage = 0;
				}
				String string = getParameter("objecttag");
				jsEnabled = string != null && string.equals("1");
				String string_134_ = getParameter("js");
				useObjectTag = !(string_134_ == null || !string_134_.equals("1"));
				String string_135_ = getParameter("advert");
				Class172.aBoolean1321 = !(string_135_ == null || !string_135_.equals("1"));
				String string_136_ = getParameter("game");
				if (string_136_ != null) {
					if (string_136_.equals("0")) {
						game = GameDefinition.RUNESCAPE;
					} else if (!string_136_.equals("1")) {
						if (!string_136_.equals("2")) {
							if (string_136_.equals("3")) {
								game = GameDefinition.GAME4;
							}
						} else {
							game = GameDefinition.GAME3;
						}
					} else {
						game = GameDefinition.STELLAR_DAWN;
					}
				}
				try {
					affiliateId = Integer.parseInt(getParameter("affid"));
				} catch (Exception exception) {
					affiliateId = 0;
				}
				ScalingSpriteLoadingScreenElement.aString3440 = getParameter("quiturl");
				settingsString = getParameter("settings");
				if (settingsString == null) {
					settingsString = "";
				}
				RSToolkit.aBoolean940 = "1".equals(getParameter("under"));
				String string_137_ = getParameter("country");
				if (string_137_ != null) {
					try {
						countryId = Integer.parseInt(string_137_);
					} catch (Exception exception) {
						countryId = 0;
					}
				}
				colourId = Integer.parseInt(getParameter("colourid"));
				if (colourId < 0 || (AwtLoadingScreen.BAR_COLOURS.length ^ 0xffffffff) >= (colourId ^ 0xffffffff)) {
					colourId = 0;
				}
				if (Integer.parseInt(getParameter("sitesettings_member")) == 1) {
					isMembers1 = isMembers2 = true;
				}
				String string_138_ = getParameter("frombilling");
				if (string_138_ != null && string_138_.equals("true")) {
					forceBilling = true;
				}
				ssKey = getParameter("sskey");
				if (ssKey != null && (ssKey.length() ^ 0xffffffff) > -3) {
					ssKey = null;
				}
				String string_139_ = getParameter("force64mb");
				if (string_139_ != null && string_139_.equals("true")) {
					force64Mb = true;
				}
				String string_140_ = getParameter("worldflags");
				if (string_140_ != null) {
					try {
						worldFlags = Integer.parseInt(string_140_);
					} catch (Exception exception) {
						/* empty */
					}
				}
				String string_141_ = getParameter("userFlow");
				if (string_141_ != null) {
					try {
						userFlow = Long.parseLong(string_141_);
					} catch (NumberFormatException numberformatexception) {
						/* empty */
					}
				}
				additionalInfo = getParameter("additionalInfo");
				if (additionalInfo != null && additionalInfo.length() > 50) {
					additionalInfo = null;
				}
				activeClient = this;
				if (GameDefinition.RUNESCAPE != game) {
					if (game == GameDefinition.STELLAR_DAWN) {
						clientHeight = 480;
						clientWidth = 640;
					}
				} else {
					clientHeight = 503;
					clientWidth = 765;
				}
				startApplet(637, clientHeight, 4, 37, game.code, 32 + buildType.getId(-124), clientWidth);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "init(" + ')');
		}
	}

	private final void js5Connect(byte i) {
		do {
			if ((js5Client.reconnectCount ^ 0xffffffff) < (js5ConnectSucceededTries ^ 0xffffffff)) {
				server.rotateConnectionMethod(0);
				js5ReconnectTimer = (js5Client.reconnectCount - 1) * 250;
				if ((js5ReconnectTimer ^ 0xffffffff) < -3001) {
					js5ReconnectTimer = 3000;
				}
				if (js5Client.reconnectCount >= 2 && (js5Client.status ^ 0xffffffff) == -7) {
					error((byte) 89, "js5connect_outofdate");
					clientState = 14;
					break;
				}
				if (js5Client.reconnectCount >= 4 && js5Client.status == -1) {
					error((byte) -126, "js5crc");
					clientState = 14;
					break;
				}
				if (js5Client.reconnectCount >= 4 && stateIsLoadingScreen(clientState, 126)) {
					if ((js5Client.status ^ 0xffffffff) != -8 && js5Client.status != 9) {
						if (js5Client.status <= 0) {
							error((byte) 112, "js5io");
						} else if (js5ErrorString != null) {
							error((byte) -60, "js5proxy_" + js5ErrorString.trim());
						} else {
							error((byte) 125, "js5connect");
						}
					} else {
						error((byte) -79, "js5connect_full");
					}
					clientState = 14;
					break;
				}
			}
			js5ConnectSucceededTries = js5Client.reconnectCount;
			if ((js5ReconnectTimer ^ 0xffffffff) < -1) {
				js5ReconnectTimer--;
			} else {
				try {
					if (i <= -127) {
						if (js5ConnectStage == 0) {
							js5ConnectRequest = server.openSocket(-111, signLink);
							js5ConnectStage++;
						}
						if ((js5ConnectStage ^ 0xffffffff) == -2) {
							if ((js5ConnectRequest.status ^ 0xffffffff) == -3) {
								if (js5ConnectRequest.result != null) {
									js5ErrorString = (String) js5ConnectRequest.result;
								}
								signalJs5Error((byte) -119, 1000);
								break;
							}
							if ((js5ConnectRequest.status ^ 0xffffffff) == -2) {
								js5ConnectStage++;
							}
						}
						if ((js5ConnectStage ^ 0xffffffff) == -3) {
							js5Stream = new ClientStream((Socket) js5ConnectRequest.result, signLink, 25000);
							RSByteBuffer packet = new RSByteBuffer(5);
							packet.writeByte(LoginOpcode.ON_DEMAND.opcode, -84);
							packet.writeInt(1571862888, 637);
							js5Stream.write((byte) 77, 0, 5, packet.payload);
							js5ConnectStage++;
							js5LastConnectTime = TimeTools.getCurrentTime(-47);
						}
						if (js5ConnectStage == 3) {
							if (stateIsLoadingScreen(clientState, 127) || js5Stream.available(95) > 0) {
								int i_82_ = js5Stream.read(-14234);
								if (i_82_ != 0) {
									signalJs5Error((byte) -96, i_82_);
									break;
								}
								js5ConnectStage++;
							} else if (TimeTools.getCurrentTime(-47) + -js5LastConnectTime > 30000L) {
								signalJs5Error((byte) -95, 1001);
								break;
							}
						}
						if ((js5ConnectStage ^ 0xffffffff) != -5) {
							break;
						}
						boolean notLoggedin = stateIsLoadingScreen(clientState, 127) || Class53_Sub1.method499(2048, clientState) || Class246_Sub3_Sub3.method3011(-6410, clientState);
						PrefetchObject[] prefetchObjects = PrefetchObject.getAll(4);
						RSByteBuffer packet = new RSByteBuffer(4 * prefetchObjects.length);
						js5Stream.read(0, true, packet.payload.length, packet.payload);
						for (int i_83_ = 0; (prefetchObjects.length ^ 0xffffffff) < (i_83_ ^ 0xffffffff); i_83_++) {
							prefetchObjects[i_83_].setWeight(packet.readInt(-2), 107);
						}
						js5Client.connect(js5Stream, !notLoggedin, (byte) 74);
						js5Stream = null;
						js5ConnectStage = 0;
						js5ConnectRequest = null;
					}
				} catch (java.io.IOException ioexception) {
					signalJs5Error((byte) -118, 1002);
				}
				break;
			}
			break;
		} while (false);
	}

	@Override
	public final void mainInit(int i) {
		if (force64Mb) {
			maxHeap = 64;
		}
		if (i == -13395) {
			Frame frame = new Frame("Jagex");
			frame.pack();
			frame.dispose();
			handleResize((byte) -106);
			asyncCache = new AsyncCache(signLink);
			js5Client = new Js5Client();
			PacketBufferManager.init(new int[] { 20, 260 }, new int[] { 1000, 100 }, 0);
			if (buildLocation != BuildLocation.LIVE) {
				Class76.aByteArrayArray590 = new byte[50][];
			}
			preferences = GamePreferences.loadPreferences();
			if (BuildLocation.LIVE != buildLocation) {
				if (!BuildLocation.isInHouseTest(true, buildLocation)) {
					if (BuildLocation.LOCAL == buildLocation) {
						gameServer.host = Launcher.mainurl;
						lobbyServer.host = Launcher.mainurl;
						secondLobbyServer.host = Launcher.mainurl;
						gameServer.initialPort = gameServer.index + 40000;
						lobbyServer.initialPort = 40000 + lobbyServer.index;
						secondLobbyServer.initialPort = secondLobbyServer.index + 40000;
						gameServer.secondPort = gameServer.index + 50000;
						lobbyServer.secondPort = lobbyServer.index + 50000;
						secondLobbyServer.secondPort = secondLobbyServer.index + 50000;
					}
				} else {
					gameServer.host = getCodeBase().getHost();
					gameServer.initialPort = 40000 + gameServer.index;
					lobbyServer.initialPort = lobbyServer.index + 40000;
					secondLobbyServer.initialPort = secondLobbyServer.index + 40000;
					gameServer.secondPort = gameServer.index + 50000;
					lobbyServer.secondPort = lobbyServer.index + 50000;
					secondLobbyServer.secondPort = 50000 - -secondLobbyServer.index;
				}
			} else {
				gameServer.host = getCodeBase().getHost();
			}
			server = gameServer;
			if (GameDefinition.STELLAR_DAWN == game) {
				AwtMouseListener.anInt5298 = 16777215;
				Class98_Sub10_Sub11.aShortArrayArray5597 = Class98_Sub10_Sub24.aShortArrayArray5669;
				Js5Manager.shiftClickEnabled = true;
				Class189.anInt1455 = 0;
				HashTableIterator.colourToReplace = Class119.aShortArrayArrayArray983;
			} else if (GameDefinition.GAME4 == game) {
				HashTableIterator.colourToReplace = FileProgressMonitor.aShortArrayArrayArray3397;
				Class98_Sub10_Sub11.aShortArrayArray5597 = Class48_Sub1_Sub1.aShortArrayArray5503;
			} else {
				Class98_Sub10_Sub11.aShortArrayArray5597 = ClientScript2Event.aShortArrayArray3987;
				HashTableIterator.colourToReplace = StrongReferenceMCNode.aShortArrayArrayArray6299;
			}
			if (game == GameDefinition.RUNESCAPE) {
				Class246_Sub3_Sub5_Sub2.aBoolean6272 = false;
			}
			SceneGraphNode.clientPalette = Class372.clientPalette = Class265.clientPalette = Sound.clientPalette = new short[256];
			try {
				systemClipboard = activeClient.getToolkit().getSystemClipboard();
			} catch (Exception exception) {
				/* empty */
			}
			keyListener = RtKeyListener.create((byte) 10, canvas);
			mouseListener = RtMouseListener.create(canvas, true, -16777216);
			try {
				if (signLink.cacheDataFile != null) {
					dataFile = new SeekableFile(signLink.cacheDataFile, 5200, 0);
					for (int indexPtr = 0; (indexPtr ^ 0xffffffff) > -38; indexPtr++) {
						indexFiles[indexPtr] = new SeekableFile(signLink.cacheIndexFiles[indexPtr], 6000, 0);
					}
					masterIndexFile = new SeekableFile(signLink.cacheId225File, 6000, 0);
					indexDataFs = new DataFs(255, dataFile, masterIndexFile, 500000);
					randomFile = new SeekableFile(signLink.randomFile, 24, 0);
					signLink.randomFile = null;
					signLink.cacheDataFile = null;
					signLink.cacheIndexFiles = null;
					signLink.cacheId225File = null;
				}
			} catch (java.io.IOException ioexception) {
				dataFile = null;
				masterIndexFile = null;
				indexDataFs = null;
				randomFile = null;
			}
			if (BuildLocation.LIVE != buildLocation) {
				displayFps = true;
			}
			AwtLoadingScreen.loadingBarTitle = TextResources.LOADING.getText(gameLanguage, (byte) 25);
		}
	}

	@Override
	public final void mainLoop(byte i) {
		if ((preferences.currentToolkit.getValue((byte) 122) ^ 0xffffffff) == -3) {
			try {
				method108((byte) 68);
			} catch (ThreadDeath threaddeath) {
				throw threaddeath;
			} catch (Throwable throwable) {
				Class305_Sub1.reportError(throwable, i ^ 0x7e, throwable.getMessage() + " (Recovered) " + generateDebugInfoString(0));
				Class223.aBoolean1679 = true;
				Class76_Sub4.method754(0, false, -125);
			}
		} else {
			method108((byte) 68);
		}
	}

	@Override
	public final void mainQuit(boolean bool) {
		try {
			if (Class66.aBoolean507) {
				Class23.method283((byte) 98);
			}
			Class98_Sub43_Sub1.method1493(124);
			if (graphicsToolkit != null) {
				graphicsToolkit.destroy(-1);
			}
			if (fullScreenFrame != null) {
				Class281.method3331(bool, fullScreenFrame, signLink);
				fullScreenFrame = null;
			}
			if (aa_Sub1.aClass123_3561 != null) {
				aa_Sub1.aClass123_3561.close(-18);
				aa_Sub1.aClass123_3561 = null;
			}
			Class246_Sub3_Sub4_Sub5.method3084(true);
			js5Client.requestClientJs5Drop(-84);
			asyncCache.shutDown((byte) -75);
			if (Class48_Sub2_Sub1.aClass265_5531 != null) {
				Class48_Sub2_Sub1.aClass265_5531.method3232((byte) -103);
				Class48_Sub2_Sub1.aClass265_5531 = null;
			}
			try {
				dataFile.method2843((byte) -54);
				int i = 0;
				if (bool != false) {
					method115();
				}
				for (/**/; i < 37; i++) {
					indexFiles[i].method2843((byte) -99);
				}
				masterIndexFile.method2843((byte) -73);
				randomFile.method2843((byte) -67);
				Class42_Sub1_Sub1.method387(true);
			} catch (Exception exception) {
				/* empty */
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "F(" + bool + ')');
		}
	}

	@Override
	public final void mainRedraw(int i) {
		do {
			if ((preferences.currentToolkit.getValue((byte) 123) ^ 0xffffffff) != -3) {
				method99((byte) -9);
				if (!cleanedStatics) {
					break;
				}
			}
			try {
				method99((byte) -9);
			} catch (ThreadDeath threaddeath) {
				throw threaddeath;
			} catch (Throwable throwable) {
				Class305_Sub1.reportError(throwable, -124, throwable.getMessage() + " (Recovered) " + generateDebugInfoString(0));
				Class223.aBoolean1679 = true;
				Class76_Sub4.method754(0, false, 70);
			}
		} while (false);
	}

	private final void method108(byte i) {
		try {
			if (clientState != 14) {
				Queue.timer++;
				if ((Queue.timer % 1000 ^ 0xffffffff) == -2) {
					GregorianCalendar gregoriancalendar = new GregorianCalendar();
					Class39_Sub1.anInt3591 = 600 * gregoriancalendar.get(11) + gregoriancalendar.get(12) * 10 - -(gregoriancalendar.get(13) / 6);
					Class76_Sub8.aRandom3767.setSeed(Class39_Sub1.anInt3591);
				}
				if (Queue.timer % 50 == 0) {
					Class98_Sub12.bytesOut = Class98_Sub50.anInt4289;
					OpenGLHeightMapToNormalMapConverter.bytesIn = Class103.anInt892;
					Class98_Sub50.anInt4289 = 0;
					Class103.anInt892 = 0;
				}
				method109((byte) 59);
				if (js5Manager != null) {
					js5Manager.process(0);
				}
				Class40.method360((byte) 79);
				SceneryShadowsPreferenceField.method578(16543);
				keyListener.method774((byte) -113);
				mouseListener.method3516((byte) 108);
				if (graphicsToolkit != null) {
					graphicsToolkit.method1806((int) TimeTools.getCurrentTime(i ^ ~0x6a));
				}
				NPCDefinitionParser.method3540(-2);
				QuickChatCategory.keyReleaseCount = 0;
				keyPressCount = 0;
				if (i != 68) {
					method103(81, null);
				}
				for (RtKeyEvent keyEvent = keyListener.getNextEvent((byte) 31); keyEvent != null; keyEvent = keyListener.getNextEvent((byte) 31)) {
					int i_85_ = keyEvent.getEventType(-111);
					if ((i_85_ ^ 0xffffffff) != -3 && (i_85_ ^ 0xffffffff) != -4) {
						if (i_85_ == 0 && (QuickChatCategory.keyReleaseCount ^ 0xffffffff) > -76) {
							Archive.anInterface7Array2845[QuickChatCategory.keyReleaseCount] = keyEvent;
							QuickChatCategory.keyReleaseCount++;
						}
					} else {
						int i_86_ = keyEvent.getKeyChar(13313);
						if (!NativeModelRenderer.method2408((byte) 57) || i_86_ != 96 && i_86_ != 167 && (i_86_ ^ 0xffffffff) != -179) {
							if (keyPressCount < 128) {
								keyPressEvents[keyPressCount] = keyEvent;
								keyPressCount++;
							}
						} else if (!GroupProgressMonitor.isOpen(-8001)) {
							OpenGlTileMaterial.method1173(0);
						} else {
							InventoriesDefinitionParser.method187(true);
						}
						Class98_Sub10_Sub3.anIntArray1283 = new int[100];
						Class98_Sub10_Sub3.anIntArray10273 = new int[100];
						Class98_Sub10_Sub3.anIntArray1289 = new int[100];
						Class261.anIntArray3879 = new int[100];
						for (int i_0_ = 0; i_0_ < 100; i_0_++) {
							Class98_Sub10_Sub3.anIntArray1283[i_0_] = ((int) (Math.random() * GameShell.frameWidth) << 4) + (int) (Math.random() * 15.0);
							Class98_Sub10_Sub3.anIntArray10273[i_0_] = (int) (Math.random() * 350.0) << 4;
							Class98_Sub10_Sub3.anIntArray1289[i_0_] = (int) (Math.random() * 102.0) + 51;
							Class261.anIntArray3879[i_0_] = 8 + (int) (Math.random() * 48.0);
						}
						Class98_Sub10_Sub3.anIntArray10356 = new int[GameShell.frameWidth >> 1];
					}
				}
				Class319.mouseScrollDelta = 0;
				for (RtMouseEvent class98_sub17 = mouseListener.getNextEvent(600); class98_sub17 != null; class98_sub17 = mouseListener.getNextEvent(600)) {
					int i_87_ = class98_sub17.getEventType(i ^ ~0x40);
					if (i_87_ == -1) {
						CpuUsagePreferenceField.aClass148_3703.addLast(class98_sub17, -20911);
					} else if (i_87_ != 6) {
						if (Class265.method3230(i + -192, i_87_)) {
							VarPlayerDefinition.aClass148_1284.addLast(class98_sub17, i ^ ~0x51ea);
							if (VarPlayerDefinition.aClass148_1284.method2424(0) > 10) {
								VarPlayerDefinition.aClass148_1284.removeFirst(i ^ 0x191a);
							}
						}
					} else {
						Class319.mouseScrollDelta += class98_sub17.method1152(i + -66);
					}
				}
				if (GroupProgressMonitor.isOpen(-8001)) {
					Class261.process(104);
				}
				if (!stateIsLoadingScreen(clientState, 126)) {
					if (FloorOverlayDefinition.method2690(clientState, 8)) {
						Class181.method2607((byte) 99);
					}
				} else {
					Class98_Sub43_Sub1.method1490(-16798);
					FloorOverlayDefinitionParser.method316(false);
				}
				if (!Class53_Sub1.method499(2048, clientState) || FloorOverlayDefinition.method2690(clientState, 8)) {
					if (Class246_Sub3_Sub3.method3011(-6410, clientState) && !FloorOverlayDefinition.method2690(clientState, i ^ 0x4c)) {
						method112(i + -58);
						Class332_Sub1.method3753(74);
					} else if (clientState != 12) {
						if (!OpenGLHeap.method1683(i ^ ~0x2c64, clientState) || FloorOverlayDefinition.method2690(clientState, 8)) {
							if (clientState == 13) {
								Class332_Sub1.method3753(120);
								if ((SocketWrapper.anInt300 ^ 0xffffffff) != 2 && (SocketWrapper.anInt300 ^ 0xffffffff) != -3 && SocketWrapper.anInt300 != 15) {
									Class98_Sub10_Sub1.handleLogout(false, false);
								}
							}
						} else {
							Class185.method2629((byte) -53);
						}
					} else {
						Class332_Sub1.method3753(115);
					}
				} else {
					method112(10);
					OrthoZoomPreferenceField.method628(19700);
					Class332_Sub1.method3753(113);
				}
				Class119_Sub3.method2186(i ^ ~0x7d, graphicsToolkit);
				VarPlayerDefinition.aClass148_1284.removeFirst(i ^ 0x191a);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "DA(" + i + ')');
		}
	}

	private final void method109(byte i) {
		do {
			try {
				boolean bool = js5Client.process(4096);
				if (i != 59) {
					anInt3550 = -77;
				}
				if (bool) {
					break;
				}
				js5Connect((byte) -128);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "EA(" + i + ')');
			}
			break;
		} while (false);
	}

	private final void method112(int i) {
		try {
			if ((clientState ^ 0xffffffff) == -8 && MaxScreenSizePreferenceField.anInt3680 == 0) {
				if (Class98_Sub10_Sub6.anInt5569 > 1) {
					Class98_Sub36.anInt4161 = WorldMapInfoDefinition.logicTimer;
					Class98_Sub10_Sub6.anInt5569--;
				}
				if (!Player.menuOpen) {
					Class46.method435((byte) 89);
				}
				for (int i_106_ = 0; (i_106_ ^ 0xffffffff) > -101; i_106_++) {
					if (!Class98_Sub10_Sub24.method1076(114)) {
						break;
					}
				}
			}
			GameDefinition.anInt2099++;
			Class98_Sub1.method946(-1, i + -119, -1, null);
			GraphicsDefinitionParser.method3563(-1, null, -1, 60);
			Class3.method172(118);
			WorldMapInfoDefinition.logicTimer++;
			for (int i_107_ = 0; (Class98_Sub10_Sub20.anInt5640 ^ 0xffffffff) < (i_107_ ^ 0xffffffff); i_107_++) {
				NPC npc = BackgroundColourLSEConfig.aClass98_Sub39Array3516[i_107_].npc;
				if (npc != null) {
					byte i_108_ = npc.definition.aByte1103;
					if ((0x1 & i_108_) != 0) {
						int i_109_ = npc.getSize(0);
						if ((0x2 & i_108_ ^ 0xffffffff) != -1 && (npc.pathLength ^ 0xffffffff) == -1 && Math.random() * 1000.0 < 10.0) {
							int i_110_ = (int) Math.round(-5.0 + Math.random() * 10.0);
							int i_111_ = (int) Math.round(-5.0 + 10.0 * Math.random());
							if ((i_110_ ^ 0xffffffff) != -1 || (i_111_ ^ 0xffffffff) != -1) {
								int i_112_ = npc.pathX[0] - -i_110_;
								int i_113_ = i_111_ + npc.pathZ[0];
								if ((i_112_ ^ 0xffffffff) > -1) {
									i_112_ = 0;
								} else if ((-i_109_ + Class165.mapWidth - 1 ^ 0xffffffff) > (i_112_ ^ 0xffffffff)) {
									i_112_ = -1 + -i_109_ + Class165.mapWidth;
								}
								if (i_113_ < 0) {
									i_113_ = 0;
								} else if (Class98_Sub10_Sub7.mapLength - i_109_ - 1 < i_113_) {
									i_113_ = Class98_Sub10_Sub7.mapLength + -i_109_ - 1;
								}
								int i_114_ = method96(VarPlayerDefinition.clipMaps[npc.plane], i_109_, Class76_Sub5.anIntArray3743, 0, 0, i_113_, i_109_, npc.pathZ[0], ISAACPseudoRNG.anIntArray974, true, i_112_, -1, 48, npc.pathX[0], i_109_);
								if (i_114_ > 0) {
									if ((i_114_ ^ 0xffffffff) < -10) {
										i_114_ = 9;
									}
									for (int i_115_ = 0; (i_115_ ^ 0xffffffff) > (i_114_ ^ 0xffffffff); i_115_++) {
										((Mob) npc).pathX[i_115_] = Class76_Sub5.anIntArray3743[-1 + i_114_ - i_115_];
										((Mob) npc).pathZ[i_115_] = ISAACPseudoRNG.anIntArray974[-1 + i_114_ + -i_115_];
										((Mob) npc).pathSpeed[i_115_] = (byte) 1;
									}
									npc.pathLength = i_114_;
								}
							}
						}
						Class333.method3762((byte) 57, true, npc);
						int i_116_ = Class98_Sub10_Sub13.method1041(npc, 0);
						Class108.method1729(-90, npc);
						Class284_Sub1_Sub2.method3370(BConfigDefinition.anInt3121, i ^ 0x180a, npc, AntialiasPreferenceField.anInt3708, i_116_);
						LoadingScreenSequence.method3334((byte) 37, AntialiasPreferenceField.anInt3708, npc);
						Class340.method3801(npc, -28111);
					}
				}
			}
			if (MaxScreenSizePreferenceField.anInt3680 == 0 && Class21_Sub4.anInt5394 == 0) {
				if (Class98_Sub46_Sub20_Sub2.cameraMode != 2) {
					AnimationDefinitionParser.method2620(0);
				} else {
					Class98_Sub46_Sub3.method1541(123);
				}
				if (Class98_Sub46_Sub10.xCamPosTile >> 1205626121 < 14 || Class98_Sub46_Sub10.xCamPosTile >> -1191358487 >= Class165.mapWidth - 14 || SpriteLoadingScreenElement.yCamPosTile >> -1312364695 < 14 || (SpriteLoadingScreenElement.yCamPosTile >> 242670249
						^ 0xffffffff) <= (Class98_Sub10_Sub7.mapLength - 14 ^ 0xffffffff)) {
					HashTableIterator.method537((byte) 66);
				}
			}
			for (;;) {
				ClientScript2Event class98_sub21 = (ClientScript2Event) Class181.aClass148_1430.removeFirst(6494);
				if (class98_sub21 == null) {
					break;
				}
				RtInterface class293 = class98_sub21.component;
				if (class293.componentIndex >= 0) {
					RtInterface class293_117_ = RtInterface.getInterface(class293.parentId, -9820);
					if (class293_117_ == null || class293_117_.dynamicComponents == null || class293_117_.dynamicComponents.length <= class293.componentIndex || class293_117_.dynamicComponents[class293.componentIndex] != class293) {
						continue;
					}
				}
				ClientScript2Runtime.handleEvent(class98_sub21);
			}
			for (;;) {
				ClientScript2Event class98_sub21 = (ClientScript2Event) Class98_Sub46_Sub10.aClass148_6018.removeFirst(6494);
				if (class98_sub21 == null) {
					break;
				}
				RtInterface class293 = class98_sub21.component;
				if ((class293.componentIndex ^ 0xffffffff) <= -1) {
					RtInterface class293_118_ = RtInterface.getInterface(class293.parentId, -9820);
					if (class293_118_ == null || class293_118_.dynamicComponents == null || (class293.componentIndex ^ 0xffffffff) <= (class293_118_.dynamicComponents.length ^ 0xffffffff) || class293_118_.dynamicComponents[class293.componentIndex] != class293) {
						continue;
					}
				}
				ClientScript2Runtime.handleEvent(class98_sub21);
			}
			for (;;) {
				ClientScript2Event class98_sub21 = (ClientScript2Event) Class151_Sub3.aClass148_4977.removeFirst(6494);
				if (class98_sub21 == null) {
					break;
				}
				RtInterface class293 = class98_sub21.component;
				if (class293.componentIndex >= 0) {
					RtInterface class293_119_ = RtInterface.getInterface(class293.parentId, -9820);
					if (class293_119_ == null || class293_119_.dynamicComponents == null || class293_119_.dynamicComponents.length <= class293.componentIndex || class293 != class293_119_.dynamicComponents[class293.componentIndex]) {
						continue;
					}
				}
				ClientScript2Runtime.handleEvent(class98_sub21);
			}
			if (Class255.aClass293_3208 != null) {
				NativeMatrix.method2118(19653);
			}
			if ((Queue.timer % 1500 ^ 0xffffffff) == -1) {
				Class4.method174((byte) 99);
			}
			if (clientState == 7 && (MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) == -1) {
				Class204.method2709((byte) 49);
			}
			Class53_Sub1.method3951((byte) -101);
			if (Class66.aBoolean507 && (OpenGlShadow.aLong6322 ^ 0xffffffffffffffffL) > (-60000L + TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL)) {
				Class23.method283((byte) 98);
			}
			if (i != 10) {
				anInt3550 = -112;
			}
			for (FriendLoginUpdate update = (FriendLoginUpdate) Class119.friendLogins.getFirst((byte) 15); update != null; update = (FriendLoginUpdate) Class119.friendLogins.getNext(false)) {
				if (-5L + TimeTools.getCurrentTime(-47) / 1000L > update.time) {
					if (update.onlineStatus > 0) {
						ItemDeque.addChatMessage((byte) -97, 5, update.friendName + TextResources.HAS_LOGGED_IN.getText(gameLanguage, (byte) 25), 0, "", "", "");
					}
					if (update.onlineStatus == 0) {
						ItemDeque.addChatMessage((byte) 109, 5, update.friendName + TextResources.HAS_LOGGED_OUT.getText(gameLanguage, (byte) 25), 0, "", "", "");
					}
					update.unlink((byte) -97);
				}
			}
			if (clientState == 7 && (MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) == -1) {
				if (aa_Sub1.aClass123_3561 == null) {
					Class98_Sub10_Sub1.handleLogout(false, false);
				} else {
					BuildLocation.anInt1511++;
					if ((BuildLocation.anInt1511 ^ 0xffffffff) < -51) {
						Class76_Sub5.anInt3746++;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i ^ 0x10e, NodeShort.KEEP_ALIVE, Class331.aClass117_2811);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					}
					try {
						Class95.method920((byte) 124);
					} catch (java.io.IOException ioexception) {
						Class98_Sub10_Sub1.handleLogout(false, false);
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "U(" + i + ')');
		}
	}

	private final void method99(byte i) {
		do {
			try {
				if (clientState != 14) {
					long l = SimpleProgressBarLSEConfig.method913(58) / 1000000L - ActionGroup.aLong5997;
					ActionGroup.aLong5997 = SimpleProgressBarLSEConfig.method913(i + 67) / 1000000L;
					boolean bool = ClientScript2Event.method1178(i + 13308);
					if (bool && Class151_Sub5.aBoolean4991 && SystemInformation.aClass268_1173 != null) {
						SystemInformation.aClass268_1173.method3249((byte) -99);
					}
					if (Class98_Sub5_Sub1.method965((byte) 101, clientState)) {
						if ((Class98_Sub10_Sub25.aLong5677 ^ 0xffffffffffffffffL) != -1L && TimeTools.getCurrentTime(-47) > Class98_Sub10_Sub25.aLong5677) {
							Class98_Sub16.setWindowMode(OpenGlModelRenderer.getWindowMode((byte) 12), Class98_Sub46_Sub15.anInt6039, 3, Class128.anInt1025, false);
						} else if (!graphicsToolkit.method1819() && recommendCanvasReplace) {
							Class277.method3292((byte) -51);
						}
					}
					if (fullScreenFrame == null) {
						java.awt.Container container;
						if (frame == null) {
							if (applet == null) {
								container = activeGameShell;
							} else {
								container = applet;
							}
						} else {
							container = frame;
						}
						int i_0_ = container.getSize().width;
						int i_1_ = container.getSize().height;
						if (frame == container) {
							Insets insets = frame.getInsets();
							i_0_ -= insets.left - -insets.right;
							i_1_ -= insets.top + insets.bottom;
						}
						if ((width ^ 0xffffffff) != (i_0_ ^ 0xffffffff) || (height ^ 0xffffffff) != (i_1_ ^ 0xffffffff) || Class33.aBoolean316) {
							if (graphicsToolkit != null && !graphicsToolkit.method1800()) {
								height = i_1_;
								width = i_0_;
							} else {
								handleResize((byte) -98);
							}
							Class98_Sub10_Sub25.aLong5677 = TimeTools.getCurrentTime(-47) - -500L;
							Class33.aBoolean316 = false;
						}
					}
					if (fullScreenFrame != null && !focus && Class98_Sub5_Sub1.method965((byte) -51, clientState)) {
						Class98_Sub16.setWindowMode(preferences.screenSize.getValue((byte) 126), -1, i + 12, -1, false);
					}
					boolean bool_2_ = false;
					if (fullRedraw) {
						fullRedraw = false;
						bool_2_ = true;
					}
					if (bool_2_) {
						clearAwtGraphics(i ^ ~0x36e3);
					}
					if (graphicsToolkit != null && graphicsToolkit.method1819() || OpenGlModelRenderer.getWindowMode((byte) 110) != 1) {
						Class98_Sub43.setAllDirty(2);
					}
					if (stateIsLoadingScreen(clientState, 126)) {
						Class34.method330((byte) 50, bool_2_);
					} else if (!Class191.method2651(clientState, (byte) -7)) {
						if (Class98_Sub5_Sub3.method974(clientState, (byte) -116)) {
							Font.method398(105);
						} else if (FloorOverlayDefinition.method2690(clientState, i ^ 0xffffffff)) {
							if (Class130.anInt1031 == 1) {
								if (Class98_Sub5_Sub3.anInt5538 < Class142.anInt1160) {
									Class98_Sub5_Sub3.anInt5538 = Class142.anInt1160;
								}
								int i_3_ = 50 * (-Class142.anInt1160 + Class98_Sub5_Sub3.anInt5538) / Class98_Sub5_Sub3.anInt5538;
								Class246_Sub2.draw(-75, Class98_Sub46_Sub10.p12FullMetrics, Class195.p12Full, true, graphicsToolkit, TextResources.LOADING.getText(gameLanguage, (byte) 25) + "<br>(" + i_3_ + "%)");
							} else if ((Class130.anInt1031 ^ 0xffffffff) != -3) {
								Class246_Sub2.draw(-115, Class98_Sub46_Sub10.p12FullMetrics, Class195.p12Full, true, graphicsToolkit, TextResources.LOADING.getText(gameLanguage, (byte) 25));
							} else {
								if (VarClientDefinitionParser.anInt1043 > QuickChatMessageParser.anInt2105) {
									QuickChatMessageParser.anInt2105 = VarClientDefinitionParser.anInt1043;
								}
								int i_4_ = 50 + (QuickChatMessageParser.anInt2105 + -VarClientDefinitionParser.anInt1043) * 50 / QuickChatMessageParser.anInt2105;
								Class246_Sub2.draw(-51, Class98_Sub46_Sub10.p12FullMetrics, Class195.p12Full, true, graphicsToolkit, TextResources.LOADING.getText(gameLanguage, (byte) 25) + "<br>(" + i_4_ + "%)");
							}
						} else if ((clientState ^ 0xffffffff) != -11) {
							if (clientState == 13) {
								Class246_Sub2.draw(i ^ 0x48, Class98_Sub46_Sub10.p12FullMetrics, Class195.p12Full, false, graphicsToolkit, TextResources.CONNECTION_LOST.getText(gameLanguage, (byte) 25) + "<br>" + TextResources.PLEASE_WAIT_ATTEMPTING.getText(gameLanguage, (byte) 25));
							}
						} else {
							Class190.method2648(l, -54);
						}
					} else {
						Font.method398(126);
					}
					if (VarPlayerDefinition.anInt1282 == 3) {
						for (int i_5_ = 0; Class69_Sub2.anInt5335 > i_5_; i_5_++) {
							Rectangle rectangle = PlatformInformation.aRectangleArray4144[i_5_];
							if (!Class232.aBooleanArray1741[i_5_]) {
								if (Class98_Sub10_Sub20.aBooleanArray5639[i_5_]) {
									graphicsToolkit.method1781(true, rectangle.height, rectangle.width, -65536, rectangle.x, rectangle.y);
								} else {
									graphicsToolkit.method1781(true, rectangle.height, rectangle.width, -16711936, rectangle.x, rectangle.y);
								}
							} else {
								graphicsToolkit.method1781(true, rectangle.height, rectangle.width, -65281, rectangle.x, rectangle.y);
							}
						}
					}
					if (GroupProgressMonitor.isOpen(-8001)) {
						Class98_Sub10_Sub3.renderDevelopersConsole(graphicsToolkit, (byte) 126);
					}
					if (signLink.msJava && Class98_Sub5_Sub1.method965((byte) 105, clientState) && (VarPlayerDefinition.anInt1282 ^ 0xffffffff) == -1 && (OpenGlModelRenderer.getWindowMode((byte) 104) ^ 0xffffffff) == -2 && !bool_2_) {
						int i_6_ = 0;
						for (int i_7_ = 0; (i_7_ ^ 0xffffffff) > (Class69_Sub2.anInt5335 ^ 0xffffffff); i_7_++) {
							if (Class98_Sub10_Sub20.aBooleanArray5639[i_7_]) {
								Class98_Sub10_Sub20.aBooleanArray5639[i_7_] = false;
								QuickChatMessageParser.aRectangleArray2106[i_6_++] = PlatformInformation.aRectangleArray4144[i_7_];
							}
						}
						try {
							if (!OpenGLHeap.aBoolean6079) {
								graphicsToolkit.method1794(QuickChatMessageParser.aRectangleArray2106, i_6_, i + 27188);
							} else {
								Class211.method2775(QuickChatMessageParser.aRectangleArray2106, i_6_, i ^ ~0x10c);
							}
						} catch (Exception_Sub1 exception_sub1) {
							/* empty */
						}
					} else if (!stateIsLoadingScreen(clientState, i + 136)) {
						for (int i_8_ = 0; (Class69_Sub2.anInt5335 ^ 0xffffffff) < (i_8_ ^ 0xffffffff); i_8_++) {
							Class98_Sub10_Sub20.aBooleanArray5639[i_8_] = false;
						}
						try {
							if (!OpenGLHeap.aBoolean6079) {
								graphicsToolkit.renderFrame(109);
							} else {
								BuildType.method181((byte) 10);
							}
						} catch (Exception_Sub1 exception_sub1) {
							Class305_Sub1.reportError(exception_sub1, -126, exception_sub1.getMessage() + " (Recovered) " + generateDebugInfoString(0));
							Class76_Sub4.method754(0, false, i + 64);
						}
					}
					if (i == -9) {
						Class289.method3408((byte) 101);
						int i_9_ = preferences.cpuUsage.getValue((byte) 121);
						if ((i_9_ ^ 0xffffffff) != -1) {
							if ((i_9_ ^ 0xffffffff) == -2) {
								TimeTools.sleep(0, 10L);
							} else if (i_9_ != 2) {
								if ((i_9_ ^ 0xffffffff) == -4) {
									TimeTools.sleep(i ^ ~0x8, 2L);
								}
							} else {
								TimeTools.sleep(0, 5L);
							}
						} else {
							TimeTools.sleep(0, 15L);
						}
						if (MapScenesDefinitionParser.aBoolean2817) {
							Class98_Sub10_Sub1.method1002(false);
						}
						if ((preferences.safeMode.getValue((byte) 123) ^ 0xffffffff) != -2 || (clientState ^ 0xffffffff) != -4 || (topLevelInterfaceId ^ 0xffffffff) == 0) {
							break;
						}
						preferences.setPreference((byte) -13, 0, preferences.safeMode);
						Class310.method3618(-5964);
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "CA(" + i + ')');
			}
			break;
		} while (false);
	}

	private final void signalJs5Error(byte i, int stat) {
		js5ConnectStage = 0;
		js5Client.reconnectCount++;
		js5ConnectRequest = null;
		js5Stream = null;
		js5Client.status = stat;
	}
}

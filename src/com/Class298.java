/* Class298 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.shadow.Shadow;

public final class Class298 {

	public static final boolean method3504(int i, int i_2_, int i_3_) {
		return !(!((i_2_ & 0x70000) != 0 | Class105.method1715(true, i_2_, i)) && !TextureMetrics.method2919(-116, i_2_, i));
	}

	public ModelRenderer	aClass146_2477;

	public Shadow			aR2479;

	public Class298() {
		/* empty */
	}
}

package com;

import java.awt.Color;

public class ParticleDescriptor {
	public byte		red;
	public byte		green;
	public byte		blue;
	public float	hue;
	public boolean	m;

	public ParticleDescriptor(byte red, byte green, byte blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		hue = 0.0F;
		m = false;
	}

	public Color inc() {
		hue += 3.3424998E-4F;
		if (hue > 1.0F) {
			hue = 0.0F;
		}
		return Color.getHSBColor(hue, 1.0F, 1.0F);
	}
}
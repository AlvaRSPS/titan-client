
/* Class332_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;
import java.net.Socket;
import java.net.URL;

import com.jagex.Launcher;
import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.crypto.ISAACPseudoRNG;
import com.jagex.game.clanchat.ClanChatMember;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5Manager;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.parser.VarPlayerDefinitionParser;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.client.ui.loading.LoadingScreenRenderer;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.RotatingSpriteLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

import jaggl.OpenGL;

public final class Class332_Sub1 extends Sprite {
	public static Object anObject5409;

	public static final void method3753(int i) {
		try {
			if (i <= 36) {
				anObject5409 = null;
			}
			if (MaxScreenSizePreferenceField.anInt3680 != 0 && (MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) != -6) {
				try {
					int i_40_;
					if (Class151_Sub9.anInt5020 != 0) {
						i_40_ = 2000;
					} else {
						i_40_ = 250;
					}
					if (i_40_ < ++VarPlayerDefinitionParser.anInt1087) {
						if (aa_Sub1.aClass123_3561 != null) {
							aa_Sub1.aClass123_3561.close(-93);
							aa_Sub1.aClass123_3561 = null;
						}
						if (Class151_Sub9.anInt5020 >= 3) {
							MaxScreenSizePreferenceField.anInt3680 = 0;
							Class369.setLoginResponse(-5, (byte) -55);
							return;
						}
						if (Class98_Sub46_Sub20_Sub2.anInt6317 != 2) {
							client.lobbyServer.rotateConnectionMethod(0);
						} else {
							client.server.rotateConnectionMethod(0);
						}
						MaxScreenSizePreferenceField.anInt3680 = 1;
						VarPlayerDefinitionParser.anInt1087 = 0;
						Class151_Sub9.anInt5020++;
					}
					if ((MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) == -2) {
						if ((Class98_Sub46_Sub20_Sub2.anInt6317 ^ 0xffffffff) != -3) {
							Class246_Sub3_Sub3.aClass143_6155 = client.lobbyServer.openSocket(-115, GameShell.signLink);
						} else {
							Class246_Sub3_Sub3.aClass143_6155 = client.server.openSocket(51, GameShell.signLink);
						}
						MaxScreenSizePreferenceField.anInt3680 = 2;
					}
					if (MaxScreenSizePreferenceField.anInt3680 == 2) {
						if (Class246_Sub3_Sub3.aClass143_6155.status == 2) {
							throw new IOException();
						}
						if ((Class246_Sub3_Sub3.aClass143_6155.status ^ 0xffffffff) != -2) {
							return;
						}
						aa_Sub1.aClass123_3561 = BuildLocation.method2668((Socket) Class246_Sub3_Sub3.aClass143_6155.result, (byte) 11, 7500);
						Class246_Sub3_Sub3.aClass143_6155 = null;
						Class49.method477(-5788);
						OutgoingPacket class98_sub11 = ActionGroup.method1556(false);
						class98_sub11.packet.writeByte(LoginOpcode.aClass222_2478.opcode, 43);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class95.method920((byte) 115);
						MaxScreenSizePreferenceField.anInt3680 = 3;
					}
					if (MaxScreenSizePreferenceField.anInt3680 == 3) {
						if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
							return;
						}
						aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, 1);
						int i_41_ = 0xff & PacketParser.buffer.payload[0];
						if ((i_41_ ^ 0xffffffff) != -1) {
							MaxScreenSizePreferenceField.anInt3680 = 0;
							Class369.setLoginResponse(i_41_, (byte) -55);
							aa_Sub1.aClass123_3561.close(-63);
							aa_Sub1.aClass123_3561 = null;
							Class98_Sub12.method1130(27089);
							return;
						}
						PacketParser.buffer.position = 0;
						RSByteBuffer buffer = new RSByteBuffer(518);
						int[] is = new int[4];
						is[2] = (int) (Math.random() * 9.9999999E7);
						is[1] = (int) (9.9999999E7 * Math.random());
						is[0] = (int) (Math.random() * 9.9999999E7);
						is[3] = (int) (Math.random() * 9.9999999E7);
						buffer.writeByte(10, 116);
						buffer.writeInt(1571862888, is[0]);
						buffer.writeInt(1571862888, is[1]);
						buffer.writeInt(1571862888, is[2]);
						buffer.writeInt(1571862888, is[3]);
						buffer.method1221(-68, 0L);
						buffer.writePJStr1(Class360.password, (byte) 113);
						buffer.method1221(-96, Class98_Sub10_Sub19.aLong5631);
						buffer.method1221(-71, Class98_Sub42.aLong4238);
						buffer.rsaEncryption(RSByteBuffer.MODULUS, true, RSByteBuffer.PUBLIC_EXPONENT);
						Class49.method477(-5788);
						OutgoingPacket class98_sub11 = ActionGroup.method1556(false);
						RsBitsBuffers class98_sub22_sub1 = class98_sub11.packet;
						if ((Class98_Sub46_Sub20_Sub2.anInt6317 ^ 0xffffffff) != -3) {
							class98_sub22_sub1.writeByte(LoginOpcode.aClass222_2484.opcode, -63);
							class98_sub22_sub1.writeShort(0, 1571862888);
							int i_42_ = class98_sub22_sub1.position;
							class98_sub22_sub1.writeInt(1571862888, 637);
							class98_sub22_sub1.method1217(buffer.payload, buffer.position, -1, 0);
							int i_43_ = class98_sub22_sub1.position;
							class98_sub22_sub1.writePJStr1(PointLight.username, (byte) 113);
							class98_sub22_sub1.writeByte(client.game.id, 58);
							class98_sub22_sub1.writeByte(client.gameLanguage, -103);
							ParamDefinition.method2430(class98_sub22_sub1, (byte) 0);
							class98_sub22_sub1.writePJStr1(client.settingsString, (byte) 113);
							class98_sub22_sub1.writeInt(1571862888, client.affiliateId);
							Class98_Sub9.method989(class98_sub22_sub1, (byte) 122);
							class98_sub22_sub1.method1235(true, is, i_43_, class98_sub22_sub1.position);
							class98_sub22_sub1.method1207((byte) 90, -i_42_ + class98_sub22_sub1.position);
						} else {
							if (client.clientState == 13) {
								class98_sub22_sub1.writeByte(LoginOpcode.aClass222_2483.opcode, 51);
							} else {
								class98_sub22_sub1.writeByte(LoginOpcode.aClass222_2481.opcode, -72);
							}
							class98_sub22_sub1.writeShort(0, 1571862888);
							int i_44_ = class98_sub22_sub1.position;
							class98_sub22_sub1.writeInt(1571862888, 637);
							class98_sub22_sub1.method1217(buffer.payload, buffer.position, -1, 0);
							int i_45_ = class98_sub22_sub1.position;
							class98_sub22_sub1.writePJStr1(PointLight.username, (byte) 113);
							class98_sub22_sub1.writeByte(OpenGlModelRenderer.anInt4855, 99);
							class98_sub22_sub1.writeByte(OpenGlModelRenderer.getWindowMode((byte) 127), 107);
							class98_sub22_sub1.writeShort(GameShell.frameWidth, 1571862888);
							class98_sub22_sub1.writeShort(GameShell.frameHeight, 1571862888);
							class98_sub22_sub1.writeByte(client.preferences.multiSample.getValue((byte) 124), -58);
							ParamDefinition.method2430(class98_sub22_sub1, (byte) 0);
							class98_sub22_sub1.writePJStr1(client.settingsString, (byte) 113);
							class98_sub22_sub1.writeInt(1571862888, client.affiliateId);
							RSByteBuffer class98_sub22_46_ = client.preferences.encode(true);
							class98_sub22_sub1.writeByte(class98_sub22_46_.position, -109);
							class98_sub22_sub1.method1217(class98_sub22_46_.payload, class98_sub22_46_.position, -1, 0);
							OpenGlGround.aBoolean5207 = true;
							RSByteBuffer class98_sub22_47_ = new RSByteBuffer(Exception_Sub1.platformInformation.method1455((byte) 116));
							Exception_Sub1.platformInformation.method1453((byte) 17, class98_sub22_47_);
							class98_sub22_sub1.method1217(class98_sub22_47_.payload, class98_sub22_47_.payload.length, -1, 0);
							class98_sub22_sub1.writeShort(StartupStage.anInt581, 1571862888);
							class98_sub22_sub1.method1221(-104, client.userFlow);
							class98_sub22_sub1.writeByte(client.additionalInfo == null ? 0 : 1, -34);
							if (client.additionalInfo != null) {
								class98_sub22_sub1.writePJStr1(client.additionalInfo, (byte) 113);
							}
							class98_sub22_sub1.writeByte(Class98_Sub42.contains("jagtheora", 0) ? 1 : 0, 47);
							Class98_Sub9.method989(class98_sub22_sub1, (byte) 100);
							class98_sub22_sub1.method1235(true, is, i_45_, class98_sub22_sub1.position);
							class98_sub22_sub1.method1207((byte) 90, class98_sub22_sub1.position + -i_44_);
						}
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class95.method920((byte) 80);
						Class331.aClass117_2811 = new ISAACPseudoRNG(is);
						for (int i_48_ = 0; i_48_ < 4; i_48_++) {
							is[i_48_] += 50;
						}
						PacketParser.buffer.initializeCypher(is, 255);
						MaxScreenSizePreferenceField.anInt3680 = 4;
					}
					if (MaxScreenSizePreferenceField.anInt3680 == 4) {
						if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
							return;
						}
						aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, 1);
						int i_49_ = 0xff & PacketParser.buffer.payload[0];
						if ((i_49_ ^ 0xffffffff) != -22) {
							if ((i_49_ ^ 0xffffffff) == -30) {
								MaxScreenSizePreferenceField.anInt3680 = 13;
							} else if (i_49_ != 1) {
								if (i_49_ != 2) {
									if ((i_49_ ^ 0xffffffff) == -16) {
										Class65.currentPacketSize = -2;
										MaxScreenSizePreferenceField.anInt3680 = 14;
									} else {
										if (i_49_ == 23 && Class151_Sub9.anInt5020 < 3) {
											Class151_Sub9.anInt5020++;
											MaxScreenSizePreferenceField.anInt3680 = 1;
											VarPlayerDefinitionParser.anInt1087 = 0;
											aa_Sub1.aClass123_3561.close(-114);
											aa_Sub1.aClass123_3561 = null;
										} else {
											MaxScreenSizePreferenceField.anInt3680 = 0;
											Class369.setLoginResponse(i_49_, (byte) -55);
											aa_Sub1.aClass123_3561.close(-113);
											aa_Sub1.aClass123_3561 = null;
											Class98_Sub12.method1130(27089);
											return;
										}
										return;
									}
								} else {
									MaxScreenSizePreferenceField.anInt3680 = 8;
								}
							} else {
								MaxScreenSizePreferenceField.anInt3680 = 5;
								Class369.setLoginResponse(i_49_, (byte) -55);
								return;
							}
						} else {
							MaxScreenSizePreferenceField.anInt3680 = 7;
						}
					}
					if (MaxScreenSizePreferenceField.anInt3680 == 6) {
						Class49.method477(-5788);
						OutgoingPacket class98_sub11 = ActionGroup.method1556(false);
						RsBitsBuffers class98_sub22_sub1 = class98_sub11.packet;
						class98_sub22_sub1.initializeCypher((byte) -104, Class331.aClass117_2811);
						class98_sub22_sub1.method1261(false, LoginOpcode.aClass222_2488.opcode);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class95.method920((byte) 115);
						MaxScreenSizePreferenceField.anInt3680 = 4;
					} else if ((MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) == -8) {
						if (aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
							aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, 1);
							int i_50_ = PacketParser.buffer.payload[0] & 0xff;
							Class98_Sub48.anInt4277 = 180 + i_50_ * 60;
							MaxScreenSizePreferenceField.anInt3680 = 0;
							Class369.setLoginResponse(21, (byte) -55);
							aa_Sub1.aClass123_3561.close(-13);
							aa_Sub1.aClass123_3561 = null;
							Class98_Sub12.method1130(27089);
						}
					} else if ((MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) == -14) {
						if (aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
							aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, 1);
							MaxScreenSizePreferenceField.anInt3680 = 0;
							Class69_Sub1.anInt5330 = PacketParser.buffer.payload[0] & 0xff;
							Class369.setLoginResponse(29, (byte) -55);
							aa_Sub1.aClass123_3561.close(-71);
							aa_Sub1.aClass123_3561 = null;
							Class98_Sub12.method1130(27089);
						}
					} else if ((MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) == -9) {
						if (aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
							aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, 1);
							Class98_Sub46_Sub19.anInt6069 = PacketParser.buffer.payload[0] & 0xff;
							MaxScreenSizePreferenceField.anInt3680 = 9;
						}
					} else {
						if ((MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) == -10) {
							RsBitsBuffers stream = PacketParser.buffer;
							if (Class98_Sub46_Sub20_Sub2.anInt6317 == 2) {
								if (!aa_Sub1.aClass123_3561.isBuffered(-1949, Class98_Sub46_Sub19.anInt6069)) {
									return;
								}
								aa_Sub1.aClass123_3561.read(stream.payload, 0, 2047, Class98_Sub46_Sub19.anInt6069);
								stream.position = 0;
								LoadingScreenSequence.rights = stream.readUnsignedByte((byte) 14);
								LoadingScreenRenderer.anInt407 = stream.readUnsignedByte((byte) 89);
								Js5Manager.aBoolean933 = (stream.readUnsignedByte((byte) 126) ^ 0xffffffff) == -2;
								Class98_Sub10_Sub35.aBoolean5732 = stream.readUnsignedByte((byte) 55) == 1;
								Class234.aBoolean1750 = stream.readUnsignedByte((byte) -123) == 1;
								BaseModel.isQuickChatWorld = stream.readUnsignedByte((byte) -104) == 1;
								OpenGLHeap.localPlayerIndex = stream.readShort((byte) 127);
								client.isMembers1 = stream.readUnsignedByte((byte) -111) == 1;
								ActionGroup.anInt6003 = stream.method1227((byte) -1);
								AdvancedMemoryCache.isMembersOnly = (stream.readUnsignedByte((byte) 74) ^ 0xffffffff) == -2;
								Class130.gameObjectDefinitionList.setAllowMembers((byte) -123, AdvancedMemoryCache.isMembersOnly);
								Class98_Sub46_Sub19.itemDefinitionList.setAllowMembers(-71, AdvancedMemoryCache.isMembersOnly);
								Class4.npcDefinitionList.setAllowMembers(9179409, AdvancedMemoryCache.isMembersOnly);
							} else {
								if (!aa_Sub1.aClass123_3561.isBuffered(-1949, Class98_Sub46_Sub19.anInt6069)) {
									return;
								}
								aa_Sub1.aClass123_3561.read(stream.payload, 0, 2047, Class98_Sub46_Sub19.anInt6069);
								stream.position = 0;
								LoadingScreenSequence.rights = stream.readUnsignedByte((byte) -8);
								LoadingScreenRenderer.anInt407 = stream.readUnsignedByte((byte) 75);
								Js5Manager.aBoolean933 = (stream.readUnsignedByte((byte) -123) ^ 0xffffffff) == -2;
								Class98_Sub10_Sub35.aBoolean5732 = stream.readUnsignedByte((byte) 46) == 1;
								Class234.aBoolean1750 = (stream.readUnsignedByte((byte) 64) ^ 0xffffffff) == -2;
								Class48.anInt409 = stream.readUShort(false);
								client.isMembers1 = (Class48.anInt409 ^ 0xffffffff) < -1;
								Class98_Sub1.anInt3814 = stream.readShort((byte) 127);
								DecoratedProgressBarLSEConfig.anInt5489 = stream.readShort((byte) 127);
								Class98_Sub10_Sub19.anInt5630 = stream.readShort((byte) 127);
								Class151_Sub5.anInt4990 = stream.readInt(-2);
								Class187.aClass143_1449 = GameShell.signLink.resolveHostName(Class151_Sub5.anInt4990, 113);
								DataFs.anInt203 = stream.readUnsignedByte((byte) -124);
								Cacheable.anInt4264 = stream.readShort((byte) 127);
								SpriteProgressBarLSEConfig.anInt5491 = stream.readShort((byte) 127);
								OpenGlGround.aBoolean5200 = (stream.readUnsignedByte((byte) 61) ^ 0xffffffff) == -2;
								Class87.localPlayer.accountName = Class87.localPlayer.displayName = Class256_Sub1.userName = stream.readPrefixedString(-1);
								Class98_Sub43_Sub2.anInt5910 = stream.readUnsignedByte((byte) -120);
								Class36.anInt349 = stream.readInt(-2);
								Class289.server = new Server();
								Class289.server.index = stream.readShort((byte) 127);
								if (Class289.server.index == 65535) {
									Class289.server.index = -1;
								}
								stream.readPrefixedString(-1);
								Class289.server.host = Launcher.mainurl;
								if (client.buildLocation != BuildLocation.LIVE) {
									Class289.server.secondPort = 50000 + Class289.server.index;
									Class289.server.initialPort = 40000 + Class289.server.index;
								}
								if (client.buildLocation != BuildLocation.LOCAL && (client.server.equals(120, client.gameServer) || client.server.equals(113, client.secondLobbyServer))) {
									Class98_Sub10_Sub25.method1080((byte) 96);
								}
							}
							if ((!Js5Manager.aBoolean933 || Class234.aBoolean1750) && !client.isMembers1) {
								try {
									JavaScriptInterface.callJsMethod("unzap", GameShell.applet, -26978);
								} catch (Throwable throwable) {
									/* empty */
								}
							} else {
								try {
									JavaScriptInterface.callJsMethod("zap", GameShell.applet, -26978);
								} catch (Throwable throwable) {
									if (Class172.aBoolean1321) {
										try {
											GameShell.applet.getAppletContext().showDocument(new URL(GameShell.applet.getCodeBase(), "blank.ws"), "tbi");
										} catch (Exception exception) {
											/* empty */
										}
									}
								}
							}
							if (BuildLocation.LIVE == client.buildLocation) {
								try {
									JavaScriptInterface.callJsMethod("loggedin", GameShell.applet, -26978);
								} catch (Throwable throwable) {
									/* empty */
								}
							}
							if ((Class98_Sub46_Sub20_Sub2.anInt6317 ^ 0xffffffff) == -3) {
								MaxScreenSizePreferenceField.anInt3680 = 11;
							} else {
								MaxScreenSizePreferenceField.anInt3680 = 0;
								Class369.setLoginResponse(2, (byte) -55);
								Class98_Sub44.method1515(2);
								HashTableIterator.setClientState(7, false);
								PacketParser.CURRENT_INCOMING_OPCODE = null;
								return;
							}
						}
						if (MaxScreenSizePreferenceField.anInt3680 == 11) {
							if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 3)) {
								return;
							}
							aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, 3);
							MaxScreenSizePreferenceField.anInt3680 = 12;
						}
						if (MaxScreenSizePreferenceField.anInt3680 == 12) {
							RsBitsBuffers class98_sub22_sub1 = PacketParser.buffer;
							class98_sub22_sub1.position = 0;
							if (class98_sub22_sub1.xzSmart((byte) 54)) {
								if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
									return;
								}
								aa_Sub1.aClass123_3561.read(class98_sub22_sub1.payload, 3, 2047, 1);
							}
							PacketParser.CURRENT_INCOMING_OPCODE = PlayerAppearence.getAll(101)[class98_sub22_sub1.xgSmart(0)];
							Class65.currentPacketSize = class98_sub22_sub1.readShort((byte) 127);
							MaxScreenSizePreferenceField.anInt3680 = 10;
						}
						if (MaxScreenSizePreferenceField.anInt3680 == 10) {
							if (aa_Sub1.aClass123_3561.isBuffered(-1949, Class65.currentPacketSize)) {
								aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, Class65.currentPacketSize);
								PacketParser.buffer.position = 0;
								int i_51_ = Class65.currentPacketSize;
								MaxScreenSizePreferenceField.anInt3680 = 0;
								Class369.setLoginResponse(2, (byte) -55);
								RotatingSpriteLoadingScreenElement.method2247(-104);
								Mob.method3964(PacketParser.buffer, 15811816);
								Class160.centreX = -1;
								if (PacketParser.CURRENT_INCOMING_OPCODE == Class150.aClass58_1212) {
									Class98_Sub36.method1459(-1048016408);
								} else {
									Class98_Sub41.decodeMapArea(68);
								}
								if ((i_51_ ^ 0xffffffff) != (PacketParser.buffer.position ^ 0xffffffff)) {
									throw new RuntimeException("lswp pos:" + PacketParser.buffer.position + " psize:" + i_51_);
								}
								PacketParser.CURRENT_INCOMING_OPCODE = null;
							}
						} else if (MaxScreenSizePreferenceField.anInt3680 == 14) {
							if ((Class65.currentPacketSize ^ 0xffffffff) == 1) {
								if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 2)) {
									return;
								}
								aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, 2);
								PacketParser.buffer.position = 0;
								Class65.currentPacketSize = PacketParser.buffer.readShort((byte) 127);
							}
							if (aa_Sub1.aClass123_3561.isBuffered(-1949, Class65.currentPacketSize)) {
								aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, Class65.currentPacketSize);
								PacketParser.buffer.position = 0;
								MaxScreenSizePreferenceField.anInt3680 = 0;
								int i_52_ = Class65.currentPacketSize;
								Class369.setLoginResponse(15, (byte) -55);
								ClanChatMember.method2412(-108);
								Mob.method3964(PacketParser.buffer, 15811816);
								if ((i_52_ ^ 0xffffffff) != (PacketParser.buffer.position ^ 0xffffffff)) {
									throw new RuntimeException("lswpr pos:" + PacketParser.buffer.position + " psize:" + i_52_);
								}
								PacketParser.CURRENT_INCOMING_OPCODE = null;
							}
						}
					}
				} catch (IOException ioexception) {
					if (aa_Sub1.aClass123_3561 != null) {
						aa_Sub1.aClass123_3561.close(-86);
						aa_Sub1.aClass123_3561 = null;
					}
					if ((Class151_Sub9.anInt5020 ^ 0xffffffff) <= -4) {
						MaxScreenSizePreferenceField.anInt3680 = 0;
						Class369.setLoginResponse(-4, (byte) -55);
						Class98_Sub12.method1130(27089);
					} else {
						if ((Class98_Sub46_Sub20_Sub2.anInt6317 ^ 0xffffffff) != -3) {
							client.lobbyServer.rotateConnectionMethod(0);
						} else {
							client.server.rotateConnectionMethod(0);
						}
						Class151_Sub9.anInt5020++;
						MaxScreenSizePreferenceField.anInt3680 = 1;
						VarPlayerDefinitionParser.anInt1087 = 0;
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.A(" + i + ')');
		}
	}

	private boolean				aBoolean5401;
	private Class42_Sub1_Sub1	aClass42_Sub1_Sub1_5403;
	private Class42_Sub1_Sub1	aClass42_Sub1_Sub1_5407;
	private OpenGlToolkit		aHa_Sub1_5406;
	private int					anInt5400	= 0;
	private int					anInt5402	= 0;
	private int					anInt5404;

	private int					anInt5405;

	private int					anInt5408;

	Class332_Sub1(OpenGlToolkit var_ha_Sub1, int i, int i_123_, boolean bool) {
		aBoolean5401 = false;
		anInt5405 = 0;
		anInt5404 = 0;
		anInt5408 = 0;
		try {
			aHa_Sub1_5406 = var_ha_Sub1;
			aClass42_Sub1_Sub1_5407 = Class82.method823(i_123_, var_ha_Sub1, i, -97, bool ? 6408 : 6407);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_123_ + ',' + bool + ')');
		}
	}

	Class332_Sub1(OpenGlToolkit var_ha_Sub1, int i, int i_124_, int i_125_, int i_126_) {
		aBoolean5401 = false;
		anInt5405 = 0;
		anInt5404 = 0;
		anInt5408 = 0;
		try {
			aHa_Sub1_5406 = var_ha_Sub1;
			aClass42_Sub1_Sub1_5407 = aa_Sub1.method153((byte) -125, i_126_, var_ha_Sub1, i_125_, i, i_124_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_124_ + ',' + i_125_ + ',' + i_126_ + ')');
		}
	}

	Class332_Sub1(OpenGlToolkit var_ha_Sub1, int i, int i_127_, int[] is, int i_128_, int i_129_) {
		aBoolean5401 = false;
		anInt5405 = 0;
		anInt5404 = 0;
		anInt5408 = 0;
		try {
			aHa_Sub1_5406 = var_ha_Sub1;
			aClass42_Sub1_Sub1_5407 = Class246_Sub9.method3136(var_ha_Sub1, false, is, i, (byte) 120, i_129_, i_128_, i_127_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_127_ + ',' + (is != null ? "{...}" : "null") + ',' + i_128_ + ',' + i_129_ + ')');
		}
	}

	@Override
	public final void draw(int i, int i_18_, int i_19_, int i_20_, int i_21_) {
		try {
			aClass42_Sub1_Sub1_5407.method372(-28003, false);
			aHa_Sub1_5406.method1829((byte) -104);
			aHa_Sub1_5406.setBlendMode((byte) -110, i_21_);
			OpenGL.glColor4ub((byte) (i_20_ >> -599196016), (byte) (i_20_ >> 983865512), (byte) i_20_, (byte) (i_20_ >> -1342849480));
			i_18_ += anInt5405;
			i += anInt5404;
			if (aClass42_Sub1_Sub1_5403 != null) {
				method3750(false, i_19_);
				aClass42_Sub1_Sub1_5403.method372(-28003, false);
				OpenGL.glBegin(7);
				OpenGL.glMultiTexCoord2f(33985, 0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
				OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
				OpenGL.glVertex2i(i, i_18_);
				OpenGL.glMultiTexCoord2f(33985, 0.0F, 0.0F);
				OpenGL.glTexCoord2f(0.0F, 0.0F);
				OpenGL.glVertex2i(i, i_18_ + aClass42_Sub1_Sub1_5407.anInt6204);
				OpenGL.glMultiTexCoord2f(33985, aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
				OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
				OpenGL.glVertex2i(aClass42_Sub1_Sub1_5407.anInt6207 + i, i_18_ + aClass42_Sub1_Sub1_5407.anInt6204);
				OpenGL.glMultiTexCoord2f(33985, aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
				OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
				OpenGL.glVertex2i(aClass42_Sub1_Sub1_5407.anInt6207 + i, i_18_);
				OpenGL.glEnd();
				method3752(-22);
			} else {
				aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5407);
				aHa_Sub1_5406.method1896(260, i_19_);
				OpenGL.glBegin(7);
				OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
				OpenGL.glVertex2i(i, i_18_);
				OpenGL.glTexCoord2f(0.0F, 0.0F);
				OpenGL.glVertex2i(i, aClass42_Sub1_Sub1_5407.anInt6204 + i_18_);
				OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
				OpenGL.glVertex2i(aClass42_Sub1_Sub1_5407.anInt6207 + i, aClass42_Sub1_Sub1_5407.anInt6204 + i_18_);
				OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
				OpenGL.glVertex2i(aClass42_Sub1_Sub1_5407.anInt6207 + i, i_18_);
				OpenGL.glEnd();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.E(" + i + ',' + i_18_ + ',' + i_19_ + ',' + i_20_ + ',' + i_21_ + ')');
		}
	}

	@Override
	public final int getHeight() {
		try {
			return aClass42_Sub1_Sub1_5407.anInt6204;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.U(" + ')');
		}
	}

	@Override
	public final int getRenderHeight() {
		try {
			return anInt5405 + aClass42_Sub1_Sub1_5407.anInt6204 + anInt5400;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.T(" + ')');
		}
	}

	@Override
	public final int getRenderWidth() {
		try {
			return aClass42_Sub1_Sub1_5407.anInt6207 - (-anInt5404 - anInt5402);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.O(" + ')');
		}
	}

	@Override
	public final int getWidth() {
		try {
			return aClass42_Sub1_Sub1_5407.anInt6207;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.AA(" + ')');
		}
	}

	@Override
	public final void method3728(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		try {
			int i_6_ = i_1_ + i;
			int i_7_ = i_0_ - -i_2_;
			aClass42_Sub1_Sub1_5407.method372(-28003, false);
			aHa_Sub1_5406.method1829((byte) -117);
			aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5407);
			aHa_Sub1_5406.setBlendMode((byte) -90, i_5_);
			aHa_Sub1_5406.method1896(260, i_3_);
			OpenGL.glColor4ub((byte) (i_4_ >> 1904370224), (byte) (i_4_ >> 1918206088), (byte) i_4_, (byte) (i_4_ >> -31071336));
			if (!aClass42_Sub1_Sub1_5407.aBoolean6211 || aBoolean5401) {
				OpenGL.glPushMatrix();
				OpenGL.glTranslatef(anInt5404, anInt5405, 0.0F);
				int i_8_ = getRenderWidth();
				int i_9_ = getRenderHeight();
				int i_10_ = i_0_ + aClass42_Sub1_Sub1_5407.anInt6204;
				OpenGL.glBegin(7);
				int i_11_ = i_0_;
				while ((i_10_ ^ 0xffffffff) >= (i_7_ ^ 0xffffffff)) {
					int i_12_ = aClass42_Sub1_Sub1_5407.anInt6207 + i;
					int i_13_ = i;
					for (/**/; (i_12_ ^ 0xffffffff) >= (i_6_ ^ 0xffffffff); i_12_ += i_8_) {
						OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
						OpenGL.glVertex2i(i_13_, i_11_);
						OpenGL.glTexCoord2f(0.0F, 0.0F);
						OpenGL.glVertex2i(i_13_, i_10_);
						OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
						OpenGL.glVertex2i(i_12_, i_10_);
						OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
						OpenGL.glVertex2i(i_12_, i_11_);
						i_13_ += i_8_;
					}
					if (i_13_ < i_6_) {
						float f = (i_6_ - i_13_) * aClass42_Sub1_Sub1_5407.aFloat6205 / aClass42_Sub1_Sub1_5407.anInt6207;
						OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
						OpenGL.glVertex2i(i_13_, i_11_);
						OpenGL.glTexCoord2f(0.0F, 0.0F);
						OpenGL.glVertex2i(i_13_, i_10_);
						OpenGL.glTexCoord2f(f, 0.0F);
						OpenGL.glVertex2i(i_6_, i_10_);
						OpenGL.glTexCoord2f(f, aClass42_Sub1_Sub1_5407.aFloat6209);
						OpenGL.glVertex2i(i_6_, i_11_);
					}
					i_10_ += i_9_;
					i_11_ += i_9_;
				}
				if ((i_7_ ^ 0xffffffff) < (i_11_ ^ 0xffffffff)) {
					float f = (aClass42_Sub1_Sub1_5407.anInt6204 + i_11_ + -i_7_) * aClass42_Sub1_Sub1_5407.aFloat6209 / aClass42_Sub1_Sub1_5407.anInt6204;
					int i_14_ = aClass42_Sub1_Sub1_5407.anInt6207 + i;
					int i_15_ = i;
					while (i_14_ <= i_6_) {
						OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
						OpenGL.glVertex2i(i_15_, i_11_);
						OpenGL.glTexCoord2f(0.0F, f);
						OpenGL.glVertex2i(i_15_, i_7_);
						OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, f);
						OpenGL.glVertex2i(i_14_, i_7_);
						OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
						OpenGL.glVertex2i(i_14_, i_11_);
						i_14_ += i_8_;
						i_15_ += i_8_;
					}
					if ((i_15_ ^ 0xffffffff) > (i_6_ ^ 0xffffffff)) {
						float f_16_ = (i_6_ + -i_15_) * aClass42_Sub1_Sub1_5407.aFloat6205 / aClass42_Sub1_Sub1_5407.anInt6207;
						OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
						OpenGL.glVertex2i(i_15_, i_11_);
						OpenGL.glTexCoord2f(0.0F, f);
						OpenGL.glVertex2i(i_15_, i_7_);
						OpenGL.glTexCoord2f(f_16_, f);
						OpenGL.glVertex2i(i_6_, i_7_);
						OpenGL.glTexCoord2f(f_16_, aClass42_Sub1_Sub1_5407.aFloat6209);
						OpenGL.glVertex2i(i_6_, i_11_);
					}
				}
				OpenGL.glEnd();
				OpenGL.glPopMatrix();
			} else {
				float f = i_2_ * aClass42_Sub1_Sub1_5407.aFloat6209 / aClass42_Sub1_Sub1_5407.anInt6204;
				float f_17_ = aClass42_Sub1_Sub1_5407.aFloat6205 * i_1_ / aClass42_Sub1_Sub1_5407.anInt6207;
				OpenGL.glBegin(7);
				OpenGL.glTexCoord2f(0.0F, f);
				OpenGL.glVertex2i(i, i_0_);
				OpenGL.glTexCoord2f(0.0F, 0.0F);
				OpenGL.glVertex2i(i, i_7_);
				OpenGL.glTexCoord2f(f_17_, 0.0F);
				OpenGL.glVertex2i(i_6_, i_7_);
				OpenGL.glTexCoord2f(f_17_, f);
				OpenGL.glVertex2i(i_6_, i_0_);
				OpenGL.glEnd();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.K(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ')');
		}
	}

	@Override
	public final void method3729(int i, int i_102_, RtInterfaceClip var_aa, int i_103_, int i_104_) {
		try {
			aa_Sub3 var_aa_Sub3 = (aa_Sub3) var_aa;
			Class42_Sub1_Sub1 class42_sub1_sub1 = var_aa_Sub3.aClass42_Sub1_Sub1_3568;
			aClass42_Sub1_Sub1_5407.method372(-28003, false);
			aHa_Sub1_5406.method1829((byte) -80);
			aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5407);
			aHa_Sub1_5406.method1896(260, 1);
			aHa_Sub1_5406.method1845(1, 847872872);
			aHa_Sub1_5406.setActiveTexture(1, class42_sub1_sub1);
			aHa_Sub1_5406.method1899(8448, 8960, 7681);
			aHa_Sub1_5406.method1840(0, 768, 102, 34168);
			aHa_Sub1_5406.setBlendMode((byte) -59, 1);
			i += anInt5404;
			i_102_ += anInt5405;
			int i_105_ = i - -aClass42_Sub1_Sub1_5407.anInt6207;
			int i_106_ = i_102_ - -aClass42_Sub1_Sub1_5407.anInt6204;
			float f = class42_sub1_sub1.aFloat6205 / class42_sub1_sub1.anInt6207;
			float f_107_ = class42_sub1_sub1.aFloat6209 / class42_sub1_sub1.anInt6204;
			float f_108_ = (i + -i_103_) * f;
			float f_109_ = (-i_103_ + i_105_) * f;
			float f_110_ = -((-i_104_ + i_102_) * f_107_) + class42_sub1_sub1.aFloat6209;
			float f_111_ = class42_sub1_sub1.aFloat6209 - f_107_ * (-i_104_ + i_106_);
			OpenGL.glBegin(7);
			OpenGL.glColor3f(1.0F, 1.0F, 1.0F);
			OpenGL.glMultiTexCoord2f(33984, 0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
			OpenGL.glMultiTexCoord2f(33985, f_108_, f_110_);
			OpenGL.glVertex2i(i, i_102_);
			OpenGL.glMultiTexCoord2f(33984, 0.0F, 0.0F);
			OpenGL.glMultiTexCoord2f(33985, f_108_, f_111_);
			OpenGL.glVertex2i(i, aClass42_Sub1_Sub1_5407.anInt6204 + i_102_);
			OpenGL.glMultiTexCoord2f(33984, aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
			OpenGL.glMultiTexCoord2f(33985, f_109_, f_111_);
			OpenGL.glVertex2i(aClass42_Sub1_Sub1_5407.anInt6207 + i, aClass42_Sub1_Sub1_5407.anInt6204 + i_102_);
			OpenGL.glMultiTexCoord2f(33984, aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
			OpenGL.glMultiTexCoord2f(33985, f_109_, f_110_);
			OpenGL.glVertex2i(i - -aClass42_Sub1_Sub1_5407.anInt6207, i_102_);
			OpenGL.glEnd();
			aHa_Sub1_5406.method1840(0, 768, 87, 5890);
			aHa_Sub1_5406.method1896(260, 0);
			aHa_Sub1_5406.setActiveTexture(1, null);
			aHa_Sub1_5406.method1845(0, 847872872);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.D(" + i + ',' + i_102_ + ',' + (var_aa != null ? "{...}" : "null") + ',' + i_103_ + ',' + i_104_ + ')');
		}
	}

	@Override
	public final void method3733(float f, float f_53_, float f_54_, float f_55_, float f_56_, float f_57_, int i, int i_58_, int i_59_, int i_60_) {
		try {
			if (aBoolean5401) {
				float f_61_ = getRenderWidth();
				float f_62_ = getRenderHeight();
				float f_63_ = (-f + f_54_) / f_61_;
				float f_64_ = (f_55_ - f_53_) / f_61_;
				float f_65_ = (-f + f_56_) / f_62_;
				float f_66_ = (-f_53_ + f_57_) / f_62_;
				float f_67_ = f_65_ * anInt5405;
				float f_68_ = f_66_ * anInt5405;
				float f_69_ = anInt5404 * f_63_;
				float f_70_ = anInt5404 * f_64_;
				float f_71_ = -f_63_ * anInt5402;
				float f_72_ = anInt5402 * -f_64_;
				float f_73_ = anInt5400 * -f_65_;
				float f_74_ = -f_66_ * anInt5400;
				f_53_ = f_70_ + f_53_ + f_68_;
				f_55_ = f_68_ + (f_55_ + f_72_);
				f = f_69_ + f + f_67_;
				f_56_ = f_73_ + (f_56_ + f_69_);
				f_54_ = f_67_ + (f_71_ + f_54_);
				f_57_ = f_74_ + (f_70_ + f_57_);
			}
			float f_75_ = f_56_ + (-f + f_54_);
			float f_76_ = f_55_ + (-f_53_ + f_57_);
			aClass42_Sub1_Sub1_5407.method372(-28003, (i_60_ & 0x1) != 0);
			aHa_Sub1_5406.method1829((byte) -114);
			aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5407);
			aHa_Sub1_5406.setBlendMode((byte) -117, i_59_);
			aHa_Sub1_5406.method1896(260, i);
			OpenGL.glColor4ub((byte) (i_58_ >> 651809520), (byte) (i_58_ >> -1535919032), (byte) i_58_, (byte) (i_58_ >> 1835121464));
			OpenGL.glBegin(7);
			OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
			OpenGL.glVertex2f(f, f_53_);
			OpenGL.glTexCoord2f(0.0F, 0.0F);
			OpenGL.glVertex2f(f_56_, f_57_);
			OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
			OpenGL.glVertex2f(f_75_, f_76_);
			OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
			OpenGL.glVertex2f(f_54_, f_55_);
			OpenGL.glEnd();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.BA(" + f + ',' + f_53_ + ',' + f_54_ + ',' + f_55_ + ',' + f_56_ + ',' + f_57_ + ',' + i + ',' + i_58_ + ',' + i_59_ + ',' + i_60_ + ')');
		}
	}

	@Override
	public final void method3736(int i, int i_112_, int i_113_, int i_114_, int i_115_, int i_116_) {
		try {
			if (aHa_Sub1_5406.aBoolean4367) {
				int[] is = aHa_Sub1_5406.getPixels(i_115_, i_116_, i_113_, i_114_);
				if (is != null) {
					for (int i_117_ = 0; (is.length ^ 0xffffffff) < (i_117_ ^ 0xffffffff); i_117_++) {
						is[i_117_] = Class41.or(is[i_117_], -16777216);
					}
					method3754(i, i_112_, i_113_, i_114_, is, 0, i_113_);
				}
			} else {
				aClass42_Sub1_Sub1_5407.method380(i_113_, i, i_112_, i_116_, i_115_, 120, i_114_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.J(" + i + ',' + i_112_ + ',' + i_113_ + ',' + i_114_ + ',' + i_115_ + ',' + i_116_ + ')');
		}
	}

	@Override
	public final void method3740(int i, int i_34_, int i_35_, int i_36_) {
		try {
			anInt5404 = i;
			anInt5405 = i_34_;
			anInt5402 = i_35_;
			anInt5400 = i_36_;
			aBoolean5401 = anInt5404 != 0 || anInt5405 != 0 || anInt5402 != 0 || anInt5400 != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.Q(" + i + ',' + i_34_ + ',' + i_35_ + ',' + i_36_ + ')');
		}
	}

	@Override
	public final void method3741(int[] is) {
		try {
			is[0] = anInt5404;
			is[3] = anInt5400;
			is[1] = anInt5405;
			is[2] = anInt5402;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.S(" + (is != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method3742(int i, int i_38_, int i_39_) {
		try {
			OpenGL.glPixelTransferf(3348, 0.5F);
			OpenGL.glPixelTransferf(3349, 0.499F);
			OpenGL.glPixelTransferf(3352, 0.5F);
			OpenGL.glPixelTransferf(3353, 0.499F);
			OpenGL.glPixelTransferf(3354, 0.5F);
			OpenGL.glPixelTransferf(3355, 0.499F);
			aClass42_Sub1_Sub1_5403 = aa_Sub1.method153((byte) 30, aClass42_Sub1_Sub1_5407.anInt6204, aHa_Sub1_5406, aClass42_Sub1_Sub1_5407.anInt6207, i, i_38_);
			anInt5408 = i_39_;
			OpenGL.glPixelTransferf(3348, 1.0F);
			OpenGL.glPixelTransferf(3349, 0.0F);
			OpenGL.glPixelTransferf(3352, 1.0F);
			OpenGL.glPixelTransferf(3353, 0.0F);
			OpenGL.glPixelTransferf(3354, 1.0F);
			OpenGL.glPixelTransferf(3355, 0.0F);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.F(" + i + ',' + i_38_ + ',' + i_39_ + ')');
		}
	}

	@Override
	public final void method3745(int i, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_, int i_28_) {
		try {
			aClass42_Sub1_Sub1_5407.method372(-28003, (i_28_ & 0x1) != 0);
			aHa_Sub1_5406.method1829((byte) -126);
			aHa_Sub1_5406.setBlendMode((byte) -75, i_27_);
			OpenGL.glColor4ub((byte) (i_26_ >> -1746196464), (byte) (i_26_ >> 1440844808), (byte) i_26_, (byte) (i_26_ >> -2121300072));
			if (!aBoolean5401) {
				if (aClass42_Sub1_Sub1_5403 != null) {
					method3750(false, i_25_);
					aClass42_Sub1_Sub1_5403.method372(-28003, true);
					OpenGL.glBegin(7);
					OpenGL.glMultiTexCoord2f(33985, 0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glVertex2i(i, i_22_);
					OpenGL.glMultiTexCoord2f(33985, 0.0F, 0.0F);
					OpenGL.glTexCoord2f(0.0F, 0.0F);
					OpenGL.glVertex2i(i, i_22_ - -i_24_);
					OpenGL.glMultiTexCoord2f(33985, aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
					OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
					OpenGL.glVertex2i(i_23_ + i, i_24_ + i_22_);
					OpenGL.glMultiTexCoord2f(33985, aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glVertex2i(i_23_ + i, i_22_);
					OpenGL.glEnd();
					method3752(-119);
				} else {
					aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5407);
					aHa_Sub1_5406.method1896(260, i_25_);
					OpenGL.glBegin(7);
					OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glVertex2i(i, i_22_);
					OpenGL.glTexCoord2f(0.0F, 0.0F);
					OpenGL.glVertex2i(i, i_24_ + i_22_);
					OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
					OpenGL.glVertex2i(i + i_23_, i_24_ + i_22_);
					OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glVertex2i(i_23_ + i, i_22_);
					OpenGL.glEnd();
				}
			} else {
				float f = (float) i_23_ / (float) getRenderWidth();
				float f_29_ = (float) i_24_ / (float) getRenderHeight();
				float f_30_ = f * anInt5404 + i;
				float f_31_ = anInt5405 * f_29_ + i_22_;
				float f_32_ = f_30_ + aClass42_Sub1_Sub1_5407.anInt6207 * f;
				float f_33_ = f_31_ + f_29_ * aClass42_Sub1_Sub1_5407.anInt6204;
				if (aClass42_Sub1_Sub1_5403 != null) {
					method3750(false, i_25_);
					aClass42_Sub1_Sub1_5403.method372(-28003, true);
					OpenGL.glBegin(7);
					OpenGL.glMultiTexCoord2f(33985, 0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glVertex2f(f_30_, f_31_);
					OpenGL.glMultiTexCoord2f(33985, 0.0F, 0.0F);
					OpenGL.glTexCoord2f(0.0F, 0.0F);
					OpenGL.glVertex2f(f_30_, f_33_);
					OpenGL.glMultiTexCoord2f(33985, aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
					OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
					OpenGL.glVertex2f(f_32_, f_33_);
					OpenGL.glMultiTexCoord2f(33985, aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glVertex2f(f_32_, f_31_);
					OpenGL.glEnd();
					method3752(-69);
				} else {
					aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5407);
					aHa_Sub1_5406.method1896(260, i_25_);
					OpenGL.glBegin(7);
					OpenGL.glTexCoord2f(0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glVertex2f(f_30_, f_31_);
					OpenGL.glTexCoord2f(0.0F, 0.0F);
					OpenGL.glVertex2f(f_30_, f_33_);
					OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
					OpenGL.glVertex2f(f_32_, f_33_);
					OpenGL.glTexCoord2f(aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
					OpenGL.glVertex2f(f_32_, f_31_);
					OpenGL.glEnd();
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.C(" + i + ',' + i_22_ + ',' + i_23_ + ',' + i_24_ + ',' + i_25_ + ',' + i_26_ + ',' + i_27_ + ',' + i_28_ + ')');
		}
	}

	@Override
	public final void method3747(float f, float f_77_, float f_78_, float f_79_, float f_80_, float f_81_, int i, RtInterfaceClip var_aa, int i_82_, int i_83_) {
		try {
			Class42_Sub1_Sub1 class42_sub1_sub1 = ((aa_Sub3) var_aa).aClass42_Sub1_Sub1_3568;
			if (aBoolean5401) {
				float f_84_ = getRenderWidth();
				float f_85_ = getRenderHeight();
				float f_86_ = (f_78_ - f) / f_84_;
				float f_87_ = (-f_77_ + f_79_) / f_84_;
				float f_88_ = (f_80_ - f) / f_85_;
				float f_89_ = (-f_77_ + f_81_) / f_85_;
				float f_90_ = anInt5405 * f_88_;
				float f_91_ = anInt5405 * f_89_;
				float f_92_ = f_86_ * anInt5404;
				float f_93_ = anInt5404 * f_87_;
				float f_94_ = anInt5402 * -f_86_;
				float f_95_ = anInt5402 * -f_87_;
				float f_96_ = -f_88_ * anInt5400;
				float f_97_ = anInt5400 * -f_89_;
				f = f + f_92_ + f_90_;
				f_79_ = f_95_ + f_79_ + f_91_;
				f_80_ = f_92_ + f_80_ + f_96_;
				f_78_ = f_78_ + f_94_ + f_90_;
				f_77_ = f_91_ + (f_93_ + f_77_);
				f_81_ = f_97_ + (f_81_ + f_93_);
			}
			float f_98_ = f_78_ - f + f_80_;
			float f_99_ = f_79_ + (f_81_ - f_77_);
			aClass42_Sub1_Sub1_5407.method372(-28003, (i & 0x1 ^ 0xffffffff) != -1);
			aHa_Sub1_5406.method1829((byte) -98);
			aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5407);
			aHa_Sub1_5406.method1896(260, 1);
			aHa_Sub1_5406.method1845(1, 847872872);
			aHa_Sub1_5406.setActiveTexture(1, class42_sub1_sub1);
			aHa_Sub1_5406.method1899(8448, 8960, 7681);
			aHa_Sub1_5406.method1840(0, 768, -65, 34168);
			aHa_Sub1_5406.setBlendMode((byte) -24, 1);
			float f_100_ = class42_sub1_sub1.aFloat6205 / class42_sub1_sub1.anInt6207;
			float f_101_ = class42_sub1_sub1.aFloat6209 / class42_sub1_sub1.anInt6204;
			OpenGL.glBegin(7);
			OpenGL.glColor3f(1.0F, 1.0F, 1.0F);
			OpenGL.glMultiTexCoord2f(33984, 0.0F, aClass42_Sub1_Sub1_5407.aFloat6209);
			OpenGL.glMultiTexCoord2f(33985, f_100_ * (-i_82_ + f), class42_sub1_sub1.aFloat6209 - f_101_ * (f_77_ - i_83_));
			OpenGL.glVertex2f(f, f_77_);
			OpenGL.glMultiTexCoord2f(33984, 0.0F, 0.0F);
			OpenGL.glMultiTexCoord2f(33985, (f_80_ - i_82_) * f_100_, -(f_101_ * (-i_83_ + f_81_)) + class42_sub1_sub1.aFloat6209);
			OpenGL.glVertex2f(f_80_, f_81_);
			OpenGL.glMultiTexCoord2f(33984, aClass42_Sub1_Sub1_5407.aFloat6205, 0.0F);
			OpenGL.glMultiTexCoord2f(33985, (-i_82_ + f_98_) * f_100_, -((f_99_ - i_83_) * f_101_) + class42_sub1_sub1.aFloat6209);
			OpenGL.glVertex2f(f_98_, f_99_);
			OpenGL.glMultiTexCoord2f(33984, aClass42_Sub1_Sub1_5407.aFloat6205, aClass42_Sub1_Sub1_5407.aFloat6209);
			OpenGL.glMultiTexCoord2f(33985, f_100_ * (f_78_ - i_82_), -((-i_83_ + f_79_) * f_101_) + class42_sub1_sub1.aFloat6209);
			OpenGL.glVertex2f(f_78_, f_79_);
			OpenGL.glEnd();
			aHa_Sub1_5406.method1840(0, 768, 91, 5890);
			aHa_Sub1_5406.method1896(260, 0);
			aHa_Sub1_5406.setActiveTexture(1, null);
			aHa_Sub1_5406.method1845(0, 847872872);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.L(" + f + ',' + f_77_ + ',' + f_78_ + ',' + f_79_ + ',' + f_80_ + ',' + f_81_ + ',' + i + ',' + (var_aa != null ? "{...}" : "null") + ',' + i_82_ + ',' + i_83_ + ')');
		}
	}

	private final void method3750(boolean bool, int i) {
		try {
			if (bool != false) {
				method3752(-23);
			}
			aHa_Sub1_5406.method1845(1, 847872872);
			aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5407);
			aHa_Sub1_5406.method1899(7681, 8960, aHa_Sub1_5406.method1892(i, 596294056));
			aHa_Sub1_5406.method1840(1, 768, -58, 34167);
			aHa_Sub1_5406.method1886(770, 0, 34200, 34168);
			aHa_Sub1_5406.method1845(0, 847872872);
			aHa_Sub1_5406.setActiveTexture(1, aClass42_Sub1_Sub1_5403);
			aHa_Sub1_5406.method1899(7681, 8960, 34479);
			aHa_Sub1_5406.method1840(1, 768, 122, 34166);
			if ((anInt5408 ^ 0xffffffff) != -1) {
				if ((anInt5408 ^ 0xffffffff) != -2) {
					if ((anInt5408 ^ 0xffffffff) == -3) {
						aHa_Sub1_5406.method1858(0.5F, 1.0F, 0.5F, 0.0F, -89);
					} else if ((anInt5408 ^ 0xffffffff) == -4) {
						aHa_Sub1_5406.method1858(128.5F, 128.5F, 128.5F, 0.0F, -40);
					}
				} else {
					aHa_Sub1_5406.method1858(1.0F, 0.5F, 0.5F, 0.0F, -59);
				}
			} else {
				aHa_Sub1_5406.method1858(0.5F, 0.5F, 1.0F, 0.0F, 108);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.H(" + bool + ',' + i + ')');
		}
	}

	private final void method3752(int i) {
		try {
			aHa_Sub1_5406.method1845(1, 847872872);
			aHa_Sub1_5406.setActiveTexture(1, null);
			aHa_Sub1_5406.method1899(8448, 8960, 8448);
			aHa_Sub1_5406.method1840(1, 768, -54, 34168);
			aHa_Sub1_5406.method1886(770, 0, 34200, 5890);
			aHa_Sub1_5406.method1845(0, 847872872);
			aHa_Sub1_5406.method1840(1, 768, -71, 34168);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.I(" + i + ')');
		}
	}

	private final void method3754(int i, int i_118_, int i_119_, int i_120_, int[] is, int i_121_, int i_122_) {
		try {
			aClass42_Sub1_Sub1_5407.method379(i_120_, is, i_121_, 3656, true, i_118_, i_122_, i_119_, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ol.B(" + i + ',' + i_118_ + ',' + i_119_ + ',' + i_120_ + ',' + (is != null ? "{...}" : "null") + ',' + i_121_ + ',' + i_122_ + ')');
		}
	}
}

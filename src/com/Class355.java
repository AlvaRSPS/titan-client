/* Class355 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;

public final class Class355 {
	public static int anInt3017;

	public static final Class98_Sub44 method3875(int i, Js5 class207, int i_0_) {
		try {
			if (i_0_ > -20) {
				anInt3017 = -75;
			}
			byte[] is = class207.getFile(i, -119);
			if (is == null) {
				return null;
			}
			return new Class98_Sub44(is);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vk.A(" + i + ',' + (class207 != null ? "{...}" : "null") + ',' + i_0_ + ')');
		}
	}
}

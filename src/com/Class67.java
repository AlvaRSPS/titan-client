/* Class67 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.input.RtMouseListener;

public final class Class67 {
	public static boolean	aBoolean520	= false;
	public static int		anInt521;

	public static final void method687(byte i, int i_0_) {
		try {
			synchronized (RtMouseListener.aClass79_2493) {
				RtMouseListener.aClass79_2493.makeSoftReferences((byte) 62, i_0_);
				if (i != 118) {
					aBoolean520 = true;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "eh.C(" + i + ',' + i_0_ + ')');
		}
	}

	public static final void method688(int i, long l, int i_1_, int i_2_) {
		try {
			int i_3_ = (0x7f61d & (int) l) >> 250951790;
			int i_4_ = (int) l >> -1669061484 & 0x3;
			int i_5_ = (int) (l >>> -1972629216) & 0x7fffffff;
			if (i == -23) {
				if (i_3_ == 10 || (i_3_ ^ 0xffffffff) == -12 || (i_3_ ^ 0xffffffff) == -23) {
					GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(i_5_, (byte) 119);
					int i_6_;
					int i_7_;
					do {
						if ((i_4_ ^ 0xffffffff) == -1 || i_4_ == 2) {
							i_6_ = class352.sizeY;
							i_7_ = class352.sizeX;
							if (!GameShell.cleanedStatics) {
								break;
							}
						}
						i_6_ = class352.sizeX;
						i_7_ = class352.sizeY;
					} while (false);
					int i_8_ = class352.anInt2948;
					if (i_4_ != 0) {
						i_8_ = (i_8_ << i_4_ & 0xf) + (i_8_ >> 4 + -i_4_);
					}
					Class76_Sub2.requestFlag(0, i_8_, i_7_, 0, 0, i_2_, i_1_, true, i_6_);
				} else {
					Class76_Sub2.requestFlag(i_4_, 0, 0, i_3_, 0, i_2_, i_1_, true, 0);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "eh.A(" + i + ',' + l + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public static final Billboard load(int billboard, byte dummy) {
		Billboard _billboard = (Billboard) Class246_Sub9.billboardLoader.get(-120, billboard);
		if (_billboard != null) {
			return _billboard;
		}
		byte[] is = NativeToolkit.aClass207_4528.getFile(billboard, 0, false);
		_billboard = new Billboard();
		if (is != null) {
			_billboard.decode((byte) 77, new RSByteBuffer(is), billboard);
		}
		Class246_Sub9.billboardLoader.put(billboard, _billboard, (byte) -80);
		return _billboard;
	}
}

/* Class295 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.File;
import java.io.FileOutputStream;

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.MemoryCacheNodeFactory;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Manager;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.definition.parser.StructsDefinitionParser;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

import jagex3.jagmisc.jagmisc;

public final class Class295 {
	public static int[] anIntArray2409 = new int[2];

	public static final void handleClientCommand(String string, boolean bool, boolean bool_37_, byte i) {
		do {
			try {
				if (string.equalsIgnoreCase("commands") || string.equalsIgnoreCase("help")) {
					Cacheable.write("commands - This command", 114);
					Cacheable.write("cls - Clear console", i ^ 0x28);
					Cacheable.write("displayfps - Toggle FPS and other information", 98);
					Cacheable.write("renderer - Print graphics renderer information", 93);
					Cacheable.write("heap - Print java memory information", -83);
					Cacheable.write("info - Print mob information", 80);
					break;
				}
				if (string.equalsIgnoreCase("cls")) {
					NativeProgressMonitor.anInt3395 = 0;
					Class98_Sub28.anInt4080 = 0;
					break;
				}
				if (string.equalsIgnoreCase("displayfps")) {
					client.displayFps = !client.displayFps;
					if (client.displayFps) {
						Cacheable.write("FPS on", i + -72);
					} else {
						Cacheable.write("FPS off", -101);
						break;
					}
					break;
				}
				if (string.startsWith("bloom")) {
					System.out.println("Reached");
					boolean bool_44_ = client.graphicsToolkit.canEnableBloom();
					if (!Class98_Sub5_Sub1.method966(29089, !bool_44_)) {
						Cacheable.write("Failed to enable bloom", 126);
					} else {
						if (!bool_44_) {
							Cacheable.write("Bloom enabled", 46);
						} else {
							Cacheable.write("Bloom disabled", -85);
							break;
						}
						break;
					}
					break;
				}
				if (string.equalsIgnoreCase("info")) {
					// if ((Class272.anInt2038 - -((((Animable)
					// Class87.localPlayer).xCoord >> 1513969193)) < 8))
					// break;
					client.hideInfo = !client.hideInfo;
					if (client.hideInfo) {
						Cacheable.write("Info Hidden", -1);
					} else {
						Cacheable.write("Info Unhidden", -1);
					}
				}
				if (string.equalsIgnoreCase("toggleGUI")) {
					client.HideGUI = !client.HideGUI;
					if (client.HideGUI) {
						Cacheable.write("GUI Hidden", -1);
					} else {
						Cacheable.write("GUI Unhidden", -1);
					}
					break;
				}
				if (string.equals("renderer")) {
					Class62 class62 = client.graphicsToolkit.method1799();
					Cacheable.write("Vendor: " + class62.anInt484, -123);
					Cacheable.write("Name: " + class62.aString489, i ^ ~0x3e);
					Cacheable.write("Version: " + class62.anInt483, 52);
					Cacheable.write("Device: " + class62.aString488, -115);
					Cacheable.write("Driver Version: " + class62.aLong485, -82);
					break;
				}
				if (string.equals("heap")) {
					Cacheable.write("Heap: " + GameShell.maxHeap + "MB", i ^ ~0x2a);
					break;
				}
			} catch (Exception exception) {
				Cacheable.write(TextResources.ERROR_EXECUTING_COMMAND.getText(client.gameLanguage, (byte) 25), 107);
				break;
			}
			if (BuildLocation.LIVE != client.buildLocation || (LoadingScreenSequence.rights ^ 0xffffffff) <= -3) {
				if (string.equalsIgnoreCase("errortest")) {
					throw new RuntimeException();
				}
				if (string.equals("nativememerror")) {
					throw new OutOfMemoryError("native(MPR");
				}
				try {
					if (string.equalsIgnoreCase("printfps")) {
						Cacheable.write("FPS: " + GameShell.fpsValue, -74);
						break;
					}
					if (string.equalsIgnoreCase("occlude")) {
						RtMouseEvent.occlusion = !RtMouseEvent.occlusion;
						if (RtMouseEvent.occlusion) {
							Cacheable.write("Occlsion now on!", i ^ 0xd);
						} else {
							Cacheable.write("Occlsion now off!", 58);
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("fpson")) {
						client.displayFps = true;
						Cacheable.write("fps debug enabled", -85);
						break;
					}
					if (string.equalsIgnoreCase("fpsoff")) {
						client.displayFps = false;
						Cacheable.write("fps debug disabled", -74);
						break;
					}
					if (string.equals("systemmem")) {
						try {
							Cacheable.write("System memory: " + jagmisc.getAvailablePhysicalMemory() / 1048576L + "/" + Exception_Sub1.platformInformation.totalPhysicMemory + "Mb", 108);
						} catch (Throwable throwable) {
							/* empty */
						}
						break;
					}
					if (string.equalsIgnoreCase("cleartext")) {
						TexturesPreferenceField.aClass218_3694.clear(i ^ 0x12);
						Cacheable.write("Text coords cleared", 95);
						break;
					}
					if (string.equalsIgnoreCase("gc")) {
						Class27.method294(true);
						for (int i_38_ = 0; (i_38_ ^ 0xffffffff) > -11; i_38_++) {
							System.gc();
						}
						Runtime runtime = Runtime.getRuntime();
						int i_39_ = (int) ((runtime.totalMemory() - runtime.freeMemory()) / 1024L);
						Cacheable.write("mem=" + i_39_ + "k", i + -235);
						break;
					}
					if (string.equalsIgnoreCase("compact")) {
						Class27.method294(true);
						for (int i_40_ = 0; i_40_ < 10; i_40_++) {
							System.gc();
						}
						Runtime runtime = Runtime.getRuntime();
						int i_41_ = (int) ((runtime.totalMemory() - runtime.freeMemory()) / 1024L);
						Cacheable.write("Memory before cleanup=" + i_41_ + "k", -95);
						MemoryCacheNodeFactory.method2727(24);
						Class27.method294(true);
						for (int i_42_ = 0; i_42_ < 10; i_42_++) {
							System.gc();
						}
						i_41_ = (int) ((runtime.totalMemory() - runtime.freeMemory()) / 1024L);
						Cacheable.write("Memory after cleanup=" + i_41_ + "k", 104);
						break;
					}
					if (string.equalsIgnoreCase("unloadnatives")) {
						Cacheable.write(AdvancedMemoryCache.unloadNatives(5) ? "Libraries unloaded" : "Library unloading failed!", -104);
						break;
					}
					if (string.equalsIgnoreCase("clientdrop")) {
						Cacheable.write("Dropped client connection", -67);
						if ((client.clientState ^ 0xffffffff) != -11) {
							if ((client.clientState ^ 0xffffffff) == -12) {
								Class76_Sub9.aBoolean3788 = true;
							}
						} else {
							Canvas_Sub1.method118((byte) 104);
						}
						break;
					}
					if (string.equalsIgnoreCase("rotateconnectmethods")) {
						client.server.rotateConnectionMethod(0);
						Cacheable.write("Rotated connection methods", i + -206);
						break;
					}
					if (string.equalsIgnoreCase("clientjs5drop")) {
						client.js5Client.requestClientJs5Drop(-45);
						Cacheable.write("Dropped client js5 net queue", i + -220);
						break;
					}
					if (string.equalsIgnoreCase("serverjs5drop")) {
						client.js5Client.requestServerDrop(i ^ 0x7106);
						Cacheable.write("Dropped server js5 net queue", -84);
						break;
					}
					if (string.equalsIgnoreCase("breakcon")) {
						GameShell.signLink.startTimeOutTimer(i ^ 0x75);
						aa_Sub1.aClass123_3561.setDummyMode(-1);
						client.js5Client.setDummyStreamMode(0);
						Cacheable.write("Breaking new connections for 5 seconds", -110);
						break;
					}
					if (string.equalsIgnoreCase("rebuild")) {
						Class98_Sub10.method999((byte) -59);
						InputStream_Sub2.method124(-109);
						Cacheable.write("Rebuilding map", 107);
						break;
					}
					if (string.equalsIgnoreCase("rebuildprofile")) {
						StreamHandler.aLong1011 = TimeTools.getCurrentTime(-47);
						Class270.aBoolean2031 = true;
						Class98_Sub10.method999((byte) -70);
						InputStream_Sub2.method124(-115);
						Cacheable.write("Rebuilding map (with profiling)", -119);
						break;
					}
					if (string.equalsIgnoreCase("wm1")) {
						Class98_Sub16.setWindowMode(1, -1, 3, -1, false);
						if (OpenGlModelRenderer.getWindowMode((byte) 124) != 1) {
							Cacheable.write("wm1 failed", 99);
						} else {
							Cacheable.write("wm1 succeeded", 51);
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("wm2")) {
						Class98_Sub16.setWindowMode(2, -1, i ^ 0x76, -1, false);
						if ((OpenGlModelRenderer.getWindowMode((byte) 107) ^ 0xffffffff) == -3) {
							Cacheable.write("wm2 succeeded", -94);
						} else {
							Cacheable.write("wm2 failed", -86);
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("wm3")) {
						Class98_Sub16.setWindowMode(3, 1024, 3, 768, false);
						if (OpenGlModelRenderer.getWindowMode((byte) -75) != 3) {
							Cacheable.write("wm3 failed", -108);
						} else {
							Cacheable.write("wm3 succeeded", 106);
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("tk0")) {
						Class76_Sub4.method754(0, false, 103);
						if (client.preferences.currentToolkit.getValue((byte) 121) == 0) {
							Cacheable.write("Entered tk0", 108);
							client.preferences.setPreference((byte) -13, 0, client.preferences.desiredToolkit);
							Class310.method3618(-5964);
							OpenGlGround.aBoolean5207 = false;
						} else {
							Cacheable.write("Failed to enter tk0", -128);
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("tk1")) {
						Class76_Sub4.method754(1, false, -116);
						if (client.preferences.currentToolkit.getValue((byte) 123) == 1) {
							Cacheable.write("Entered tk1", 110);
							client.preferences.setPreference((byte) -13, 1, client.preferences.desiredToolkit);
							Class310.method3618(i + -6081);
							OpenGlGround.aBoolean5207 = false;
						} else {
							Cacheable.write("Failed to enter tk1", -104);
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("tk2")) {
						Class76_Sub4.method754(2, false, 71);
						if ((client.preferences.currentToolkit.getValue((byte) 120) ^ 0xffffffff) != -3) {
							Cacheable.write("Failed to enter tk2", -64);
						} else {
							Cacheable.write("Entered tk2", -116);
							client.preferences.setPreference((byte) -13, 2, client.preferences.desiredToolkit);
							Class310.method3618(-5964);
							OpenGlGround.aBoolean5207 = false;
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("tk3")) {
						Class76_Sub4.method754(3, false, i + -13);
						if ((client.preferences.currentToolkit.getValue((byte) 127) ^ 0xffffffff) != -4) {
							Cacheable.write("Failed to enter tk3", 50);
						} else {
							Cacheable.write("Entered tk3", 97);
							client.preferences.setPreference((byte) -13, 3, client.preferences.desiredToolkit);
							Class310.method3618(-5964);
							OpenGlGround.aBoolean5207 = false;
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("tk5")) {
						Class76_Sub4.method754(5, false, 76);
						if (client.preferences.currentToolkit.getValue((byte) 124) == 5) {
							Cacheable.write("Entered tk5", 111);
							client.preferences.setPreference((byte) -13, 5, client.preferences.desiredToolkit);
							Class310.method3618(i + -6081);
							OpenGlGround.aBoolean5207 = false;
						} else {
							Cacheable.write("Failed to enter tk5", 88);
							break;
						}
						break;
					}
					if (string.startsWith("setba")) {
						if ((string.length() ^ 0xffffffff) > -7) {
							Cacheable.write("Invalid buildarea value", -80);
						} else {
							int i_43_ = JavaNetworkWriter.parseInteger(-102, string.substring(6));
							if ((i_43_ ^ 0xffffffff) > -1 || i_43_ > RsFloatBuffer.method1262(i + -109, GameShell.maxHeap)) {
								Cacheable.write("Invalid buildarea value", -63);
							} else {
								client.preferences.setPreference((byte) -13, i_43_, client.preferences.buildArea);
								Class310.method3618(-5964);
								OpenGlGround.aBoolean5207 = false;
								Cacheable.write("maxbuildarea=" + client.preferences.buildArea.getValue((byte) 120), -65);
								break;
							}
							break;
						}
						break;
					}
					if (string.startsWith("rect_debug")) {
						if (string.length() < 10) {
							Cacheable.write("Invalid rect_debug value", -126);
						} else {
							VarPlayerDefinition.anInt1282 = JavaNetworkWriter.parseInteger(64, string.substring(10).trim());
							Cacheable.write("rect_debug=" + VarPlayerDefinition.anInt1282, -119);
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("qa_op_test")) {
						Class15.qaOpTest = true;
						Cacheable.write("qa_op_test=" + Class15.qaOpTest, -113);
						break;
					}
					if (string.equalsIgnoreCase("clipcomponents")) {
						FloorUnderlayDefinitionParser.clipComponents = !FloorUnderlayDefinitionParser.clipComponents;
						Cacheable.write("clipcomponents=" + FloorUnderlayDefinitionParser.clipComponents, i ^ ~0x37);
						break;
					}
					if (string.startsWith("bloom")) {
						System.out.println("Reached");
						boolean bool_44_ = client.graphicsToolkit.canEnableBloom();
						if (!Class98_Sub5_Sub1.method966(29089, !bool_44_)) {
							Cacheable.write("Failed to enable bloom", 126);
						} else {
							if (!bool_44_) {
								Cacheable.write("Bloom enabled", 46);
							} else {
								Cacheable.write("Bloom disabled", -85);
								break;
							}
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("tween")) {
						if (!Class357.forcedTweening) {
							Class357.forcedTweening = true;
							Cacheable.write("Forced tweening ENABLED!", 123);
						} else {
							Class357.forcedTweening = false;
							Cacheable.write("Forced tweening disabled.", i ^ ~0x2c);
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("shiftclick")) {
						if (Js5Manager.shiftClickEnabled) {
							Cacheable.write("Shift-click disabled.", i + -55);
							Js5Manager.shiftClickEnabled = false;
						} else {
							Cacheable.write("Shift-click ENABLED!", i ^ 0x11);
							Js5Manager.shiftClickEnabled = true;
							break;
						}
						break;
					}
					if (string.equalsIgnoreCase("getcgcoord")) {
						Cacheable.write("x:" + (Class87.localPlayer.boundExtentsX >> -766503447) + " z:" + (Class87.localPlayer.boundExtentsZ >> 1009373513), -102);
						break;
					}
					if (string.equalsIgnoreCase("getheight")) {
						Cacheable.write("Height: " + Class78.aSArray594[Class87.localPlayer.plane].getTileHeight(Class87.localPlayer.boundExtentsZ >> -447569975, -12639, Class87.localPlayer.boundExtentsX >> 1269250985), 88);
						break;
					}
					if (string.equalsIgnoreCase("resetminimap")) {
						client.spriteJs5.method2766(16);
						client.spriteJs5.discardAllUnpacked((byte) -116);
						Class98_Sub10_Sub23.mapScenesDefinitionList.method3770(i + -83);
						Class216.worldMapInfoDefinitionList.method3808(0);
						InputStream_Sub2.method124(-79);
						Cacheable.write("Minimap reset", 103);
						break;
					}
					if (string.startsWith("mc")) {
						if (client.graphicsToolkit.method1810()) {
							int i_45_ = Integer.parseInt(string.substring(3));
							if ((i_45_ ^ 0xffffffff) <= -2) {
								if (i_45_ > 4) {
									i_45_ = 4;
								}
							} else {
								i_45_ = 1;
							}
							RotatingSpriteLSEConfig.anInt5499 = i_45_;
							Class98_Sub10.method999((byte) -47);
							Cacheable.write("Render cores now: " + RotatingSpriteLSEConfig.anInt5499, -93);
						} else {
							Cacheable.write("Current toolkit doesn't support multiple cores", 121);
							break;
						}
						break;
					}
					if (string.startsWith("cachespace")) {
						Cacheable.write("I(s): " + Class69_Sub2.aClass79_5334.getFreeSpace(1551398789) + "/" + Class69_Sub2.aClass79_5334.getMaxSize(18), 53);
						Cacheable.write("I(m): " + Class64_Sub5.aClass79_3650.getFreeSpace(1551398789) + "/" + Class64_Sub5.aClass79_3650.getMaxSize(16), -122);
						Cacheable.write("O(s): " + Class98_Sub46_Sub19.itemDefinitionList.spriteCache.method726(true) + "/" + Class98_Sub46_Sub19.itemDefinitionList.spriteCache.method730(-19536), i + -197);
						break;
					}
					if (string.equalsIgnoreCase("getcamerapos")) {
						Cacheable.write("Pos: " + Class87.localPlayer.plane + "," + ((Class98_Sub46_Sub10.xCamPosTile >> 1855638953) - -Class272.gameSceneBaseX >> -82400218) + "," + ((SpriteLoadingScreenElement.yCamPosTile >> -583047319) + aa_Sub2.gameSceneBaseY >> 559817670) + "," + (0x3f
								& Class272.gameSceneBaseX + (Class98_Sub46_Sub10.xCamPosTile >> 86672457)) + "," + (0x3f & (SpriteLoadingScreenElement.yCamPosTile >> 1182901385) + aa_Sub2.gameSceneBaseY) + " Height: " + (StrongReferenceMCNode.getHeight(Class87.localPlayer.plane,
										SpriteLoadingScreenElement.yCamPosTile, Class98_Sub46_Sub10.xCamPosTile, 24111) + -AdvancedMemoryCache.anInt601), 96);
						Cacheable.write("Look: " + Class87.localPlayer.plane + "," + (Exception_Sub1.anInt44 - -Class272.gameSceneBaseX >> -1575614138) + "," + (aa_Sub2.gameSceneBaseY + NodeString.anInt3915 >> 1704567462) + "," + (0x3f & Class272.gameSceneBaseX + Exception_Sub1.anInt44)
								+ "," + (0x3f & NodeString.anInt3915 - -aa_Sub2.gameSceneBaseY) + " Height: " + (StrongReferenceMCNode.getHeight(Class87.localPlayer.plane, NodeString.anInt3915, Exception_Sub1.anInt44, 24111) - Class303.anInt2530), 122);
						break;
					}
					if (string.equals("renderprofile") || string.equals("rp")) {
						Class170.showProfiling = !Class170.showProfiling;
						client.graphicsToolkit.method1761(Class170.showProfiling);
						Class228.method2862(-123);
						Cacheable.write("showprofiling=" + Class170.showProfiling, 89);
						break;
					}
					if (string.startsWith("performancetest")) {
						int i_46_ = -1;
						int i_47_ = 1000;
						if ((string.length() ^ 0xffffffff) < -16) {
							String[] strings = Class112.splitString(string, ' ', false);
							try {
								if ((strings.length ^ 0xffffffff) < -2) {
									i_47_ = Integer.parseInt(strings[1]);
								}
							} catch (Throwable throwable) {
								/* empty */
							}
							try {
								if ((strings.length ^ 0xffffffff) < -3) {
									i_46_ = Integer.parseInt(strings[2]);
								}
							} catch (Throwable throwable) {
								/* empty */
							}
						}
						if ((i_46_ ^ 0xffffffff) != 0) {
							Cacheable.write("Performance: " + Class66.method683((byte) -86, i_47_, i_46_), -64);
						} else {
							Cacheable.write("Java toolkit: " + Class66.method683((byte) -109, i_47_, 0), -108);
							Cacheable.write("SSE toolkit:  " + Class66.method683((byte) -128, i_47_, 2), i ^ 0x2b);
							Cacheable.write("D3D toolkit:  " + Class66.method683((byte) -117, i_47_, 3), -61);
							Cacheable.write("GL toolkit:   " + Class66.method683((byte) -104, i_47_, 1), 126);
							Cacheable.write("GLX toolkit:  " + Class66.method683((byte) -92, i_47_, 5), i + 10);
							break;
						}
						break;
					}
					if (string.equals("nonpcs")) {
						Class237_Sub1.noNpcs = !Class237_Sub1.noNpcs;
						Cacheable.write("nonpcs=" + Class237_Sub1.noNpcs, 94);
						break;
					}
					if (string.equals("autoworld")) {
						Class98_Sub10_Sub25.method1080((byte) -86);
						Cacheable.write("auto world selected", -63);
						break;
					}
					if (string.startsWith("switchworld")) {
						int i_48_ = Integer.parseInt(string.substring(12));
						Class98_Sub12.method1131(-8804, i_48_, Class275.method3283((byte) 113, i_48_).aString3634);
						Cacheable.write("switched", i + -77);
						break;
					}
					if (string.equals("getworld")) {
						Cacheable.write("w: " + client.server.index, i + -196);
						break;
					}
					if (string.startsWith("pc")) {
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class246_Sub4_Sub2.SEND_PRIVATE_MESSAGE, Class331.aClass117_2811);
						class98_sub11.packet.writeByte(0, i ^ 0x48);
						int i_49_ = class98_sub11.packet.position;
						int i_50_ = string.indexOf(" ", 4);
						class98_sub11.packet.writePJStr1(string.substring(3, i_50_), (byte) 113);
						Class284_Sub1_Sub1.method3368(i + 10, string.substring(i_50_), class98_sub11.packet);
						class98_sub11.packet.method1211((byte) 118, -i_49_ + class98_sub11.packet.position);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						break;
					}
					if (string.equals("savevarcs")) {
						Class23.method283((byte) -80);
						Cacheable.write("perm varcs saved", 103);
						break;
					}
					if (string.equals("scramblevarcs")) {
						for (int i_51_ = 0; (Class76_Sub5.intGlobalConfigs.length ^ 0xffffffff) < (i_51_ ^ 0xffffffff); i_51_++) {
							if (Class140.boolGlobalConfigs[i_51_]) {
								Class76_Sub5.intGlobalConfigs[i_51_] = (int) (Math.random() * 99999.0);
								if (Math.random() > 0.5) {
									Class76_Sub5.intGlobalConfigs[i_51_] *= -1;
								}
							}
						}
						Class23.method283((byte) -118);
						Cacheable.write("perm varcs scrambled", -114);
						break;
					}
					if (string.equals("showcolmap")) {
						Class44.showColMap = true;
						InputStream_Sub2.method124(-119);
						Cacheable.write("colmap is shown", -67);
						break;
					}
					if (string.equals("hidecolmap")) {
						Class44.showColMap = false;
						InputStream_Sub2.method124(122);
						Cacheable.write("colmap is hidden", i ^ ~0x2f);
						break;
					}
					if (string.equals("resetcache")) {
						Class98_Sub10_Sub15.method1050((byte) 114);
						Cacheable.write("Caches reset", -104);
						break;
					}
					if (string.equals("profilecpu")) {
						Cacheable.write(String.valueOf(Class278_Sub1.method3320(12)) + "ms", i + -204);
						break;
					}
					if (string.startsWith("getclientvarpbit")) {
						int value = Integer.parseInt(string.substring(17));
						Cacheable.write("varpbit=" + StartupStage.varValues.getClientVarpBit(value, i ^ 0x1cb8), 65);
						break;
					}
					if (string.startsWith("getclientvarp")) {
						int value = Integer.parseInt(string.substring(14));
						Cacheable.write("varp=" + StartupStage.varValues.getClientVarp(value, i ^ ~0xb), -83);
						break;
					}
					if (string.startsWith("directlogin")) {
						String[] strings = Class112.splitString(string.substring(12), ' ', false);
						if ((strings.length ^ 0xffffffff) <= -3) {
							int i_54_ = strings.length > 2 ? Integer.parseInt(strings[2]) : 0;
							Class251.handleLobbyToWorldStage(strings[1], -17877, strings[0], i_54_);
							break;
						}
					}
					if (string.startsWith("csprofileclear")) {
						ClientScript2Runtime.method3147();
						break;
					}
					if (string.startsWith("csprofileoutputc")) {
						ClientScript2Runtime.method3143(100, false);
						break;
					}
					if (string.startsWith("csprofileoutputt")) {
						ClientScript2Runtime.method3143(10, true);
						break;
					}
					if (string.startsWith("texsize")) {
						int size = Integer.parseInt(string.substring(8));
						client.graphicsToolkit.method1778(size);
						break;
					}
					if (string.equals("soundstreamcount")) {
						Cacheable.write("Active streams: " + Class81.aClass98_Sub31_Sub3_619.method1377(), -93);
						break;
					}
					if (string.equals("autosetup")) {
						Class98_Sub28.method1306((byte) -106);
						Cacheable.write("Complete. Toolkit now: " + client.preferences.currentToolkit.getValue((byte) 123), i + -52);
						break;
					}
					if (string.equals("errormessage")) {
						Cacheable.write(client.activeClient.generateDebugInfoString(0), 58);
						break;
					}
					if (string.equals("heapdump")) {
						if (SignLink.osNameLowerCase.startsWith("win")) {
							Class98_Sub30.method1319(0, new File("C:\\Temp\\heap.dump"), false);
						} else {
							Class98_Sub30.method1319(0, new File("/tmp/heap.dump"), false);
						}
						Cacheable.write("Done", -115);
						break;
					}
					if (string.equals("os")) {
						Cacheable.write("Name: " + SignLink.osNameLowerCase, i + -234);
						Cacheable.write("Arch: " + SignLink.osArch, -80);
						Cacheable.write("Ver: " + SignLink.osVersion, -100);
						break;
					}
					if (string.startsWith("w2debug")) {
						int i_56_ = Integer.parseInt(string.substring(8, 9));
						Class98_Sub10_Sub14.anInt5614 = i_56_;
						Class98_Sub10.method999((byte) 124);
						Cacheable.write("Toggled!", 74);
						break;
					}
					if (string.startsWith("ortho ")) {
						int i_57_ = string.indexOf(' ');
						if (i_57_ < 0) {
							Cacheable.write("Syntax: ortho <n>", -66);
						} else {
							int value = JavaNetworkWriter.parseInteger(84, string.substring(i_57_ - -1));
							client.preferences.setPreference((byte) -13, value, client.preferences.orthoZoom);
							Class310.method3618(-5964);
							OpenGlGround.aBoolean5207 = false;
							Class230.method2871(-45);
							if ((value ^ 0xffffffff) != (client.preferences.orthoZoom.getValue((byte) 124) ^ 0xffffffff)) {
								Cacheable.write("Failed to change ortho mode", -94);
							} else {
								Cacheable.write("Successfully changed ortho mode", -104);
								break;
							}
							break;
						}
						break;
					}
					if (string.startsWith("orthozoom ")) {
						if (client.preferences.orthoZoom.getValue((byte) 122) == 0) {
							Cacheable.write("enable ortho mode first (use 'ortho <n>')", -114);
						} else {
							int zoom = JavaNetworkWriter.parseInteger(-52, string.substring(string.indexOf(' ') + 1));
							Class16.orthoZoom = zoom;
							Cacheable.write("orthozoom=" + Class16.orthoZoom, -124);
							break;
						}
						break;
					}
					if (string.startsWith("orthotilesize ")) {
						int i_60_ = JavaNetworkWriter.parseInteger(-54, string.substring(1 + string.indexOf(' ')));
						WorldMapInfoDefinitionParser.anInt2856 = FileOnDisk.anInt3018 = i_60_;
						Cacheable.write("ortho tile size=" + i_60_, -73);
						Class230.method2871(-37);
						break;
					}
					if (string.equals("orthocamlock")) {
						Class69.orthoCameraLock = !Class69.orthoCameraLock;
						Cacheable.write("ortho camera lock is " + (Class69.orthoCameraLock ? "on" : "off"), 53);
						break;
					}
					if (string.startsWith("setoutput ")) {
						File file = new File(string.substring(10));
						if (file.exists()) {
							file = new File(string.substring(10) + "." + TimeTools.getCurrentTime(-47) + ".log");
							if (file.exists()) {
								Cacheable.write("file already exists!", -106);
								break;
							}
						}
						if (StructsDefinitionParser.aFileOutputStream1969 != null) {
							StructsDefinitionParser.aFileOutputStream1969.close();
							StructsDefinitionParser.aFileOutputStream1969 = null;
						}
						try {
							StructsDefinitionParser.aFileOutputStream1969 = new FileOutputStream(file);
						} catch (java.io.FileNotFoundException filenotfoundexception) {
							Cacheable.write("Could not create " + file.getName(), i + -15);
						} catch (SecurityException securityexception) {
							Cacheable.write("Cannot write to " + file.getName(), 67);
						}
						break;
					}
					if (string.equals("closeoutput")) {
						if (StructsDefinitionParser.aFileOutputStream1969 != null) {
							StructsDefinitionParser.aFileOutputStream1969.close();
						}
						StructsDefinitionParser.aFileOutputStream1969 = null;
						break;
					}
					if (string.startsWith("runscript ")) {
						File file = new File(string.substring(10));
						if (!file.exists()) {
							Cacheable.write("No such file", 45);
							break;
						}
						byte[] is = Class273.method3281(i ^ ~0x3, file);
						if (is == null) {
							Cacheable.write("Failed to read file", i ^ 0x2b);
							break;
						}
						String[] strings = Class112.splitString(GraphicsBuffer.method1435(Class69_Sub2.method707(is, true), "", (byte) -110, '\r'), '\n', false);
						Class261.method1044((byte) 91, strings);
					}
					if (string.startsWith("zoom ")) {
						short zoom = (short) JavaNetworkWriter.parseInteger(66, string.substring(5));
						if (zoom > 0) {
							Js5Client.clientZoom = zoom;
						}
						break;
					}
					if ((client.clientState ^ 0xffffffff) == -11) {
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, TextResources.aClass171_2650, Class331.aClass117_2811);
						class98_sub11.packet.writeByte(string.length() - -3, i ^ 0x25);
						class98_sub11.packet.writeByte(!bool_37_ ? 0 : 1, -61);
						class98_sub11.packet.writeByte(!bool ? 0 : 1, 93);
						class98_sub11.packet.writePJStr1(string, (byte) 113);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					}
					if (string.startsWith("fps ") && client.buildLocation != BuildLocation.LIVE) {
						Class342.method3815(JavaNetworkWriter.parseInteger(i ^ 0x59, string.substring(4)), 59);
						break;
					}
				} catch (Exception exception) {
					Cacheable.write(TextResources.ERROR_EXECUTING_COMMAND.getText(client.gameLanguage, (byte) 25), -117);
					break;
				}
			}
			if (client.clientState == 10) {
				break;
			}
			Cacheable.write(TextResources.UNKNOWN_DEVELOPER_COMMAND.getText(client.gameLanguage, (byte) 25) + string, 71);
			break;
		} while (false);
	}
}

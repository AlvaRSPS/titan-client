
/* Class31_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

public final class ProxySocket extends SocketWrapper {
	public static Class	aClass3588;
	public static Class	aClass3589;

	public static Class method312(String string) {
		Class var_class;
		try {
			var_class = Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
		return var_class;
	}

	private ProxySelector proxySelector;

	public ProxySocket() {
		try {
			proxySelector = ProxySelector.getDefault();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	private final Socket connectProxy(int i, Proxy proxy) throws IOException {
		Socket socket;
		try {
			if (proxy.type() == Proxy.Type.DIRECT) {
				return createDirectSocket((byte) -53);
			}
			java.net.SocketAddress socketaddress = proxy.address();
			if (!(socketaddress instanceof InetSocketAddress)) {
				return null;
			}
			InetSocketAddress proxyAddress = (InetSocketAddress) socketaddress;
			if (proxy.type() != Proxy.Type.HTTP) {
				if (proxy.type() == Proxy.Type.SOCKS) {
					Socket socket_0_ = new Socket(proxy);
					socket_0_.connect(new InetSocketAddress(hostName, port));
					return socket_0_;
				}
			} else {
				String header = null;
				try {
					Class authInfo = Class.forName("sun.net.www.protocol.http.AuthenticationInfo");
					Method getProxyAuth = authInfo.getDeclaredMethod("getProxyAuth", aClass3588 == null ? aClass3588 = method312("java.lang.String") : aClass3588, Integer.TYPE);
					getProxyAuth.setAccessible(true);
					Object object = getProxyAuth.invoke(null, proxyAddress.getHostName(), new Integer(proxyAddress.getPort()));
					if (object != null) {
						Method supportsPreemptAuth = authInfo.getDeclaredMethod("supportsPreemptiveAuthorization");
						supportsPreemptAuth.setAccessible(true);
						if (((Boolean) supportsPreemptAuth.invoke(object)).booleanValue()) {
							Method headerName = authInfo.getDeclaredMethod("getHeaderName");
							headerName.setAccessible(true);
							Method headerValue = authInfo.getDeclaredMethod("getHeaderValue", aClass3589 == null ? (aClass3589 = method312("java.net.URL")) : aClass3589, aClass3588 == null ? (aClass3588 = method312("java.lang.String")) : aClass3588);
							headerValue.setAccessible(true);
							String header_name = (String) headerName.invoke(object);
							String header_value = (String) headerValue.invoke(object, new URL("https://" + hostName + "/"), "https");
							header = header_name + ": " + header_value;
						}
					}
				} catch (Exception exception) {
					/* empty */
				}
				return this.connectProxy(proxyAddress.getHostName(), proxyAddress.getPort(), header);
			}
			if (i != -6562) {
				proxySelector = null;
			}
			socket = null;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return socket;
	}

	private final Socket connectProxy(String proxy_host, int proxy_port, String auth_header) throws IOException {
		Socket socket;
		try {
			Socket proxySocket = new Socket(proxy_host, proxy_port);
			proxySocket.setSoTimeout(10000);
			OutputStream outputstream = proxySocket.getOutputStream();
			if (null == auth_header) {
				outputstream.write(("CONNECT " + hostName + ":" + port + " HTTP/1.0\n\n").getBytes(Charset.forName("ISO-8859-1")));
			} else {
				outputstream.write(("CONNECT " + hostName + ":" + port + " HTTP/1.0\n" + auth_header + "\n\n").getBytes(Charset.forName("ISO-8859-1")));
			}
			outputstream.flush();
			BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(proxySocket.getInputStream()));
			String response = bufferedreader.readLine();
			if (response != null) {
				if (response.startsWith("HTTP/1.0 200") || response.startsWith("HTTP/1.1 200")) {
					return proxySocket;
				}
				if (response.startsWith("HTTP/1.0 407") || response.startsWith("HTTP/1.1 407")) {
					int headerLine = 0;
					String proxyAuthenticate = "proxy-authenticate: ";
					for (response = bufferedreader.readLine(); response != null && headerLine < 50; response = bufferedreader.readLine()) {
						if (response.toLowerCase().startsWith(proxyAuthenticate)) {
							response = response.substring(proxyAuthenticate.length()).trim();
							int responseIndex = response.indexOf(' ');
							if (-1 != responseIndex) {
								response = response.substring(0, responseIndex);
							}
							throw new ProxyException(response);
						}
						headerLine++;
					}
					throw new ProxyException("");
				}
			}
			outputstream.close();
			bufferedreader.close();
			proxySocket.close();
			socket = null;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return socket;
	}

	@Override
	public final Socket open(int i) throws IOException {
		Socket socket;
		try {
			if (i != -2) {
				return null;
			}
			boolean useSystemProxies = Boolean.parseBoolean(System.getProperty("java.net.useSystemProxies"));
			if (!useSystemProxies) {
				System.setProperty("java.net.useSystemProxies", "true");
			}
			boolean isHttps = -444 == (port ^ 0xffffffff);
			List<Proxy> proxies;
			List<Proxy> httpProxy;
			try {
				proxies = proxySelector.select(new URI((!isHttps ? "http" : "https") + "://" + hostName));
				httpProxy = proxySelector.select(new URI((isHttps ? "http" : "https") + "://" + hostName));
			} catch (URISyntaxException urisyntaxexception) {
				return createDirectSocket((byte) -53);
			}
			proxies.addAll(httpProxy);
			Object[] proxiesArray = proxies.toArray();
			ProxyException caughtException = null;
			Object[] proxies_array = proxiesArray;
			for (int proxiesCount = 0; (proxies_array.length ^ 0xffffffff) < (proxiesCount ^ 0xffffffff); proxiesCount++) {
				Object object = proxies_array[proxiesCount];
				Proxy proxy = (Proxy) object;
				Socket _socket;
				try {
					Socket proxySocket = this.connectProxy(-6562, proxy);
					if (null == proxySocket) {
						continue;
					}
					_socket = proxySocket;
				} catch (ProxyException proxyException) {
					caughtException = proxyException;
					continue;
				} catch (IOException ioException) {
					continue;
				}
				return _socket;
			}
			if (null != caughtException) {
				throw caughtException;
			}
			socket = createDirectSocket((byte) -53);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return socket;
	}
}

/* Class98_Sub10_Sub8 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;

public final class Class98_Sub10_Sub8 extends Class98_Sub10 {
	public static boolean[]				aBooleanArray5579	= new boolean[8];
	public static TextureMetricsList	aD5578;

	public static final Class98_Sub46_Sub10 method1026(int i) {
		if (i != -3) {
			return null;
		}
		return WorldMap.aClass98_Sub46_Sub10_2056;
	}

	public static final int method1027(int i, int i_0_, int i_1_) {
		if ((i_0_ ^ 0xffffffff) == 0) {
			return 12345678;
		}
		i = i * (i_0_ & 0x7f) >> 2041991591;
		do {
			if ((i ^ 0xffffffff) > -3) {
				i = 2;
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			if ((i ^ 0xffffffff) < -127) {
				i = 126;
			}
		} while (false);
		if (i_1_ >= -73) {
			aBooleanArray5579 = null;
		}
		return i + (i_0_ & 0xff80);
	}

	public static final ChatStatus setChatStatus(byte dummy, int status) {
		ChatStatus[] chatStatuses = EnumDefinition.getAllStatuses((byte) 67);
		for (ChatStatus chatStatus : chatStatuses) {
			if (status == chatStatus.privateChatStatus) {
				return chatStatus;
			}
		}
		return null;
	}

	public Class98_Sub10_Sub8() {
		super(0, true);
	}

	private final int method1030(int i, byte i_7_, int i_8_) {
		try {
			int i_9_ = 57 * i_8_ + i;
			if (i_7_ != 3) {
				method1027(-5, 93, 26);
			}
			i_9_ = i_9_ << -924402879 ^ i_9_;
			return -(((789221 + i_9_ * i_9_ * 15731) * i_9_ - -1376312589 & 0x7fffffff) / 262144) + 4096;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ej.H(" + i + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	@Override
	public final int[] method990(int i, int i_2_) {
		try {
			if (i != 255) {
				aBooleanArray5579 = null;
			}
			int[] is = this.aClass16_3863.method237((byte) 98, i_2_);
			if (this.aClass16_3863.aBoolean198) {
				int i_3_ = GameObjectDefinition.anIntArray3001[i_2_];
				for (int i_4_ = 0; (i_4_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_4_++) {
					is[i_4_] = method1030(MonoOrStereoPreferenceField.anIntArray3640[i_4_], (byte) 3, i_3_) % 4096;
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ej.G(" + i + ',' + i_2_ + ')');
		}
	}
}

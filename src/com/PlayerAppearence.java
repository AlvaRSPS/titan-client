package com;

import com.jagex.game.clanchat.ClanChatMember;
import com.jagex.game.client.archive.Archive;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Index;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.SkyboxDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.parser.AnimationDefinitionParser;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.IdentikitDefinitionParser;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;
import com.jagex.game.client.definition.parser.ItemDefinitionParser;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.definition.parser.NPCDefinitionParser;
import com.jagex.game.client.definition.parser.QuestDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.definition.parser.SkyboxDefinitionParser;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;
import com.jagex.game.client.preferences.LightningDetailPreferenceField;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.preferences.SafeModePreferenceField;
import com.jagex.game.client.preferences.ScreenSizePreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.AwtLoadingScreen;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.ScalingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SimpleProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.DecoratedProgressBarLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.input.RtKeyListener;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

/* Class313 - Decompiled by JODE
 *//*
	*/
public final class PlayerAppearence {
	public static float aFloat2680 = 0.25F;

	public static final IncomingOpcode[] getAll(int i) {
		return new IncomingOpcode[] { Class370.HINT_ICON, Class18.aClass58_215, Class98_Sub10_Sub20.aClass58_5638, AwtGraphicsBuffer.aClass58_5883, Class251.aClass58_1921, Billboard.VARPBIT_SMALL, Class76.aClass58_587, Class211.SEND_VOICE, Class53.SEND_EXPERIENCE_UPDATE,
				Class98_Sub50.CHANGE_WALK_HERE_OPTION_TEXT, Class232.SEND_FRIEND, Class40.aClass58_369, Class151_Sub6.aClass58_4998, Class246_Sub4_Sub2.SEND_RUN_ENERGY, Class287.aClass58_2194, QuestDefinitionParser.aClass58_161, ScreenSizePreferenceField.aClass58_3715, RsFloatBuffer.aClass58_5793,
				PacketParser.aClass58_5466, QuickChatMessageType.aClass58_2912, Class98_Sub41.aClass58_4199, Class48_Sub1_Sub2.VARP_SMALL, DummyInputStream.aClass58_28, Class320.aClass58_2708, Class246_Sub3_Sub4_Sub5.aClass58_6264, Class39.aClass58_364, Class188.aClass58_1452,
				SkyboxDefinition.VARP_LARGE, Player.CLAN_CHAT_DATA, GameObjectDefinitionParser.SEND_GROUND_ITEM, ActionQueueEntry.RECIEVE_PRIVATE_MESSAGE, Class150.aClass58_1212, Class98_Sub31_Sub2.aClass58_5838, Class224_Sub1.aClass58_5032, MonoOrStereoPreferenceField.aClass58_3637,
				InventoriesDefinitionParser.SEND_CLAN_CHAT_MESSAGE, Class98_Sub46_Sub15.SEND_WINDOW_PANE, Class369.IGNORE_LIST, TextResources.aClass58_2651, OpenGLRenderEffectManager.aClass58_433, Class98_Sub10_Sub15.aClass58_5615, GameObjectDefinition.aClass58_2993, Class98_Sub42.aClass58_4222,
				Class151_Sub8.VARCSTR_LARGE, RtKeyListener.aClass58_592, Class16.aClass58_191, Animable.aClass58_6151, RtMouseListener.aClass58_2495, SafeModePreferenceField.aClass58_3642, Class150.aClass58_1210, OpenGlElementBufferPointer.ATTACH_INTERFACE, Class372.VARPBIT_LARGE, Class98_Sub10_Sub20.aClass58_5635,
				ParamDefinition.SEND_MESSAGE, Class336.PLAYER_ON_INTERFACE, Class246_Sub3_Sub4_Sub3.aClass58_6457, Class284_Sub2_Sub2.aClass58_6197, GroundItem.aClass58_4029, SimpleProgressBarLSEConfig.aClass58_5493, GroupProgressMonitor.aClass58_3406, Class98_Sub46_Sub6.aClass58_5975,
				Class27.SEND_CLOSE_INTERFACE, DecoratedProgressBarLSEConfig.SEND_PUBLIC_CHAT_MESSAGE, Class283.aClass58_2139, LightningDetailPreferenceField.aClass58_3665, Class246_Sub3_Sub2_Sub1.aClass58_6335, Class98_Sub12.aClass58_3877, aa_Sub3.aClass58_3566, Class98_Sub10_Sub14.aClass58_5606,
				Class76.SEND_SOUND, Class27.aClass58_274, MapScenesDefinitionParser.NPC_ON_INTERFACE, BuildLocation.aClass58_1507, SafeModePreferenceField.aClass58_3645, Archive.aClass58_2844, Class44.aClass58_379, Class151_Sub6.aClass58_4997, Class98_Sub10_Sub14.aClass58_5608, Class65.aClass58_499,
				Class211.aClass58_1596, Class151_Sub6.aClass58_4999, Class3.aClass58_75, SunDefinition.aClass58_1992, Char.SEND_GRAND_EXCHANGE_OFFER, ModelRenderer.aClass58_1179, Class159.SEND_IGNORE_LIST, NodeInteger.SEND_INTERFACE_ANIMATION, Class151_Sub5.aClass58_4992,
				RtInterfaceAttachment.VARCSTR_SMALL, Js5Index.aClass58_2661, Class283.aClass58_2143, Class98_Sub46_Sub19.aClass58_6057, Js5.aClass58_1576, TextLSEConfig.aClass58_3533, aa_Sub1.aClass58_3554, Class36.aClass58_344, Class308.aClass58_2581, Class98_Sub10_Sub28.aClass58_5697,
				FriendLoginUpdate.aClass58_6166, RtKeyListener.aClass58_591, NPCDefinitionParser.aClass58_2507, OpenGlGround.aClass58_5205, Huffman.aClass58_1609, Class15.aClass58_184, Class76_Sub2.aClass58_3731, Class69_Sub2.aClass58_5333, Class25.aClass58_266, Class284_Sub1.aClass58_5176,
				GameObjectDefinition.aClass58_2943, FileProgressMonitor.aClass58_3398, Class358.aClass58_3029, Class287.aClass58_2187, Class246_Sub3_Sub4_Sub4.aClass58_6487, SkyboxDefinitionParser.aClass58_469, RSByteBuffer.SEND_CHUNK_UPDATE, ClanChatMember.aClass58_1192, Class277.aClass58_2052,
				Exception_Sub1.aClass58_43, Class98_Sub47.aClass58_4270, RemoveRoofsPreferenceField.SEND_AMASK, ItemSpriteCacheKey.SET_PLAYER_OPTION, Class98_Sub6.aClass58_3844 };
	}

	public static final void method3625(int i) {
		int[] is = new int[Class98_Sub46_Sub19.itemDefinitionList.count];
		int i_23_ = 0;
		for (int i_24_ = 0; i_24_ < Class98_Sub46_Sub19.itemDefinitionList.count; i_24_++) {
			ItemDefinition definition = Class98_Sub46_Sub19.itemDefinitionList.get(i_24_, (byte) -124);
			if ((definition.manWear ^ 0xffffffff) <= -1 || (definition.womanWear ^ 0xffffffff) <= -1) {
				is[i_23_++] = i_24_;
			}
		}
		Class255.anIntArray3207 = new int[i_23_];

		/*
		 * try { BufferedWriter bw = new BufferedWriter(new
		 * FileWriter("equipids.txt")); for (int i_25_ = 0; (i_25_ ^ 0xffffffff)
		 * > (i_23_ ^ 0xffffffff); i_25_++) { Class255.anIntArray3207[i_25_] =
		 * is[i_25_]; bw.write(is[i_25_] + ":" + i_25_); bw.newLine(); }
		 * bw.flush(); bw.close(); } catch (Exception e) { e.printStackTrace();
		 * }
		 */

		Class255.anIntArray3207 = new int[i_23_];
		for (int i_25_ = 0; i_25_ < i_23_; i_25_++) {
			Class255.anIntArray3207[i_25_] = is[i_25_];
		}

	}

	public static final Class98_Sub10 method3630(int i, int i_111_) {
		int i_113_ = i;
		while_216_: do {
			while_215_: do {
				while_214_: do {
					while_213_: do {
						while_212_: do {
							while_211_: do {
								while_210_: do {
									while_209_: do {
										while_208_: do {
											while_207_: do {
												while_206_: do {
													while_205_: do {
														while_204_: do {
															while_203_: do {
																while_202_: do {
																	while_201_: do {
																		while_200_: do {
																			while_199_: do {
																				while_198_: do {
																					while_197_: do {
																						while_196_: do {
																							while_195_: do {
																								while_194_: do {
																									while_193_: do {
																										while_192_: do {
																											while_191_: do {
																												while_190_: do {
																													while_189_: do {
																														while_188_: do {
																															while_187_: do {
																																while_186_: do {
																																	while_185_: do {
																																		while_184_: do {
																																			while_183_: do {
																																				while_182_: do {
																																					while_181_: do {
																																						while_180_: do {
																																							while_179_: do {
																																								while_178_: do {
																																									do {
																																										if (i_113_ != 0) {
																																											if ((i_113_ ^ 0xffffffff) == -2) {
																																												break;
																																											}
																																											if (i_113_ == 2) {
																																												break while_178_;
																																											}
																																											if (i_113_ == 3) {
																																												break while_179_;
																																											}
																																											if (i_113_ == 4) {
																																												break while_180_;
																																											}
																																											if (i_113_ == 5) {
																																												break while_181_;
																																											}
																																											if (i_113_ == 6) {
																																												break while_182_;
																																											}
																																											if (i_113_ == 7) {
																																												break while_183_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -9) {
																																												break while_184_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -10) {
																																												break while_185_;
																																											}
																																											if (i_113_ == 10) {
																																												break while_186_;
																																											}
																																											if (i_113_ == 11) {
																																												break while_187_;
																																											}
																																											if (i_113_ == 12) {
																																												break while_188_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -14) {
																																												break while_189_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -15) {
																																												break while_190_;
																																											}
																																											if (i_113_ == 15) {
																																												break while_191_;
																																											}
																																											if (i_113_ == 16) {
																																												break while_192_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -18) {
																																												break while_193_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -19) {
																																												break while_194_;
																																											}
																																											if (i_113_ == 19) {
																																												break while_195_;
																																											}
																																											if (i_113_ == 20) {
																																												break while_196_;
																																											}
																																											if (i_113_ == 21) {
																																												break while_197_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -23) {
																																												break while_198_;
																																											}
																																											if (i_113_ == 23) {
																																												break while_199_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -25) {
																																												break while_200_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -26) {
																																												break while_201_;
																																											}
																																											if (i_113_ == 26) {
																																												break while_202_;
																																											}
																																											if (i_113_ == 27) {
																																												break while_203_;
																																											}
																																											if (i_113_ == 28) {
																																												break while_204_;
																																											}
																																											if (i_113_ == 29) {
																																												break while_205_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -31) {
																																												break while_206_;
																																											}
																																											if (i_113_ == 31) {
																																												break while_207_;
																																											}
																																											if (i_113_ == 32) {
																																												break while_208_;
																																											}
																																											if (i_113_ == 33) {
																																												break while_209_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -35) {
																																												break while_210_;
																																											}
																																											if (i_113_ == 35) {
																																												break while_211_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -37) {
																																												break while_212_;
																																											}
																																											if (i_113_ == 37) {
																																												break while_213_;
																																											}
																																											if ((i_113_ ^ 0xffffffff) == -39) {
																																												break while_214_;
																																											}
																																											if (i_113_ != 39) {
																																												break while_216_;
																																											}
																																											if (!GameShell.cleanedStatics) {
																																												break while_215_;
																																											}
																																										}
																																										return new Class98_Sub10_Sub13();
																																									} while (false);
																																									return new Class98_Sub10_Sub22();
																																								} while (false);
																																								return new Class98_Sub10_Sub18();
																																							} while (false);
																																							return new Class98_Sub10_Sub3();
																																						} while (false);
																																						return new Class98_Sub10_Sub38();
																																					} while (false);
																																					return new Class98_Sub10_Sub24();
																																				} while (false);
																																				return new Class98_Sub10_Sub15();
																																			} while (false);
																																			return new Class98_Sub10_Sub7();
																																		} while (false);
																																		return new Class98_Sub10_Sub9();
																																	} while (false);
																																	return new Class98_Sub10_Sub11();
																																} while (false);
																																return new Class98_Sub10_Sub33();
																															} while (false);
																															return new Class98_Sub10_Sub4();
																														} while (false);
																														return new Class98_Sub10_Sub30();
																													} while (false);
																													return new Class98_Sub10_Sub8();
																												} while (false);
																												return new Class98_Sub10_Sub17();
																											} while (false);
																											return new Class98_Sub10_Sub26();
																										} while (false);
																										return new Class98_Sub10_Sub32();
																									} while (false);
																									return new Class98_Sub10_Sub6();
																								} while (false);
																								return new Class98_Sub10_Sub5_Sub1();
																							} while (false);
																							return new Class98_Sub10_Sub2();
																						} while (false);
																						return new Class98_Sub10_Sub29();
																					} while (false);
																					return new Class98_Sub10_Sub12();
																				} while (false);
																				return new Class98_Sub10_Sub39();
																			} while (false);
																			return new Class98_Sub10_Sub27();
																		} while (false);
																		return new Class98_Sub10_Sub16();
																	} while (false);
																	return new Class98_Sub10_Sub14();
																} while (false);
																return new Class98_Sub10_Sub31();
															} while (false);
															return new Class98_Sub10_Sub23();
														} while (false);
														return new Class98_Sub10_Sub28();
													} while (false);
													return new Class98_Sub10_Sub36();
												} while (false);
												return new Class98_Sub10_Sub10();
											} while (false);
											return new Class98_Sub10_Sub34();
										} while (false);
										return new Class98_Sub10_Sub37();
									} while (false);
									return new Class98_Sub10_Sub20();
								} while (false);
								return new Class98_Sub10_Sub35();
							} while (false);
							return new Class98_Sub10_Sub1();
						} while (false);
						return new Class98_Sub10_Sub25();
					} while (false);
					return new Class98_Sub10_Sub21();
				} while (false);
				return new Class98_Sub10_Sub19();
			} while (false);
			return new Class98_Sub10_Sub5();
		} while (false);
		return null;
	}

	public static final int method3636(int i, int i_124_) {
		if (Class40.anIntArrayArray367 != null) {
			return Class40.anIntArrayArray367[i][i_124_] & 0xffffff;
		}
		return 0;
	}

	private long				aLong2679;
	private int					anInt2682;
	public int[]				colours;
	private int[]				equipment;
	boolean						female;
	private long				hash;
	int							npc	= -1;

	ItemAppearanceOverride[]	overrides;

	public PlayerAppearence() {
		/* empty */
	}

	private final void computeHash(int i) {
		long[] table = WhirlpoolGenerator.CRC_64;
		/*hash = -1L;
		hash = hash >>> -9573176 ^ table[(int) (0xffL & (hash ^ anInt2682 >> 1903458056))];
		hash = hash >>> 1694471240 ^ table[(int) (0xffL & (anInt2682 ^ hash))];
		for (int id = 0; (id ^ 0xffffffff) > -13; id++) {
			hash = table[(int) (0xffL & (hash ^ equipment[id] >> -1026146760))] ^ hash >>> 1854597576;
			hash = hash >>> 649681160 ^ table[(int) (0xffL & (equipment[id] >> -773668400 ^ hash))];
			hash = hash >>> 519626696 ^ table[(int) ((equipment[id] >> -1074692952 ^ hash) & 0xffL)];
			hash = table[(int) (0xffL & (hash ^ equipment[id]))] ^ hash >>> 690165768;
		}
		for (int id = 0; id < 5; id++) {
			hash = table[(int) (0xffL & (colours[id] ^ hash))] ^ hash >>> 376623304;
		}
		hash = hash >>> -1793821816 ^ table[(int) (((!female ? 0 : 1) ^ hash) & 0xffL)];*/
		hash = -1L;
		hash = hash >>> 8 ^ table[(int) (0xffL & (anInt2682 >> 8 ^ hash))];
		hash = table[(int) ((hash ^ anInt2682) & 0xffL)] ^ hash >>> 8;
		for (int element : equipment) {
			hash = table[(int) (0xffL & (element >> 24 ^ hash))] ^ hash >>> 8;
			hash = table[(int) (0xffL & (element >> 16 ^ hash))] ^ hash >>> 8;
			hash = hash >>> 8 ^ table[(int) ((element >> 8 ^ hash) & 0xffL)];
			hash = hash >>> 8 ^ table[(int) ((hash ^ element) & 0xffL)];
		}
		if (overrides != null) {
			for (ItemAppearanceOverride override : overrides) {
				if (override != null) {
					int[] is;
					int[] is_2_;
					if (!female) {
						is = override.wear;
						is_2_ = override.manhead;
					} else {
						is = override.wear2;
						is_2_ = override.womanhead;
					}
					if (is != null) {
						for (int element : is) {
							hash = table[(int) (0xffL & (element >> 8 ^ hash))] ^ hash >>> 8;
							hash = table[(int) ((element ^ hash) & 0xffL)] ^ hash >>> 8;
						}
					}
					if (is_2_ != null) {
						for (int element : is_2_) {
							hash = table[(int) (0xffL & (hash ^ element >> 8))] ^ hash >>> 8;
							hash = hash >>> 8 ^ table[(int) ((element ^ hash) & 0xffL)];
						}
					}
					if (override.recol_d != null) {
						for (short element : override.recol_d) {
							hash = hash >>> 8 ^ table[(int) ((element >> 8 ^ hash) & 0xffL)];
							hash = table[(int) ((element ^ hash) & 0xffL)] ^ hash >>> 8;
						}
					}
					if (override.retex_d != null) {
						for (short element : override.retex_d) {
							hash = hash >>> 8 ^ table[(int) (0xffL & (hash ^ element >> 8))];
							hash = table[(int) (0xffL & (element ^ hash))] ^ hash >>> 8;
						}
					}
				}
			}
		}
		for (int i_7_ = 0; i_7_ < 5; i_7_++)
			hash = hash >>> 8 ^ table[(int) (0xffL & (this.colours[i_7_] ^ hash))];
		hash = table[(int) ((hash ^ (this.female ? 1 : 0)) & 0xffL)] ^ hash >>> 8;
	}

	public final ModelRenderer method3624(byte i, int i_0_, IdentikitDefinitionParser identikitDefParser, ItemDefinitionParser itemDefParser, RSToolkit toolkit, int i_1_, VarValues interface6, AnimationDefinition animationDefinition, NPCDefinitionParser npcDefParser, int i_2_,
			AnimationDefinitionParser animDefParser, int initialMask) {
		if (npc != -1) {
			return npcDefParser.get(5, npc).method2299(animationDefinition, false, interface6, i_0_, i_1_, animDefParser, initialMask, toolkit, null, i_2_);
		}
		int functionMask = initialMask;
		if (animationDefinition != null) {
			boolean bool = false;
			boolean bool_5_ = false;
			boolean bool_6_ = false;
			boolean bool_7_ = false;
			int i_8_ = -1;
			int i_9_ = -1;
			functionMask |= 0x20;
			i_8_ = animationDefinition.frameIds[i_1_];
			int i_11_ = i_8_ >>> 1075836880;
			AnimationSkeletonSet skeletonSetOld = animDefParser.method2624(2, i_11_);
			i_8_ &= 0xffff;
			if (skeletonSetOld != null) {
				bool_5_ |= skeletonSetOld.method1619(i_8_, 31239);
				bool |= skeletonSetOld.method1617(false, i_8_);
				bool_7_ |= skeletonSetOld.method1615(i_8_, false);
				bool_6_ |= animationDefinition.aBoolean817;
			}
			if ((animationDefinition.aBoolean825 || Class357.forcedTweening) && i_2_ != -1 && animationDefinition.frameIds.length > i_2_) {
				i_9_ = animationDefinition.frameIds[i_2_];
				int i_12_ = i_9_ >>> 334721392;
				i_9_ &= 0xffff;
				AnimationSkeletonSet skeletonSet = i_12_ == i_11_ ? skeletonSetOld : animDefParser.method2624(2, i_12_);
				if (skeletonSet != null) {
					bool_5_ |= skeletonSet.method1619(i_9_, 31239);
					bool |= skeletonSet.method1617(false, i_9_);
					bool_7_ |= skeletonSet.method1615(i_9_, false);
				}
			}
			if (bool_5_) {
				functionMask |= 0x80;
			}
			if (bool) {
				functionMask |= 0x100;
			}
			if (bool_6_) {
				functionMask |= 0x200;
			}
			if (bool_7_) {
				functionMask |= 0x400;
			}
		}
		ModelRenderer class146;
		synchronized (StoreProgressMonitor.aClass79_3411) {
			class146 = (ModelRenderer) StoreProgressMonitor.aClass79_3411.get(-128, hash);
		}
		if (class146 == null || toolkit.c(class146.functionMask(), functionMask) != 0) {
			if (class146 != null) {
				functionMask = toolkit.mergeFunctionMask(functionMask, class146.functionMask());
			}
			int i_14_ = functionMask;
			boolean bool = false;
			for (int i_15_ = 0; (i_15_ ^ 0xffffffff) > -13; i_15_++) {
				int i_16_ = equipment[i_15_];
				ItemAppearanceOverride override = null;
				if ((i_16_ & 0x40000000 ^ 0xffffffff) == -1) {
					if ((i_16_ & ~0x7fffffff) != 0 && !identikitDefParser.get(0x3fffffff & i_16_, 3).method2474(113)) {
						bool = true;
					}
				} else {
					if (overrides != null && overrides[i_15_] != null)
						override = overrides[i_15_];
					if (!itemDefParser.get(0x3fffffff & i_16_, (byte) -128).isHeadModelCached(female, 92, override))
						bool = true;
				}
			}
			if (bool) {
				return null;
			}
			BaseModel[] class178s = new BaseModel[12];
			int i_17_ = 0;
			for (int i_18_ = 0; (i_18_ ^ 0xffffffff) > -13; i_18_++) {
				int i_19_ = equipment[i_18_];
				ItemAppearanceOverride override = null;
				if ((i_19_ & 0x40000000 ^ 0xffffffff) != -1) {
					if (overrides != null && overrides[i_18_] != null)
						override = overrides[i_18_];
					BaseModel class178 = itemDefParser.get(i_19_ & 0x3fffffff, (byte) -120).method3486(female, 0, override);
					if (class178 != null) {
						class178s[i_17_++] = class178;
					}
				} else if ((~0x7fffffff & i_19_) != 0) {
					BaseModel class178 = identikitDefParser.get(i_19_ & 0x3fffffff, 3).method2476((byte) -99);
					if (class178 != null) {
						class178s[i_17_++] = class178;
					}
				}
			}
			BaseModel class178 = new BaseModel(class178s, i_17_);
			i_14_ |= 0x4000;
			class146 = toolkit.createModelRenderer(class178, i_14_, Class81.anInt624, 64, 768);
			for (int i_20_ = 0; i_20_ < 5; i_20_++) {
				for (int i_21_ = 0; (HashTableIterator.colourToReplace.length ^ 0xffffffff) < (i_21_ ^ 0xffffffff); i_21_++) {
					if (colours[i_20_] < HashTableIterator.colourToReplace[i_21_][i_20_].length) {
						class146.recolour(Class98_Sub10_Sub11.aShortArrayArray5597[i_21_][i_20_], HashTableIterator.colourToReplace[i_21_][i_20_][colours[i_20_]]);
					}
				}
			}
			class146.updateFunctionMask(functionMask);
			synchronized (StoreProgressMonitor.aClass79_3411) {
				StoreProgressMonitor.aClass79_3411.put(hash, class146, (byte) -80);
			}
		}
		if (animationDefinition == null) {
			return class146;
		}
		ModelRenderer class146_22_ = class146.method2341((byte) 4, functionMask, true);
		if (i <= 83) {
			hash = -7L;
		}
		class146_22_ = animationDefinition.method937(i_2_, i_0_, initialMask, 127, class146, i_1_);
		return class146_22_;
	}

	public final ModelRenderer method3626(int i, AnimationDefinition class97, int i_26_, int i_27_, int i_28_, int i_29_, int i_30_, int i_31_, IdentikitDefinitionParser class83, RSToolkit var_ha, int i_32_, AnimationDefinitionParser class183) {
		int i_33_ = i_31_;
		if (class97 != null) {
			boolean bool = false;
			boolean bool_34_ = false;
			boolean bool_35_ = false;
			boolean bool_36_ = false;
			int i_37_ = -1;
			int i_38_ = -1;
			i_37_ = class97.frameIds[i];
			i_33_ |= 0x20;
			int i_40_ = i_37_ >>> -1674141136;
			AnimationSkeletonSet class98_sub46_sub16 = class183.method2624(i_32_ ^ 0x102, i_40_);
			i_37_ &= 0xffff;
			if (class98_sub46_sub16 != null) {
				bool_34_ |= class98_sub46_sub16.method1619(i_37_, 31239);
				bool |= class98_sub46_sub16.method1617(false, i_37_);
				bool_36_ |= class98_sub46_sub16.method1615(i_37_, false);
				bool_35_ |= class97.aBoolean817;
			}
			if ((class97.aBoolean825 || Class357.forcedTweening) && i_30_ != -1 && class97.frameIds.length > i_30_) {
				i_38_ = class97.frameIds[i_30_];
				int i_41_ = i_38_ >>> 1835918000;
				i_38_ &= 0xffff;
				AnimationSkeletonSet class98_sub46_sub16_42_;
				if ((i_41_ ^ 0xffffffff) == (i_40_ ^ 0xffffffff)) {
					class98_sub46_sub16_42_ = class98_sub46_sub16;
				} else {
					class98_sub46_sub16_42_ = class183.method2624(2, i_38_ >>> 243215344);
				}
				if (class98_sub46_sub16_42_ != null) {
					bool_34_ |= class98_sub46_sub16_42_.method1619(i_38_, i_32_ + 30983);
					bool |= class98_sub46_sub16_42_.method1617(false, i_38_);
					bool_36_ |= class98_sub46_sub16_42_.method1615(i_38_, false);
				}
			}
			if (bool_34_) {
				i_33_ |= 0x80;
			}
			if (bool) {
				i_33_ |= 0x100;
			}
			if (bool_35_) {
				i_33_ |= 0x200;
			}
			if (bool_36_) {
				i_33_ |= 0x400;
			}
		}
		long l = i_29_ << -715025360 | (long) i_27_ << 270913248 | i_28_;
		ModelRenderer class146;
		synchronized (StoreProgressMonitor.aClass79_3411) {
			class146 = (ModelRenderer) StoreProgressMonitor.aClass79_3411.get(-125, l);
		}
		if (class146 == null || (var_ha.c(class146.functionMask(), i_33_) ^ 0xffffffff) != -1) {
			if (class146 != null) {
				i_33_ = var_ha.mergeFunctionMask(i_33_, class146.functionMask());
			}
			int i_43_ = i_33_;
			BaseModel[] class178s = new BaseModel[3];
			int i_44_ = 0;
			if (class83.get(i_28_, i_32_ + -253).method2474(i_32_ + -150) && class83.get(i_29_, i_32_ + -253).method2474(125) && class83.get(i_27_, 3).method2474(i_32_ + -150)) {
				BaseModel class178 = class83.get(i_28_, 3).method2476((byte) -103);
				if (class178 != null) {
					class178s[i_44_++] = class178;
				}
				class178 = class83.get(i_29_, i_32_ + -253).method2476((byte) 68);
				if (class178 != null) {
					class178s[i_44_++] = class178;
				}
				class178 = class83.get(i_27_, i_32_ ^ 0x103).method2476((byte) 110);
				if (class178 != null) {
					class178s[i_44_++] = class178;
				}
				i_43_ |= 0x4000;
				class178 = new BaseModel(class178s, i_44_);
				class146 = var_ha.createModelRenderer(class178, i_43_, Class81.anInt624, 64, 768);
				for (int i_45_ = 0; (i_45_ ^ 0xffffffff) > -6; i_45_++) {
					for (int i_46_ = 0; HashTableIterator.colourToReplace.length > i_46_; i_46_++) {
						if (colours[i_45_] < HashTableIterator.colourToReplace[i_46_][i_45_].length) {
							class146.recolour(Class98_Sub10_Sub11.aShortArrayArray5597[i_46_][i_45_], HashTableIterator.colourToReplace[i_46_][i_45_][colours[i_45_]]);
						}
					}
				}
				class146.updateFunctionMask(i_33_);
				synchronized (StoreProgressMonitor.aClass79_3411) {
					StoreProgressMonitor.aClass79_3411.put(l, class146, (byte) -80);
				}
			} else {
				return null;
			}
		}
		if (class97 == null) {
			return class146;
		}
		class146 = class146.method2341((byte) 4, i_33_, true);
		class146 = class97.method937(i_30_, i_26_, i_31_, -43, class146, i);
		return class146;
	}

	public final ModelRenderer method3628(RenderAnimDefinitionParser class257, AnimationDefinition animation, AnimationDefinition class97_50_, AnimationDefinitionParser class183, int i, IdentikitDefinitionParser class83, int[] is, ItemDefinitionParser class205, boolean bool, int i_51_,
			Class226[] class226s, NPCDefinitionParser npcLoader, VarValues interface6, boolean bool_52_, int initialMask, int i_54_, int i_55_, int i_56_, int i_57_, int i_58_, RSToolkit toolkit) {
		if ((npc ^ 0xffffffff) != 0) {
			return npcLoader.get(5, npc).method2301(i, i_58_, is, i_57_, (byte) 101, animation, class226s, toolkit, i_51_, i_56_, interface6, null, i_55_, class257, initialMask, i_54_, class183, class97_50_);
		}
		int functionMask = initialMask;
		long hash = this.hash;
		int[] usedEquipment = equipment;
		if (animation != null && (animation.offhand >= 0 || animation.mainhand >= 0)) {
			usedEquipment = new int[12];
			for (int part = 0; part < 12; part++) {
				usedEquipment[part] = equipment[part];
			}
			if ((animation.offhand ^ 0xffffffff) <= -1) {
				if (animation.offhand != 65535) {
					usedEquipment[5] = Class41.or(animation.offhand, 1073741824);
					hash ^= (long) usedEquipment[5] << 1378327584;
				} else {
					usedEquipment[5] = 0;
					hash ^= ~0xffffffffL;
				}
			}
			if (animation.mainhand >= 0) {
				if ((animation.mainhand ^ 0xffffffff) != -65536) {
					usedEquipment[3] = Class41.or(animation.mainhand, 1073741824);
					hash ^= usedEquipment[3];
				} else {
					usedEquipment[3] = 0;
					hash ^= 0xffffffffL;
				}
			}
		}
		boolean bool_62_ = false;
		boolean bool_63_ = false;
		boolean bool_64_ = false;
		boolean bool_65_ = animation != null || class97_50_ != null;
		int i_66_ = class226s == null ? 0 : class226s.length;
		for (int i_67_ = 0; (i_67_ ^ 0xffffffff) > (i_66_ ^ 0xffffffff); i_67_++) {
			DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_67_] = null;
			if (class226s[i_67_] != null) {
				AnimationDefinition class97_68_ = class183.method2623(class226s[i_67_].anInt1700, 16383);
				if (class97_68_.frameIds != null) {
					bool_65_ = true;
					FileOnDisk.aClass97Array3023[i_67_] = class97_68_;
					int i_69_ = class226s[i_67_].anInt1702;
					int i_70_ = class226s[i_67_].anInt1701;
					int i_71_ = class97_68_.frameIds[i_69_];
					DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_67_] = class183.method2624(2, i_71_ >>> -1761370288);
					i_71_ &= 0xffff;
					ScalingSpriteLSEConfig.anIntArray3546[i_67_] = i_71_;
					if (DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_67_] != null) {
						bool_63_ |= DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_67_].method1619(i_71_, 31239);
						bool_62_ |= DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_67_].method1617(false, i_71_);
						bool_64_ = bool_64_ | DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_67_].method1615(i_71_, !bool);
					}
					if (!class97_68_.aBoolean825 && !Class357.forcedTweening || i_70_ == -1 || (i_70_ ^ 0xffffffff) <= (class97_68_.frameIds.length ^ 0xffffffff)) {
						Class98_Sub10_Sub17.anIntArray5624[i_67_] = 0;
						SceneGraphNodeList.anIntArray1631[i_67_] = 0;
						Class351.aClass98_Sub46_Sub16Array2924[i_67_] = null;
						AwtLoadingScreen.anIntArray3355[i_67_] = -1;
					} else {
						Class98_Sub10_Sub17.anIntArray5624[i_67_] = class97_68_.frameLengths[i_69_];
						SceneGraphNodeList.anIntArray1631[i_67_] = class226s[i_67_].anInt1707;
						int i_72_ = class97_68_.frameIds[i_70_];
						Class351.aClass98_Sub46_Sub16Array2924[i_67_] = class183.method2624(2, i_72_ >>> 347419536);
						i_72_ &= 0xffff;
						AwtLoadingScreen.anIntArray3355[i_67_] = i_72_;
						if (Class351.aClass98_Sub46_Sub16Array2924[i_67_] != null) {
							bool_63_ |= Class351.aClass98_Sub46_Sub16Array2924[i_67_].method1619(i_72_, 31239);
							bool_62_ |= Class351.aClass98_Sub46_Sub16Array2924[i_67_].method1617(false, i_72_);
							bool_64_ |= Class351.aClass98_Sub46_Sub16Array2924[i_67_].method1615(i_72_, false);
						}
					}
				}
			}
		}
		int i_73_ = -1;
		int i_74_ = -1;
		int i_75_ = 0;
		AnimationSkeletonSet class98_sub46_sub16 = null;
		AnimationSkeletonSet class98_sub46_sub16_76_ = null;
		int i_77_ = -1;
		int i_78_ = -1;
		int i_79_ = 0;
		AnimationSkeletonSet class98_sub46_sub16_80_ = null;
		AnimationSkeletonSet class98_sub46_sub16_81_ = null;
		if (bool_65_) {
			functionMask |= 0x20;
			if (animation != null) {
				i_73_ = animation.frameIds[i];
				int i_82_ = i_73_ >>> -454090480;
				i_73_ &= 0xffff;
				class98_sub46_sub16 = class183.method2624(2, i_82_);
				if (class98_sub46_sub16 != null) {
					bool_63_ |= class98_sub46_sub16.method1619(i_73_, 31239);
					bool_62_ |= class98_sub46_sub16.method1617(false, i_73_);
					bool_64_ |= class98_sub46_sub16.method1615(i_73_, false);
				}
				if ((animation.aBoolean825 || Class357.forcedTweening) && (i_56_ ^ 0xffffffff) != 0 && animation.frameIds.length > i_56_) {
					i_74_ = animation.frameIds[i_56_];
					i_75_ = animation.frameLengths[i];
					int i_83_ = i_74_ >>> -240024528;
					class98_sub46_sub16_76_ = (i_83_ ^ 0xffffffff) == (i_82_ ^ 0xffffffff) ? class98_sub46_sub16 : class183.method2624(2, i_83_);
					i_74_ &= 0xffff;
					if (class98_sub46_sub16_76_ != null) {
						bool_63_ |= class98_sub46_sub16_76_.method1619(i_74_, 31239);
						bool_62_ |= class98_sub46_sub16_76_.method1617(false, i_74_);
						bool_64_ |= class98_sub46_sub16_76_.method1615(i_74_, false);
					}
				}
			}
			if (class97_50_ != null) {
				i_77_ = class97_50_.frameIds[i_57_];
				int i_84_ = i_77_ >>> 1300157968;
				i_77_ &= 0xffff;
				class98_sub46_sub16_80_ = class183.method2624(2, i_84_);
				if (class98_sub46_sub16_80_ != null) {
					bool_63_ |= class98_sub46_sub16_80_.method1619(i_77_, 31239);
					bool_62_ = bool_62_ | class98_sub46_sub16_80_.method1617(!bool, i_77_);
					bool_64_ |= class98_sub46_sub16_80_.method1615(i_77_, false);
				}
				if ((class97_50_.aBoolean825 || Class357.forcedTweening) && i_54_ != -1 && class97_50_.frameIds.length > i_54_) {
					i_79_ = class97_50_.frameLengths[i_57_];
					i_78_ = class97_50_.frameIds[i_54_];
					int i_85_ = i_78_ >>> -1612855760;
					class98_sub46_sub16_81_ = (i_85_ ^ 0xffffffff) != (i_84_ ^ 0xffffffff) ? class183.method2624(2, i_85_) : class98_sub46_sub16_80_;
					i_78_ &= 0xffff;
					if (class98_sub46_sub16_81_ != null) {
						bool_63_ |= class98_sub46_sub16_81_.method1619(i_78_, 31239);
						bool_62_ |= class98_sub46_sub16_81_.method1617(false, i_78_);
						bool_64_ |= class98_sub46_sub16_81_.method1615(i_78_, false);
					}
				}
			}
			if (bool_63_) {
				functionMask |= 0x80;
			}
			if (bool_62_) {
				functionMask |= 0x100;
			}
			if (bool_64_) {
				functionMask |= 0x400;
			}
		}
		ModelRenderer model;
		synchronized (Class211.aClass79_1594) {
			model = (ModelRenderer) Class211.aClass79_1594.get(-119, hash);
		}
		RenderAnimDefinition renderAnim = null;
		if ((anInt2682 ^ 0xffffffff) != 0) {
			renderAnim = class257.method3199(false, anInt2682);
		}
		if (model == null || (toolkit.c(model.functionMask(), functionMask) ^ 0xffffffff) != -1) {
			if (model != null) {
				functionMask = toolkit.mergeFunctionMask(functionMask, model.functionMask());
			}
			int i_86_ = functionMask;
			boolean bool_87_ = false;
			for (int equipment = 0; equipment < 12; equipment++) {
				ItemAppearanceOverride override = null;
				int equipmentPart = usedEquipment[equipment];
				if ((equipmentPart & 0x40000000 ^ 0xffffffff) == -1) {
					if ((equipmentPart & ~0x7fffffff ^ 0xffffffff) != -1 && !class83.get(0x3fffffff & equipmentPart, 3).method2475(0)) {
						bool_87_ = true;
					}
				} else {
					if (/*!bool_58_ &&*/ overrides != null && overrides[equipment] != null)
						override = overrides[equipment];

					if (!class205.get(equipmentPart & 0x3fffffff, (byte) -128).isWornModelCached(0, female, override))
						bool_87_ = true;
				}
			}
			if (!bool_87_) {
				BaseModel[] baseModel = new BaseModel[12];
				for (int i_90_ = 0; (i_90_ ^ 0xffffffff) > -13; i_90_++) {
					int i_91_ = usedEquipment[i_90_];
					ItemAppearanceOverride override = null;
					if ((i_91_ & 0x40000000 ^ 0xffffffff) == -1) {
						if ((~0x7fffffff & i_91_ ^ 0xffffffff) != -1) {
							BaseModel class178 = class83.get(0x3fffffff & i_91_, 3).method2473(2);
							if (class178 != null) {
								baseModel[i_90_] = class178;
							}
						}
					} else {
						if (/*!bool_63_ &&*/ overrides != null && overrides[i_90_] != null)
							override = overrides[i_90_];
						BaseModel class178 = class205.get(0x3fffffff & i_91_, (byte) -124).method3500(female, override, 124);
						if (class178 != null) {
							baseModel[i_90_] = class178;
						}
					}
				}
				if (renderAnim != null && renderAnim.anIntArrayArray2366 != null) {
					for (int i_92_ = 0; renderAnim.anIntArrayArray2366.length > i_92_; i_92_++) {
						if (baseModel[i_92_] != null) {
							int i_93_ = 0;
							int i_94_ = 0;
							int i_95_ = 0;
							int i_96_ = 0;
							int i_97_ = 0;
							int i_98_ = 0;
							if (renderAnim.anIntArrayArray2366[i_92_] != null) {
								i_98_ = renderAnim.anIntArrayArray2366[i_92_][5] << -264909725;
								i_96_ = renderAnim.anIntArrayArray2366[i_92_][3] << 819372707;
								i_94_ = renderAnim.anIntArrayArray2366[i_92_][1];
								i_97_ = renderAnim.anIntArrayArray2366[i_92_][4] << 1212264099;
								i_93_ = renderAnim.anIntArrayArray2366[i_92_][0];
								i_95_ = renderAnim.anIntArrayArray2366[i_92_][2];
							}
							if ((i_96_ ^ 0xffffffff) != -1 || (i_97_ ^ 0xffffffff) != -1 || i_98_ != 0) {
								baseModel[i_92_].method2600(i_98_, i_96_, (byte) 117, i_97_);
							}
							if (i_93_ != 0 || i_94_ != 0 || i_95_ != 0) {
								baseModel[i_92_].method2597(i_95_, i_93_, (byte) 104, i_94_);
							}
						}
					}
				}
				i_86_ |= 0x4000;
				BaseModel class178 = new BaseModel(baseModel, baseModel.length);
				model = toolkit.createModelRenderer(class178, i_86_, Class81.anInt624, 64, 850);
				for (int i_99_ = 0; i_99_ < 5; i_99_++) {
					for (int i_100_ = 0; (HashTableIterator.colourToReplace.length ^ 0xffffffff) < (i_100_ ^ 0xffffffff); i_100_++) {
						if ((HashTableIterator.colourToReplace[i_100_][i_99_].length ^ 0xffffffff) < (colours[i_99_] ^ 0xffffffff)) {
							model.recolour(Class98_Sub10_Sub11.aShortArrayArray5597[i_100_][i_99_], HashTableIterator.colourToReplace[i_100_][i_99_][colours[i_99_]]);
						}
					}
				}
				if (bool_52_) {
					model.updateFunctionMask(functionMask);
					synchronized (Class211.aClass79_1594) {
						Class211.aClass79_1594.put(hash, model, (byte) -80);
					}
					aLong2679 = hash;
				}
			} else {
				if (aLong2679 != -1L) {
					synchronized (Class211.aClass79_1594) {
						model = (ModelRenderer) Class211.aClass79_1594.get(-123, aLong2679);
					}
				}
				if (model == null || toolkit.c(model.functionMask(), functionMask) != 0) {
					return null;
				}
			}
		}
		ModelRenderer class146_101_ = model.method2341((byte) 4, functionMask, bool);
		boolean bool_102_ = false;
		if (is != null) {
			for (int i_103_ = 0; i_103_ < 12; i_103_++) {
				if ((is[i_103_] ^ 0xffffffff) != 0) {
					bool_102_ = true;
				}
			}
		}
		if (!bool_65_ && !bool_102_) {
			return class146_101_;
		}
		Matrix[] class111s = null;
		if (renderAnim != null) {
			class111s = renderAnim.method3481(toolkit, (byte) 45);
		}
		if (bool_102_ && class111s != null) {
			for (int i_104_ = 0; (i_104_ ^ 0xffffffff) > -13; i_104_++) {
				if (class111s[i_104_] != null) {
					class146_101_.method2331(class111s[i_104_], 1 << i_104_, true);
				}
			}
		}
		int i_105_ = 0;
		int i_106_ = 1;
		for (/**/; i_66_ > i_105_; i_105_++) {
			if (DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_105_] != null) {
				class146_101_.method2323(ScalingSpriteLSEConfig.anIntArray3546[i_105_], -1 + SceneGraphNodeList.anIntArray1631[i_105_], Class351.aClass98_Sub46_Sub16Array2924[i_105_], -27033, i_106_, DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_105_], false,
						AwtLoadingScreen.anIntArray3355[i_105_], null, 0, Class98_Sub10_Sub17.anIntArray5624[i_105_]);
			}
			i_106_ <<= 1;
		}
		if (bool_102_) {
			for (int i_107_ = 0; (i_107_ ^ 0xffffffff) > -13; i_107_++) {
				if (is[i_107_] != -1) {
					int i_108_ = -i_58_ + is[i_107_];
					i_108_ &= 0x3fff;
					Matrix class111 = toolkit.createMatrix();
					class111.method2101(i_108_);
					class146_101_.method2331(class111, 1 << i_107_, false);
				}
			}
		}
		if (bool_102_ && class111s != null) {
			for (int i_109_ = 0; i_109_ < 12; i_109_++) {
				if (class111s[i_109_] != null) {
					class146_101_.method2331(class111s[i_109_], 1 << i_109_, false);
				}
			}
		}
		if (class98_sub46_sub16 == null || class98_sub46_sub16_80_ == null) {
			if (class98_sub46_sub16 != null) {
				class146_101_.method2338(-1 + i_55_, class98_sub46_sub16, i_73_, class98_sub46_sub16_76_, false, 0, 112, i_75_, i_74_);
			} else if (class98_sub46_sub16_80_ != null) {
				class146_101_.method2338(i_51_ - 1, class98_sub46_sub16_80_, i_77_, class98_sub46_sub16_81_, false, 0, 119, i_79_, i_78_);
			}
		} else {
			class146_101_.method2321(i_75_, i_73_, class98_sub46_sub16, class98_sub46_sub16_76_, animation.animationFlowControl, i_79_, 28777, i_77_, class98_sub46_sub16_80_, i_78_, -1 + i_55_, false, class98_sub46_sub16_81_, -1 + i_51_, i_74_);
		}
		for (int i_110_ = 0; i_66_ > i_110_; i_110_++) {
			DecoratedProgressBarLoadingScreenElement.aClass98_Sub46_Sub16Array5468[i_110_] = null;
			Class351.aClass98_Sub46_Sub16Array2924[i_110_] = null;
			FileOnDisk.aClass97Array3023[i_110_] = null;
		}
		return class146_101_;
	}

	public final void method3634(int i, int i_121_, ItemDefinitionParser class205, int i_122_) {
		if (i_122_ != 1073741824) {
			female = false;
		}
		if (i != -1) {
			if (class205.get(i, (byte) -118) != null) {
				equipment[i_121_] = Class41.or(1073741824, i);
				computeHash(i_122_ ^ 0x4000005d);
			}
		} else {
			equipment[i_121_] = 0;
		}
	}

	public final void setColour(int colour, int index, int i_118_) {
		if (i_118_ == -9) {
			colours[index] = colour;
			computeHash(87);
		}
	}

	public final void setKit(int i, int slot, IdentikitDefinitionParser loader, int id) {
		int index = Class370.IDENTIKIT_EQUIPMENT_SLOTS[slot];
		if (i == 12 && loader.get(id, 3) != null) {
			equipment[index] = Class41.or(id, -2147483648);
			computeHash(i + 105);
		}
	}

	public final void setMale(boolean male, boolean bool_123_) {
		female = male;
		computeHash(123);
	}

	public final void update(int[] _equips, ItemAppearanceOverride[] overrides, int i, boolean female, int[] bodyColours, int i_48_, int _npc) {
		if (i_48_ != anInt2682) {
			anInt2682 = i_48_;
		}
		equipment = _equips;
		this.overrides = overrides;
		this.female = female;
		npc = _npc;
		colours = bodyColours;
		computeHash(95);
		// System.out.println("Equips: " + Arrays.toString(equipment) + "
		// Female: " + female + " Npc: " + npc + " Colours: " +
		// Arrays.toString(colours));
	}
}

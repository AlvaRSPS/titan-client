/* Class246_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;

public abstract class Char extends SceneGraphNode {
	public static IncomingOpcode	SEND_GRAND_EXCHANGE_OFFER	= new IncomingOpcode(83, 20);
	public static Js5				animationJs5;

	public static final void method2723(int i, int i_0_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[0][i][i_0_];
		for (int i_1_ = 0; i_1_ < 3; i_1_++) {
			Class172 class172_2_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_1_][i][i_0_] = QuickChatCategory.aClass172ArrayArrayArray5948[i_1_ + 1][i][i_0_];
			if (class172_2_ != null) {
				for (Class154 class154 = class172_2_.aClass154_1325; class154 != null; class154 = class154.aClass154_1233) {
					Class246_Sub3_Sub4 class246_sub3_sub4 = class154.aClass246_Sub3_Sub4_1232;
					if (class246_sub3_sub4.aShort6158 == i && class246_sub3_sub4.aShort6157 == i_0_) {
						class246_sub3_sub4.plane--;
					}
				}
				if (class172_2_.aClass246_Sub3_Sub1_1332 != null) {
					class172_2_.aClass246_Sub3_Sub1_1332.plane--;
				}
				if (class172_2_.aClass246_Sub3_Sub3_1324 != null) {
					class172_2_.aClass246_Sub3_Sub3_1324.plane--;
				}
				if (class172_2_.aClass246_Sub3_Sub3_1333 != null) {
					class172_2_.aClass246_Sub3_Sub3_1333.plane--;
				}
				if (class172_2_.aClass246_Sub3_Sub5_1334 != null) {
					class172_2_.aClass246_Sub3_Sub5_1334.plane--;
				}
				if (class172_2_.aClass246_Sub3_Sub5_1326 != null) {
					class172_2_.aClass246_Sub3_Sub5_1326.plane--;
				}
			}
		}
		if (QuickChatCategory.aClass172ArrayArrayArray5948[0][i][i_0_] == null) {
			QuickChatCategory.aClass172ArrayArrayArray5948[0][i][i_0_] = new Class172(0);
			QuickChatCategory.aClass172ArrayArrayArray5948[0][i][i_0_].aByte1322 = (byte) 1;
		}
		QuickChatCategory.aClass172ArrayArrayArray5948[0][i][i_0_].aClass172_1330 = class172;
		QuickChatCategory.aClass172ArrayArrayArray5948[3][i][i_0_] = null;
	}

	public static final Class98_Sub47 method2979(int i) {
		if (WorldMap.aClass148_2065 == null || PointLight.aClass157_3835 == null) {
			return null;
		}
		Class98_Sub47 class98_sub47 = (Class98_Sub47) PointLight.aClass157_3835.method2503(1000);
		for (/**/; class98_sub47 != null; class98_sub47 = (Class98_Sub47) PointLight.aClass157_3835.method2503(1000)) {
			WorldMapInfoDefinition class24 = WorldMap.aClass341_2057.get(123, class98_sub47.anInt4268);
			if (class24 != null && class24.aBoolean241 && class24.method284(35, WorldMap.anInterface6_2060)) {
				return class98_sub47;
			}
		}
		return null;
	}

	public static final void method2984(int i, byte i_7_, int i_8_, int i_9_) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_9_, i_7_ + 2, 11);
		class98_sub46_sub17.method1626((byte) -103);
		class98_sub46_sub17.anInt6054 = i_8_;
		class98_sub46_sub17.headId = i;
	}

	boolean			aBoolean5078;
	public boolean	aBoolean5082	= false;
	byte			collisionPlane;
	public Char		animator;
	public int		anInt5080;
	public int		anInt5083;
	public int		anInt5085;

	public int		anInt5089;

	public int		boundExtentsX;

	public int		boundExtentsZ;

	public byte		plane;

	public Char() {
		/* empty */
	}

	abstract Class228 method2974(byte i, RSToolkit var_ha);

	abstract Class246_Sub1 method2975(RSToolkit var_ha, int i);

	abstract boolean method2976(int i, RSToolkit var_ha, byte i_0_, int i_1_);

	abstract boolean method2977(RSToolkit var_ha, byte i);

	public abstract boolean method2978(int i);

	abstract int method2980(int i, PointLight[] class98_sub5s);

	public abstract void method2981(Char class246_sub3_3_, byte i, boolean bool, int i_4_, RSToolkit var_ha, int i_5_, int i_6_);

	abstract boolean method2982(byte i);

	public abstract int method2985(boolean bool);

	int method2986(int i) {
		try {
			if (i != -14240) {
				method2976(82, null, (byte) -22, -7);
			}
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gg.FB(" + i + ')');
		}
	}

	public abstract boolean method2987(int i);

	public abstract void method2988(RSToolkit var_ha, int i);

	public final int method2989(int i, boolean bool, PointLight[] class98_sub5s, int i_10_) {
		try {
			if (bool != false) {
				method2988(null, -104);
			}
			long l = SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[plane][i][i_10_];
			long l_11_ = 0L;
			int i_12_ = 0;
			while (l_11_ <= 48L) {
				int i_13_ = (int) (0xffffL & l >> (int) l_11_);
				if (i_13_ <= 0) {
					break;
				}
				l_11_ += 16L;
				class98_sub5s[i_12_++] = Class98_Sub10_Sub31.aClass1Array5717[-1 + i_13_].light;
			}
			for (int i_14_ = i_12_; (i_14_ ^ 0xffffffff) > -5; i_14_++) {
				class98_sub5s[i_14_] = null;
			}
			return i_12_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gg.MB(" + i + ',' + bool + ',' + (class98_sub5s != null ? "{...}" : "null") + ',' + i_10_ + ')');
		}
	}

	public abstract int method2990(int i);

	abstract boolean method2991(boolean bool);

	public abstract void method2992(byte i);
}

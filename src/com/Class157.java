/* Class157 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;

public final class Class157 {
	public static float aFloat1249;

	public static void method2507(int i) {
		do {
			try {
				PacketBufferManager.pool100 = null;
				if (i > 19) {
					break;
				}
				aFloat1249 = -0.7901263F;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "kk.D(" + i + ')');
			}
			break;
		} while (false);
	}

	private LinkedList	aClass148_1250;

	private Node		aClass98_1251;

	public Class157() {
		/* empty */
	}

	public Class157(LinkedList class148) {
		try {
			aClass148_1250 = class148;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kk.<init>(" + (class148 != null ? "{...}" : "null") + ')');
		}
	}

	public final Node method2503(int i) {
		try {
			if (i != 1000) {
				return null;
			}
			Node class98 = aClass98_1251;
			if (aClass148_1250.top == class98) {
				aClass98_1251 = null;
				return null;
			}
			aClass98_1251 = class98.next;
			return class98;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kk.F(" + i + ')');
		}
	}

	public final Node method2504(byte i) {
		try {
			if (i >= -113) {
				return null;
			}
			Node class98 = aClass148_1250.top.next;
			if (class98 == aClass148_1250.top) {
				aClass98_1251 = null;
				return null;
			}
			aClass98_1251 = class98.next;
			return class98;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kk.B(" + i + ')');
		}
	}

	public final void method2505(byte i, LinkedList class148) {
		try {
			aClass148_1250 = class148;
			if (i != 26) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kk.C(" + i + ',' + (class148 != null ? "{...}" : "null") + ')');
		}
	}
}

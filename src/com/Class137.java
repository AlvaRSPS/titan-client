/* Class137 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;

public final class Class137 {
	public static RtAwtFontWrapper	aClass326_1080;
	public static int				anInt1079;
	public static int[]				anIntArray1081	= { 1, 2, 4, 8 };

	public static final void addChatMessage(String nameSimple, int type, String displayName, String message, int index, String clan, byte i_5_, int flags, String nameUnfiltered) {
		ChatLine chatLine = Class98_Sub46_Sub3.buffer[99];
		for (int count = 99; count > 0; count--) {
			Class98_Sub46_Sub3.buffer[count] = Class98_Sub46_Sub3.buffer[count + -1];
		}
		if (chatLine != null) {
			chatLine.update(index, message, displayName, clan, flags, nameUnfiltered, type, nameSimple, -30991);
		} else {
			chatLine = new ChatLine(type, flags, displayName, nameUnfiltered, nameSimple, clan, index, message);
		}
		RectangleLoadingScreenElement.chatLinesCount++;
		Class98_Sub46_Sub3.buffer[0] = chatLine;
		ClientInventory.timestampChatboxUpdate = WorldMapInfoDefinition.logicTimer;
	}
}

/* Class346 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class346 {
	public static float		aFloat2900;
	public static int[][]	anIntArrayArray2893	= { { 2, 4, 6, 0 }, { 0, 2, 3, 5, 6, 4 }, { 0, 1, 4, 5 }, { 4, 6, 0, 2 }, { 2, 4, 0 }, { 0, 2, 4 }, { 6, 0, 1, 2, 4, 5 }, { 0, 1, 2, 4, 6, 7 }, { 4, 7, 6, 0 }, { 0, 8, 6, 1, 9, 2, 9, 4 }, { 2, 9, 4, 0, 8, 6 }, { 2, 11, 3, 7, 10, 10, 6, 6 }, { 2, 4, 6,
			0 } };

	public static final char method3829(char c, int i) {
		try {
			if (i != 0) {
				return '\uff8e';
			}
			int i_7_ = c;
			while_241_: do {
				while_240_: do {
					while_239_: do {
						while_238_: do {
							while_237_: do {
								while_236_: do {
									while_235_: do {
										while_234_: do {
											while_233_: do {
												while_232_: do {
													do {
														if (i_7_ != 32 && i_7_ != 160 && (i_7_ ^ 0xffffffff) != -96 && (i_7_ ^ 0xffffffff) != -46) {
															if (i_7_ == 91 || (i_7_ ^ 0xffffffff) == -94 || i_7_ == 35) {
																break;
															}
															if (i_7_ == 224 || (i_7_ ^ 0xffffffff) == -226 || (i_7_ ^ 0xffffffff) == -227 || i_7_ == 228 || i_7_ == 227 || (i_7_ ^ 0xffffffff) == -193 || (i_7_ ^ 0xffffffff) == -194 || (i_7_ ^ 0xffffffff) == -195 || i_7_ == 196 || i_7_ == 195) {
																break while_232_;
															}
															if ((i_7_ ^ 0xffffffff) == -233 || i_7_ == 233 || (i_7_ ^ 0xffffffff) == -235 || i_7_ == 235 || (i_7_ ^ 0xffffffff) == -201 || i_7_ == 201 || i_7_ == 202 || i_7_ == 203) {
																break while_233_;
															}
															if (i_7_ == 237 || (i_7_ ^ 0xffffffff) == -239 || (i_7_ ^ 0xffffffff) == -240 || i_7_ == 205 || (i_7_ ^ 0xffffffff) == -207 || i_7_ == 207) {
																break while_234_;
															}
															if ((i_7_ ^ 0xffffffff) == -243 || i_7_ == 243 || (i_7_ ^ 0xffffffff) == -245 || i_7_ == 246 || (i_7_ ^ 0xffffffff) == -246 || (i_7_ ^ 0xffffffff) == -211 || (i_7_ ^ 0xffffffff) == -212 || (i_7_ ^ 0xffffffff) == -213 || i_7_ == 214
																	|| i_7_ == 213) {
																break while_235_;
															}
															if (i_7_ == 249 || (i_7_ ^ 0xffffffff) == -251 || (i_7_ ^ 0xffffffff) == -252 || (i_7_ ^ 0xffffffff) == -253 || (i_7_ ^ 0xffffffff) == -218 || (i_7_ ^ 0xffffffff) == -219 || i_7_ == 219 || i_7_ == 220) {
																break while_236_;
															}
															if ((i_7_ ^ 0xffffffff) == -232 || (i_7_ ^ 0xffffffff) == -200) {
																break while_237_;
															}
															if (i_7_ == 255 || (i_7_ ^ 0xffffffff) == -377) {
																break while_238_;
															}
															if (i_7_ == 241 || (i_7_ ^ 0xffffffff) == -210) {
																break while_239_;
															}
															if ((i_7_ ^ 0xffffffff) != -224) {
																break while_241_;
															}
															if (!GameShell.cleanedStatics) {
																break while_240_;
															}
														}
														return '_';
													} while (false);
													return c;
												} while (false);
												return 'a';
											} while (false);
											return 'e';
										} while (false);
										return 'i';
									} while (false);
									return 'o';
								} while (false);
								return 'u';
							} while (false);
							return 'c';
						} while (false);
						return 'y';
					} while (false);
					return 'n';
				} while (false);
				return 'b';
			} while (false);
			return Character.toLowerCase(c);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vaa.B(" + c + ',' + i + ')');
		}
	}

	public static void method3830(int i) {
		do {
			try {
				anIntArrayArray2893 = null;
				if (i == -211) {
					break;
				}
				aFloat2900 = -0.43294057F;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vaa.E(" + i + ')');
			}
			break;
		} while (false);
	}

	private boolean		aBoolean2895;
	private Class175	aClass175_2901;
	private Class175[]	aClass175Array2899;
	private Class175[]	aClass175Array2903;
	private Sprite		aClass332_2894;
	private int			anInt2892;
	private int			anInt2896	= -1;
	private int			anInt2897;
	private int			anInt2898;
	private int			anInt2902;

	private int			anInt2904;

	private int			anInt2905;

	public Class346(int i, Class175[] class175s, int i_22_, int i_23_, int i_24_, int i_25_) {

		aBoolean2895 = true;
		anInt2905 = i_25_;
		aClass175Array2903 = class175s;
		anInt2902 = i;
		anInt2898 = i_23_;
		anInt2892 = i_24_;
		if (class175s == null) {
			aClass175Array2899 = null;
			aClass175_2901 = null;
		} else {
			aClass175Array2899 = new Class175[class175s.length];
			aClass175_2901 = (i_22_ ^ 0xffffffff) <= -1 ? class175s[i_22_] : null;
		}
	}

	public final void method3827(byte i) {
		try {
			if (i == -87) {
				if (aClass175Array2903 != null) {
					for (Class175 element : aClass175Array2903) {
						element.method2570();
					}
				}
				aClass332_2894 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vaa.C(" + i + ')');
		}
	}

	public final boolean method3828(int renderHeight, int dummy, RSToolkit toolkit) {
		if ((renderHeight ^ 0xffffffff) != (anInt2896 ^ 0xffffffff)) {
			anInt2896 = renderHeight;
			int i_2_ = Class23.priorPowerOf2(31, renderHeight);
			System.out.println("MOMOMOMO: " + i_2_);
			if (i_2_ > 512) {
				i_2_ = 512;
			}
			if ((i_2_ ^ 0xffffffff) >= -1) {
				i_2_ = 1;
			}
			if ((anInt2897 ^ 0xffffffff) != (i_2_ ^ 0xffffffff)) {
				anInt2897 = i_2_;
				aClass332_2894 = null;
			}
			if (aClass175Array2903 != null) {
				anInt2904 = 0;
				int[] is = new int[aClass175Array2903.length];
				for (Class175 class175 : aClass175Array2903) {
					if (class175.method2575(anInt2898, anInt2892, anInt2905, anInt2896)) {
						is[anInt2904] = class175.anInt1361;
						aClass175Array2899[anInt2904++] = class175;
					}
				}
				Class33.method323(is, aClass175Array2899, 0, -1 + anInt2904, 0);
			}
			aBoolean2895 = true;
		}
		boolean bool = false;
		if (aBoolean2895) {
			aBoolean2895 = false;
			for (int i_4_ = anInt2904 + -1; (i_4_ ^ 0xffffffff) <= -1; i_4_--) {
				boolean bool_5_ = aClass175Array2899[i_4_].method2577(toolkit, aClass175_2901);
				bool |= bool_5_;
				Class346 class346_6_ = this;
				class346_6_.aBoolean2895 = class346_6_.aBoolean2895 | !bool_5_;
			}
		}
		return bool;
	}

	public final void method3831(int i, int i_8_, int i_9_, int y, int i_11_, int x, RSToolkit toolkit, int screenHeight, int screenWidth, int colour) {

		i_9_ = 0x3fff & i + i_9_;
		if ((anInt2902 ^ 0xffffffff) != 0 && anInt2897 != 0) {
			TextureMetrics class238 = Class98_Sub10_Sub8.aD5578.getInfo(anInt2902, i_11_ ^ 0x2f2f);
			if (aClass332_2894 == null && Class98_Sub10_Sub8.aD5578.isCached(115, anInt2902)) {
				int[] is = (class238.anInt1818 ^ 0xffffffff) != -3 ? Class98_Sub10_Sub8.aD5578.getPixelsRgb(anInt2902, (byte) -112, anInt2897, 0.7F, false, anInt2897) : Class98_Sub10_Sub8.aD5578.getPixelsArgb(111, anInt2897, anInt2902, 0.7F, false, anInt2897);
				aClass332_2894 = toolkit.createSprite(-7962, 0, anInt2897, anInt2897, is, anInt2897);
			}
			if ((class238.anInt1818 ^ 0xffffffff) == -3) {
				System.out.println("Reeeached");

				toolkit.fillRectangle(x, y, screenWidth, screenHeight, colour, 0);
			}
			if (aClass332_2894 != null) {
				int i_16_ = (class238.anInt1818 ^ 0xffffffff) != -3 ? 0 : 1;
				int h = i_8_ * screenHeight / -4096;
				int w;
				for (w = screenHeight * i_9_ / 4096 + (screenWidth - screenHeight) / 2; (w ^ 0xffffffff) < (screenHeight ^ 0xffffffff); w -= screenHeight) {
					/* empty */
				}
				for (/**/; w < 0; w += screenHeight) {
					/* empty */
				}
				for (/**/; screenHeight < h; h -= screenHeight) {
					/* empty */
				}
				for (/**/; (h ^ 0xffffffff) > -1; h += screenHeight) {
					/* empty */
				}
				for (int width = w + -screenHeight; (screenWidth ^ 0xffffffff) < (width ^ 0xffffffff); width += screenHeight) {
					for (int height = h - screenHeight; height < screenHeight; height += screenHeight) {
						aClass332_2894.method3727(width - -x, height - -y, screenHeight, screenHeight, 1, 0, i_16_);
					}
				}
			}
		} else {
			System.out.println("Reeeached");

			toolkit.fillRectangle(x, y, screenWidth, screenHeight, 2104862, 0);//// 2104862
			//// for
			//// black
			/*for (int i_41_ = 0; i_41_ < 100; i_41_++) {
				int _x = Class98_Sub10_Sub3.anIntArray1283[i_41_] >> 4;
				int _y = Class98_Sub10_Sub3.anIntArray10273[i_41_] >> 4;
				_x += (Class284_Sub2_Sub2.COSINE[64 * ((_x & 0x40) + _y) & 0x3fff]) >> 10;
				if (_x >= 0 && _x >> 1 < Class98_Sub10_Sub3.anIntArray10356.length && (Class98_Sub10_Sub3.anIntArray10356[_x >> 1] >> 2 < 350 - _y))
					toolkit.fillRectangle(_x, _y, 2, 2, (Class98_Sub10_Sub3.anIntArray1289[i_41_] << 24 | 0xffffff), 1);
			}
			for (int i_44_ = 0; i_44_ < Class98_Sub10_Sub3.anIntArray10356.length; i_44_++) {
				int i_45_ = Class98_Sub10_Sub3.anIntArray10356[i_44_] >> 2;
				toolkit.fillRectangle(i_44_ << 1, 350 - i_45_, 2, i_45_, -2130706433, 1);
			}*/
		}
		for (int i_21_ = anInt2904 + -1; (i_21_ ^ 0xffffffff) <= -1; i_21_--) {
			aClass175Array2899[i_21_].method2573(toolkit, x, y, screenWidth, screenHeight, i_8_, i_9_);
		}
	}
}


/* Class277 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Dimension;

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public abstract class Class277 {
	public static OutgoingOpcode	aClass171_2051	= new OutgoingOpcode(68, 3);
	/* synthetic */ static Class	aClass2053;
	public static IncomingOpcode	aClass58_2052	= new IncomingOpcode(116, 8);
	public static int				anInt2050;
	public static int[]				anIntArray2049	= new int[13];

	public static final void method3289(int i, int i_11_, int i_12_, int i_13_, RSToolkit var_ha, int i_14_, boolean bool, int i_15_, int i_16_) {
		do {
			try {
				Interface19 interface19 = (Interface19) Class21_Sub1.method268(i, i_15_, i_12_);
				if (interface19 != null) {
					GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(interface19.method64(30472), (byte) 119);
					int i_17_ = 0x3 & interface19.method66(4657);
					int i_18_ = interface19.method63((byte) 20);
					if (class352.anInt2990 != -1) {
						NameHashTable.method2201(i_17_, class352, i_11_, var_ha, i_14_, (byte) 70);
					} else {
						int i_19_ = i_16_;
						if ((class352.anInt2998 ^ 0xffffffff) < -1) {
							i_19_ = i_13_;
						}
						if ((i_18_ ^ 0xffffffff) == -1 || i_18_ == 2) {
							if (i_17_ != 0) {
								if (i_17_ != 1) {
									if (i_17_ == 2) {
										var_ha.method1755(8479, i_14_, 3 + i_11_, i_19_, 4);
									} else if ((i_17_ ^ 0xffffffff) == -4) {
										var_ha.method1753(22294, 4, i_19_, i_14_ - -3, i_11_);
									}
								} else {
									var_ha.method1753(22294, 4, i_19_, i_14_, i_11_);
								}
							} else {
								var_ha.method1755(8479, i_14_, i_11_, i_19_, 4);
							}
						}
						if (i_18_ == 3) {
							if (i_17_ == 0) {
								var_ha.drawPlayerSquareDot(1, 1, i_14_, i_19_, (byte) -66, i_11_);
							} else if (i_17_ == 1) {
								var_ha.drawPlayerSquareDot(1, 1, i_14_, i_19_, (byte) -66, 3 + i_11_);
							} else if (i_17_ != 2) {
								if ((i_17_ ^ 0xffffffff) == -4) {
									var_ha.drawPlayerSquareDot(1, 1, i_14_ - -3, i_19_, (byte) -66, i_11_);
								}
							} else {
								var_ha.drawPlayerSquareDot(1, 1, i_14_ - -3, i_19_, (byte) -66, i_11_ - -3);
							}
						}
						if (i_18_ == 2) {
							if (i_17_ != 0) {
								if (i_17_ == 1) {
									var_ha.method1755(8479, i_14_, i_11_ + 3, i_19_, 4);
								} else if ((i_17_ ^ 0xffffffff) == -3) {
									var_ha.method1753(22294, 4, i_19_, 3 + i_14_, i_11_);
								} else if ((i_17_ ^ 0xffffffff) == -4) {
									var_ha.method1755(8479, i_14_, i_11_, i_19_, 4);
								}
							} else {
								var_ha.method1753(22294, 4, i_19_, i_14_, i_11_);
							}
						}
					}
				}
				interface19 = (Interface19) Class246_Sub3_Sub4.method931(i, i_15_, i_12_, aClass2053 != null ? aClass2053 : (aClass2053 = method3294("com.Interface19")));
				if (interface19 != null) {
					GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(interface19.method64(30472), (byte) 119);
					int i_20_ = 0x3 & interface19.method66(4657);
					int i_21_ = interface19.method63((byte) 20);
					if (class352.anInt2990 != -1) {
						NameHashTable.method2201(i_20_, class352, i_11_, var_ha, i_14_, (byte) 70);
					} else if ((i_21_ ^ 0xffffffff) == -10) {
						int i_22_ = -1118482;
						if ((class352.anInt2998 ^ 0xffffffff) < -1) {
							i_22_ = -1179648;
						}
						if ((i_20_ ^ 0xffffffff) == -1 || i_20_ == 2) {
							var_ha.method1789(3 + i_14_, i_22_, i_14_, 3 + i_11_, -10550, i_11_);
						} else {
							var_ha.method1789(i_14_, i_22_, 3 + i_14_, i_11_ + 3, -10550, i_11_);
						}
					}
				}
				interface19 = (Interface19) AsyncCache.method3177(i, i_15_, i_12_);
				if (bool != true) {
					anInt2050 = -91;
				}
				if (interface19 == null) {
					break;
				}
				GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(interface19.method64(30472), (byte) 119);
				int i_23_ = interface19.method66(4657) & 0x3;
				if (class352.anInt2990 == -1) {
					break;
				}
				NameHashTable.method2201(i_23_, class352, i_11_, var_ha, i_14_, (byte) 70);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rfa.F(" + i + ',' + i_11_ + ',' + i_12_ + ',' + i_13_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_14_ + ',' + bool + ',' + i_15_ + ',' + i_16_ + ')');
			}
			break;
		} while (false);
	}

	public static final void method3292(byte i) {
		try {
			if (!client.graphicsToolkit.method1766()) {
				Class76_Sub4.method754(client.preferences.currentToolkit.getValue((byte) 123), false, 65);
			} else {
				client.graphicsToolkit.method1786(GameShell.canvas);
				Class98_Sub43_Sub4.method1510(28837);
				if (!OpenGLHeap.aBoolean6079) {
					Dimension dimension = GameShell.canvas.getSize();
					client.graphicsToolkit.addCanvas(GameShell.canvas, dimension.width, dimension.height);
				} else {
					GrandExchangeOffer.method1699(GameShell.canvas, 7);
				}
				client.graphicsToolkit.switchCanvas(GameShell.canvas);
			}
			Class98_Sub43.setAllDirty(2);
			Class358.aBoolean3033 = true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rfa.B(" + i + ')');
		}
	}

	public static final int method3293(int i, NPC npc) {
		try {
			NPCDefinition definition = npc.definition;
			if (i <= 119) {
				anIntArray2049 = null;
			}
			if (definition.anIntArray1109 != null) {
				definition = definition.method2300(StartupStage.varValues, (byte) 64);
				if (definition == null) {
					return -1;
				}
			}
			int i_25_ = definition.anInt1102;
			RenderAnimDefinition renderDefinition = npc.method3039(1);
			if (npc.anInt6385 == -1 || npc.aBoolean6359) {
				i_25_ = definition.anInt1120;
			} else if ((renderDefinition.anInt2389 ^ 0xffffffff) != (npc.anInt6385 ^ 0xffffffff) && renderDefinition.anInt2361 != npc.anInt6385 && (renderDefinition.anInt2402 ^ 0xffffffff) != (npc.anInt6385 ^ 0xffffffff)
					&& npc.anInt6385 != renderDefinition.anInt2357) {
				if ((renderDefinition.anInt2368 ^ 0xffffffff) == (npc.anInt6385 ^ 0xffffffff) || (npc.anInt6385 ^ 0xffffffff) == (renderDefinition.anInt2394 ^ 0xffffffff) || npc.anInt6385 == renderDefinition.anInt2403 || npc.anInt6385 == renderDefinition.anInt2377) {
					i_25_ = definition.anInt1132;
				}
			} else {
				i_25_ = definition.anInt1147;
			}
			System.out.println("Class277: " + i_25_);
			return i_25_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rfa.C(" + i + ',' + (npc != null ? "{...}" : "null") + ')');
		}
	}

	/* synthetic */
	public static Class method3294(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	abstract Class98_Sub46_Sub20 method3290(Class98_Sub46_Sub20 class98_sub46_sub20, byte i);
}

/* Class251 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.definition.QuestDefinition;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.preferences.AntialiasPreferenceField;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class Class251 {
	public static IncomingOpcode	aClass58_1921	= new IncomingOpcode(4, 6);
	public static int				headiconPrayerId;

	public static final void handleLobbyToWorldStage(String password, int i, String username, int i_24_) {
		OpenGlModelRenderer.anInt4855 = i_24_;
		Class98_Sub46_Sub20_Sub2.anInt6317 = 2;
		Class342.method3814(false, username, i + 17782, password);
	}

	public static final void updateMapArea(int i, int centreY, boolean force, int centreX, int clientState) {
		if (force || centreX != Class160.centreX || (centreY ^ 0xffffffff) != (Class275.centreY ^ 0xffffffff) || (Font.localPlane ^ 0xffffffff) != (SunDefinitionParser.anInt963 ^ 0xffffffff) && (client.preferences.aClass64_Sub3_4076.getValue((byte) 123) ^ 0xffffffff) != -2) {
			Class160.centreX = centreX;
			SunDefinitionParser.anInt963 = Font.localPlane;
			Class275.centreY = centreY;
			if (client.preferences.aClass64_Sub3_4076.getValue((byte) 123) == 1) {
				SunDefinitionParser.anInt963 = 0;
			}
			HashTableIterator.setClientState(clientState, false);
			Class246_Sub2.draw(-77, Class98_Sub46_Sub10.p12FullMetrics, Class195.p12Full, true, client.graphicsToolkit, TextResources.LOADING.getText(client.gameLanguage, (byte) 25));
			int regionX = Class272.gameSceneBaseX;
			Class272.gameSceneBaseX = (Class160.centreX - (Class165.mapWidth >> 4)) * 8;
			int regionZ = aa_Sub2.gameSceneBaseY;
			aa_Sub2.gameSceneBaseY = (-(Class98_Sub10_Sub7.mapLength >> 4) + Class275.centreY) * 8;
			NodeShort.aClass98_Sub46_Sub10_4195 = WorldMap.method3303(Class160.centreX * 8, Class275.centreY * 8);
			AntialiasPreferenceField.aClass370_3707 = null;
			int dX = -regionX + Class272.gameSceneBaseX;
			int dZ = -regionZ + aa_Sub2.gameSceneBaseY;
			if ((clientState ^ 0xffffffff) == -12) {
				for (int index = 0; (index ^ 0xffffffff) > (Class98_Sub10_Sub20.anInt5640 ^ 0xffffffff); index++) {
					NodeObject node = BackgroundColourLSEConfig.aClass98_Sub39Array3516[index];
					if (node != null) {
						NPC npc = node.npc;
						for (int step = 0; step < 10; step++) {
							npc.pathX[step] -= dX;
							npc.pathZ[step] -= dZ;
						}
						npc.boundExtentsZ -= dZ * 512;
						npc.boundExtentsX -= dX * 512;
					}
				}
			} else {
				Class150.npcCount = 0;
				boolean bool_10_ = false;
				int width = (-1 + Class165.mapWidth) * 512;
				int length = (-1 + Class98_Sub10_Sub7.mapLength) * 512;
				for (int index = 0; Class98_Sub10_Sub20.anInt5640 > index; index++) {
					NodeObject node = BackgroundColourLSEConfig.aClass98_Sub39Array3516[index];
					if (node != null) {
						NPC npc = node.npc;
						npc.boundExtentsX -= 512 * dX;
						npc.boundExtentsZ -= dZ * 512;
						if ((npc.boundExtentsX ^ 0xffffffff) > -1 || (npc.boundExtentsX ^ 0xffffffff) < (width ^ 0xffffffff) || (npc.boundExtentsZ ^ 0xffffffff) > -1 || (length ^ 0xffffffff) > (npc.boundExtentsZ ^ 0xffffffff)) {
							npc.setDefinition(null, 1);
							node.unlink(113);
							bool_10_ = true;
						} else {
							boolean keep = true;
							for (int step = 0; (step ^ 0xffffffff) > -11; step++) {
								((Mob) npc).pathX[step] -= dX;
								((Mob) npc).pathZ[step] -= dZ;
								if (npc.pathX[step] < 0 || Class165.mapWidth <= npc.pathX[step] || (npc.pathZ[step] ^ 0xffffffff) > -1 || (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) >= (npc.pathZ[step] ^ 0xffffffff)) {
									keep = false;
								}
							}
							if (!keep) {
								npc.setDefinition(null, 1);
								node.unlink(97);
								bool_10_ = true;
							} else {
								Orientation.npcIndices[Class150.npcCount++] = npc.index;
							}
						}
					}
				}
				if (bool_10_) {
					Class98_Sub10_Sub20.anInt5640 = ProceduralTextureSource.npc.size((byte) -6);
					ProceduralTextureSource.npc.getValues(BackgroundColourLSEConfig.aClass98_Sub39Array3516, (byte) 74);
				}
			}
			for (int index = 0; index < 2048; index++) {
				Player player = Class151_Sub9.players[index];
				if (player != null) {
					for (int step = 0; step < 10; step++) {
						((Mob) player).pathX[step] -= dX;
						((Mob) player).pathZ[step] -= dZ;
					}
					player.boundExtentsZ -= 512 * dZ;
					player.boundExtentsX -= 512 * dX;
				}
			}
			if (i == -6547) {
				Class36[] class36s = OpenGlArrayBufferPointer.aClass36Array903;
				for (int i_18_ = 0; (i_18_ ^ 0xffffffff) > (class36s.length ^ 0xffffffff); i_18_++) {
					Class36 class36 = class36s[i_18_];
					if (class36 != null) {
						class36.anInt338 -= dX * 512;
						class36.anInt347 -= 512 * dZ;
					}
				}
				for (Class98_Sub33 class98_sub33 = (Class98_Sub33) Class191.aClass148_1478.getFirst(32); class98_sub33 != null; class98_sub33 = (Class98_Sub33) Class191.aClass148_1478.getNext(88)) {
					class98_sub33.localX -= dX;
					class98_sub33.localZ -= dZ;
					if (Class151_Sub9.anInt5028 != 4 && ((class98_sub33.localX ^ 0xffffffff) > -1 || class98_sub33.localZ < 0 || (class98_sub33.localX ^ 0xffffffff) <= (Class165.mapWidth ^ 0xffffffff) || Class98_Sub10_Sub7.mapLength <= class98_sub33.localZ)) {
						class98_sub33.unlink(83);
					}
				}
				for (Class98_Sub33 class98_sub33 = (Class98_Sub33) OutgoingPacket.aClass148_3866.getFirst(32); class98_sub33 != null; class98_sub33 = (Class98_Sub33) OutgoingPacket.aClass148_3866.getNext(116)) {
					class98_sub33.localZ -= dZ;
					class98_sub33.localX -= dX;
					if (Class151_Sub9.anInt5028 != 4 && (class98_sub33.localX < 0 || class98_sub33.localZ < 0 || (Class165.mapWidth ^ 0xffffffff) >= (class98_sub33.localX ^ 0xffffffff) || Class98_Sub10_Sub7.mapLength <= class98_sub33.localZ)) {
						class98_sub33.unlink(i + 6603);
					}
				}
				if (Class151_Sub9.anInt5028 != 4) {
					for (ItemDeque items = (ItemDeque) ModelRenderer.groundItems.startIteration(101); items != null; items = (ItemDeque) ModelRenderer.groundItems.iterateNext(i + 6546)) {
						int x = (int) (items.hash & 0x3fffL);
						int localX = -Class272.gameSceneBaseX + x;
						int z = (int) (items.hash >> 14 & 0x3fffL);
						int localZ = z + -aa_Sub2.gameSceneBaseY;
						if ((localX ^ 0xffffffff) > -1 || localZ < 0 || Class165.mapWidth <= localX || Class98_Sub10_Sub7.mapLength <= localZ) {
							items.unlink(61);
						}
					}
				}
				if ((LightIntensityDefinitionParser.anInt2024 ^ 0xffffffff) != -1) {
					Class246_Sub3_Sub1_Sub2.anInt6251 -= dZ;
					LightIntensityDefinitionParser.anInt2024 -= dX;
				}
				QuestDefinition.method2820((byte) 126);
				if ((clientState ^ 0xffffffff) != -12) {
					Class98_Sub10_Sub21.anInt5643 -= dZ;
					Exception_Sub1.anInt44 -= dX;
					NewsFetcher.anInt3098 -= dX;
					NodeString.anInt3915 -= dZ;
					SpriteLoadingScreenElement.yCamPosTile -= dZ * 512;
					Class98_Sub46_Sub10.xCamPosTile -= 512 * dX;
					if (Math.abs(dX) > Class165.mapWidth || Math.abs(dZ) > Class98_Sub10_Sub7.mapLength) {
						WhirlpoolGenerator.method3980((byte) 124);
					}
				} else if (Class98_Sub46_Sub20_Sub2.cameraMode == 4) {
					Class224_Sub3_Sub1.cameraYPosition -= dZ * 512;
					Js5Client.anInt1051 -= 512 * dZ;
					Minimap.cameraXPosition -= 512 * dX;
					StrongReferenceMCNode.anInt6295 -= dX * 512;
				} else {
					Class116.anInt967 = CharacterShadowsPreferenceField.anInt3712 = -1;
					Class98_Sub46_Sub20_Sub2.cameraMode = 1;
				}
				OrthoZoomPreferenceField.method622((byte) -38);
				InputStream_Sub2.method124(i + 6456);
				Class98_Sub10_Sub11.animatedObjects.clear((byte) 47);
				QuickChatMessageParser.projectiles.clear((byte) 47);
				TexturesPreferenceField.aClass218_3694.clear(i + 6435);
				OpenGlTileMaterial.method1171(i + 6547);
			}
		}
	}

	public int	anInt1917;

	public int	anInt1918;

	public int	anInt1919;

	public int	anInt1920;

	public Class251() {
		/* empty */
	}
}

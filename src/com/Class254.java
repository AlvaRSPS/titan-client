/* Class254 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.quickchat.QuickChatCategory;

public final class Class254 {
	public static OutgoingOpcode	aClass171_1940	= new OutgoingOpcode(35, 5);
	public static Sprite[]			aClass332Array1943;
	public static int				anInt1944		= 0;

	public static final void method3186(int i) {
		try {
			int i_1_ = 0;
			if (i < 6) {
				method3188(6);
			}
			for (int i_2_ = 0; i_2_ < Class165.mapWidth; i_2_++) {
				for (int i_3_ = 0; (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) < (i_3_ ^ 0xffffffff); i_3_++) {
					if (AsyncCache.method3175(i_2_, i_1_, QuickChatCategory.aClass172ArrayArrayArray5948, i_3_, (byte) -76, true)) {
						i_1_++;
					}
					if (i_1_ >= 512) {
						return;
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pu.A(" + i + ')');
		}
	}

	public static final boolean method3187(int i, int i_4_, int i_5_, byte i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_) {
		try {
			if (!ItemSpriteCacheKey.method719(i_5_, i_12_, i_7_, i_6_ ^ 0x52)) {
				return false;
			}
			i_7_ = Class114.anIntArray958[1];
			i_5_ = Class114.anIntArray958[2];
			i_12_ = Class114.anIntArray958[0];
			if (i_6_ != 82) {
				method3186(-21);
			}
			if (!ItemSpriteCacheKey.method719(i_10_, i_11_, i, 0)) {
				return false;
			}
			i = Class114.anIntArray958[1];
			i_10_ = Class114.anIntArray958[2];
			i_11_ = Class114.anIntArray958[0];
			if (!ItemSpriteCacheKey.method719(i_8_, i_4_, i_9_, 0)) {
				return false;
			}
			i_9_ = Class114.anIntArray958[1];
			i_4_ = Class114.anIntArray958[0];
			i_8_ = Class114.anIntArray958[2];
			return StringConcatenator.method3243(i_11_, i_12_, (byte) 82, i_10_, i_9_, i_7_, i_4_, i, i_5_, i_8_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pu.F(" + i + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + i_9_ + ',' + i_10_ + ',' + i_11_ + ',' + i_12_ + ')');
		}
	}

	public static void method3188(int i) {
		try {
			aClass171_1940 = null;
			if (i != -386) {
				method3187(80, 18, 123, (byte) 83, -110, -108, -70, -39, -108, -12);
			}
			aClass332Array1943 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pu.C(" + i + ')');
		}
	}

	private Cacheable	aClass98_Sub46_1941;

	private Cacheable[]	aClass98_Sub46Array1939;

	private long		aLong1942;

	private int			anInt1938;

	Class254(int i) {
		try {
			anInt1938 = i;
			aClass98_Sub46Array1939 = new Cacheable[i];
			for (int i_15_ = 0; i > i_15_; i_15_++) {
				Cacheable class98_sub46 = aClass98_Sub46Array1939[i_15_] = new Cacheable();
				class98_sub46.nextCacheable = class98_sub46;
				class98_sub46.previousCacheable = class98_sub46;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pu.<init>(" + i + ')');
		}
	}

	public final void method3185(byte i, Cacheable class98_sub46, long l) {
		try {
			if (class98_sub46.previousCacheable != null) {
				class98_sub46.uncache((byte) -90);
			}
			Cacheable class98_sub46_0_ = aClass98_Sub46Array1939[(int) (-1 + anInt1938 & l)];
			class98_sub46.nextCacheable = class98_sub46_0_;
			class98_sub46.previousCacheable = class98_sub46_0_.previousCacheable;
			class98_sub46.previousCacheable.nextCacheable = class98_sub46;
			if (i > -6) {
				aLong1942 = -42L;
			}
			class98_sub46.nextCacheable.previousCacheable = class98_sub46;
			class98_sub46.cachedKey = l;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pu.E(" + i + ',' + (class98_sub46 != null ? "{...}" : "null") + ',' + l + ')');
		}
	}

	public final Cacheable method3189(long l, byte i) {
		try {
			aLong1942 = l;
			Cacheable class98_sub46 = aClass98_Sub46Array1939[(int) (l & anInt1938 + -1)];
			aClass98_Sub46_1941 = class98_sub46.nextCacheable;
			for (/**/; aClass98_Sub46_1941 != class98_sub46; aClass98_Sub46_1941 = aClass98_Sub46_1941.nextCacheable) {
				if ((aClass98_Sub46_1941.cachedKey ^ 0xffffffffffffffffL) == (l ^ 0xffffffffffffffffL)) {
					Cacheable class98_sub46_14_ = aClass98_Sub46_1941;
					aClass98_Sub46_1941 = aClass98_Sub46_1941.nextCacheable;
					return class98_sub46_14_;
				}
			}
			aClass98_Sub46_1941 = null;
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pu.D(" + l + ',' + i + ')');
		}
	}

	public final Cacheable method3190(boolean bool) {
		try {
			if (aClass98_Sub46_1941 == null) {
				return null;
			}
			if (bool != false) {
				return null;
			}
			for (Cacheable class98_sub46 = aClass98_Sub46Array1939[(int) (-1 + anInt1938 & aLong1942)]; class98_sub46 != aClass98_Sub46_1941; aClass98_Sub46_1941 = aClass98_Sub46_1941.nextCacheable) {
				if (aLong1942 == aClass98_Sub46_1941.cachedKey) {
					Cacheable class98_sub46_16_ = aClass98_Sub46_1941;
					aClass98_Sub46_1941 = aClass98_Sub46_1941.nextCacheable;
					return class98_sub46_16_;
				}
			}
			aClass98_Sub46_1941 = null;
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pu.B(" + bool + ')');
		}
	}
}

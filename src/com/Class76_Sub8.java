
/* Class76_Sub8 - Decompiled by JODE
 */ package com; /*
					*/

import java.util.Random;

import com.jagex.core.timetools.timebase.FrameTimeBase;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.toolkit.matrix.NativeMatrix;

import jaggl.OpenGL;

public final class Class76_Sub8 extends Class76 {
	public static boolean	aBoolean3771	= false;
	public static int		anInt3766;
	public static int		anInt3770;
	public static int		anInt3778;
	public static int		anInt3780;
	public static Random	aRandom3767;

	static {
		anInt3766 = 0;
		anInt3778 = 0;
		aRandom3767 = new Random();
	}

	public static void method764(byte i) {
		do {
			try {
				aRandom3767 = null;
				if (i == 122) {
					break;
				}
				method764((byte) -116);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "laa.A(" + i + ')');
			}
			break;
		} while (false);
	}

	private boolean				aBoolean3768;
	private boolean				aBoolean3769;
	private boolean				aBoolean3772;
	private boolean				aBoolean3775;
	private Class273			aClass273_3773;
	private Class273			aClass273_3774;
	private Class273			aClass273_3777;
	private Class273			aClass273_3781;

	private float[]				aFloatArray3776	= new float[4];

	private Interface4_Impl2	anInterface4_Impl2_3779;

	Class76_Sub8(OpenGLXToolkit var_ha_Sub3_Sub2, Js5 class207) {
		super(var_ha_Sub3_Sub2);
		aBoolean3768 = false;
		try {
			if (class207 == null || !var_ha_Sub3_Sub2.aBoolean6134) {
				aBoolean3772 = false;
			} else {
				aClass273_3781 = FrameTimeBase.method2927(class207.method2739("gl", "uw_ground_unlit", -32734), var_ha_Sub3_Sub2, 34336, 25246);
				aClass273_3774 = FrameTimeBase.method2927(class207.method2739("gl", "uw_ground_lit", -32734), var_ha_Sub3_Sub2, 34336, 25246);
				aClass273_3773 = FrameTimeBase.method2927(class207.method2739("gl", "uw_model_unlit", -32734), var_ha_Sub3_Sub2, 34336, 25246);
				aClass273_3777 = FrameTimeBase.method2927(class207.method2739("gl", "uw_model_lit", -32734), var_ha_Sub3_Sub2, 34336, 25246);
				if (aClass273_3777 != null & aClass273_3773 != null & aClass273_3781 != null & aClass273_3774 != null) {
					anInterface4_Impl2_3779 = this.aHa_Sub3_585.method2012(2, 1, (byte) 31, new int[] { 0, -1 }, false);
					anInterface4_Impl2_3779.method46(false, false, -37);
					aBoolean3772 = true;
				} else {
					aBoolean3772 = false;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "laa.<init>(" + (var_ha_Sub3_Sub2 != null ? "{...}" : "null") + ',' + (class207 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method738(int i) {
		do {
			try {
				if (i > -45) {
					aRandom3767 = null;
				}
				if (!aBoolean3769) {
					break;
				}
				int i_4_ = this.aHa_Sub3_585.getFarPlane();
				int i_5_ = this.aHa_Sub3_585.getNearPlane();
				float f = -((i_4_ + -i_5_) * 0.125F) + i_4_;
				float f_6_ = i_4_ - (-i_5_ + i_4_) * 0.25F;
				OpenGL.glProgramLocalParameter4fARB(34336, 0, f_6_, f, 1.0F / this.aHa_Sub3_585.method1948(126), this.aHa_Sub3_585.method2018((byte) 98) / 255.0F);
				this.aHa_Sub3_585.method1951((byte) 120, 1);
				this.aHa_Sub3_585.method1984(2, this.aHa_Sub3_585.method1998((byte) 89));
				this.aHa_Sub3_585.method1951((byte) 120, 0);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "laa.K(" + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method739(int i) {
		do {
			try {
				this.aHa_Sub3_585.method1951((byte) 120, 1);
				this.aHa_Sub3_585.method2005(null, 76);
				this.aHa_Sub3_585.method2019(Class249.aClass128_1903, Class249.aClass128_1903, 22831);
				this.aHa_Sub3_585.method2051(0, -108, QuickChat.aClass65_2499);
				this.aHa_Sub3_585.method2051(2, -54, MaxScreenSizePreferenceField.aClass65_3681);
				this.aHa_Sub3_585.method1953(i ^ 0x5c, QuickChat.aClass65_2499, 0);
				this.aHa_Sub3_585.method1951((byte) 120, 0);
				if (aBoolean3768) {
					this.aHa_Sub3_585.method2051(0, -87, QuickChat.aClass65_2499);
					this.aHa_Sub3_585.method1953(-93, QuickChat.aClass65_2499, 0);
					aBoolean3768 = false;
				}
				if (aBoolean3769) {
					OpenGL.glBindProgramARB(34336, 0);
					OpenGL.glDisable(34820);
					OpenGL.glDisable(34336);
					aBoolean3769 = false;
				}
				if (i == -2) {
					break;
				}
				method746(-34, -50, 9);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "laa.C(" + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method742(int i, int i_3_, Interface4 interface4) {
		try {
			do {
				if (interface4 == null) {
					if (aBoolean3768) {
						break;
					}
					this.aHa_Sub3_585.method2005(this.aHa_Sub3_585.anInterface4_4586, 41);
					this.aHa_Sub3_585.method2015(1, (byte) -124);
					this.aHa_Sub3_585.method2051(0, i + -132, IncomingOpcode.aClass65_459);
					this.aHa_Sub3_585.method1953(-108, IncomingOpcode.aClass65_459, 0);
					aBoolean3768 = true;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if (aBoolean3768) {
					this.aHa_Sub3_585.method2051(0, -57, QuickChat.aClass65_2499);
					this.aHa_Sub3_585.method1953(-67, QuickChat.aClass65_2499, 0);
					aBoolean3768 = false;
				}
				this.aHa_Sub3_585.method2005(interface4, -124);
				this.aHa_Sub3_585.method2015(i_3_, (byte) -128);
			} while (false);
			if (i != 6) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "laa.I(" + i + ',' + i_3_ + ',' + (interface4 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method743(int i, boolean bool) {
		try {
			if (i <= 93) {
				anInterface4_Impl2_3779 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "laa.D(" + i + ',' + bool + ')');
		}
	}

	@Override
	public final boolean method745(byte i) {
		try {
			if (i != 27) {
				method748(-106, true);
			}
			return aBoolean3772;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "laa.H(" + i + ')');
		}
	}

	@Override
	public final void method746(int i, int i_0_, int i_1_) {
		do {
			try {
				if (i_1_ < -75) {
					break;
				}
				method748(69, true);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "laa.E(" + i + ',' + i_0_ + ',' + i_1_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method747(int i) {
		try {
			if (i != -25684) {
				anInt3778 = -102;
			}
			int i_2_ = this.aHa_Sub3_585.method2017((byte) 67);
			NativeMatrix class111_sub3 = this.aHa_Sub3_585.method1956((byte) -90);
			do {
				if (!aBoolean3775) {
					OpenGL.glBindProgramARB(34336, (i_2_ ^ 0xffffffff) != -2147483648 ? aClass273_3773.anInt2040 : aClass273_3781.anInt2040);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				OpenGL.glBindProgramARB(34336, i_2_ == 2147483647 ? aClass273_3774.anInt2040 : aClass273_3777.anInt2040);
			} while (false);
			OpenGL.glEnable(34336);
			aBoolean3769 = true;
			class111_sub3.method2120((byte) 51, 0.0F, -1.0F, i_2_, aFloatArray3776, 0.0F);
			OpenGL.glProgramLocalParameter4fARB(34336, 1, aFloatArray3776[0], aFloatArray3776[1], aFloatArray3776[2], aFloatArray3776[3]);
			method738(i + 25626);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "laa.P(" + i + ')');
		}
	}

	@Override
	public final void method748(int i, boolean bool) {
		try {
			if (i == 69) {
				aBoolean3775 = bool;
				this.aHa_Sub3_585.method1951((byte) 120, 1);
				this.aHa_Sub3_585.method2005(anInterface4_Impl2_3779, -114);
				this.aHa_Sub3_585.method2019(Class288.aClass128_3381, Class323.aClass128_2715, 22831);
				this.aHa_Sub3_585.method2051(0, -81, MaxScreenSizePreferenceField.aClass65_3681);
				this.aHa_Sub3_585.method2026(2, true, (byte) 27, QuickChat.aClass65_2499, false);
				this.aHa_Sub3_585.method1953(-125, IncomingOpcode.aClass65_459, 0);
				this.aHa_Sub3_585.method1951((byte) 120, 0);
				method747(-25684);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "laa.B(" + i + ',' + bool + ')');
		}
	}
}

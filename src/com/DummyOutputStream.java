
/* OutputStream_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;
import java.io.IOException;
import java.io.OutputStream;

import com.jagex.game.client.definition.CursorDefinition;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.input.impl.AwtMouseListener;

public final class DummyOutputStream extends OutputStream {
	public static boolean			aBoolean35;
	public static OutgoingOpcode	aClass171_34;
	public static int[]				anIntArray38;
	public static int				mapdotsId;
	public static RtInterface		rtInterface	= null;

	static {
		GameShell.renderTimer = 500;
		aClass171_34 = new OutgoingOpcode(8, 3);
		aBoolean35 = false;
		anIntArray38 = new int[] { 16776960, 16711680, 65280, 65535, 16711935, 16777215 };
	}

	public static final RSToolkit create(TextureMetricsList textureSource, int height, int width, Canvas canvas, int i_1_) {
		return new PureJavaToolkit(canvas, textureSource, width, height);
	}

	public static final void method129(int i, int i_2_, HorizontalAlignment class63, int i_3_, int i_4_, boolean bool, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, VerticalAlignment class110) {
		CharacterShadowsPreferenceField.aClass324_3713 = null;
		Class42_Sub2.aClass324_5359 = null;
		ParticleManager.anInt384 = i_6_;
		Class76_Sub10.anInt3793 = i_2_;
		CursorDefinition.aClass324_1733 = null;
		Class98_Sub10_Sub38.anInt5751 = i_5_;
		Class98_Sub10_Sub18.anInt5626 = i_3_;
		GamePreferences.anInt4060 = i_9_;
		Class151.aClass63_1216 = class63;
		Class277.anInt2050 = i_7_;
		Class15.anInt170 = i_4_;
		GameObjectDefinitionParser.anInt2524 = i_8_;
		Class246_Sub10.anInt5153 = i;
		Class202.aClass110_1547 = class110;
		NativeToolkit.method2062(-4264);
		Class98_Sub10.aBoolean3858 = true;
	}

	public static final void method130(int i, int i_10_, int i_11_) {
		AwtMouseListener.method3523(i_11_, i, i_10_ ^ i_10_);
	}

	public static void method131(byte i) {
		do {
			aClass171_34 = null;
			GameShell.frameTimeBase = null;
			rtInterface = null;
			anIntArray38 = null;
			if (i == 4) {
				break;
			}
			GameShell.renderTimer = -72;
			break;
		} while (false);
	}

	@Override
	public final void write(int i) throws IOException {
		throw new IOException("Tried to write to DummyOutputStream");
	}
}

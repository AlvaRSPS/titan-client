/* Class53_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.QuestDefinition;
import com.jagex.game.client.definition.parser.BConfigDefinitionParser;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class Class53_Sub1 extends Class53 {
	public static Class164	aClass164_3633	= new Class164(1);
	public static int		anInt3636		= -1;
	public static int		js5InitializationProgress;

	static {
		js5InitializationProgress = -1;
	}

	public static final void method3951(byte i) {
		if (Class224_Sub3_Sub1.aBoolean6144) {
			while ((Class98_Sub9.anInt3854 ^ 0xffffffff) > (Class98_Sub28_Sub1.aClass53_Sub1Array5805.length ^ 0xffffffff)) {
				Class53_Sub1 class53_sub1 = Class98_Sub28_Sub1.aClass53_Sub1Array5805[Class98_Sub9.anInt3854];
				if (class53_sub1 == null || (class53_sub1.anInt3631 ^ 0xffffffff) != 0) {
					Class98_Sub9.anInt3854++;
				} else {
					if (QuestDefinition.aClass98_Sub4_1657 == null) {
						QuestDefinition.aClass98_Sub4_1657 = Class48_Sub2_Sub1.aClass265_5531.method3229(90, class53_sub1.aString3634);
					}
					int i_1_ = QuestDefinition.aClass98_Sub4_1657.anInt3827;
					if (i_1_ == -1) {
						break;
					}
					QuestDefinition.aClass98_Sub4_1657 = null;
					Class98_Sub9.anInt3854++;
					class53_sub1.anInt3631 = i_1_;
				}
			}
		}
	}

	public static final void method498(int i) {
		try {
			for (int i_0_ = i; (HorizontalAlignment.anInt493 ^ 0xffffffff) < (i_0_ ^ 0xffffffff); i_0_++) {
				int i_1_ = BConfigDefinitionParser.method2678((byte) 6, i_0_ - -Class268.anInt2007, HorizontalAlignment.anInt493) * Class191.anInt1477;
				for (int i_2_ = 0; i_2_ < Class191.anInt1477; i_2_++) {
					int i_3_ = i_1_ + BConfigDefinitionParser.method2678((byte) 6, Class76_Sub8.anInt3780 + i_2_, Class191.anInt1477);
					if ((Class230.anInt1732 ^ 0xffffffff) == (OpenGlModelRenderer.anIntArray4873[i_3_] ^ 0xffffffff)) {
						Class172.anInterface17Array1327[i_3_].copyFromRenderer(0, 0, FontSpecifications.anInt1513, Class98_Sub10_Sub38.anInt5761, FontSpecifications.anInt1513 * i_2_, i_0_ * Class98_Sub10_Sub38.anInt5761, true, true);
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jha.F(" + i + ')');
		}
	}

	public static final boolean method499(int i, int clientState) {
		return !((clientState ^ 0xffffffff) != -4 && clientState != 4 && clientState != 5 && clientState != 6);
	}

	public static void method500(int i) {
		try {
			if (i >= -93) {
				method499(86, 125);
			}
			aClass164_3633 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jha.J(" + i + ')');
		}
	}

	public static final boolean method502(int i, int i_5_, byte i_6_) {
		try {
			if (i_6_ < 110) {
				method500(-79);
			}
			return (i_5_ & 0x800 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jha.H(" + i + ',' + i_5_ + ',' + i_6_ + ')');
		}
	}

	public int	anInt3631	= -1;

	int			anInt3632;

	String		aString3630;

	String		aString3634;

	public final Class114 method501(int i) {
		try {
			if (i != -1) {
				method500(-79);
			}
			return Class98_Sub10_Sub36.aClass114Array5744[this.anInt426];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jha.I(" + i + ')');
		}
	}
}

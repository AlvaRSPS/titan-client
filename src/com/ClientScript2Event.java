/* Class98_Sub21 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;
import com.jagex.game.input.impl.AwtKeyListener;

public final class ClientScript2Event extends Node {
	private static short[]	aShortArray3983	= { -10304, 9104, 25485, 4620, 4540 };
	private static short[]	aShortArray3988	= { -1, -1, -1, -1, -1 };
	private static short[]	aShortArray3989	= { 6798, 8741, 25238, 4626, 4550 };
	public static short[][]	aShortArrayArray3987;

	static {
		aShortArrayArray3987 = new short[][] { aShortArray3989, aShortArray3983, aShortArray3988 };
	}

	public static final int method1176(boolean bool) {
		try {
			if (Cacheable.anInt4261 == 1) {
				return Class48_Sub1_Sub2.anInt5519;
			}
			if (bool != false) {
				aShortArray3989 = null;
			}
			return ParamDefinition.anInt1208;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "je.E(" + bool + ')');
		}
	}

	public static void method1177(int i) {
		try {
			aShortArray3989 = null;
			aShortArrayArray3987 = null;
			aShortArray3983 = null;
			if (i != 24301) {
				method1177(-117);
			}
			aShortArray3988 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "je.A(" + i + ')');
		}
	}

	public static final boolean method1178(int i) {
		try {
			if (i != 13299) {
				aShortArrayArray3987 = null;
			}
			try {
				if ((RenderAnimDefinitionParser.anInt1948 ^ 0xffffffff) == -3) {
					if (Class81.aClass98_Sub7_620 == null) {
						Class81.aClass98_Sub7_620 = Class98_Sub7.method985(LightIntensityDefinitionParser.aClass207_2025, RtInterfaceAttachment.anInt3951, Class76_Sub8.anInt3770);
						if (Class81.aClass98_Sub7_620 == null) {
							return false;
						}
					}
					if (Class202.aClass308_1550 == null) {
						Class202.aClass308_1550 = new Class308(Class94.aClass207_793, NodeInteger.aClass207_4127);
					}
					Class98_Sub31_Sub2 class98_sub31_sub2 = BConfigDefinition.aClass98_Sub31_Sub2_3122;
					if (Class116.aClass98_Sub31_Sub2_965 != null) {
						class98_sub31_sub2 = Class116.aClass98_Sub31_Sub2_965;
					}
					if (class98_sub31_sub2.method1352(Class81.aClass98_Sub7_620, 22050, Class202.aClass308_1550, MonoOrStereoPreferenceField.aClass207_3641, false)) {
						BConfigDefinition.aClass98_Sub31_Sub2_3122 = class98_sub31_sub2;
						BConfigDefinition.aClass98_Sub31_Sub2_3122.method1358((byte) 23);
						if (Class22.anInt219 <= 0) {
							RenderAnimDefinitionParser.anInt1948 = 0;
							BConfigDefinition.aClass98_Sub31_Sub2_3122.method1366(Class224_Sub3.anInt5037, (byte) 124);
							for (int i_0_ = 0; (AwtKeyListener.anIntArray3804.length ^ 0xffffffff) < (i_0_ ^ 0xffffffff); i_0_++) {
								BConfigDefinition.aClass98_Sub31_Sub2_3122.method1363(-17, i_0_, AwtKeyListener.anIntArray3804[i_0_]);
								AwtKeyListener.anIntArray3804[i_0_] = 255;
							}
						} else {
							RenderAnimDefinitionParser.anInt1948 = 3;
							BConfigDefinition.aClass98_Sub31_Sub2_3122.method1366(Class224_Sub3.anInt5037 < Class22.anInt219 ? Class224_Sub3.anInt5037 : Class22.anInt219, (byte) 33);
							for (int i_1_ = 0; i_1_ < AwtKeyListener.anIntArray3804.length; i_1_++) {
								BConfigDefinition.aClass98_Sub31_Sub2_3122.method1363(-17, i_1_, AwtKeyListener.anIntArray3804[i_1_]);
								AwtKeyListener.anIntArray3804[i_1_] = 255;
							}
						}
						if (Class116.aClass98_Sub31_Sub2_965 == null) {
							if ((RectangleLoadingScreenElement.aLong3455 ^ 0xffffffffffffffffL) < -1L) {
								BConfigDefinition.aClass98_Sub31_Sub2_3122.method1341(true, Class1.aBoolean66, Class81.aClass98_Sub7_620, RectangleLoadingScreenElement.aLong3455, -3);
							} else {
								BConfigDefinition.aClass98_Sub31_Sub2_3122.method1332(Class1.aBoolean66, Class81.aClass98_Sub7_620, (byte) -4);
							}
						}
						if (Class270.aClass268_2032 != null) {
							Class270.aClass268_2032.method3252(i + -13299, BConfigDefinition.aClass98_Sub31_Sub2_3122);
						}
						LightIntensityDefinitionParser.aClass207_2025 = null;
						Class202.aClass308_1550 = null;
						Class81.aClass98_Sub7_620 = null;
						RectangleLoadingScreenElement.aLong3455 = 0L;
						Class116.aClass98_Sub31_Sub2_965 = null;
						return true;
					}
				}
			} catch (Exception exception) {
				exception.printStackTrace();
				BConfigDefinition.aClass98_Sub31_Sub2_3122.method1364(98);
				LightIntensityDefinitionParser.aClass207_2025 = null;
				Class202.aClass308_1550 = null;
				RenderAnimDefinitionParser.anInt1948 = 0;
				Class81.aClass98_Sub7_620 = null;
				Class116.aClass98_Sub31_Sub2_965 = null;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "je.C(" + i + ')');
		}
	}

	public static final boolean method1179(int i, int i_2_) {
		try {
			if (i_2_ != 255) {
				aShortArray3983 = null;
			}
			return !(i != 49 && i != 59 && i != 1006 && i != 21 && i != 9);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "je.B(" + i + ',' + i_2_ + ')');
		}
	}

	public static final int method1180(int i, byte i_3_, int i_4_, int i_5_, int i_6_) {
		try {
			int i_8_ = 65536 - Class284_Sub2_Sub2.COSINE[8192 * i_5_ / i] >> -219666591;
			return (i_8_ * i_6_ >> -23038832) + (i_4_ * (65536 - i_8_) >> 1646652976);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "je.D(" + i + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ')');
		}
	}

	public boolean		aBoolean3980;
	public RtInterface	aClass293_3982;
	public RtInterface	component;
	public int			anInt3976;
	public int			anInt3977;

	public int			mouseY;

	public int			code;

	public int			mouseX;

	public int			anInt3990;

	public Object[]		param;

	public String		opbase;

	public ClientScript2Event() {
		/* empty */
	}
}

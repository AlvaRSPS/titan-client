
/* Class101 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;
import java.awt.Dimension;

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;

public final class GrandExchangeOffer {
	public static boolean				aBoolean856		= true;
	public static Class126				aClass126_848	= new Class126();
	public static int					anInt849		= -1;
	public static int					anInt854;
	public static SunDefinitionParser	sunDefinitionList;

	public static final void method1699(Canvas canvas, int i) {
		Dimension dimension = canvas.getSize();
		Class287_Sub2.method3391(dimension.height, dimension.width, 2);
		if ((Cacheable.anInt4261 ^ 0xffffffff) != -2) {
			Class154.aHa1231.addCanvas(canvas, Class151_Sub7.anInt5005, ParamDefinition.anInt1208);
		} else {
			Class154.aHa1231.addCanvas(canvas, aa_Sub1.anInt3556, Class48_Sub1_Sub2.anInt5519);
		}
	}

	public static final Class246_Sub3_Sub5 method1701(int i, int i_1_, int i_2_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_1_][i_2_];
		if (class172 == null) {
			return null;
		}
		return class172.aClass246_Sub3_Sub5_1334;
	}

	public static final void method1702(int i, boolean bool, boolean bool_3_) {
		if (bool) {
			RtInterfaceAttachment.anInt3952++;
			NameHashTable.method2199(false);
		}
		if (bool_3_) {
			Class98_Sub10_Sub9.anInt5580++;
			DecoratedProgressBarLSEConfig.method904((byte) -110);
		}
	}

	private byte	barId;
	int				price;
	int				sumComplete;
	int				amountComplete;
	int				itemId;
	int				offerAmount;

	public GrandExchangeOffer() {
		/* empty */
	}

	GrandExchangeOffer(RSByteBuffer packet) {
		barId = packet.readSignedByte((byte) -19);
		itemId = packet.readShort((byte) 127);
		price = packet.readInt(-2);
		offerAmount = packet.readInt(-2);
		amountComplete = packet.readInt(-2);
		sumComplete = packet.readInt(-2);
	}

	public final int method1698(boolean bool) {
		if ((barId & 0x8) != 8) {
			return 0;
		}
		return 1;
	}

	public final int method1700(int i) {
		return 0x7 & barId;
	}
}

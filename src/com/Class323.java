/* Class323 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5;

public final class Class323 {
	public static Class128	aClass128_2715	= new Class128();
	public static Js5		animationFrameBasesJs5;

	public static final void method3675(int i, int i_0_, boolean bool, int i_1_, int i_2_) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i, -40, 10);
		class98_sub46_sub17.method1626((byte) -103);
		class98_sub46_sub17.headId = i_1_;
		class98_sub46_sub17.anInt6054 = i_0_;
		class98_sub46_sub17.anInt6053 = i_2_;
	}

	public static final void method3676(int i, int i_3_, int i_4_, int i_5_, byte[] is, boolean bool, int i_6_, byte[] is_7_, int i_8_) {
		if (bool == false) {
			int i_9_ = -(i_5_ >> -1495816926);
			i_5_ = -(i_5_ & 0x3);
			for (int i_10_ = -i; i_10_ < 0; i_10_++) {
				for (int i_11_ = i_9_; (i_11_ ^ 0xffffffff) > -1; i_11_++) {
					is[i_8_++] += -is_7_[i_6_++];
					is[i_8_++] += -is_7_[i_6_++];
					is[i_8_++] += -is_7_[i_6_++];
					is[i_8_++] += -is_7_[i_6_++];
				}
				for (int i_12_ = i_5_; i_12_ < 0; i_12_++) {
					is[i_8_++] += -is_7_[i_6_++];
				}
				i_6_ += i_4_;
				i_8_ += i_3_;
			}
		}
	}

	public static final int method3678(byte i, Class38 class38) {
		if (i != 115) {
			return -85;
		}
		if (class38 != Node.aClass38_834) {
			if (class38 != Class357.aClass38_3026) {
				if (class38 == Class204.aClass38_1552) {
					return 34066;
				}
			} else {
				return 34065;
			}
		} else {
			return 9216;
		}
		throw new IllegalArgumentException();
	}
}

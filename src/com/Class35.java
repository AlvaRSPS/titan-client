
/* Class35 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.File;

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.toolkit.matrix.Matrix;

public final class Class35 {
	public static Class85	aClass85_332	= new Class85(7, 19);
	public static int[]		anIntArray333;
	public static String[]	aStringArray335	= new String[5];

	static {

	}

	public static final int getMaxProgress(int i) {
		if (Class2.fontIds == null) {
			return 0;
		}
		return Class2.fontIds.length * 2;
	}

	public static final int getNativeProgress(int i, String string) {
		if (!Class242.aClass88_1848.is_signed) {
			return -1;
		}
		if (AdvancedMemoryCache.libraryPaths.containsKey(string)) {
			return 100;
		}
		String string_5_ = SystemInformation.getLibraryName(79, string);
		if (string_5_ == null) {
			return -1;
		}
		String string_6_ = TimeTools.aString2863 + string_5_;
		if (!Class223.aClass207_1681.method2737(true, "", string_6_)) {
			return -1;
		}
		if (!Class223.aClass207_1681.isGroupCached(string_6_, 0)) {
			return Class223.aClass207_1681.getGroupProgress(29952, string_6_);
		}
		if (i > -28) {
			anIntArray333 = null;
		}
		byte[] is = Class223.aClass207_1681.method2739(string_6_, "", -32734);
		File file;
		try {
			file = CacheLocator.openFile(string_5_, -127);
		} catch (RuntimeException runtimeException) {
			return -1;
		}
		if (is != null && file != null) {
			boolean bool = true;
			byte[] is_7_ = Class273.method3281(-67, file);
			if (is_7_ != null && is_7_.length == is.length) {
				for (int i_8_ = 0; (i_8_ ^ 0xffffffff) > (is_7_.length ^ 0xffffffff); i_8_++) {
					if (is[i_8_] != is_7_[i_8_]) {
						bool = false;
						break;
					}
				}
			} else {
				bool = false;
			}
			try {
				if (!bool) {
					Class242.aClass88_1848.writeFile(is, true, file);
				}
			} catch (Throwable throwable) {
				return -1;
			}
			Class221.method2821(file, string, -320);
			return 100;
		}
		return -1;
	}

	public static final void method333(Class246_Sub1 class246_sub1, int i) {
		class246_sub1.aClass246_Sub3_5069 = null;
		int i_1_ = class246_sub1.aClass246_Sub6Array5067.length;
		for (int i_2_ = 0; (i_1_ ^ 0xffffffff) < (i_2_ ^ 0xffffffff); i_2_++) {
			class246_sub1.aClass246_Sub6Array5067[i_2_].aBoolean5114 = false;
		}
		synchronized (Class98_Sub46_Sub20_Sub2.aClass218Array6316) {
			if (Class98_Sub46_Sub20_Sub2.aClass218Array6316.length > i_1_ && (Class1.anIntArray65[i_1_] ^ 0xffffffff) > -201) {
				Class98_Sub46_Sub20_Sub2.aClass218Array6316[i_1_].addLast(true, class246_sub1);
				Class1.anIntArray65[i_1_]++;
			}
		}
	}

	public static long method335(long l, long l_3_) {
		return l & l_3_;
	}

	public static final int method338(int i, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, byte i_14_) {
		i_9_ &= 0x3;
		if (i_14_ != -23) {
			return 76;
		}
		if ((i_10_ & 0x1) == 1) {
			int i_15_ = i_13_;
			i_13_ = i;
			i = i_15_;
		}
		if (i_9_ == 0) {
			return i_12_;
		}
		if ((i_9_ ^ 0xffffffff) == -2) {
			return 1 - i_13_ + -i_11_ + 7;
		}
		if ((i_9_ ^ 0xffffffff) == -3) {
			return -i_12_ + 7 - i + 1;
		}
		return i_11_;
	}

	public Matrix	aClass111_334;

	public Class35	aClass35_328;

	public int		anInt327;

	public int		anInt329;

	public int		anInt330;

	public int		anInt331;

	public int		anInt337;

	Class35(int i, int i_16_) {
		anInt329 = i;
		anInt327 = i_16_;
	}

	public final Class66 method331(byte i) {
		return Class21.method263(anInt329, 31866);
	}

	public final Class35 method336(int i, int i_4_) {
		return new Class35(anInt329, i_4_);
	}
}

/* Class98_Sub11 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class OutgoingPacket extends Node {
	public static LinkedList				aClass148_3866					= new LinkedList();
	public static Class85					aClass85_3868;
	public static AnimationSkeletonSet[]	aClass98_Sub46_Sub16Array3870	= new AnimationSkeletonSet[14];

	static {
		aClass85_3868 = new Class85(10, 7);
	}

	public static void method1123(byte i) {
		try {
			if (i != -90) {
				method1124(-18, (byte) 126);
			}
			aClass148_3866 = null;
			aClass98_Sub46_Sub16Array3870 = null;
			aClass85_3868 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "eda.A(" + i + ')');
		}
	}

	public static final void method1124(int i, byte i_0_) {
		try {
			Class232.aClass79_1740.makeSoftReferences((byte) 62, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "eda.E(" + i + ',' + i_0_ + ')');
		}
	}

	public static final void method1127(byte i, int i_2_) {
		do {
			try {
				Class98_Sub36 class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.get(i_2_, i + -68);
				if (class98_sub36 != null) {
					class98_sub36.aClass237_Sub1_4157.method2903(8);
					Class291.method3414(-1, class98_sub36.aBoolean4154, class98_sub36.anInt4160);
					class98_sub36.unlink(69);
				}
				if (i == 67) {
					break;
				}
				quickSort(true, null, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "eda.D(" + i + ',' + i_2_ + ')');
			}
			break;
		} while (false);
	}

	public static final void method1158(int i) {
		if (i == -2) {
			OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class219.aClass171_1640, Class331.aClass117_2811);
			class98_sub11.packet.writeByte(OpenGlModelRenderer.getWindowMode((byte) 112), i ^ 0x24);
			class98_sub11.packet.writeShort(GameShell.frameWidth, i + 1571862890);
			class98_sub11.packet.writeShort(GameShell.frameHeight, 1571862888);
			class98_sub11.packet.writeByte(client.preferences.multiSample.getValue((byte) 120), -70);
			Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
		}
	}

	public static final void quickSort(boolean bool, short[] is, String[] strings) {
		try {
			ProceduralTextureSource.method3207(47, is, strings, strings.length - 1, 0);
			if (bool != true) {
				method1123((byte) 9);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "eda.B(" + bool + ',' + (is != null ? "{...}" : "null") + ',' + (strings != null ? "{...}" : "null") + ')');
		}
	}

	OutgoingOpcode			outgoingOpcode;

	int						packetSize;

	int						anInt3869;

	public RsBitsBuffers	packet;

	public final void method1125(byte i) {
		if ((Class98_Sub33.anInt4117 ^ 0xffffffff) > (StrongReferenceMCNode.aClass98_Sub11Array6302.length ^ 0xffffffff)) {
			if (i != 6) {
				method1124(-24, (byte) -11);
			}
			StrongReferenceMCNode.aClass98_Sub11Array6302[Class98_Sub33.anInt4117++] = this;
		}
	}
}

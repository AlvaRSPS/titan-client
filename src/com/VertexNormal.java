/* Class286 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.VarClientDefinition;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.input.impl.AwtMouseListener;

public final class VertexNormal {
	public static float aFloat2182;

	public static final void method3380(int i, int i_0_) {
		Class98_Sub46_Sub17 dialogue = Class185.method2628(i_0_, i + -72, i);
		dialogue.method1621(0);
	}

	public static final void method3381(int i, int i_1_, int i_2_, int i_3_, Mob class246_sub3_sub4_sub2, int i_4_, Mob class246_sub3_sub4_sub2_5_, int i_6_, int i_7_, int i_8_) {
		int i_9_ = class246_sub3_sub4_sub2_5_.getArmyIcon(28213);
		if (i_9_ != -1) {
			Sprite class332 = (Sprite) Class378.aClass79_3189.get(-120, i_9_);
			if (class332 == null) {
				Image[] class324s = Image.loadImages(client.spriteJs5, i_9_, 0);
				if (class324s == null) {
					return;
				}
				class332 = client.graphicsToolkit.createSprite(class324s[0], true);
				Class378.aClass79_3189.put(i_9_, class332, (byte) -80);
			}
			Class168.method2533(i_7_ >> -1098562655, class246_sub3_sub4_sub2.boundExtentsX, i_8_ >> 101128225, 256 * class246_sub3_sub4_sub2.getSize(0), (byte) 94, class246_sub3_sub4_sub2.boundExtentsZ, i, class246_sub3_sub4_sub2.plane, i_2_, 0);
			int i_10_ = i_1_ - -Class259.anIntArray1957[0] + -18;
			int i_11_ = -70 + Class259.anIntArray1957[1] + i_4_;
			i_10_ += i_6_ / 4 * 18;
			i_11_ += 18 * (i_6_ % 4);
			class332.draw(i_10_, i_11_);
			if (class246_sub3_sub4_sub2_5_ == class246_sub3_sub4_sub2) {
				client.graphicsToolkit.method1781(true, 18, 18, -256, i_10_ - 1, -1 + i_11_);
			}
			AnimatedProgressBarLSEConfig.method908(i_11_ + 18, -1 + i_11_, false, i_10_ - 1, 18 + i_10_);
			Class246_Sub2 class246_sub2 = VarClientDefinition.method883(-90);
			if (i_3_ > 15) {
				class246_sub2.anInt5071 = i_11_;
				class246_sub2.aClass246_Sub3_Sub4_Sub2_5076 = class246_sub3_sub4_sub2_5_;
				class246_sub2.anInt5075 = i_11_ - -16;
				class246_sub2.anInt5073 = 16 + i_10_;
				class246_sub2.anInt5074 = i_10_;
				Class151_Sub2.aClass218_4973.addLast(true, class246_sub2);
			}
		}
	}

	public static final void method3382(int i, int i_12_, int i_13_, int i_14_, int i_15_) {
		if (Class40.anIntArrayArray367 != null) {
			Class40.anIntArrayArray367[i][i_12_] = ~0xffffff | i_13_;
		}
		if (GraphicsDefinitionParser.aShortArrayArray2534 != null) {
			GraphicsDefinitionParser.aShortArrayArray2534[i][i_12_] = (short) i_14_;
		}
		if (AwtMouseListener.aByteArrayArray5291 != null) {
			AwtMouseListener.aByteArrayArray5291[i][i_12_] = (byte) i_15_;
		}
	}

	public static final int method3383(Player player, boolean bool) {
		if (bool != true) {
			return -121;
		}
		int i = player.anInt6522;
		RenderAnimDefinition class294 = player.method3039(1);
		if (player.anInt6385 == -1 || player.aBoolean6359) {
			i = player.anInt6527;
		} else if ((player.anInt6385 ^ 0xffffffff) != (class294.anInt2389 ^ 0xffffffff) && (class294.anInt2361 ^ 0xffffffff) != (player.anInt6385 ^ 0xffffffff) && (player.anInt6385 ^ 0xffffffff) != (class294.anInt2402 ^ 0xffffffff)
				&& class294.anInt2357 != player.anInt6385) {
			if ((player.anInt6385 ^ 0xffffffff) == (class294.anInt2368 ^ 0xffffffff) || (class294.anInt2394 ^ 0xffffffff) == (player.anInt6385 ^ 0xffffffff) || player.anInt6385 == class294.anInt2403 || (player.anInt6385 ^ 0xffffffff) == (class294.anInt2377
					^ 0xffffffff)) {
				i = player.anInt6524;
			}
		} else {
			i = player.anInt6517;
		}
		return i;
	}

	public int	anInt2180;

	public int	faceCount;

	public int	anInt2183;

	public int	anInt2184;

	public VertexNormal() {
		/* empty */
	}
}

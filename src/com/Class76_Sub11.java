
/* Class76_Sub11 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Exception;

public final class Class76_Sub11 extends Class76 {
	public static Sprite				aClass332_3795;
	public static AdvancedMemoryCache	aClass79_3797	= new AdvancedMemoryCache(8);
	public static int					anInt3798;
	public static int					anInt3800;
	public static int[]					anIntArray3796	= new int[250];

	Class76_Sub11(NativeToolkit var_ha_Sub3) {
		super(var_ha_Sub3);
	}

	@Override
	public final void method739(int i) {
		try {
			if (i != -2) {
				method745((byte) -106);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wk.C(" + i + ')');
		}
	}

	@Override
	public final void method742(int i, int i_6_, Interface4 interface4) {
		try {
			if (i != 6) {
				anInt3798 = 101;
			}
			this.aHa_Sub3_585.method2005(interface4, -123);
			this.aHa_Sub3_585.method2015(i_6_, (byte) 117);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wk.I(" + i + ',' + i_6_ + ',' + (interface4 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method743(int i, boolean bool) {
		if (i <= 93) {
			RSToolkit.createToolkit(-78, null, 97, null, 28, null);
		}
	}

	@Override
	public final boolean method745(byte i) {
		try {
			if (i != 27) {
				GameShell.applet = null;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wk.H(" + i + ')');
		}
	}

	@Override
	public final void method746(int i, int i_4_, int i_5_) {
		do {
			try {
				if (i_5_ < -75) {
					break;
				}
				aClass332_3795 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wk.E(" + i + ',' + i_4_ + ',' + i_5_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method748(int i, boolean bool) {
		try {
			if (i != 69) {
				aClass332_3795 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wk.B(" + i + ',' + bool + ')');
		}
	}
}

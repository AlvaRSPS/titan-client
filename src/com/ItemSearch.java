/* Class57 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;

public final class ItemSearch {
	public static int[]	anIntArray457	= null;
	public static int[]	anIntArray458	= new int[1];

	public static final void findItems(boolean stockMarketOnly, int paramId, String seekItemNamePart, int value, String string_1_, boolean paramIsString, int seekParamIntegerValue) {
		Class208.itemsJs5.discardUnpacked = 1;
		seekItemNamePart = seekItemNamePart.toLowerCase();
		short[] foundIndices = new short[16];
		int paramDefinitionInteger = -1;
		String paramDefinitionString = null;
		if (paramId != -1) {
			ParamDefinition definition = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, paramId);
			if (definition == null || !paramIsString == definition.isString(false)) {
				return;
			}
			if (definition.isString(false)) {
				paramDefinitionString = definition.defaultString;
			} else {
				paramDefinitionInteger = definition.defaultInteger;
			}
		}
		int foundIndexPtr = 0;
		for (int objectIndex = 0; objectIndex < Class98_Sub46_Sub19.itemDefinitionList.count; objectIndex++) {
			ItemDefinition itemDefinition = Class98_Sub46_Sub19.itemDefinitionList.get(objectIndex, (byte) -119);
			if ((!stockMarketOnly || itemDefinition.stockMarket) && (itemDefinition.certTemplate ^ 0xffffffff) == 0 && itemDefinition.lentTemplate == -1 && (itemDefinition.dummyItem ^ 0xffffffff) == -1 && (itemDefinition.name.toLowerCase().indexOf(seekItemNamePart) ^ 0xffffffff) != 0) {
				if ((paramId ^ 0xffffffff) != 0) {
					if (paramIsString) {
						if (!string_1_.equals(itemDefinition.getParam(paramDefinitionString, -1, paramId))) {
							continue;
						}
					} else if (itemDefinition.method3494(paramId, (byte) -90, paramDefinitionInteger) != seekParamIntegerValue) {
						continue;
					}
				}
				if (foundIndexPtr >= 250) {
					MaxScreenSizePreferenceField.resultIndexBuffer = null;
					Class18.resultBufferSize = -1;
					return;
				}
				if (foundIndices.length <= foundIndexPtr) {
					short[] newFoundIndices = new short[foundIndices.length * 2];
					for (int index = 0; (index ^ 0xffffffff) > (foundIndexPtr ^ 0xffffffff); index++) {
						newFoundIndices[index] = foundIndices[index];
					}
					foundIndices = newFoundIndices;
				}
				foundIndices[foundIndexPtr++] = (short) objectIndex;
			}
		}
		MaxScreenSizePreferenceField.resultIndexBuffer = foundIndices;
		Class18.resultBufferSize = foundIndexPtr;
		Class85.resultBufferPtr = 0;
		String[] foundNames = new String[Class18.resultBufferSize];
		for (int listPtr = 0; (Class18.resultBufferSize ^ 0xffffffff) < (listPtr ^ 0xffffffff); listPtr++) {
			foundNames[listPtr] = Class98_Sub46_Sub19.itemDefinitionList.get(foundIndices[listPtr], (byte) -127).name;
		}
		OutgoingPacket.quickSort(true, MaxScreenSizePreferenceField.resultIndexBuffer, foundNames);
		Class208.itemsJs5.discardAllUnpacked((byte) -116);
		Class208.itemsJs5.discardUnpacked = value;
	}

	public static final void findItems(boolean stockmarketOnly, String name, int i) {
		ItemSearch.findItems(stockmarketOnly, name, -1, -1, -1);
	}

	public static final void findItems(boolean stockMarketOnly, String seekItemNamePart, int seekParamIntegerValue, int dummy, int paramIndex) {
		if (dummy == -1) {
			findItems(stockMarketOnly, paramIndex, seekItemNamePart, 2, null, false, seekParamIntegerValue);
		}
	}

	public static final void findItems(String seekItemNamePart, String paramValue, byte i, boolean stockMarketOnly, int paramIndex) {
		ItemSearch.findItems(stockMarketOnly, paramIndex, seekItemNamePart, 2, paramValue, true, -1);
	}
}

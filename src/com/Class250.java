/* Class250 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;

public final class Class250 {
	public static OutgoingOpcode aClass171_1913 = new OutgoingOpcode(76, 7);

	public static void method3165(int i) {
		try {
			aClass171_1913 = null;
			if (i != 76) {
				method3165(-70);
			}
			client.secondLobbyServer = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pl.B(" + i + ')');
		}
	}

	public static final int method3166(int i, int i_0_, int i_1_, byte i_2_) {
		try {
			i_1_ &= 0x3;
			if (i_1_ == 0) {
				return i_0_;
			}
			if (i_1_ == 1) {
				return -i + 7;
			}
			if (i_1_ == 2) {
				return -i_0_ + 7;
			}
			return i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pl.C(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public static final void method3167(byte i, Class98_Sub31 class98_sub31) {
		try {
			if (class98_sub31.aClass98_Sub24_4104 != null) {
				class98_sub31.aClass98_Sub24_4104.anInt4008 = 0;
			}
			if (i == -32) {
				class98_sub31.aBoolean4102 = false;
				for (Class98_Sub31 class98_sub31_6_ = class98_sub31.method1322(); class98_sub31_6_ != null; class98_sub31_6_ = class98_sub31.method1327()) {
					method3167((byte) -32, class98_sub31_6_);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pl.D(" + i + ',' + (class98_sub31 != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method3168(Js5 class207, boolean bool, TextureMetricsList var_d) {
		do {
			try {
				Class98_Sub10_Sub39.aClass207_5773 = class207;
				Class98_Sub10_Sub8.aD5578 = var_d;
				if (bool == true) {
					break;
				}
				method3166(-101, -39, 69, (byte) -47);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "pl.A(" + (class207 != null ? "{...}" : "null") + ',' + bool + ',' + (var_d != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public float[][]	aFloatArrayArray1910;

	public int[]		anIntArray1911;

	public int[]		anIntArray1912;

	public int[]		anIntArray1915;

	Class250(int[] is, int[] is_4_, int[] is_5_, float[][] fs) {
		try {
			anIntArray1915 = is_4_;
			anIntArray1911 = is;
			anIntArray1912 = is_5_;
			aFloatArrayArray1910 = fs;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pl.<init>(" + (is != null ? "{...}" : "null") + ',' + (is_4_ != null ? "{...}" : "null") + ',' + (is_5_ != null ? "{...}" : "null") + ',' + (fs != null ? "{...}" : "null") + ')');
		}
	}
}

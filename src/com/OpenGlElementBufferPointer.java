/* Class322 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.LightIntensityDefinition;

public final class OpenGlElementBufferPointer {
	public static float				aFloat2712;
	public static Js5				quickChatMenuJs5;
	public static IncomingOpcode	ATTACH_INTERFACE	= new IncomingOpcode(50, 7);

	public static final String decodeString(RSByteBuffer buffer, byte i) {
		return Class98_Sub10_Sub26.method1084(false, buffer, 32767);
	}

	public static final void method3671(int i, int i_0_, int i_1_, Class246_Sub3_Sub1 class246_sub3_sub1) {
		Class172 class172 = Class100.method1693(i, i_0_, i_1_);
		if (class172 != null) {
			class172.aClass246_Sub3_Sub1_1332 = class246_sub3_sub1;
			int i_2_ = Class78.aSArray594 == Class81.aSArray618 ? 1 : 0;
			if (class246_sub3_sub1.method2978(-127)) {
				if (class246_sub3_sub1.method2987(6540)) {
					((Char) class246_sub3_sub1).animator = Class359.aClass246_Sub3Array3056[i_2_];
					Class359.aClass246_Sub3Array3056[i_2_] = class246_sub3_sub1;
				} else {
					((Char) class246_sub3_sub1).animator = LightIntensityDefinition.aClass246_Sub3Array3198[i_2_];
					LightIntensityDefinition.aClass246_Sub3Array3198[i_2_] = class246_sub3_sub1;
					Class358.aBoolean3033 = true;
				}
			} else {
				((Char) class246_sub3_sub1).animator = Class130.aClass246_Sub3Array1029[i_2_];
				Class130.aClass246_Sub3Array1029[i_2_] = class246_sub3_sub1;
			}
		}
	}

	public static final boolean method3672(int i, int i_3_, int i_4_) {
		if (i_4_ != 2048) {
			return true;
		}
		if ((0x800 & i_3_) == 0) {
			return false;
		}
		return true;
	}

	public static final void method3673(int i, int i_5_, int i_6_, boolean bool) {
		Class281.flags = new byte[i_6_][i_5_][i];
		if (bool != true) {
			return;
		}
	}

	public static final void method3674(int i, Player player) {
		Class98_Sub42 class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub14.aClass377_5612.get(((Mob) player).index, -1);
		if (class98_sub42 == null) {
			Class98_Sub31_Sub4.method1383(player, null, ((Mob) player).pathX[0], 0, i + 3, ((Mob) player).pathZ[0], ((Char) player).plane, null);
		} else {
			class98_sub42.method1478(true);
		}
	}

	public OpenGLElementBuffer anInterface8_2711;
}


/* Class98_Sub45 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Node;

public final class ItemDeque extends Node {
	public static OutgoingOpcode	aClass171_4256;
	public static int[]				anIntArray4257;
	public static int[][]			anIntArrayArray4258;
	public static String[]			ignoreDisplayNames	= new String[100];

	static {
		aClass171_4256 = new OutgoingOpcode(33, 3);
		anIntArray4257 = new int[32];
		anIntArrayArray4258 = new int[][] { { 2, 4 }, { 2, 4 }, { 5, 2, 4 }, { 4, 5, 2 }, { 2, 4, 5 }, { 5, 2, 4 }, { 1, 6, 2, 5 }, { 1, 6, 7, 1 }, { 6, 7, 1, 1 }, { 0, 8, 9, 8, 9, 4 }, { 8, 9, 4, 0, 8, 9 }, { 2, 10, 0, 10, 11, 11 }, { 2, 4 }, { 1, 6, 7, 1 }, { 1, 6, 7, 1 } };
	}

	public static final void addChatMessage(byte dummy, int type, String message, int flags, String nameUnfiltered, String displayName, String nameSimple) {
		Class137.addChatMessage(nameSimple, type, displayName, message, -1, null, (byte) -99, flags, nameUnfiltered);
	}

	public static final Class92 method1520(int i, int i_2_) {
		if (i_2_ != 14883) {
			anIntArray4257 = null;
		}
		Class92 class92 = (Class92) Class142.aClass79_1158.get(-126, i);
		if (class92 != null) {
			return class92;
		}
		byte[] is = Class42_Sub1_Sub1.aClass207_6206.getFile(i, 0, false);
		class92 = new Class92();
		if (is != null) {
			class92.decode(-62, new RSByteBuffer(is));
		}
		class92.method897((byte) 115);
		Class142.aClass79_1158.put(i, class92, (byte) -80);
		return class92;
	}

	public LinkedList aClass148_4254 = new LinkedList();

	public ItemDeque() {
		/* empty */
	}
}

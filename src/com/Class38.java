/* Class38 - Decompiled by JODE
 */ package com; /*
					*/

public final class Class38 {
	public static Class100				aClass100_357				= new Class100(128);
	public static TextureMetricsList	aD356;
	public static int					anInt355;
	public static int					anInt358;
	public static int					anInt359;
	public static int					anInt360;
	public static int					DECODE_MASKS_PLAYERS_COUNT	= 0;

	public static final String method347(RtInterface class293, byte i, int i_0_) {
		if (!client.method116(class293).method1666((byte) -72, i_0_) && class293.param == null) {
			return null;
		}
		if (class293.menuActions == null || (i_0_ ^ 0xffffffff) <= (class293.menuActions.length ^ 0xffffffff) || class293.menuActions[i_0_] == null || class293.menuActions[i_0_].trim().length() == 0) {
			if (Class15.qaOpTest) {
				return "Hidden-" + i_0_;
			}
			return null;
		}
		return class293.menuActions[i_0_];
	}

	public Class38() {
		/* empty */
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

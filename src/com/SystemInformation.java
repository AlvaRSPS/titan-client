/* Class145 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.ui.loading.impl.elements.impl.TextLoadingScreenElement;

public final class SystemInformation {
	public static Class268	aClass268_1173;
	public static int		anInt1170		= -1;
	public static int		anInt1172		= 0;
	public static int[]		anIntArray1175;
	public static int[]		anIntArray1177	= new int[2];

	public static final String getLibraryName(int i, String string) {
		if (TextLoadingScreenElement.osName.startsWith("win")) {
			return string + ".dll";
		}
		if (!TextLoadingScreenElement.osName.startsWith("linux")) {
			if (TextLoadingScreenElement.osName.startsWith("mac")) {
				return "lib" + string + ".dylib";
			}
		} else {
			return "lib" + string + ".so";
		}
		return null;
	}

	public static final long method2313(byte i, String string) {
		int length = string.length();
		long l = 0L;
		for (int index = 0; (length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
			l = string.charAt(index) + (l << -470840507) + -l;
		}
		return l;
	}

	private boolean	aBoolean1174;

	private boolean	aBoolean1176;

	private int		availableProcessors;

	private int		maxHeap;

	public SystemInformation(boolean bool, int heap, int avProcessors, boolean bool_4_) {
		availableProcessors = avProcessors;
		aBoolean1174 = bool;
		aBoolean1176 = bool_4_;
		maxHeap = heap;
	}

	public final int getAvailableProcessors(int i) {
		return availableProcessors;
	}

	public final int getMaxHeapMb(int i) {
		return maxHeap;
	}

	public final boolean isArm(boolean bool) {
		return aBoolean1176;
	}

	public final boolean method2317(boolean bool) {
		return aBoolean1174;
	}
}

/* Class49 - Decompiled by JODE
 */ package com; /*
					*/

public final class Class49 {
	public static OutgoingOpcode	aClass171_413	= new OutgoingOpcode(38, 7);
	public static float				aFloat416;
	public static int				anInt415		= 0;
	public static int[]				femaleParts;

	static {
		femaleParts = new int[] { 7, 8, 9, 10, 11, 12, 13, 15 };
	}

	public static final void method477(int i) {
		Class336.aClass148_2827.clear((byte) 47);
		Class62.anInt490 = 0;
	}

	private long	aLong412;

	private int		anInt411;

	public Class49(Class169 class169) {
		anInt411 = 1;
		aLong412 = class169.anInt1300;
	}

	public Class49(Class169[] class169s) {
		for (int i = 0; (class169s.length ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
			method478(class169s[i], 13);
		}
	}

	private final void method478(Class169 class169, int i) {
		aLong412 |= class169.anInt1300 << Class169.anInt1304 * anInt411++;
	}

	public final Class169 method479(int i, byte i_0_) {
		return Class169.method2537(method481(15, i), (byte) 40);
	}

	public final int method480(byte i) {
		return anInt411;
	}

	private final int method481(int i, int i_1_) {
		return 0xf & (int) (aLong412 >> Class169.anInt1304 * i_1_);
	}
}

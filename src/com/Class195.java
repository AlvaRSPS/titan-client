/* Class195 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;
import com.jagex.game.client.preferences.IdleAnimationsPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.RotatingSpriteLoadingScreenElement;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class Class195 {
	public static int	anInt1506;
	public static int[]	anIntArray1497	= new int[16];
	public static Font	p12Full;

	public static void method2660(int i) {
		try {
			if (i != 26845) {
				method2660(22);
			}
			anIntArray1497 = null;
			p12Full = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mt.D(" + i + ')');
		}
	}

	public static final boolean method2661(int i, int i_0_, int i_1_, int i_2_, byte i_3_) {
		try {
			if (i_3_ > -94) {
				method2662(-60);
			}
			if (!RtMouseEvent.occlusion || !Js5Client.aBoolean1052) {
				return false;
			}
			if ((Class4.pixelsOccludedCount ^ 0xffffffff) > -101) {
				return false;
			}
			if (!Class76_Sub5.method758((byte) 72, i, i_0_, i_1_)) {
				return false;
			}
			int i_4_ = i_1_ << Class151_Sub8.tileScale;
			int i_5_ = i_0_ << Class151_Sub8.tileScale;
			if (OpenGlGround.method3427(Class78.aSArray594[i].getTileHeight(i_0_, -12639, i_1_), NativeShadow.anInt6333, i_2_, NativeShadow.anInt6333, (byte) 16, i_5_, i_4_)) {
				Class151_Sub7.cpsOccludedCount++;
				return true;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mt.C(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public static final boolean method2662(int i) {
		try {
			if (client.useObjectTag) {
				try {
					JavaScriptInterface.callJsMethod("showVideoAd", GameShell.applet, -26978);
					return true;
				} catch (Throwable throwable) {
					/* empty */
				}
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mt.F(" + i + ')');
		}
	}

	public static final boolean method2663(int i, int i_6_, boolean bool) {
		try {
			if (bool != false) {
				method2660(-61);
			}
			return (i & 0x34 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mt.A(" + i + ',' + i_6_ + ',' + bool + ')');
		}
	}

	boolean						aBoolean1501;
	private NativeToolkit		aHa_Sub3_1504;

	Interface4_Impl1			anInterface4_Impl1_1498;

	Interface4_Impl1			anInterface4_Impl1_1500		= null;

	Interface4_Impl2[]			anInterface4_Impl2Array1503	= null;

	private Interface4_Impl2[]	anInterface4_Impl2Array1505;

	Class195(NativeToolkit var_ha_Sub3) {
		anInterface4_Impl1_1498 = null;
		anInterface4_Impl2Array1505 = null;
		try {
			aHa_Sub3_1504 = var_ha_Sub3;
			aBoolean1501 = aHa_Sub3_1504.aBoolean4588;
			if (aBoolean1501 && !aHa_Sub3_1504.method1977(Class162.aClass162_1266, true, Class74.aClass164_547)) {
				aBoolean1501 = false;
			}
			if (aBoolean1501 || aHa_Sub3_1504.method1942(0, Class74.aClass164_547, Class162.aClass162_1266)) {
				Class98_Sub42.method1477(false);
				if (!aBoolean1501) {
					anInterface4_Impl2Array1503 = new Interface4_Impl2[16];
					for (int i = 0; (i ^ 0xffffffff) > -17; i++) {
						byte[] is = Class98_Sub10_Sub20.method1061(2, 32768, 2 * 128 * i * 128, Class241.anObject1847);
						anInterface4_Impl2Array1503[i] = aHa_Sub3_1504.method2053(128, Class74.aClass164_547, (byte) 87, is, true, 128);
					}
					anInterface4_Impl2Array1505 = new Interface4_Impl2[16];
					for (int i = 0; i < 16; i++) {
						byte[] is = Class98_Sub10_Sub20.method1061(2, 32768, 16384 * i * 2, IdleAnimationsPreferenceField.anObject3709);
						anInterface4_Impl2Array1505[i] = aHa_Sub3_1504.method2053(128, Class74.aClass164_547, (byte) 87, is, true, 128);
					}
				} else {
					byte[] is = Class98_Sub28_Sub1.unpackDataBuffer(false, Class241.anObject1847, false);
					anInterface4_Impl1_1498 = aHa_Sub3_1504.method2044(95, Class74.aClass164_547, is, 128, 128, 16);
					is = Class98_Sub28_Sub1.unpackDataBuffer(false, IdleAnimationsPreferenceField.anObject3709, false);
					aHa_Sub3_1504.method2044(125, Class74.aClass164_547, is, 128, 128, 16);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mt.<init>(" + (var_ha_Sub3 != null ? "{...}" : "null") + ')');
		}
	}

	public final boolean method2659(int i) {
		try {
			if (i != -22382) {
				return false;
			}
			if (!aBoolean1501) {
				return anInterface4_Impl2Array1503 != null;
			}
			return anInterface4_Impl1_1498 != null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mt.B(" + i + ')');
		}
	}

	public final boolean method2664(int i) {
		try {
			if (anInterface4_Impl1_1500 == null) {
				if (Class332_Sub1.anObject5409 == null) {
					byte[] is = RotatingSpriteLoadingScreenElement.method2244(-31633, 16.0F, 0.5F, 0.6F, 16, 8, 128, new Class39_Sub1(419684), 4.0F, 4.0F, 128);
					Class332_Sub1.anObject5409 = GroundBlendingPreferenceField.method654(2, is, false);
				}
				byte[] is = Class98_Sub28_Sub1.unpackDataBuffer(false, Class332_Sub1.anObject5409, false);
				byte[] is_8_ = new byte[is.length * 4];
				int i_9_ = 0;
				for (int i_10_ = 0; (i_10_ ^ 0xffffffff) > -17; i_10_++) {
					int i_11_ = 128 * i_10_ * 128;
					int i_12_ = i_11_;
					for (int i_13_ = 0; (i_13_ ^ 0xffffffff) > -129; i_13_++) {
						int i_14_ = i_12_ + i_13_ * 128;
						int i_15_ = i_12_ - -(128 * (0x7f & -1 + i_13_));
						int i_16_ = i_12_ + 128 * (i_13_ + 1 & 0x7f);
						for (int i_17_ = 0; i_17_ < 128; i_17_++) {
							float f = (is[i_17_ + i_15_] & 0xff) - (0xff & is[i_16_ + i_17_]);
							float f_18_ = (is[(-1 + i_17_ & 0x7f) + i_14_] & 0xff) - (is[(1 + i_17_ & 0x7f) + i_14_] & 0xff);
							float f_19_ = (float) (128.0 / Math.sqrt(f * f + (16384.0F + f_18_ * f_18_)));
							is_8_[i_9_++] = (byte) (int) (f_18_ * f_19_ + 127.0F);
							is_8_[i_9_++] = (byte) (int) (127.0F + 128.0F * f_19_);
							is_8_[i_9_++] = (byte) (int) (f_19_ * f + 127.0F);
							is_8_[i_9_++] = is[i_11_++];
						}
					}
				}
				anInterface4_Impl1_1500 = aHa_Sub3_1504.method2044(-81, Class62.aClass164_486, is_8_, 128, 128, 16);
			}
			return anInterface4_Impl1_1500 != null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mt.E(" + i + ')');
		}
	}
}

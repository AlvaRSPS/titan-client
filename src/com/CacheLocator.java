
/* Class316 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.File;
import java.io.RandomAccessFile;
import java.util.Hashtable;

public class CacheLocator {
	private static int						cache_id;
	private static Hashtable<String, File>	fileCache	= new Hashtable<String, File>(16);
	private static String					game_name;
	private static boolean					initialized	= false;
	private static String					userHome;

	public static void initialize(boolean bool, int cacheId, String gameName) {
		do {
			cache_id = cacheId;
			game_name = gameName;
			try {
				if (bool != false) {
					break;
				}
				userHome = System.getProperty("user.home");
				if (userHome != null) {
					userHome += "/";
				}
			} catch (Exception exception) {
				/* empty */
			}
			initialized = true;
			if (userHome != null) {
				break;
			}
			userHome = "~/";
			break;
		} while (false);
	}

	public static File openFile(int cacheId, int dummyInteger, String fileName, String gameName) {
		if (!initialized) {
			throw new RuntimeException("");
		}
		File file = fileCache.get(fileName);
		if (file != null) {
			return file;
		}
		String[] cacheLocations = { "c:/rscache/", "/rscache/", "c:/windows/", "c:/winnt/", "c:/", userHome, "/tmp/", "" };
		String[] cacheDirectories = { ".titan_cache_" + cacheId, ".file_store_" + cacheId };
		for (int tryNo = dummyInteger; (tryNo ^ 0xffffffff) > -3; tryNo++) {
			for (int count = 0; (count ^ 0xffffffff) > (cacheDirectories.length ^ 0xffffffff); count++) {
				for (String cacheLocation : cacheLocations) {
					String filePath = cacheLocation + cacheDirectories[count] + "/" + (gameName != null ? gameName + "/" : "") + fileName;
					RandomAccessFile randomaccessfile = null;
					try {
						File openedFile = new File(filePath);
						if ((tryNo ^ 0xffffffff) != -1 || openedFile.exists()) {
							if ((tryNo ^ 0xffffffff) != -2 || cacheLocation.length() <= 0 || new File(cacheLocation).exists()) {
								new File(cacheLocation + cacheDirectories[count]).mkdir();
								if (gameName != null) {
									new File(cacheLocation + cacheDirectories[count] + "/" + gameName).mkdir();
								}
								randomaccessfile = new RandomAccessFile(openedFile, "rw");
								int testValue = randomaccessfile.read();
								randomaccessfile.seek(0L);
								randomaccessfile.write(testValue);
								randomaccessfile.seek(0L);
								randomaccessfile.close();
								fileCache.put(fileName, openedFile);
								return openedFile;
							}
						}
					} catch (Exception exception) {
						try {
							if (randomaccessfile != null) {
								randomaccessfile.close();
							}
						} catch (Exception exception_10_) {
							/* empty */
						}
					}
				}
			}
		}
		throw new RuntimeException();
	}

	public static File openFile(String fileName, int i) {
		return openFile(cache_id, 0, fileName, game_name);
	}
}

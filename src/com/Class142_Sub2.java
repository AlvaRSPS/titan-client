/* Class142_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.timetools.general.TimeTools;

public final class Class142_Sub2 extends Class142 {
	public Class142_Sub2() {
		/* empty */
	}

	@Override
	public final long method2308(byte i) {
		return TimeTools.getCurrentTime(-47) * 1000000L;
	}
}

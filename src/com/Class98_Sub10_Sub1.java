/* Class98_Sub10_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.input.impl.AwtKeyListener;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class98_Sub10_Sub1 extends Class98_Sub10 {
	public static int	anInt5543;
	public static Js5	musicJs5;

	public static final void handleLogout(boolean bool, boolean bool_14_) {
		if (aa_Sub1.aClass123_3561 != null) {
			aa_Sub1.aClass123_3561.close(-14);
			aa_Sub1.aClass123_3561 = null;
		}
		MaxScreenSizePreferenceField.anInt3680 = 0;
		Class98_Sub10_Sub15.method1050((byte) 97);
		PacketBufferManager.method2229();
		for (int index = 0; index < 4; index++) {
			VarPlayerDefinition.clipMaps[index].method2950((byte) -99);
		}
		OpenGLHeightMapToNormalMapConverter.method2173(false, -7);
		System.gc();
		Class96.method923(127, 2);
		Class151_Sub5.aBoolean4991 = bool;
		Class144.anInt1169 = -1;
		Class233.method2883((byte) 111);
		TextResources.method3614(true, 121);
		Class272.gameSceneBaseX = 0;
		Class160.centreX = 0;
		Class275.centreY = 0;
		aa_Sub2.gameSceneBaseY = 0;
		OpenGLHeightMapToNormalMapConverter.anInt978 = Class151_Sub9.anInt5028 = 0;
		for (int index = 0; OpenGlArrayBufferPointer.aClass36Array903.length > index; index++) {
			OpenGlArrayBufferPointer.aClass36Array903[index] = null;
		}
		HitmarksDefinitionParser.method2195(30574);
		for (int index = 0; index < 2048; index++) {
			Class151_Sub9.players[index] = null;
		}
		Class150.npcCount = 0;
		ProceduralTextureSource.npc.clear(-96);
		Class98_Sub10_Sub20.anInt5640 = 0;
		ModelRenderer.groundItems.clear(-126);
		Class284.method3359(9268);
		StartupStage.anInt581 = 0;
		StartupStage.varValues.method2288((byte) -103);
		SpriteProgressBarLoadingScreenElement.method3977(true);
		FriendLoginUpdate.method3104(5134);
		CursorDefinitionParser.aLong121 = 0L;
		Class284.aClass98_Sub4_2167 = null;
		if (bool_14_) {
			HashTableIterator.setClientState(12, false);
		} else {
			HashTableIterator.setClientState(3, false);
			try {
				JavaScriptInterface.callJsMethod("loggedout", GameShell.applet, -26978);
			} catch (Throwable throwable) {
				/* empty */
			}
		}
	}

	public static final void method1002(boolean bool) {
		try {
			FloorOverlayDefinition.floorOverlayDefinitionList.cacheMakeSoftReferences((byte) 30, 5);
			Class82.floorUnderDefinitionList.makeSoftReferences(5, 1);
			ParamDefinition.identikitDefinitionList.method827((byte) -63, 5);
			Class130.gameObjectDefinitionList.method3548(5, (byte) 85);
			Class4.npcDefinitionList.method3539(5, (byte) 123);
			Class98_Sub46_Sub19.itemDefinitionList.makeSoftReferences(5, 1);
			Class151_Sub7.animationDefinitionList.method2621(5, 3);
			BuildLocation.gfxDefinitionList.makeSoftReferences((byte) -53, 5);
			DataFs.bConfigDefinitionList.method2681((byte) 126, 5);
			SpriteLoadingScreenElement.varPlayerDefinitionList.method2284((byte) -120, 5);
			Class370.renderAnimDefinitionList.method3197((byte) 30, 5);
			Class216.worldMapInfoDefinitionList.method3806(5, false);
			Class98_Sub10_Sub23.mapScenesDefinitionList.method3767(56, 5);
			Class98_Sub43_Sub1.paramDefinitionList.method3943(5, false);
			Class303.questDefinitionList.makeSoftReferences(32, 5);
			SimpleProgressBarLoadingScreenElement.skyboxDefinitionList.method530((byte) -32, 5);
			GrandExchangeOffer.sunDefinitionList.method2153(5, (byte) -125);
			Class21_Sub1.lightIntensityDefinitionList.method3272(119, 5);
			Class18.cursorDefinitionList.method203(bool, 5);
			Class62.structsDefinitionList.method3227(5, -112);
			Class246_Sub3_Sub1.hitmarksDefinitionList.makeSoftReferences(5, (byte) 126);
			Font.method404(5, 2974);
			LightIntensityDefinitionParser.method3266(50, (byte) -50);
			OutgoingPacket.method1124(50, (byte) 78);
			AwtKeyListener.method787((byte) 110, 5);
			Class67.method687((byte) 118, 5);
			Class275.aClass79_2046.makeSoftReferences((byte) 62, 5);
			Class224_Sub3.aClass79_5039.makeSoftReferences((byte) 62, 5);
			Class378.aClass79_3189.makeSoftReferences((byte) 62, 5);
			Class98_Sub6.aClass79_3847.makeSoftReferences((byte) 62, 5);
			ClientScript2Runtime.aClass79_1890.makeSoftReferences((byte) 62, 5);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aca.H(" + bool + ')');
		}
	}

	public static final boolean method1005(int i, int i_16_, byte i_17_) {
		try {
			if (i_17_ != -23) {
				musicJs5 = null;
			}
			if ((0x800 & i ^ 0xffffffff) == -1 || (i_16_ & 0x37 ^ 0xffffffff) == -1) {
				return false;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aca.D(" + i + ',' + i_16_ + ',' + i_17_ + ')');
		}
	}

	public static void method1006(int i) {
		try {
			if (i == -1) {
				client.startupStages = null;
				musicJs5 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aca.B(" + i + ')');
		}
	}

	public static final void setParticleSetting(int i, int i_15_) {
		try {
			if ((i_15_ ^ 0xffffffff) > -1 || (i_15_ ^ 0xffffffff) < -3) {
				i_15_ = 0;
			}
			if (i > -41) {
				client.startupStages = null;
			}
			RotatingSpriteLSEConfig.anInt5497 = i_15_;
			SimpleProgressBarLoadingScreenElement.aClass246_Sub5Array5469 = new Class246_Sub5[1 + Class224_Sub1.anIntArray5034[RotatingSpriteLSEConfig.anInt5497]];
			Class258.anInt1952 = 0;
			Class273.anInt2039 = 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aca.F(" + i + ',' + i_15_ + ')');
		}
	}

	private int anInt5541 = 4096;

	public Class98_Sub10_Sub1() {
		super(1, true);
	}

	@Override
	public final int[] method990(int i, int i_0_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_0_);
			if (i != 255) {
				anInt5543 = 92;
			}
			if (this.aClass16_3863.aBoolean198) {
				int[] is_1_ = method1000(i_0_ + -1 & SoftwareNativeHeap.anInt6075, 0, i ^ 0xff);
				int[] is_2_ = method1000(i_0_, 0, 0);
				int[] is_3_ = method1000(SoftwareNativeHeap.anInt6075 & 1 + i_0_, 0, 0);
				for (int i_4_ = 0; (i_4_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_4_++) {
					int i_5_ = anInt5541 * (is_3_[i_4_] - is_1_[i_4_]);
					int i_6_ = (is_2_[i_4_ - -1 & Class329.anInt2761] - is_2_[Class329.anInt2761 & i_4_ + -1]) * anInt5541;
					int i_7_ = i_6_ >> 1820664684;
					int i_8_ = i_5_ >> 1009040556;
					int i_9_ = i_7_ * i_7_ >> 466133996;
					int i_10_ = i_8_ * i_8_ >> -1631782228;
					int i_11_ = (int) (4096.0 * Math.sqrt((i_9_ - -i_10_ + 4096) / 4096.0F));
					int i_12_ = (i_11_ ^ 0xffffffff) != -1 ? 16777216 / i_11_ : 0;
					is[i_4_] = 4096 + -i_12_;
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aca.G(" + i + ',' + i_0_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_13_) {
		do {
			try {
				if (i_13_ >= -92) {
					musicJs5 = null;
				}
				if ((i ^ 0xffffffff) != -1) {
					break;
				}
				anInt5541 = class98_sub22.readShort((byte) 127);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "aca.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_13_ + ')');
			}
			break;
		} while (false);
	}
}

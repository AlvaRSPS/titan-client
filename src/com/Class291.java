/* Class291 - Decompiled by JODE
 */ package com; /*
					*/

public final class Class291 {
	public static double aDouble2199;

	public static final void method3414(int i, boolean bool, int i_0_) {
		if (i == -1) {
			if (bool) {
				OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class98_Sub42.aClass171_4235, Class331.aClass117_2811);
				frame.packet.writeShort(i_0_, 1571862888);
				Class98_Sub10_Sub29.sendPacket(false, frame);
			} else {
				ClientScript2Runtime.method3152(Class288.aClass105_3375, i_0_, -1);
			}
		}
	}

	public static final void method3415(int i, RSByteBuffer buffer) {
		if (i > 64) {
			for (int i_1_ = 0; i_1_ < Class42_Sub3.anInt5366; i_1_++) {
				int i_2_ = buffer.readSmart(1689622712);
				int i_3_ = buffer.readShort((byte) 127);
				if (i_3_ == 65535) {
					i_3_ = -1;
				}
				if (OpenGlTileMaterial.aClass53_Sub1Array3967[i_2_] != null) {
					OpenGlTileMaterial.aClass53_Sub1Array3967[i_2_].anInt429 = i_3_;
				}
			}
		}
	}
}

/* Class98_Sub50 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5FileRequest;
import com.jagex.game.client.definition.AnimationDefinition;

public final class Class98_Sub50 extends Node {
	public static OutgoingOpcode		aClass171_4290					= new OutgoingOpcode(69, 2);
	public static Sprite				aClass332_4287;
	public static AnimationDefinition[]	aClass97Array4293				= new AnimationDefinition[14];
	public static int					anInt4289;
	public static int					anInt4292;
	public static int					anInt4294;
	public static IncomingOpcode		CHANGE_WALK_HERE_OPTION_TEXT	= new IncomingOpcode(9, -1);

	static {
		anInt4294 = 0;
	}

	public static final int method1670(byte i, int i_0_) {
		i_0_ = (~0x2aaaaaaa & i_0_ >>> 2099622177) + (i_0_ & 0x55555555);
		i_0_ = ((~0x33333331 & i_0_) >>> -1815143518) + (0x33333333 & i_0_);
		i_0_ = (i_0_ >>> -954051964) + i_0_ & 0xf0f0f0f;
		i_0_ += i_0_ >>> 810774152;
		if (i <= 88) {
			anInt4294 = -42;
		}
		i_0_ += i_0_ >>> -194578672;
		return i_0_ & 0xff;
	}

	public static final void method1671(int i, byte i_1_, int i_2_, int i_3_, int i_4_) {
		do {
			if (i_1_ != 0) {
				aClass171_4290 = null;
			}
			if (Class76_Sub8.anInt3778 > i || (i ^ 0xffffffff) < (Class3.anInt77 ^ 0xffffffff)) {
				break;
			}
			i_2_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_2_);
			i_3_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_3_);
			Class48_Sub2_Sub1.method471(i, (byte) 59, i_4_, i_2_, i_3_);
			break;
		} while (false);
	}

	public static final void method1672(byte i) {
		client.preferences.setPreference((byte) -13, 1, client.preferences.aClass64_Sub3_4041);
		client.preferences.setPreference((byte) -13, 1, client.preferences.aClass64_Sub3_4076);
		client.preferences.setPreference((byte) -13, 2, client.preferences.removeRoofs);
		client.preferences.setPreference((byte) -13, 2, client.preferences.removeRoofsMode);
		client.preferences.setPreference((byte) -13, 1, client.preferences.groundDecoration);
		client.preferences.setPreference((byte) -13, 1, client.preferences.groundBlending);
		client.preferences.setPreference((byte) -13, 1, client.preferences.idleAnimations);
		client.preferences.setPreference((byte) -13, 1, client.preferences.flickeringEffects);
		client.preferences.setPreference((byte) -13, 1, client.preferences.characterShadows);
		client.preferences.setPreference((byte) -13, 1, client.preferences.textures);
		client.preferences.setPreference((byte) -13, 1, client.preferences.sceneryShadows);
		client.preferences.setPreference((byte) -13, 1, client.preferences.lightningDetail);
		client.preferences.setPreference((byte) -13, 0, client.preferences.waterDetail);
		client.preferences.setPreference((byte) -13, 1, client.preferences.fog);
		client.preferences.setPreference((byte) -13, 0, client.preferences.antiAliasing);
		client.preferences.setPreference((byte) -13, 0, client.preferences.multiSample);
		client.preferences.setPreference((byte) -13, 1, client.preferences.particles);
		if (i == 19) {
			client.preferences.setPreference((byte) -13, 0, client.preferences.buildArea);
			client.preferences.setPreference((byte) -13, 0, client.preferences.unknown3);
			Class151_Sub1.method2450((byte) 101);
			client.preferences.setPreference((byte) -13, 1, client.preferences.maxScreenSize);
			client.preferences.setPreference((byte) -13, 3, client.preferences.graphicsLevel);
			Js5FileRequest.method1593((byte) 62);
			WhirlpoolGenerator.method3980((byte) 127);
			Class33.aBoolean316 = true;
		}
	}

	long aLong4288;

	public Class98_Sub50() {
		/* empty */
	}

	Class98_Sub50(long l) {
		aLong4288 = l;
	}
}

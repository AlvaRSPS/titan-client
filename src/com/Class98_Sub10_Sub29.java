/* Class98_Sub10_Sub29 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.quickchat.QuickChatMessage;
import com.jagex.game.client.ui.layout.HorizontalAlignment;

public final class Class98_Sub10_Sub29 extends Class98_Sub10 {
	public static final void method1090(int i, RtInterfaceClip var_aa, int i_14_, int i_15_, RtInterface interf, int i_16_, long l, int i_17_, int i_18_) {
		int i_19_ = i_14_ * i_14_ + i_15_ * i_15_;
		if (i_19_ <= l) {
			int i_20_ = Math.min(interf.renderWidth / 2, interf.renderHeight / 2);
			if ((i_19_ ^ 0xffffffff) >= (i_20_ * i_20_ ^ 0xffffffff)) {
				Class4.drawMapDot(i_16_, i_14_, i_17_, interf, var_aa, i_15_, (byte) -24, QuickChatMessage.aClass332Array6032[i]);
			} else {
				i_20_ -= 10;
				int i_21_;
				do {
					if (Class98_Sub46_Sub20_Sub2.cameraMode == 4) {
						i_21_ = 0x3fff & (int) RsFloatBuffer.aFloat5794;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					i_21_ = 0x3fff & Class204.anInt1553 + (int) RsFloatBuffer.aFloat5794;
				} while (false);
				int i_22_ = Class284_Sub2_Sub2.SINE[i_21_];
				int i_23_ = Class284_Sub2_Sub2.COSINE[i_21_];
				if (Class98_Sub46_Sub20_Sub2.cameraMode != 4) {
					i_22_ = i_22_ * 256 / (Class151.anInt1213 - -256);
					i_23_ = i_23_ * 256 / (256 + Class151.anInt1213);
				}
				int i_24_ = i_22_ * i_14_ - -(i_15_ * i_23_) >> -1569017266;
				int i_25_ = -(i_15_ * i_22_) + i_23_ * i_14_ >> 813721934;
				double d = Math.atan2(i_24_, i_25_);
				int i_26_ = (int) (Math.sin(d) * i_20_);
				int i_27_ = (int) (Math.cos(d) * i_20_);
				GameObjectDefinition.aClass332Array3000[i].method3730(i_17_ + interf.renderWidth / 2.0F + i_26_, -i_27_ + (i_16_ + interf.renderHeight / 2.0F), 4096, (int) (-d / 6.283185307179586 * 65535.0));
			}
		}
	}

	public static final boolean method1092(int i, int i_37_, byte i_38_) {
		if (i_38_ >= -64) {
			return true;
		}
		return (i & 0x20 ^ 0xffffffff) != -1;
	}

	public static final void sendPacket(boolean bool, OutgoingPacket frame) {
		Class336.aClass148_2827.addLast(frame, -20911);
		frame.anInt3869 = frame.packet.position;
		Class62.anInt490 += frame.anInt3869;
		frame.packet.position = 0;
	}

	private int	anInt5707	= 4;

	private int	anInt5708	= 4;

	public Class98_Sub10_Sub29() {
		super(1, false);
	}

	@Override
	public final int[] method990(int i, int i_30_) {
		int[] is = this.aClass16_3863.method237((byte) 98, i_30_);
		if (this.aClass16_3863.aBoolean198) {
			int i_31_ = Class25.anInt268 / anInt5707;
			int i_32_ = HorizontalAlignment.anInt492 / anInt5708;
			int[] is_33_;
			if (i_32_ > 0) {
				int i_34_ = i_30_ % i_32_;
				is_33_ = method1000(i_34_ * HorizontalAlignment.anInt492 / i_32_, 0, 0);
			} else {
				is_33_ = method1000(0, 0, i + -255);
			}
			for (int i_35_ = 0; Class25.anInt268 > i_35_; i_35_++) {
				if (i_31_ > 0) {
					int i_36_ = i_35_ % i_31_;
					is[i_35_] = is_33_[Class25.anInt268 * i_36_ / i_31_];
				} else {
					is[i_35_] = is_33_[0];
				}
			}
		}
		if (i != 255) {
			anInt5708 = 9;
		}
		return is;
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_28_) {
		do {
			int i_29_ = i;
			do {
				if (i_29_ != 0) {
					if ((i_29_ ^ 0xffffffff) != -2) {
						break;
					}
				} else {
					anInt5707 = class98_sub22.readUnsignedByte((byte) 9);
					break;
				}
				anInt5708 = class98_sub22.readUnsignedByte((byte) 37);
			} while (false);
			if (i_28_ <= -92) {
				break;
			}
			anInt5708 = -70;
			break;
		} while (false);
	}

	@Override
	public final int[][] method997(int i, int i_0_) {
		int[][] is = this.aClass223_3859.method2828(i_0_, 0);
		if (this.aClass223_3859.aBoolean1683) {
			int i_1_ = Class25.anInt268 / anInt5707;
			int i_2_ = HorizontalAlignment.anInt492 / anInt5708;
			int[][] is_3_;
			if (i_2_ <= 0) {
				is_3_ = method994(0, 24431, 0);
			} else {
				int i_4_ = i_0_ % i_2_;
				is_3_ = method994(HorizontalAlignment.anInt492 * i_4_ / i_2_, 24431, 0);
			}
			int[] is_5_ = is_3_[0];
			int[] is_6_ = is_3_[1];
			int[] is_7_ = is_3_[2];
			int[] is_8_ = is[0];
			int[] is_9_ = is[1];
			int[] is_10_ = is[2];
			for (int i_11_ = 0; Class25.anInt268 > i_11_; i_11_++) {
				int i_12_;
				if (i_1_ > 0) {
					int i_13_ = i_11_ % i_1_;
					i_12_ = i_13_ * Class25.anInt268 / i_1_;
				} else {
					i_12_ = 0;
				}
				is_8_[i_11_] = is_5_[i_12_];
				is_9_[i_11_] = is_6_[i_12_];
				is_10_[i_11_] = is_7_[i_12_];
			}
		}
		return is;
	}
}

/* Class156_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;

public final class NativeOpenGlElementArrayBuffer extends Class156 implements OpenGLElementBuffer {
	public static HashTable	aClass377_3277	= new HashTable(4);
	public static int		anInt3278;
	public static int[]		anIntArray3279	= new int[13];

	public static void method2497(byte i) {
		try {
			if (i >= -62) {
				anInt3278 = -42;
			}
			aClass377_3277 = null;
			anIntArray3279 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qs.E(" + i + ')');
		}
	}

	public static final int method2498(int i, byte i_3_, boolean bool) {
		try {
			if (bool) {
				return 0;
			}
			ClientInventory class98_sub3 = WaterDetailPreferenceField.get(i, bool, 6);
			if (class98_sub3 == null) {
				return Class98_Sub46_Sub14.inventoriesDefinitionList.method185(9, i).anInt6055;
			}
			int i_4_ = 0;
			for (int element : class98_sub3.anIntArray3824) {
				if (element == -1) {
					i_4_++;
				}
			}
			if (i_3_ <= 93) {
				return -9;
			}
			i_4_ += Class98_Sub46_Sub14.inventoriesDefinitionList.method185(9, i).anInt6055 - class98_sub3.anIntArray3824.length;
			return i_4_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qs.C(" + i + ',' + i_3_ + ',' + bool + ')');
		}
	}

	public static final int method2499(int i, int i_6_, int i_7_) {
		try {
			int i_8_ = Class242.method2934(11348, i_7_ + -1, -1 + i_6_) + Class242.method2934(11348, 1 + i_7_, -1 + i_6_) - (-Class242.method2934(11348, i_7_ - 1, 1 + i_6_) + -Class242.method2934(11348, i_7_ - -1, 1 + i_6_));
			int i_9_ = Class242.method2934(11348, i_7_ + -1, i_6_) - (-Class242.method2934(11348, 1 + i_7_, i_6_) + -Class242.method2934(11348, i_7_, -1 + i_6_) + -Class242.method2934(11348, i_7_, 1 + i_6_));
			int i_10_ = Class242.method2934(11348, i_7_, i_6_);
			return i_9_ / 8 + i_8_ / 16 - -(i_10_ / 4);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qs.D(" + i + ',' + i_6_ + ',' + i_7_ + ')');
		}
	}

	private int anInt3276;

	public NativeOpenGlElementArrayBuffer(OpenGlToolkit var_ha_Sub1, int i, byte[] is, int i_0_) {
		super(var_ha_Sub1, is, i_0_);
		try {
			anInt3276 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qs.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + (is != null ? "{...}" : "null") + ',' + i_0_ + ')');
		}
	}

	@Override
	public final int method19(int i) {
		try {
			if (i != -22132) {
				return 116;
			}
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qs.B(" + i + ')');
		}
	}

	@Override
	public final void write(byte i, byte[] is, int i_1_, int i_2_) {
		try {
			method2496(is, i_1_);
			if (i == -47) {
				anInt3276 = i_2_;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qs.H(" + i + ',' + (is != null ? "{...}" : "null") + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	@Override
	public final int method21(int i) {
		try {
			if (i != 5061) {
				anInt3278 = 93;
			}
			return anInt3276;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qs.A(" + i + ')');
		}
	}

	@Override
	public final long method22(int i) {
		try {
			if (i != 20260) {
				return -35L;
			}
			return ((Class156) this).aBuffer1247.getAddress();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qs.I(" + i + ')');
		}
	}
}

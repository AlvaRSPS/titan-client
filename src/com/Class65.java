/* Class65 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;

public final class Class65 {
	public static RtInterface[]		aClass293Array500;
	/* synthetic */ static Class	aClass504;
	public static IncomingOpcode	aClass58_499						= new IncomingOpcode(78, 4);
	public static int				anInt498;
	public static int				anInt502							= 0;
	public static int				currentPacketSize					= 0;
	public static int[]				DECODE_MASKS_PLAYERS_INDEXES_LIST	= new int[2048];
	public static int				timerBarDefaultId;
	public static char[]			unicodeUnescapes					= { '\u20ac', '\0', '\u201a', '\u0192', '\u201e', '\u2026', '\u2020', '\u2021', '\u02c6', '\u2030', '\u0160', '\u2039', '\u0152', '\0', '\u017d', '\0', '\0', '\u2018', '\u2019', '\u201c', '\u201d', '\u2022', '\u2013', '\u2014',
			'\u02dc', '\u2122', '\u0161', '\u203a', '\u0153', '\0', '\u017e', '\u0178' };

	public static final void method678(int i, boolean bool) {
		if (bool == false) {
			Class81.anInt624 = i;
			synchronized (StoreProgressMonitor.aClass79_3411) {
				StoreProgressMonitor.aClass79_3411.clear(76);
			}
			synchronized (Class211.aClass79_1594) {
				Class211.aClass79_1594.clear(19);
			}
		}
	}

	public static final boolean method679(int i, int i_0_, byte i_1_, int i_2_) {
		boolean bool = true;
		Interface19 interface19 = (Interface19) Class21_Sub1.method268(i_0_, i, i_2_);
		if (interface19 != null) {
			bool &= GameObjectDefinition.method2603((byte) 76, interface19);
		}
		interface19 = (Interface19) Class246_Sub3_Sub4.method931(i_0_, i, i_2_, aClass504 != null ? aClass504 : (aClass504 = method681("com.Interface19")));
		if (interface19 != null) {
			bool &= GameObjectDefinition.method2603((byte) 76, interface19);
		}
		interface19 = (Interface19) AsyncCache.method3177(i_0_, i, i_2_);
		if (interface19 != null) {
			bool &= GameObjectDefinition.method2603((byte) 76, interface19);
		}
		return bool;
	}

	/* synthetic */
	public static Class method681(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public Class65() {
		/* empty */
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

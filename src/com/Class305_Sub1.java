
/* Class305_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.DataInputStream;
import java.net.URL;

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5Index;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.GraphicsDefinition;
import com.jagex.game.client.definition.LightIntensityDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.parser.QuestDefinitionParser;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.ui.loading.impl.AwtLoadingScreen;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.impl.AnimatedLoadingBarLoadingScreenElement;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;

public final class Class305_Sub1 extends MapRegion {
	/* synthetic */static Class	aClass5304;
	public static int			anInt5303	= 0;

	public static final void method3587(int i, int i_94_, int i_95_) {
		try {
			if (i_94_ < -1) {
				Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_95_, -101, 7);
				class98_sub46_sub17.method1626((byte) -103);
				class98_sub46_sub17.anInt6054 = i;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ms.T(" + i + ',' + i_94_ + ',' + i_95_ + ')');
		}
	}

	/* synthetic */
	public static Class method3592(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public static final void reportError(Throwable throwable, int i, String message) {
		do {
			try {
				try {
					String safeErrorString = "";
					if (throwable != null) {
						safeErrorString = QuestDefinitionParser.generateDebugInfo(throwable, (byte) -24);
					}
					if (message != null) {
						if (throwable != null) {
							safeErrorString += " | ";
						}
						safeErrorString += message;
					}
					FloorOverlayDefinition.printError(safeErrorString, (byte) -80);
					safeErrorString = Class76_Sub9.replace("%3a", 4185, ":", safeErrorString);
					safeErrorString = Class76_Sub9.replace("%40", 4185, "@", safeErrorString);
					safeErrorString = Class76_Sub9.replace("%26", 4185, "&", safeErrorString);
					safeErrorString = Class76_Sub9.replace("%23", 4185, "#", safeErrorString);
					if (GameShell.gameApplet != null) {
						SignLinkRequest request = GameShell.errorSignLink.openUrlDataInputStream(-108, new URL(GameShell.gameApplet.getCodeBase(), "clienterror.ws?c=" + GameShell.revision + "&text_2=" + (Class256_Sub1.userName == null ? String.valueOf(Class106.aLong904) : Class256_Sub1.userName)
								+ "&v1=" + SignLink.java_vendor + "&v2=" + SignLink.javaVersion + "get_truncated_string_callcount=" + safeErrorString));
						while ((request.status ^ 0xffffffff) == -1) {
							TimeTools.sleep(0, 1L);
						}
						if (request.status != 1) {
							break;
						}
						DataInputStream datainputstream = (DataInputStream) request.result;
						datainputstream.read();
						datainputstream.close();
					}
				} catch (Exception exception) {
					/* empty */
				}
				break;
			} catch (RuntimeException runtimeException) {
				throw runtimeException;
			}
		} while (false);
	}

	int anInt5302 = 99;

	Class305_Sub1(int i, int i_0_, int i_1_, boolean bool) {
		super(i, i_0_, i_1_, bool, FloorOverlayDefinition.floorOverlayDefinitionList, Class82.floorUnderDefinitionList);
	}

	public final void method3582(int i, int i_2_, int i_3_, RSToolkit var_ha, RSByteBuffer groundDataBuffer, int[] is) {
		do {
			try {
				if (!this.underwater) {
					boolean bool = false;
					if (i != 17685) {
						anInt5302 = 114;
					}
					Class28 class28 = null;
					if (is != null) {
						is[0] = -1;
					}
					while (groundDataBuffer.payload.length > groundDataBuffer.position) {
						int i_4_ = groundDataBuffer.readUnsignedByte((byte) -125);
						if ((i_4_ ^ 0xffffffff) != -1) {
							if ((i_4_ ^ 0xffffffff) != -2) {
								if (i_4_ == 2) {
									if (class28 == null) {
										class28 = new Class28();
									}
									class28.method297(-50, groundDataBuffer);
								} else if (i_4_ != 128) {
									if ((i_4_ ^ 0xffffffff) == -130) {
										if (this.aByteArrayArrayArray2554 == null) {
											this.aByteArrayArrayArray2554 = new byte[4][][];
										}
										bool = true;
										for (int i_5_ = 0; i_5_ < 4; i_5_++) {
											byte i_6_ = groundDataBuffer.readSignedByte((byte) -19);
											if ((i_6_ ^ 0xffffffff) != -1 || this.aByteArrayArrayArray2554[i_5_] == null) {
												if ((i_6_ ^ 0xffffffff) == -2) {
													if (this.aByteArrayArrayArray2554[i_5_] == null) {
														((MapRegion) this).aByteArrayArrayArray2554[i_5_] = new byte[this.sizeX - -1][1 + this.sizeZ];
													}
													for (int i_7_ = 0; (i_7_ ^ 0xffffffff) > -65; i_7_ += 4) {
														for (int i_8_ = 0; (i_8_ ^ 0xffffffff) > -65; i_8_ += 4) {
															byte i_9_ = groundDataBuffer.readSignedByte((byte) -19);
															for (int i_10_ = i_7_ + i_3_; (i_10_ ^ 0xffffffff) > (i_7_ - (-i_3_ + -4) ^ 0xffffffff); i_10_++) {
																for (int i_11_ = i_8_ - -i_2_; i_11_ < i_2_ + i_8_ + 4; i_11_++) {
																	if (i_10_ >= 0 && (i_10_ ^ 0xffffffff) > (this.sizeX ^ 0xffffffff) && (i_11_ ^ 0xffffffff) <= -1 && (i_11_ ^ 0xffffffff) > (this.sizeZ ^ 0xffffffff)) {
																		((MapRegion) this).aByteArrayArrayArray2554[i_5_][i_10_][i_11_] = i_9_;
																	}
																}
															}
														}
													}
												} else if ((i_6_ ^ 0xffffffff) == -3) {
													if (this.aByteArrayArrayArray2554[i_5_] == null) {
														((MapRegion) this).aByteArrayArrayArray2554[i_5_] = new byte[1 + this.sizeX][1 + this.sizeZ];
													}
													if ((i_5_ ^ 0xffffffff) < -1) {
														int i_12_ = i_3_;
														int i_13_ = i_3_ - -64;
														int i_14_ = i_2_;
														if ((i_14_ ^ 0xffffffff) > -1) {
															i_14_ = 0;
														} else if ((i_14_ ^ 0xffffffff) <= (this.sizeZ ^ 0xffffffff)) {
															i_14_ = this.sizeZ;
														}
														if (i_13_ >= 0) {
															if ((this.sizeX ^ 0xffffffff) >= (i_13_ ^ 0xffffffff)) {
																i_13_ = this.sizeX;
															}
														} else {
															i_13_ = 0;
														}
														int i_15_ = i_2_ - -64;
														if ((i_12_ ^ 0xffffffff) > -1) {
															i_12_ = 0;
														} else if ((i_12_ ^ 0xffffffff) <= (this.sizeX ^ 0xffffffff)) {
															i_12_ = this.sizeX;
														}
														if (i_15_ >= 0) {
															if ((i_15_ ^ 0xffffffff) <= (this.sizeZ ^ 0xffffffff)) {
																i_15_ = this.sizeZ;
															}
														} else {
															i_15_ = 0;
														}
														for (/**/; (i_12_ ^ 0xffffffff) > (i_13_ ^ 0xffffffff); i_12_++) {
															for (/**/; (i_15_ ^ 0xffffffff) < (i_14_ ^ 0xffffffff); i_14_++) {
																((MapRegion) this).aByteArrayArrayArray2554[i_5_][i_12_][i_14_] = this.aByteArrayArrayArray2554[i_5_ + -1][i_12_][i_14_];
															}
														}
													}
												}
											} else {
												int i_16_ = i_3_;
												int i_17_ = 64 + i_3_;
												int i_18_ = i_2_;
												if ((i_16_ ^ 0xffffffff) <= -1) {
													if ((this.sizeX ^ 0xffffffff) >= (i_16_ ^ 0xffffffff)) {
														i_16_ = this.sizeX;
													}
												} else {
													i_16_ = 0;
												}
												if ((i_18_ ^ 0xffffffff) <= -1) {
													if ((this.sizeZ ^ 0xffffffff) >= (i_18_ ^ 0xffffffff)) {
														i_18_ = this.sizeZ;
													}
												} else {
													i_18_ = 0;
												}
												int i_19_ = 64 + i_2_;
												if (i_17_ >= 0) {
													if ((this.sizeX ^ 0xffffffff) >= (i_17_ ^ 0xffffffff)) {
														i_17_ = this.sizeX;
													}
												} else {
													i_17_ = 0;
												}
												if (i_19_ >= 0) {
													if (i_19_ >= this.sizeZ) {
														i_19_ = this.sizeZ;
													}
												} else {
													i_19_ = 0;
												}
												for (/**/; i_17_ > i_16_; i_16_++) {
													for (/**/; (i_19_ ^ 0xffffffff) < (i_18_ ^ 0xffffffff); i_18_++) {
														((MapRegion) this).aByteArrayArrayArray2554[i_5_][i_16_][i_18_] = (byte) 0;
													}
												}
											}
										}
									} else {
										throw new IllegalStateException("");
									}
								} else if (is != null) {
									is[0] = groundDataBuffer.readShort((byte) 127);
									is[1] = groundDataBuffer.readUShort(false);
									is[2] = groundDataBuffer.readUShort(false);
									is[3] = groundDataBuffer.readUShort(false);
									is[4] = groundDataBuffer.readShort((byte) 127);
								} else {
									groundDataBuffer.position += 10;
								}
							} else {
								int i_20_ = groundDataBuffer.readUnsignedByte((byte) -103);
								if (i_20_ > 0) {
									for (int i_21_ = 0; i_20_ > i_21_; i_21_++) {
										Class1 class1 = new Class1(var_ha, groundDataBuffer, 2);
										if ((class1.anInt62 ^ 0xffffffff) == -32) {
											LightIntensityDefinition class379 = Class21_Sub1.lightIntensityDefinitionList.method3268(i ^ ~0x4531, groundDataBuffer.readShort((byte) 127));
											class1.method166(class379.anInt3197, class379.anInt3194, class379.anInt3193, (byte) -103, class379.anInt3195);
										}
										if ((var_ha.method1822() ^ 0xffffffff) < -1) {
											PointLight class98_sub5 = class1.light;
											int i_22_ = (i_3_ << -384073015) + class98_sub5.getX(7019);
											int i_23_ = class98_sub5.getZ(28699) - -(i_2_ << -1220460727);
											int i_24_ = i_22_ >> 1950973833;
											int i_25_ = i_23_ >> -130868727;
											if ((i_24_ ^ 0xffffffff) <= -1 && (i_25_ ^ 0xffffffff) <= -1 && (i_24_ ^ 0xffffffff) > (this.sizeX ^ 0xffffffff) && (i_25_ ^ 0xffffffff) > (this.sizeZ ^ 0xffffffff)) {
												class98_sub5.reposition(i_23_, (byte) -123, i_22_, this.heightMap[class1.anInt57][i_24_][i_25_] + -class98_sub5.getY((byte) 96));
												Class1.method619(class1);
											}
										}
									}
								}
							}
						} else {
							class28 = new Class28(groundDataBuffer);
						}
					}
					if (class28 != null) {
						for (int i_26_ = 0; i_26_ < 8; i_26_++) {
							for (int i_27_ = 0; i_27_ < 8; i_27_++) {
								int xChunk = (i_3_ >> -436409085) - -i_26_;
								int yChunk = (i_2_ >> -1019194845) - -i_27_;
								// System.out.println("i_2_ >> -1019194845: " +
								// (i_2_));
								if ((xChunk ^ 0xffffffff) <= -1 && this.sizeX >> -182199197 > xChunk && (yChunk ^ 0xffffffff) <= -1 && yChunk < this.sizeZ >> -262306781) {
									Class246_Sub3_Sub3.method3015(yChunk, xChunk, (byte) 71, class28);
								}
							}
						}
					}
					if (bool || this.aByteArrayArrayArray2554 == null) {
						break;
					}
					for (int i_30_ = 0; (i_30_ ^ 0xffffffff) > -5; i_30_++) {
						if (this.aByteArrayArrayArray2554[i_30_] != null) {
							for (int i_31_ = 0; i_31_ < 16; i_31_++) {
								for (int i_32_ = 0; i_32_ < 16; i_32_++) {
									int i_33_ = (i_3_ >> 1076603618) + i_31_;
									int i_34_ = (i_2_ >> 2130564034) - -i_32_;
									if ((i_33_ ^ 0xffffffff) <= -1 && i_33_ < 26 && i_34_ >= 0 && (i_34_ ^ 0xffffffff) > -27) {
										((MapRegion) this).aByteArrayArrayArray2554[i_30_][i_33_][i_34_] = (byte) 0;
									}
								}
							}
						}
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ms.R(" + i + ',' + i_2_ + ',' + i_3_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + (groundDataBuffer != null ? "{...}" : "null") + ',' + (is != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final Interface19 method3583(int i, int i_35_, int i_36_, int i_37_, int i_38_) {
		try {
			Interface19 interface19 = null;
			if ((i_35_ ^ 0xffffffff) == i_37_) {
				interface19 = (Interface19) Class21_Sub1.method268(i_36_, i_38_, i);
			}
			if ((i_35_ ^ 0xffffffff) == -2) {
				interface19 = (Interface19) GrandExchangeOffer.method1701(i_36_, i_38_, i);
			}
			if (i_35_ == 2) {
				interface19 = (Interface19) Class246_Sub3_Sub4.method931(i_36_, i_38_, i, aClass5304 != null ? aClass5304 : (aClass5304 = method3592("com.Interface19")));
			}
			if ((i_35_ ^ 0xffffffff) == -4) {
				interface19 = (Interface19) AsyncCache.method3177(i_36_, i_38_, i);
			}
			return interface19;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ms.S(" + i + ',' + i_35_ + ',' + i_36_ + ',' + i_37_ + ',' + i_38_ + ')');
		}
	}

	public final void method3584(ClipMap[] class243s, int i, int i_39_, byte[] is, int i_40_, int i_41_, int i_42_, int i_43_, int i_44_, RSToolkit var_ha, int i_45_) {// important
		try {
			RSByteBuffer class98_sub22 = new RSByteBuffer(is);
			int i_47_ = -1;
			for (;;) {
				int i_48_ = class98_sub22.method1208(3893);
				if ((i_48_ ^ 0xffffffff) == -1) {
					break;
				}
				i_47_ += i_48_;
				int i_49_ = 0;
				for (;;) {
					int i_50_ = class98_sub22.readSmart(1689622712);
					if (i_50_ == 0) {
						break;
					}
					i_49_ += i_50_ - 1;
					int i_51_ = i_49_ & 0x3f;
					int i_52_ = (i_49_ & 0xfe7) >> -456824154;
					int i_53_ = i_49_ >> 1908291884;
					int i_54_ = class98_sub22.readUnsignedByte((byte) -127);
					int i_55_ = i_54_ >> 1689510498;
					int i_56_ = i_54_ & 0x3;
					if ((i_53_ ^ 0xffffffff) == (i_39_ ^ 0xffffffff) && i_52_ >= i_42_ && (i_52_ ^ 0xffffffff) > (i_42_ - -8 ^ 0xffffffff) && (i_51_ ^ 0xffffffff) <= (i_43_ ^ 0xffffffff) && i_51_ < i_43_ + 8) {
						GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(i_47_, (byte) 119);
						int i_57_ = AwtLoadingScreen.method3451(i_52_ & 0x7, i_56_, false, i_51_ & 0x7, i_44_, class352.sizeY, class352.sizeX) + i_41_;
						int i_58_ = Class35.method338(class352.sizeX, i_44_, i_56_, 0x7 & i_52_, 0x7 & i_51_, class352.sizeY, (byte) -23) + i_40_;
						if (i_57_ > 0 && (i_58_ ^ 0xffffffff) < -1 && (-1 + this.sizeX ^ 0xffffffff) < (i_57_ ^ 0xffffffff) && i_58_ < this.sizeZ - 1) {
							ClipMap class243 = null;
							if (!this.underwater) {
								int i_59_ = i;
								if ((Class281.flags[1][i_57_][i_58_] & 0x2) == 2) {
									i_59_--;
								}
								if (i_59_ >= 0) {
									class243 = class243s[i_59_];
								}
							}
							method3588(i_47_, i_58_, -1, i, false, i_57_, var_ha, 0x3 & i_44_ + i_56_, i_55_, i, class243);
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ms.P(" + (class243s != null ? "{...}" : "null") + ',' + i + ',' + i_39_ + ',' + (is != null ? "{...}" : "null") + ',' + i_40_ + ',' + i_41_ + ',' + i_42_ + ',' + i_43_ + ',' + i_44_ + ',' + (var_ha != null ? "{...}" : "null") + ','
					+ i_45_ + ')');
		}
	}

	public final void method3586(int i, int i_61_, int[] is, int i_62_, int i_63_, RSByteBuffer mapBuffer, RSToolkit var_ha, int i_64_, boolean bool, int i_65_, int i_66_) {
		do {
			try {
				if (!this.underwater) {
					if (bool != true) {
						anInt5302 = -44;
					}
					boolean bool_67_ = false;
					Class28 class28 = null;
					if (is != null) {
						is[0] = -1;
					}
					int i_68_ = (0x7 & i) * 8;
					int i_69_ = 8 * (i_61_ & 0x7);
					while (mapBuffer.position < mapBuffer.payload.length) {
						int i_70_ = mapBuffer.readUnsignedByte((byte) -116);
						if (i_70_ == 0) {
							class28 = new Class28(mapBuffer);
						} else if ((i_70_ ^ 0xffffffff) == -2) {
							int i_71_ = mapBuffer.readUnsignedByte((byte) -105);
							if ((i_71_ ^ 0xffffffff) < -1) {
								for (int i_72_ = 0; i_72_ < i_71_; i_72_++) {
									Class1 class1 = new Class1(var_ha, mapBuffer, 2);
									if (class1.anInt62 == 31) {
										LightIntensityDefinition class379 = Class21_Sub1.lightIntensityDefinitionList.method3268(-37, mapBuffer.readShort((byte) 127));
										class1.method166(class379.anInt3197, class379.anInt3194, class379.anInt3193, (byte) -98, class379.anInt3195);
									}
									if (var_ha.method1822() > 0) {
										PointLight class98_sub5 = class1.light;
										int i_73_ = class98_sub5.getX(7019) >> 945832681;
										int i_74_ = class98_sub5.getZ(28699) >> 341917193;
										if (class1.anInt57 == i_65_ && (i_73_ ^ 0xffffffff) <= (i_68_ ^ 0xffffffff) && i_73_ < 8 + i_68_ && (i_74_ ^ 0xffffffff) <= (i_69_ ^ 0xffffffff) && i_69_ + 8 > i_74_) {
											int i_75_ = (i_62_ << -854995383) + GraphicsBuffer.method1433(0xfff & class98_sub5.getZ(28699), i_64_, class98_sub5.getX(7019) & 0xfff, -7175);
											i_73_ = i_75_ >> -1912547319;
											int i_76_ = (i_63_ << 1462769929) - -GraphicsDefinition.method1724(7, i_64_, 0xfff & class98_sub5.getZ(28699), 0xfff & class98_sub5.getX(7019));
											i_74_ = i_76_ >> 1721073289;
											if ((i_73_ ^ 0xffffffff) <= -1 && (i_74_ ^ 0xffffffff) <= -1 && (this.sizeX ^ 0xffffffff) < (i_73_ ^ 0xffffffff) && this.sizeZ > i_74_) {
												class98_sub5.reposition(i_76_, (byte) -122, i_75_, this.heightMap[i_65_][i_73_][i_74_] - class98_sub5.getY((byte) 72));
												Class1.method619(class1);
											}
										}
									}
								}
							}
						} else if ((i_70_ ^ 0xffffffff) != -3) {
							if (i_70_ == 128) {
								if (is != null) {
									is[0] = mapBuffer.readShort((byte) 127);
									is[1] = mapBuffer.readUShort(!bool);
									is[2] = mapBuffer.readUShort(false);
									is[3] = mapBuffer.readUShort(false);
									is[4] = mapBuffer.readShort((byte) 127);
								} else {
									mapBuffer.position += 10;
								}
							} else if ((i_70_ ^ 0xffffffff) == -130) {
								if (this.aByteArrayArrayArray2554 == null) {
									this.aByteArrayArrayArray2554 = new byte[4][][];
								}
								for (int i_77_ = 0; (i_77_ ^ 0xffffffff) > -5; i_77_++) {
									byte i_78_ = mapBuffer.readSignedByte((byte) -19);
									if ((i_78_ ^ 0xffffffff) != -1 || this.aByteArrayArrayArray2554[i_66_] == null) {
										if (i_78_ == 1) {
											if (this.aByteArrayArrayArray2554[i_66_] == null) {
												((MapRegion) this).aByteArrayArrayArray2554[i_66_] = new byte[1 + this.sizeX][1 + this.sizeZ];
											}
											for (int i_79_ = 0; (i_79_ ^ 0xffffffff) > -65; i_79_ += 4) {
												for (int i_80_ = 0; (i_80_ ^ 0xffffffff) > -65; i_80_ += 4) {
													byte i_81_ = mapBuffer.readSignedByte((byte) -19);
													if ((i_65_ ^ 0xffffffff) <= (i_77_ ^ 0xffffffff)) {
														for (int i_82_ = i_79_; (i_82_ ^ 0xffffffff) > (i_79_ - -4 ^ 0xffffffff); i_82_++) {
															for (int i_83_ = i_80_; i_83_ < i_80_ + 4; i_83_++) {
																if ((i_68_ ^ 0xffffffff) >= (i_82_ ^ 0xffffffff) && (8 + i_68_ ^ 0xffffffff) < (i_82_ ^ 0xffffffff) && i_69_ <= i_83_ && (i_69_ ^ 0xffffffff) > (i_69_ + 8 ^ 0xffffffff)) {
																	int i_84_ = i_62_ - -GraphicsDefinition.method1720(0x7 & i_82_, 0, 0x7 & i_83_, i_64_);
																	int i_85_ = Class250.method3166(i_82_ & 0x7, i_83_ & 0x7, i_64_, (byte) -122) + i_63_;
																	if (i_84_ >= 0 && (this.sizeX ^ 0xffffffff) < (i_84_ ^ 0xffffffff) && (i_85_ ^ 0xffffffff) <= -1 && (this.sizeZ ^ 0xffffffff) < (i_85_ ^ 0xffffffff)) {
																		((MapRegion) this).aByteArrayArrayArray2554[i_66_][i_84_][i_85_] = i_81_;
																	}
																}
															}
														}
													}
												}
											}
										}
									} else if (i_77_ <= i_65_) {
										int i_86_ = i_62_;
										int i_87_ = i_62_ + 7;
										int i_88_ = i_63_;
										if (i_88_ < 0) {
											i_88_ = 0;
										} else if (i_88_ >= this.sizeZ) {
											i_88_ = this.sizeZ;
										}
										int i_89_ = 7 + i_63_;
										if (i_86_ < 0) {
											i_86_ = 0;
										} else if (i_86_ >= this.sizeX) {
											i_86_ = this.sizeX;
										}
										if (i_87_ >= 0) {
											if ((this.sizeX ^ 0xffffffff) >= (i_87_ ^ 0xffffffff)) {
												i_87_ = this.sizeX;
											}
										} else {
											i_87_ = 0;
										}
										if (i_89_ >= 0) {
											if (i_89_ >= this.sizeZ) {
												i_89_ = this.sizeZ;
											}
										} else {
											i_89_ = 0;
										}
										for (/**/; (i_87_ ^ 0xffffffff) < (i_86_ ^ 0xffffffff); i_86_++) {
											for (/**/; (i_88_ ^ 0xffffffff) > (i_89_ ^ 0xffffffff); i_88_++) {
												((MapRegion) this).aByteArrayArrayArray2554[i_66_][i_86_][i_88_] = (byte) 0;
											}
										}
									}
								}
							} else {
								throw new IllegalStateException("");
							}
						} else {
							if (class28 == null) {
								class28 = new Class28();
							}
							class28.method297(-50, mapBuffer);
						}
					}
					if (class28 != null) {
						Class246_Sub3_Sub3.method3015(i_63_ >> -243448253, i_62_ >> -466562909, (byte) 31, class28);
					}
					if (bool_67_ || this.aByteArrayArrayArray2554 == null || this.aByteArrayArrayArray2554[i_66_] == null) {
						break;
					}
					int i_90_ = 7 + i_62_;
					int i_91_ = i_63_ - -7;
					for (int i_92_ = i_62_; (i_92_ ^ 0xffffffff) > (i_90_ ^ 0xffffffff); i_92_++) {
						for (int i_93_ = i_63_; (i_91_ ^ 0xffffffff) < (i_93_ ^ 0xffffffff); i_93_++) {
							((MapRegion) this).aByteArrayArrayArray2554[i_66_][i_92_][i_93_] = (byte) 0;
						}
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ms.Q(" + i + ',' + i_61_ + ',' + (is != null ? "{...}" : "null") + ',' + i_62_ + ',' + i_63_ + ',' + (mapBuffer != null ? "{...}" : "null") + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_64_ + ',' + bool + ',' + i_65_
						+ ',' + i_66_ + ')');
			}
			break;
		} while (false);
	}

	public final void method3588(int i, int i_96_, int i_97_, int i_98_, boolean bool, int i_99_, RSToolkit var_ha, int i_100_, int i_101_, int i_102_, ClipMap class243) {// IMPORTANT
		try {
			if (client.preferences.aClass64_Sub3_4076.getValue((byte) 120) != 0 || RenderAnimDefinition.method3477(i_96_, i_98_, i_99_, SunDefinitionParser.anInt963, 55)) {
				if ((anInt5302 ^ 0xffffffff) < (i_102_ ^ 0xffffffff)) {
					anInt5302 = i_102_;
				}
				GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(i, (byte) 119);
				if (client.preferences.textures.getValue((byte) 121) != 0 || !class352.aBoolean2982) {
					int i_103_;
					int i_104_;
					if ((i_100_ ^ 0xffffffff) == -2 || (i_100_ ^ 0xffffffff) == -4) {
						i_103_ = class352.sizeX;
						i_104_ = class352.sizeY;
					} else {
						i_103_ = class352.sizeY;
						i_104_ = class352.sizeX;
					}
					int i_105_;
					int i_106_;
					if ((i_99_ - -i_103_ ^ 0xffffffff) < (this.sizeX ^ 0xffffffff)) {
						i_106_ = i_99_ - -1;
						i_105_ = i_99_;
					} else {
						i_105_ = (i_103_ >> 1955640673) + i_99_;
						i_106_ = i_99_ + (i_103_ - -1 >> 1935606721);
					}
					int i_107_;
					int i_108_;
					if ((this.sizeZ ^ 0xffffffff) <= (i_96_ - -i_104_ ^ 0xffffffff)) {
						i_108_ = (i_104_ >> -1353300511) + i_96_;
						i_107_ = i_96_ - -(i_104_ + 1 >> -730048575);
					} else {
						i_107_ = 1 + i_96_;
						i_108_ = i_96_;
					}
					Ground var_s = Class78.aSArray594[i_98_];
					int i_109_ = var_s.getTileHeight(i_108_, -12639, i_105_) + var_s.getTileHeight(i_108_, -12639, i_106_) + var_s.getTileHeight(i_107_, -12639, i_105_) - -var_s.getTileHeight(i_107_, -12639, i_106_) >> -60292670;
					int i_110_ = (i_103_ << -882314456) + (i_99_ << 1219490217);
					if (bool != false) {
						method3591(null, 36, (byte) -79, null, null, 13);
					}
					int i_111_ = (i_96_ << -1157872503) + (i_104_ << -1315771672);
					boolean bool_112_ = Class98_Sub9.aBoolean3851 && !this.underwater && class352.aBoolean3007;
					if (class352.method3858(103)) {
						Class98_Sub31_Sub4.method1383(null, class352, i_99_, i_100_, 3, i_96_, i_102_, null);
					}
					boolean bool_113_ = (i_97_ ^ 0xffffffff) == 0 && (class352.anInt2941 ^ 0xffffffff) == 0 && class352.anIntArray2979 == null && class352.transformIDs == null && !class352.aBoolean3005 && !class352.aBoolean2984;
					if (!Class99.aBoolean838 || (!Class360.method3909(-4, i_101_) || class352.anInt2956 == 1) && (!Entity.method3100(i_101_, (byte) 85) || class352.anInt2956 != 0)) {
						if ((i_101_ ^ 0xffffffff) == -23) {
							if (client.preferences.groundDecoration.getValue((byte) 121) != 0 || class352.anInt2998 != 0 || (class352.actionCount ^ 0xffffffff) == -2 || class352.aBoolean2969) {
								Class246_Sub3_Sub1 class246_sub3_sub1;
								if (bool_113_) {
									Class246_Sub3_Sub1_Sub1 class246_sub3_sub1_sub1 = new Class246_Sub3_Sub1_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_100_, bool_112_);
									class246_sub3_sub1 = class246_sub3_sub1_sub1;
									if (class246_sub3_sub1_sub1.method65(!bool)) {
										class246_sub3_sub1_sub1.method62(var_ha, 24447);
									}
								} else {
									class246_sub3_sub1 = new Class246_Sub3_Sub1_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_100_, i_97_);
								}
								OpenGlElementBufferPointer.method3671(i_102_, i_99_, i_96_, class246_sub3_sub1);
								if ((class352.actionCount ^ 0xffffffff) == -2 && class243 != null) {
									class243.method2946(i_99_, (byte) -14, i_96_);
								}
							}
						} else if (i_101_ == 10 || i_101_ == 11) {
							Class246_Sub3_Sub4_Sub1 class246_sub3_sub4_sub1 = null;
							Class246_Sub3_Sub4 class246_sub3_sub4;
							int i_114_;
							if (bool_113_) {
								Class246_Sub3_Sub4_Sub1 class246_sub3_sub4_sub1_115_ = new Class246_Sub3_Sub4_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_99_, -1 + i_103_ + i_99_, i_96_, -1 + i_104_ + i_96_, i_101_, i_100_, bool_112_);
								i_114_ = class246_sub3_sub4_sub1_115_.method3027(94);
								class246_sub3_sub4 = class246_sub3_sub4_sub1_115_;
								class246_sub3_sub4_sub1 = class246_sub3_sub4_sub1_115_;
							} else {
								class246_sub3_sub4 = new Class246_Sub3_Sub4_Sub5(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_99_, -1 + i_99_ - -i_103_, i_96_, -1 + i_104_ + i_96_, i_101_, i_100_, i_97_);
								i_114_ = 15;
							}
							if (LoginOpcode.method2826(class246_sub3_sub4, false)) {
								if (class246_sub3_sub4_sub1 != null && class246_sub3_sub4_sub1.method65(true)) {
									class246_sub3_sub4_sub1.method62(var_ha, 24447);
								}
								if (class352.aBoolean2947 && Class98_Sub9.aBoolean3851) {
									if ((i_114_ ^ 0xffffffff) < -31) {
										i_114_ = 30;
									}
									for (int i_116_ = 0; (i_116_ ^ 0xffffffff) >= (i_103_ ^ 0xffffffff); i_116_++) {
										for (int i_117_ = 0; i_117_ <= i_104_; i_117_++) {
											var_s.setShadowing(i_99_ + i_116_, i_96_ + i_117_, i_114_);
										}
									}
								}
							}
							if (class352.actionCount != 0 && class243 != null) {
								class243.method2949(0, !class352.clippingFlag, i_99_, i_104_, i_103_, class352.walkable, i_96_);
							}
						} else if (i_101_ >= 12 && i_101_ <= 17 || i_101_ >= 18 && (i_101_ ^ 0xffffffff) >= -22) {
							Class246_Sub3_Sub4 class246_sub3_sub4;
							if (!bool_113_) {
								class246_sub3_sub4 = new Class246_Sub3_Sub4_Sub5(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_99_, i_103_ + i_99_ + -1, i_96_, i_104_ + i_96_ + -1, i_101_, i_100_, i_97_);
							} else {
								Class246_Sub3_Sub4_Sub1 class246_sub3_sub4_sub1 = new Class246_Sub3_Sub4_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_99_, -1 + i_99_ + i_103_, i_96_, i_96_ - (-i_104_ - -1), i_101_, i_100_, bool_112_);
								if (class246_sub3_sub4_sub1.method65(!bool)) {
									class246_sub3_sub4_sub1.method62(var_ha, 24447);
								}
								class246_sub3_sub4 = class246_sub3_sub4_sub1;
							}
							LoginOpcode.method2826(class246_sub3_sub4, false);
							if (Class98_Sub9.aBoolean3851 && !this.underwater && i_101_ >= 12 && i_101_ <= 17 && (i_101_ ^ 0xffffffff) != -14 && i_102_ > 0 && (class352.anInt2956 ^ 0xffffffff) != -1) {
								((MapRegion) this).aByteArrayArrayArray2550[i_102_][i_99_][i_96_] = (byte) Class41.or(this.aByteArrayArrayArray2550[i_102_][i_99_][i_96_], 4);
							}
							if (class352.actionCount != 0 && class243 != null) {
								class243.method2949(0, !class352.clippingFlag, i_99_, i_104_, i_103_, class352.walkable, i_96_);
							}
						} else if (i_101_ == 0) {
							int i_118_ = class352.anInt2956;
							if (Class98_Sub42.aBoolean4218 && class352.anInt2956 == -1) {
								i_118_ = 1;
							}
							Class246_Sub3_Sub3 class246_sub3_sub3;
							if (!bool_113_) {
								class246_sub3_sub3 = new Class246_Sub3_Sub3_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_100_, i_97_);
							} else {
								Class246_Sub3_Sub3_Sub2 class246_sub3_sub3_sub2 = new Class246_Sub3_Sub3_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_100_, bool_112_);
								class246_sub3_sub3 = class246_sub3_sub3_sub2;
								if (class246_sub3_sub3_sub2.method65(true)) {
									class246_sub3_sub3_sub2.method62(var_ha, 24447);
								}
							}
							Class78.method790(i_102_, i_99_, i_96_, class246_sub3_sub3, null);
							if ((i_100_ ^ 0xffffffff) == -1) {
								if (Class98_Sub9.aBoolean3851 && class352.aBoolean2947) {
									var_s.setShadowing(i_99_, i_96_, 50);
									var_s.setShadowing(i_99_, 1 + i_96_, 50);
								}
								if (i_118_ == 1 && !this.underwater) {
									VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_, class352.anInt2953, class352.anInt2986, 1, 8);
								}
							} else if (i_100_ != 1) {
								if (i_100_ != 2) {
									if (i_100_ == 3) {
										if (Class98_Sub9.aBoolean3851 && class352.aBoolean2947) {
											var_s.setShadowing(i_99_, i_96_, 50);
											var_s.setShadowing(i_99_ + 1, i_96_, 50);
										}
										if ((i_118_ ^ 0xffffffff) == -2 && !this.underwater) {
											VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_, class352.anInt2953, class352.anInt2986, 2, 8);
										}
									}
								} else {
									if (Class98_Sub9.aBoolean3851 && class352.aBoolean2947) {
										var_s.setShadowing(i_99_ + 1, i_96_, 50);
										var_s.setShadowing(1 + i_99_, i_96_ - -1, 50);
									}
									if ((i_118_ ^ 0xffffffff) == -2 && !this.underwater) {
										VarClientStringsDefinitionParser.method2921(i_99_ + 1, i_102_, i_96_, -class352.anInt2953, class352.anInt2986, 1, 8);
									}
								}
							} else {
								if (Class98_Sub9.aBoolean3851 && class352.aBoolean2947) {
									var_s.setShadowing(i_99_, i_96_ - -1, 50);
									var_s.setShadowing(i_99_ + 1, i_96_ - -1, 50);
								}
								if (i_118_ == 1 && !this.underwater) {
									VarClientStringsDefinitionParser.method2921(i_99_, i_102_, 1 + i_96_, -class352.anInt2953, class352.anInt2986, 2, 8);
								}
							}
							if (class352.actionCount != 0 && class243 != null) {
								class243.method2945(i_96_, class352.walkable, !class352.clippingFlag, i_100_, i_99_, i_101_, (byte) 122);
							}
							if ((class352.anInt2966 ^ 0xffffffff) != -65) {
								Class246_Sub3_Sub5.method1439(i_102_, i_99_, i_96_, class352.anInt2966);
							}
						} else if (i_101_ == 1) {
							Class246_Sub3_Sub3 class246_sub3_sub3;
							if (!bool_113_) {
								class246_sub3_sub3 = new Class246_Sub3_Sub3_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_100_, i_97_);
							} else {
								Class246_Sub3_Sub3_Sub2 class246_sub3_sub3_sub2 = new Class246_Sub3_Sub3_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_100_, bool_112_);
								if (class246_sub3_sub3_sub2.method65(!bool)) {
									class246_sub3_sub3_sub2.method62(var_ha, 24447);
								}
								class246_sub3_sub3 = class246_sub3_sub3_sub2;
							}
							Class78.method790(i_102_, i_99_, i_96_, class246_sub3_sub3, null);
							if (class352.aBoolean2947 && Class98_Sub9.aBoolean3851) {
								if ((i_100_ ^ 0xffffffff) == -1) {
									var_s.setShadowing(i_99_, 1 + i_96_, 50);
								} else if ((i_100_ ^ 0xffffffff) != -2) {
									if ((i_100_ ^ 0xffffffff) == -3) {
										var_s.setShadowing(i_99_ + 1, i_96_, 50);
									} else if ((i_100_ ^ 0xffffffff) == -4) {
										var_s.setShadowing(i_99_, i_96_, 50);
									}
								} else {
									var_s.setShadowing(1 + i_99_, i_96_ - -1, 50);
								}
							}
							if ((class352.actionCount ^ 0xffffffff) != -1 && class243 != null) {
								class243.method2945(i_96_, class352.walkable, !class352.clippingFlag, i_100_, i_99_, i_101_, (byte) 104);
							}
						} else if ((i_101_ ^ 0xffffffff) == -3) {
							int i_119_ = 0x3 & 1 + i_100_;
							Class246_Sub3_Sub3 class246_sub3_sub3;
							Class246_Sub3_Sub3 class246_sub3_sub3_120_;
							if (bool_113_) {
								Class246_Sub3_Sub3_Sub2 class246_sub3_sub3_sub2 = new Class246_Sub3_Sub3_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, 4 + i_100_, bool_112_);
								Class246_Sub3_Sub3_Sub2 class246_sub3_sub3_sub2_121_ = new Class246_Sub3_Sub3_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_119_, bool_112_);
								if (class246_sub3_sub3_sub2.method65(true)) {
									class246_sub3_sub3_sub2.method62(var_ha, 24447);
								}
								class246_sub3_sub3 = class246_sub3_sub3_sub2;
								if (class246_sub3_sub3_sub2_121_.method65(true)) {
									class246_sub3_sub3_sub2_121_.method62(var_ha, 24447);
								}
								class246_sub3_sub3_120_ = class246_sub3_sub3_sub2_121_;
							} else {
								class246_sub3_sub3 = new Class246_Sub3_Sub3_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_100_ + 4, i_97_);
								class246_sub3_sub3_120_ = new Class246_Sub3_Sub3_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_119_, i_97_);
							}
							Class78.method790(i_102_, i_99_, i_96_, class246_sub3_sub3, class246_sub3_sub3_120_);
							if ((class352.anInt2956 == 1 || Class98_Sub42.aBoolean4218 && (class352.anInt2956 ^ 0xffffffff) == 0) && !this.underwater) {
								if ((i_100_ ^ 0xffffffff) == -1) {
									VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_, class352.anInt2953, class352.anInt2986, 1, 8);
									VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_ - -1, class352.anInt2953, class352.anInt2986, 2, 8);
								} else if (i_100_ != 1) {
									if ((i_100_ ^ 0xffffffff) != -3) {
										if ((i_100_ ^ 0xffffffff) == -4) {
											VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_, class352.anInt2953, class352.anInt2986, 1, 8);
											VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_, class352.anInt2953, class352.anInt2986, 2, 8);
										}
									} else {
										VarClientStringsDefinitionParser.method2921(i_99_ - -1, i_102_, i_96_, class352.anInt2953, class352.anInt2986, 1, 8);
										VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_, class352.anInt2953, class352.anInt2986, 2, 8);
									}
								} else {
									VarClientStringsDefinitionParser.method2921(1 + i_99_, i_102_, i_96_, class352.anInt2953, class352.anInt2986, 1, 8);
									VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_ + 1, class352.anInt2953, class352.anInt2986, 2, 8);
								}
							}
							if ((class352.actionCount ^ 0xffffffff) != -1 && class243 != null) {
								class243.method2945(i_96_, class352.walkable, !class352.clippingFlag, i_100_, i_99_, i_101_, (byte) 70);
							}
							if ((class352.anInt2966 ^ 0xffffffff) != -65) {
								Class246_Sub3_Sub5.method1439(i_102_, i_99_, i_96_, class352.anInt2966);
							}
						} else if (i_101_ == 3) {
							Class246_Sub3_Sub3 class246_sub3_sub3;
							if (!bool_113_) {
								class246_sub3_sub3 = new Class246_Sub3_Sub3_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_100_, i_97_);
							} else {
								Class246_Sub3_Sub3_Sub2 class246_sub3_sub3_sub2 = new Class246_Sub3_Sub3_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_101_, i_100_, bool_112_);
								class246_sub3_sub3 = class246_sub3_sub3_sub2;
								if (class246_sub3_sub3_sub2.method65(true)) {
									class246_sub3_sub3_sub2.method62(var_ha, 24447);
								}
							}
							Class78.method790(i_102_, i_99_, i_96_, class246_sub3_sub3, null);
							if (class352.aBoolean2947 && Class98_Sub9.aBoolean3851) {
								if (i_100_ != 0) {
									if (i_100_ == 1) {
										var_s.setShadowing(1 + i_99_, 1 + i_96_, 50);
									} else if ((i_100_ ^ 0xffffffff) == -3) {
										var_s.setShadowing(1 + i_99_, i_96_, 50);
									} else if (i_100_ == 3) {
										var_s.setShadowing(i_99_, i_96_, 50);
									}
								} else {
									var_s.setShadowing(i_99_, 1 + i_96_, 50);
								}
							}
							if ((class352.actionCount ^ 0xffffffff) != -1 && class243 != null) {
								class243.method2945(i_96_, class352.walkable, !class352.clippingFlag, i_100_, i_99_, i_101_, (byte) 126);
							}
						} else if (i_101_ == 9) {
							Class246_Sub3_Sub4 class246_sub3_sub4;
							if (bool_113_) {
								Class246_Sub3_Sub4_Sub1 class246_sub3_sub4_sub1 = new Class246_Sub3_Sub4_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_99_, i_99_, i_96_, i_96_, i_101_, i_100_, bool_112_);
								class246_sub3_sub4 = class246_sub3_sub4_sub1;
								if (class246_sub3_sub4_sub1.method65(true)) {
									class246_sub3_sub4_sub1.method62(var_ha, 24447);
								}
							} else {
								class246_sub3_sub4 = new Class246_Sub3_Sub4_Sub5(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_99_, i_103_ + i_99_ + -1, i_96_, -1 + i_104_ + i_96_, i_101_, i_100_, i_97_);
							}
							LoginOpcode.method2826(class246_sub3_sub4, false);
							if ((class352.anInt2956 ^ 0xffffffff) == -2 && !this.underwater) {
								int i_122_;
								if ((0x1 & i_100_) == 0) {
									i_122_ = 8;
								} else {
									i_122_ = 16;
								}
								VarClientStringsDefinitionParser.method2921(i_99_, i_102_, i_96_, 0, class352.anInt2986, i_122_, 8);
							}
							if ((class352.actionCount ^ 0xffffffff) != -1 && class243 != null) {
								class243.method2949(0, !class352.clippingFlag, i_99_, i_104_, i_103_, class352.walkable, i_96_);
							}
							if ((class352.anInt2966 ^ 0xffffffff) != -65) {
								Class246_Sub3_Sub5.method1439(i_102_, i_99_, i_96_, class352.anInt2966);
							}
						} else if (i_101_ == 4) {
							Class246_Sub3_Sub5 class246_sub3_sub5;
							if (!bool_113_) {
								class246_sub3_sub5 = new Class246_Sub3_Sub5_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, 0, 0, i_101_, i_100_, i_97_);
							} else {
								Class246_Sub3_Sub5_Sub2 class246_sub3_sub5_sub2 = new Class246_Sub3_Sub5_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, 0, 0, i_101_, i_100_);
								if (class246_sub3_sub5_sub2.method65(!bool)) {
									class246_sub3_sub5_sub2.method62(var_ha, 24447);
								}
								class246_sub3_sub5 = class246_sub3_sub5_sub2;
							}
							Class246_Sub3_Sub1.method2995(i_102_, i_99_, i_96_, class246_sub3_sub5, null);
						} else if ((i_101_ ^ 0xffffffff) == -6) {
							int i_123_ = 65;
							Interface19 interface19 = (Interface19) Class21_Sub1.method268(i_102_, i_99_, i_96_);
							if (interface19 != null) {
								i_123_ = 1 + Class130.gameObjectDefinitionList.get(interface19.method64(30472), (byte) 119).anInt2966;
							}
							Class246_Sub3_Sub5 class246_sub3_sub5;
							if (!bool_113_) {
								class246_sub3_sub5 = new Class246_Sub3_Sub5_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_123_ * Class351.anIntArray2923[i_100_], i_123_ * LoadingScreenElementType.anIntArray951[i_100_], i_101_, i_100_, i_97_);
							} else {
								Class246_Sub3_Sub5_Sub2 class246_sub3_sub5_sub2 = new Class246_Sub3_Sub5_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, Class351.anIntArray2923[i_100_] * i_123_, i_123_ * LoadingScreenElementType.anIntArray951[i_100_],
										i_101_, i_100_);
								if (class246_sub3_sub5_sub2.method65(true)) {
									class246_sub3_sub5_sub2.method62(var_ha, 24447);
								}
								class246_sub3_sub5 = class246_sub3_sub5_sub2;
							}
							Class246_Sub3_Sub1.method2995(i_102_, i_99_, i_96_, class246_sub3_sub5, null);
						} else if (i_101_ == 6) {
							int i_124_ = 33;
							Interface19 interface19 = (Interface19) Class21_Sub1.method268(i_102_, i_99_, i_96_);
							if (interface19 != null) {
								i_124_ = 1 + Class130.gameObjectDefinitionList.get(interface19.method64(30472), (byte) 119).anInt2966 / 2;
							}
							Class246_Sub3_Sub5 class246_sub3_sub5;
							if (bool_113_) {
								Class246_Sub3_Sub5_Sub2 class246_sub3_sub5_sub2 = new Class246_Sub3_Sub5_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, Class351.anIntArray2923[i_100_] * i_124_, LoadingScreenElementType.anIntArray951[i_100_] * i_124_,
										i_101_, 4 + i_100_);
								class246_sub3_sub5 = class246_sub3_sub5_sub2;
								if (class246_sub3_sub5_sub2.method65(true)) {
									class246_sub3_sub5_sub2.method62(var_ha, 24447);
								}
							} else {
								class246_sub3_sub5 = new Class246_Sub3_Sub5_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, OrthoZoomPreferenceField.anIntArray3688[i_100_] * i_124_, i_124_ * Class78.anIntArray595[i_100_], i_101_, 4 + i_100_, i_97_);
							}
							Class246_Sub3_Sub1.method2995(i_102_, i_99_, i_96_, class246_sub3_sub5, null);
						} else if (i_101_ == 7) {
							int i_125_ = 0x3 & i_100_ - -2;
							Class246_Sub3_Sub5 class246_sub3_sub5;
							if (!bool_113_) {
								class246_sub3_sub5 = new Class246_Sub3_Sub5_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, 0, 0, i_101_, 4 + i_125_, i_97_);
							} else {
								Class246_Sub3_Sub5_Sub2 class246_sub3_sub5_sub2 = new Class246_Sub3_Sub5_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, 0, 0, i_101_, 4 + i_125_);
								class246_sub3_sub5 = class246_sub3_sub5_sub2;
								if (class246_sub3_sub5_sub2.method65(true)) {
									class246_sub3_sub5_sub2.method62(var_ha, 24447);
								}
							}
							Class246_Sub3_Sub1.method2995(i_102_, i_99_, i_96_, class246_sub3_sub5, null);
						} else if ((i_101_ ^ 0xffffffff) == -9) {
							int i_126_ = 0x3 & 2 + i_100_;
							int i_127_ = 33;
							Interface19 interface19 = (Interface19) Class21_Sub1.method268(i_102_, i_99_, i_96_);
							if (interface19 != null) {
								i_127_ = Class130.gameObjectDefinitionList.get(interface19.method64(30472), (byte) 119).anInt2966 / 2 - -1;
							}
							Class246_Sub3_Sub5 class246_sub3_sub5;
							Class246_Sub3_Sub5 class246_sub3_sub5_128_;
							if (!bool_113_) {
								Class246_Sub3_Sub5_Sub1 class246_sub3_sub5_sub1 = new Class246_Sub3_Sub5_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, OrthoZoomPreferenceField.anIntArray3688[i_100_] * i_127_, Class78.anIntArray595[i_100_] * i_127_,
										i_101_, 4 + i_100_, i_97_);
								Class246_Sub3_Sub5_Sub1 class246_sub3_sub5_sub1_129_ = new Class246_Sub3_Sub5_Sub1(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, 0, 0, i_101_, i_126_ + 4, i_97_);
								class246_sub3_sub5 = class246_sub3_sub5_sub1;
								class246_sub3_sub5_128_ = class246_sub3_sub5_sub1_129_;
							} else {
								Class246_Sub3_Sub5_Sub2 class246_sub3_sub5_sub2 = new Class246_Sub3_Sub5_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, i_127_ * OrthoZoomPreferenceField.anIntArray3688[i_100_], i_127_ * Class78.anIntArray595[i_100_],
										i_101_, 4 + i_100_);
								Class246_Sub3_Sub5_Sub2 class246_sub3_sub5_sub2_130_ = new Class246_Sub3_Sub5_Sub2(var_ha, class352, i_102_, i_98_, i_110_, i_109_, i_111_, this.underwater, 0, 0, i_101_, 4 + i_126_);
								if (class246_sub3_sub5_sub2.method65(true)) {
									class246_sub3_sub5_sub2.method62(var_ha, 24447);
								}
								if (class246_sub3_sub5_sub2_130_.method65(true)) {
									class246_sub3_sub5_sub2_130_.method62(var_ha, 24447);
								}
								class246_sub3_sub5 = class246_sub3_sub5_sub2;
								class246_sub3_sub5_128_ = class246_sub3_sub5_sub2_130_;
							}
							Class246_Sub3_Sub1.method2995(i_102_, i_99_, i_96_, class246_sub3_sub5, class246_sub3_sub5_128_);
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ms.AA(" + i + ',' + i_96_ + ',' + i_97_ + ',' + i_98_ + ',' + bool + ',' + i_99_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_100_ + ',' + i_101_ + ',' + i_102_ + ',' + (class243 != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3589(boolean bool, byte i, RSToolkit var_ha) {
		try {
			Class262.method3213();
			if (i == 105) {
				if (!bool) {
					if (this.sizeHl > 1) {
						for (int i_131_ = 0; i_131_ < this.sizeX; i_131_++) {
							for (int i_132_ = 0; (this.sizeZ ^ 0xffffffff) < (i_132_ ^ 0xffffffff); i_132_++) {
								if ((0x2 & Class281.flags[1][i_131_][i_132_] ^ 0xffffffff) == -3) {
									Char.method2723(i_131_, i_132_);
								}
							}
						}
					}
					for (int i_133_ = 0; this.sizeHl > i_133_; i_133_++) {
						for (int i_134_ = 0; (i_134_ ^ 0xffffffff) >= (this.sizeZ ^ 0xffffffff); i_134_++) {
							for (int i_135_ = 0; i_135_ <= this.sizeX; i_135_++) {
								if ((this.aByteArrayArrayArray2550[i_133_][i_135_][i_134_] & 0x4) != 0) {
									int i_136_ = i_135_;
									int i_137_ = i_135_;
									int i_138_ = i_134_;
									int i_139_ = i_134_;
									for (/**/; (i_138_ ^ 0xffffffff) < -1 && (0x4 & this.aByteArrayArrayArray2550[i_133_][i_135_][-1 + i_138_]) != 0; i_138_--) {
										if ((-i_138_ + i_139_ ^ 0xffffffff) <= -11) {
											break;
										}
									}
									for (/**/; i_139_ < this.sizeZ && (0x4 & this.aByteArrayArrayArray2550[i_133_][i_135_][i_139_ + 1] ^ 0xffffffff) != -1; i_139_++) {
										if ((-i_138_ + i_139_ ^ 0xffffffff) <= -11) {
											break;
										}
									}
									while_128_: for (/**/; i_136_ > 0; i_136_--) {
										if ((i_137_ + -i_136_ ^ 0xffffffff) <= -11) {
											break;
										}
										for (int i_140_ = i_138_; i_140_ <= i_139_; i_140_++) {
											if ((this.aByteArrayArrayArray2550[i_133_][-1 + i_136_][i_140_] & 0x4) == 0) {
												break while_128_;
											}
										}
									}
									while_129_: for (/**/; i_137_ < this.sizeX && (i_137_ + -i_136_ ^ 0xffffffff) > -11; i_137_++) {
										for (int i_141_ = i_138_; (i_141_ ^ 0xffffffff) >= (i_139_ ^ 0xffffffff); i_141_++) {
											if ((this.aByteArrayArrayArray2550[i_133_][1 + i_137_][i_141_] & 0x4 ^ 0xffffffff) == -1) {
												break while_129_;
											}
										}
									}
									if (((1 + -i_136_ + i_137_) * (i_139_ + -i_138_ + 1) ^ 0xffffffff) <= -5) {
										int i_142_ = this.heightMap[i_133_][i_136_][i_138_];
										Class98_Sub46_Sub10.method1565(i_136_ << 361291369, 512 + (i_139_ << -91630135), false, 4, i_133_, 512 + (i_137_ << -2021985079), i_142_, i_138_ << 467375497, i_142_);
										for (int i_143_ = i_136_; i_137_ >= i_143_; i_143_++) {
											for (int i_144_ = i_138_; i_144_ <= i_139_; i_144_++) {
												((MapRegion) this).aByteArrayArrayArray2550[i_133_][i_143_][i_144_] = (byte) Class202.and(this.aByteArrayArrayArray2550[i_133_][i_143_][i_144_], -5);
											}
										}
									}
								}
							}
						}
					}
					Class284_Sub1_Sub2.method3371(31398);
				}
				this.aByteArrayArrayArray2550 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ms.BA(" + bool + ',' + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3590(int i, int i_145_, RSToolkit var_ha, ClipMap class243, int i_146_, int i_147_, int i_148_) {
		do {
			try {
				Interface19 interface19 = method3583(i, i_147_, i_145_, -1, i_148_);
				if (interface19 != null) {
					GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(interface19.method64(30472), (byte) 119);
					int i_149_ = interface19.method63((byte) 20);
					int i_150_ = interface19.method66(4657);
					if (class352.method3858(95)) {
						Js5Index.method3620(class352, -22015, i, i_145_, i_148_);
					}
					if (interface19.method65(true)) {
						interface19.method67(-25163, var_ha);
					}
					if ((i_147_ ^ 0xffffffff) == -1) {
						AnimatedLoadingBarLoadingScreenElement.method3972(i_145_, i_148_, i);
						if (class352.actionCount != 0) {
							class243.method2937(i_149_, class352.walkable, (byte) 84, !class352.clippingFlag, i_148_, i_150_, i);
						}
						if (class352.anInt2956 == 1) {
							if (i_150_ != 0) {
								if ((i_150_ ^ 0xffffffff) == -2) {
									Class100.method1692(2, i_145_, i_148_, 1 + i, 64);
								} else if (i_150_ != 2) {
									if (i_150_ == 3) {
										Class100.method1692(2, i_145_, i_148_, i, i_146_ + 63);
									}
								} else {
									Class100.method1692(1, i_145_, i_148_ - -1, i, i_146_ ^ 0x41);
								}
							} else {
								Class100.method1692(1, i_145_, i_148_, i, 64);
							}
						}
					} else if ((i_147_ ^ 0xffffffff) == -2) {
						SceneGraphNodeList.method2807(i_145_, i_148_, i);
					} else if ((i_147_ ^ 0xffffffff) != -3) {
						if (i_147_ == 3) {
							Class98_Sub46_Sub14.method1602(i_145_, i_148_, i);
							if (class352.actionCount == 1) {
								class243.method2948(false, i_148_, i);
							}
						}
					} else {
						RtInterfaceAttachment.method1162(i_145_, i_148_, i, aClass5304 != null ? aClass5304 : (aClass5304 = method3592("com.Interface19")));
						if ((class352.actionCount ^ 0xffffffff) != -1 && (this.sizeX ^ 0xffffffff) < (i_148_ - -class352.sizeY ^ 0xffffffff) && (i + class352.sizeY ^ 0xffffffff) > (this.sizeZ ^ 0xffffffff) && (class352.sizeX + i_148_
								^ 0xffffffff) > (this.sizeX ^ 0xffffffff) && this.sizeZ > i - -class352.sizeX) {
							class243.method2951(class352.sizeY, class352.walkable, i_150_, class352.sizeX, i, !class352.clippingFlag, 131072, i_148_);
						}
						if ((i_149_ ^ 0xffffffff) == -10) {
							if ((i_150_ & 0x1) == 0) {
								Class100.method1692(8, i_145_, i_148_, i, 64);
							} else {
								Class100.method1692(16, i_145_, i_148_, i, 64);
							}
						}
					}
				}
				if (i_146_ == 1) {
					break;
				}
				anInt5303 = 81;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ms.W(" + i + ',' + i_145_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + (class243 != null ? "{...}" : "null") + ',' + i_146_ + ',' + i_147_ + ',' + i_148_ + ')');
			}
			break;
		} while (false);
	}

	public final void method3591(ClipMap[] class243s, int i, byte i_151_, byte[] is, RSToolkit var_ha, int i_152_) {// IMPORTANT
		try {
			if (i_151_ >= -13) {
				method3582(-109, 44, -116, null, null, null);
			}
			RSByteBuffer class98_sub22 = new RSByteBuffer(is);
			int i_153_ = -1;
			for (;;) {
				int i_154_ = class98_sub22.method1208(3893);
				if ((i_154_ ^ 0xffffffff) == -1) {
					break;
				}
				i_153_ += i_154_;
				int i_155_ = 0;
				for (;;) {
					int i_156_ = class98_sub22.readSmart(1689622712);
					if (i_156_ == 0) {
						break;
					}
					i_155_ += -1 + i_156_;
					int i_157_ = 0x3f & i_155_;
					int i_158_ = (0xfea & i_155_) >> -1921081114;
					int i_159_ = i_155_ >> 1860692172;
					int i_160_ = class98_sub22.readUnsignedByte((byte) 50);
					int i_161_ = i_160_ >> 175799586;
					int i_162_ = 0x3 & i_160_;
					int i_163_ = i_152_ + i_158_;
					int i_164_ = i_157_ + i;
					if ((i_163_ ^ 0xffffffff) < -1 && (i_164_ ^ 0xffffffff) < -1 && (i_163_ ^ 0xffffffff) > (this.sizeX - 1 ^ 0xffffffff) && -1 + this.sizeZ > i_164_) {
						ClipMap class243 = null;
						if (!this.underwater) {
							int i_165_ = i_159_;
							if ((Class281.flags[1][i_163_][i_164_] & 0x2) == 2) {
								i_165_--;
							}
							if ((i_165_ ^ 0xffffffff) <= -1) {
								class243 = class243s[i_165_];
							}
						}
						// int absX = Class272.anInt2038 + i_163_;
						// int absY = aa_Sub2.anInt3562 + i_164_;
						// try {
						// BufferedWriter writer = new BufferedWriter(new
						// FileWriter("regiondump", true));
						// writer.write(i_153_ + "," + absX + "," + absY + "," +
						// i_159_ + "," + i_161_ + "," + i_162_);
						// writer.newLine();
						// writer.flush();
						// } catch(Exception e) {
						// e.printStackTrace();
						// }
						method3588(i_153_, i_164_, -1, i_159_, false, i_163_, var_ha, i_162_, i_161_, i_159_, class243);
					}
				}
			}
		} catch (Exception runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ms.U(" + (class243s != null ? "{...}" : "null") + ',' + i + ',' + i_151_ + ',' + (is != null ? "{...}" : "null") + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_152_ + ')');
		}
	}
}

/* Class98_Sub15 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.preferences.FlickeringEffectsPreferenceField;

public final class NodeString extends Node {
	public static float[]	aFloatArray3916	= new float[4];
	public static int		anInt3915;

	public static final void method1144(int i, int i_0_, boolean bool, int i_1_, Js5 class207, int i_2_, long l) {
		FlickeringEffectsPreferenceField.method604(0, bool, false, class207, i_1_, i, i_2_, l);
	}

	public String value;

	public NodeString() {
		/* empty */
	}

	public NodeString(String value) {
		this.value = value;
	}
}

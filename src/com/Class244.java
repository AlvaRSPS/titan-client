/* Class244 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.constants.BuildType;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class Class244 {

	public static final void method2953(byte i, int i_0_, int i_1_) {
		try {
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(0, -71, 15);
			class98_sub46_sub17.method1626((byte) -103);
			class98_sub46_sub17.headId = i_1_;
			class98_sub46_sub17.anInt6054 = i_0_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pga.B(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method2954(int i) {
		BuildType.WIP = null;

	}

	FontSpecifications	aClass197_1858;

	Font				aClass43_1859	= null;

	Class244(Font class43) {
		aClass197_1858 = null;
		try {
			aClass43_1859 = class43;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pga.<init>(" + (class43 != null ? "{...}" : "null") + ')');
		}
	}

	Class244(Font class43, FontSpecifications class197) {
		aClass197_1858 = null;
		try {
			aClass197_1858 = class197;
			aClass43_1859 = class43;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pga.<init>(" + (class43 != null ? "{...}" : "null") + ',' + (class197 != null ? "{...}" : "null") + ')');
		}
	}
}

/* Class76 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public abstract class Class76 {
	public static byte[][]			aByteArrayArray590;
	public static Class28[][]		aClass28ArrayArray586;
	public static IncomingOpcode	aClass58_587	= new IncomingOpcode(6, -2);
	public static int				hintMapedgeId;
	public static IncomingOpcode	SEND_SOUND		= new IncomingOpcode(69, 8);

	NativeToolkit					aHa_Sub3_585;

	Class76(NativeToolkit var_ha_Sub3) {
		aHa_Sub3_585 = var_ha_Sub3;
	}

	void method737(int i) {
		do {
			if (i == 2899) {
				break;
			}
			method746(-41, 122, 8);
			break;
		} while (false);
	}

	void method738(int i) {
		do {
			if (i <= -45) {
				break;
			}
			method745((byte) 50);
			break;
		} while (false);
	}

	public abstract void method739(int i);

	void method740(int i) {
		if (i >= -49) {
			method748(93, false);
		}
	}

	void method741(byte i) {

	}

	public abstract void method742(int i, int i_1_, Interface4 interface4);

	public abstract void method743(int i, boolean bool);

	abstract boolean method745(byte i);

	public abstract void method746(int i, int i_2_, int i_3_);

	void method747(int i) {
		try {
			if (i != -25684) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ew.P(" + i + ')');
		}
	}

	public abstract void method748(int i, boolean bool);

	void method749(int i) {
		try {
			if (i != 8) {
				method739(112);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ew.M(" + i + ')');
		}
	}
}

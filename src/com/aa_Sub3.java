/* aa_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.input.impl.AwtKeyListener;

public final class aa_Sub3 extends RtInterfaceClip {
	public static boolean				aBoolean3569		= false;
	public static boolean[]				aBooleanArray3575	= new boolean[200];
	public static char[]				aCharArray3572		= new char[] { '[', ']', '#' };
	public static OutgoingOpcode		aClass171_3570		= new OutgoingOpcode(40, 4);
	public static QuickChatMessageType	aClass348_3573		= new QuickChatMessageType(9, 0, 4, 1);
	public static IncomingOpcode		aClass58_3566		= new IncomingOpcode(67, -1);
	public static float					aFloat3567;
	public static int[]					anIntArray3571		= new int[5];
	public static boolean[]				isDirty				= new boolean[100];

	public static final int method157(int i, byte i_0_) {
		return (0x3ff26 & i) >> -1240258357;
	}

	public static final boolean method159(Js5 class207, int i, Class98_Sub31_Sub2 class98_sub31_sub2, Class268 class268, Js5 class207_2_, Js5 class207_3_) {
		BConfigDefinition.aClass98_Sub31_Sub2_3122 = class98_sub31_sub2;
		NodeInteger.aClass207_4127 = class207_2_;
		Class94.aClass207_793 = class207_3_;
		AwtKeyListener.anIntArray3804 = new int[16];
		Class270.aClass268_2032 = class268;
		MonoOrStereoPreferenceField.aClass207_3641 = class207;
		for (int i_4_ = 0; i_4_ < 16; i_4_++) {
			AwtKeyListener.anIntArray3804[i_4_] = 255;
		}
		return true;
	}

	public Class42_Sub1_Sub1 aClass42_Sub1_Sub1_3568;

	aa_Sub3(OpenGlToolkit var_ha_Sub1, int i, int i_1_, byte[] is) {
		aClass42_Sub1_Sub1_3568 = Class284_Sub2.method3374(6406, i_1_, 14764, 6406, var_ha_Sub1, false, is, i);
		aClass42_Sub1_Sub1_3568.method383(false, 10242, false);
	}
}

/* Class222 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.LightIntensityDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;
import com.jagex.game.input.impl.AwtMouseListener;

public final class LoginOpcode {
	public static boolean			aBoolean1667		= false;
	public static byte[]			aByteArray2492;
	public static OutgoingOpcode	aClass171_1669		= new OutgoingOpcode(22, 7);
	public static LoginOpcode		aClass222_2478		= new LoginOpcode(14, 0);
	public static LoginOpcode		aClass222_2481		= new LoginOpcode(16, -2);
	public static LoginOpcode		aClass222_2482		= new LoginOpcode(17, 0);

	public static LoginOpcode		aClass222_2483		= new LoginOpcode(18, -2);

	public static LoginOpcode		aClass222_2484		= new LoginOpcode(19, -2);

	public static LoginOpcode		aClass222_2485		= new LoginOpcode(22, -2);

	public static LoginOpcode		aClass222_2486		= new LoginOpcode(23, 4);

	public static LoginOpcode		aClass222_2487		= new LoginOpcode(24, -1);

	public static LoginOpcode		aClass222_2488		= new LoginOpcode(26, 0);
	public static LoginOpcode		aClass222_2489		= new LoginOpcode(27, 0);
	public static LoginOpcode		aClass222_2490		= new LoginOpcode(28, -2);
	private static LoginOpcode[]	aClass222Array2491	= new LoginOpcode[32];
	public static float[]			aFloatArray1671		= { 0.0F, -1.0F, 0.0F, 0.0F };
	public static int				anInt1670			= -1;
	public static int				anInt1672;
	public static LoginOpcode		ON_DEMAND			= new LoginOpcode(15, 4);

	static {
		LoginOpcode[] loginOpcode = getLoginOpcodes();
		for (int count = 0; (count ^ 0xffffffff) > (loginOpcode.length ^ 0xffffffff); count++) {
			aClass222Array2491[loginOpcode[count].opcode] = loginOpcode[count];
		}
	}

	public static final LoginOpcode[] getLoginOpcodes() {
		return new LoginOpcode[] { aClass222_2478, ON_DEMAND, aClass222_2481, aClass222_2482, aClass222_2483, aClass222_2484, aClass222_2485, aClass222_2486, aClass222_2487, aClass222_2488, aClass222_2489, aClass222_2490 };
	}

	public static final int method2824(byte i, ActionQueueEntry class98_sub46_sub8) {
		try {
			String string = HitmarksDefinition.getMenuText((byte) -124, class98_sub46_sub8);
			int[] is = null;
			if (AwtMouseListener.method3526(124, class98_sub46_sub8.actionId)) {
				is = Class98_Sub46_Sub19.itemDefinitionList.get((int) class98_sub46_sub8.aLong5987, (byte) -128).campaigns;
			} else if ((class98_sub46_sub8.anInt5988 ^ 0xffffffff) != 0) {
				is = Class98_Sub46_Sub19.itemDefinitionList.get(class98_sub46_sub8.anInt5988, (byte) -125).campaigns;
			} else if (!Class36.method340(class98_sub46_sub8.actionId, (byte) -70)) {
				if (Class98_Sub10_Sub21.method1064(class98_sub46_sub8.actionId, false)) {
					GameObjectDefinition class352;
					if (class98_sub46_sub8.actionId == 1009) {
						class352 = Class130.gameObjectDefinitionList.get((int) class98_sub46_sub8.aLong5987, (byte) 119);
					} else {
						class352 = Class130.gameObjectDefinitionList.get((int) (class98_sub46_sub8.aLong5987 >>> 762669664 & 0x7fffffffL), (byte) 119);
					}
					if (class352.transformIDs != null) {
						class352 = class352.get(StartupStage.varValues, (byte) -38);
					}
					if (class352 != null) {
						is = class352.anIntArray2934;
					}
				}
			} else {
				NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get((int) class98_sub46_sub8.aLong5987, -1);
				if (class98_sub39 != null) {
					NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
					NPCDefinition class141 = class246_sub3_sub4_sub2_sub1.definition;
					if (class141.anIntArray1109 != null) {
						class141 = class141.method2300(StartupStage.varValues, (byte) 55);
					}
					if (class141 != null) {
						is = class141.anIntArray1152;
					}
				}
			}
			if (is != null) {
				string += GroundBlendingPreferenceField.method653(0, is);
			}
			int i_1_ = Class42_Sub1.p13FullMetrics.method2676((byte) -126, GroupProgressMonitor.aClass332Array3408, string);
			if (class98_sub46_sub8.aBoolean5983) {
				i_1_ += Class284_Sub2_Sub2.aClass332_6199.getWidth() + 4;
			}
			return i_1_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oe.D(" + i + ',' + (class98_sub46_sub8 != null ? "{...}" : "null") + ')');
		}
	}

	public static final int method2825(int i, int i_2_) {
		try {
			if (i >= -31) {
				return -69;
			}
			int i_3_ = i_2_ * (i_2_ * i_2_ >> -1546827604) >> -2091093524;
			int i_4_ = i_2_ * 6 - 61440;
			int i_5_ = (i_2_ * i_4_ >> -1680263604) + 40960;
			return i_3_ * i_5_ >> -94375156;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oe.B(" + i + ',' + i_2_ + ')');
		}
	}

	public static final boolean method2826(Class246_Sub3_Sub4 class246_sub3_sub4, boolean bool) {
		boolean bool_6_ = Class78.aSArray594 == Class81.aSArray618;
		int i = 0;
		short i_7_ = 0;
		byte i_8_ = 0;
		class246_sub3_sub4.method3022(-8675);
		if (class246_sub3_sub4.aShort6158 < 0 || class246_sub3_sub4.aShort6157 < 0 || class246_sub3_sub4.aShort6160 >= BConfigDefinition.anInt3112 || class246_sub3_sub4.aShort6159 >= Class64_Sub9.anInt3662) {
			return false;
		}
		short i_9_ = 0;
		for (int i_10_ = class246_sub3_sub4.aShort6158; i_10_ <= class246_sub3_sub4.aShort6160; i_10_++) {
			for (int i_11_ = class246_sub3_sub4.aShort6157; i_11_ <= class246_sub3_sub4.aShort6159; i_11_++) {
				Class172 class172 = Class100.method1693(class246_sub3_sub4.plane, i_10_, i_11_);
				if (class172 != null) {
					Class154 class154 = Class154.method3524((byte) -90, class246_sub3_sub4);
					Class154 class154_12_ = class172.aClass154_1325;
					if (class154_12_ == null) {
						class172.aClass154_1325 = class154;
					} else {
						for (/**/; class154_12_.aClass154_1233 != null; class154_12_ = class154_12_.aClass154_1233) {
							/* empty */
						}
						class154_12_.aClass154_1233 = class154;
					}
					if (bool_6_ && (Class40.anIntArrayArray367[i_10_][i_11_] & ~0xffffff) != 0) {
						i = Class40.anIntArrayArray367[i_10_][i_11_];
						i_7_ = GraphicsDefinitionParser.aShortArrayArray2534[i_10_][i_11_];
						i_8_ = AwtMouseListener.aByteArrayArray5291[i_10_][i_11_];
					}
					if (!bool && class172.aClass246_Sub3_Sub1_1332 != null && class172.aClass246_Sub3_Sub1_1332.aShort6149 > i_9_) {
						i_9_ = class172.aClass246_Sub3_Sub1_1332.aShort6149;
					}
				}
			}
		}
		if (bool_6_ && (i & ~0xffffff) != 0) {
			for (int i_13_ = class246_sub3_sub4.aShort6158; i_13_ <= class246_sub3_sub4.aShort6160; i_13_++) {
				for (int i_14_ = class246_sub3_sub4.aShort6157; i_14_ <= class246_sub3_sub4.aShort6159; i_14_++) {
					if ((Class40.anIntArrayArray367[i_13_][i_14_] & ~0xffffff) == 0) {
						Class40.anIntArrayArray367[i_13_][i_14_] = i;
						GraphicsDefinitionParser.aShortArrayArray2534[i_13_][i_14_] = i_7_;
						AwtMouseListener.aByteArrayArray5291[i_13_][i_14_] = i_8_;
					}
				}
			}
		}
		if (bool) {
			Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[Class347.dynamicCount++] = class246_sub3_sub4;
		} else {
			int i_15_ = Class78.aSArray594 == Class81.aSArray618 ? 1 : 0;
			if (class246_sub3_sub4.method2978(-127)) {
				if (class246_sub3_sub4.method2987(6540)) {
					class246_sub3_sub4.animator = Class359.aClass246_Sub3Array3056[i_15_];
					Class359.aClass246_Sub3Array3056[i_15_] = class246_sub3_sub4;
				} else {
					class246_sub3_sub4.animator = LightIntensityDefinition.aClass246_Sub3Array3198[i_15_];
					LightIntensityDefinition.aClass246_Sub3Array3198[i_15_] = class246_sub3_sub4;
					Class358.aBoolean3033 = true;
				}
			} else {
				class246_sub3_sub4.animator = Class130.aClass246_Sub3Array1029[i_15_];
				Class130.aClass246_Sub3Array1029[i_15_] = class246_sub3_sub4;
			}
		}
		if (bool) {
			class246_sub3_sub4.anInt5089 -= i_9_;
		}
		return true;
	}

	public static void method2827(byte i) {
		if (i > -7) {
			method2826(null, true);
		}
		aClass171_1669 = null;
		aFloatArray1671 = null;
	}

	public int opcode;

	LoginOpcode(int i, int i_16_) {
		opcode = i;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

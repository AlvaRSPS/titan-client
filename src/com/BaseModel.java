/* Class178 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class BaseModel {
	public static boolean	isQuickChatWorld	= false;
	public static int		mouseX				= 0;

	public static final float method2588(float f, int i, int i_49_, float f_50_, float f_51_) {
		float[] fs = Class48_Sub2_Sub1.aFloatArrayArray5522[i];
		return fs[2] * f_51_ + (f * fs[0] + fs[1] * f_50_);
	}

	public byte			aByte1422;
	public byte[]		aByteArray1388;
	public byte[]		aByteArray1399;
	public byte[]		aByteArray1402;
	public byte[]		aByteArray1411;
	public byte[]		aByteArray1414;
	public byte[]		aByteArray1420;
	public byte[]		aByteArray1423;
	public Class106[]	aClass106Array1419;
	public Class35[]	aClass35Array1398;
	public Class87[]	aClass87Array1413;
	public int			anInt1391;
	public int			anInt1396;
	public int			anInt1406;
	public int			anInt1407	= 0;
	public int[]		anIntArray1386;
	public int[]		anIntArray1389;
	public int[]		anIntArray1390;
	public int[]		anIntArray1395;
	public int[]		anIntArray1397;
	public int[]		anIntArray1400;
	public int[]		anIntArray1404;
	public int[]		anIntArray1412;
	public int[]		anIntArray1416;
	public int[]		anIntArray1417;
	public int[]		anIntArray1418;
	public short[]		aShortArray1385;
	public short[]		aShortArray1392;
	public short[]		aShortArray1393;
	public short[]		aShortArray1394;
	public short[]		aShortArray1403;
	public short[]		aShortArray1408;
	public short[]		aShortArray1410;
	public short[]		aShortArray1421;
	public short[]		colours;
	public short[]		textures;

	public int			version		= 12;

	public BaseModel() {
		anInt1391 = 0;
		aByte1422 = (byte) 0;
		anInt1406 = 0;
		anInt1396 = 0;
	}

	BaseModel(byte[] is) {
		anInt1391 = 0;
		aByte1422 = (byte) 0;
		anInt1406 = 0;
		anInt1396 = 0;
		try {
			if (is[-1 + is.length] == -1 && is[is.length + -2] == -1) {
				method2589(is, 1);
			} else {
				method2587(is, -1);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lv.<init>(" + (is != null ? "{...}" : "null") + ')');
		}
	}

	BaseModel(int i, int i_212_, int i_213_) {
		anInt1391 = 0;
		aByte1422 = (byte) 0;
		anInt1406 = 0;
		anInt1396 = 0;
		try {
			aByteArray1420 = new byte[i_212_];
			anIntArray1416 = new int[i];
			aByteArray1402 = new byte[i_212_];
			anIntArray1418 = new int[i];
			anIntArray1400 = new int[i];
			aByteArray1414 = new byte[i_212_];
			anIntArray1395 = new int[i_212_];
			colours = new short[i_212_];
			aShortArray1410 = new short[i_212_];
			aShortArray1393 = new short[i_212_];
			anIntArray1417 = new int[i];
			aByteArray1411 = new byte[i_212_];
			if (i_213_ > 0) {
				aByteArray1399 = new byte[i_213_];
				aByteArray1423 = new byte[i_213_];
				anIntArray1412 = new int[i_213_];
				aShortArray1385 = new short[i_213_];
				anIntArray1386 = new int[i_213_];
				anIntArray1390 = new int[i_213_];
				anIntArray1397 = new int[i_213_];
				aByteArray1388 = new byte[i_213_];
				anIntArray1404 = new int[i_213_];
				aShortArray1421 = new short[i_213_];
				anIntArray1389 = new int[i_213_];
				aShortArray1403 = new short[i_213_];
			}
			textures = new short[i_212_];
			aShortArray1392 = new short[i_212_];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lv.<init>(" + i + ',' + i_212_ + ',' + i_213_ + ')');
		}
	}

	public BaseModel(BaseModel[] class178s, int i) {
		anInt1391 = 0;
		aByte1422 = (byte) 0;
		anInt1406 = 0;
		anInt1396 = 0;
		try {
			anInt1407 = 0;
			anInt1391 = 0;
			anInt1396 = 0;
			int i_214_ = 0;
			int i_215_ = 0;
			int i_216_ = 0;
			boolean bool = false;
			boolean bool_217_ = false;
			boolean bool_218_ = false;
			boolean bool_219_ = false;
			boolean bool_220_ = false;
			aByte1422 = (byte) -1;
			boolean bool_221_ = false;
			for (int i_222_ = 0; i_222_ < i; i_222_++) {
				BaseModel class178_223_ = class178s[i_222_];
				if (class178_223_ != null) {
					anInt1391 += class178_223_.anInt1391;
					anInt1407 += class178_223_.anInt1407;
					anInt1396 += class178_223_.anInt1396;
					if (class178_223_.aClass87Array1413 != null) {
						i_214_ += class178_223_.aClass87Array1413.length;
					}
					if (class178_223_.aClass106Array1419 != null) {
						i_216_ += class178_223_.aClass106Array1419.length;
					}
					bool = bool | class178_223_.aByteArray1414 != null;
					if (class178_223_.aClass35Array1398 != null) {
						i_215_ += class178_223_.aClass35Array1398.length;
					}
					bool_220_ = bool_220_ | class178_223_.textures != null;
					bool_219_ = bool_219_ | class178_223_.aByteArray1420 != null;
					bool_218_ = bool_218_ | class178_223_.aByteArray1411 != null;
					if (class178_223_.aByteArray1402 != null) {
						bool_217_ = true;
					} else {
						if ((aByte1422 ^ 0xffffffff) == 0) {
							aByte1422 = class178_223_.aByte1422;
						}
						if ((aByte1422 ^ 0xffffffff) != (class178_223_.aByte1422 ^ 0xffffffff)) {
							bool_217_ = true;
						}
					}
					bool_221_ = bool_221_ | class178_223_.anIntArray1395 != null;
				}
			}
			aShortArray1410 = new short[anInt1391];
			aShortArray1392 = new short[anInt1391];
			anIntArray1418 = new int[anInt1407];
			if (bool_221_) {
				anIntArray1395 = new int[anInt1391];
			}
			anIntArray1400 = new int[anInt1407];
			if ((i_215_ ^ 0xffffffff) < -1) {
				aClass35Array1398 = new Class35[i_215_];
			}
			if (bool_218_) {
				aByteArray1411 = new byte[anInt1391];
			}
			anIntArray1417 = new int[anInt1407];
			aShortArray1394 = new short[anInt1391];
			if (bool_220_) {
				textures = new short[anInt1391];
			}
			if (bool_217_) {
				aByteArray1402 = new byte[anInt1391];
			}
			if ((anInt1396 ^ 0xffffffff) < -1) {
				aShortArray1403 = new short[anInt1396];
				aByteArray1388 = new byte[anInt1396];
				aByteArray1399 = new byte[anInt1396];
				anIntArray1390 = new int[anInt1396];
				aByteArray1423 = new byte[anInt1396];
				aShortArray1385 = new short[anInt1396];
				anIntArray1386 = new int[anInt1396];
				anIntArray1397 = new int[anInt1396];
				aShortArray1421 = new short[anInt1396];
				anIntArray1412 = new int[anInt1396];
				anIntArray1389 = new int[anInt1396];
				anIntArray1404 = new int[anInt1396];
			}
			if (i_214_ > 0) {
				aClass87Array1413 = new Class87[i_214_];
			}
			if (bool_219_) {
				aByteArray1420 = new byte[anInt1391];
			}
			aShortArray1408 = new short[anInt1407];
			anIntArray1416 = new int[anInt1407];
			colours = new short[anInt1391];
			if (bool) {
				aByteArray1414 = new byte[anInt1391];
			}
			aShortArray1393 = new short[anInt1391];
			if (i_216_ > 0) {
				aClass106Array1419 = new Class106[i_216_];
			}
			anInt1407 = 0;
			anInt1396 = 0;
			i_214_ = 0;
			i_216_ = 0;
			anInt1391 = 0;
			i_215_ = 0;
			for (int i_224_ = 0; i > i_224_; i_224_++) {
				short i_225_ = (short) (1 << i_224_);
				BaseModel class178_226_ = class178s[i_224_];
				if (class178_226_ != null) {
					if (class178_226_.aClass106Array1419 != null) {
						for (int i_227_ = 0; (class178_226_.aClass106Array1419.length ^ 0xffffffff) < (i_227_ ^ 0xffffffff); i_227_++) {
							Class106 class106 = class178_226_.aClass106Array1419[i_227_];
							aClass106Array1419[i_216_++] = class106.method1719(anInt1391 + class106.anInt906, -125);
						}
					}
					for (int i_228_ = 0; (i_228_ ^ 0xffffffff) > (class178_226_.anInt1391 ^ 0xffffffff); i_228_++) {
						if (bool && class178_226_.aByteArray1414 != null) {
							aByteArray1414[anInt1391] = class178_226_.aByteArray1414[i_228_];
						}
						if (bool_217_) {
							if (class178_226_.aByteArray1402 != null) {
								aByteArray1402[anInt1391] = class178_226_.aByteArray1402[i_228_];
							} else {
								aByteArray1402[anInt1391] = class178_226_.aByte1422;
							}
						}
						if (bool_218_ && class178_226_.aByteArray1411 != null) {
							aByteArray1411[anInt1391] = class178_226_.aByteArray1411[i_228_];
						}
						if (bool_220_) {
							if (class178_226_.textures == null) {
								textures[anInt1391] = (short) -1;
							} else {
								textures[anInt1391] = class178_226_.textures[i_228_];
							}
						}
						if (bool_221_) {
							if (class178_226_.anIntArray1395 != null) {
								anIntArray1395[anInt1391] = class178_226_.anIntArray1395[i_228_];
							} else {
								anIntArray1395[anInt1391] = -1;
							}
						}
						aShortArray1393[anInt1391] = (short) method2598(class178_226_, class178_226_.aShortArray1393[i_228_], i_225_, 0);
						aShortArray1410[anInt1391] = (short) method2598(class178_226_, class178_226_.aShortArray1410[i_228_], i_225_, 0);
						aShortArray1392[anInt1391] = (short) method2598(class178_226_, class178_226_.aShortArray1392[i_228_], i_225_, 0);
						aShortArray1394[anInt1391] = i_225_;
						colours[anInt1391] = class178_226_.colours[i_228_];
						anInt1391++;
					}
					if (class178_226_.aClass87Array1413 != null) {
						for (int i_229_ = 0; class178_226_.aClass87Array1413.length > i_229_; i_229_++) {
							int i_230_ = method2598(class178_226_, class178_226_.aClass87Array1413[i_229_].anInt666, i_225_, 0);
							int i_231_ = method2598(class178_226_, class178_226_.aClass87Array1413[i_229_].anInt661, i_225_, 0);
							int i_232_ = method2598(class178_226_, class178_226_.aClass87Array1413[i_229_].anInt674, i_225_, 0);
							aClass87Array1413[i_214_] = class178_226_.aClass87Array1413[i_229_].method857(i_230_, true, i_232_, i_231_);
							i_214_++;
						}
					}
					if (class178_226_.aClass35Array1398 != null) {
						for (int i_233_ = 0; (i_233_ ^ 0xffffffff) > (class178_226_.aClass35Array1398.length ^ 0xffffffff); i_233_++) {
							int i_234_ = method2598(class178_226_, class178_226_.aClass35Array1398[i_233_].anInt327, i_225_, 0);
							aClass35Array1398[i_215_] = class178_226_.aClass35Array1398[i_233_].method336(-1854, i_234_);
							i_215_++;
						}
					}
				}
			}
			int i_235_ = 0;
			anInt1406 = anInt1407;
			for (int i_236_ = 0; i > i_236_; i_236_++) {
				short i_237_ = (short) (1 << i_236_);
				BaseModel class178_238_ = class178s[i_236_];
				if (class178_238_ != null) {
					for (int i_239_ = 0; class178_238_.anInt1391 > i_239_; i_239_++) {
						if (bool_219_) {
							aByteArray1420[i_235_++] = (byte) (class178_238_.aByteArray1420 != null && class178_238_.aByteArray1420[i_239_] != -1 ? class178_238_.aByteArray1420[i_239_] + anInt1396 : -1);
						}
					}
					for (int i_240_ = 0; i_240_ < class178_238_.anInt1396; i_240_++) {
						byte i_241_ = aByteArray1388[anInt1396] = class178_238_.aByteArray1388[i_240_];
						if ((i_241_ ^ 0xffffffff) == -1) {
							aShortArray1403[anInt1396] = (short) method2598(class178_238_, class178_238_.aShortArray1403[i_240_], i_237_, 0);
							aShortArray1421[anInt1396] = (short) method2598(class178_238_, class178_238_.aShortArray1421[i_240_], i_237_, 0);
							aShortArray1385[anInt1396] = (short) method2598(class178_238_, class178_238_.aShortArray1385[i_240_], i_237_, 0);
						}
						if ((i_241_ ^ 0xffffffff) <= -2 && (i_241_ ^ 0xffffffff) >= -4) {
							aShortArray1403[anInt1396] = class178_238_.aShortArray1403[i_240_];
							aShortArray1421[anInt1396] = class178_238_.aShortArray1421[i_240_];
							aShortArray1385[anInt1396] = class178_238_.aShortArray1385[i_240_];
							anIntArray1389[anInt1396] = class178_238_.anIntArray1389[i_240_];
							anIntArray1404[anInt1396] = class178_238_.anIntArray1404[i_240_];
							anIntArray1390[anInt1396] = class178_238_.anIntArray1390[i_240_];
							aByteArray1423[anInt1396] = class178_238_.aByteArray1423[i_240_];
							aByteArray1399[anInt1396] = class178_238_.aByteArray1399[i_240_];
							anIntArray1412[anInt1396] = class178_238_.anIntArray1412[i_240_];
						}
						if ((i_241_ ^ 0xffffffff) == -3) {
							anIntArray1397[anInt1396] = class178_238_.anIntArray1397[i_240_];
							anIntArray1386[anInt1396] = class178_238_.anIntArray1386[i_240_];
						}
						anInt1396++;
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lv.<init>(" + (class178s != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private final void method2587(byte[] is, int i) {
		do {
			try {
				boolean bool = false;
				boolean bool_0_ = false;
				RSByteBuffer class98_sub22 = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_1_ = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_2_ = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_3_ = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_4_ = new RSByteBuffer(is);
				class98_sub22.position = -18 + is.length;
				anInt1407 = class98_sub22.readShort((byte) 127);
				anInt1391 = class98_sub22.readShort((byte) 127);
				anInt1396 = class98_sub22.readUnsignedByte((byte) -104);
				int i_5_ = class98_sub22.readUnsignedByte((byte) -106);
				int i_6_ = class98_sub22.readUnsignedByte((byte) 113);
				int i_7_ = class98_sub22.readUnsignedByte((byte) -106);
				int i_8_ = class98_sub22.readUnsignedByte((byte) -119);
				int i_9_ = class98_sub22.readUnsignedByte((byte) -8);
				int i_10_ = class98_sub22.readShort((byte) 127);
				int i_11_ = class98_sub22.readShort((byte) 127);
				int i_12_ = class98_sub22.readShort((byte) 127);
				int i_13_ = class98_sub22.readShort((byte) 127);
				int i_14_ = 0;
				int i_15_ = i_14_;
				i_14_ += anInt1407;
				int i_16_ = i_14_;
				i_14_ += anInt1391;
				int i_17_ = i_14_;
				if (i_6_ == 255) {
					i_14_ += anInt1391;
				}
				int i_18_ = i_14_;
				if ((i_8_ ^ 0xffffffff) == -2) {
					i_14_ += anInt1391;
				}
				int i_19_ = i_14_;
				if (i_5_ == 1) {
					i_14_ += anInt1391;
				}
				int i_20_ = i_14_;
				if ((i_9_ ^ 0xffffffff) == -2) {
					i_14_ += anInt1407;
				}
				int i_21_ = i_14_;
				if (i_7_ == 1) {
					i_14_ += anInt1391;
				}
				int i_22_ = i_14_;
				i_14_ += i_13_;
				int i_23_ = i_14_;
				i_14_ += 2 * anInt1391;
				int i_24_ = i_14_;
				i_14_ += anInt1396 * 6;
				int i_25_ = i_14_;
				i_14_ += i_10_;
				int i_26_ = i_14_;
				i_14_ += i_11_;
				int i_27_ = i_14_;
				anIntArray1416 = new int[anInt1407];
				if (i_7_ == 1) {
					aByteArray1411 = new byte[anInt1391];
				}
				if (anInt1396 > 0) {
					aShortArray1421 = new short[anInt1396];
					aShortArray1385 = new short[anInt1396];
					aShortArray1403 = new short[anInt1396];
					aByteArray1388 = new byte[anInt1396];
				}
				if (i_5_ == 1) {
					aByteArray1414 = new byte[anInt1391];
					aByteArray1420 = new byte[anInt1391];
					textures = new short[anInt1391];
				}
				colours = new short[anInt1391];
				anIntArray1418 = new int[anInt1407];
				anIntArray1400 = new int[anInt1407];
				aShortArray1410 = new short[anInt1391];
				i_14_ += i_12_;
				aShortArray1392 = new short[anInt1391];
				class98_sub22.position = i_15_;
				aShortArray1393 = new short[anInt1391];
				if ((i_8_ ^ 0xffffffff) == -2) {
					anIntArray1395 = new int[anInt1391];
				}
				if ((i_6_ ^ 0xffffffff) == -256) {
					aByteArray1402 = new byte[anInt1391];
				} else {
					aByte1422 = (byte) i_6_;
				}
				if (i_9_ == 1) {
					anIntArray1417 = new int[anInt1407];
				}
				class98_sub22_1_.position = i_25_;
				class98_sub22_2_.position = i_26_;
				class98_sub22_3_.position = i_27_;
				class98_sub22_4_.position = i_20_;
				int i_28_ = 0;
				int i_29_ = 0;
				int i_30_ = 0;
				for (int i_31_ = 0; anInt1407 > i_31_; i_31_++) {
					int i_32_ = class98_sub22.readUnsignedByte((byte) 20);
					int i_33_ = 0;
					if ((i_32_ & 0x1 ^ 0xffffffff) != -1) {
						i_33_ = class98_sub22_1_.method1239(40);
					}
					int i_34_ = 0;
					if ((i_32_ & 0x2 ^ 0xffffffff) != -1) {
						i_34_ = class98_sub22_2_.method1239(-122);
					}
					int i_35_ = 0;
					if ((0x4 & i_32_ ^ 0xffffffff) != -1) {
						i_35_ = class98_sub22_3_.method1239(i ^ ~0x31);
					}
					anIntArray1416[i_31_] = i_33_ + i_28_;
					anIntArray1400[i_31_] = i_34_ + i_29_;
					anIntArray1418[i_31_] = i_30_ + i_35_;
					i_29_ = anIntArray1400[i_31_];
					i_30_ = anIntArray1418[i_31_];
					i_28_ = anIntArray1416[i_31_];
					if ((i_9_ ^ 0xffffffff) == -2) {
						anIntArray1417[i_31_] = class98_sub22_4_.readUnsignedByte((byte) -127);
					}
				}
				class98_sub22.position = i_23_;
				class98_sub22_1_.position = i_19_;
				class98_sub22_2_.position = i_17_;
				class98_sub22_3_.position = i_21_;
				class98_sub22_4_.position = i_18_;
				for (int i_36_ = 0; i_36_ < anInt1391; i_36_++) {
					colours[i_36_] = (short) class98_sub22.readShort((byte) 127);
					if ((i_5_ ^ 0xffffffff) == -2) {
						int i_37_ = class98_sub22_1_.readUnsignedByte((byte) -105);
						if ((i_37_ & 0x1 ^ 0xffffffff) != -2) {
							aByteArray1414[i_36_] = (byte) 0;
						} else {
							bool = true;
							aByteArray1414[i_36_] = (byte) 1;
						}
						if ((0x2 & i_37_) != 2) {
							aByteArray1420[i_36_] = (byte) -1;
							textures[i_36_] = (short) -1;
						} else {
							aByteArray1420[i_36_] = (byte) (i_37_ >> -870827966);
							textures[i_36_] = colours[i_36_];
							colours[i_36_] = (short) 127;
							if (textures[i_36_] != -1) {
								bool_0_ = true;
							}
						}
					}
					if ((i_6_ ^ 0xffffffff) == -256) {
						aByteArray1402[i_36_] = class98_sub22_2_.readSignedByte((byte) -19);
					}
					if (i_7_ == 1) {
						aByteArray1411[i_36_] = class98_sub22_3_.readSignedByte((byte) -19);
					}
					if ((i_8_ ^ 0xffffffff) == -2) {
						anIntArray1395[i_36_] = class98_sub22_4_.readUnsignedByte((byte) 39);
					}
				}
				anInt1406 = i;
				class98_sub22.position = i_22_;
				class98_sub22_1_.position = i_16_;
				short i_38_ = 0;
				short i_39_ = 0;
				short i_40_ = 0;
				int i_41_ = 0;
				for (int i_42_ = 0; anInt1391 > i_42_; i_42_++) {
					int i_43_ = class98_sub22_1_.readUnsignedByte((byte) 32);
					if ((i_43_ ^ 0xffffffff) == -2) {
						i_38_ = (short) (class98_sub22.method1239(-96) + i_41_);
						i_41_ = i_38_;
						i_39_ = (short) (class98_sub22.method1239(105) + i_41_);
						i_41_ = i_39_;
						i_40_ = (short) (class98_sub22.method1239(i + -104) + i_41_);
						aShortArray1393[i_42_] = i_38_;
						i_41_ = i_40_;
						aShortArray1410[i_42_] = i_39_;
						aShortArray1392[i_42_] = i_40_;
						if ((anInt1406 ^ 0xffffffff) > (i_38_ ^ 0xffffffff)) {
							anInt1406 = i_38_;
						}
						if (i_39_ > anInt1406) {
							anInt1406 = i_39_;
						}
						if ((anInt1406 ^ 0xffffffff) > (i_40_ ^ 0xffffffff)) {
							anInt1406 = i_40_;
						}
					}
					if (i_43_ == 2) {
						i_39_ = i_40_;
						i_40_ = (short) (i_41_ + class98_sub22.method1239(120));
						aShortArray1393[i_42_] = i_38_;
						i_41_ = i_40_;
						aShortArray1410[i_42_] = i_39_;
						aShortArray1392[i_42_] = i_40_;
						if (i_40_ > anInt1406) {
							anInt1406 = i_40_;
						}
					}
					if ((i_43_ ^ 0xffffffff) == -4) {
						i_38_ = i_40_;
						i_40_ = (short) (class98_sub22.method1239(39) + i_41_);
						aShortArray1393[i_42_] = i_38_;
						i_41_ = i_40_;
						aShortArray1410[i_42_] = i_39_;
						aShortArray1392[i_42_] = i_40_;
						if (i_40_ > anInt1406) {
							anInt1406 = i_40_;
						}
					}
					if (i_43_ == 4) {
						short i_44_ = i_38_;
						i_38_ = i_39_;
						i_39_ = i_44_;
						i_40_ = (short) (class98_sub22.method1239(116) + i_41_);
						aShortArray1393[i_42_] = i_38_;
						i_41_ = i_40_;
						aShortArray1410[i_42_] = i_39_;
						aShortArray1392[i_42_] = i_40_;
						if (anInt1406 < i_40_) {
							anInt1406 = i_40_;
						}
					}
				}
				class98_sub22.position = i_24_;
				anInt1406++;
				for (int i_45_ = 0; anInt1396 > i_45_; i_45_++) {
					aByteArray1388[i_45_] = (byte) 0;
					aShortArray1403[i_45_] = (short) class98_sub22.readShort((byte) 127);
					aShortArray1421[i_45_] = (short) class98_sub22.readShort((byte) 127);
					aShortArray1385[i_45_] = (short) class98_sub22.readShort((byte) 127);
				}
				if (aByteArray1420 != null) {
					boolean bool_46_ = false;
					for (int i_47_ = 0; (i_47_ ^ 0xffffffff) > (anInt1391 ^ 0xffffffff); i_47_++) {
						int i_48_ = 0xff & aByteArray1420[i_47_];
						if ((i_48_ ^ 0xffffffff) != -256) {
							if ((0xffff & aShortArray1403[i_48_]) != aShortArray1393[i_47_] || (0xffff & aShortArray1421[i_48_]) != aShortArray1410[i_47_] || (aShortArray1392[i_47_] ^ 0xffffffff) != (aShortArray1385[i_48_] & 0xffff ^ 0xffffffff)) {
								bool_46_ = true;
							} else {
								aByteArray1420[i_47_] = (byte) -1;
							}
						}
					}
					if (!bool_46_) {
						aByteArray1420 = null;
					}
				}
				if (!bool) {
					aByteArray1414 = null;
				}
				if (bool_0_) {
					break;
				}
				textures = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "lv.H(" + (is != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	private final void method2589(byte[] is, int i) {
		do {
			try {
				RSByteBuffer class98_sub22 = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_52_ = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_53_ = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_54_ = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_55_ = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_56_ = new RSByteBuffer(is);
				RSByteBuffer class98_sub22_57_ = new RSByteBuffer(is);
				class98_sub22.position = is.length - 23;
				anInt1407 = class98_sub22.readShort((byte) 127);
				anInt1391 = class98_sub22.readShort((byte) 127);
				anInt1396 = class98_sub22.readUnsignedByte((byte) 7);
				int i_58_ = class98_sub22.readUnsignedByte((byte) -111);
				boolean bool = (i_58_ & i) == 1;
				boolean bool_59_ = (0x2 & i_58_ ^ 0xffffffff) == -3;
				boolean bool_60_ = (i_58_ & 0x4) == 4;
				boolean bool_61_ = (i_58_ & 0x8 ^ 0xffffffff) == -9;
				if (bool_61_) {
					class98_sub22.position -= 7;
					version = class98_sub22.readUnsignedByte((byte) -118);
					class98_sub22.position += 6;
				}
				int i_62_ = class98_sub22.readUnsignedByte((byte) 42);
				int i_63_ = class98_sub22.readUnsignedByte((byte) 66);
				int i_64_ = class98_sub22.readUnsignedByte((byte) -118);
				int i_65_ = class98_sub22.readUnsignedByte((byte) -112);
				int i_66_ = class98_sub22.readUnsignedByte((byte) -126);
				int i_67_ = class98_sub22.readShort((byte) 127);
				int i_68_ = class98_sub22.readShort((byte) 127);
				int i_69_ = class98_sub22.readShort((byte) 127);
				int i_70_ = class98_sub22.readShort((byte) 127);
				int i_71_ = class98_sub22.readShort((byte) 127);
				int i_72_ = 0;
				int i_73_ = 0;
				int i_74_ = 0;
				if (anInt1396 > 0) {
					class98_sub22.position = 0;
					aByteArray1388 = new byte[anInt1396];
					for (int i_75_ = 0; i_75_ < anInt1396; i_75_++) {
						byte i_76_ = aByteArray1388[i_75_] = class98_sub22.readSignedByte((byte) -19);
						if ((i_76_ ^ 0xffffffff) == -3) {
							i_74_++;
						}
						if ((i_76_ ^ 0xffffffff) == -1) {
							i_72_++;
						}
						if ((i_76_ ^ 0xffffffff) <= -2 && i_76_ <= 3) {
							i_73_++;
						}
					}
				}
				int i_77_ = anInt1396;
				int i_78_ = i_77_;
				i_77_ += anInt1407;
				int i_79_ = i_77_;
				if (bool) {
					i_77_ += anInt1391;
				}
				int i_80_ = i_77_;
				i_77_ += anInt1391;
				int i_81_ = i_77_;
				if (i_62_ == 255) {
					i_77_ += anInt1391;
				}
				int i_82_ = i_77_;
				if (i_64_ == 1) {
					i_77_ += anInt1391;
				}
				int i_83_ = i_77_;
				if (i_66_ == 1) {
					i_77_ += anInt1407;
				}
				int i_84_ = i_77_;
				if (i_63_ == 1) {
					i_77_ += anInt1391;
				}
				int i_85_ = i_77_;
				i_77_ += i_70_;
				int i_86_ = i_77_;
				if (i_65_ == 1) {
					i_77_ += 2 * anInt1391;
				}
				int i_87_ = i_77_;
				i_77_ += i_71_;
				int i_88_ = i_77_;
				i_77_ += 2 * anInt1391;
				int i_89_ = i_77_;
				i_77_ += i_67_;
				int i_90_ = i_77_;
				i_77_ += i_68_;
				int i_91_ = i_77_;
				i_77_ += i_69_;
				int i_92_ = i_77_;
				i_77_ += i_72_ * 6;
				int i_93_ = i_77_;
				i_77_ += i_73_ * 6;
				int i_94_ = 6;
				if ((version ^ 0xffffffff) != -15) {
					if ((version ^ 0xffffffff) <= -16) {
						i_94_ = 9;
					}
				} else {
					i_94_ = 7;
				}
				int i_95_ = i_77_;
				i_77_ += i_73_ * i_94_;
				int i_96_ = i_77_;
				i_77_ += i_73_;
				int i_97_ = i_77_;
				i_77_ += i_73_;
				int i_98_ = i_77_;
				i_77_ += 2 * i_74_ + i_73_;
				aShortArray1410 = new short[anInt1391];
				aShortArray1393 = new short[anInt1391];
				int i_99_ = i_77_;
				if ((i_64_ ^ 0xffffffff) == -2) {
					anIntArray1395 = new int[anInt1391];
				}
				if (bool) {
					aByteArray1414 = new byte[anInt1391];
				}
				if ((i_63_ ^ 0xffffffff) == -2) {
					aByteArray1411 = new byte[anInt1391];
				}
				class98_sub22.position = i_78_;
				if ((anInt1396 ^ 0xffffffff) < -1) {
					if ((i_73_ ^ 0xffffffff) < -1) {
						anIntArray1412 = new int[i_73_];
						anIntArray1389 = new int[i_73_];
						aByteArray1423 = new byte[i_73_];
						aByteArray1399 = new byte[i_73_];
						anIntArray1404 = new int[i_73_];
						anIntArray1390 = new int[i_73_];
					}
					if ((i_74_ ^ 0xffffffff) < -1) {
						anIntArray1397 = new int[i_74_];
						anIntArray1386 = new int[i_74_];
					}
					aShortArray1403 = new short[anInt1396];
					aShortArray1421 = new short[anInt1396];
					aShortArray1385 = new short[anInt1396];
				}
				anIntArray1416 = new int[anInt1407];
				if (i_65_ == 1) {
					textures = new short[anInt1391];
				}
				aShortArray1392 = new short[anInt1391];
				colours = new short[anInt1391];
				if ((i_62_ ^ 0xffffffff) == -256) {
					aByteArray1402 = new byte[anInt1391];
				} else {
					aByte1422 = (byte) i_62_;
				}
				anIntArray1400 = new int[anInt1407];
				anIntArray1418 = new int[anInt1407];
				if (i_65_ == 1 && (anInt1396 ^ 0xffffffff) < -1) {
					aByteArray1420 = new byte[anInt1391];
				}
				if ((i_66_ ^ 0xffffffff) == -2) {
					anIntArray1417 = new int[anInt1407];
				}
				class98_sub22_52_.position = i_89_;
				class98_sub22_53_.position = i_90_;
				class98_sub22_54_.position = i_91_;
				class98_sub22_55_.position = i_83_;
				int i_100_ = 0;
				int i_101_ = 0;
				int i_102_ = 0;
				for (int i_103_ = 0; (anInt1407 ^ 0xffffffff) < (i_103_ ^ 0xffffffff); i_103_++) {
					int i_104_ = class98_sub22.readUnsignedByte((byte) 50);
					int i_105_ = 0;
					if ((0x1 & i_104_ ^ 0xffffffff) != -1) {
						i_105_ = class98_sub22_52_.method1239(i + -65);
					}
					int i_106_ = 0;
					if ((i_104_ & 0x2 ^ 0xffffffff) != -1) {
						i_106_ = class98_sub22_53_.method1239(-111);
					}
					int i_107_ = 0;
					if ((0x4 & i_104_) != 0) {
						i_107_ = class98_sub22_54_.method1239(56);
					}
					anIntArray1416[i_103_] = i_100_ + i_105_;
					anIntArray1400[i_103_] = i_101_ + i_106_;
					anIntArray1418[i_103_] = i_102_ - -i_107_;
					i_101_ = anIntArray1400[i_103_];
					i_102_ = anIntArray1418[i_103_];
					i_100_ = anIntArray1416[i_103_];
					if ((i_66_ ^ 0xffffffff) == -2) {
						anIntArray1417[i_103_] = class98_sub22_55_.readUnsignedByte((byte) 63);
					}
				}
				class98_sub22.position = i_88_;
				class98_sub22_52_.position = i_79_;
				class98_sub22_53_.position = i_81_;
				class98_sub22_54_.position = i_84_;
				class98_sub22_55_.position = i_82_;
				class98_sub22_56_.position = i_86_;
				class98_sub22_57_.position = i_87_;
				for (int i_108_ = 0; anInt1391 > i_108_; i_108_++) {
					colours[i_108_] = (short) class98_sub22.readShort((byte) 127);
					if (bool) {
						aByteArray1414[i_108_] = class98_sub22_52_.readSignedByte((byte) -19);
					}
					if (i_62_ == 255) {
						aByteArray1402[i_108_] = class98_sub22_53_.readSignedByte((byte) -19);
					}
					if ((i_63_ ^ 0xffffffff) == -2) {
						aByteArray1411[i_108_] = class98_sub22_54_.readSignedByte((byte) -19);
					}
					if (i_64_ == 1) {
						anIntArray1395[i_108_] = class98_sub22_55_.readUnsignedByte((byte) 9);
					}
					if (i_65_ == 1) {
						textures[i_108_] = (short) (class98_sub22_56_.readShort((byte) 127) + -1);
					}
					if (aByteArray1420 != null) {
						if (textures[i_108_] == -1) {
							aByteArray1420[i_108_] = (byte) -1;
						} else {
							aByteArray1420[i_108_] = (byte) (class98_sub22_57_.readUnsignedByte((byte) -120) + -1);
						}
					}
				}
				anInt1406 = -1;
				class98_sub22.position = i_85_;
				class98_sub22_52_.position = i_80_;
				short i_109_ = 0;
				short i_110_ = 0;
				short i_111_ = 0;
				int i_112_ = 0;
				for (int i_113_ = 0; i_113_ < anInt1391; i_113_++) {
					int i_114_ = class98_sub22_52_.readUnsignedByte((byte) -118);
					if ((i_114_ ^ 0xffffffff) == -2) {
						i_109_ = (short) (i_112_ + class98_sub22.method1239(44));
						i_112_ = i_109_;
						i_110_ = (short) (class98_sub22.method1239(i ^ 0x44) + i_112_);
						i_112_ = i_110_;
						i_111_ = (short) (class98_sub22.method1239(i + -99) + i_112_);
						i_112_ = i_111_;
						aShortArray1393[i_113_] = i_109_;
						aShortArray1410[i_113_] = i_110_;
						aShortArray1392[i_113_] = i_111_;
						if (i_109_ > anInt1406) {
							anInt1406 = i_109_;
						}
						if (anInt1406 < i_110_) {
							anInt1406 = i_110_;
						}
						if (i_111_ > anInt1406) {
							anInt1406 = i_111_;
						}
					}
					if ((i_114_ ^ 0xffffffff) == -3) {
						i_110_ = i_111_;
						i_111_ = (short) (i_112_ + class98_sub22.method1239(-78));
						aShortArray1393[i_113_] = i_109_;
						i_112_ = i_111_;
						aShortArray1410[i_113_] = i_110_;
						aShortArray1392[i_113_] = i_111_;
						if ((i_111_ ^ 0xffffffff) < (anInt1406 ^ 0xffffffff)) {
							anInt1406 = i_111_;
						}
					}
					if ((i_114_ ^ 0xffffffff) == -4) {
						i_109_ = i_111_;
						i_111_ = (short) (i_112_ + class98_sub22.method1239(74));
						i_112_ = i_111_;
						aShortArray1393[i_113_] = i_109_;
						aShortArray1410[i_113_] = i_110_;
						aShortArray1392[i_113_] = i_111_;
						if ((anInt1406 ^ 0xffffffff) > (i_111_ ^ 0xffffffff)) {
							anInt1406 = i_111_;
						}
					}
					if ((i_114_ ^ 0xffffffff) == -5) {
						short i_115_ = i_109_;
						i_109_ = i_110_;
						i_110_ = i_115_;
						i_111_ = (short) (i_112_ + class98_sub22.method1239(i + -87));
						aShortArray1393[i_113_] = i_109_;
						i_112_ = i_111_;
						aShortArray1410[i_113_] = i_110_;
						aShortArray1392[i_113_] = i_111_;
						if ((anInt1406 ^ 0xffffffff) > (i_111_ ^ 0xffffffff)) {
							anInt1406 = i_111_;
						}
					}
				}
				anInt1406++;
				class98_sub22.position = i_92_;
				class98_sub22_52_.position = i_93_;
				class98_sub22_53_.position = i_95_;
				class98_sub22_54_.position = i_96_;
				class98_sub22_55_.position = i_97_;
				class98_sub22_56_.position = i_98_;
				for (int i_116_ = 0; anInt1396 > i_116_; i_116_++) {
					int i_117_ = aByteArray1388[i_116_] & 0xff;
					if ((i_117_ ^ 0xffffffff) == -1) {
						aShortArray1403[i_116_] = (short) class98_sub22.readShort((byte) 127);
						aShortArray1421[i_116_] = (short) class98_sub22.readShort((byte) 127);
						aShortArray1385[i_116_] = (short) class98_sub22.readShort((byte) 127);
					}
					if (i_117_ == 1) {
						aShortArray1403[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						aShortArray1421[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						aShortArray1385[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						if (version < 15) {
							anIntArray1389[i_116_] = class98_sub22_53_.readShort((byte) 127);
							if (version >= 14) {
								anIntArray1404[i_116_] = class98_sub22_53_.readMediumInt(-127);
							} else {
								anIntArray1404[i_116_] = class98_sub22_53_.readShort((byte) 127);
							}
							anIntArray1390[i_116_] = class98_sub22_53_.readShort((byte) 127);
						} else {
							anIntArray1389[i_116_] = class98_sub22_53_.readMediumInt(i + -129);
							anIntArray1404[i_116_] = class98_sub22_53_.readMediumInt(-127);
							anIntArray1390[i_116_] = class98_sub22_53_.readMediumInt(Class369.method3953(i, -125));
						}
						aByteArray1423[i_116_] = class98_sub22_54_.readSignedByte((byte) -19);
						aByteArray1399[i_116_] = class98_sub22_55_.readSignedByte((byte) -19);
						anIntArray1412[i_116_] = class98_sub22_56_.readSignedByte((byte) -19);
					}
					if (i_117_ == 2) {
						aShortArray1403[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						aShortArray1421[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						aShortArray1385[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						if ((version ^ 0xffffffff) <= -16) {
							anIntArray1389[i_116_] = class98_sub22_53_.readMediumInt(Class369.method3953(i, -124));
							anIntArray1404[i_116_] = class98_sub22_53_.readMediumInt(-126);
							anIntArray1390[i_116_] = class98_sub22_53_.readMediumInt(-128);
						} else {
							anIntArray1389[i_116_] = class98_sub22_53_.readShort((byte) 127);
							if (version < 14) {
								anIntArray1404[i_116_] = class98_sub22_53_.readShort((byte) 127);
							} else {
								anIntArray1404[i_116_] = class98_sub22_53_.readMediumInt(-127);
							}
							anIntArray1390[i_116_] = class98_sub22_53_.readShort((byte) 127);
						}
						aByteArray1423[i_116_] = class98_sub22_54_.readSignedByte((byte) -19);
						aByteArray1399[i_116_] = class98_sub22_55_.readSignedByte((byte) -19);
						anIntArray1412[i_116_] = class98_sub22_56_.readSignedByte((byte) -19);
						anIntArray1397[i_116_] = class98_sub22_56_.readSignedByte((byte) -19);
						anIntArray1386[i_116_] = class98_sub22_56_.readSignedByte((byte) -19);
					}
					if (i_117_ == 3) {
						aShortArray1403[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						aShortArray1421[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						aShortArray1385[i_116_] = (short) class98_sub22_52_.readShort((byte) 127);
						if (version < 15) {
							anIntArray1389[i_116_] = class98_sub22_53_.readShort((byte) 127);
							if ((version ^ 0xffffffff) > -15) {
								anIntArray1404[i_116_] = class98_sub22_53_.readShort((byte) 127);
							} else {
								anIntArray1404[i_116_] = class98_sub22_53_.readMediumInt(-123);
							}
							anIntArray1390[i_116_] = class98_sub22_53_.readShort((byte) 127);
						} else {
							anIntArray1389[i_116_] = class98_sub22_53_.readMediumInt(i + -126);
							anIntArray1404[i_116_] = class98_sub22_53_.readMediumInt(-123);
							anIntArray1390[i_116_] = class98_sub22_53_.readMediumInt(-123);
						}
						aByteArray1423[i_116_] = class98_sub22_54_.readSignedByte((byte) -19);
						aByteArray1399[i_116_] = class98_sub22_55_.readSignedByte((byte) -19);
						anIntArray1412[i_116_] = class98_sub22_56_.readSignedByte((byte) -19);
					}
				}
				class98_sub22.position = i_99_;
				if (bool_59_) {
					int i_118_ = class98_sub22.readUnsignedByte((byte) -119);
					if ((i_118_ ^ 0xffffffff) < -1) {
						aClass87Array1413 = new Class87[i_118_];
						for (int i_119_ = 0; (i_118_ ^ 0xffffffff) < (i_119_ ^ 0xffffffff); i_119_++) {
							int i_120_ = class98_sub22.readShort((byte) 127);
							int i_121_ = class98_sub22.readShort((byte) 127);
							byte i_122_;
							if ((i_62_ ^ 0xffffffff) != -256) {
								i_122_ = (byte) i_62_;
							} else {
								i_122_ = aByteArray1402[i_121_];
							}
							aClass87Array1413[i_119_] = new Class87(i_120_, aShortArray1393[i_121_], aShortArray1410[i_121_], aShortArray1392[i_121_], i_122_);
						}
					}
					int i_123_ = class98_sub22.readUnsignedByte((byte) 113);
					if (i_123_ > 0) {
						aClass35Array1398 = new Class35[i_123_];
						for (int i_124_ = 0; (i_123_ ^ 0xffffffff) < (i_124_ ^ 0xffffffff); i_124_++) {
							int i_125_ = class98_sub22.readShort((byte) 127);
							int i_126_ = class98_sub22.readShort((byte) 127);
							aClass35Array1398[i_124_] = new Class35(i_125_, i_126_);
						}
					}
				}
				if (!bool_60_) {
					break;
				}
				int i_127_ = class98_sub22.readUnsignedByte((byte) 6);
				if ((i_127_ ^ 0xffffffff) >= -1) {
					break;
				}
				aClass106Array1419 = new Class106[i_127_];
				for (int i_128_ = 0; (i_127_ ^ 0xffffffff) < (i_128_ ^ 0xffffffff); i_128_++) {
					int i_129_ = class98_sub22.readShort((byte) 127);
					int i_130_ = class98_sub22.readShort((byte) 127);
					int i_131_ = class98_sub22.readUnsignedByte((byte) 18);
					byte i_132_ = class98_sub22.readSignedByte((byte) -19);
					aClass106Array1419[i_128_] = new Class106(i_129_, i_130_, i_131_, i_132_);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "lv.E(" + (is != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final int[][] method2591(byte i) {
		try {
			int[] is = new int[256];
			int i_137_ = 0;
			for (int i_138_ = 0; (i_138_ ^ 0xffffffff) > (anInt1391 ^ 0xffffffff); i_138_++) {
				int i_139_ = anIntArray1395[i_138_];
				if ((i_139_ ^ 0xffffffff) <= -1) {
					if ((i_139_ ^ 0xffffffff) < (i_137_ ^ 0xffffffff)) {
						i_137_ = i_139_;
					}
					is[i_139_]++;
				}
			}
			int[][] is_140_ = new int[1 + i_137_][];
			if (i <= 96) {
				return null;
			}
			for (int i_141_ = 0; i_137_ >= i_141_; i_141_++) {
				is_140_[i_141_] = new int[is[i_141_]];
				is[i_141_] = 0;
			}
			for (int i_142_ = 0; (i_142_ ^ 0xffffffff) > (anInt1391 ^ 0xffffffff); i_142_++) {
				int i_143_ = anIntArray1395[i_142_];
				if ((i_143_ ^ 0xffffffff) <= -1) {
					is_140_[i_143_][is[i_143_]++] = i_142_;
				}
			}
			return is_140_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lv.J(" + i + ')');
		}
	}

	public final int method2594(byte i, short i_150_, short i_151_, byte i_152_, boolean bool, int i_153_, int i_154_, byte i_155_, int i_156_) {
		try {
			aShortArray1393[anInt1391] = (short) i_154_;
			aShortArray1410[anInt1391] = (short) i_156_;
			aShortArray1392[anInt1391] = (short) i_153_;
			aByteArray1414[anInt1391] = i;
			aByteArray1420[anInt1391] = i_155_;
			colours[anInt1391] = i_150_;
			aByteArray1411[anInt1391] = i_152_;
			if (bool != false) {
				anIntArray1390 = null;
			}
			textures[anInt1391] = i_151_;
			return anInt1391++;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lv.F(" + i + ',' + i_150_ + ',' + i_151_ + ',' + i_152_ + ',' + bool + ',' + i_153_ + ',' + i_154_ + ',' + i_155_ + ',' + i_156_ + ')');
		}
	}

	public final int[][] method2595(int i, boolean bool) {
		int[] is = new int[256];
		int i_157_ = 0;
		int i_158_ = !bool ? anInt1406 : anInt1407;
		for (int i_159_ = 0; (i_158_ ^ 0xffffffff) < (i_159_ ^ 0xffffffff); i_159_++) {
			int i_160_ = anIntArray1417[i_159_];
			if (i_160_ >= 0) {
				if ((i_157_ ^ 0xffffffff) > (i_160_ ^ 0xffffffff)) {
					i_157_ = i_160_;
				}
				is[i_160_]++;
			}
		}
		int[][] is_161_ = new int[i_157_ + 1][];
		for (int i_162_ = 0; i_157_ >= i_162_; i_162_++) {
			is_161_[i_162_] = new int[is[i_162_]];
			is[i_162_] = 0;
		}
		for (int i_163_ = 0; i_158_ > i_163_; i_163_++) {
			int i_164_ = anIntArray1417[i_163_];
			if ((i_164_ ^ 0xffffffff) <= -1) {
				is_161_[i_164_][is[i_164_]++] = i_163_;
			}
		}
		return is_161_;
	}

	public final int[][] method2596(int i) {
		int[] is = new int[256];
		int i_165_ = 0;
		for (int i_166_ = 0; (aClass106Array1419.length ^ 0xffffffff) < (i_166_ ^ 0xffffffff); i_166_++) {
			int i_167_ = aClass106Array1419[i_166_].anInt908;
			if ((i_167_ ^ 0xffffffff) <= -1) {
				if (i_165_ < i_167_) {
					i_165_ = i_167_;
				}
				is[i_167_]++;
			}
		}
		int[][] is_168_ = new int[i_165_ + 1][];
		for (int i_169_ = 0; (i_169_ ^ 0xffffffff) >= (i_165_ ^ 0xffffffff); i_169_++) {
			is_168_[i_169_] = new int[is[i_169_]];
			is[i_169_] = 0;
		}
		int i_170_ = 0;
		if (i != 21517) {
			anIntArray1418 = null;
		}
		for (/**/; (i_170_ ^ 0xffffffff) > (aClass106Array1419.length ^ 0xffffffff); i_170_++) {
			int i_171_ = aClass106Array1419[i_170_].anInt908;
			if (i_171_ >= 0) {
				is_168_[i_171_][is[i_171_]++] = i_170_;
			}
		}
		return is_168_;
	}

	public final void method2597(int i, int i_172_, byte i_173_, int i_174_) {
		for (int i_175_ = 0; i_175_ < anInt1407; i_175_++) {
			anIntArray1416[i_175_] += i_172_;
			anIntArray1400[i_175_] += i_174_;
			anIntArray1418[i_175_] += i;
		}
	}

	private final int method2598(BaseModel class178_176_, int i, short i_177_, int i_178_) {
		int i_179_ = class178_176_.anIntArray1416[i];
		int i_180_ = class178_176_.anIntArray1400[i];
		int i_181_ = class178_176_.anIntArray1418[i];
		for (int i_182_ = i_178_; (anInt1407 ^ 0xffffffff) < (i_182_ ^ 0xffffffff); i_182_++) {
			if (anIntArray1416[i_182_] == i_179_ && anIntArray1400[i_182_] == i_180_ && anIntArray1418[i_182_] == i_181_) {
				aShortArray1408[i_182_] = (short) Class41.or(aShortArray1408[i_182_], i_177_);
				return i_182_;
			}
		}
		anIntArray1416[anInt1407] = i_179_;
		anIntArray1400[anInt1407] = i_180_;
		anIntArray1418[anInt1407] = i_181_;
		aShortArray1408[anInt1407] = i_177_;
		anIntArray1417[anInt1407] = class178_176_.anIntArray1417 != null ? class178_176_.anIntArray1417[i] : -1;
		return anInt1407++;
	}

	public final int method2599(int i, int i_183_, int i_184_, int i_185_) {
		for (int i_186_ = 0; anInt1407 > i_186_; i_186_++) {
			if ((i_183_ ^ 0xffffffff) == (anIntArray1416[i_186_] ^ 0xffffffff) && (anIntArray1400[i_186_] ^ 0xffffffff) == (i_184_ ^ 0xffffffff) && i_185_ == anIntArray1418[i_186_]) {
				return i_186_;
			}
		}
		anIntArray1416[anInt1407] = i_183_;
		anIntArray1400[anInt1407] = i_184_;
		anIntArray1418[anInt1407] = i_185_;
		anInt1406 = anInt1407 + 1;
		if (i != 14418) {
			method2598(null, -77, (short) 58, 51);
		}
		return anInt1407++;
	}

	public final void method2600(int i, int i_187_, byte i_188_, int i_189_) {
		do {
			if ((i ^ 0xffffffff) != -1) {
				int i_190_ = Class284_Sub2_Sub2.SINE[i];
				int i_191_ = Class284_Sub2_Sub2.COSINE[i];
				for (int i_192_ = 0; i_192_ < anInt1407; i_192_++) {
					int i_193_ = i_191_ * anIntArray1416[i_192_] + anIntArray1400[i_192_] * i_190_ >> -455896722;
					anIntArray1400[i_192_] = -(anIntArray1416[i_192_] * i_190_) + anIntArray1400[i_192_] * i_191_ >> 1929293742;
					anIntArray1416[i_192_] = i_193_;
				}
			}
			if ((i_187_ ^ 0xffffffff) != -1) {
				int i_195_ = Class284_Sub2_Sub2.SINE[i_187_];
				int i_196_ = Class284_Sub2_Sub2.COSINE[i_187_];
				for (int i_197_ = 0; anInt1407 > i_197_; i_197_++) {
					int i_198_ = anIntArray1400[i_197_] * i_196_ + -(i_195_ * anIntArray1418[i_197_]) >> 324877006;
					anIntArray1418[i_197_] = anIntArray1400[i_197_] * i_195_ + anIntArray1418[i_197_] * i_196_ >> -2137564178;
					anIntArray1400[i_197_] = i_198_;
				}
			}
			if (i_189_ == 0) {
				break;
			}
			int i_199_ = Class284_Sub2_Sub2.SINE[i_189_];
			int i_200_ = Class284_Sub2_Sub2.COSINE[i_189_];
			for (int i_201_ = 0; (i_201_ ^ 0xffffffff) > (anInt1407 ^ 0xffffffff); i_201_++) {
				int i_202_ = i_199_ * anIntArray1418[i_201_] + i_200_ * anIntArray1416[i_201_] >> 616678414;
				anIntArray1418[i_201_] = anIntArray1418[i_201_] * i_200_ - i_199_ * anIntArray1416[i_201_] >> -1745229234;
				anIntArray1416[i_201_] = i_202_;
			}
			break;
		} while (false);
	}

	public final byte method2601(byte i, byte i_203_, short i_204_, short i_205_, short i_206_, short i_207_, short i_208_, byte i_209_, short i_210_, byte i_211_) {
		if (anInt1396 >= 255) {
			throw new IllegalStateException();
		}
		aByteArray1388[anInt1396] = (byte) 3;
		aShortArray1403[anInt1396] = i_205_;
		aShortArray1421[anInt1396] = i_210_;
		aShortArray1385[anInt1396] = i_204_;
		anIntArray1389[anInt1396] = i_208_;
		anIntArray1404[anInt1396] = i_207_;
		anIntArray1390[anInt1396] = i_206_;
		aByteArray1423[anInt1396] = i_211_;
		aByteArray1399[anInt1396] = i;
		anIntArray1412[anInt1396] = i_203_;
		if (i_209_ <= 116) {
			return (byte) -112;
		}
		return (byte) anInt1396++;
	}

	public final void replaceColour(int i, short oldColour, short newColour) {
		for (int colourIndex = i; anInt1391 > colourIndex; colourIndex++) {
			if ((oldColour ^ 0xffffffff) == (colours[colourIndex] ^ 0xffffffff)) {
				colours[colourIndex] = newColour;
			}
		}

	}

	public final void replaceTexture(short newTexture, byte i_133_, short oldTexture) {
		if (textures != null) {
			for (int textureIndex = 0; textureIndex < anInt1391; textureIndex++) {
				if ((oldTexture ^ 0xffffffff) == (textures[textureIndex] ^ 0xffffffff)) {
					textures[textureIndex] = newTexture;
				}
			}
		}
	}

	public final void scaleLog2(int i, int i_144_) {
		do {
			for (int i_145_ = 0; anInt1407 > i_145_; i_145_++) {
				anIntArray1416[i_145_] <<= i_144_;
				anIntArray1400[i_145_] <<= i_144_;
				anIntArray1418[i_145_] <<= i_144_;
			}
			if ((anInt1396 ^ 0xffffffff) >= -1 || anIntArray1389 == null) {
				break;
			}
			for (int i_146_ = 0; (anIntArray1389.length ^ 0xffffffff) < (i_146_ ^ 0xffffffff); i_146_++) {
				anIntArray1389[i_146_] <<= i_144_;
				anIntArray1404[i_146_] <<= i_144_;
				if (aByteArray1388[i_146_] != 1) {
					anIntArray1390[i_146_] <<= i_144_;
				}
			}
			break;
		} while (false);
	}
}

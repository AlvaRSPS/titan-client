/* Class82 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.toolkit.shadow.OpenGlShadow;

public final class Class82 {
	public static OutgoingOpcode				aClass171_625;
	public static int							anInt629;
	public static FloorUnderlayDefinitionParser	floorUnderDefinitionList;

	static {
		aClass171_625 = new OutgoingOpcode(14, 11);
	}

	public static void method821(int i) {
		try {
			if (i == 14) {
				aClass171_625 = null;
				floorUnderDefinitionList = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fg.B(" + i + ')');
		}
	}

	public static final void method822(int i) {
		try {
			OpenGlShadow.aClass79_6321.clear(i ^ 0x27b6);
			if (i != 10157) {
				aClass171_625 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fg.C(" + i + ')');
		}
	}

	public static final Class42_Sub1_Sub1 method823(int i, OpenGlToolkit var_ha_Sub1, int i_0_, int i_1_, int i_2_) {
		try {
			if (!var_ha_Sub1.aBoolean4426 && (!Class81.method815(i_0_, 0) || !Class81.method815(i, 0))) {
				if (var_ha_Sub1.haveArbTextureRectangle) {
					return new Class42_Sub1_Sub1(var_ha_Sub1, 34037, i_2_, i_0_, i);
				}
				return new Class42_Sub1_Sub1(var_ha_Sub1, i_2_, i_0_, i, Class48.findNextGreaterPwr2(423660257, i_0_), Class48.findNextGreaterPwr2(423660257, i));
			}
			return new Class42_Sub1_Sub1(var_ha_Sub1, 3553, i_2_, i_0_, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fg.A(" + i + ',' + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	private OpenGLXToolkit	aHa_Sub3_Sub2_627;

	long					aLong628;

	public Class82(OpenGLXToolkit var_ha_Sub3_Sub2, long l, Class230[] class230s) {
		try {
			aLong628 = l;
			aHa_Sub3_Sub2_627 = var_ha_Sub3_Sub2;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fg.<init>(" + (var_ha_Sub3_Sub2 != null ? "{...}" : "null") + ',' + l + ',' + (class230s != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			aHa_Sub3_Sub2_627.method2082(0, aLong628);
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fg.finalize(" + ')');
		}
	}
}

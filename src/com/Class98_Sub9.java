/* Class98_Sub9 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.matrix.NativeMatrix;

public final class Class98_Sub9 extends Node {
	public static boolean	aBoolean3851;
	public static int		anInt3854;

	static {
		anInt3854 = 0;
	}

	public static void method988(int i) {
		try {
			if (i == 29030) {
				client.preferences = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "daa.B(" + i + ')');
		}
	}

	public static final void method989(RsBitsBuffers packet, byte i) {
		try {
			packet.writeInt(1571862888, Class94.animationFramesJs5.getIndexCrc32((byte) -72));
			packet.writeInt(1571862888, Class323.animationFrameBasesJs5.getIndexCrc32((byte) -105));
			packet.writeInt(1571862888, client.miscellaneousJs5.getIndexCrc32((byte) -117));
			packet.writeInt(1571862888, TexturesPreferenceField.interfacesJs5.getIndexCrc32((byte) -105));
			packet.writeInt(1571862888, Class76_Sub2.soundEffectsJs5.getIndexCrc32((byte) -63));
			packet.writeInt(1571862888, Class234.mapsJs5.getIndexCrc32((byte) -40));
			packet.writeInt(1571862888, Class98_Sub10_Sub1.musicJs5.getIndexCrc32((byte) -68));
			packet.writeInt(1571862888, Class76_Sub9.modelsJs5.getIndexCrc32((byte) -49));
			packet.writeInt(1571862888, client.spriteJs5.getIndexCrc32((byte) -80));
			packet.writeInt(1571862888, RSByteBuffer.texturesJs5.getIndexCrc32((byte) -8));
			packet.writeInt(1571862888, NodeShort.huffmanJs5.getIndexCrc32((byte) -101));
			packet.writeInt(1571862888, HashTableIterator.music2Js5.getIndexCrc32((byte) -81));
			packet.writeInt(1571862888, NewsLSEConfig.clientScriptJs5.getIndexCrc32((byte) -33));
			packet.writeInt(1571862888, client.fontJs5.getIndexCrc32((byte) -38));
			packet.writeInt(1571862888, BuildLocation.midiInstrumentsJs5.getIndexCrc32((byte) -78));
			packet.writeInt(1571862888, Class119_Sub2.sound3Js5.getIndexCrc32((byte) -38));
			packet.writeInt(1571862888, Class375.objectsJs5.getIndexCrc32((byte) -17));
			packet.writeInt(1571862888, Class98_Sub10_Sub24.enumsJs5.getIndexCrc32((byte) -95));
			packet.writeInt(1571862888, Class234.npcJs5.getIndexCrc32((byte) -14));
			packet.writeInt(1571862888, Class208.itemsJs5.getIndexCrc32((byte) -48));
			packet.writeInt(1571862888, Char.animationJs5.getIndexCrc32((byte) -24));
			packet.writeInt(1571862888, PlayerUpdateMask.graphicJs5.getIndexCrc32((byte) -12));
			packet.writeInt(1571862888, Class98_Sub46_Sub19.bConfigJs5.getIndexCrc32((byte) -66));
			packet.writeInt(1571862888, RenderAnimDefinitionParser.worldMapJs5.getIndexCrc32((byte) -73));
			packet.writeInt(1571862888, Class81.quickChatMessagesJs5.getIndexCrc32((byte) -51));
			packet.writeInt(1571862888, OpenGlElementBufferPointer.quickChatMenuJs5.getIndexCrc32((byte) -13));
			packet.writeInt(1571862888, TexturesPreferenceField.materialsJs5.getIndexCrc32((byte) -16));
			packet.writeInt(1571862888, Class245.particlesJs5.getIndexCrc32((byte) -30));
			packet.writeInt(1571862888, AsyncCache.defaultsJs5.getIndexCrc32((byte) -104));
			packet.writeInt(1571862888, NativeMatrix.billboardsJs5.getIndexCrc32((byte) -95));
			packet.writeInt(1571862888, Class245.nativesJs5.getIndexCrc32((byte) -59));
			packet.writeInt(1571862888, client.shaderJs5.getIndexCrc32((byte) -12));
			packet.writeInt(1571862888, GroundItem.method1276(-2));
			packet.writeInt(1571862888, SpriteLoadingScreenElement.method2243(true));
			packet.writeInt(1571862888, ParticleManager.vorbisJs5.getIndexCrc32((byte) -90));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "daa.A(" + (packet != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	int	anInt3852;

	int	anInt3853;

	Class98_Sub9(int i, int i_0_) {
		try {
			anInt3852 = i;
			anInt3853 = i_0_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "daa.<init>(" + i + ',' + i_0_ + ')');
		}
	}
}

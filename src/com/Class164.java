/* Class164 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class Class164 {
	public static int anInt1274;

	public static final void drawLocationSprite(int locX, int id, int gameScreenHeight, RtInterfaceClip clip, RSToolkit toolkit, int dummy, RtInterface interf, int gameScreenWidth, int locY) {
		do {
			WorldMapInfoDefinition mapInfo = Class216.worldMapInfoDefinitionList.get(-72, id);
			//System.out.println(mapInfo.locationName);

			if (mapInfo == null || !mapInfo.morphisms || !mapInfo.method284(34, StartupStage.varValues)) {
				break;
			}

			if (mapInfo.anIntArray265 != null) {
				int[] dimensions = new int[mapInfo.anIntArray265.length];
				for (int index = 0; (index ^ 0xffffffff) > (dimensions.length / 2 ^ 0xffffffff); index++) {
					int i_6_;
					if (Class98_Sub46_Sub20_Sub2.cameraMode != 4) {
						i_6_ = 0x3fff & (int) RsFloatBuffer.aFloat5794 + Class204.anInt1553;
					} else {
						i_6_ = (int) RsFloatBuffer.aFloat5794 & 0x3fff;
					}
					int i_7_ = Class284_Sub2_Sub2.SINE[i_6_];
					int i_8_ = Class284_Sub2_Sub2.COSINE[i_6_];
					if (Class98_Sub46_Sub20_Sub2.cameraMode != 4) {
						i_8_ = 256 * i_8_ / (256 + Class151.anInt1213);
						i_7_ = 256 * i_7_ / (256 + Class151.anInt1213);
					}
					dimensions[2 * index] = gameScreenWidth + interf.renderWidth / 2 + (i_7_ * (4 * mapInfo.anIntArray265[2 * index - -1] + locY) - -(i_8_ * (locX - -(mapInfo.anIntArray265[index * 2] * 4))) >> 1110343022);
					dimensions[2 * index + 1] = interf.renderHeight / 2 + gameScreenHeight - (i_8_ * (4 * mapInfo.anIntArray265[index * 2 + 1] + locY) - i_7_ * (4 * mapInfo.anIntArray265[2 * index] + locX) >> -2040004466);
				}
				Class136.method2272(toolkit, dimensions, mapInfo.anInt249, interf.anIntArray2217, interf.anIntArray2298);
				if ((mapInfo.anInt250 ^ 0xffffffff) >= -1) {
					for (int i_9_ = 0; i_9_ < -1 + dimensions.length / 2; i_9_++) {
						toolkit.a(dimensions[i_9_ * 2], dimensions[2 * i_9_ - -1], dimensions[2 * i_9_ - -2], dimensions[2 + 2 * i_9_ + 1], mapInfo.anIntArray234[mapInfo.aByteArray229[i_9_] & 0xff], 1, clip, gameScreenWidth, gameScreenHeight);
					}
					toolkit.a(dimensions[-2 + dimensions.length], dimensions[dimensions.length - 1], dimensions[0], dimensions[1], mapInfo.anIntArray234[0xff & mapInfo.aByteArray229[mapInfo.aByteArray229.length - 1]], 1, clip, gameScreenWidth, gameScreenHeight);
				} else {
					for (int i_10_ = 0; dimensions.length / 2 - 1 > i_10_; i_10_++) {
						int i_11_ = dimensions[2 * i_10_];
						int i_12_ = dimensions[2 * i_10_ + 1];
						int i_13_ = dimensions[2 * (i_10_ + 1)];
						int i_14_ = dimensions[1 + 2 * (i_10_ - -1)];
						if (i_11_ > i_13_) {
							int i_15_ = i_11_;
							i_11_ = i_13_;
							int i_16_ = i_12_;
							i_12_ = i_14_;
							i_13_ = i_15_;
							i_14_ = i_16_;
						} else if (i_13_ == i_11_ && i_12_ > i_14_) {
							int i_17_ = i_12_;
							i_12_ = i_14_;
							i_14_ = i_17_;
						}
						toolkit.a(i_11_, i_12_, i_13_, i_14_, mapInfo.anIntArray234[mapInfo.aByteArray229[i_10_] & 0xff], 1, clip, gameScreenWidth, gameScreenHeight, mapInfo.anInt250, mapInfo.anInt253, mapInfo.anInt224);
					}
					int i_18_ = dimensions[-2 + dimensions.length];
					int i_19_ = dimensions[-1 + dimensions.length];
					int i_20_ = dimensions[0];
					int i_21_ = dimensions[1];
					if (i_20_ >= i_18_) {
						if ((i_20_ ^ 0xffffffff) == (i_18_ ^ 0xffffffff) && (i_19_ ^ 0xffffffff) < (i_21_ ^ 0xffffffff)) {
							int i_22_ = i_19_;
							i_19_ = i_21_;
							i_21_ = i_22_;
						}
					} else {
						int i_23_ = i_18_;
						int i_24_ = i_19_;
						i_18_ = i_20_;
						i_20_ = i_23_;
						i_19_ = i_21_;
						i_21_ = i_24_;
					}
					toolkit.a(i_18_, i_19_, i_20_, i_21_, mapInfo.anIntArray234[mapInfo.aByteArray229[mapInfo.aByteArray229.length - 1] & 0xff], 1, clip, gameScreenWidth, gameScreenHeight, mapInfo.anInt250, mapInfo.anInt253, mapInfo.anInt224);
				}
			}

			Sprite sprite = null;
			if ((mapInfo.anInt245 ^ 0xffffffff) != 0) {
				sprite = mapInfo.getSprite((byte) 92, toolkit, false);
				if (sprite != null) {
					Class4.drawMapDot(gameScreenHeight, locY, gameScreenWidth, interf, clip, locX, (byte) -24, sprite);
				}
			}
			if (mapInfo.locationName == null) {
				break;
			}
			int height = 0;
			if (sprite != null) {
				height = sprite.getHeight();
			}
			Font font = Class69_Sub2.p11Full;
			FontSpecifications specs = StrongReferenceMCNode.p11FullMetrics;
			if (mapInfo.anInt264 == 1) {
				specs = Class98_Sub46_Sub10.p12FullMetrics;
				font = Class195.p12Full;
			}
			if ((mapInfo.anInt264 ^ 0xffffffff) == -3) {
				specs = Class42_Sub1.p13FullMetrics;
				font = Class98_Sub10_Sub34.p13Full;
			}
			OutgoingOpcode.method2540(locX, gameScreenWidth, specs, gameScreenHeight, interf, height, (byte) 109, mapInfo.locationName, clip, mapInfo.anInt257, locY, font);
			break;
		} while (false);
	}

	int anInt1275;

	public Class164(int i) {
		anInt1275 = i;
		System.out.println("Type: " + anInt1275);
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

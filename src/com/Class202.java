/* Class202 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.CursorDefinition;
import com.jagex.game.client.definition.IdentikitDefinition;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.ui.layout.VerticalAlignment;

public final class Class202 {
	public static boolean			aBoolean1548	= false;
	public static VerticalAlignment	aClass110_1547;
	public static Class308			aClass308_1550;

	public static int and(int i, int i_28_) {
		return i & i_28_;
	}

	public static final void method2698(byte i, int i_0_, int i_1_, int i_2_, int i_3_) {
		try {
			float f = (float) WorldMap.anInt2084 / (float) WorldMap.anInt2089;
			int i_4_ = i_1_;
			if (i < 47) {
				method2700(43, null, true, -10, null);
			}
			int i_5_ = i_3_;
			do {
				if (!(f < 1.0F)) {
					i_4_ = (int) (i_3_ / f);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				i_5_ = (int) (i_1_ * f);
			} while (false);
			i_0_ -= (i_3_ + -i_5_) / 2;
			i_2_ -= (-i_4_ + i_1_) / 2;
			NodeShort.anInt4197 = WorldMap.anInt2084 + -(i_0_ * WorldMap.anInt2084 / i_5_);
			Class169.anInt1307 = -1;
			GrandExchangeOffer.anInt849 = -1;
			Class42_Sub4.anInt5371 = i_2_ * WorldMap.anInt2089 / i_4_;
			aa_Sub1.method155(-1);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ne.A(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public static void method2699(byte i) {
		try {
			if (i == 44) {
				aClass110_1547 = null;
				aClass308_1550 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ne.B(" + i + ')');
		}
	}

	public static final int method2700(int i, Class53_Sub1 class53_sub1, boolean bool, int i_6_, Class53_Sub1 class53_sub1_7_) {
		try {
			if ((i ^ 0xffffffff) == -2) {
				int i_8_ = class53_sub1.anInt429;
				int i_9_ = class53_sub1_7_.anInt429;
				if (!bool) {
					if ((i_9_ ^ 0xffffffff) == 0) {
						i_9_ = 2001;
					}
					if ((i_8_ ^ 0xffffffff) == 0) {
						i_8_ = 2001;
					}
				}
				return i_8_ - i_9_;
			}
			if ((i ^ 0xffffffff) == -3) {
				return Class336.method3772(class53_sub1.method501(i_6_).aString957, class53_sub1_7_.method501(-1).aString957, client.gameLanguage, 1166845806);
			}
			if ((i ^ 0xffffffff) == -4) {
				if (!class53_sub1.aString3630.equals("-")) {
					if (class53_sub1_7_.aString3630.equals("-")) {
						if (bool) {
							return 1;
						}
						return -1;
					}
				} else {
					if (class53_sub1_7_.aString3630.equals("-")) {
						return 0;
					}
					if (bool) {
						return -1;
					}
					return 1;
				}
				return Class336.method3772(class53_sub1.aString3630, class53_sub1_7_.aString3630, client.gameLanguage, 1166845806);
			}
			if ((i ^ 0xffffffff) == -5) {
				if (!class53_sub1.method495((byte) 122)) {
					if (class53_sub1_7_.method495((byte) 127)) {
						return -1;
					}
					return 0;
				}
				if (!class53_sub1_7_.method495((byte) 122)) {
					return 1;
				}
				return 0;
			}
			if (i == 5) {
				if (class53_sub1.method493(9811)) {
					if (class53_sub1_7_.method493(9811)) {
						return 0;
					}
					return 1;
				}
				if (class53_sub1_7_.method493(9811)) {
					return -1;
				}
				return 0;
			}
			if ((i ^ 0xffffffff) == -7) {
				if (class53_sub1.method496((byte) 72)) {
					if (!class53_sub1_7_.method496((byte) 90)) {
						return 1;
					}
					return 0;
				}
				if (class53_sub1_7_.method496((byte) 60)) {
					return -1;
				}
				return 0;
			}
			if (i_6_ != -1) {
				method2700(-89, null, true, -108, null);
			}
			if ((i ^ 0xffffffff) == -8) {
				if (!class53_sub1.method497(false)) {
					if (class53_sub1_7_.method497(false)) {
						return -1;
					}
					return 0;
				}
				if (class53_sub1_7_.method497(false)) {
					return 0;
				}
				return 1;
			}
			if ((i ^ 0xffffffff) == -9) {
				int i_10_ = class53_sub1.anInt3631;
				int i_11_ = class53_sub1_7_.anInt3631;
				do {
					if (!bool) {
						if (i_10_ == -1) {
							i_10_ = 1000;
						}
						if (i_11_ != -1) {
							break;
						}
						i_11_ = 1000;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					if ((i_11_ ^ 0xffffffff) == -1001) {
						i_11_ = -1;
					}
					if (i_10_ == 1000) {
						i_10_ = -1;
					}
				} while (false);
				return -i_11_ + i_10_;
			}
			return class53_sub1.anInt3632 - class53_sub1_7_.anInt3632;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ne.F(" + i + ',' + (class53_sub1 != null ? "{...}" : "null") + ',' + bool + ',' + i_6_ + ',' + (class53_sub1_7_ != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method2701(int i, int i_12_, RSToolkit toolkit, int i_13_, byte i_14_, int i_15_) {
		do {
			try {
				toolkit.setClip(i, i_13_, i + i_12_, i_15_ + i_13_);
				toolkit.drawPlayerSquareDot(i_12_, i_15_, i_13_, -16777216, (byte) -66, i);
				if ((QuickChatCategoryParser.loadingProgress ^ 0xffffffff) <= -101) {
					float f = (float) WorldMap.anInt2084 / (float) WorldMap.anInt2089;
					int i_16_ = i_12_;
					int i_17_ = i_15_;
					if (!(f < 1.0F)) {
						i_16_ = (int) (i_15_ / f);
					} else {
						i_17_ = (int) (i_12_ * f);
					}
					i += (i_12_ - i_16_) / 2;
					i_13_ += (-i_17_ + i_15_) / 2;
					if (IdentikitDefinition.aClass332_1221 == null || IdentikitDefinition.aClass332_1221.getWidth() != i_12_ || i_15_ != IdentikitDefinition.aClass332_1221.getHeight()) {
						WorldMap.method3308(WorldMap.anInt2075, WorldMap.anInt2078 + WorldMap.anInt2084, WorldMap.anInt2089 + WorldMap.anInt2075, WorldMap.anInt2078, i, i_13_, i - -i_16_, i_17_ + i_13_);
						WorldMap.method3309(toolkit);
						IdentikitDefinition.aClass332_1221 = toolkit.method1797(i, i_13_, i_16_, i_17_, false);
					}
					if (i_14_ == -90) {
						IdentikitDefinition.aClass332_1221.draw(i, i_13_);
						int width = i_16_ * RtInterfaceClip.anInt48 / WorldMap.anInt2089;
						int height = Class246_Sub3_Sub5_Sub2.anInt6268 * i_17_ / WorldMap.anInt2084;
						int x = Class166.anInt1279 * i_16_ / WorldMap.anInt2089 + i;
						int y = -height + -(CursorDefinition.anInt1739 * i_17_ / WorldMap.anInt2084) + i_17_ + i_13_;
						int colour = -1996554240;
						if (client.game == GameDefinition.STELLAR_DAWN) {
							colour = -1996488705;
						}
						toolkit.fillRectangle(x, y, width, height, colour, 1);
						toolkit.drawRectangle(x, y, width, height, colour, 0);
						if (GroundBlendingPreferenceField.anInt3711 <= 0) {
							break;
						}
						int i_23_;
						if (Class287.anInt2186 <= 50) {
							i_23_ = 5 * Class287.anInt2186;
						} else {
							i_23_ = -(Class287.anInt2186 * 5) + 500;
						}
						for (Class98_Sub47 class98_sub47 = (Class98_Sub47) WorldMap.aClass148_2065.getFirst(32); class98_sub47 != null; class98_sub47 = (Class98_Sub47) WorldMap.aClass148_2065.getNext(105)) {
							WorldMapInfoDefinition class24 = WorldMap.aClass341_2057.get(117, class98_sub47.anInt4268);
							if (Class87.method855(i_14_ + 215, class24)) {
								if (Class98_Sub5_Sub2.anInt5536 == class98_sub47.anInt4268) {
									int i_24_ = i + i_16_ * class98_sub47.anInt4272 / WorldMap.anInt2089;
									int i_25_ = i_13_ - -((-class98_sub47.anInt4267 + WorldMap.anInt2084) * i_17_ / WorldMap.anInt2084);
									toolkit.drawPlayerSquareDot(4, 4, -2 + i_25_, i_23_ << 2063784920 | 0xffff00, (byte) -66, i_24_ + -2);
								} else if ((Class256.anInt1945 ^ 0xffffffff) != 0 && (Class256.anInt1945 ^ 0xffffffff) == (class24.anInt246 ^ 0xffffffff)) {
									int i_26_ = i_16_ * class98_sub47.anInt4272 / WorldMap.anInt2089 + i;
									int i_27_ = i_17_ * (WorldMap.anInt2084 + -class98_sub47.anInt4267) / WorldMap.anInt2084 + i_13_;
									toolkit.drawPlayerSquareDot(4, 4, -2 + i_27_, 0xffff00 | i_23_ << 1169738168, (byte) -66, -2 + i_26_);
								}
							}
						}
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ne.E(" + i + ',' + i_12_ + ',' + (toolkit != null ? "{...}" : "null") + ',' + i_13_ + ',' + i_14_ + ',' + i_15_ + ')');
			}
			break;
		} while (false);
	}

	private OpenGlToolkit	aHa_Sub1_1546;

	int						anInt1549;

	Class202(OpenGlToolkit var_ha_Sub1, int i, int i_29_) {
		try {
			aHa_Sub1_1546 = var_ha_Sub1;
			anInt1549 = i_29_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ne.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_29_ + ')');
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			aHa_Sub1_1546.method1872(anInt1549, 2834);
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ne.finalize(" + ')');
		}
	}
}

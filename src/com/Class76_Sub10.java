/* Class76_Sub10 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class76_Sub10 extends Class76 {
	public static int	anInt3793;
	public static int	anInt3794;

	public static final char method769(char c, int i, byte i_3_) {
		if (i_3_ > -66) {
			return '|';
		}
		if (c >= 192 && (c ^ 0xffffffff) >= -256) {
			if ((c ^ 0xffffffff) <= -193 && (c ^ 0xffffffff) >= -199) {
				return 'A';
			}
			if (c == 199) {
				return 'C';
			}
			if (c >= 200 && c <= 203) {
				return 'E';
			}
			if ((c ^ 0xffffffff) <= -205 && c <= 207) {
				return 'I';
			}
			if ((c ^ 0xffffffff) <= -211 && (c ^ 0xffffffff) >= -215) {
				return 'O';
			}
			if (c >= 217 && (c ^ 0xffffffff) >= -221) {
				return 'U';
			}
			if ((c ^ 0xffffffff) == -222) {
				return 'Y';
			}
			if ((c ^ 0xffffffff) == -224) {
				return 's';
			}
			if (c >= 224 && c <= 230) {
				return 'a';
			}
			if (c == 231) {
				return 'c';
			}
			if ((c ^ 0xffffffff) <= -233 && c <= 235) {
				return 'e';
			}
			if (c >= 236 && c <= 239) {
				return 'i';
			}
			if ((c ^ 0xffffffff) <= -243 && c <= 246) {
				return 'o';
			}
			if ((c ^ 0xffffffff) <= -250 && (c ^ 0xffffffff) >= -253) {
				return 'u';
			}
			if (c == 253 || (c ^ 0xffffffff) == -256) {
				return 'y';
			}
		}
		if ((c ^ 0xffffffff) == -339) {
			return 'O';
		}
		if ((c ^ 0xffffffff) == -340) {
			return 'o';
		}
		if ((c ^ 0xffffffff) == -377) {
			return 'Y';
		}
		return c;
	}

	public static final void method770(int i) {
		do {
			try {
				int count = Class2.playerCount;
				int[] indices = Class319.playerIndices;
				for (int index = 0; index < count; index++) {
					Player player = Class151_Sub9.players[indices[index]];
					if (player != null && (player.anInt6384 ^ 0xffffffff) < -1) {
						player.anInt6384--;
						if ((player.anInt6384 ^ 0xffffffff) == -1) {
							player.message = null;
						}
					}
				}
				for (int i_6_ = 0; Class150.npcCount > i_6_; i_6_++) {
					long l = Orientation.npcIndices[i_6_];
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(l, -1);
					if (class98_sub39 != null) {
						NPC npc = class98_sub39.npc;
						if (npc.anInt6384 > 0) {
							npc.anInt6384--;
							if ((npc.anInt6384 ^ 0xffffffff) == -1) {
								npc.message = null;
							}
						}
					}
				}
				if (i == -256) {
					break;
				}
				method770(98);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vga.F(" + i + ')');
			}
			break;
		} while (false);
	}

	Class76_Sub10(NativeToolkit var_ha_Sub3) {
		super(var_ha_Sub3);
	}

	@Override
	public final void method739(int i) {
		try {
			if (i != -2) {
				method770(13);
			}
			this.aHa_Sub3_585.method2029((byte) -62, false);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vga.C(" + i + ')');
		}
	}

	@Override
	public final void method742(int i, int i_2_, Interface4 interface4) {
		do {
			try {
				this.aHa_Sub3_585.method2005(interface4, 63);
				this.aHa_Sub3_585.method2015(i_2_, (byte) -118);
				if (i == 6) {
					break;
				}
				method742(-90, 37, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vga.I(" + i + ',' + i_2_ + ',' + (interface4 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method743(int i, boolean bool) {
		do {
			try {
				if (i >= 93) {
					break;
				}
				anInt3794 = 111;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vga.D(" + i + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final boolean method745(byte i) {
		try {
			return i == 27;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vga.H(" + i + ')');
		}
	}

	@Override
	public final void method746(int i, int i_0_, int i_1_) {
		try {
			if (i_1_ >= -75) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vga.E(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	@Override
	public final void method748(int i, boolean bool) {
		try {
			if (i != 69) {
				method739(77);
			}
			this.aHa_Sub3_585.method2029((byte) -93, true);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vga.B(" + i + ',' + bool + ')');
		}
	}
}

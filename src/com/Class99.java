/* Class99 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.MemoryCacheNodeFactory;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.quickchat.QuickChatCategory;

public final class Class99 {
	public static boolean aBoolean838 = false;

	public static final boolean method1686(int i, int i_0_, boolean bool) {
		try {
			if (bool != false) {
				aBoolean838 = true;
			}
			return !(!(MemoryCacheNodeFactory.method2725(32768, i, i_0_) | (0x800 & i) != 0) && !GamePreferences.method1292(i_0_, (byte) 121, i));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ge.A(" + i + ',' + i_0_ + ',' + bool + ')');
		}
	}

	public static final void method1687(Class246_Sub3_Sub4 class246_sub3_sub4, boolean bool) {
		for (int i = class246_sub3_sub4.aShort6158; i <= class246_sub3_sub4.aShort6160; i++) {
			for (int i_1_ = class246_sub3_sub4.aShort6157; i_1_ <= class246_sub3_sub4.aShort6159; i_1_++) {
				Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[class246_sub3_sub4.plane][i][i_1_];
				if (class172 != null) {
					Class154 class154 = class172.aClass154_1325;
					Class154 class154_2_ = null;
					for (/**/; class154 != null; class154 = class154.aClass154_1233) {
						if (class154.aClass246_Sub3_Sub4_1232 == class246_sub3_sub4) {
							if (class154_2_ != null) {
								class154_2_.aClass154_1233 = class154.aClass154_1233;
							} else {
								class172.aClass154_1325 = class154.aClass154_1233;
							}
							class154.method2491(2);
							break;
						}
						class154_2_ = class154;
					}
				}
			}
		}
		if (!bool) {
			PacketBufferManager.method2227(class246_sub3_sub4);
		}
	}
}

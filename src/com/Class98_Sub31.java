/* Class98_Sub31 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;

public abstract class Class98_Sub31 extends Node {
	volatile boolean	aBoolean4102	= true;
	Class98_Sub24		aClass98_Sub24_4104;
	Class98_Sub31		aClass98_Sub31_4101;
	int					anInt4103;

	public Class98_Sub31() {
		/* empty */
	}

	public abstract void method1321(int i);

	abstract Class98_Sub31 method1322();

	int method1323() {
		return 255;
	}

	public final void method1324(int[] is, int i, int i_0_) {
		if (aBoolean4102) {
			method1325(is, i, i_0_);
		} else {
			method1321(i_0_);
		}
	}

	public abstract void method1325(int[] is, int i, int i_1_);

	abstract int method1326();

	abstract Class98_Sub31 method1327();
}

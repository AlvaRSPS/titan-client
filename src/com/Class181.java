/* Class181 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.MemoryCacheNodeFactory;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.core.crypto.ISAACPseudoRNG;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.definition.parser.IdentikitDefinitionParser;
import com.jagex.game.client.definition.parser.NPCDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.client.preferences.AntialiasPreferenceField;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;
import com.jagex.game.client.ui.loading.impl.AwtLoadingScreen;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.model.NativeModelRenderer;
import com.jagex.game.toolkit.shadow.OpenGlShadow;

public final class Class181 {
	public static LinkedList	aClass148_1430	= new LinkedList();
	public static int			anInt1432		= 0;

	public static final void method2607(byte i) {
		do {
			try {
				RenderAnimDefinitionParser.method3201((byte) 65, false);
				Class142.anInt1160 = 0;
				boolean bool = true;
				for (int i_0_ = 0; (client.unknown.length ^ 0xffffffff) < (i_0_ ^ 0xffffffff); i_0_++) {
					if ((Class287.regionTerrainId[i_0_] ^ 0xffffffff) != 0 && client.unknown[i_0_] == null) {
						client.unknown[i_0_] = Class234.mapsJs5.getFile(0, Class287.regionTerrainId[i_0_], false);
						if (client.unknown[i_0_] == null) {
							Class142.anInt1160++;
							bool = false;
						}
					}
					if ((Class98_Sub36.regionObjectMapId[i_0_] ^ 0xffffffff) != 0 && Class255.aByteArrayArray3211[i_0_] == null) {
						Class255.aByteArrayArray3211[i_0_] = Class234.mapsJs5.getFile(Class98_Sub46_Sub17.regionXteaKey[i_0_], 5, 0, Class98_Sub36.regionObjectMapId[i_0_]);
						if (Class255.aByteArrayArray3211[i_0_] == null) {
							bool = false;
							Class142.anInt1160++;
						}
					}
					if ((GameObjectDefinitionParser.regionUwTerrainId[i_0_] ^ 0xffffffff) != 0 && Player.aByteArrayArray6533[i_0_] == null) {
						Player.aByteArrayArray6533[i_0_] = Class234.mapsJs5.getFile(0, GameObjectDefinitionParser.regionUwTerrainId[i_0_], false);
						if (Player.aByteArrayArray6533[i_0_] == null) {
							Class142.anInt1160++;
							bool = false;
						}
					}
					if ((HashTable.regionUwObjectMapId[i_0_] ^ 0xffffffff) != 0 && HashTable.aByteArrayArray3182[i_0_] == null) {
						HashTable.aByteArrayArray3182[i_0_] = Class234.mapsJs5.getFile(0, HashTable.regionUwObjectMapId[i_0_], false);
						if (HashTable.aByteArrayArray3182[i_0_] == null) {
							Class142.anInt1160++;
							bool = false;
						}
					}
					if (Class76_Sub7.anIntArray3765 != null && Class105.regionNpcMapData[i_0_] == null && Class76_Sub7.anIntArray3765[i_0_] != -1) {
						Class105.regionNpcMapData[i_0_] = Class234.mapsJs5.getFile(Class98_Sub46_Sub17.regionXteaKey[i_0_], 5, 0, Class76_Sub7.anIntArray3765[i_0_]);
						if (Class105.regionNpcMapData[i_0_] == null) {
							Class142.anInt1160++;
							bool = false;
						}
					}
				}
				if (AntialiasPreferenceField.aClass370_3707 == null) {
					if (NodeShort.aClass98_Sub46_Sub10_4195 != null && RenderAnimDefinitionParser.worldMapJs5.method2728(NodeShort.aClass98_Sub46_Sub10_4195.aString6017 + "_staticelements", 0)) {
						if (RenderAnimDefinitionParser.worldMapJs5.isGroupCached(NodeShort.aClass98_Sub46_Sub10_4195.aString6017 + "_staticelements", 0)) {
							AntialiasPreferenceField.aClass370_3707 = Class370.method491(113, AdvancedMemoryCache.isMembersOnly, RenderAnimDefinitionParser.worldMapJs5, NodeShort.aClass98_Sub46_Sub10_4195.aString6017 + "_staticelements");
						} else {
							Class142.anInt1160++;
							bool = false;
						}
					} else {
						AntialiasPreferenceField.aClass370_3707 = new Class370(0);
					}
				}
				if (!bool) {
					Class130.anInt1031 = 1;
				} else {
					bool = true;
					VarClientDefinitionParser.anInt1043 = 0;
					for (int i_1_ = 0; (client.unknown.length ^ 0xffffffff) < (i_1_ ^ 0xffffffff); i_1_++) {
						byte[] is = Class255.aByteArrayArray3211[i_1_];
						if (is != null) {
							int i_2_ = -Class272.gameSceneBaseX + (HitmarksDefinitionParser.regionPositionHash[i_1_] >> 1965275688) * 64;
							int i_3_ = (0xff & HitmarksDefinitionParser.regionPositionHash[i_1_]) * 64 - aa_Sub2.gameSceneBaseY;
							if (Class151_Sub9.anInt5028 != 0) {
								i_3_ = 10;
								i_2_ = 10;
							}
							bool &= StreamHandler.method2205(i_3_, is, i_2_, Class165.mapWidth, Class98_Sub10_Sub7.mapLength, 107);
						}
						is = HashTable.aByteArrayArray3182[i_1_];
						if (is != null) {
							int i_4_ = 64 * (HitmarksDefinitionParser.regionPositionHash[i_1_] >> -389792216) - Class272.gameSceneBaseX;
							int i_5_ = 64 * (HitmarksDefinitionParser.regionPositionHash[i_1_] & 0xff) + -aa_Sub2.gameSceneBaseY;
							if (Class151_Sub9.anInt5028 != 0) {
								i_4_ = 10;
								i_5_ = 10;
							}
							bool &= StreamHandler.method2205(i_5_, is, i_4_, Class165.mapWidth, Class98_Sub10_Sub7.mapLength, 119);
						}
					}
					if (!bool) {
						Class130.anInt1031 = 2;
					} else {
						if (Class130.anInt1031 != 0) {
							Class246_Sub2.draw(-77, Class98_Sub46_Sub10.p12FullMetrics, Class195.p12Full, true, client.graphicsToolkit, TextResources.LOADING.getText(client.gameLanguage, (byte) 25) + "<br>(100%)");
						}
						Class128.method2224(22696);
						Class98_Sub10_Sub15.method1050((byte) 101);
						NPCDefinitionParser.method3542(50);
						boolean bool_6_ = false;
						if (client.graphicsToolkit.method1747() && client.preferences.waterDetail.getValue((byte) 124) == 2) {
							for (int i_7_ = 0; client.unknown.length > i_7_; i_7_++) {
								if (HashTable.aByteArrayArray3182[i_7_] != null || Player.aByteArrayArray6533[i_7_] != null) {
									bool_6_ = true;
									break;
								}
							}
						}
						int i_8_;
						if ((client.preferences.fog.getValue((byte) 125) ^ 0xffffffff) == -2) {
							i_8_ = Class262.anIntArray1961[RSByteBuffer.anInt3994];
						} else {
							i_8_ = Class303.anIntArray2531[RSByteBuffer.anInt3994];
						}
						if (client.graphicsToolkit.method1788()) {
							i_8_++;
						}
						ISAACPseudoRNG.method2164(client.graphicsToolkit, RotatingSpriteLSEConfig.anInt5499, 9, 4, Class165.mapWidth, Class98_Sub10_Sub7.mapLength, i_8_, bool_6_, client.graphicsToolkit.method1822() > 0);
						IdentikitDefinitionParser.method825(Class98_Sub10_Sub14.anInt5614);
						if (Class98_Sub10_Sub14.anInt5614 == 0) {
							Js5.method2746(null);
						} else {
							Js5.method2746(Class69_Sub2.p11Full);
						}
						for (int i_9_ = 0; (i_9_ ^ 0xffffffff) > -5; i_9_++) {
							VarPlayerDefinition.clipMaps[i_9_].method2950((byte) -99);
						}
						TextureMetrics.method2920(-125);
						TextResources.method3614(false, -119);
						Class369.method3954(0);
						Class232.aBoolean1744 = false;
						OpenGlGround.aClass346_5202 = null;
						Class128.method2224(22696);
						System.gc();
						RenderAnimDefinitionParser.method3201((byte) 56, true);
						LoadingScreenElementType.method2145((byte) -46);
						HashTableIterator.sceneryShadows = client.preferences.sceneryShadows.getValue((byte) 124);
						Class202.aBoolean1548 = GameShell.maxHeap >= 96;
						ActionGroup.waterDetail = (client.preferences.waterDetail.getValue((byte) 121) ^ 0xffffffff) == -3;
						AnimationDefinition.lightningDetailLevel = (client.preferences.lightningDetail.getValue((byte) 121) ^ 0xffffffff) == -2;
						LoadingScreenElementType.anInt950 = client.preferences.aClass64_Sub3_4076.getValue((byte) 120) != 1 ? SunDefinitionParser.anInt963 : -1;
						Class319.groundBlendingEnabled = client.preferences.groundBlending.getValue((byte) 125) == 1;
						Class369.texturesEnabled = (client.preferences.textures.getValue((byte) 122) ^ 0xffffffff) == -2;
						NativeModelRenderer.aClass305_Sub1_4952 = new Class305_Sub1(4, Class165.mapWidth, Class98_Sub10_Sub7.mapLength, false);
						if (Class151_Sub9.anInt5028 != 0) {
							MapRegion.method900(11948, client.unknown, NativeModelRenderer.aClass305_Sub1_4952);
						} else {
							Class42.readRegionGroundData(NativeModelRenderer.aClass305_Sub1_4952, 0, client.unknown);
						}
						Player.method3065(Class165.mapWidth >> -1731069852, Class98_Sub10_Sub7.mapLength >> -1481341692, true);
						Class329.method3708(-1);
						if (bool_6_) {
							Class248.method3158(true);
							Class98_Sub31_Sub1.aClass305_Sub1_5816 = new Class305_Sub1(1, Class165.mapWidth, Class98_Sub10_Sub7.mapLength, true);
							if ((Class151_Sub9.anInt5028 ^ 0xffffffff) != -1) {
								MapRegion.method900(11948, Player.aByteArrayArray6533, Class98_Sub31_Sub1.aClass305_Sub1_5816);
								RenderAnimDefinitionParser.method3201((byte) 123, true);
							} else {
								Class42.readRegionGroundData(Class98_Sub31_Sub1.aClass305_Sub1_5816, 0, Player.aByteArrayArray6533);
								RenderAnimDefinitionParser.method3201((byte) 113, true);
							}
							Class98_Sub31_Sub1.aClass305_Sub1_5816.method3577(0, -57, NativeModelRenderer.aClass305_Sub1_4952.heightMap[0]);
							Class98_Sub31_Sub1.aClass305_Sub1_5816.method3579(0, null, client.graphicsToolkit, null);
							Class248.method3158(false);
						}
						NativeModelRenderer.aClass305_Sub1_4952.method3579(0, VarPlayerDefinition.clipMaps, client.graphicsToolkit, bool_6_ ? Class98_Sub31_Sub1.aClass305_Sub1_5816.heightMap : null);
						if ((Class151_Sub9.anInt5028 ^ 0xffffffff) == -1) {
							RenderAnimDefinitionParser.method3201((byte) 50, true);
							Class92.method898(false, Class255.aByteArrayArray3211, NativeModelRenderer.aClass305_Sub1_4952);
							if (Class105.regionNpcMapData != null) {
								Animable.method3005(21378);
							}
						} else {
							RenderAnimDefinitionParser.method3201((byte) 67, true);
							Class48_Sub2.method470(Class255.aByteArrayArray3211, NativeModelRenderer.aClass305_Sub1_4952, -4789);
						}
						Class98_Sub10_Sub15.method1050((byte) 104);
						if ((GameShell.maxHeap ^ 0xffffffff) > -97) {
							MemoryCacheNodeFactory.method2727(79);
						}
						RenderAnimDefinitionParser.method3201((byte) 64, true);
						NativeModelRenderer.aClass305_Sub1_4952.addTilesToRenderer(null, (byte) 114, bool_6_ ? Class81.aSArray618[0] : null, client.graphicsToolkit);
						NativeModelRenderer.aClass305_Sub1_4952.method3589(false, (byte) 105, client.graphicsToolkit);
						RenderAnimDefinitionParser.method3201((byte) 49, true);
						if (bool_6_) {
							Class248.method3158(true);
							RenderAnimDefinitionParser.method3201((byte) 96, true);
							if (Class151_Sub9.anInt5028 == 0) {
								Class92.method898(false, HashTable.aByteArrayArray3182, Class98_Sub31_Sub1.aClass305_Sub1_5816);
							} else {
								Class48_Sub2.method470(HashTable.aByteArrayArray3182, Class98_Sub31_Sub1.aClass305_Sub1_5816, -4789);
							}
							Class98_Sub10_Sub15.method1050((byte) 123);
							RenderAnimDefinitionParser.method3201((byte) 126, true);
							Class98_Sub31_Sub1.aClass305_Sub1_5816.addTilesToRenderer(StrongReferenceMCNode.aSArray6298[0], (byte) -81, null, client.graphicsToolkit);
							Class98_Sub31_Sub1.aClass305_Sub1_5816.method3589(true, (byte) 105, client.graphicsToolkit);
							RenderAnimDefinitionParser.method3201((byte) 47, true);
							Class248.method3158(false);
						}
						OpenGlShadow.method1645(-125);
						int i_10_ = NativeModelRenderer.aClass305_Sub1_4952.anInt5302;
						if (i_10_ > Font.localPlane) {
							i_10_ = Font.localPlane;
						}
						if (Font.localPlane + -1 > i_10_) {
							i_10_ = Font.localPlane + -1;
						}
						if (client.preferences.aClass64_Sub3_4076.getValue((byte) 126) != 0) {
							Class46.method439(0);
						} else {
							Class46.method439(i_10_);
						}
						for (int i_11_ = 0; (i_11_ ^ 0xffffffff) > -5; i_11_++) {
							for (int i_12_ = 0; Class165.mapWidth > i_12_; i_12_++) {
								for (int i_13_ = 0; i_13_ < Class98_Sub10_Sub7.mapLength; i_13_++) {
									Class246_Sub3_Sub2_Sub1.method1437(i_12_, i_11_, (byte) 64, i_13_);
								}
							}
						}
						Js5Client.method2264((byte) -109);
						Class128.method2224(22696);
						Class98_Sub33.method789(125);
						Class98_Sub10_Sub15.method1050((byte) 103);
						Class96.method928((byte) -42);
						if (GameShell.frame != null && aa_Sub1.aClass123_3561 != null && client.clientState == 11) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, aa_Sub3.aClass171_3570, Class331.aClass117_2811);
							class98_sub11.packet.writeInt(1571862888, 1057001181);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if ((Class151_Sub9.anInt5028 ^ 0xffffffff) == -1) {
							int i_14_ = (-(Class165.mapWidth >> 1784299556) + Class160.centreX) / 8;
							int i_15_ = ((Class165.mapWidth >> 554209988) + Class160.centreX) / 8;
							int i_16_ = (-(Class98_Sub10_Sub7.mapLength >> -69835196) + Class275.centreY) / 8;
							int i_17_ = (Class275.centreY - -(Class98_Sub10_Sub7.mapLength >> -1955440636)) / 8;
							for (int i_18_ = i_14_ + -1; (i_18_ ^ 0xffffffff) >= (i_15_ + 1 ^ 0xffffffff); i_18_++) {
								for (int i_19_ = -1 + i_16_; i_19_ <= 1 + i_17_; i_19_++) {
									if (i_14_ > i_18_ || i_15_ < i_18_ || (i_19_ ^ 0xffffffff) > (i_16_ ^ 0xffffffff) || i_19_ > i_17_) {
										Class234.mapsJs5.method2755("m" + i_18_ + "_" + i_19_, -114);
										Class234.mapsJs5.method2755("l" + i_18_ + "_" + i_19_, -127);
									}
								}
							}
						}
						if ((client.clientState ^ 0xffffffff) != -5) {
							if (client.clientState == 8) {
								HashTableIterator.setClientState(7, false);
							} else {
								HashTableIterator.setClientState(10, false);
								if (aa_Sub1.aClass123_3561 != null) {
									OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, AwtLoadingScreen.aClass171_3339, Class331.aClass117_2811);
									Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
								}
							}
						} else {
							HashTableIterator.setClientState(3, false);
						}
						Sound.method3778((byte) -72);
						Class128.method2224(22696);
						FloorOverlayDefinitionParser.method316(false);
						Class358.aBoolean3033 = true;
						if (!Class270.aBoolean2031) {
							break;
						}
						Cacheable.write("Took: " + (-StreamHandler.aLong1011 + TimeTools.getCurrentTime(-47)) + "ms", -80);
						Class270.aBoolean2031 = false;
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mc.F(" + i + ')');
			}
			break;
		} while (false);

	}

	public static final void method2608(int i, Player player, int[] is, int i_20_) {
		do {
			try {
				if (player.anIntArray6373 != null) {
					boolean bool = true;
					for (int index = 0; (index ^ 0xffffffff) > (player.anIntArray6373.length ^ 0xffffffff); index++) {
						if ((player.anIntArray6373[index] ^ 0xffffffff) != (is[index] ^ 0xffffffff)) {
							bool = false;
							break;
						}
					}
					if (bool && (player.anInt6413 ^ 0xffffffff) != 0) {
						AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(player.anInt6413, 16383);
						int i_22_ = class97.anInt819;
						if ((i_22_ ^ 0xffffffff) == -2) {
							player.anInt6361 = 1;
							player.anInt6400 = i;
							player.anInt6366 = 0;
							player.anInt6405 = 0;
							player.anInt6393 = 0;
							if (!player.aBoolean6371) {
								Class349.method3840((byte) -128, player, player.anInt6393, class97);
							}
						}
						if (i_22_ == 2) {
							player.anInt6405 = 0;
						}
					}
				}
				boolean bool = true;
				for (int i_23_ = i_20_; is.length > i_23_; i_23_++) {
					if ((is[i_23_] ^ 0xffffffff) != 0) {
						bool = false;
					}
					if (player.anIntArray6373 == null || (player.anIntArray6373[i_23_] ^ 0xffffffff) == 0 || (Class151_Sub7.animationDefinitionList.method2623(is[i_23_], 16383).anInt829 ^ 0xffffffff) <= (Class151_Sub7.animationDefinitionList.method2623(
							player.anIntArray6373[i_23_], 16383).anInt829 ^ 0xffffffff)) {
						player.anInt6400 = i;
						player.anIntArray6373 = is;
						break;
					}
				}
				if (!bool) {
					break;
				}
				player.anIntArray6373 = is;
				player.anInt6400 = i;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mc.D(" + i + ',' + (player != null ? "{...}" : "null") + ',' + (is != null ? "{...}" : "null") + ',' + i_20_ + ')');
			}
			break;
		} while (false);
	}

	public static final void method2610(boolean bool, boolean bool_26_, int i) {
		ClientInventory class98_sub3 = WaterDetailPreferenceField.get(i, bool_26_, 6);
		if (bool == true && class98_sub3 != null) {
			class98_sub3.unlink(111);
		}
	}

	public int	anInt1425	= 128;
	public int	anInt1426;

	public int	anInt1427;

	public int	anInt1428;

	public int	anInt1429	= 128;

	public int	anInt1431;

	public Class181(int i) {
		anInt1428 = i;
	}

	private Class181(int i, int i_27_, int i_28_, int i_29_, int i_30_, int i_31_) {
		anInt1428 = i;
		anInt1425 = i_28_;
		anInt1426 = i_29_;
		anInt1427 = i_30_;
		anInt1431 = i_31_;
		anInt1429 = i_27_;

	}

	public final void method2609(Class181 class181_24_, byte i) {
		anInt1429 = class181_24_.anInt1429;
		anInt1428 = class181_24_.anInt1428;
		anInt1426 = class181_24_.anInt1426;
		anInt1425 = class181_24_.anInt1425;
		anInt1431 = class181_24_.anInt1431;
		anInt1427 = class181_24_.anInt1427;
	}

	public final Class181 method2611(int i) {
		return new Class181(anInt1428, anInt1429, anInt1425, anInt1426, anInt1427, anInt1431);

	}
}

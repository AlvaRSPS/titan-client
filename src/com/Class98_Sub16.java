/* Class98_Sub16 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class Class98_Sub16 extends Node {
	public static int[]	anIntArray3928	= new int[32];
	public static int[]	anIntArray3933	= new int[4];
	public static int	wallsOccludedCount;

	public static void method1146(boolean bool) {
		do {
			try {
				anIntArray3933 = null;
				anIntArray3928 = null;
				if (bool == false) {
					break;
				}
				anIntArray3933 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "hea.D(" + bool + ')');
			}
			break;
		} while (false);
	}

	public static final void method1147(int i, int i_0_) {
		try {
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_0_, -126, 6);
			class98_sub46_sub17.method1621(0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hea.E(" + i + ',' + i_0_ + ')');
		}
	}

	public static final int method1149(boolean bool, FloorOverlayDefinition class199, RSToolkit var_ha) {
		try {
			if (bool != false) {
				anIntArray3933 = null;
			}
			if (class199.blendColour != -1) {
				return class199.blendColour;
			}
			if (class199.textureId != -1) {
				TextureMetrics class238 = var_ha.metricsList.getInfo(class199.textureId, -28755);
				if (!class238.aBoolean1825) {
					return class238.colour;
				}
			}
			return class199.colourHsl;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hea.B(" + bool + ',' + (class199 != null ? "{...}" : "null") + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	public static final void setWindowMode(int screenSize, int i_2_, int i_3_, int i_4_, boolean bool) {
		Class230.method2871(i_3_ + -117);
		Class98_Sub10_Sub25.aLong5677 = 0L;
		int windowMode = OpenGlModelRenderer.getWindowMode((byte) -68);
		if (screenSize == i_3_ || (windowMode ^ 0xffffffff) == -4) {
			bool = true;
		}
		if (!client.graphicsToolkit.method1800()) {
			bool = true;
		}
		AnimatedProgressBarLSEConfig.method909(screenSize, i_3_ + -29761, bool, windowMode, i_2_, i_4_);
	}

	Class89				aClass89_3935;
	Class98_Sub24_Sub1	aClass98_Sub24_Sub1_3934;
	Class98_Sub31_Sub5	aClass98_Sub31_Sub5_3939;
	Class98_Sub44		aClass98_Sub44_3918;
	int					anInt3919;
	int					anInt3920;
	int					anInt3921;
	int					anInt3922;
	int					anInt3923;
	int					anInt3924;
	int					anInt3925;
	int					anInt3926;
	int					anInt3929;
	int					anInt3930;
	int					anInt3931;
	int					anInt3932;
	int					anInt3936;

	int					anInt3937;

	int					anInt3938;

	int					anInt3940;

	int					anInt3941;

	public final void method1148(int i) {
		do {
			try {
				aClass98_Sub31_Sub5_3939 = null;
				aClass98_Sub44_3918 = null;
				aClass98_Sub24_Sub1_3934 = null;
				aClass89_3935 = null;
				if (i == -1) {
					break;
				}
				method1146(false);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "hea.A(" + i + ')');
			}
			break;
		} while (false);
	}
}

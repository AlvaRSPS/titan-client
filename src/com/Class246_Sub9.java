/* Class246_Sub9 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteLSEConfig;

public final class Class246_Sub9 extends SceneGraphNode {
	public static AdvancedMemoryCache	billboardLoader;
	public static int[][]				anIntArrayArray5141	= { { 0, 2, 4, 6 }, { 6, 0, 2, 3, 5, 3 }, { 6, 0, 2, 4 }, { 2, 5, 6, 1 }, { 0, 2, 6 }, { 6, 0, 2 }, { 5, 6, 0, 1, 2, 4 }, { 7, 7, 1, 2, 4, 6 }, { 2, 4, 4, 7 }, { 6, 6, 4, 0, 1, 1, 3, 3 }, { 0, 2, 2, 6, 6, 4 }, { 0, 2, 2, 3, 7, 0, 4, 3 }, {
			0, 2, 4, 6 } };

	static {
		billboardLoader = new AdvancedMemoryCache(64);
	}

	public static final RtInterface getDynamicComponent(byte i, int dwordId, int idx) {
		RtInterface comp = RtInterface.getInterface(dwordId, -9820);
		if (idx == -1) {
			return comp;
		}
		if (comp == null || comp.dynamicComponents == null || (idx ^ 0xffffffff) <= (comp.dynamicComponents.length ^ 0xffffffff)) {
			return null;
		}
		return comp.dynamicComponents[idx];
	}

	public static final Class42_Sub1_Sub1 method3136(OpenGlToolkit var_ha_Sub1, boolean bool, int[] is, int i, byte i_46_, int i_47_, int i_48_, int i_49_) {
		try {
			if (i_46_ != 120) {
				return null;
			}
			if (!var_ha_Sub1.aBoolean4426 && (!Class81.method815(i, i_46_ + -120) || !Class81.method815(i_49_, i_46_ + -120))) {
				if (var_ha_Sub1.haveArbTextureRectangle) {
					return new Class42_Sub1_Sub1(var_ha_Sub1, 34037, i, i_49_, bool, is, i_47_, i_48_);
				}
				return new Class42_Sub1_Sub1(var_ha_Sub1, i, i_49_, Class48.findNextGreaterPwr2(423660257, i), Class48.findNextGreaterPwr2(423660257, i_49_), is);
			}
			return new Class42_Sub1_Sub1(var_ha_Sub1, 3553, i, i_49_, bool, is, i_47_, i_48_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qba.B(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + bool + ',' + (is != null ? "{...}" : "null") + ',' + i + ',' + i_46_ + ',' + i_47_ + ',' + i_48_ + ',' + i_49_ + ')');
		}
	}

	public static void method3137(int i) {
		try {
			if (i == -6086) {
				billboardLoader = null;
				anIntArrayArray5141 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qba.A(" + i + ')');
		}
	}

	boolean				aBoolean5139	= false;
	private boolean		aBoolean5150;
	Class216			aClass216_5136;
	private Class216	aClass216_5142;
	SceneGraphNodeList	aClass218_5140;
	Class246_Sub5		aClass246_Sub5_5133;
	Class87				aClass87_5131;
	Class92				aClass92_5132;
	private long		aLong5134;
	int					anInt5135;
	private int			anInt5137;
	private int			anInt5143;
	private int			anInt5144;
	private int			anInt5145;

	private int			anInt5146;

	private int			anInt5147;

	private int			anInt5148;

	private int			anInt5149;

	Class246_Sub9(RSToolkit var_ha, Class87 class87, Class246_Sub5 class246_sub5, long l) {
		anInt5137 = 0;
		aClass216_5136 = new Class216();
		aClass216_5142 = new Class216();
		aBoolean5150 = false;
		try {
			aClass246_Sub5_5133 = class246_sub5;
			aClass87_5131 = class87;
			aLong5134 = l;
			aClass92_5132 = aClass87_5131.method856(0);
			if (!var_ha.method1780() && aClass92_5132.anInt764 != -1) {
				aClass92_5132 = ItemDeque.method1520(aClass92_5132.anInt764, 14883);
			}
			aClass218_5140 = new SceneGraphNodeList();
			anInt5137 += 64.0 * Math.random();
			method3138(-1);
			aClass216_5142.anInt1620 = aClass216_5136.anInt1620;
			aClass216_5142.anInt1619 = aClass216_5136.anInt1619;
			aClass216_5142.anInt1629 = aClass216_5136.anInt1629;
			aClass216_5142.anInt1625 = aClass216_5136.anInt1625;
			aClass216_5142.anInt1627 = aClass216_5136.anInt1627;
			aClass216_5142.anInt1623 = aClass216_5136.anInt1623;
			aClass216_5142.anInt1624 = aClass216_5136.anInt1624;
			aClass216_5142.anInt1626 = aClass216_5136.anInt1626;
			aClass216_5142.anInt1628 = aClass216_5136.anInt1628;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qba.<init>(" + (var_ha != null ? "{...}" : "null") + ',' + (class87 != null ? "{...}" : "null") + ',' + (class246_sub5 != null ? "{...}" : "null") + ',' + l + ')');
		}
	}

	public final void method3134(long l, RSToolkit var_ha, byte i) {
		try {
			for (Class246_Sub4_Sub2_Sub1 class246_sub4_sub2_sub1 = (Class246_Sub4_Sub2_Sub1) aClass218_5140.getFirst((byte) 15); class246_sub4_sub2_sub1 != null; class246_sub4_sub2_sub1 = (Class246_Sub4_Sub2_Sub1) aClass218_5140.getNext(false)) {
				class246_sub4_sub2_sub1.method3111(var_ha, l);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qba.C(" + l + ',' + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final void method3135(RSToolkit var_ha, boolean bool, long l, int i, int i_1_) {
		try {
			if (!aBoolean5139) {
				if (aClass92_5132.anInt752 <= RotatingSpriteLSEConfig.anInt5497) {
					if (SpriteLSEConfig.anIntArray3536[RotatingSpriteLSEConfig.anInt5497] < Class237_Sub1.anInt5047) {
						bool = false;
					} else if (aBoolean5150) {
						bool = false;
					} else if ((aClass92_5132.anInt762 ^ 0xffffffff) != 0) {
						int i_2_ = (int) (-aLong5134 + l);
						if (!aClass92_5132.aBoolean783 && (i_2_ ^ 0xffffffff) < (aClass92_5132.anInt762 ^ 0xffffffff)) {
							bool = false;
						} else {
							i_2_ %= aClass92_5132.anInt762;
						}
						if (!aClass92_5132.aBoolean732 && i_2_ < aClass92_5132.anInt746) {
							bool = false;
						}
						if (aClass92_5132.aBoolean732 && (aClass92_5132.anInt746 ^ 0xffffffff) >= (i_2_ ^ 0xffffffff)) {
							bool = false;
						}
					}
				} else {
					bool = false;
				}
			} else {
				bool = false;
			}
			if (bool) {
				ParticleManager.emitterCount++;
				int i_3_ = (aClass216_5136.anInt1619 + aClass216_5136.anInt1624 - -aClass216_5136.anInt1623) / 3;
				int i_4_ = (aClass216_5136.anInt1620 + aClass216_5136.anInt1627 - -aClass216_5136.anInt1625) / 3;
				int i_5_ = (aClass216_5136.anInt1626 + aClass216_5136.anInt1629 - -aClass216_5136.anInt1628) / 3;
				if ((i_3_ ^ 0xffffffff) != (aClass216_5136.anInt1617 ^ 0xffffffff) || i_4_ != aClass216_5136.anInt1618 || aClass216_5136.anInt1621 != i_5_) {
					aClass216_5136.anInt1617 = i_3_;
					aClass216_5136.anInt1618 = i_4_;
					aClass216_5136.anInt1621 = i_5_;
					int i_6_ = aClass216_5136.anInt1624 - aClass216_5136.anInt1619;
					int i_7_ = aClass216_5136.anInt1627 + -aClass216_5136.anInt1620;
					int i_8_ = aClass216_5136.anInt1628 + -aClass216_5136.anInt1629;
					int i_9_ = -aClass216_5136.anInt1619 + aClass216_5136.anInt1623;
					int i_10_ = -aClass216_5136.anInt1620 + aClass216_5136.anInt1625;
					int i_11_ = -aClass216_5136.anInt1629 + aClass216_5136.anInt1626;
					anInt5144 = -(i_9_ * i_7_) + i_10_ * i_6_;
					anInt5146 = -(i_8_ * i_10_) + i_7_ * i_11_;
					for (anInt5143 = i_8_ * i_9_ - i_6_ * i_11_; (anInt5146 ^ 0xffffffff) < -32768 || (anInt5143 ^ 0xffffffff) < -32768 || anInt5144 > 32767 || (anInt5146 ^ 0xffffffff) > 32766 || (anInt5143 ^ 0xffffffff) > 32766 || anInt5144 < -32767; anInt5143 >>= 1) {
						anInt5144 >>= 1;
						anInt5146 >>= 1;
					}
					int i_12_ = (int) Math.sqrt(anInt5144 * anInt5144 + anInt5146 * anInt5146 - -(anInt5143 * anInt5143));
					if ((i_12_ ^ 0xffffffff) >= -1) {
						i_12_ = 1;
					}
					anInt5144 = 32767 * anInt5144 / i_12_;
					anInt5143 = anInt5143 * 32767 / i_12_;
					anInt5146 = anInt5146 * 32767 / i_12_;
					if (aClass92_5132.aShort786 > 0 || aClass92_5132.aShort754 > 0) {
						int i_13_ = (int) (2607.5945876176133 * Math.atan2(anInt5144, anInt5146));
						int i_14_ = (int) (Math.atan2(anInt5143, Math.sqrt(anInt5144 * anInt5144 + anInt5146 * anInt5146)) * 2607.5945876176133);
						anInt5148 = -aClass92_5132.aShort747 + aClass92_5132.aShort786;
						anInt5149 = i_13_ - -aClass92_5132.aShort747 + -(anInt5148 >> 1768988289);
						anInt5145 = aClass92_5132.aShort754 - aClass92_5132.aShort763;
						anInt5147 = aClass92_5132.aShort763 + (i_14_ + -(anInt5145 >> 981292577));
					}
				}
				anInt5137 += (int) (i_1_ * (Math.random() * (aClass92_5132.anInt790 + -aClass92_5132.anInt750) + aClass92_5132.anInt750));
				if ((anInt5137 ^ 0xffffffff) < -64) {
					int i_15_ = anInt5137 >> 759811430;
					anInt5137 &= 0x3f;
					for (int i_16_ = 0; (i_16_ ^ 0xffffffff) > (i_15_ ^ 0xffffffff); i_16_++) {
						int i_17_;
						int i_18_;
						int i_19_;
						if (aClass92_5132.aShort786 > 0 || aClass92_5132.aShort754 > 0) {
							int i_20_ = (int) (Math.random() * anInt5148) + anInt5149;
							i_20_ &= 0x3fff;
							int i_21_ = Class284_Sub2_Sub2.SINE[i_20_];
							int i_22_ = Class284_Sub2_Sub2.COSINE[i_20_];
							int i_23_ = (int) (anInt5145 * Math.random()) + anInt5147;
							i_23_ &= 0x1fff;
							int i_24_ = Class284_Sub2_Sub2.SINE[i_23_];
							int i_25_ = Class284_Sub2_Sub2.COSINE[i_23_];
							int i_26_ = 13;
							i_17_ = i_22_ * i_24_ >> i_26_;
							i_18_ = (i_25_ << 515344801) * -1;
							i_19_ = i_21_ * i_24_ >> i_26_;
						} else {
							i_17_ = anInt5146;
							i_18_ = anInt5143;
							i_19_ = anInt5144;
						}
						float f = (float) Math.random();
						float f_27_ = (float) Math.random();
						if (f + f_27_ > 1.0F) {
							f = 1.0F - f;
							f_27_ = -f_27_ + 1.0F;
						}
						float f_28_ = 1.0F - (f_27_ + f);
						int i_29_ = (int) (aClass216_5136.anInt1624 * f_27_ + aClass216_5136.anInt1619 * f + aClass216_5136.anInt1623 * f_28_);
						int i_30_ = (int) (f_28_ * aClass216_5136.anInt1625 + (aClass216_5136.anInt1620 * f + f_27_ * aClass216_5136.anInt1627));
						int i_31_ = (int) (aClass216_5136.anInt1626 * f_28_ + (aClass216_5136.anInt1629 * f + aClass216_5136.anInt1628 * f_27_));
						int i_32_ = (int) (aClass216_5142.anInt1619 * f + f_27_ * aClass216_5142.anInt1624 + f_28_ * aClass216_5142.anInt1623);
						int i_33_ = (int) (f_28_ * aClass216_5142.anInt1625 + (aClass216_5142.anInt1627 * f_27_ + aClass216_5142.anInt1620 * f));
						int i_34_ = (int) (f_27_ * aClass216_5142.anInt1628 + f * aClass216_5142.anInt1629 + aClass216_5142.anInt1626 * f_28_);
						int i_35_ = -i_32_ + i_29_;
						int i_36_ = -i_33_ + i_30_;
						int i_37_ = i_31_ + -i_34_;
						int i_38_ = (int) (i_35_ * Math.random() + i_32_);
						int i_39_ = (int) (i_33_ + i_36_ * Math.random());
						int i_40_ = (int) (i_34_ + Math.random() * i_37_);
						int i_41_ = (int) (Math.random() * (-aClass92_5132.anInt770 + aClass92_5132.anInt731)) + aClass92_5132.anInt770;
						int i_42_ = (int) (Math.random() * (aClass92_5132.anInt787 - aClass92_5132.anInt766)) + aClass92_5132.anInt766;
						int i_43_ = aClass92_5132.anInt780 - -(int) (Math.random() * (aClass92_5132.anInt788 - aClass92_5132.anInt780));
						int i_44_;
						if (!aClass92_5132.aBoolean759) {
							i_44_ = (int) (Math.random() * aClass92_5132.anInt765 + aClass92_5132.anInt756) << -1210811208 | (int) (aClass92_5132.anInt741 + Math.random() * aClass92_5132.anInt730) << -752155600 | (int) (Math.random() * aClass92_5132.anInt734 + aClass92_5132.anInt757) << 891035144
									| (int) (aClass92_5132.anInt771 + aClass92_5132.anInt737 * Math.random());
						} else {
							double d = Math.random();
							i_44_ = (int) (aClass92_5132.anInt741 + d * aClass92_5132.anInt730) << 1742099248 | (int) (aClass92_5132.anInt734 * d + aClass92_5132.anInt757) << -1937900184 | (int) (d * aClass92_5132.anInt737 + aClass92_5132.anInt771) | (int) (Math.random() * aClass92_5132.anInt765
									+ aClass92_5132.anInt756) << -1050005608;
						}
						int i_45_ = aClass92_5132.anInt729;
						if (!var_ha.method1780() && !aClass92_5132.aBoolean791) {
							i_45_ = -1;
						}
						if ((Class351.anInt2922 ^ 0xffffffff) != (ClientStream.anInt3089 ^ 0xffffffff)) {
							Class246_Sub4_Sub2_Sub1 class246_sub4_sub2_sub1 = Class185.aClass246_Sub4_Sub2_Sub1Array1445[Class351.anInt2922];
							Class351.anInt2922 = Class351.anInt2922 + 1 & 0x3ff;
							class246_sub4_sub2_sub1.method3112(this, i_38_, i_39_, i_40_, i_17_, i_18_, i_19_, i_41_, i_42_, i_44_, i_43_, i_45_, aClass92_5132.aBoolean753, aClass92_5132.aBoolean778);
						} else {
							new Class246_Sub4_Sub2_Sub1(this, i_38_, i_39_, i_40_, i_17_, i_18_, i_19_, i_41_, i_42_, i_44_, i_43_, i_45_, aClass92_5132.aBoolean753, aClass92_5132.aBoolean778);
						}
					}
				}
			}
			if (!aClass216_5136.method2795(aClass216_5142, true)) {
				Class216 class216 = aClass216_5142;
				aClass216_5142 = aClass216_5136;
				aClass216_5136 = class216;
				aClass216_5136.anInt1620 = aClass87_5131.anInt668;
				aClass216_5136.anInt1626 = aClass87_5131.anInt662;
				aClass216_5136.anInt1621 = aClass216_5142.anInt1621;
				aClass216_5136.anInt1623 = aClass87_5131.anInt659;
				aClass216_5136.anInt1619 = aClass87_5131.anInt670;
				aClass216_5136.anInt1628 = aClass87_5131.anInt656;
				aClass216_5136.anInt1629 = aClass87_5131.anInt671;
				aClass216_5136.anInt1627 = aClass87_5131.anInt664;
				aClass216_5136.anInt1617 = aClass216_5142.anInt1617;
				aClass216_5136.anInt1625 = aClass87_5131.anInt669;
				aClass216_5136.anInt1618 = aClass216_5142.anInt1618;
				aClass216_5136.anInt1624 = aClass87_5131.anInt663;
			}
			if (i == -64) {
				anInt5135 = 0;
				for (Class246_Sub4_Sub2_Sub1 class246_sub4_sub2_sub1 = (Class246_Sub4_Sub2_Sub1) aClass218_5140.getFirst((byte) 15); class246_sub4_sub2_sub1 != null; class246_sub4_sub2_sub1 = (Class246_Sub4_Sub2_Sub1) aClass218_5140.getNext(false)) {
					class246_sub4_sub2_sub1.method3109(l, i_1_);
					anInt5135++;
				}
				ParticleManager.particleCount += anInt5135;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qba.F(" + (var_ha != null ? "{...}" : "null") + ',' + bool + ',' + l + ',' + i + ',' + i_1_ + ')');
		}
	}

	public final void method3138(int i) {
		do {
			try {
				aClass216_5136.anInt1629 = aClass87_5131.anInt671;
				aClass216_5136.anInt1624 = aClass87_5131.anInt663;
				aClass216_5136.anInt1626 = aClass87_5131.anInt662;
				if (i == -1) {
					aClass216_5136.anInt1620 = aClass87_5131.anInt668;
					aClass216_5136.anInt1619 = aClass87_5131.anInt670;
					aClass216_5136.anInt1627 = aClass87_5131.anInt664;
					aClass216_5136.anInt1625 = aClass87_5131.anInt669;
					aClass216_5136.anInt1623 = aClass87_5131.anInt659;
					aClass216_5136.anInt1628 = aClass87_5131.anInt656;
					if (aClass216_5136.anInt1624 == aClass216_5136.anInt1619 && aClass216_5136.anInt1623 == aClass216_5136.anInt1624 && aClass216_5136.anInt1627 == aClass216_5136.anInt1620 && (aClass216_5136.anInt1627 ^ 0xffffffff) == (aClass216_5136.anInt1625 ^ 0xffffffff)
							&& (aClass216_5136.anInt1628 ^ 0xffffffff) == (aClass216_5136.anInt1629 ^ 0xffffffff) && aClass216_5136.anInt1626 == aClass216_5136.anInt1628) {
						aBoolean5150 = true;
					} else {
						if (!aBoolean5150) {
							break;
						}
						aClass216_5142.anInt1623 = aClass216_5136.anInt1623;
						aClass216_5142.anInt1625 = aClass216_5136.anInt1625;
						aClass216_5142.anInt1624 = aClass216_5136.anInt1624;
						aClass216_5142.anInt1620 = aClass216_5136.anInt1620;
						aClass216_5142.anInt1629 = aClass216_5136.anInt1629;
						aClass216_5142.anInt1627 = aClass216_5136.anInt1627;
						aClass216_5142.anInt1626 = aClass216_5136.anInt1626;
						aClass216_5142.anInt1619 = aClass216_5136.anInt1619;
						aClass216_5142.anInt1628 = aClass216_5136.anInt1628;
						aBoolean5150 = false;
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qba.E(" + i + ')');
			}
			break;
		} while (false);
	}
}

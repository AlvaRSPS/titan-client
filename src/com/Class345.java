/* Class345 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class Class345 {
	public static VarClientDefinitionParser varClientDefinitionList;

	public static final void method3824(String string, int i) {
		if (i == 2 && !string.equals("")) {
			Class39_Sub1.anInt3594++;
			OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i ^ 0x106, Class98_Sub23.aClass171_3998, Class331.aClass117_2811);
			class98_sub11.packet.writeByte(NativeShadow.encodedStringLength(string, (byte) 74), i ^ ~0x40);
			class98_sub11.packet.writePJStr1(string, (byte) 113);
			Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
		}
	}

	public static final int safeLight(int light, int colour, byte dummy) {
		if (-2 == colour) {
			return 12345678;
		}
		if (colour == -1) {
			if (light < 2) {
				light = 2;
			} else if (light > 126) {
				light = 126;
			}

			return light;
		}

		light = light * (colour & 0x7f) >> 7;
		if (light < 2) {
			light = 2;
		} else if (light > 126) {
			light = 126;
		}

		return light + (colour & 0xff80);
	}

	private OpenGlToolkit	aHa_Sub1_2890;

	long					aLong2891;

	Class345(OpenGlToolkit var_ha_Sub1, long l, int i) {
		try {
			aLong2891 = l;
			aHa_Sub1_2890 = var_ha_Sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "v.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + l + ',' + i + ')');
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			aHa_Sub1_2890.method1855(false, aLong2891);
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "v.finalize(" + ')');
		}
	}
}

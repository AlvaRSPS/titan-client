/* Class235 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.toolkit.matrix.JavaMatrix;
import com.jagex.game.toolkit.model.JavaModelRenderer;

public final class JavaThreadResource {
	public static long		aLong1753;
	public static int[]		anIntArray1764	= null;

	public boolean			aBoolean1758;
	public boolean			aBoolean1759;
	public boolean			aBoolean1762;
	public JavaMatrix	aClass111_Sub2_1760;
	public Rasterizer		rasterizer;
	public JavaModelRenderer		aClass146_Sub1_1769;
	public JavaModelRenderer		aClass146_Sub1_1772;
	public JavaModelRenderer		aClass146_Sub1_1774;
	public JavaModelRenderer		aClass146_Sub1_1776;
	public JavaModelRenderer		aClass146_Sub1_1778;
	public JavaModelRenderer		aClass146_Sub1_1787;
	public JavaModelRenderer		aClass146_Sub1_1790;
	public JavaModelRenderer		aClass146_Sub1_1793;
	public JavaModelRenderer		aClass146_Sub1_1798;
	public JavaModelRenderer		aClass146_Sub1_1800;
	public float[]			aFloatArray1799;
	public PureJavaToolkit	toolkit;
	public int				anInt1754;
	public int				anInt1755;
	public int				anInt1757		= 0;
	public int				anInt1761;
	public int				anInt1763;
	public int				anInt1771;
	public int				anInt1779;
	public int				anInt1783;
	public int[]			anIntArray1765;
	public int[]			anIntArray1766;
	public int[]			anIntArray1768;
	public int[]			anIntArray1770;
	public int[]			anIntArray1773;
	public int[]			anIntArray1775;
	public int[]			anIntArray1777;
	public int[]			anIntArray1780;
	public int[]			anIntArray1781;
	public int[]			anIntArray1782;
	public int[]			anIntArray1784;
	public int[]			anIntArray1785;
	public int[]			anIntArray1786;
	public int[]			anIntArray1788;
	public int[]			anIntArray1789;
	public int[]			anIntArray1791;
	public int[]			anIntArray1792;
	public int[]			anIntArray1794;
	public int[]			anIntArray1795;
	public int[]			anIntArray1796;
	public int[]			anIntArray1797;

	Runnable				runnable;

	JavaThreadResource(PureJavaToolkit tk) {
		anInt1755 = 0;
		anInt1754 = 0;
		aBoolean1762 = true;
		anInt1763 = 0;
		aBoolean1759 = false;
		aClass111_Sub2_1760 = new JavaMatrix();
		anIntArray1766 = new int[JavaModelRenderer.anInt4825];
		anIntArray1765 = new int[JavaModelRenderer.anInt4825];
		anIntArray1770 = new int[10];
		anIntArray1781 = new int[64];
		anIntArray1777 = new int[10];
		anIntArray1785 = new int[10000];
		anIntArray1784 = new int[64];
		anIntArray1792 = new int[8];
		anIntArray1780 = new int[JavaModelRenderer.anInt4825];
		anIntArray1768 = new int[10];
		anIntArray1789 = new int[10];
		aFloatArray1799 = new float[2];
		anIntArray1797 = new int[8];
		anIntArray1786 = new int[JavaModelRenderer.anInt4825];
		anIntArray1795 = new int[64];
		anIntArray1782 = new int[10000];
		anIntArray1796 = new int[JavaModelRenderer.anInt4825];
		anIntArray1791 = new int[8];
		anIntArray1794 = new int[JavaModelRenderer.anInt4825];
		anIntArray1788 = new int[64];
		anIntArray1775 = new int[JavaModelRenderer.anInt4825];
		toolkit = tk;
		anInt1761 = -255 + toolkit.anInt4484;
		rasterizer = new Rasterizer(tk, this);
		aClass146_Sub1_1774 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1790 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1778 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1769 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1776 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1800 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1787 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1793 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1798 = new JavaModelRenderer(toolkit);
		aClass146_Sub1_1772 = new JavaModelRenderer(toolkit);
		anIntArray1773 = new int[JavaModelRenderer.anInt4810];
		for (int i = 0; (i ^ 0xffffffff) > (JavaModelRenderer.anInt4810 ^ 0xffffffff); i++) {
			anIntArray1773[i] = -1;
		}
	}

	public final void resetRasterizer(byte dummy) {
		if (dummy < -34) {
			rasterizer = new Rasterizer(toolkit, this);
		}
	}

	public final void setRunnable(byte dummy, Runnable runnable) {
		this.runnable = runnable;
	}
}

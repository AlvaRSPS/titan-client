/* Class98_Sub10_Sub15 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.ProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.matrix.NativeMatrix;

public final class Class98_Sub10_Sub15 extends Class98_Sub10 {
	public static IncomingOpcode aClass58_5615;

	static {
		aClass58_5615 = new IncomingOpcode(40, -1);
		client.affiliateId = 0;
	}

	public static final void method1050(byte i) {
		try {
			FloorOverlayDefinition.floorOverlayDefinitionList.cacheClear((byte) -117);
			Class82.floorUnderDefinitionList.clear((byte) -76);
			ParamDefinition.identikitDefinitionList.method829(114);
			Class130.gameObjectDefinitionList.method3549((byte) 100);
			Class4.npcDefinitionList.method3544((byte) 1);
			Class98_Sub46_Sub19.itemDefinitionList.cacheClear(76);
			Class151_Sub7.animationDefinitionList.method2618(true);
			BuildLocation.gfxDefinitionList.clear((byte) 0);
			DataFs.bConfigDefinitionList.method2684(-4742);
			SpriteLoadingScreenElement.varPlayerDefinitionList.method2281(-106);
			Class370.renderAnimDefinitionList.method3200((byte) 83);
			Class98_Sub10_Sub23.mapScenesDefinitionList.method3770(34);
			Class216.worldMapInfoDefinitionList.method3808(0);
			Class303.questDefinitionList.clear((byte) 68);
			Class98_Sub43_Sub1.paramDefinitionList.method3938(101);
			SimpleProgressBarLoadingScreenElement.skyboxDefinitionList.method529((byte) 116);
			GrandExchangeOffer.sunDefinitionList.method2152(21185);
			Class21_Sub1.lightIntensityDefinitionList.method3265(1);
			Class18.cursorDefinitionList.method201((byte) 123);
			Class62.structsDefinitionList.method3223((byte) 17);
			Class246_Sub3_Sub1.hitmarksDefinitionList.clear((byte) -53);
			QuickChatMessageParser.method3330(1);
			Class318.method3657((byte) -118);
			Class142.method2310((byte) 91);
			Class82.method822(10157);
			if (client.buildLocation != BuildLocation.LIVE) {
				for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > (Class76.aByteArrayArray590.length ^ 0xffffffff); i_2_++) {
					Class76.aByteArrayArray590[i_2_] = null;
				}
				Class98_Sub48.anInt4281 = 0;
			}
			ProgressBarLoadingScreenElement.method3962((byte) 0);
			Class21_Sub1.method269(true);
			Class98_Sub10_Sub21.method1063((byte) -41);
			Class98_Sub28.method1296((byte) 17);
			Class98_Sub10_Sub2.method1008(-47);
			ClientScript2Runtime.aClass79_1890.clear(16);
			client.graphicsToolkit.method1812();
			JavaNetworkHandler.method2211((byte) -23);
			Class39.method353((byte) -79);
			Class94.animationFramesJs5.discardAllUnpacked((byte) -123);
			Class323.animationFrameBasesJs5.discardAllUnpacked((byte) -117);
			client.miscellaneousJs5.discardAllUnpacked((byte) -126);
			TexturesPreferenceField.interfacesJs5.discardAllUnpacked((byte) -117);
			Class76_Sub2.soundEffectsJs5.discardAllUnpacked((byte) -119);
			Class234.mapsJs5.discardAllUnpacked((byte) -126);
			Class98_Sub10_Sub1.musicJs5.discardAllUnpacked((byte) -120);
			Class76_Sub9.modelsJs5.discardAllUnpacked((byte) -128);
			client.spriteJs5.discardAllUnpacked((byte) -126);
			RSByteBuffer.texturesJs5.discardAllUnpacked((byte) -127);
			NodeShort.huffmanJs5.discardAllUnpacked((byte) -124);
			HashTableIterator.music2Js5.discardAllUnpacked((byte) -124);
			NewsLSEConfig.clientScriptJs5.discardAllUnpacked((byte) -123);
			client.fontJs5.discardAllUnpacked((byte) -124);
			BuildLocation.midiInstrumentsJs5.discardAllUnpacked((byte) -118);
			Class119_Sub2.sound3Js5.discardAllUnpacked((byte) -116);
			Class375.objectsJs5.discardAllUnpacked((byte) -127);
			Class98_Sub10_Sub24.enumsJs5.discardAllUnpacked((byte) -119);
			Class234.npcJs5.discardAllUnpacked((byte) -126);
			Class208.itemsJs5.discardAllUnpacked((byte) -127);
			Char.animationJs5.discardAllUnpacked((byte) -118);
			PlayerUpdateMask.graphicJs5.discardAllUnpacked((byte) -118);
			Class98_Sub46_Sub19.bConfigJs5.discardAllUnpacked((byte) -115);
			RenderAnimDefinitionParser.worldMapJs5.discardAllUnpacked((byte) -119);
			Class81.quickChatMessagesJs5.discardAllUnpacked((byte) -126);
			OpenGlElementBufferPointer.quickChatMenuJs5.discardAllUnpacked((byte) -123);
			TexturesPreferenceField.materialsJs5.discardAllUnpacked((byte) -122);
			Class245.particlesJs5.discardAllUnpacked((byte) -118);
			AsyncCache.defaultsJs5.discardAllUnpacked((byte) -121);
			NativeMatrix.billboardsJs5.discardAllUnpacked((byte) -120);
			Class245.nativesJs5.discardAllUnpacked((byte) -115);
			client.shaderJs5.discardAllUnpacked((byte) -126);
			ParticleManager.vorbisJs5.discardAllUnpacked((byte) -119);
			Class275.aClass79_2046.clear(115);
			Class224_Sub3.aClass79_5039.clear(90);
			Class378.aClass79_3189.clear(43);
			Class98_Sub6.aClass79_3847.clear(116);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "id.D(" + i + ')');
		}
	}

	private int	anInt5616	= 4096;

	private int	anInt5617	= 0;

	public Class98_Sub10_Sub15() {
		super(1, false);
	}

	@Override
	public final int[] method990(int i, int i_15_) {
		try {
			if (i != 255) {
				method997(-53, 122);
			}
			int[] is = this.aClass16_3863.method237((byte) 98, i_15_);
			if (this.aClass16_3863.aBoolean198) {
				int[] is_16_ = method1000(i_15_, 0, 0);
				for (int i_17_ = 0; (i_17_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_17_++) {
					int i_18_ = is_16_[i_17_];
					if (anInt5617 > i_18_) {
						is[i_17_] = anInt5617;
					} else if ((anInt5616 ^ 0xffffffff) > (i_18_ ^ 0xffffffff)) {
						is[i_17_] = anInt5616;
					} else {
						is[i_17_] = i_18_;
					}
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "id.G(" + i + ',' + i_15_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_0_) {
		do {
			try {
				int i_1_ = i;
				while_99_: do {
					do {
						if (i_1_ != 0) {
							if (i_1_ != 1) {
								if (i_1_ == 2) {
									break;
								}
								break while_99_;
							}
						} else {
							anInt5617 = class98_sub22.readShort((byte) 127);
							break while_99_;
						}
						anInt5616 = class98_sub22.readShort((byte) 127);
						break while_99_;
					} while (false);
					this.aBoolean3861 = (class98_sub22.readUnsignedByte((byte) 61) ^ 0xffffffff) == -2;
				} while (false);
				if (i_0_ <= -92) {
					break;
				}
				method991(-80, null, (byte) -62);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "id.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_0_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int[][] method997(int i, int i_3_) {
		try {
			int[][] is = this.aClass223_3859.method2828(i_3_, 0);
			if (i >= -76) {
				method1050((byte) 14);
			}
			if (this.aClass223_3859.aBoolean1683) {
				int[][] is_4_ = method994(i_3_, 24431, 0);
				int[] is_5_ = is_4_[0];
				int[] is_6_ = is_4_[1];
				int[] is_7_ = is_4_[2];
				int[] is_8_ = is[0];
				int[] is_9_ = is[1];
				int[] is_10_ = is[2];
				for (int i_11_ = 0; (Class25.anInt268 ^ 0xffffffff) < (i_11_ ^ 0xffffffff); i_11_++) {
					int i_12_ = is_5_[i_11_];
					int i_13_ = is_6_[i_11_];
					int i_14_ = is_7_[i_11_];
					if ((i_12_ ^ 0xffffffff) <= (anInt5617 ^ 0xffffffff)) {
						if (i_12_ <= anInt5616) {
							is_8_[i_11_] = i_12_;
						} else {
							is_8_[i_11_] = anInt5616;
						}
					} else {
						is_8_[i_11_] = anInt5617;
					}
					if ((anInt5617 ^ 0xffffffff) < (i_13_ ^ 0xffffffff)) {
						is_9_[i_11_] = anInt5617;
					} else if ((anInt5616 ^ 0xffffffff) <= (i_13_ ^ 0xffffffff)) {
						is_9_[i_11_] = i_13_;
					} else {
						is_9_[i_11_] = anInt5616;
					}
					if ((i_14_ ^ 0xffffffff) <= (anInt5617 ^ 0xffffffff)) {
						if (anInt5616 >= i_14_) {
							is_10_[i_11_] = i_14_;
						} else {
							is_10_[i_11_] = anInt5616;
						}
					} else {
						is_10_[i_11_] = anInt5617;
					}
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "id.C(" + i + ',' + i_3_ + ')');
		}
	}
}

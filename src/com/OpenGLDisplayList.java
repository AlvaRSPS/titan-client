
/* Class91 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.AnimationDefinition;

import jaggl.OpenGL;

public final class OpenGLDisplayList {
	public static Class28	aClass28_722;
	public static int		anInt724	= -1;

	public static final void method890(int i, int i_2_, int i_3_, int i_4_, byte i_5_, int i_6_) {
		int i_7_ = -i_2_ + i_3_;
		int i_8_ = -i_6_ + i_4_;
		if (i_8_ == 0) {
			if ((i_7_ ^ 0xffffffff) != -1) {
				Class48_Sub2_Sub1.method471(i_6_, (byte) -123, i, i_2_, i_3_);
			}
		} else if ((i_7_ ^ 0xffffffff) == -1) {
			Class160.method2513((byte) -83, i, i_6_, i_4_, i_2_);
		} else {
			if (i_7_ < 0) {
				i_7_ = -i_7_;
			}
			if ((i_8_ ^ 0xffffffff) > -1) {
				i_8_ = -i_8_;
			}
			boolean bool = (i_8_ ^ 0xffffffff) > (i_7_ ^ 0xffffffff);
			if (bool) {
				int i_9_ = i_6_;
				i_6_ = i_2_;
				int i_10_ = i_4_;
				i_4_ = i_3_;
				i_2_ = i_9_;
				i_3_ = i_10_;
			}
			if ((i_4_ ^ 0xffffffff) > (i_6_ ^ 0xffffffff)) {
				int i_11_ = i_6_;
				int i_12_ = i_2_;
				i_6_ = i_4_;
				i_4_ = i_11_;
				i_2_ = i_3_;
				i_3_ = i_12_;
			}
			if (i_5_ <= -21) {
				int i_13_ = i_2_;
				int i_14_ = i_4_ + -i_6_;
				int i_15_ = i_3_ - i_2_;
				int i_16_ = -(i_14_ >> -1811836063);
				int i_17_ = (i_2_ ^ 0xffffffff) > (i_3_ ^ 0xffffffff) ? 1 : -1;
				if (i_15_ < 0) {
					i_15_ = -i_15_;
				}
				if (!bool) {
					for (int i_18_ = i_6_; i_4_ >= i_18_; i_18_++) {
						i_16_ += i_15_;
						AnimationDefinition.anIntArrayArray814[i_13_][i_18_] = i;
						if ((i_16_ ^ 0xffffffff) < -1) {
							i_13_ += i_17_;
							i_16_ -= i_14_;
						}
					}
				} else {
					for (int i_19_ = i_6_; i_19_ <= i_4_; i_19_++) {
						i_16_ += i_15_;
						AnimationDefinition.anIntArrayArray814[i_19_][i_13_] = i;
						if (i_16_ > 0) {
							i_13_ += i_17_;
							i_16_ -= i_14_;
						}
					}
				}
			}
		}
	}

	private int handle;

	public OpenGLDisplayList(OpenGlToolkit openGLToolkit, int i) {
		handle = OpenGL.glGenLists(i);
	}

	public final void callList(char listId, boolean bool) {
		OpenGL.glCallList(listId + handle);
	}

	public final void endList(byte i) {
		OpenGL.glEndList();
	}

	public final void newList(int listId, int i_1_) {
		if (i_1_ == -30389) {
			OpenGL.glNewList(listId + handle, 4864);
		}
	}
}

/* Class39 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.MemoryCacheNode;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.LightningDetailPreferenceField;
import com.jagex.game.client.preferences.ParticlesPreferenceField;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatMessage;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public abstract class Class39 {
	public static Class126			aClass126_361	= new Class126();
	public static IncomingOpcode	aClass58_364	= new IncomingOpcode(25, 0);
	public static Class85			aClass85_362	= new Class85(14, 8);
	public static int				crossId;

	public static final String method349(int i, int i_0_, int i_1_, long l, boolean bool) {
		try {
			char c = ',';
			char c_2_ = '.';
			if ((i ^ 0xffffffff) == -1) {
				c_2_ = ',';
				c = '.';
			}
			if ((i ^ 0xffffffff) == -3) {
				c_2_ = '\u00a0';
			}
			boolean bool_3_ = false;
			if (i_1_ != 48) {
				return null;
			}
			if (l < 0L) {
				bool_3_ = true;
				l = -l;
			}
			StringBuffer stringbuffer = new StringBuffer(26);
			if ((i_0_ ^ 0xffffffff) < -1) {
				for (int i_4_ = 0; i_4_ < i_0_; i_4_++) {
					int i_5_ = (int) l;
					l /= 10L;
					stringbuffer.append((char) (-((int) l * 10) + 48 - -i_5_));
				}
				stringbuffer.append(c);
			}
			int i_6_ = 0;
			for (;;) {
				int i_7_ = (int) l;
				l /= 10L;
				stringbuffer.append((char) (i_7_ + 48 - 10 * (int) l));
				if ((l ^ 0xffffffffffffffffL) == -1L) {
					break;
				}
				if (bool && (++i_6_ % 3 ^ 0xffffffff) == -1) {
					stringbuffer.append(c_2_);
				}
			}
			if (bool_3_) {
				stringbuffer.append('-');
			}
			return stringbuffer.reverse().toString();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cn.E(" + i + ',' + i_0_ + ',' + i_1_ + ',' + l + ',' + bool + ')');
		}
	}

	public static final void method350(int i, int i_8_, int i_9_, RSToolkit var_ha) {
		try {
			if ((i_9_ ^ 0xffffffff) <= -1 && (i_8_ ^ 0xffffffff) <= -1 && (MemoryCacheNode.anInt5952 ^ 0xffffffff) != -1 && (LoadingScreenSequence.anInt2132 ^ 0xffffffff) != -1) {
				int i_10_;
				int i_11_;
				int i_12_;
				int i_13_;
				Matrix class111;
				int i_14_;
				int i_15_;
				if (OpenGLHeap.aBoolean6079) {
					Class98_Sub46_Sub14.method1604(false, (byte) 88);
					class111 = var_ha.method1752();
					int[] is = var_ha.Y();
					i_10_ = is[3];
					i_12_ = is[0];
					i_13_ = is[2];
					i_11_ = is[1];
					i_14_ = i_9_ - -RsBitsBuffers.method1253(false, i ^ ~0x7b40);
					i_15_ = OpenGLTexture2DSource.method3932(false, (byte) -67) + i_8_;
				} else {
					var_ha.DA(QuickChatMessage.anInt6025, Class98_Sub46_Sub20.anInt6074, MemoryCacheNode.anInt5952, LoadingScreenSequence.anInt2132);
					i_10_ = LoadingScreenSequence.anInt2132;
					i_11_ = Class98_Sub46_Sub20.anInt6074;
					i_12_ = QuickChatMessage.anInt6025;
					i_13_ = MemoryCacheNode.anInt5952;
					var_ha.setClip(Class98_Sub10_Sub17.anInt5623, LightningDetailPreferenceField.anInt3664, MemoryCacheNode.anInt5952, LoadingScreenSequence.anInt2132);
					class111 = var_ha.createMatrix();
					class111.method2093(WaterDetailPreferenceField.anInt3717, Class98_Sub42.anInt4239, Class137.anInt1079, FileOnDisk.anInt3025, Class287_Sub2.anInt3274, ParticlesPreferenceField.anInt3655);
					i_14_ = i_9_;
					var_ha.a(class111);
					i_15_ = i_8_;
				}
				if (i_13_ == 0) {
					i_13_ = 1;
				}
				Minimap.clipPlanes(-546, true);
				if (i_10_ == 0) {
					i_10_ = 1;
				}
				if (StrongReferenceMCNode.aSArray6298 != null && (!Class98_Sub10_Sub9.aBoolean5585 || (Class98_Sub4.anInt3826 & 0x40) != 0)) {
					int i_16_ = -1;
					int i_17_ = -1;
					int i_18_ = var_ha.getNearPlane();
					int i_19_ = var_ha.getFarPlane();
					int i_20_;
					int i_21_;
					int i_22_;
					int i_23_;
					if (VarClientStringsDefinitionParser.aBoolean1839) {
						i_20_ = i_22_ = (-i_12_ + i_14_) * Class16.anInt197 / i_13_;
						i_23_ = i_21_ = Class16.anInt197 * (i_15_ - i_11_) / i_10_;
					} else {
						i_20_ = (-i_12_ + i_14_) * i_18_ / i_13_;
						i_21_ = i_19_ * (i_15_ - i_11_) / i_10_;
						i_22_ = i_19_ * (i_14_ - i_12_) / i_13_;
						i_23_ = (i_15_ + -i_11_) * i_18_ / i_10_;
					}
					int[] is = { i_20_, i_23_, i_18_ };
					int[] is_24_ = { i_22_, i_21_, i_19_ };
					class111.method2108(is);
					class111.method2108(is_24_);
					float f = Js5.method2753(4, 10665, is[2], is_24_[0], is_24_[1], is[1], is_24_[2], is[0]);
					if (f > 0.0F) {
						int i_25_ = is_24_[0] + -is[0];
						int i_26_ = -is[2] + is_24_[2];
						int i_27_ = (int) (i_25_ * f + is[0]);
						int i_28_ = (int) (is[2] + i_26_ * f);
						i_16_ = i_27_ + (Class87.localPlayer.getSize(0) + -1 << 602308552) >> 503614921;
						i_17_ = (-1 + Class87.localPlayer.getSize(i) << 24452936) + i_28_ >> -970214583;
						int i_29_ = Class87.localPlayer.plane;
						if (i_29_ < 3 && (0x2 & Class281.flags[1][i_27_ >> 216058729][i_28_ >> 636707433] ^ 0xffffffff) != -1) {
							i_29_++;
						}
					}
					if ((i_16_ ^ 0xffffffff) != 0 && (i_17_ ^ 0xffffffff) != 0) {
						if (Class98_Sub10_Sub9.aBoolean5585 && (Class98_Sub4.anInt3826 & 0x40) != 0) {
							RtInterface class293 = Class246_Sub9.getDynamicComponent((byte) 72, Class187.anInt1450, Class310.anInt2652);
							if (class293 != null) {
								ActionQueueEntry.addAction(false, true, 0L, Class336.anInt2823, i_16_, " ->", true, i_17_, 46, i_16_ << 937987840 | i_17_, -1, false, Class287_Sub2.aString3272);
							} else {
								Class98_Sub10_Sub32.method1098((byte) 100);
							}
						} else {
							if (QuickChatCategory.aBoolean5943) {
								ActionQueueEntry.addAction(false, true, 0L, -1, i_16_, "", true, i_17_, 60, i_16_ << -896229152 | i_17_, -1, false, TextResources.FACE_HERE.getText(client.gameLanguage, (byte) 25));
							}
							ActionQueueEntry.addAction(false, true, 0L, FloorOverlayDefinition.anInt1541, i_16_, "", true, i_17_, 6, i_16_ << -1708316192 | i_17_, -1, false, SceneGraphNodeList.optionText);
						}
					}
				}
				if (OpenGLHeap.aBoolean6079) {
					Js5.method2765((byte) 119);
				}
				for (int i_30_ = i; i_30_ < (!OpenGLHeap.aBoolean6079 ? 1 : 2); i_30_++) {
					boolean bool = (i_30_ ^ 0xffffffff) == -1;
					Class84 class84 = bool ? Class98_Sub10_Sub27.aClass84_5692 : SunDefinition.aClass84_1988;
					int i_31_ = i_9_;
					int i_32_ = i_8_;
					if (OpenGLHeap.aBoolean6079) {
						Class98_Sub46_Sub14.method1604(bool, (byte) 88);
						i_31_ += RsBitsBuffers.method1253(bool, -31553);
						i_32_ += OpenGLTexture2DSource.method3932(bool, (byte) -67);
					}
					SceneGraphNodeList class218 = class84.aClass218_635;
					for (Class246_Sub1 class246_sub1 = (Class246_Sub1) class218.getFirst((byte) 15); class246_sub1 != null; class246_sub1 = (Class246_Sub1) class218.getNext(false)) {
						if ((Class246_Sub3_Sub5_Sub2.aBoolean6272 || (class246_sub1.aClass246_Sub3_5069.plane ^ 0xffffffff) == (Class87.localPlayer.plane ^ 0xffffffff)) && class246_sub1.method2969(var_ha, i_31_, i_32_, -66)) {
							int i_35_;
							int i_36_;
							if (class246_sub1.aClass246_Sub3_5069 instanceof Class246_Sub3_Sub4) {
								i_35_ = ((Class246_Sub3_Sub4) class246_sub1.aClass246_Sub3_5069).aShort6158;
								i_36_ = ((Class246_Sub3_Sub4) class246_sub1.aClass246_Sub3_5069).aShort6157;
							} else {
								i_35_ = class246_sub1.aClass246_Sub3_5069.boundExtentsX >> -1829008855;
								i_36_ = class246_sub1.aClass246_Sub3_5069.boundExtentsZ >> -277104311;
							}
							if (class246_sub1.aClass246_Sub3_5069 instanceof Player) {
								Player player = (Player) class246_sub1.aClass246_Sub3_5069;
								int i_37_ = player.getSize(i);
								if ((0x1 & i_37_) == 0 && (0x1ff & player.boundExtentsX ^ 0xffffffff) == -1 && (0x1ff & player.boundExtentsZ ^ 0xffffffff) == -1 || (0x1 & i_37_) == 1 && (0x1ff & player.boundExtentsX ^ 0xffffffff) == -257 && (0x1ff
										& player.boundExtentsZ) == 256) {
									int i_38_ = player.boundExtentsX + -(-1 + player.getSize(0) << -982663128);
									int i_39_ = player.boundExtentsZ + -(player.getSize(i) + -1 << -515460824);
									for (int i_40_ = 0; (i_40_ ^ 0xffffffff) > (Class150.npcCount ^ 0xffffffff); i_40_++) {
										NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[i_40_], i + -1);
										if (class98_sub39 != null) {
											NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
											if (class246_sub3_sub4_sub2_sub1.anInt6411 != Queue.timer && class246_sub3_sub4_sub2_sub1.aBoolean6371) {
												int i_41_ = -(-1 + class246_sub3_sub4_sub2_sub1.definition.boundSize << -1071692376) + class246_sub3_sub4_sub2_sub1.boundExtentsX;
												int i_42_ = -(class246_sub3_sub4_sub2_sub1.definition.boundSize - 1 << -1646493944) + class246_sub3_sub4_sub2_sub1.boundExtentsZ;
												if ((i_41_ ^ 0xffffffff) <= (i_38_ ^ 0xffffffff) && (class246_sub3_sub4_sub2_sub1.definition.boundSize ^ 0xffffffff) >= (-(i_41_ - i_38_ >> 1292283049) + player.getSize(i) ^ 0xffffffff) && (i_39_ ^ 0xffffffff) >= (i_42_ ^ 0xffffffff)
														&& (class246_sub3_sub4_sub2_sub1.definition.boundSize ^ 0xffffffff) >= (player.getSize(0) + -(-i_39_ + i_42_ >> -1124961751) ^ 0xffffffff)) {
													Class98_Sub10.method995(class246_sub3_sub4_sub2_sub1, (byte) 55, class246_sub1.aClass246_Sub3_5069.plane != Class87.localPlayer.plane);
													class246_sub3_sub4_sub2_sub1.anInt6411 = Queue.timer;
												}
											}
										}
									}
									int i_43_ = Class2.playerCount;
									int[] is = Class319.playerIndices;
									for (int i_44_ = 0; (i_43_ ^ 0xffffffff) < (i_44_ ^ 0xffffffff); i_44_++) {
										Player player_45_ = Class151_Sub9.players[is[i_44_]];
										if (player_45_ != null && player_45_.anInt6411 != Queue.timer && player_45_ != player && player_45_.aBoolean6371) {
											int i_46_ = player_45_.boundExtentsX + -(-1 + player_45_.getSize(0) << 1331379528);
											int i_47_ = player_45_.boundExtentsZ + -(-1 + player_45_.getSize(0) << -453541336);
											if (i_46_ >= i_38_ && player_45_.getSize(i) <= -(i_46_ + -i_38_ >> -688451639) + player.getSize(0) && (i_39_ ^ 0xffffffff) >= (i_47_ ^ 0xffffffff) && player_45_.getSize(i) <= -(-i_39_ + i_47_ >> 1317624233) + player.getSize(0)) {
												Class98_Sub30.method1311((Class87.localPlayer.plane ^ 0xffffffff) != (class246_sub1.aClass246_Sub3_5069.plane ^ 0xffffffff), true, player_45_);
												player_45_.anInt6411 = Queue.timer;
											}
										}
									}
								}
								if ((player.anInt6411 ^ 0xffffffff) == (Queue.timer ^ 0xffffffff)) {
									continue;
								}
								Class98_Sub30.method1311((Class87.localPlayer.plane ^ 0xffffffff) != (class246_sub1.aClass246_Sub3_5069.plane ^ 0xffffffff), true, player);
								player.anInt6411 = Queue.timer;
							}
							if (class246_sub1.aClass246_Sub3_5069 instanceof NPC) {
								NPC npc = (NPC) class246_sub1.aClass246_Sub3_5069;
								if (npc.definition != null) {
									if ((0x1 & npc.definition.boundSize ^ 0xffffffff) == -1 && (0x1ff & npc.boundExtentsX ^ 0xffffffff) == -1 && (npc.boundExtentsZ & 0x1ff) == 0 || (0x1 & npc.definition.boundSize) == 1 && (npc.boundExtentsX & 0x1ff ^ 0xffffffff) == -257
											&& (npc.boundExtentsZ & 0x1ff) == 256) {
										int i_48_ = -(-1 + npc.definition.boundSize << -685347000) + npc.boundExtentsX;
										int i_49_ = npc.boundExtentsZ + -(npc.definition.boundSize + -1 << 986207144);
										for (int i_50_ = 0; (Class150.npcCount ^ 0xffffffff) < (i_50_ ^ 0xffffffff); i_50_++) {
											NodeObject entry = (NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[i_50_], -1);
											if (entry != null) {
												NPC _npc = entry.npc;
												if (Queue.timer != _npc.anInt6411 && npc != _npc && _npc.aBoolean6371) {
													int i_52_ = _npc.boundExtentsX - (_npc.definition.boundSize + -1 << -1718624696);
													int i_53_ = _npc.boundExtentsZ + -(-1 + _npc.definition.boundSize << -559324184);
													if ((i_48_ ^ 0xffffffff) >= (i_52_ ^ 0xffffffff) && npc.definition.boundSize - (-i_48_ + i_52_ >> -1827274359) >= _npc.definition.boundSize && i_53_ >= i_49_ && (-(i_53_ - i_49_ >> 1989017673) + npc.definition.boundSize
															^ 0xffffffff) <= (_npc.definition.boundSize ^ 0xffffffff)) {
														Class98_Sub10.method995(_npc, (byte) 55, Class87.localPlayer.plane != class246_sub1.aClass246_Sub3_5069.plane);
														_npc.anInt6411 = Queue.timer;
													}
												}
											}
										}
										int i_54_ = Class2.playerCount;
										int[] is = Class319.playerIndices;
										for (int i_55_ = 0; (i_54_ ^ 0xffffffff) < (i_55_ ^ 0xffffffff); i_55_++) {
											Player player = Class151_Sub9.players[is[i_55_]];
											if (player != null && (player.anInt6411 ^ 0xffffffff) != (Queue.timer ^ 0xffffffff) && player.aBoolean6371) {
												int i_56_ = player.boundExtentsX - (player.getSize(0) + -1 << 714231912);
												int i_57_ = player.boundExtentsZ - (-1 + player.getSize(0) << 1201196744);
												if (i_56_ >= i_48_ && (player.getSize(0) ^ 0xffffffff) >= (-(-i_48_ + i_56_ >> -857371959) + npc.definition.boundSize ^ 0xffffffff) && i_57_ >= i_49_ && (player.getSize(0) ^ 0xffffffff) >= (npc.definition.boundSize + -(i_57_ - i_49_ >> 317921065)
														^ 0xffffffff)) {
													Class98_Sub30.method1311(Class87.localPlayer.plane != class246_sub1.aClass246_Sub3_5069.plane, true, player);
													player.anInt6411 = Queue.timer;
												}
											}
										}
									}
									if ((npc.anInt6411 ^ 0xffffffff) == (Queue.timer ^ 0xffffffff)) {
										continue;
									}
									Class98_Sub10.method995(npc, (byte) 55, class246_sub1.aClass246_Sub3_5069.plane != Class87.localPlayer.plane);
									npc.anInt6411 = Queue.timer;
								}
							}
							if (class246_sub1.aClass246_Sub3_5069 instanceof Class246_Sub3_Sub2_Sub1) {
								int i_58_ = Class272.gameSceneBaseX + i_35_;
								int i_59_ = i_36_ - -aa_Sub2.gameSceneBaseY;
								ItemDeque class98_sub45 = (ItemDeque) ModelRenderer.groundItems.get(i_58_ | i_59_ << 229022126 | class246_sub1.aClass246_Sub3_5069.plane << 808463964, -1);
								if (class98_sub45 != null) {
									int i_60_ = 0;
									GroundItem class98_sub26 = (GroundItem) class98_sub45.aClass148_4254.method2427(i ^ ~0x6c);
									while (class98_sub26 != null) {
										ItemDefinition class297 = Class98_Sub46_Sub19.itemDefinitionList.get(class98_sub26.itemId, (byte) -126);
										if (Class98_Sub10_Sub9.aBoolean5585 && (Class87.localPlayer.plane ^ 0xffffffff) == (class246_sub1.aClass246_Sub3_5069.plane ^ 0xffffffff)) {
											ParamDefinition class149 = (QuickChatCategory.anInt5945 ^ 0xffffffff) != 0 ? Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, QuickChatCategory.anInt5945) : null;
											if ((Class98_Sub4.anInt3826 & 0x1) != 0 && (class149 == null || class297.method3494(QuickChatCategory.anInt5945, (byte) -89, class149.defaultInteger) != class149.defaultInteger)) {
												ActionQueueEntry.addAction(false, true, class98_sub26.itemId, Class336.anInt2823, i_35_, Class246_Sub3_Sub3.applyMenuText + " -> <col=ff9040>" + class297.name, false, i_36_, 58, i_60_, -1, false, Class287_Sub2.aString3272);
											}
										}
										if ((class246_sub1.aClass246_Sub3_5069.plane ^ 0xffffffff) == (Class87.localPlayer.plane ^ 0xffffffff)) {
											String[] strings = class297.options;
											for (int index = 4; (index ^ 0xffffffff) <= -1; index--) {
												if (strings != null && strings[index] != null) {
													int i_62_ = 0;
													if ((index ^ 0xffffffff) == -1) {
														i_62_ = 13;
													}
													int i_63_ = Class284_Sub2.anInt5186;
													if (index == 1) {
														i_62_ = 23;
													}
													if (index == 2) {
														i_62_ = 2;
													}
													if ((index ^ 0xffffffff) == -4) {
														i_62_ = 30;
													}
													if ((index ^ 0xffffffff) == (class297.curser1op ^ 0xffffffff)) {
														i_63_ = class297.cursor1;
													}
													if ((index ^ 0xffffffff) == -5) {
														i_62_ = 18;
													}
													if ((class297.cursor2Options ^ 0xffffffff) == (index ^ 0xffffffff)) {
														i_63_ = class297.cursor2;
													}
													ActionQueueEntry.addAction(false, true, class98_sub26.itemId, i_63_, i_35_, "<col=ff9040>" + class297.name, false, i_36_, i_62_, i_60_, -1, false, strings[index]);
												}
											}
										}
										ActionQueueEntry.addAction(false, true, class98_sub26.itemId, Class16.anInt190, i_35_, "<col=ff9040>" + class297.name, false, i_36_, 1008, i_60_, -1, class246_sub1.aClass246_Sub3_5069.plane != Class87.localPlayer.plane, TextResources.EXAMINE.getText(
												client.gameLanguage, (byte) 25));
										class98_sub26 = (GroundItem) class98_sub45.aClass148_4254.getPrevious((byte) -121);
										i_60_++;
									}
								}
							}
							if (class246_sub1.aClass246_Sub3_5069 instanceof Interface19) {
								Interface19 interface19 = (Interface19) class246_sub1.aClass246_Sub3_5069;
								GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(interface19.method64(i ^ 0x7708), (byte) 119);
								if (class352.transformIDs != null) {
									class352 = class352.get(StartupStage.varValues, (byte) -55);
								}
								if (class352 != null) {
									if (Class98_Sub10_Sub9.aBoolean5585 && (class246_sub1.aClass246_Sub3_5069.plane ^ 0xffffffff) == (Class87.localPlayer.plane ^ 0xffffffff)) {
										ParamDefinition class149 = (QuickChatCategory.anInt5945 ^ 0xffffffff) == 0 ? null : Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, QuickChatCategory.anInt5945);
										if ((0x4 & Class98_Sub4.anInt3826 ^ 0xffffffff) != -1 && (class149 == null || class352.method3866(class149.defaultInteger, QuickChatCategory.anInt5945, i ^ 0x1) != class149.defaultInteger)) {
											ActionQueueEntry.addAction(false, true, Class98_Sub10_Sub39.method1120(interface19, (byte) 113, i_35_, i_36_), Class336.anInt2823, i_35_, Class246_Sub3_Sub3.applyMenuText + " -> <col=00ffff>" + class352.name, false, i_36_, 50, interface19.hashCode(), -1,
													false, Class287_Sub2.aString3272);
										}
									}
									if ((class246_sub1.aClass246_Sub3_5069.plane ^ 0xffffffff) == (Class87.localPlayer.plane ^ 0xffffffff)) {
										String[] strings = class352.options;
										if (strings != null) {
											for (int i_64_ = 4; i_64_ >= 0; i_64_--) {
												if (strings[i_64_] != null) {
													int i_65_ = 0;
													if ((i_64_ ^ 0xffffffff) == -1) {
														i_65_ = 15;
													}
													int i_66_ = Class284_Sub2.anInt5186;
													if (i_64_ == 1) {
														i_65_ = 4;
													}
													if (i_64_ == 2) {
														i_65_ = 8;
													}
													if ((i_64_ ^ 0xffffffff) == -4) {
														i_65_ = 16;
													}
													if (i_64_ == class352.anInt3002) {
														i_66_ = class352.anInt3008;
													}
													if (i_64_ == 4) {
														i_65_ = 1007;
													}
													if ((class352.anInt2933 ^ 0xffffffff) == (i_64_ ^ 0xffffffff)) {
														i_66_ = class352.anInt2977;
													}
													ActionQueueEntry.addAction(false, true, Class98_Sub10_Sub39.method1120(interface19, (byte) 113, i_35_, i_36_), i_66_, i_35_, "<col=00ffff>" + class352.name, false, i_36_, i_65_, interface19.hashCode(), -1, false, strings[i_64_]);
												}
											}
										}
										ActionQueueEntry.addAction(false, true, class352.id, Class16.anInt190, i_35_, "<col=00ffff>" + class352.name, false, i_36_, 1009, interface19.hashCode(), -1, (Class87.localPlayer.plane ^ 0xffffffff) != (class246_sub1.aClass246_Sub3_5069.plane
												^ 0xffffffff), TextResources.EXAMINE.getText(client.gameLanguage, (byte) 25));
									}
								}
							}
						}
					}
					if (OpenGLHeap.aBoolean6079) {
						Js5.method2765((byte) 53);
					}
				}
				Minimap.clipPlanes(-546, false);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cn.F(" + i + ',' + i_8_ + ',' + i_9_ + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method353(byte i) {
		do {
			try {
				if (FloorUnderlayDefinitionParser.aHa1225 != null) {
					FloorUnderlayDefinitionParser.aHa1225.destroy(-1);
					TextureMetrics.aClass43_1828 = null;
					FloorUnderlayDefinitionParser.aHa1225 = null;
				}
				if (i == -79) {
					break;
				}
				aClass85_362 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cn.G(" + i + ')');
			}
			break;
		} while (false);
	}

	public Class39() {
		/* empty */
	}

	public abstract void method352(int i, int i_67_, float f, int i_68_, float f_69_, int i_70_, float f_71_, float f_72_, float[] fs, int i_73_, int i_74_);
}

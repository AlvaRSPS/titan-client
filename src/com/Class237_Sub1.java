
/* Class237_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;

public final class Class237_Sub1 extends Class237 {
	public static int		anInt5047	= 0;
	public static boolean	noNpcs		= false;

	public static final Sprite loadSprite(int i, Js5 class207, byte i_7_) {
		Sprite sprite = (Sprite) Js5Client.titleCache.get(-123, i);
		if (sprite == null) {
			do {
				if (!client.fontCacheSelector) {
					sprite = Class271.loadRgbImageJpeg(class207.getFile(i, 58), 1);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				sprite = client.graphicsToolkit.createSprite(Image.loadFirst(class207, i), true);
			} while (false);
			Js5Client.titleCache.put(i, sprite, (byte) -80);
		}
		return sprite;
	}

	private byte[][]		aByteArrayArray5041	= new byte[10][];
	private Js5				aClass207_5046;
	private RSByteBuffer	aClass98_Sub22_5043	= new RSByteBuffer(null);
	private RSByteBuffer	aClass98_Sub22_5048	= new RSByteBuffer(null);
	private int				anInt5042;
	private int				anInt5045;

	private int[]			anIntArray5040;

	Class237_Sub1(int i, Js5 class207, int i_0_) {
		super(i);
		try {
			anInt5042 = i_0_;
			aClass207_5046 = class207;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dha.<init>(" + i + ',' + (class207 != null ? "{...}" : "null") + ',' + i_0_ + ')');
		}
	}

	@Override
	public final int method2899(int i, byte[] is) throws IOException {
		try {
			if (anIntArray5040 == null) {
				if (!aClass207_5046.isFileCached(0, anInt5042, -6329)) {
					return 0;
				}
				byte[] is_1_ = aClass207_5046.getFile(0, anInt5042, false);
				if (is_1_ == null) {
					throw new IllegalStateException("");
				}
				aClass98_Sub22_5048.payload = is_1_;
				aClass98_Sub22_5048.position = 0;
				int i_2_ = is_1_.length >> -830071903;
				anIntArray5040 = new int[i_2_];
				for (int i_3_ = 0; (i_3_ ^ 0xffffffff) > (i_2_ ^ 0xffffffff); i_3_++) {
					anIntArray5040[i_3_] = aClass98_Sub22_5048.readShort((byte) 127);
				}
			}
			if ((anIntArray5040.length ^ 0xffffffff) >= (anInt5045 ^ 0xffffffff)) {
				return -1;
			}
			method2916((byte) 44);
			aClass98_Sub22_5048.position = 0;
			aClass98_Sub22_5048.payload = is;
			if (i <= 43) {
				anIntArray5040 = null;
			}
			while (aClass98_Sub22_5048.position < aClass98_Sub22_5048.payload.length) {
				if (aClass98_Sub22_5043.payload == null) {
					if (aByteArrayArray5041[0] == null) {
						aClass98_Sub22_5048.payload = null;
						return aClass98_Sub22_5048.position;
					}
					aClass98_Sub22_5043.payload = aByteArrayArray5041[0];
				}
				int i_4_ = -aClass98_Sub22_5048.position + aClass98_Sub22_5048.payload.length;
				int i_5_ = -aClass98_Sub22_5043.position + aClass98_Sub22_5043.payload.length;
				if ((i_4_ ^ 0xffffffff) <= (i_5_ ^ 0xffffffff)) {
					aClass98_Sub22_5048.method1217(aClass98_Sub22_5043.payload, i_5_, -1, aClass98_Sub22_5043.position);
					aClass98_Sub22_5043.payload = null;
					aClass98_Sub22_5043.position = 0;
					anInt5045++;
					for (int i_6_ = 0; i_6_ < 9; i_6_++) {
						aByteArrayArray5041[i_6_] = aByteArrayArray5041[1 + i_6_];
					}
					aByteArrayArray5041[9] = null;
					if (anInt5045 >= anIntArray5040.length) {
						aClass98_Sub22_5048.payload = null;
						return aClass98_Sub22_5048.position;
					}
				} else {
					aClass98_Sub22_5043.getData(aClass98_Sub22_5048.payload, true, i_4_, aClass98_Sub22_5048.position);
					aClass98_Sub22_5048.payload = null;
					return is.length;
				}
			}
			aClass98_Sub22_5048.payload = null;
			return is.length;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dha.O(" + i + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	public final void method2916(byte i) {
		while_56_: do {
			try {
				if (anIntArray5040 != null) {
					if (i != 44) {
						anInt5047 = -49;
					}
					int i_8_ = 0;
					for (;;) {
						if (i_8_ >= 10) {
							break while_56_;
						}
						if (anInt5045 + i_8_ >= anIntArray5040.length) {
							break;
						}
						if (aByteArrayArray5041[i_8_] == null && aClass207_5046.isFileCached(0, anIntArray5040[anInt5045 + i_8_], -6329)) {
							aByteArrayArray5041[i_8_] = aClass207_5046.getFile(0, anIntArray5040[i_8_ + anInt5045], false);
						}
						i_8_++;
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "dha.S(" + i + ')');
			}
			break;
		} while (false);
	}
}

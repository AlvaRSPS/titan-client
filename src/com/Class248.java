/* Class248 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategory;

public final class Class248 {
	public static boolean	aBoolean1896;
	public static HashTable	aClass377_1894;
	public static int[][]	anIntArrayArray1895	= new int[6][];
	public static int		ignoreListSize;

	static {
		aClass377_1894 = new HashTable(8);
		ignoreListSize = 0;
		aBoolean1896 = false;
	}

	public static void method3157(int i) {
		do {
			try {
				anIntArrayArray1895 = null;
				aClass377_1894 = null;
				if (i == -2229) {
					break;
				}
				method3159(-17);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "pj.C(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method3158(boolean bool) {
		if (bool) {
			QuickChatCategory.aClass172ArrayArrayArray5948 = Class252.aClass172ArrayArrayArray1927;
			Class78.aSArray594 = Class81.aSArray618;
		} else {
			QuickChatCategory.aClass172ArrayArrayArray5948 = Class246_Sub2.aClass172ArrayArrayArray5077;
			Class78.aSArray594 = StrongReferenceMCNode.aSArray6298;
		}
		OpenGLTexture2DSource.anInt3103 = QuickChatCategory.aClass172ArrayArrayArray5948.length;
	}

	public static final void method3159(int i) {
		try {
			Class246_Sub3_Sub1_Sub2.anInt6251 = -1;
			LightIntensityDefinitionParser.anInt2024 = -1;
			Class333.anInt3386 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pj.B(" + i + ')');
		}
	}
}

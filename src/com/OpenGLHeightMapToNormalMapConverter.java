
/* Class118 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Archive;
import com.jagex.game.client.definition.IdentikitDefinition;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.input.impl.AwtKeyListener;
import com.jagex.game.toolkit.shadow.NativeShadow;

import jaggl.OpenGL;

public final class OpenGLHeightMapToNormalMapConverter {
	public static int	anInt978	= 0;
	public static int	anInt980	= -1;
	public static int	bytesIn;
	public static int	gameTipsIndexCrc;

	public static final void method2173(boolean bool, int i) {
		do {
			if (bool && WorldMap.aClass98_Sub46_Sub10_2056 != null) {
				RsBitsBuffers.anInt5789 = WorldMap.aClass98_Sub46_Sub10_2056.anInt6014;
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			RsBitsBuffers.anInt5789 = -1;
		} while (false);
		CursorDefinitionParser.aClass293_120 = null;
		QuickChatCategoryParser.loadingProgress = 0;
		WorldMap.aClass98_Sub46_Sub10_2056 = null;
		InventoriesDefinitionParser.aClass148_110 = null;
		WorldMap.method3299();
		WorldMap.aClass148_2065.clear((byte) 47);
		Js5Archive.aClass326_5308 = null;
		Js5Archive.aClass326_5315 = null;
		IdentikitDefinition.aClass332_1221 = null;
		Class224.aClass326_1686 = null;
		Class151_Sub7.aClass326_5009 = null;
		WorldMap.aClass370_2066 = null;
		GrandExchangeOffer.anInt849 = -1;
		ProceduralTextureSource.aClass326_3263 = null;
		Class169.anInt1307 = -1;
		Class271.aClass326_2033 = null;
		AwtKeyListener.aClass326_3805 = null;
		Class137.aClass326_1080 = null;
		if (WorldMap.aClass341_2057 != null) {
			WorldMap.aClass341_2057.method3808(0);
			WorldMap.aClass341_2057.method3809(64, -30502, 128);
		}
		if (WorldMap.mapScenesLoader != null) {
			WorldMap.mapScenesLoader.method3771(109, 64, 64);
		}
		if (WorldMap.aClass302_2062 != null) {
			WorldMap.aClass302_2062.method3550(-129, 64);
		}
		DataFs.bConfigDefinitionList.method2679(64, (byte) -91);
	}

	public static final void removeFriend(int i, String name) {
		if (name != null) {
			if (name.startsWith("*")) {
				name = name.substring(1);
			}
			String filteredName = Class353.filterName(-1, name);
			if (filteredName != null) {
				for (int count = 0; Class314.friendListSize > count; count++) {
					String friendName = Class98_Sub25.friends[count];
					if (friendName.startsWith("*")) {
						friendName = friendName.substring(1);
					}
					friendName = Class353.filterName(-1, friendName);
					if (friendName != null && friendName.equals(filteredName)) {
						Class314.friendListSize--;
						for (int index = count; (Class314.friendListSize ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
							Class98_Sub25.friends[index] = Class98_Sub25.friends[1 + index];
							TextLSEConfig.friendsDisplayNames[index] = TextLSEConfig.friendsDisplayNames[1 + index];
							GroundItem.friendsOnline[index] = GroundItem.friendsOnline[1 + index];
							Class98_Sub10_Sub17.worlds[index] = Class98_Sub10_Sub17.worlds[index + 1];
							Class69.anIntArray3222[index] = Class69.anIntArray3222[1 + index];
							aa_Sub3.aBooleanArray3575[index] = aa_Sub3.aBooleanArray3575[1 + index];
						}
						NewsFetcher.anInt3099 = WorldMapInfoDefinition.logicTimer;
						OutgoingPacket out = Class246_Sub3_Sub4.prepareOutgoingPacket(260, NativeShadow.aClass171_6330, Class331.aClass117_2811);
						out.packet.writeByte(NativeShadow.encodedStringLength(name, (byte) 75), 67);
						out.packet.writePJStr1(name, (byte) 113);
						Class98_Sub10_Sub29.sendPacket(false, out);
						break;
					}
				}
			}
		}
	}

	private Class336		aClass336_976;

	private OpenGlToolkit	aHa_Sub1_977;

	OpenGLHeightMapToNormalMapConverter(OpenGlToolkit var_ha_Sub1) {
		aHa_Sub1_977 = var_ha_Sub1;
	}

	public final boolean method2171(boolean bool) {
		if (bool != true) {
			anInt980 = -111;
		}
		if (aHa_Sub1_977.haveExtFrameBufferObject && aHa_Sub1_977.aBoolean4447 && aClass336_976 == null) {
			Class345 class345 = Class246_Sub3_Sub1_Sub1.method2996(35632, aHa_Sub1_977, false,
					"uniform float rcpRelief;\nuniform vec2 sampleSize;\nuniform sampler3D heightMap;\nvoid main() {\nfloat dx = texture3D(heightMap, vec3(-sampleSize.x, 0.0, 0.0)+gl_TexCoord[0].xyz).r - texture3D(heightMap, vec3(sampleSize.x, 0.0, 0.0)+gl_TexCoord[0].xyz).r;\nfloat dy = texture3D(heightMap, vec3(0.0, -sampleSize.y, 0.0)+gl_TexCoord[0].xyz).r - texture3D(heightMap, vec3(0.0, sampleSize.y, 0.0)+gl_TexCoord[0].xyz).r;\ngl_FragColor = vec4(0.5+normalize(vec3(dx, dy, rcpRelief))*0.5, texture3D(heightMap, gl_TexCoord[0].xyz).r);\n}\n");
			if (class345 != null) {
				aClass336_976 = Billboard.method2584(aHa_Sub1_977, new Class345[] { class345 }, bool);
			}
		}
		if (aClass336_976 == null) {
			return false;
		}
		return true;
	}

	public final boolean method2172(int i, Class42_Sub4 class42_sub4, float f, Class42_Sub4 class42_sub4_4_) {
		if (!method2171(true)) {
			return false;
		}
		Class288 class288 = aHa_Sub1_977.aClass288_4363;
		Class98_Sub46_Sub14 class98_sub46_sub14 = new Class98_Sub46_Sub14(aHa_Sub1_977, 6408, class42_sub4_4_.anInt5369, class42_sub4_4_.anInt5372);
		boolean bool = false;
		aHa_Sub1_977.method1898(true, class288);
		class288.method3406((byte) 120, 0, class98_sub46_sub14);
		if (class288.method3403((byte) 74)) {
			OpenGL.glPushMatrix();
			OpenGL.glLoadIdentity();
			OpenGL.glMatrixMode(5889);
			OpenGL.glPushMatrix();
			OpenGL.glLoadIdentity();
			OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
			OpenGL.glPushAttrib(2048);
			OpenGL.glViewport(0, 0, class42_sub4_4_.anInt5369, class42_sub4_4_.anInt5372);
			OpenGL.glUseProgramObjectARB(aClass336_976.aLong2821);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(aClass336_976.aLong2821, "heightMap"), 0);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(aClass336_976.aLong2821, "rcpRelief"), 1.0F / f);
			OpenGL.glUniform2fARB(OpenGL.glGetUniformLocationARB(aClass336_976.aLong2821, "sampleSize"), 1.0F / class42_sub4.anInt5369, 1.0F / class42_sub4.anInt5372);
			for (int i_5_ = 0; i_5_ < class42_sub4_4_.anInt5368; i_5_++) {
				float f_6_ = (float) i_5_ / (float) class42_sub4_4_.anInt5368;
				aHa_Sub1_977.setActiveTexture(1, class42_sub4);
				OpenGL.glBegin(7);
				OpenGL.glTexCoord3f(0.0F, 0.0F, f_6_);
				OpenGL.glVertex2f(0.0F, 0.0F);
				OpenGL.glTexCoord3f(1.0F, 0.0F, f_6_);
				OpenGL.glVertex2f(1.0F, 0.0F);
				OpenGL.glTexCoord3f(1.0F, 1.0F, f_6_);
				OpenGL.glVertex2f(1.0F, 1.0F);
				OpenGL.glTexCoord3f(0.0F, 1.0F, f_6_);
				OpenGL.glVertex2f(0.0F, 1.0F);
				OpenGL.glEnd();
				class42_sub4_4_.method395(0, 0, i_5_, i, 0, class42_sub4_4_.anInt5369, class42_sub4_4_.anInt5372, 0);
			}
			OpenGL.glUseProgramObjectARB(0L);
			OpenGL.glPopAttrib();
			OpenGL.glPopMatrix();
			OpenGL.glMatrixMode(5888);
			bool = true;
			OpenGL.glPopMatrix();
		}
		class288.method3401(i, true);
		aHa_Sub1_977.method1907(class288, -1);
		return bool;
	}
}

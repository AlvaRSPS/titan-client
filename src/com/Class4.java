/* Class4 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.parser.NPCDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.constants.BuildLocation;

public final class Class4 {
	public static boolean				aBoolean79			= false;
	public static QuickChatMessageType	aClass348_82		= new QuickChatMessageType(2, 4, 4, 0);
	public static int[]					anIntArray83		= new int[1];
	public static NPCDefinitionParser	npcDefinitionList;
	public static int					pixelsOccludedCount	= 0;

	public static final void drawMapDot(int i, int i_0_, int i_1_, RtInterface class293, RtInterfaceClip var_aa, int i_2_, byte i_3_, Sprite sprite) {
		if (sprite != null) {
			int i_4_;
			do {
				if (Class98_Sub46_Sub20_Sub2.cameraMode != 4) {
					i_4_ = (int) RsFloatBuffer.aFloat5794 + Class204.anInt1553 & 0x3fff;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				i_4_ = (int) RsFloatBuffer.aFloat5794 & 0x3fff;
			} while (false);
			int i_5_ = 10 + Math.max(class293.renderWidth / 2, class293.renderHeight / 2);
			int i_6_ = i_2_ * i_2_ - -(i_0_ * i_0_);
			if ((i_5_ * i_5_ ^ 0xffffffff) <= (i_6_ ^ 0xffffffff)) {
				int i_7_ = Class284_Sub2_Sub2.SINE[i_4_];
				int i_8_ = Class284_Sub2_Sub2.COSINE[i_4_];
				if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) != -5) {
					i_7_ = 256 * i_7_ / (256 + Class151.anInt1213);
					i_8_ = 256 * i_8_ / (256 + Class151.anInt1213);
				}
				int i_9_ = i_7_ * i_0_ - -(i_8_ * i_2_) >> -1366947250;
				int i_10_ = i_0_ * i_8_ + -(i_2_ * i_7_) >> -1991891442;
				sprite.method3729(i_9_ + class293.renderWidth / 2 + i_1_ - sprite.getRenderWidth() / 2, -i_10_ + class293.renderHeight / 2 + i + -(sprite.getRenderHeight() / 2), var_aa, i_1_, i);
			}
		}
	}

	public static final void method174(byte i) {
		if (client.buildLocation != BuildLocation.LOCAL) {
			if (i != 99) {
				method174((byte) -1);
			}
			try {
				JavaScriptInterface.callJsMethod("tbrefresh", client.activeClient, -26978);
			} catch (Throwable throwable) {
				/* empty */
			}
		}
	}
}

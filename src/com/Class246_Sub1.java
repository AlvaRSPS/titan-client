/* Class246_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;

public final class Class246_Sub1 extends SceneGraphNode {
	public static OutgoingOpcode aClass171_5068 = new OutgoingOpcode(66, -1);

	public static final void method2966(int i) {
		try {
			if (i == 66) {
				for (Class98_Sub46_Sub3 class98_sub46_sub3 = (Class98_Sub46_Sub3) Class98_Sub10_Sub11.animatedObjects.getFirst(32); class98_sub46_sub3 != null; class98_sub46_sub3 = (Class98_Sub46_Sub3) Class98_Sub10_Sub11.animatedObjects.getNext(i ^ 0x39)) {
					Class246_Sub3_Sub4_Sub3 class246_sub3_sub4_sub3 = class98_sub46_sub3.aClass246_Sub3_Sub4_Sub3_5954;
					if (class246_sub3_sub4_sub3.aBoolean6450) {
						class98_sub46_sub3.unlink(48);
						class246_sub3_sub4_sub3.method3067(120);
					} else if ((class246_sub3_sub4_sub3.anInt6445 ^ 0xffffffff) >= (Queue.timer ^ 0xffffffff)) {
						class246_sub3_sub4_sub3.method3073((byte) 31, GameDefinition.anInt2099);
						if (class246_sub3_sub4_sub3.aBoolean6450) {
							class98_sub46_sub3.unlink(67);
						} else {
							LoginOpcode.method2826(class246_sub3_sub4_sub3, true);
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bd.B(" + i + ')');
		}
	}

	public static final boolean method2967(int i, int i_0_, byte i_1_) {
		try {
			if (i_1_ != 91) {
				return true;
			}
			return (i_0_ & 0x10000 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bd.C(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method2968(byte i) {
		do {
			try {
				aClass171_5068 = null;
				if (i < -76) {
					break;
				}
				method2968((byte) -4);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "bd.A(" + i + ')');
			}
			break;
		} while (false);
	}

	boolean			aBoolean5070;

	Char			aClass246_Sub3_5069;

	Class246_Sub6[]	aClass246_Sub6Array5067;

	public final boolean method2969(RSToolkit var_ha, int i, int i_2_, int i_3_) {
		try {
			if (i_3_ > -51) {
				method2967(117, -111, (byte) -10);
			}
			int i_4_ = aClass246_Sub3_5069.method2986(-14240);
			if (aClass246_Sub6Array5067 != null) {
				for (int i_5_ = 0; i_5_ < aClass246_Sub6Array5067.length; i_5_++) {
					aClass246_Sub6Array5067[i_5_].anInt5109 <<= i_4_;
					if (aClass246_Sub6Array5067[i_5_].method3130(i, i_2_) && aClass246_Sub3_5069.method2976(i, var_ha, (byte) 79, i_2_)) {
						aClass246_Sub6Array5067[i_5_].anInt5109 >>= i_4_;
						return true;
					}
					aClass246_Sub6Array5067[i_5_].anInt5109 >>= i_4_;
				}
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bd.D(" + (var_ha != null ? "{...}" : "null") + ',' + i + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}
}

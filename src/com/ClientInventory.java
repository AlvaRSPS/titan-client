/* Class98_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.shadow.OpenGlShadow;

public final class ClientInventory extends Node {
	public static int timestampChatboxUpdate = 0;

	public static final int method2212(boolean bool, byte i, int i_0_, int i_1_) {
		if (i != -96) {
			method2212(true, (byte) -78, 7, 22);
		}
		ClientInventory class98_sub3 = WaterDetailPreferenceField.get(i_0_, bool, 6);
		if (class98_sub3 == null) {
			return 0;
		}
		if (i_1_ < 0 || (i_1_ ^ 0xffffffff) <= (class98_sub3.anIntArray3823.length ^ 0xffffffff)) {
			return 0;
		}
		return class98_sub3.anIntArray3823[i_1_];
	}

	public int[]	anIntArray3823	= new int[1];

	int[]			anIntArray3824	= { -1 };

	public ClientInventory() {
		/* empty */
	}

	public final ModelRenderer method951(AnimationDefinition class97, int i, int i_0_, boolean bool, PlayerAppearence class313, int i_1_, int i_2_, int i_3_, byte i_4_, RSToolkit var_ha) {
		try {
			ModelRenderer class146 = null;
			int i_5_ = i_2_;
			RenderAnimDefinition class294 = null;
			if ((i_1_ ^ 0xffffffff) != 0) {
				class294 = Class370.renderAnimDefinitionList.method3199(false, i_1_);
			}
			int[] is = anIntArray3824;
			if (class294 != null && class294.anIntArray2379 != null) {
				is = new int[class294.anIntArray2379.length];
				for (int i_6_ = 0; (i_6_ ^ 0xffffffff) > (class294.anIntArray2379.length ^ 0xffffffff); i_6_++) {
					int i_7_ = class294.anIntArray2379[i_6_];
					if (i_7_ < 0 || (i_7_ ^ 0xffffffff) <= (anIntArray3824.length ^ 0xffffffff)) {
						is[i_6_] = -1;
					} else {
						is[i_6_] = anIntArray3824[i_7_];
					}
				}
			}
			boolean bool_8_ = false;
			boolean bool_9_ = false;
			boolean bool_10_ = false;
			boolean bool_11_ = false;
			int i_12_ = -1;
			int i_13_ = -1;
			int i_14_ = 0;
			AnimationSkeletonSet class98_sub46_sub16 = null;
			AnimationSkeletonSet class98_sub46_sub16_15_ = null;
			if (i_4_ > -43) {
				method951(null, 78, 100, false, null, 112, 24, -42, (byte) 36, null);
			}
			if (class97 != null) {
				i_5_ |= 0x20;
				i_12_ = class97.frameIds[i_0_];
				int i_16_ = i_12_ >>> -2044512464;
				class98_sub46_sub16 = Class151_Sub7.animationDefinitionList.method2624(2, i_16_);
				i_12_ &= 0xffff;
				if (class98_sub46_sub16 != null) {
					bool_9_ |= class98_sub46_sub16.method1619(i_12_, 31239);
					bool_8_ |= class98_sub46_sub16.method1617(false, i_12_);
					bool_11_ |= class98_sub46_sub16.method1615(i_12_, false);
					bool_10_ |= class97.aBoolean817;
				}
				if ((class97.aBoolean825 || Class357.forcedTweening) && (i_3_ ^ 0xffffffff) != 0 && class97.frameIds.length > i_3_) {
					i_14_ = class97.frameLengths[i_0_];
					i_13_ = class97.frameIds[i_3_];
					int i_17_ = i_13_ >>> 951346256;
					i_13_ &= 0xffff;
					if ((i_16_ ^ 0xffffffff) != (i_17_ ^ 0xffffffff)) {
						class98_sub46_sub16_15_ = Class151_Sub7.animationDefinitionList.method2624(2, i_13_ >>> -1068924144);
					} else {
						class98_sub46_sub16_15_ = class98_sub46_sub16;
					}
					if (class98_sub46_sub16_15_ != null) {
						bool_9_ |= class98_sub46_sub16_15_.method1619(i_13_, 31239);
						bool_8_ |= class98_sub46_sub16_15_.method1617(false, i_13_);
						bool_11_ |= class98_sub46_sub16_15_.method1615(i_13_, false);
					}
				}
				if (bool_9_) {
					i_5_ |= 0x80;
				}
				if (bool_8_) {
					i_5_ |= 0x100;
				}
				if (bool_10_) {
					i_5_ |= 0x200;
				}
				if (bool_11_) {
					i_5_ |= 0x400;
				}
			}
			long l = method952(i_1_, bool, is, class313 != null ? class313.colours : null, -29509);
			if (OpenGlShadow.aClass79_6321 != null) {
				class146 = (ModelRenderer) OpenGlShadow.aClass79_6321.get(-120, l);
			}
			if (class146 == null || (var_ha.c(class146.functionMask(), i_5_) ^ 0xffffffff) != -1) {
				if (class146 != null) {
					i_5_ = var_ha.mergeFunctionMask(i_5_, class146.functionMask());
				}
				int i_18_ = i_5_;
				boolean bool_19_ = false;
				ItemAppearanceOverride override = null;
				for (int i_20_ = 0; (is.length ^ 0xffffffff) < (i_20_ ^ 0xffffffff); i_20_++) {
					if ((is[i_20_] ^ 0xffffffff) != 0 && !Class98_Sub46_Sub19.itemDefinitionList.get(is[i_20_], (byte) -123).isWornModelCached(0, bool, override)) {
						bool_19_ = true;
					}
				}
				if (bool_19_) {
					return null;
				}
				BaseModel[] class178s = new BaseModel[is.length];
				for (int i_21_ = 0; i_21_ < is.length; i_21_++) {
					if ((is[i_21_] ^ 0xffffffff) != 0) {
						class178s[i_21_] = Class98_Sub46_Sub19.itemDefinitionList.get(is[i_21_], (byte) -116).method3500(bool, override, 109);
					}
				}
				if (class294 != null && class294.anIntArrayArray2366 != null) {
					for (int i_22_ = 0; (i_22_ ^ 0xffffffff) > (class294.anIntArrayArray2366.length ^ 0xffffffff); i_22_++) {
						if (class294.anIntArrayArray2366[i_22_] != null && class178s[i_22_] != null) {
							int i_23_ = class294.anIntArrayArray2366[i_22_][0];
							int i_24_ = class294.anIntArrayArray2366[i_22_][1];
							int i_25_ = class294.anIntArrayArray2366[i_22_][2];
							int i_26_ = class294.anIntArrayArray2366[i_22_][3];
							int i_27_ = class294.anIntArrayArray2366[i_22_][4];
							int i_28_ = class294.anIntArrayArray2366[i_22_][5];
							if ((i_26_ ^ 0xffffffff) != -1 || (i_27_ ^ 0xffffffff) != -1 || i_28_ != 0) {
								class178s[i_22_].method2600(i_28_, i_26_, (byte) -128, i_27_);
							}
							if ((i_23_ ^ 0xffffffff) != -1 || i_24_ != 0 || (i_25_ ^ 0xffffffff) != -1) {
								class178s[i_22_].method2597(i_25_, i_23_, (byte) 108, i_24_);
							}
						}
					}
				}
				if (class313 != null) {
					i_18_ |= 0x4000;
				}
				BaseModel class178 = new BaseModel(class178s, class178s.length);
				class146 = var_ha.createModelRenderer(class178, i_18_, Class105.anInt3415, 64, 850);
				if (class313 != null) {
					for (int i_29_ = 0; i_29_ < 5; i_29_++) {
						for (int i_30_ = 0; HashTableIterator.colourToReplace.length > i_30_; i_30_++) {
							if (HashTableIterator.colourToReplace[i_30_][i_29_].length > class313.colours[i_29_]) {
								class146.recolour(Class98_Sub10_Sub11.aShortArrayArray5597[i_30_][i_29_], HashTableIterator.colourToReplace[i_30_][i_29_][class313.colours[i_29_]]);
							}
						}
					}
				}
				if (OpenGlShadow.aClass79_6321 != null) {
					class146.updateFunctionMask(i_5_);
					OpenGlShadow.aClass79_6321.put(l, class146, (byte) -80);
				}
			}
			if (class97 == null || class98_sub46_sub16 == null) {
				return class146;
			}
			ModelRenderer class146_31_ = class146.method2341((byte) 1, i_5_, true);
			class146_31_.method2338(i - 1, class98_sub46_sub16, i_12_, class98_sub46_sub16_15_, class97.aBoolean817, 0, -104, i_14_, i_13_);
			return class146_31_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bc.A(" + (class97 != null ? "{...}" : "null") + ',' + i + ',' + i_0_ + ',' + bool + ',' + (class313 != null ? "{...}" : "null") + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	private final long method952(int i, boolean bool, int[] is, int[] is_32_, int i_33_) {
		try {
			long[] ls = WhirlpoolGenerator.CRC_64;
			long l = -1L;
			l = ls[(int) ((i >> 1392131400 ^ l) & 0xffL)] ^ l >>> -155609528;
			l = ls[(int) (0xffL & (i ^ l))] ^ l >>> -1669638136;
			for (int element : is) {
				l = ls[(int) ((element >> -276629928 ^ l) & 0xffL)] ^ l >>> -947925880;
				l = l >>> -1018516728 ^ ls[(int) ((l ^ element >> 525663440) & 0xffL)];
				l = ls[(int) ((l ^ element >> 285473992) & 0xffL)] ^ l >>> -600711224;
				l = ls[(int) (0xffL & (element ^ l))] ^ l >>> -24100984;
			}
			if (is_32_ != null) {
				for (int i_35_ = 0; (i_35_ ^ 0xffffffff) > -6; i_35_++) {
					l = l >>> 1900652552 ^ ls[(int) ((is_32_[i_35_] ^ l) & 0xffL)];
				}
			}
			if (i_33_ != -29509) {
				return -33L;
			}
			l = ls[(int) (((!bool ? 0 : 1) ^ l) & 0xffL)] ^ l >>> -778341368;
			return l;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bc.B(" + i + ',' + bool + ',' + (is != null ? "{...}" : "null") + ',' + (is_32_ != null ? "{...}" : "null") + ',' + i_33_ + ')');
		}
	}
}

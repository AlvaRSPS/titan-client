
/* Class98_Sub10_Sub19 - Decompiled by JODE
 */ package com; /*
					*/

import java.util.Random;

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.preferences.BuildAreaPreferenceField;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;

public final class Class98_Sub10_Sub19 extends Class98_Sub10 {
	public static long	aLong5631	= 0L;
	public static int	anInt5630;

	public static final void loadFonts(byte i, RSToolkit var_ha) {
		try {
			if (i < -35) {
				Class242.fonts = new Class244[Class2.fontIds.length];
				for (int i_1_ = 0; (Class2.fontIds.length ^ 0xffffffff) < (i_1_ ^ 0xffffffff); i_1_++) {
					int i_2_ = Class2.fontIds[i_1_];
					FontSpecifications class197 = FontSpecifications.load(BuildAreaPreferenceField.glyphsStore, true, i_2_);
					Font class43 = var_ha.createFont(class197, Image.loadImages(MaxScreenSizePreferenceField.fontmetricsStore, i_2_), true);
					Class242.fonts[i_1_] = new Class244(class43, class197);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lp.E(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method1057(int i, int i_0_) {
		try {
			if (i_0_ != 1024) {
				method1057(93, 113);
			}
			if (RenderAnimDefinitionParser.anInt1948 == 0) {
				BConfigDefinition.aClass98_Sub31_Sub2_3122.method1366(i, (byte) 58);
			} else {
				Class224_Sub3.anInt5037 = i;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lp.B(" + i + ',' + i_0_ + ')');
		}
	}

	public static final void method1059(boolean bool, int i, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_) {
		try {
			int i_36_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_32_);
			int i_37_ = GZipDecompressor.method3219(bool, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_35_);
			int i_38_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i);
			int i_39_ = GZipDecompressor.method3219(bool, Class3.anInt77, Class76_Sub8.anInt3778, i_34_);
			int i_40_ = GZipDecompressor.method3219(bool, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_33_ + i_32_);
			int i_41_ = GZipDecompressor.method3219(bool, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, -i_33_ + i_35_);
			for (int i_42_ = i_36_; i_40_ > i_42_; i_42_++) {
				Class333.method3761(i_31_, AnimationDefinition.anIntArrayArray814[i_42_], i_38_, i_39_, (byte) -123);
			}
			for (int i_43_ = i_37_; i_43_ > i_41_; i_43_--) {
				Class333.method3761(i_31_, AnimationDefinition.anIntArrayArray814[i_43_], i_38_, i_39_, (byte) -128);
			}
			int i_44_ = GZipDecompressor.method3219(bool, Class3.anInt77, Class76_Sub8.anInt3778, i_33_ + i);
			int i_45_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_34_ - i_33_);
			for (int i_46_ = i_40_; (i_41_ ^ 0xffffffff) <= (i_46_ ^ 0xffffffff); i_46_++) {
				int[] is = AnimationDefinition.anIntArrayArray814[i_46_];
				Class333.method3761(i_31_, is, i_38_, i_44_, (byte) -124);
				Class333.method3761(i_31_, is, i_45_, i_39_, (byte) 1);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lp.D(" + bool + ',' + i + ',' + i_31_ + ',' + i_32_ + ',' + i_33_ + ',' + i_34_ + ',' + i_35_ + ')');
		}
	}

	private int	anInt5628;
	private int	anInt5629;

	private int	anInt5632;

	private int	anInt5633	= 0;

	private int	anInt5634;

	public Class98_Sub10_Sub19() {
		super(0, true);
		anInt5628 = 0;
		anInt5629 = 16;
		anInt5634 = 4096;
		anInt5632 = 2000;
	}

	@Override
	public final void method1001(byte i) {
		try {
			if (i == 66) {
				Class98_Sub31_Sub4.initializeTrig(i + -66);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lp.I(" + i + ')');
		}
	}

	@Override
	public final int[] method990(int i, int i_5_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_5_);
			if (this.aClass16_3863.aBoolean198) {
				int i_6_ = anInt5634 >> 471123713;
				int[][] is_7_ = this.aClass16_3863.method238(-4);
				Random random = new Random(anInt5628);
				for (int i_8_ = 0; (anInt5632 ^ 0xffffffff) < (i_8_ ^ 0xffffffff); i_8_++) {
					int i_9_ = anInt5634 <= 0 ? anInt5633 : anInt5633 + -i_6_ + HorizontalAlignment.randomNumber(-28737, anInt5634, random);
					i_9_ = (0xffb & i_9_) >> -745670044;
					int i_10_ = HorizontalAlignment.randomNumber(-28737, Class25.anInt268, random);
					int i_11_ = HorizontalAlignment.randomNumber(i ^ ~0x70bf, HorizontalAlignment.anInt492, random);
					int i_12_ = (anInt5629 * Class278_Sub1.COSINE[i_9_] >> 1421866124) + i_10_;
					int i_13_ = i_11_ + (aa_Sub2.SINE[i_9_] * anInt5629 >> -8291220);
					int i_14_ = -i_11_ + i_13_;
					int i_15_ = -i_10_ + i_12_;
					if (i_15_ != 0 || (i_14_ ^ 0xffffffff) != -1) {
						if (i_14_ < 0) {
							i_14_ = -i_14_;
						}
						if ((i_15_ ^ 0xffffffff) > -1) {
							i_15_ = -i_15_;
						}
						boolean bool = i_15_ < i_14_;
						if (bool) {
							int i_16_ = i_10_;
							i_10_ = i_11_;
							int i_17_ = i_12_;
							i_12_ = i_13_;
							i_11_ = i_16_;
							i_13_ = i_17_;
						}
						if (i_10_ > i_12_) {
							int i_18_ = i_10_;
							int i_19_ = i_11_;
							i_10_ = i_12_;
							i_11_ = i_13_;
							i_12_ = i_18_;
							i_13_ = i_19_;
						}
						int i_20_ = i_11_;
						int i_21_ = -i_10_ + i_12_;
						int i_22_ = i_13_ - i_11_;
						int i_23_ = -i_21_ / 2;
						int i_24_ = 2048 / i_21_;
						int i_25_ = -(HorizontalAlignment.randomNumber(-28737, 4096, random) >> -951063678) + 1024;
						if ((i_22_ ^ 0xffffffff) > -1) {
							i_22_ = -i_22_;
						}
						int i_26_ = i_13_ > i_11_ ? 1 : -1;
						for (int i_27_ = i_10_; i_27_ < i_12_; i_27_++) {
							int i_28_ = (i_27_ - i_10_) * i_24_ + i_25_ - -1024;
							int i_29_ = Class329.anInt2761 & i_27_;
							int i_30_ = i_20_ & SoftwareNativeHeap.anInt6075;
							i_23_ += i_22_;
							if (bool) {
								is_7_[i_30_][i_29_] = i_28_;
							} else {
								is_7_[i_29_][i_30_] = i_28_;
							}
							if ((i_23_ ^ 0xffffffff) < -1) {
								i_20_ -= -i_26_;
								i_23_ = -i_21_ + i_23_;
							}
						}
					}
				}
			}
			if (i != 255) {
				method1001((byte) -38);
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lp.G(" + i + ',' + i_5_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_3_) {
		do {
			try {
				int i_4_ = i;
				while_117_: do {
					while_116_: do {
						while_115_: do {
							do {
								if (i_4_ != 0) {
									if (i_4_ != 1) {
										if (i_4_ != 2) {
											if ((i_4_ ^ 0xffffffff) != -4) {
												if ((i_4_ ^ 0xffffffff) == -5) {
													break while_116_;
												}
												break while_117_;
											}
										} else {
											break;
										}
										break while_115_;
									}
								} else {
									anInt5628 = class98_sub22.readUnsignedByte((byte) 69);
									break while_117_;
								}
								anInt5632 = class98_sub22.readShort((byte) 127);
								break while_117_;
							} while (false);
							anInt5629 = class98_sub22.readUnsignedByte((byte) -126);
							break while_117_;
						} while (false);
						anInt5633 = class98_sub22.readShort((byte) 127);
						break while_117_;
					} while (false);
					anInt5634 = class98_sub22.readShort((byte) 127);
				} while (false);
				if (i_3_ < -92) {
					break;
				}
				method1057(-103, -126);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "lp.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_3_ + ')');
			}
			break;
		} while (false);
	}
}

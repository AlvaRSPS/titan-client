/* Class333 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.GraphicsDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.preferences.AntialiasPreferenceField;
import com.jagex.game.constants.BuildLocation;

public final class Class333 implements DepthBufferObject {
	public static int	xTele;
	public static int	anInt3386	= 0;
	public static int	anInt3390	= 0;

	public static final void method3761(int i, int[] is, int i_0_, int i_1_, byte i_2_) {
		try {
			i_0_--;
			int i_4_ = --i_1_ + -7;
			while (i_4_ > i_0_) {
				is[++i_0_] = i;
				is[++i_0_] = i;
				is[++i_0_] = i;
				is[++i_0_] = i;
				is[++i_0_] = i;
				is[++i_0_] = i;
				is[++i_0_] = i;
				is[++i_0_] = i;
			}
			while (i_0_ < i_1_) {
				is[++i_0_] = i;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uh.B(" + i + ',' + (is != null ? "{...}" : "null") + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public static final void method3762(byte i, boolean bool, Mob class246_sub3_sub4_sub2) {
		do {
			try {
				RenderAnimDefinition class294 = class246_sub3_sub4_sub2.method3039(1);
				if (class246_sub3_sub4_sub2.pathLength == 0) {
					AntialiasPreferenceField.anInt3708 = -1;
					BConfigDefinition.anInt3121 = 0;
					class246_sub3_sub4_sub2.anInt6433 = 0;
				} else {
					if (class246_sub3_sub4_sub2.anInt6413 != -1 && class246_sub3_sub4_sub2.anInt6400 == 0) {
						AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class246_sub3_sub4_sub2.anInt6413, 16383);
						if ((class246_sub3_sub4_sub2.anInt6436 ^ 0xffffffff) < -1 && class97.resetWhenWalk == 0) {
							class246_sub3_sub4_sub2.anInt6433++;
							AntialiasPreferenceField.anInt3708 = -1;
							BConfigDefinition.anInt3121 = 0;
							break;
						}
						if ((class246_sub3_sub4_sub2.anInt6436 ^ 0xffffffff) >= -1 && (class97.priority ^ 0xffffffff) == -1) {
							BConfigDefinition.anInt3121 = 0;
							class246_sub3_sub4_sub2.anInt6433++;
							AntialiasPreferenceField.anInt3708 = -1;
							break;
						}
					}
					if (class246_sub3_sub4_sub2.anInt6379 != -1 && class246_sub3_sub4_sub2.anInt6391 <= Queue.timer) {
						GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, class246_sub3_sub4_sub2.anInt6379);
						if (class107.aBoolean909 && class107.animation != -1) {
							AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383);
							if (class246_sub3_sub4_sub2.anInt6436 > 0 && class97.resetWhenWalk == 0) {
								class246_sub3_sub4_sub2.anInt6433++;
								BConfigDefinition.anInt3121 = 0;
								AntialiasPreferenceField.anInt3708 = -1;
								break;
							}
							if (class246_sub3_sub4_sub2.anInt6436 <= 0 && (class97.priority ^ 0xffffffff) == -1) {
								class246_sub3_sub4_sub2.anInt6433++;
								AntialiasPreferenceField.anInt3708 = -1;
								BConfigDefinition.anInt3121 = 0;
								break;
							}
						}
					}
					if ((class246_sub3_sub4_sub2.anInt6379 ^ 0xffffffff) != 0 && Queue.timer >= class246_sub3_sub4_sub2.anInt6391) {
						GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, class246_sub3_sub4_sub2.anInt6379);
						if (class107.aBoolean909 && class107.animation != -1) {
							AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383);
							if (class246_sub3_sub4_sub2.anInt6436 > 0 && class97.resetWhenWalk == 0) {
								BConfigDefinition.anInt3121 = 0;
								class246_sub3_sub4_sub2.anInt6433++;
								AntialiasPreferenceField.anInt3708 = -1;
								break;
							}
							if ((class246_sub3_sub4_sub2.anInt6436 ^ 0xffffffff) >= -1 && (class97.priority ^ 0xffffffff) == -1) {
								class246_sub3_sub4_sub2.anInt6433++;
								AntialiasPreferenceField.anInt3708 = -1;
								BConfigDefinition.anInt3121 = 0;
								break;
							}
						}
					}
					int i_6_ = class246_sub3_sub4_sub2.boundExtentsX;
					int i_7_ = class246_sub3_sub4_sub2.boundExtentsZ;
					int i_8_ = 512 * class246_sub3_sub4_sub2.pathX[-1 + class246_sub3_sub4_sub2.pathLength] + 256 * class246_sub3_sub4_sub2.getSize(0);
					int i_9_ = class246_sub3_sub4_sub2.pathZ[-1 + class246_sub3_sub4_sub2.pathLength] * 512 - -(class246_sub3_sub4_sub2.getSize(0) * 256);
					if (i_6_ < i_8_) {
						if (i_7_ >= i_9_) {
							if ((i_7_ ^ 0xffffffff) < (i_9_ ^ 0xffffffff)) {
								class246_sub3_sub4_sub2.method3042(14336, -8193);
							} else {
								class246_sub3_sub4_sub2.method3042(12288, -8193);
							}
						} else {
							class246_sub3_sub4_sub2.method3042(10240, -8193);
						}
					} else if (i_8_ < i_6_) {
						if (i_7_ < i_9_) {
							class246_sub3_sub4_sub2.method3042(6144, -8193);
						} else if ((i_7_ ^ 0xffffffff) >= (i_9_ ^ 0xffffffff)) {
							class246_sub3_sub4_sub2.method3042(4096, -8193);
						} else {
							class246_sub3_sub4_sub2.method3042(2048, -8193);
						}
					} else if (i_9_ <= i_7_) {
						if ((i_7_ ^ 0xffffffff) < (i_9_ ^ 0xffffffff)) {
							class246_sub3_sub4_sub2.method3042(0, -8193);
						}
					} else {
						class246_sub3_sub4_sub2.method3042(8192, -8193);
					}
					byte i_10_ = class246_sub3_sub4_sub2.pathSpeed[class246_sub3_sub4_sub2.pathLength + -1];
					if (!bool && (i_8_ + -i_6_ > 1024 || (i_8_ + -i_6_ ^ 0xffffffff) > 1023 || (-i_7_ + i_9_ ^ 0xffffffff) < -1025 || -i_7_ + i_9_ < -1024)) {
						class246_sub3_sub4_sub2.boundExtentsX = i_8_;
						class246_sub3_sub4_sub2.boundExtentsZ = i_9_;
						class246_sub3_sub4_sub2.method3047(class246_sub3_sub4_sub2.anInt6415, false, 75);
						AntialiasPreferenceField.anInt3708 = -1;
						if (class246_sub3_sub4_sub2.anInt6436 > 0) {
							class246_sub3_sub4_sub2.anInt6436--;
						}
						class246_sub3_sub4_sub2.pathLength--;
						BConfigDefinition.anInt3121 = 0;
					} else {
						int i_11_ = 16;
						boolean bool_12_ = true;
						if (class246_sub3_sub4_sub2 instanceof NPC) {
							bool_12_ = ((NPC) class246_sub3_sub4_sub2).definition.aBoolean1126;
						}
						if (!bool_12_) {
							if (!bool && class246_sub3_sub4_sub2.pathLength > 1) {
								i_11_ = 24;
							}
							if (!bool && (class246_sub3_sub4_sub2.pathLength ^ 0xffffffff) < -3) {
								i_11_ = 32;
							}
						} else {
							int i_13_ = -class246_sub3_sub4_sub2.yaw.value + class246_sub3_sub4_sub2.anInt6415;
							if ((i_13_ ^ 0xffffffff) != -1 && (class246_sub3_sub4_sub2.anInt6364 ^ 0xffffffff) == 0 && (class246_sub3_sub4_sub2.anInt6414 ^ 0xffffffff) != -1) {
								i_11_ = 8;
							}
							if (!bool && (class246_sub3_sub4_sub2.pathLength ^ 0xffffffff) < -3) {
								i_11_ = 24;
							}
							if (!bool && (class246_sub3_sub4_sub2.pathLength ^ 0xffffffff) < -4) {
								i_11_ = 32;
							}
						}
						if ((class246_sub3_sub4_sub2.anInt6433 ^ 0xffffffff) < -1 && class246_sub3_sub4_sub2.pathLength > 1) {
							i_11_ = 32;
							class246_sub3_sub4_sub2.anInt6433--;
						}
						if ((i_10_ ^ 0xffffffff) == -3) {
							i_11_ <<= 1;
						} else if (i_10_ == 0) {
							i_11_ >>= 1;
						}
						if (class294.anInt2401 != -1) {
							i_11_ <<= 9;
							if ((class246_sub3_sub4_sub2.pathLength ^ 0xffffffff) == -2) {
								int i_14_ = class246_sub3_sub4_sub2.anInt6435 * class246_sub3_sub4_sub2.anInt6435;
								int i_15_ = ((class246_sub3_sub4_sub2.boundExtentsX ^ 0xffffffff) >= (i_8_ ^ 0xffffffff) ? i_8_ + -class246_sub3_sub4_sub2.boundExtentsX : class246_sub3_sub4_sub2.boundExtentsX - i_8_) << -1268948087;
								int i_16_ = (i_9_ < class246_sub3_sub4_sub2.boundExtentsZ ? class246_sub3_sub4_sub2.boundExtentsZ + -i_9_ : i_9_ + -class246_sub3_sub4_sub2.boundExtentsZ) << -1396718167;
								int i_17_ = (i_15_ ^ 0xffffffff) < (i_16_ ^ 0xffffffff) ? i_15_ : i_16_;
								int i_18_ = class294.anInt2401 * 2 * i_17_;
								if (i_18_ >= i_14_) {
									if ((i_14_ / 2 ^ 0xffffffff) < (i_17_ ^ 0xffffffff)) {
										class246_sub3_sub4_sub2.anInt6435 -= class294.anInt2401;
										if ((class246_sub3_sub4_sub2.anInt6435 ^ 0xffffffff) > -1) {
											class246_sub3_sub4_sub2.anInt6435 = 0;
										}
									} else if ((class246_sub3_sub4_sub2.anInt6435 ^ 0xffffffff) > (i_11_ ^ 0xffffffff)) {
										class246_sub3_sub4_sub2.anInt6435 += class294.anInt2401;
										if ((i_11_ ^ 0xffffffff) > (class246_sub3_sub4_sub2.anInt6435 ^ 0xffffffff)) {
											class246_sub3_sub4_sub2.anInt6435 = i_11_;
										}
									}
								} else {
									class246_sub3_sub4_sub2.anInt6435 /= 2;
								}
							} else if (class246_sub3_sub4_sub2.anInt6435 >= i_11_) {
								if (class246_sub3_sub4_sub2.anInt6435 > 0) {
									class246_sub3_sub4_sub2.anInt6435 -= class294.anInt2401;
									if (class246_sub3_sub4_sub2.anInt6435 < 0) {
										class246_sub3_sub4_sub2.anInt6435 = 0;
									}
								}
							} else {
								class246_sub3_sub4_sub2.anInt6435 += class294.anInt2401;
								if ((class246_sub3_sub4_sub2.anInt6435 ^ 0xffffffff) < (i_11_ ^ 0xffffffff)) {
									class246_sub3_sub4_sub2.anInt6435 = i_11_;
								}
							}
							i_11_ = class246_sub3_sub4_sub2.anInt6435 >> -1583796215;
							if ((i_11_ ^ 0xffffffff) > -2) {
								i_11_ = 1;
							}
						}
						BConfigDefinition.anInt3121 = 0;
						if (i_8_ != i_6_ || i_9_ != i_7_) {
							if ((i_8_ ^ 0xffffffff) < (i_6_ ^ 0xffffffff)) {
								class246_sub3_sub4_sub2.boundExtentsX += i_11_;
								BConfigDefinition.anInt3121 |= 0x4;
								if ((i_8_ ^ 0xffffffff) > (class246_sub3_sub4_sub2.boundExtentsX ^ 0xffffffff)) {
									class246_sub3_sub4_sub2.boundExtentsX = i_8_;
								}
							} else if ((i_8_ ^ 0xffffffff) > (i_6_ ^ 0xffffffff)) {
								BConfigDefinition.anInt3121 |= 0x8;
								class246_sub3_sub4_sub2.boundExtentsX -= i_11_;
								if (class246_sub3_sub4_sub2.boundExtentsX < i_8_) {
									class246_sub3_sub4_sub2.boundExtentsX = i_8_;
								}
							}
							if ((i_11_ ^ 0xffffffff) > -33) {
								AntialiasPreferenceField.anInt3708 = i_10_;
							} else {
								AntialiasPreferenceField.anInt3708 = 2;
							}
							if (i_7_ >= i_9_) {
								if ((i_9_ ^ 0xffffffff) > (i_7_ ^ 0xffffffff)) {
									BConfigDefinition.anInt3121 |= 0x2;
									class246_sub3_sub4_sub2.boundExtentsZ -= i_11_;
									if ((class246_sub3_sub4_sub2.boundExtentsZ ^ 0xffffffff) > (i_9_ ^ 0xffffffff)) {
										class246_sub3_sub4_sub2.boundExtentsZ = i_9_;
									}
								}
							} else {
								BConfigDefinition.anInt3121 |= 0x1;
								class246_sub3_sub4_sub2.boundExtentsZ += i_11_;
								if (i_9_ < class246_sub3_sub4_sub2.boundExtentsZ) {
									class246_sub3_sub4_sub2.boundExtentsZ = i_9_;
								}
							}
						} else {
							AntialiasPreferenceField.anInt3708 = -1;
						}
						if (i_8_ != class246_sub3_sub4_sub2.boundExtentsX || (class246_sub3_sub4_sub2.boundExtentsZ ^ 0xffffffff) != (i_9_ ^ 0xffffffff)) {
							break;
						}
						if (class246_sub3_sub4_sub2.anInt6436 > 0) {
							class246_sub3_sub4_sub2.anInt6436--;
						}
						class246_sub3_sub4_sub2.pathLength--;
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "uh.A(" + i + ',' + bool + ',' + (class246_sub3_sub4_sub2 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	float[]	aFloatArray3389;

	int		anInt3387;

	int		anInt3388;

	Class333(int i, int i_19_) {
		try {
			anInt3387 = i_19_;
			aFloatArray3389 = new float[i_19_ * i];
			anInt3388 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uh.<init>(" + i + ',' + i_19_ + ')');
		}
	}
}

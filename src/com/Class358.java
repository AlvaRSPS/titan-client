/* Class358 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;

public final class Class358 {
	public static boolean			aBoolean3033	= true;
	public static LinkedList		aClass148_3032	= new LinkedList();
	public static IncomingOpcode	aClass58_3029	= new IncomingOpcode(110, -1);
	public static int[]				anIntArray3034	= { 36064, 36065, 36066, 36067, 36068, 36069, 36070, 36071, 36096 };

	public static final void method3887(int i) {
		do {
			try {
				Class287_Sub1.anIntArray3421 = RemoveRoofsPreferenceField.method610(35, 2048, 0.4F, 4, true, true, 8, 8);
				if (i == 110) {
					break;
				}
				method3889(-6, -105, false);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vn.D(" + i + ')');
			}
			break;
		} while (false);
	}

	public static void method3888(byte i) {
		try {
			anIntArray3034 = null;
			if (i != -3) {
				method3889(-15, -67, false);
			}
			aClass148_3032 = null;
			aClass58_3029 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vn.F(" + i + ')');
		}
	}

	public static final boolean method3889(int i, int i_1_, boolean bool) {
		try {
			if (bool != false) {
				anIntArray3034 = null;
			}
			if (i == 11) {
				i = 10;
			}
			GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(i_1_, (byte) 119);
			if (i >= 5 && i <= 8) {
				i = 4;
			}
			return class352.method3853((byte) 49, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vn.C(" + i + ',' + i_1_ + ',' + bool + ')');
		}
	}

	Entity			aClass246_Sub4_3028	= new Entity();

	private Entity	aClass246_Sub4_3031;

	public Class358() {
		try {
			aClass246_Sub4_3028.previous = aClass246_Sub4_3028;
			aClass246_Sub4_3028.next = aClass246_Sub4_3028;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vn.<init>(" + ')');
		}
	}

	public final Entity method3884(int i) {
		try {
			if (i < 18) {
				return null;
			}
			Entity class246_sub4 = aClass246_Sub4_3031;
			if (aClass246_Sub4_3028 == class246_sub4) {
				aClass246_Sub4_3031 = null;
				return null;
			}
			aClass246_Sub4_3031 = class246_sub4.next;
			return class246_sub4;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vn.G(" + i + ')');
		}
	}

	public final int method3885(boolean bool) {
		try {
			if (bool != true) {
				return 36;
			}
			int i = 0;
			for (Entity class246_sub4 = aClass246_Sub4_3028.next; aClass246_Sub4_3028 != class246_sub4; class246_sub4 = class246_sub4.next) {
				i++;
			}
			return i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vn.B(" + bool + ')');
		}
	}

	public final void method3886(byte i) {
		try {
			for (;;) {
				Entity class246_sub4 = aClass246_Sub4_3028.next;
				if (aClass246_Sub4_3028 == class246_sub4) {
					break;
				}
				class246_sub4.method3101(-74);
			}
			aClass246_Sub4_3031 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vn.H(" + i + ')');
		}
	}

	public final Entity method3890(byte i) {
		try {
			if (i != 71) {
				return null;
			}
			Entity class246_sub4 = aClass246_Sub4_3028.next;
			if (class246_sub4 == aClass246_Sub4_3028) {
				aClass246_Sub4_3031 = null;
				return null;
			}
			aClass246_Sub4_3031 = class246_sub4.next;
			return class246_sub4;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vn.A(" + i + ')');
		}
	}

	public final void method3891(Entity class246_sub4, int i) {
		try {
			if (class246_sub4.previous != null) {
				class246_sub4.method3101(-102);
			}
			class246_sub4.next = aClass246_Sub4_3028;
			if (i != 8) {
				method3884(51);
			}
			class246_sub4.previous = aClass246_Sub4_3028.previous;
			class246_sub4.previous.next = class246_sub4;
			class246_sub4.next.previous = class246_sub4;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vn.E(" + (class246_sub4 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}
}

/* Class92 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.anticheat.ReflectionAntiCheat;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5FileRequest;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.SkyboxDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.definition.parser.ItemDefinitionParser;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;

public final class Class92 {

	public static final void method891(int i, int i_0_) {
		GrandExchangeOffer.anInt849 = -1;
		Class42_Sub4.anInt5371 = i;
		Class169.anInt1307 = -1;
	}

	public static final void method892(int i, int i_1_, int i_2_, int screenWidth, boolean isGameView, int screenHeight) {
		if (QuickChatCategory.aClass172ArrayArrayArray5948 == null) {
			client.graphicsToolkit.drawPlayerSquareDot(screenHeight, screenWidth, i_2_, -16777216, (byte) -66, i_1_);
		} else if (Class87.localPlayer.boundExtentsX < 0 || (512 * Class165.mapWidth ^ 0xffffffff) >= (Class87.localPlayer.boundExtentsX ^ 0xffffffff) || (Class87.localPlayer.boundExtentsZ ^ 0xffffffff) > -1 || (Class87.localPlayer.boundExtentsZ ^ 0xffffffff) <= (512
				* Class98_Sub10_Sub7.mapLength ^ 0xffffffff)) {
			client.graphicsToolkit.drawPlayerSquareDot(screenHeight, screenWidth, i_2_, -16777216, (byte) -66, i_1_);
		} else {
			RemoveRoofsPreferenceField.anInt3676++;
			if (Class87.localPlayer != null && Class87.localPlayer.boundExtentsX - 256 * (Class87.localPlayer.getSize(0) + -1) >> 9 == LightIntensityDefinitionParser.anInt2024 && (Class87.localPlayer.boundExtentsZ - (Class87.localPlayer.getSize(0) * 256 + -256) >> 9
					^ 0xffffffff) == (Class246_Sub3_Sub1_Sub2.anInt6251 ^ 0xffffffff)) {
				Class246_Sub3_Sub1_Sub2.anInt6251 = -1;
				LightIntensityDefinitionParser.anInt2024 = -1;
				RSByteBuffer.method1216(-17470);
			}
			Class98_Sub44.method1512(true);
			if (!isGameView) {
				ItemDefinitionParser.method2716(-9627);
			}
			Class246_Sub1.method2966(66);
			Class151_Sub3.method2453(i_2_, true, screenHeight, 100, screenWidth, i_1_);
			screenWidth = Class332_Sub2.anInt5421;
			i_1_ = Class151_Sub8.anInt5016;
			Class16.anInt197 = Class16.orthoZoom;
			screenHeight = Queue.anInt1612;
			i_2_ = Class98_Sub31_Sub2.anInt5824;
			if (Class98_Sub46_Sub20_Sub2.cameraMode == 1) {
				int cameraY = (int) Class119_Sub4.aFloat4740;
				if (Font.anInt372 >> 861735464 > cameraY) {
					cameraY = Font.anInt372 >> -1370935128;
				}
				if (GroupProgressMonitor.aBooleanArray3410[4] && (cameraY ^ 0xffffffff) > (Class98_Sub10_Sub13.anIntArray5603[4] + 128 ^ 0xffffffff)) {
					cameraY = 128 + Class98_Sub10_Sub13.anIntArray5603[4];
				}
				int cameraX = 0x3fff & Class98_Sub10_Sub9.anInt5581 + (int) RsFloatBuffer.aFloat5794;
				OpenGlArrayBufferPointer.renderCamera(false, cameraX, (cameraY >> 3) + 600 << 2, Class224_Sub3_Sub1.cameraYPosition, screenWidth, Minimap.cameraXPosition, cameraY, -200 + StrongReferenceMCNode.getHeight(Font.localPlane, Class87.localPlayer.boundExtentsZ,
						Class87.localPlayer.boundExtentsX, i ^ 0xd76));
			} else if (Class98_Sub46_Sub20_Sub2.cameraMode == 4) {
				int cameraY = (int) Class119_Sub4.aFloat4740;
				if (cameraY < Font.anInt372 >> 8) {
					cameraY = Font.anInt372 >> 8;
				}
				if (GroupProgressMonitor.aBooleanArray3410[4] && 128 + Class98_Sub10_Sub13.anIntArray5603[4] > cameraY) {
					cameraY = Class98_Sub10_Sub13.anIntArray5603[4] - -128;
				}
				int cameraX = (int) RsFloatBuffer.aFloat5794 & 0x3fff;
				OpenGlArrayBufferPointer.renderCamera(false, cameraX, (cameraY >> 3) + 600 << 2, Class224_Sub3_Sub1.cameraYPosition, screenWidth, Minimap.cameraXPosition, cameraY, StrongReferenceMCNode.getHeight(Font.localPlane, Js5Client.anInt1051, StrongReferenceMCNode.anInt6295, 24111) - 200);
			} else if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) == -6) {
				Class50.method484(screenWidth, i ^ 0x5301);
			}
			int xCamPosTile = Class98_Sub46_Sub10.xCamPosTile;
			int i_10_ = AdvancedMemoryCache.anInt601;
			int yCamPosTile = SpriteLoadingScreenElement.yCamPosTile;
			int i_12_ = Mob.anInt6357;
			int camX = Class186.cameraX;
			for (int cameraMode = 0; (cameraMode ^ 0xffffffff) > -6; cameraMode++) {
				if (GroupProgressMonitor.aBooleanArray3410[cameraMode]) {
					int i_15_ = (int) (Math.random() * (2 * aa_Sub3.anIntArray3571[cameraMode] + 1) - aa_Sub3.anIntArray3571[cameraMode] + Math.sin(QuickChatCategoryParser.anIntArray1597[cameraMode] * (GraphicsBuffer.anIntArray4109[cameraMode] / 100.0))
							* Class98_Sub10_Sub13.anIntArray5603[cameraMode]);
					System.out.println("i:::: " + i_15_);
					if (cameraMode == 3) {
						Class186.cameraX = 0x3fff & Class186.cameraX + i_15_;
					}
					if (cameraMode == 0) {
						Class98_Sub46_Sub10.xCamPosTile += i_15_ << 2;
					}
					if (cameraMode == 2) {
						SpriteLoadingScreenElement.yCamPosTile += i_15_ << 2;
					}
					if (cameraMode == 4) {
						Mob.anInt6357 += i_15_;
						if (Mob.anInt6357 >= 1024) {
							if (Mob.anInt6357 > 3072) {
								Mob.anInt6357 = 3072;
							}
						} else {
							Mob.anInt6357 = 1024;
						}
					}
					if (cameraMode == 1) {
						AdvancedMemoryCache.anInt601 += i_15_ << 2;
					}
				}
			}
			if (Class98_Sub46_Sub10.xCamPosTile < 0) {
				Class98_Sub46_Sub10.xCamPosTile = 0;
			}
			if ((BConfigDefinition.anInt3112 << 9) - 1 < Class98_Sub46_Sub10.xCamPosTile) {
				Class98_Sub46_Sub10.xCamPosTile = (BConfigDefinition.anInt3112 << 9) - 1;
			}
			if (SpriteLoadingScreenElement.yCamPosTile < 0) {
				SpriteLoadingScreenElement.yCamPosTile = 0;
			}
			if ((SpriteLoadingScreenElement.yCamPosTile ^ 0xffffffff) < (-1 + (Class64_Sub9.anInt3662 << 9) ^ 0xffffffff)) {
				SpriteLoadingScreenElement.yCamPosTile = (Class64_Sub9.anInt3662 << 9) - 1;
			}
			Class246_Sub3_Sub4_Sub1.method3025((byte) 72);
			Class329.method3708(i + -21338);
			client.graphicsToolkit.setClip(i_1_, i_2_, screenHeight + i_1_, screenWidth + i_2_);
			Minimap.clipPlanes(i ^ ~0x5178, true);
			if (!OpenGLHeap.aBoolean6079) {
				client.graphicsToolkit.ya();
				int colour = Class284_Sub1_Sub2.layerColour;
				if (OpenGlGround.aClass346_5202 == null) {

					client.graphicsToolkit.clearImage(colour);
				} else {
					client.graphicsToolkit.setAmbientIntensity(1.0F);
					client.graphicsToolkit.setSun(16777215, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F);
					OpenGlGround.aClass346_5202.method3831(NativeOpenGlElementArrayBuffer.anInt3278 << 3, Mob.anInt6357, Class186.cameraX, i_2_, i + -45783, i_1_, client.graphicsToolkit, screenWidth, screenHeight, colour);
				}
			} else {
				Class156_Sub2.method2500(Class284_Sub1_Sub2.layerColour, i + -21337);
				if ((Class16.anInt197 ^ 0xffffffff) != (Class48_Sub2_Sub1.anInt5525 ^ 0xffffffff)) {
					Class358.aBoolean3033 = true;
				}
				Class48_Sub2_Sub1.anInt5525 = Class16.anInt197;
			}
			Class128.method2224(i ^ 0xbf1);
			SunDefinition.aClass111_1986.method2093(Class98_Sub46_Sub10.xCamPosTile, AdvancedMemoryCache.anInt601, SpriteLoadingScreenElement.yCamPosTile, 0x3fff & -Mob.anInt6357, -Class186.cameraX & 0x3fff, 0x3fff & -Class308.anInt2584);
			client.graphicsToolkit.a(SunDefinition.aClass111_1986);
			client.graphicsToolkit.DA(i_1_ - -(screenHeight / 2), screenWidth / 2 + i_2_, Class98_Sub10_Sub14.anInt5610 << 1, Class98_Sub10_Sub14.anInt5610 << 1);
			ProceduralTextureSource.method3208(Class98_Sub10_Sub14.anInt5610 << 1, -128, Class98_Sub10_Sub14.anInt5610 << 1, i_2_ - -(screenWidth / 2), i_1_ + screenHeight / 2);
			Class98_Sub46_Sub6.method1548(0x3fff & -Class186.cameraX, AdvancedMemoryCache.anInt601, -Mob.anInt6357 & 0x3fff, 0x3fff & -Class308.anInt2584, (byte) -78, SpriteLoadingScreenElement.yCamPosTile, Class98_Sub46_Sub10.xCamPosTile);
			byte roofsRemoved = client.preferences.removeRoofsMode.getValue((byte) 126) != 2 ? (byte) 1 : (byte) RemoveRoofsPreferenceField.anInt3676;
			if (!OpenGLHeap.aBoolean6079) {
				SkyboxDefinition.method535(Queue.timer, Class98_Sub46_Sub10.xCamPosTile, AdvancedMemoryCache.anInt601, SpriteLoadingScreenElement.yCamPosTile, OutputStream_Sub2.aByteArrayArrayArray41, Class204.anIntArray1551, Class336.anIntArray2826, Class287.anIntArray2195, Class295.anIntArray2409,
						Class48_Sub1_Sub2.anIntArray5518, 1 + Class87.localPlayer.plane, roofsRemoved, Class87.localPlayer.boundExtentsX >> 9, Class87.localPlayer.boundExtentsZ >> 9, client.preferences.flickeringEffects.getValue((byte) 121) == 0, true, VarClientStringsDefinitionParser.aBoolean1839
								? Class16.anInt197 : -1, 0, false);
			} else {
				Class216.method2797(-Mob.anInt6357 & 0x3fff, 0x3fff & -Class308.anInt2584, -Class186.cameraX & 0x3fff, i + 4643);
				Class154.method2493(AdvancedMemoryCache.anInt601, 1 + Class87.localPlayer.plane, OutputStream_Sub2.aByteArrayArrayArray41, Class16.anInt197, Class98_Sub46_Sub10.xCamPosTile, Class48_Sub1_Sub2.anIntArray5518, (client.preferences.flickeringEffects.getValue((byte) 126)
						^ 0xffffffff) == -1, Class204.anIntArray1551, roofsRemoved, true, Class87.localPlayer.boundExtentsX >> 9, Class87.localPlayer.boundExtentsZ >> 9, Class287.anIntArray2195, Queue.timer, Class295.anIntArray2409, SpriteLoadingScreenElement.yCamPosTile,
						Class336.anIntArray2826, true);
			}
			Class128.method2224(22696);
			if ((client.clientState ^ 0xffffffff) == -11) {
				Class290.method3411(i_1_, (byte) 7, i_2_, screenWidth, 256, 256, screenHeight);
				NodeInteger.method1450(256, screenHeight, screenWidth, 256, (byte) -124, i_1_, i_2_);
				RenderAnimDefinition.method3479(256, screenHeight, -7957, screenWidth, i_1_, i_2_, 256);
				Js5FileRequest.method1594(i_2_, i_1_, screenWidth, screenHeight, (byte) 116);
			}
			Class114.method2148();
			Class186.cameraX = camX;
			Class98_Sub46_Sub10.xCamPosTile = xCamPosTile;
			SpriteLoadingScreenElement.yCamPosTile = yCamPosTile;
			Mob.anInt6357 = i_12_;
			AdvancedMemoryCache.anInt601 = i_10_;
			if (Class98_Sub10_Sub12.aBoolean5599 && (client.js5Client.getPriorityRequestCount(-1) ^ 0xffffffff) == -1) {
				Class98_Sub10_Sub12.aBoolean5599 = false;
			}
			if (Class98_Sub10_Sub12.aBoolean5599) {
				client.graphicsToolkit.drawPlayerSquareDot(screenHeight, screenWidth, i_2_, -16777216, (byte) -66, i_1_);
				Class246_Sub2.draw(i + -21457, Class98_Sub46_Sub10.p12FullMetrics, Class195.p12Full, false, client.graphicsToolkit, TextResources.LOADING.getText(client.gameLanguage, (byte) 25));
			}
			Minimap.clipPlanes(-546, false);

		}
	}

	public static final boolean method893(int i) {
		ReflectionRequest class98_sub19 = (ReflectionRequest) ReflectionAntiCheat.requestQueue.getFirst(32);
		if (class98_sub19 == null) {
			return false;
		}
		for (int i_19_ = 0; (class98_sub19.opCount ^ 0xffffffff) < (i_19_ ^ 0xffffffff); i_19_++) {
			if (class98_sub19.fieldRequests[i_19_] != null && class98_sub19.fieldRequests[i_19_].status == 0) {
				return false;
			}
			if (class98_sub19.methodRequests[i_19_] != null && (class98_sub19.methodRequests[i_19_].status ^ 0xffffffff) == -1) {
				return false;
			}
		}
		return true;
	}

	public static final void method898(boolean bool, byte[][] is, Class305_Sub1 class305_sub1) {
		int i = client.unknown.length;
		for (int i_33_ = 0; (i_33_ ^ 0xffffffff) > (i ^ 0xffffffff); i_33_++) {
			byte[] is_34_ = is[i_33_];
			if (is_34_ != null) {
				int i_35_ = (HitmarksDefinitionParser.regionPositionHash[i_33_] >> 1369954824) * 64 - Class272.gameSceneBaseX;
				int i_36_ = -aa_Sub2.gameSceneBaseY + 64 * (HitmarksDefinitionParser.regionPositionHash[i_33_] & 0xff);
				Class128.method2224(22696);
				class305_sub1.method3591(VarPlayerDefinition.clipMaps, i_36_, (byte) -19, is_34_, client.graphicsToolkit, i_35_);
			}
		}
	}

	boolean		aBoolean732;
	boolean		aBoolean736;
	boolean		aBoolean753;
	boolean		aBoolean759;
	boolean		aBoolean776;
	boolean		aBoolean778;
	boolean		aBoolean783;
	boolean		aBoolean789;
	boolean		aBoolean791;
	private int	anInt726;
	int			anInt727;
	int			anInt729;
	int			anInt730;
	int			anInt731;
	int			anInt733;
	int			anInt734;
	int			anInt737;
	int			anInt739	= 0;
	private int	anInt740;
	int			anInt741;
	int			anInt742;
	int			anInt743;
	private int	anInt744;
	int			anInt745;
	int			anInt746;
	private int	anInt748;
	private int	anInt749;
	int			anInt750;
	private int	anInt751;
	int			anInt752;
	private int	anInt755;
	int			anInt756;
	int			anInt757;
	int			anInt758;
	int			anInt760;
	int			anInt761;
	int			anInt762;
	int			anInt764;
	int			anInt765;
	int			anInt766;
	private int	anInt767;
	private int	anInt769;
	int			anInt770;
	int			anInt771;
	private int	anInt773;
	int			anInt774;
	int			anInt775;
	int			anInt777;
	int			anInt779;
	int			anInt780;
	int			anInt781;
	int			anInt782;
	int			anInt784;
	int			anInt785;
	int			anInt787;
	int			anInt788;
	int			anInt790;
	int			anInt792;
	int[]		anIntArray728;
	int[]		anIntArray735;
	int[]		anIntArray768;

	int[]		anIntArray772;

	short		aShort747;

	short		aShort754;

	short		aShort763;

	short		aShort786;

	public Class92() {
		anInt729 = -1;
		anInt749 = 100;
		anInt748 = 100;
		aBoolean732 = true;
		aBoolean753 = true;
		anInt764 = -1;
		aBoolean736 = false;
		anInt762 = -1;
		aBoolean759 = true;
		anInt752 = 0;
		anInt773 = 100;
		aBoolean778 = true;
		anInt774 = -2;
		anInt745 = -1;
		anInt784 = 0;
		anInt746 = -1;
		anInt775 = -1;
		anInt782 = -2;
		anInt767 = 100;
		aBoolean776 = true;
		aBoolean783 = true;
		aBoolean791 = false;
		aBoolean789 = false;
	}

	public final void decode(int i, RSByteBuffer class98_sub22) {
		for (;;) {
			int i_20_ = class98_sub22.readUnsignedByte((byte) -128);
			if ((i_20_ ^ 0xffffffff) == -1) {
				break;
			}
			decode(class98_sub22, i_20_, (byte) -112);
		}
	}

	private final void decode(RSByteBuffer buffer, int i, byte i_21_) {
		try {
			if (i == 1) {
				aShort747 = (short) buffer.readShort((byte) 127);
				aShort786 = (short) buffer.readShort((byte) 127);
				aShort763 = (short) buffer.readShort((byte) 127);
				aShort754 = (short) buffer.readShort((byte) 127);
				int i_22_ = 3;
				aShort763 <<= i_22_;
				aShort786 <<= i_22_;
				aShort747 <<= i_22_;
				aShort754 <<= i_22_;
			} else if ((i ^ 0xffffffff) == -3) {
				buffer.readUnsignedByte((byte) 47);
			} else if (i == 3) {
				anInt770 = buffer.readInt(-2);
				anInt731 = buffer.readInt(-2);
			} else if (i != 4) {
				if ((i ^ 0xffffffff) == -6) {
					anInt780 = anInt788 = buffer.readShort((byte) 127) << -577553204 << 1586483106;
				} else if (i != 6) {
					if (i == 7) {
						anInt766 = buffer.readShort((byte) 127);
						anInt787 = buffer.readShort((byte) 127);
					} else if (i != 8) {
						if (i == 9) {
							int i_23_ = buffer.readUnsignedByte((byte) 41);
							anIntArray728 = new int[i_23_];
							for (int i_24_ = 0; i_24_ < i_23_; i_24_++) {
								anIntArray728[i_24_] = buffer.readShort((byte) 127);
							}
						} else if ((i ^ 0xffffffff) == -11) {
							int i_25_ = buffer.readUnsignedByte((byte) 121);
							anIntArray772 = new int[i_25_];
							for (int i_26_ = 0; (i_25_ ^ 0xffffffff) < (i_26_ ^ 0xffffffff); i_26_++) {
								anIntArray772[i_26_] = buffer.readShort((byte) 127);
							}
						} else if ((i ^ 0xffffffff) != -13) {
							if (i == 13) {
								anInt782 = buffer.readSignedByte((byte) -19);
							} else if (i == 14) {
								anInt784 = buffer.readShort((byte) 127);
							} else if ((i ^ 0xffffffff) == -16) {
								anInt729 = buffer.readShort((byte) 127);
							} else if ((i ^ 0xffffffff) != -17) {
								if ((i ^ 0xffffffff) == -18) {
									anInt764 = buffer.readShort((byte) 127);
								} else if (i != 18) {
									if ((i ^ 0xffffffff) == -20) {
										anInt752 = buffer.readUnsignedByte((byte) -111);
									} else if (i == 20) {
										anInt773 = buffer.readUnsignedByte((byte) 107);
									} else if (i == 21) {
										anInt767 = buffer.readUnsignedByte((byte) 68);
									} else if ((i ^ 0xffffffff) != -23) {
										if ((i ^ 0xffffffff) == -24) {
											anInt748 = buffer.readUnsignedByte((byte) -119);
										} else if ((i ^ 0xffffffff) == -25) {
											aBoolean759 = false;
										} else if (i != 25) {
											if (i != 26) {
												if (i != 27) {
													if (i == 28) {
														anInt749 = buffer.readUnsignedByte((byte) -117);
													} else if (i != 29) {
														if (i != 30) {
															if (i == 31) {
																anInt780 = buffer.readShort((byte) 127) << -1910098324 << 660699586;
																anInt788 = buffer.readShort((byte) 127) << 1930543596 << 296157090;
															} else if ((i ^ 0xffffffff) != -33) {
																if (i == 33) {
																	aBoolean789 = true;
																} else if (i == 34) {
																	aBoolean776 = false;
																}
															} else {
																aBoolean778 = false;
															}
														} else {
															aBoolean791 = true;
														}
													} else {
														buffer.readUShort(false);
													}
												} else {
													anInt775 = buffer.readShort((byte) 127) << -1135326388 << 1935821730;
												}
											} else {
												aBoolean753 = false;
											}
										} else {
											int i_27_ = buffer.readUnsignedByte((byte) -124);
											anIntArray735 = new int[i_27_];
											for (int i_28_ = 0; (i_27_ ^ 0xffffffff) < (i_28_ ^ 0xffffffff); i_28_++) {
												anIntArray735[i_28_] = buffer.readShort((byte) 127);
											}
										}
									} else {
										anInt745 = buffer.readInt(-2);
									}
								} else {
									anInt760 = buffer.readInt(-2);
								}
							} else {
								aBoolean732 = (buffer.readUnsignedByte((byte) 13) ^ 0xffffffff) == -2;
								anInt746 = buffer.readShort((byte) 127);
								anInt762 = buffer.readShort((byte) 127);
								aBoolean783 = buffer.readUnsignedByte((byte) -120) == 1;
							}
						} else {
							anInt774 = buffer.readSignedByte((byte) -19);
						}
					} else {
						anInt750 = buffer.readShort((byte) 127);
						anInt790 = buffer.readShort((byte) 127);
					}
				} else {
					anInt726 = buffer.readInt(-2);
					anInt744 = buffer.readInt(-2);
				}
			} else {
				anInt739 = buffer.readUnsignedByte((byte) 81);
				anInt792 = buffer.readSignedByte((byte) -19);
			}
			if (i_21_ >= -101) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fp.C(" + (buffer != null ? "{...}" : "null") + ',' + i + ',' + i_21_ + ')');
		}
	}

	public final void method897(byte i) {
		do {
			try {
				if ((anInt774 ^ 0xffffffff) < 1 || anInt782 > -2) {
					aBoolean736 = true;
				}
				anInt741 = (anInt726 & 0xffc52c) >> -731747568;
				anInt751 = 0xff & anInt744 >> -1356228592;
				anInt730 = -anInt741 + anInt751;
				anInt740 = (0xff9a & anInt744) >> 1249635720;
				anInt757 = (anInt726 & 0xff64) >> 547175144;
				anInt734 = -anInt757 + anInt740;
				anInt771 = anInt726 & 0xff;
				anInt755 = 0xff & anInt744;
				anInt769 = 0xff & anInt744 >> 1155880536;
				if (i > 56) {
					anInt737 = anInt755 - anInt771;
					anInt756 = anInt726 >> 328179352 & 0xff;
					anInt765 = -anInt756 + anInt769;
					if (anInt760 != 0) {
						anInt761 = anInt773 * anInt787 / 100;
						anInt758 = anInt787 * anInt767 / 100;
						if (anInt761 == 0) {
							anInt761 = 1;
						}
						anInt743 = (-anInt741 + -(anInt730 / 2) + (anInt760 >> 1956144528 & 0xff) << -1025810040) / anInt761;
						anInt733 = (-(anInt734 / 2) + -anInt757 + (anInt760 >> -21137720 & 0xff) << -1200703608) / anInt761;
						anInt727 = (-anInt771 + -(anInt737 / 2) + (0xff & anInt760) << -178823608) / anInt761;
						if ((anInt758 ^ 0xffffffff) == -1) {
							anInt758 = 1;
						}
						Class92 class92_29_ = this;
						class92_29_.anInt743 = class92_29_.anInt743 + ((anInt743 ^ 0xffffffff) >= -1 ? 4 : -4);
						Class92 class92_30_ = this;
						class92_30_.anInt733 = class92_30_.anInt733 + (anInt733 <= 0 ? 4 : -4);
						Class92 class92_31_ = this;
						class92_31_.anInt727 = class92_31_.anInt727 + (anInt727 <= 0 ? 4 : -4);
						anInt779 = ((anInt760 >> -686226984 & 0xff) - (anInt765 / 2 + anInt756) << -121018712) / anInt758;
						Class92 class92_32_ = this;
						class92_32_.anInt779 = class92_32_.anInt779 + (anInt779 <= 0 ? 4 : -4);
					}
					if (anInt775 != -1) {
						anInt777 = anInt749 * anInt787 / 100;
						if ((anInt777 ^ 0xffffffff) == -1) {
							anInt777 = 1;
						}
						anInt742 = (-anInt780 + -((anInt788 - anInt780) / 2) + anInt775) / anInt777;
					}
					if ((anInt745 ^ 0xffffffff) == 0) {
						break;
					}
					anInt785 = anInt787 * anInt748 / 100;
					if ((anInt785 ^ 0xffffffff) == -1) {
						anInt785 = 1;
					}
					anInt781 = (anInt745 - (-anInt770 + anInt731) / 2 - anInt770) / anInt785;
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fp.H(" + i + ')');
			}
			break;
		} while (false);
	}
}

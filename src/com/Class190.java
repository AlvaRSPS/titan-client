
/* Class190 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.definition.parser.IdentikitDefinitionParser;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;
import com.jagex.game.client.preferences.ParticlesPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.input.impl.AwtKeyListener;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.ground.NativeGround;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;

public final class Class190 {
	public static int[] anIntArray1463 = new int[1];

	public static final byte method2644(int i, int i_0_, int i_1_) {
		try {
			if (i_0_ <= 54) {
				method2644(8, 49, -34);
			}
			if ((i ^ 0xffffffff) != -10) {
				return (byte) 0;
			}
			if ((i_1_ & 0x1) != 0) {
				return (byte) 2;
			}
			return (byte) 1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mj.E(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method2646(int i) {
		do {
			try {
				anIntArray1463 = null;
				// poolVar = null;
				if (i == 27387) {
					break;
				}
				method2646(102);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mj.D(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method2648(long l, int i) {
		try {
			if (QuickChatCategory.aClass172ArrayArrayArray5948 != null) {
				if (Class98_Sub46_Sub20_Sub2.cameraMode == 1 || Class98_Sub46_Sub20_Sub2.cameraMode == 5) {
					FloorUnderlayDefinitionParser.method2489(l, (byte) -102);
				} else if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) == -5) {
					ParticlesPreferenceField.method573(-1, l);
				}
			}
			ParticleManager.process(Queue.timer, true, client.graphicsToolkit);
			if (client.topLevelInterfaceId != -1) {
				RtInterface.method3844(client.topLevelInterfaceId, -47);
			}
			for (int i_40_ = 0; (Class69_Sub2.anInt5335 ^ 0xffffffff) < (i_40_ ^ 0xffffffff); i_40_++) {
				if (aa_Sub3.isDirty[i_40_]) {
					Class98_Sub10_Sub20.aBooleanArray5639[i_40_] = true;
				}
				Class232.aBooleanArray1741[i_40_] = aa_Sub3.isDirty[i_40_];
				aa_Sub3.isDirty[i_40_] = false;
			}
			AwtKeyListener.anInt3803 = Queue.timer;
			Class98_Sub1.method946(-1, -125, -1, null);
			GraphicsDefinitionParser.method3563(-1, null, -1, 60);
			if ((client.topLevelInterfaceId ^ 0xffffffff) != 0) {
				Class69_Sub2.anInt5335 = 0;
				Queue.method2791((byte) 118);
			}
			client.graphicsToolkit.clearClip();
			Class98_Sub10_Sub22.method1069(256, client.graphicsToolkit);
			int i_41_ = IdentikitDefinitionParser.method824((byte) -72);
			if (i_41_ == -1) {
				i_41_ = Class21_Sub2.cursorId;
			}
			if ((i_41_ ^ 0xffffffff) == 0) {
				i_41_ = OutputStream_Sub2.anInt39;
			}
			Font.method401(i_41_, true);
			int i_42_ = Class87.localPlayer.getSize(0) << 2087692776;
			Class169.method2535(GameDefinition.anInt2099, Class87.localPlayer.plane, Class87.localPlayer.boundExtentsZ - -i_42_, Class87.localPlayer.boundExtentsX + i_42_, (byte) 112);
			GameDefinition.anInt2099 = 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mj.A(" + l + ',' + i + ')');
		}
	}

	boolean						aBoolean1470	= true;
	private Class15				aClass15_1464;
	private NativeToolkit		aHa_Sub3_1465;
	int							anInt1460;
	private int					anInt1462;
	private int					anInt1466;
	private int					anInt1467;
	private int					anInt1469		= -1;

	private int					anInt1472;

	private Interface2_Impl2	anInterface2_Impl2_1471;

	private Interface4_Impl2	anInterface4_Impl2_1461;

	Class190(NativeToolkit var_ha_Sub3, Class15 class15, NativeGround var_s_Sub2, int i, int i_17_, int i_18_, int i_19_, int i_20_) {
		try {
			aClass15_1464 = class15;
			anInt1466 = i_20_;
			anInt1467 = i_19_;
			aHa_Sub3_1465 = var_ha_Sub3;
			int i_21_ = 1 << i_18_;
			int i_22_ = 0;
			int i_23_ = i << i_18_;
			int i_24_ = i_17_ << i_18_;
			for (int i_25_ = 0; i_21_ > i_25_; i_25_++) {
				int i_26_ = var_s_Sub2.width * (i_24_ - -i_25_) + i_23_;
				for (int i_27_ = 0; (i_21_ ^ 0xffffffff) < (i_27_ ^ 0xffffffff); i_27_++) {
					short[] is = var_s_Sub2.aShortArrayArray5230[i_26_++];
					if (is != null) {
						i_22_ += is.length;
					}
				}
			}
			if (i_22_ > 0) {
				anInt1472 = 2147483647;
				anInt1462 = -2147483648;
				anInterface2_Impl2_1471 = aHa_Sub3_1465.method1990((byte) 84, false);
				anInterface2_Impl2_1471.method76(i_22_, 20779);
				for (int i_28_ = 0; i_28_ < 4; i_28_++) {
					Buffer buffer = anInterface2_Impl2_1471.method78(true, -102);
					if (buffer != null) {
						Stream stream = aHa_Sub3_1465.method2043(24022, buffer);
						if (Stream.a()) {
							for (int i_29_ = 0; (i_29_ ^ 0xffffffff) > (i_21_ ^ 0xffffffff); i_29_++) {
								int i_30_ = (i_24_ - -i_29_) * var_s_Sub2.width + i_23_;
								for (int i_31_ = 0; (i_21_ ^ 0xffffffff) < (i_31_ ^ 0xffffffff); i_31_++) {
									short[] is = var_s_Sub2.aShortArrayArray5230[i_30_++];
									if (is != null) {
										for (int i_32_ = 0; (is.length ^ 0xffffffff) < (i_32_ ^ 0xffffffff); i_32_++) {
											int i_33_ = 0xffff & is[i_32_];
											if (i_33_ > anInt1462) {
												anInt1462 = i_33_;
											}
											if (anInt1472 > i_33_) {
												anInt1472 = i_33_;
											}
											stream.c(i_33_);
										}
									}
								}
							}
						} else {
							for (int i_34_ = 0; i_34_ < i_21_; i_34_++) {
								int i_35_ = i_23_ + (i_24_ - -i_34_) * var_s_Sub2.width;
								for (int i_36_ = 0; i_36_ < i_21_; i_36_++) {
									short[] is = var_s_Sub2.aShortArrayArray5230[i_35_++];
									if (is != null) {
										for (short element : is) {
											int i_38_ = element & 0xffff;
											if ((i_38_ ^ 0xffffffff) > (anInt1472 ^ 0xffffffff)) {
												anInt1472 = i_38_;
											}
											if ((i_38_ ^ 0xffffffff) < (anInt1462 ^ 0xffffffff)) {
												anInt1462 = i_38_;
											}
											stream.d(i_38_);
										}
									}
								}
							}
						}
						stream.flush();
						if (anInterface2_Impl2_1471.method79((byte) -120)) {
							break;
						}
					}
				}
				anInt1460 = i_22_ / 3;
			} else {
				anInt1460 = 0;
				anInterface4_Impl2_1461 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mj.<init>(" + (var_ha_Sub3 != null ? "{...}" : "null") + ',' + (class15 != null ? "{...}" : "null") + ',' + (var_s_Sub2 != null ? "{...}" : "null") + ',' + i + ',' + i_17_ + ',' + i_18_ + ',' + i_19_ + ',' + i_20_ + ')');
		}
	}

	public final void method2643(int i) {
		try {
			if (i != 30925) {
				method2648(-107L, -52);
			}
			method2645(-18732, anInterface2_Impl2_1471, anInt1460);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mj.C(" + i + ')');
		}
	}

	public final void method2645(int i, Interface2_Impl2 interface2_impl2, int i_2_) {
		try {
			if ((i_2_ ^ 0xffffffff) < -1) {
				method2647(128);
				aHa_Sub3_1465.method2005(anInterface4_Impl2_1461, 125);
				aHa_Sub3_1465.method1973(Class336.aClass232_2822, 1 + -anInt1472 + anInt1462, 0, 26810, interface2_impl2, anInt1472, i_2_);
			}
			if (i != -18732) {
				method2646(41);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mj.B(" + i + ',' + (interface2_impl2 != null ? "{...}" : "null") + ',' + i_2_ + ')');
		}
	}

	private final void method2647(int i) {
		try {
			if (i != 128) {
				anInterface4_Impl2_1461 = null;
			}
			if (aBoolean1470) {
				aBoolean1470 = false;
				byte[] is = aClass15_1464.aByteArray176;
				int i_3_ = 0;
				int i_4_ = aClass15_1464.anInt180;
				int i_5_ = aClass15_1464.anInt180 * anInt1466 + anInt1467;
				for (int i_6_ = -128; (i_6_ ^ 0xffffffff) > -1; i_6_++) {
					i_3_ = -i_3_ + (i_3_ << -266560856);
					for (int i_7_ = -128; i_7_ < 0; i_7_++) {
						if (is[i_5_++] != 0) {
							i_3_++;
						}
					}
					i_5_ += i_4_ - 128;
				}
				if (anInterface4_Impl2_1461 != null && (i_3_ ^ 0xffffffff) == (anInt1469 ^ 0xffffffff)) {
					aBoolean1470 = false;
				} else {
					anInt1469 = i_3_;
					i_5_ = anInt1467 - -(anInt1466 * i_4_);
					int i_8_ = 0;
					if (!aHa_Sub3_1465.method1942(i + -128, Class53_Sub1.aClass164_3633, Class162.aClass162_1266)) {
						if (MonoOrStereoPreferenceField.anIntArray3639 == null) {
							MonoOrStereoPreferenceField.anIntArray3639 = new int[16384];
						}
						int[] is_9_ = MonoOrStereoPreferenceField.anIntArray3639;
						for (int i_10_ = -128; (i_10_ ^ 0xffffffff) > -1; i_10_++) {
							for (int i_11_ = -128; (i_11_ ^ 0xffffffff) > -1; i_11_++) {
								if ((is[i_5_] ^ 0xffffffff) != -1) {
									is_9_[i_8_++] = 1140850688;
								} else {
									int i_12_ = 0;
									if (is[i_5_ - 1] != 0) {
										i_12_++;
									}
									if (is[i_5_ - -1] != 0) {
										i_12_++;
									}
									if (is[i_5_ - i_4_] != 0) {
										i_12_++;
									}
									if ((is[i_4_ + i_5_] ^ 0xffffffff) != -1) {
										i_12_++;
									}
									is_9_[i_8_++] = i_12_ * 17 << -1644342728;
								}
								i_5_++;
							}
							i_5_ += aClass15_1464.anInt180 + -128;
						}
						if (anInterface4_Impl2_1461 != null) {
							anInterface4_Impl2_1461.method49(17779, 0, 128, MonoOrStereoPreferenceField.anIntArray3639, 0, 0, 128, 128);
						} else {
							anInterface4_Impl2_1461 = aHa_Sub3_1465.method2012(128, 128, (byte) 31, MonoOrStereoPreferenceField.anIntArray3639, false);
							anInterface4_Impl2_1461.method46(false, false, i + -61);
						}
					} else {
						if (LoginOpcode.aByteArray2492 == null) {
							LoginOpcode.aByteArray2492 = new byte[16384];
						}
						byte[] is_13_ = LoginOpcode.aByteArray2492;
						for (int i_14_ = -128; (i_14_ ^ 0xffffffff) > -1; i_14_++) {
							for (int i_15_ = -128; (i_15_ ^ 0xffffffff) > -1; i_15_++) {
								if ((is[i_5_] ^ 0xffffffff) != -1) {
									is_13_[i_8_++] = (byte) 68;
								} else {
									int i_16_ = 0;
									if ((is[-1 + i_5_] ^ 0xffffffff) != -1) {
										i_16_++;
									}
									if (is[i_5_ - -1] != 0) {
										i_16_++;
									}
									if ((is[i_5_ - i_4_] ^ 0xffffffff) != -1) {
										i_16_++;
									}
									if (is[i_4_ + i_5_] != 0) {
										i_16_++;
									}
									is_13_[i_8_++] = (byte) (17 * i_16_);
								}
								i_5_++;
							}
							i_5_ += aClass15_1464.anInt180 - 128;
						}
						if (anInterface4_Impl2_1461 == null) {
							anInterface4_Impl2_1461 = aHa_Sub3_1465.method2053(128, Class53_Sub1.aClass164_3633, (byte) 87, LoginOpcode.aByteArray2492, false, 128);
							anInterface4_Impl2_1461.method46(false, false, 124);
						} else {
							anInterface4_Impl2_1461.method41(128, 0, 128, 0, 128, -26946, LoginOpcode.aByteArray2492, Class53_Sub1.aClass164_3633, 0);
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mj.F(" + i + ')');
		}
	}
}

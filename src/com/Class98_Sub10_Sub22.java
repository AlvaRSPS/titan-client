/* Class98_Sub10_Sub22 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.MemoryCacheNodeFactory;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5Index;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.IdentikitDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.SkyboxDefinition;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.preferences.BuildAreaPreferenceField;
import com.jagex.game.client.preferences.LightningDetailPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.constants.BuildType;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class98_Sub10_Sub22 extends Class98_Sub10 {
	public static OutgoingOpcode	aClass171_5652;
	public static volatile Object	anObject5653	= null;

	static {
		aClass171_5652 = new OutgoingOpcode(23, 2);
	}

	public static final void method1069(int i, RSToolkit var_ha) {
		if (Player.menuOpen) {
			Class116.method2159((byte) 80, var_ha);
		} else {
			Class96.method926(2, var_ha);
		}
	}

	public static final void method1070(byte i, Class85 class85) {
		if (class85 == OpenGlToolkit.aClass85_4299) {
			int i_5_ = PacketParser.buffer.readByteA(true);
			int i_6_ = Class53.chunkX - -((0x7c & i_5_) >> 1327059812);
			int i_7_ = MapScenesDefinitionParser.chunkY + (i_5_ & 0x7);
			int i_8_ = PacketParser.buffer.readByteA(true);
			int i_9_ = PacketParser.buffer.readShort1((byte) 51);
			GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(i_9_, (byte) 119);
			int i_10_ = PacketParser.buffer.readByteA(true);
			int i_11_ = i_10_ >> 948044066;
			int i_12_ = BuildAreaPreferenceField.anIntArray3685[i_11_];
			if (i_11_ == 11) {
				i_11_ = 10;
			}
			int i_13_ = 0;
			if (class352.aByteArray2994 != null) {
				int i_14_ = -1;
				for (int i_15_ = 0; class352.aByteArray2994.length > i_15_; i_15_++) {
					if (class352.aByteArray2994[i_15_] == i_11_) {
						i_14_ = i_15_;
						break;
					}
				}
				i_13_ = class352.anIntArrayArray2951[i_14_].length;
			}
			int i_16_ = 0;
			if (class352.aShortArray2965 != null) {
				i_16_ = class352.aShortArray2965.length;
			}
			int i_17_ = 0;
			if (class352.aShortArray2974 != null) {
				i_17_ = class352.aShortArray2974.length;
			}
			if ((i_8_ & 0x1) == 1) {
				IdentikitDefinition.method2478(i_7_, i_12_, null, i_6_, -75, MemoryCacheNodeFactory.tileZ);
			} else {
				int[] is = null;
				if ((i_8_ & 0x2 ^ 0xffffffff) == -3) {
					is = new int[i_13_];
					for (int i_18_ = 0; i_18_ < i_13_; i_18_++) {
						is[i_18_] = PacketParser.buffer.readShort((byte) 127);
					}
				}
				short[] is_19_ = null;
				if ((i_8_ & 0x4) == 4) {
					is_19_ = new short[i_16_];
					for (int i_20_ = 0; (i_20_ ^ 0xffffffff) > (i_16_ ^ 0xffffffff); i_20_++) {
						is_19_[i_20_] = (short) PacketParser.buffer.readShort((byte) 127);
					}
				}
				short[] is_21_ = null;
				if ((0x8 & i_8_) == 8) {
					is_21_ = new short[i_17_];
					for (int i_22_ = 0; i_22_ < i_17_; i_22_++) {
						is_21_[i_22_] = (short) PacketParser.buffer.readShort((byte) 127);
					}
				}
				IdentikitDefinition.method2478(i_7_, i_12_, new Class185(QuickChatMessageParser.aLong2112++, is, is_19_, is_21_), i_6_, -92, MemoryCacheNodeFactory.tileZ);
			}
		} else if (class85 == Class39.aClass85_362) {
			int i_23_ = PacketParser.buffer.readUnsignedByte((byte) -124);
			int i_24_ = Class53.chunkX - -(i_23_ >> -1645863036 & 0x7);
			int i_25_ = MapScenesDefinitionParser.chunkY + (i_23_ & 0x7);
			int i_26_ = PacketParser.buffer.readShort((byte) 127);
			if (i_26_ == 65535) {
				i_26_ = -1;
			}
			int i_27_ = PacketParser.buffer.readUnsignedByte((byte) -2);
			int i_28_ = 0xf & i_27_ >> 218942468;
			int i_29_ = 0x7 & i_27_;
			int i_30_ = PacketParser.buffer.readUnsignedByte((byte) -103);
			int i_31_ = PacketParser.buffer.readUnsignedByte((byte) -113);
			int i_32_ = PacketParser.buffer.readShort((byte) 127);
			if ((i_24_ ^ 0xffffffff) <= -1 && i_25_ >= 0 && Class165.mapWidth > i_24_ && Class98_Sub10_Sub7.mapLength > i_25_) {
				int i_33_ = 1 + i_28_;
				if (Class87.localPlayer.pathX[0] >= -i_33_ + i_24_ && i_24_ + i_33_ >= Class87.localPlayer.pathX[0] && (i_25_ - i_33_ ^ 0xffffffff) >= (Class87.localPlayer.pathZ[0] ^ 0xffffffff) && Class87.localPlayer.pathZ[0] <= i_25_ - -i_33_) {
					Class21_Sub4.method278(i_31_, i_32_, i_26_, i_29_, i_30_, (byte) 65, (i_25_ << -1680627448) + (i_24_ << 129807376) + (MemoryCacheNodeFactory.tileZ << -1551112) - -i_28_);
				}
			}
		} else if (Class98_Sub23.aClass85_4007 == class85) {
			int i_34_ = PacketParser.buffer.readByteC((byte) -128);
			int i_35_ = i_34_ >> 1098097986;
			int i_36_ = i_34_ & 0x3;
			int i_37_ = BuildAreaPreferenceField.anIntArray3685[i_35_];
			int i_38_ = PacketParser.buffer.readByteS(i ^ ~0x56);
			int i_39_ = Class53.chunkX + (i_38_ >> 909525444 & 0x7);
			int i_40_ = MapScenesDefinitionParser.chunkY + (i_38_ & 0x7);
			if (Js5Index.method3623(-114, Class151_Sub9.anInt5028) || (i_39_ ^ 0xffffffff) <= -1 && (i_40_ ^ 0xffffffff) <= -1 && Class165.mapWidth > i_39_ && i_40_ < Class98_Sub10_Sub7.mapLength) {
				Class98_Sub33.method591(-85, i_39_, i_35_, i_40_, i_37_, -1, i_36_, MemoryCacheNodeFactory.tileZ);
			}
		} else if (Class35.aClass85_332 == class85) {
			int i_41_ = PacketParser.buffer.readUnsignedByte((byte) 17);
			int i_42_ = Class53.chunkX * 2 + ((i_41_ & 0xfa) >> -1945955580);
			int i_43_ = (0xf & i_41_) + MapScenesDefinitionParser.chunkY * 2;
			int i_44_ = PacketParser.buffer.readUnsignedByte((byte) 36);
			boolean bool = (i_44_ & 0x1) != 0;
			boolean bool_45_ = (0x2 & i_44_) != 0;
			int i_46_ = !bool_45_ ? -1 : i_44_ >> 1165493090;
			int i_47_ = i_42_ - -PacketParser.buffer.readSignedByte((byte) -19);
			int i_48_ = PacketParser.buffer.readSignedByte((byte) -19) + i_43_;
			int i_49_ = PacketParser.buffer.readUShort(false);
			int i_50_ = PacketParser.buffer.readUShort(false);
			int i_51_ = PacketParser.buffer.readShort((byte) 127);
			int i_52_ = PacketParser.buffer.readUnsignedByte((byte) -121);
			if (!bool_45_) {
				i_52_ *= 4;
			} else {
				i_52_ = (byte) i_52_;
			}
			int i_53_ = 4 * PacketParser.buffer.readUnsignedByte((byte) 71);
			int i_54_ = PacketParser.buffer.readShort((byte) 127);
			int i_55_ = PacketParser.buffer.readShort((byte) 127);
			int i_56_ = PacketParser.buffer.readUnsignedByte((byte) -119);
			int i_57_ = PacketParser.buffer.readShort((byte) 127);
			if ((i_56_ ^ 0xffffffff) == -256) {
				i_56_ = -1;
			}
			if ((i_42_ ^ 0xffffffff) <= -1 && (i_43_ ^ 0xffffffff) <= -1 && (i_42_ ^ 0xffffffff) > (Class165.mapWidth * 2 ^ 0xffffffff) && i_43_ < Class165.mapWidth * 2 && i_47_ >= 0 && i_48_ >= 0 && Class98_Sub10_Sub7.mapLength * 2 > i_47_ && Class98_Sub10_Sub7.mapLength * 2 > i_48_ && (i_51_
					^ 0xffffffff) != -65536) {
				i_43_ = 256 * i_43_;
				i_53_ <<= 2;
				i_57_ <<= 2;
				i_52_ <<= 2;
				i_42_ = 256 * i_42_;
				i_48_ = 256 * i_48_;
				i_47_ *= 256;
				if ((i_49_ ^ 0xffffffff) != -1 && i_46_ != -1) {
					Mob class246_sub3_sub4_sub2 = null;
					if ((i_49_ ^ 0xffffffff) <= -1) {
						int i_58_ = -1 + i_49_;
						NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(i_58_, -1);
						if (class98_sub39 != null) {
							class246_sub3_sub4_sub2 = class98_sub39.npc;
						}
					} else {
						int i_59_ = -1 + -i_49_;
						if ((i_59_ ^ 0xffffffff) != (OpenGLHeap.localPlayerIndex ^ 0xffffffff)) {
							class246_sub3_sub4_sub2 = Class151_Sub9.players[i_59_];
						} else {
							class246_sub3_sub4_sub2 = Class87.localPlayer;
						}
					}
					if (class246_sub3_sub4_sub2 != null) {
						RenderAnimDefinition class294 = class246_sub3_sub4_sub2.method3039(1);
						if (class294.anIntArrayArray2366 != null && class294.anIntArrayArray2366[i_46_] != null) {
							i_52_ -= class294.anIntArrayArray2366[i_46_][1];
						}
						if (class294.anIntArrayArray2364 != null && class294.anIntArrayArray2364[i_46_] != null) {
							i_52_ -= class294.anIntArrayArray2364[i_46_][1];
						}
					}
				}
				Class246_Sub3_Sub4_Sub4 class246_sub3_sub4_sub4 = new Class246_Sub3_Sub4_Sub4(i_51_, MemoryCacheNodeFactory.tileZ, MemoryCacheNodeFactory.tileZ, i_42_, i_43_, i_52_, i_54_ + Queue.timer, i_55_ - -Queue.timer, i_56_, i_57_, i_49_, i_50_, i_53_, bool, i_46_);
				class246_sub3_sub4_sub4.method3074(i_47_, -i_53_ + StrongReferenceMCNode.getHeight(MemoryCacheNodeFactory.tileZ, i_48_, i_47_, i ^ ~0x5e03), Queue.timer + i_54_, (byte) 108, i_48_);
				QuickChatMessageParser.projectiles.addLast(new Class98_Sub46_Sub5(class246_sub3_sub4_sub4), i + -20866);
			}
		} else if (Class242.aClass85_1849 == class85) {// ROAR
			// projectile
			int i_60_ = PacketParser.buffer.readUnsignedByte((byte) -120);
			boolean bool = (i_60_ & 0x80 ^ 0xffffffff) != -1;
			int i_61_ = Class53.chunkX - -(0x7 & i_60_ >> -1600745245);
			int i_62_ = (i_60_ & 0x7) + MapScenesDefinitionParser.chunkY;
			int i_63_ = i_61_ + PacketParser.buffer.readSignedByte((byte) -19);
			int i_64_ = i_62_ - -PacketParser.buffer.readSignedByte((byte) -19);
			int i_65_ = PacketParser.buffer.readUShort(false);
			int i_66_ = PacketParser.buffer.readShort((byte) 127);
			int i_67_ = PacketParser.buffer.readUnsignedByte((byte) 0) * 4;
			int i_68_ = 4 * PacketParser.buffer.readUnsignedByte((byte) -8);
			int i_69_ = PacketParser.buffer.readShort((byte) 127);
			int i_70_ = PacketParser.buffer.readShort((byte) 127);
			int i_71_ = PacketParser.buffer.readUnsignedByte((byte) -100);
			int i_72_ = PacketParser.buffer.readShort((byte) 127);
			if (i_71_ == 255) {
				i_71_ = -1;
			}
			if (i_61_ >= 0 && i_62_ >= 0 && Class165.mapWidth > i_61_ && (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) < (i_62_ ^ 0xffffffff) && i_63_ >= 0 && (i_64_ ^ 0xffffffff) <= -1 && i_63_ < Class165.mapWidth && (i_64_ ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) && (i_66_
					^ 0xffffffff) != -65536) {
				i_68_ <<= 2;
				i_61_ = i_61_ * 512 + 256;
				i_67_ <<= 2;
				i_62_ = 256 + i_62_ * 512;
				i_63_ = 256 + i_63_ * 512;
				i_64_ = i_64_ * 512 + 256;
				i_72_ <<= 2;
				Class246_Sub3_Sub4_Sub4 class246_sub3_sub4_sub4 = new Class246_Sub3_Sub4_Sub4(i_66_, MemoryCacheNodeFactory.tileZ, MemoryCacheNodeFactory.tileZ, i_61_, i_62_, i_67_, i_69_ + Queue.timer, i_70_ - -Queue.timer, i_71_, i_72_, 0, i_65_, i_68_, bool, -1);
				class246_sub3_sub4_sub4.method3074(i_63_, StrongReferenceMCNode.getHeight(MemoryCacheNodeFactory.tileZ, i_64_, i_63_, 24111) - i_68_, i_69_ - -Queue.timer, (byte) 108, i_64_);
				QuickChatMessageParser.projectiles.addLast(new Class98_Sub46_Sub5(class246_sub3_sub4_sub4), -20911);
			}
		} else {
			if (class85 == SpriteProgressBarLoadingScreenElement.aClass85_5474) {
				int i_73_ = PacketParser.buffer.readUnsignedByte((byte) -102);
				int tileY = MapScenesDefinitionParser.chunkY + (i_73_ & 0x7);
				int i_75_ = aa_Sub2.gameSceneBaseY + tileY;
				int tileX = (0x7 & i_73_ >> -1172232540) + Class53.chunkX;
				int i_77_ = tileX - -Class272.gameSceneBaseX;
				int i_78_ = PacketParser.buffer.readShort((byte) 127);
				int i_79_ = PacketParser.buffer.readShort((byte) 127);
				int i_80_ = PacketParser.buffer.readShort((byte) 127);
				if (ModelRenderer.groundItems != null) {
					ItemDeque class98_sub45 = (ItemDeque) ModelRenderer.groundItems.get(i_77_ | i_75_ << -45976338 | MemoryCacheNodeFactory.tileZ << -2048549060, i ^ 0x2c);
					if (class98_sub45 != null) {
						for (GroundItem class98_sub26 = (GroundItem) class98_sub45.aClass148_4254.getFirst(32); class98_sub26 != null; class98_sub26 = (GroundItem) class98_sub45.aClass148_4254.getNext(i + 142)) {
							if ((i_78_ & 0x7fff ^ 0xffffffff) == (class98_sub26.itemId ^ 0xffffffff) && class98_sub26.amount == i_79_) {
								class98_sub26.unlink(44);
								class98_sub26.amount = i_80_;
								Class48_Sub1.method458(class98_sub26, MemoryCacheNodeFactory.tileZ, i_75_, i_77_, true);
								break;
							}
						}
						if (tileX >= 0 && tileY >= 0 && (tileX ^ 0xffffffff) > (Class165.mapWidth ^ 0xffffffff) && tileY < Class98_Sub10_Sub7.mapLength) {
							Class246_Sub3_Sub2_Sub1.method1437(tileX, MemoryCacheNodeFactory.tileZ, (byte) 114, tileY);
						}
					}
				}
			} else if (class85 == LightningDetailPreferenceField.aClass85_3667) {
				PacketParser.buffer.readUnsignedByte((byte) -113);
				int i_81_ = PacketParser.buffer.readUnsignedByte((byte) -122);
				int i_82_ = ((0x72 & i_81_) >> 889841188) + Class53.chunkX;
				int i_83_ = (0x7 & i_81_) + MapScenesDefinitionParser.chunkY;
				int i_84_ = PacketParser.buffer.readShort((byte) 127);
				int i_85_ = PacketParser.buffer.readUnsignedByte((byte) -7);
				int i_86_ = PacketParser.buffer.readMediumInt(-123);
				String string = PacketParser.buffer.readString((byte) 84);
				Orientation.method3696((byte) 118, i_86_, i_83_, i_84_, string, i_85_, MemoryCacheNodeFactory.tileZ, i_82_);
			} else if (BuildType.SEND_GROUND_ITEM == class85) {
				int itemId = PacketParser.buffer.readLEShortA((byte) -68);
				int tileLocation = PacketParser.buffer.readByteS(i ^ ~0x50);
				int tileY = (tileLocation & 0x7) + MapScenesDefinitionParser.chunkY;
				int yCoord = aa_Sub2.gameSceneBaseY - -tileY;
				int tileX = Class53.chunkX + (0x7 & tileLocation >> 4);
				int xCoord = Class272.gameSceneBaseX - -tileX;
				int amount = PacketParser.buffer.readLEShortA((byte) 95);
				boolean bool = (tileX ^ 0xffffffff) <= -1 && (tileY ^ 0xffffffff) <= -1 && (tileX ^ 0xffffffff) > (Class165.mapWidth ^ 0xffffffff) && Class98_Sub10_Sub7.mapLength > tileY;
				if (bool || Js5Index.method3623(i + -56, Class151_Sub9.anInt5028)) {
					Class48_Sub1.method458(new GroundItem(itemId, amount), MemoryCacheNodeFactory.tileZ, yCoord, xCoord, true);
					if (bool) {
						Class246_Sub3_Sub2_Sub1.method1437(tileX, MemoryCacheNodeFactory.tileZ, (byte) 59, tileY);
					}
				}
			} else if (class85 == AdvancedMemoryCache.aClass85_600) { // anim
				int i_94_ = PacketParser.buffer.readByteS(-32);
				int i_95_ = Class53.chunkX - -(0x7 & i_94_ >> -1992025084);
				int i_96_ = MapScenesDefinitionParser.chunkY - -(0x7 & i_94_);
				int i_97_ = PacketParser.buffer.readLEShortA((byte) 105);
				if ((i_97_ ^ 0xffffffff) == -65536) {
					i_97_ = -1;
				}
				int i_98_ = PacketParser.buffer.readByteA(true);
				int i_99_ = i_98_ >> -1003983550;
				int i_100_ = 0x3 & i_98_;
				int i_101_ = BuildAreaPreferenceField.anIntArray3685[i_99_];
				Class283.method3351(i_99_, i_100_, true, i_95_, i_96_, MemoryCacheNodeFactory.tileZ, i_97_, i_101_);
			} else if (class85 == Class246_Sub4_Sub2.aClass85_6186) {
				int i_102_ = PacketParser.buffer.readUnsignedByte((byte) -107);
				int i_103_ = Class53.chunkX + ((0x7d & i_102_) >> -1581133244);
				int i_104_ = (0x7 & i_102_) + MapScenesDefinitionParser.chunkY;
				int i_105_ = PacketParser.buffer.readShort((byte) 127);
				if (i_105_ == 65535) {
					i_105_ = -1;
				}
				int i_106_ = PacketParser.buffer.readUnsignedByte((byte) 56);
				int i_107_ = (i_106_ & 0xf6) >> -1409175612;
				int i_108_ = i_106_ & 0x7;
				int i_109_ = PacketParser.buffer.readUnsignedByte((byte) -123);
				int i_110_ = PacketParser.buffer.readUnsignedByte((byte) -107);
				int i_111_ = PacketParser.buffer.readShort((byte) 127);
				if (i_103_ >= 0 && (i_104_ ^ 0xffffffff) <= -1 && (Class165.mapWidth ^ 0xffffffff) < (i_103_ ^ 0xffffffff) && (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) < (i_104_ ^ 0xffffffff)) {
					int i_112_ = i_107_ + 1;
					if ((Class87.localPlayer.pathX[0] ^ 0xffffffff) <= (i_103_ + -i_112_ ^ 0xffffffff) && Class87.localPlayer.pathX[0] <= i_112_ + i_103_ && -i_112_ + i_104_ <= Class87.localPlayer.pathZ[0] && (Class87.localPlayer.pathZ[0] ^ 0xffffffff) >= (i_112_
							+ i_104_ ^ 0xffffffff)) {
						HitmarksDefinition.method844(i_111_, false, i_109_, i_108_, i_110_, (i_103_ << 1399144080) + (MemoryCacheNodeFactory.tileZ << -26386888) + (i_104_ << -1188209688) - -i_107_, i_105_, i + -21);
					}
				}
			} else if (class85 == Class98_Sub10_Sub16.REMOVE_GROUND_ITEM) {
				int tileLocation = PacketParser.buffer.readByteS(-20);
				int tileY = (tileLocation & 0x7) + MapScenesDefinitionParser.chunkY;
				int yCoord = aa_Sub2.gameSceneBaseY + tileY;
				int tileX = Class53.chunkX - -((tileLocation & 0x79) >> 1994183780);
				int xCoord = tileX - -Class272.gameSceneBaseX;
				int itemId = PacketParser.buffer.readShortA(90);
				ItemDeque deque = (ItemDeque) ModelRenderer.groundItems.get(xCoord | yCoord << -1573020242 | MemoryCacheNodeFactory.tileZ << -1438380164, -1);
				if (deque != null) {
					for (GroundItem item = (GroundItem) deque.aClass148_4254.getFirst(32); item != null; item = (GroundItem) deque.aClass148_4254.getNext(117)) {
						if ((item.itemId ^ 0xffffffff) == (itemId & 0x7fff ^ 0xffffffff)) {
							item.unlink(107);
							break;
						}
					}
					if (deque.aClass148_4254.method2420(-127)) {
						deque.unlink(64);
					}
					if (tileX >= 0 && (tileY ^ 0xffffffff) <= -1 && tileX < Class165.mapWidth && (tileY ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff)) {
						Class246_Sub3_Sub2_Sub1.method1437(tileX, MemoryCacheNodeFactory.tileZ, (byte) 94, tileY);
					}
				}
				// System.out.println("i_118_: " + itemId + " i_114_: " + tileY
				// + " i_115_: " + yCoord + " i_116_:" + tileX);
			} else if (class85 == SkyboxDefinition.aClass85_471) { // animate
																	// object
				int i_119_ = PacketParser.buffer.readByteC((byte) 102);
				int i_120_ = Class53.chunkX + (i_119_ >> 2113611716 & 0x7);
				int i_121_ = MapScenesDefinitionParser.chunkY + (0x7 & i_119_);
				int i_122_ = PacketParser.buffer.readShort1((byte) 49);
				// System.out.println(i_122_);
				int i_123_ = PacketParser.buffer.readUnsignedByte((byte) 45);
				int i_124_ = i_123_ >> -705226718;
				int i_125_ = i_123_ & 0x3;
				int i_126_ = BuildAreaPreferenceField.anIntArray3685[i_124_];
				if (Js5Index.method3623(-100, Class151_Sub9.anInt5028) || (i_120_ ^ 0xffffffff) <= -1 && (i_121_ ^ 0xffffffff) <= -1 && (Class165.mapWidth ^ 0xffffffff) < (i_120_ ^ 0xffffffff) && Class98_Sub10_Sub7.mapLength > i_121_) {
					Class98_Sub33.method591(-60, i_120_, i_124_, i_121_, i_126_, i_122_, i_125_, MemoryCacheNodeFactory.tileZ);
				}
			} else if (RectangleLoadingScreenElement.aClass85_3454 == class85) {
				int i_127_ = PacketParser.buffer.readShort((byte) 127);
				int i_128_ = PacketParser.buffer.readUnsignedByte((byte) -107);
				Class130.gameObjectDefinitionList.get(i_127_, (byte) 119).method3853((byte) 49, i_128_);
			} else if (Class351.aClass85_2921 == class85) {
				int i_129_ = PacketParser.buffer.readLEShortA((byte) 109);
				int i_130_ = PacketParser.buffer.readShortA(99);
				int i_131_ = PacketParser.buffer.readShort((byte) 127);
				int i_132_ = PacketParser.buffer.readByteC((byte) 25);
				int tileY = MapScenesDefinitionParser.chunkY + (0x7 & i_132_);
				int i_134_ = tileY + aa_Sub2.gameSceneBaseY;
				int tileX = (0x7 & i_132_ >> -999540380) + Class53.chunkX;
				int i_136_ = Class272.gameSceneBaseX + tileX;
				if ((OpenGLHeap.localPlayerIndex ^ 0xffffffff) != (i_131_ ^ 0xffffffff)) {
					boolean bool = tileX >= 0 && (tileY ^ 0xffffffff) <= -1 && tileX < Class165.mapWidth && (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) < (tileY ^ 0xffffffff);
					if (bool || Js5Index.method3623(-69, Class151_Sub9.anInt5028)) {
						Class48_Sub1.method458(new GroundItem(i_129_, i_130_), MemoryCacheNodeFactory.tileZ, i_134_, i_136_, true);
						if (bool) {
							Class246_Sub3_Sub2_Sub1.method1437(tileX, MemoryCacheNodeFactory.tileZ, (byte) 39, tileY);
						}
					}
				}
			} else if (class85 == OutgoingPacket.aClass85_3868) {
				int i_137_ = PacketParser.buffer.readUnsignedByte((byte) -117);
				int i_138_ = (i_137_ >> -1502134172 & 0x7) + Class53.chunkX;
				int i_139_ = MapScenesDefinitionParser.chunkY - -(0x7 & i_137_);
				int i_140_ = PacketParser.buffer.readShort((byte) 127);
				int i_141_ = PacketParser.buffer.readUnsignedByte((byte) -114);
				int i_142_ = PacketParser.buffer.readShort((byte) 127);
				int i_143_ = PacketParser.buffer.readUnsignedByte((byte) 40);
				if (i_138_ >= 0 && i_139_ >= 0 && (Class165.mapWidth ^ 0xffffffff) < (i_138_ ^ 0xffffffff) && Class98_Sub10_Sub7.mapLength > i_139_) {
					int i_144_ = i_138_ * 512 - -256;
					int i_145_ = i_139_ * 512 + 256;
					int i_146_ = MemoryCacheNodeFactory.tileZ;
					if (i_146_ < 3 && Class1.isBridge(i_139_, (byte) -104, i_138_)) {
						i_146_++;
					}
					Class246_Sub3_Sub4_Sub3 class246_sub3_sub4_sub3 = new Class246_Sub3_Sub4_Sub3(i_140_, i_142_, Queue.timer, MemoryCacheNodeFactory.tileZ, i_146_, i_144_, StrongReferenceMCNode.getHeight(MemoryCacheNodeFactory.tileZ, i_145_, i_144_, 24111) - i_141_, i_145_, i_138_, i_138_, i_139_,
							i_139_, i_143_);
					Class98_Sub10_Sub11.animatedObjects.addLast(new Class98_Sub46_Sub3(class246_sub3_sub4_sub3), i ^ 0x5182);
				}
			} else {
				Class305_Sub1.reportError(null, -123, "T3 - " + class85);
				Class98_Sub10_Sub1.handleLogout(false, false);
			}
		}
	}

	private int	anInt5654;

	private int	anInt5655;

	private int	anInt5656;

	public Class98_Sub10_Sub22() {
		this(0);
	}

	private Class98_Sub10_Sub22(int i) {
		super(0, false);
		try {
			method1071(122, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nn.<init>(" + i + ')');
		}
	}

	private final void method1071(int i, int i_147_) {
		try {
			if (i > 121) {
				anInt5656 = (0xff00 & i_147_) >> 376071844;
				anInt5655 = i_147_ << -339638620 & 0xff0;
				anInt5654 = (i_147_ & 0xff0000) >> -1095348980;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nn.E(" + i + ',' + i_147_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_149_) {
		try {
			if (i_149_ > -92) {
				anInt5656 = 88;
			}
			int i_150_ = i;
			if ((i_150_ ^ 0xffffffff) == -1) {
				method1071(122, class98_sub22.readMediumInt(-124));
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nn.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_149_ + ')');
		}
	}

	@Override
	public final int[][] method997(int i, int i_0_) {
		try {
			if (i >= -76) {
				anInt5656 = -70;
			}
			int[][] is = this.aClass223_3859.method2828(i_0_, 0);
			if (this.aClass223_3859.aBoolean1683) {
				int[] is_1_ = is[0];
				int[] is_2_ = is[1];
				int[] is_3_ = is[2];
				for (int i_4_ = 0; (i_4_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_4_++) {
					is_1_[i_4_] = anInt5654;
					is_2_[i_4_] = anInt5656;
					is_3_[i_4_] = anInt5655;
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nn.C(" + i + ',' + i_0_ + ')');
		}
	}
}

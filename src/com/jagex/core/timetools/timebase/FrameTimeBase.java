
/* Class240 - Decompiled by JODE
 */ package com.jagex.core.timetools.timebase; /*
												*/

import com.Class273;
import com.Class4;
import com.OpenGLXToolkit;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.core.timetools.timebase.impl.CpuFrameTimeBase;
import com.jagex.core.timetools.timebase.impl.JagmiscFrameTimeBase;
import com.jagex.game.client.archive.Js5Exception;

import jaggl.OpenGL;

public abstract class FrameTimeBase {
	public static final FrameTimeBase create(int i) {
		try {
			return new JagmiscFrameTimeBase();
		} catch (Throwable throwable) {
			try {
				return (FrameTimeBase) Class.forName("com.jagex.core.timetools.timebase.impl.SunFrameTimeBase").newInstance();
			} catch (Throwable throwable_14_) {
				return new CpuFrameTimeBase();
			}
		}
	}

	public static final Class273 method2927(byte[] is, OpenGLXToolkit var_ha_Sub3_Sub2, int i, int i_1_) {
		try {
			if (is == null) {
				return null;
			}
			int i_2_ = OpenGL.glGenProgramARB();
			OpenGL.glBindProgramARB(i, i_2_);
			OpenGL.glProgramRawARB(i, 34933, is);
			OpenGL.glGetIntegerv(34379, Class4.anIntArray83, 0);
			if ((Class4.anIntArray83[0] ^ 0xffffffff) != 0) {
				OpenGL.glBindProgramARB(i, 0);
				return null;
			}
			OpenGL.glBindProgramARB(i, 0);
			if (i_1_ != 25246) {
				return null;
			}
			return new Class273(var_ha_Sub3_Sub2, i, i_2_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pca.F(" + (is != null ? "{...}" : "null") + ',' + (var_ha_Sub3_Sub2 != null ? "{...}" : "null") + ',' + i + ',' + i_1_ + ')');
		}
	}

	public abstract int addInterval(int i, long l);

	public abstract long currentTimeNanos(byte i);

	public abstract void reset(boolean bool);

	public abstract long timeToNextFrame(byte i);

	public final int waitForNextFrame(int i, long l) {
		long waitTime = timeToNextFrame((byte) -93);
		if (waitTime > i) {
			TimeTools.sleep(0, waitTime);
		}
		return addInterval(123, l);
	}
}

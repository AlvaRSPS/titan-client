/* Class240_Sub2 - Decompiled by JODE
 */ package com.jagex.core.timetools.timebase.impl; /*
													*/

import com.jagex.core.timetools.timebase.FrameTimeBase;

public final class SunFrameTimeBase extends FrameTimeBase {
	private long	currentTime			= 0L;
	private long	lastTime;
	private int		measurementCount	= 1;
	private int		measurementPtr;
	private long[]	measurements		= new long[10];
	private long	nextFrameTime		= 0L;

	SunFrameTimeBase() {
		measurementPtr = 0;
		lastTime = 0L;
		nextFrameTime = System.nanoTime();
		currentTime = System.nanoTime();
	}

	@Override
	public final int addInterval(int i, long interval) {
		int index;
		if (i <= 117) {
			nextFrameTime = -7L;
		}
		if (nextFrameTime < currentTime) {
			lastTime += currentTime + -nextFrameTime;
			nextFrameTime += currentTime - nextFrameTime;
			currentTime += interval;
			return 1;
		}
		int lagCount = 0;
		do {
			lagCount++;
			currentTime += interval;
		} while (10 > lagCount && (currentTime ^ 0xffffffffffffffffL) > (nextFrameTime ^ 0xffffffffffffffffL));
		if ((nextFrameTime ^ 0xffffffffffffffffL) < (currentTime ^ 0xffffffffffffffffL)) {
			currentTime = nextFrameTime;
		}
		index = lagCount;
		return index;
	}

	@Override
	public final long currentTimeNanos(byte i) {
		long l;
		if (i <= 12) {
			return 126L;
		}
		l = nextFrameTime;
		return l;
	}

	private final long doMeasurement(int i) {
		long l;
		long currentTime = System.nanoTime();
		long deltaTime = -lastTime + currentTime;
		lastTime = currentTime;
		if (4999999999L > (deltaTime ^ 0xffffffffffffffffL) && 5000000000L > deltaTime) {
			measurements[measurementPtr] = deltaTime;
			measurementPtr = (1 + measurementPtr) % 10;
			if ((measurementCount ^ 0xffffffff) > -2) {
				measurementCount++;
			}
		}
		long buffer = 0L;
		for (int mPtr = 1; (measurementCount ^ 0xffffffff) <= (mPtr ^ 0xffffffff); mPtr++) {
			buffer += measurements[(10 + measurementPtr + -mPtr) % 10];
		}
		l = buffer / measurementCount;
		return l;
	}

	@Override
	public final void reset(boolean bool) {
		do {
			lastTime = 0L;
			if (currentTime > nextFrameTime) {
				nextFrameTime += currentTime + -nextFrameTime;
			}
			if (!bool) {
				break;
			}
			currentTimeNanos((byte) -114);
			break;
		} while (false);
	}

	@Override
	public final long timeToNextFrame(byte i) {
		long l;
		nextFrameTime += doMeasurement(-123);
		if (nextFrameTime < currentTime) {
			return (-nextFrameTime + currentTime) / 1000000L;
		}
		if (i >= -80) {
			addInterval(73, -115L);
		}
		l = 0L;
		return l;
	}
}

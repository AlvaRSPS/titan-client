/* Class240_Sub3 - Decompiled by JODE
 */ package com.jagex.core.timetools.timebase.impl; /*
													*/

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.core.timetools.timebase.FrameTimeBase;

public final class CpuFrameTimeBase extends FrameTimeBase {
	private long	currentTime			= 0L;
	private long	lastTime			= 0L;
	private int		measurementCount	= 1;
	private int		measurementPtr;
	private long[]	measurements		= new long[10];
	private long	nextFrameTime		= 0L;

	public CpuFrameTimeBase() {
		measurementPtr = 0;
	}

	@Override
	public final int addInterval(int i, long l) {
		if (currentTime >= nextFrameTime) {
			int lagCount = 0;
			do {
				nextFrameTime += l;
			} while (++lagCount < 10 && (currentTime ^ 0xffffffffffffffffL) < (nextFrameTime ^ 0xffffffffffffffffL));
			if ((currentTime ^ 0xffffffffffffffffL) < (nextFrameTime ^ 0xffffffffffffffffL)) {
				nextFrameTime = currentTime;
			}
			return lagCount;
		}
		lastTime += -currentTime + nextFrameTime;
		currentTime += nextFrameTime - currentTime;
		nextFrameTime += l;
		return 1;
	}

	@Override
	public final long currentTimeNanos(byte i) {
		if (i <= 12) {
			return 77L;
		}
		return currentTime;
	}

	private final long doMeasurement(int i) {
		long currentTime = TimeTools.getCurrentTime(-47) * 1000000L;
		long deltaTime = currentTime + -lastTime;
		lastTime = currentTime;
		if ((deltaTime ^ 0xffffffffffffffffL) < 4999999999L && deltaTime < 5000000000L) {
			measurements[measurementPtr] = deltaTime;
			measurementPtr = (1 + measurementPtr) % 10;
			if ((measurementCount ^ 0xffffffff) > -11) {
				measurementCount++;
			}
		}
		long l_1_ = i;
		for (int i_2_ = 1; (i_2_ ^ 0xffffffff) >= (measurementCount ^ 0xffffffff); i_2_++) {
			l_1_ += measurements[(-i_2_ + measurementPtr + 10) % 10];
		}
		return l_1_ / measurementCount;
	}

	@Override
	public final void reset(boolean bool) {
		do {
			lastTime = 0L;
			if ((currentTime ^ 0xffffffffffffffffL) > (nextFrameTime ^ 0xffffffffffffffffL)) {
				currentTime += -currentTime + nextFrameTime;
			}
			if (bool == false) {
				break;
			}
			currentTimeNanos((byte) 9);
			break;
		} while (false);
	}

	@Override
	public final long timeToNextFrame(byte i) {
		if (i >= -80) {
			lastTime = 38L;
		}
		currentTime += doMeasurement(0);
		if ((currentTime ^ 0xffffffffffffffffL) > (nextFrameTime ^ 0xffffffffffffffffL)) {
			return (-currentTime + nextFrameTime) / 1000000L;
		}
		return 0L;
	}
}

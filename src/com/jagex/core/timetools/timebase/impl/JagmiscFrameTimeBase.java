
/* Class240_Sub1 - Decompiled by JODE
 */ package com.jagex.core.timetools.timebase.impl; /*
													*/

import com.jagex.core.timetools.timebase.FrameTimeBase;

import jagex3.jagmisc.jagmisc;

public final class JagmiscFrameTimeBase extends FrameTimeBase {
	private long	currentTime	= 0L;
	private long	lastTime;
	private int		measurementCount;
	private int		measurementPtr;
	private long[]	measurements;
	private long	nextFrameTime;

	public JagmiscFrameTimeBase() {
		nextFrameTime = 0L;
		lastTime = 0L;
		measurementPtr = 0;
		measurementCount = 1;
		measurements = new long[10];
		nextFrameTime = currentTime = jagmisc.nanoTime();
		if (currentTime == 0L) {
			throw new RuntimeException();
		}
	}

	@Override
	public final int addInterval(int i, long l) {
		if (i <= 117) {
			measurementCount = 14;
		}
		if ((nextFrameTime ^ 0xffffffffffffffffL) >= (currentTime ^ 0xffffffffffffffffL)) {
			int i_3_ = 0;
			do {
				nextFrameTime += l;
			} while (++i_3_ < 10 && nextFrameTime < currentTime);
			if ((nextFrameTime ^ 0xffffffffffffffffL) > (currentTime ^ 0xffffffffffffffffL)) {
				nextFrameTime = currentTime;
			}
			return i_3_;
		}
		lastTime += nextFrameTime + -currentTime;
		currentTime += nextFrameTime + -currentTime;
		nextFrameTime += l;
		return 1;
	}

	@Override
	public final long currentTimeNanos(byte i) {
		return currentTime;
	}

	private final long doMeasurement(int i) {
		long currentTime = jagmisc.nanoTime();
		long deltaTime = currentTime - lastTime;
		lastTime = currentTime;
		if ((deltaTime ^ 0xffffffffffffffffL) < 4999999999L && deltaTime < 5000000000L) {
			measurements[measurementPtr] = deltaTime;
			if (measurementCount < 1) {
				measurementCount++;
			}
			measurementPtr = (1 + measurementPtr) % 10;
		}
		long measurementSum = 0L;
		for (int index = 1; (index ^ 0xffffffff) >= (measurementCount ^ 0xffffffff); index++) {
			measurementSum += measurements[(10 + measurementPtr + -index) % 10];
		}
		return measurementSum / measurementCount;
	}

	@Override
	public final void reset(boolean bool) {
		do {
			lastTime = 0L;
			if ((nextFrameTime ^ 0xffffffffffffffffL) < (currentTime ^ 0xffffffffffffffffL)) {
				currentTime += nextFrameTime - currentTime;
			}
			if (bool == false) {
				break;
			}
			nextFrameTime = 111L;
			break;
		} while (false);
	}

	@Override
	public final long timeToNextFrame(byte i) {
		currentTime += doMeasurement(27103);
		if (nextFrameTime > currentTime) {
			return (nextFrameTime - currentTime) / 1000000L;
		}
		if (i >= -80) {
			measurementCount = -48;
		}
		return 0L;
	}
}

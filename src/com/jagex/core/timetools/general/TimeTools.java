/* Class343 - Decompiled by JODE
 */ package com.jagex.core.timetools.general;

/*
												*/

public class TimeTools {
	public static String	aString2863;
	public static long		lastTimePoll;
	public static long		timeError;

	public static final synchronized long getCurrentTime(int i) {
		long currentTime = System.currentTimeMillis();
		if (lastTimePoll > currentTime) {
			timeError += lastTimePoll - currentTime;
		}
		lastTimePoll = currentTime;
		return timeError + currentTime;
	}

	public static final boolean method3817(byte i, int i_0_) {
		return !((i_0_ ^ 0xffffffff) != -47 && (i_0_ ^ 0xffffffff) != -51 && i_0_ != 48 && (i_0_ ^ 0xffffffff) != -59 && (i_0_ ^ 0xffffffff) != -6 && i_0_ != 57 && i_0_ != 59);
	}

	public static final void sleep(int i, long l) {
		if ((l ^ 0xffffffffffffffffL) < -1L) {
			if ((l % 10L ^ 0xffffffffffffffffL) != -1L) {
				sleepInner(l, 75);
			} else {
				sleepInner(-1L + l, 60);
				sleepInner(1L, i + 116);
			}
		}
	}

	public static final void sleepInner(long l, int i) {
		try {
			Thread.sleep(l);
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
	}

}

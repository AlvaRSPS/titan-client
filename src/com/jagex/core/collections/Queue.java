/* Class215 - Decompiled by JODE
 */ package com.jagex.core.collections; /*
										*/

import com.Class189;
import com.Class246_Sub3_Sub5_Sub2;
import com.Class65;
import com.Class92;
import com.Class98_Sub10_Sub24;
import com.Class98_Sub10_Sub36;
import com.ClientScript2Event;
import com.GameShell;
import com.IncomingOpcode;
import com.client;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class Queue {
	public static float	aFloat1613;
	public static int	anInt1612	= 0;
	public static int	timer		= 0;

	public static final void method2791(byte i) {
		do {
			Class65.aClass293Array500 = null;
			if (OpenGLHeap.aBoolean6079 && (OpenGlModelRenderer.getWindowMode((byte) 106) ^ 0xffffffff) != -2) {
				Class92.method892(21337, 0, 0, ClientScript2Event.method1176(false), client.clientState == 3 || (client.clientState ^ 0xffffffff) == -8, Class98_Sub10_Sub36.method1110((byte) 60));
			}
			int i_5_ = 0;
			int i_7_ = 0;
			if (OpenGLHeap.aBoolean6079) {
				i_5_ = Class189.method2642((byte) 42);
				i_7_ = MapScenesDefinitionParser.method3765(false);
			}
			Class246_Sub3_Sub5_Sub2.renderInterface(i_7_ + GameShell.frameHeight, -1, i_7_, client.topLevelInterfaceId, i_7_, i_5_, GameShell.frameWidth + i_5_, (byte) 88, i_5_);
			if (Class65.aClass293Array500 == null) {
				break;
			}
			Class98_Sub10_Sub24.renderInterface(-1412584499, true, IncomingOpcode.anInt463, i_5_, i_7_, Class189.aClass293_1457.anInt2238, Class65.aClass293Array500, GameShell.frameWidth + i_5_, i_7_ + GameShell.frameHeight, true, Class64_Sub5.anInt3654);
			Class65.aClass293Array500 = null;
			break;
		} while (false);
	}

	private Cacheable	current;

	public Cacheable	head	= new Cacheable();

	public Queue() {
		head.previousCacheable = head;
		head.nextCacheable = head;
	}

	public final void clear(int i) {
		for (;;) {
			Cacheable queueNode = head.nextCacheable;
			if (head == queueNode) {
				break;
			}
			queueNode.uncache((byte) -90);
		}
		current = null;
	}

	public final Cacheable getFirst(int i) {
		Cacheable queueNode = head.nextCacheable;
		if (queueNode == head) {
			current = null;
			return null;
		}
		current = queueNode.nextCacheable;
		return queueNode;
	}

	public final Cacheable getNext(int i) {
		Cacheable queueNode = current;
		if (head == queueNode) {
			current = null;
			return null;
		}
		current = queueNode.nextCacheable;
		return queueNode;
	}

	public final void insert(Cacheable node, int i) {
		if (node.previousCacheable != null) {
			node.uncache((byte) -90);
		}
		node.nextCacheable = head;
		node.previousCacheable = head.previousCacheable;
		node.previousCacheable.nextCacheable = node;
		node.nextCacheable.previousCacheable = node;
	}

	public final Cacheable remove(int i) {
		Cacheable queueNode = head.nextCacheable;
		if (head == queueNode) {
			return null;
		}
		queueNode.uncache((byte) -90);
		return queueNode;
	}

	public final int size(int i) {
		int count = 0;
		for (Cacheable queueNode = head.nextCacheable; head != queueNode; queueNode = queueNode.nextCacheable) {
			count++;
		}
		return count;
	}
}

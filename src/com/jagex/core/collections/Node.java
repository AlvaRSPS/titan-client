/* Class98 - Decompiled by JODE
 */ package com.jagex.core.collections; /*
										*/

import com.Class164;
import com.Class275;
import com.Class30;
import com.Class38;
import com.Class42_Sub3;
import com.Class53_Sub1;
import com.Class98_Sub10_Sub28;
import com.Class98_Sub28_Sub1;
import com.GrandExchangeOffer;
import com.RSByteBuffer;
import com.StringConcatenator;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;

public class Node {
	public static Class38	aClass38_834;
	public static int		anInt835	= -1;
	public static int		tradeStatus	= 0;

	static {
		aClass38_834 = new Class38();
	}

	public static final void method943(byte[] is, boolean bool, boolean bool_0_) {
		do {
			try {
				if (bool_0_ != false) {
					method943(null, false, true);
				}
				if (Class98_Sub10_Sub28.aClass98_Sub22_5705 == null) {
					Class98_Sub10_Sub28.aClass98_Sub22_5705 = new RSByteBuffer(20000);
				}
				Class98_Sub10_Sub28.aClass98_Sub22_5705.method1217(is, is.length, -1, 0);
				if (!bool) {
					break;
				}
				Class30.method304(120, Class98_Sub10_Sub28.aClass98_Sub22_5705.payload);
				Class98_Sub28_Sub1.aClass53_Sub1Array5805 = new Class53_Sub1[Class42_Sub3.anInt5366];
				int i = 0;
				for (int i_1_ = Class164.anInt1274; GrandExchangeOffer.anInt854 >= i_1_; i_1_++) {
					Class53_Sub1 class53_sub1 = Class275.method3283((byte) 127, i_1_);
					if (class53_sub1 != null) {
						Class98_Sub28_Sub1.aClass53_Sub1Array5805[i++] = class53_sub1;
					}
				}
				GraphicsLevelPreferenceField.aBoolean3671 = false;
				StringConcatenator.aLong1998 = TimeTools.getCurrentTime(-47);
				Class98_Sub10_Sub28.aClass98_Sub22_5705 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "gda.U(" + (is != null ? "{...}" : "null") + ',' + bool + ',' + bool_0_ + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method944(int i, int i_2_, byte i_3_) {
		try {
			if (i_3_ != 85) {
				method944(43, 69, (byte) 22);
			}
			return (0x22 & i ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gda.W(" + i + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public long	hash;

	public Node	next;

	public Node	previous;

	public Node() {
		/* empty */
	}

	public final boolean isLinked(byte i) {
		return previous != null;
	}

	public final void unlink(int i) {
		if (previous != null) {
			previous.next = next;
			next.previous = previous;
			previous = null;
			next = null;
		}
	}
}

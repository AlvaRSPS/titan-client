/* Class148 - Decompiled by JODE
 */ package com.jagex.core.collections; /*
										*/

import com.Class105;
import com.Class314;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.toolkit.shadow.OpenGlShadow;

public final class LinkedList {
	public static Class314	aClass314_1195;
	public static double	aDouble1197		= -1.0;
	public static int[]		anIntArray1196	= new int[32];

	public static final void method2429(int i, int i_6_) {
		Class105.anInt3415 = i_6_;
		OpenGlShadow.aClass79_6321.clear(119);
	}

	private Node	current;

	public Node		top	= new Node();

	public LinkedList() {
		top.previous = top;
		top.next = top;
	}

	public final void addLast(Node class98, int i) {
		do {
			try {
				if (class98.previous != null) {
					class98.unlink(71);
				}
				class98.previous = top.previous;
				class98.next = top;
				class98.previous.next = class98;
				class98.next.previous = class98;
				if (i == -20911) {
					break;
				}
				top = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "kba.B(" + (class98 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final void clear(byte i) {
		do {
			try {
				for (;;) {
					Node class98 = top.next;
					if (class98 == top) {
						break;
					}
					class98.unlink(87);
				}
				current = null;
				if (i == 47) {
					break;
				}
				method2429(-56, -39);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "kba.G(" + i + ')');
			}
			break;
		} while (false);
	}

	public final Node getFirst(int i) {
		try {
			if (i != 32) {
				return null;
			}
			Node class98 = top.next;
			if (top == class98) {
				current = null;
				return null;
			}
			current = class98.next;
			return class98;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kba.D(" + i + ')');
		}
	}

	public final Node getNext(int i) {
		try {
			Node class98 = current;
			if (top == class98) {
				current = null;
				return null;
			}
			current = class98.next;
			return class98;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kba.F(" + i + ')');
		}
	}

	public final Node getPrevious(byte i) {
		try {
			Node class98 = current;
			if (class98 == top) {
				current = null;
				return null;
			}
			current = class98.previous;
			return class98;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kba.C(" + i + ')');
		}
	}

	public final boolean method2420(int i) {
		try {
			if (i > -123) {
				anIntArray1196 = null;
			}
			return top == top.next;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kba.L(" + i + ')');
		}
	}

	public final void method2423(int i, Node class98) {
		do {
			try {
				if (class98.previous != null) {
					class98.unlink(100);
				}
				class98.previous = top;
				class98.next = top.next;
				class98.previous.next = class98;
				class98.next.previous = class98;
				if (i == -2) {
					break;
				}
				method2427(100);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "kba.E(" + i + ',' + (class98 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final int method2424(int i) {
		try {
			int i_1_ = i;
			for (Node class98 = top.next; top != class98; class98 = class98.next) {
				i_1_++;
			}
			return i_1_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kba.M(" + i + ')');
		}
	}

	public final void method2425(LinkedList class148_2_, int i) {
		try {
			method2426(class148_2_, top.next, (byte) -123);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kba.N(" + (class148_2_ != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private final void method2426(LinkedList class148_4_, Node class98, byte i) {
		do {
			try {
				if (i > -57) {
					removeFirst(-21);
				}
				Node class98_5_ = top.previous;
				top.previous = class98.previous;
				class98.previous.next = top;
				if (class98 == top) {
					break;
				}
				class98.previous = class148_4_.top.previous;
				class98.previous.next = class98;
				class148_4_.top.previous = class98_5_;
				class98_5_.next = class148_4_.top;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "kba.O(" + (class148_4_ != null ? "{...}" : "null") + ',' + (class98 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final Node method2427(int i) {
		try {
			if (i > -39) {
				return null;
			}
			Node class98 = top.previous;
			if (top == class98) {
				current = null;
				return null;
			}
			current = class98.previous;
			return class98;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kba.H(" + i + ')');
		}
	}

	public final Node removeFirst(int i) {
		try {
			Node class98 = top.next;
			if (top == class98) {
				return null;
			}
			class98.unlink(90);
			if (i != 6494) {
				current = null;
			}
			return class98;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kba.J(" + i + ')');
		}
	}
}

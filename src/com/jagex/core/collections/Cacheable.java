
/* Class98_Sub46 - Decompiled by JODE
 */ package com.jagex.core.collections; /*
										*/

import java.util.Date;

import com.Class112;
import com.Class230;
import com.Class98_Sub28;
import com.Class98_Sub46_Sub20;
import com.OpenGLXToolkit;
import com.RtInterfaceClip;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.definition.CursorDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.parser.StructsDefinitionParser;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;

import jaggl.OpenGL;

public class Cacheable extends Node {
	public static int	anInt4261;
	public static int	anInt4264;

	public static final void insertBefore(Cacheable insertSpot, byte i, Cacheable insertee) {
		if (i == 37) {
			if (insertee.previousCacheable != null) {
				insertee.uncache((byte) -90);
			}
			insertee.nextCacheable = insertSpot.nextCacheable;
			insertee.previousCacheable = insertSpot;
			insertee.previousCacheable.nextCacheable = insertee;
			insertee.nextCacheable.previousCacheable = insertee;
		}
	}

	public static final Class230 method1526(byte[] is, OpenGLXToolkit var_ha_Sub3_Sub2, int i, int i_7_) {
		if (is == null || (is.length ^ 0xffffffff) == -1) {
			return null;
		}
		long l = OpenGL.glCreateShaderObjectARB(i_7_);
		OpenGL.glShaderSourceRawARB(l, is);
		OpenGL.glCompileShaderARB(l);
		OpenGL.glGetObjectParameterivARB(l, 35713, CursorDefinition.anIntArray1734, 0);
		if ((CursorDefinition.anIntArray1734[0] ^ 0xffffffff) == -1) {
			if (CursorDefinition.anIntArray1734[0] == 0) {
				System.out.println("Shader compile failed:");
			}
			OpenGL.glGetObjectParameterivARB(l, 35716, CursorDefinition.anIntArray1734, 1);
			if ((CursorDefinition.anIntArray1734[1] ^ 0xffffffff) < -2) {
				byte[] is_8_ = new byte[CursorDefinition.anIntArray1734[1]];
				OpenGL.glGetInfoLogARB(l, CursorDefinition.anIntArray1734[1], CursorDefinition.anIntArray1734, 0, is_8_, 0);
				System.out.println(new String(is_8_));
			}
			if ((CursorDefinition.anIntArray1734[0] ^ 0xffffffff) == -1) {
				OpenGL.glDeleteObjectARB(l);
				return null;
			}
		}
		return new Class230(var_ha_Sub3_Sub2, l, i_7_);
	}

	public static final void write(String text, int i) {
		if (Class98_Sub46_Sub20.aStringArray6073 == null) {
			StructsDefinitionParser.method3222((byte) -43);
		}
		ParamDefinition.GMT.setTime(new Date(TimeTools.getCurrentTime(-47)));
		int hour = ParamDefinition.GMT.get(11);
		int minutes = ParamDefinition.GMT.get(12);
		int seconds = ParamDefinition.GMT.get(13);
		String time = Integer.toString(hour / 10) + hour % 10 + ":" + minutes / 10 + minutes % 10 + ":" + seconds / 10 + seconds % 10;
		String[] strings = Class112.splitString(text, '\n', false);
		for (int i_5_ = 0; (i_5_ ^ 0xffffffff) > (strings.length ^ 0xffffffff); i_5_++) {
			for (int i_6_ = Class98_Sub28.anInt4080; (i_6_ ^ 0xffffffff) < -1; i_6_--) {
				Class98_Sub46_Sub20.aStringArray6073[i_6_] = Class98_Sub46_Sub20.aStringArray6073[-1 + i_6_];
			}
			Class98_Sub46_Sub20.aStringArray6073[0] = time + ": " + strings[i_5_];
			if (StructsDefinitionParser.aFileOutputStream1969 != null) {
				try {
					StructsDefinitionParser.aFileOutputStream1969.write(RtInterfaceClip.method152(0, Class98_Sub46_Sub20.aStringArray6073[0] + "\n"));
				} catch (java.io.IOException ioexception) {
					/* empty */
				}
			}
			if (Class98_Sub28.anInt4080 < Class98_Sub46_Sub20.aStringArray6073.length + -1) {
				if ((NativeProgressMonitor.anInt3395 ^ 0xffffffff) < -1) {
					NativeProgressMonitor.anInt3395++;
				}
				Class98_Sub28.anInt4080++;
			}
		}
	}

	public long			cachedKey;

	public Cacheable	nextCacheable;

	public Cacheable	previousCacheable;

	public Cacheable() {
		/* empty */
	}

	public final boolean isCached(byte i) {
		return previousCacheable != null;
	}

	public final void uncache(byte i) {
		if (previousCacheable != null) {
			previousCacheable.nextCacheable = nextCacheable;
			nextCacheable.previousCacheable = previousCacheable;
			nextCacheable = null;
			previousCacheable = null;
		}
	}
}

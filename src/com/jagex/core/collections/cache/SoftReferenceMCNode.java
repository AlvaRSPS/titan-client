
/* Class98_Sub46_Sub2_Sub1 - Decompiled by JODE
 */ package com.jagex.core.collections.cache; /*
												*/

import java.lang.ref.SoftReference;

public final class SoftReferenceMCNode extends MemoryCacheNode {
	private SoftReference<Object> reference;

	SoftReferenceMCNode(Object object, int i) {
		super(i);
		reference = new SoftReference<Object>(object);
	}

	@Override
	public final Object getObject(boolean bool) {
		return reference.get();
	}

	@Override
	public final boolean isSoftReference(int i) {
		return true;
	}
}


/* Class79 - Decompiled by JODE
 */ package com.jagex.core.collections.cache; /*
												*/

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.Class166;
import com.Class85;
import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;

public final class AdvancedMemoryCache {
	public static boolean					isMembersOnly		= false;
	public static Class						aClass607;
	public static Class85					aClass85_600		= new Class85(8, 4);
	public static int						anInt601;
	public static int[]						anIntArray603		= new int[1000];
	public static Hashtable<String, Class>	libraryClasses		= new Hashtable<String, Class>();
	public static Hashtable<String, File>	libraryPaths		= new Hashtable<String, File>();

	public static MemoryCacheNodeFactory	softRefNodeFactory	= MemoryCacheNodeFactory.create((byte) 5);

	public static final boolean loadNative(int i, Class<Class166> var_class, String string) {
		Class var_class_1_ = libraryClasses.get(string);
		if (var_class_1_ != null) {
			return var_class_1_.getClassLoader() == var_class.getClassLoader();
		}
		File file = null;
		if (file == null) {
			file = libraryPaths.get(string);
		}
		if (file != null) {
			try {
				file = new File(file.getCanonicalPath());
				Class runtimeClass = Class.forName("java.lang.Runtime");
				Class var_class_3_ = Class.forName("java.lang.reflect.AccessibleObject");
				Method setAccessible = var_class_3_.getDeclaredMethod("setAccessible", Boolean.TYPE);
				Method load = runtimeClass.getDeclaredMethod("load0", Class.forName("java.lang.Class"), Class.forName("java.lang.String"));
				setAccessible.invoke(load, Boolean.TRUE);
				load.invoke(Runtime.getRuntime(), var_class, file.getPath());
				setAccessible.invoke(load, Boolean.FALSE);
				AdvancedMemoryCache.libraryClasses.put(string, var_class);
				return true;
			} catch (NoSuchMethodException nosuchmethodexception) {
				System.load(file.getPath());
				AdvancedMemoryCache.libraryClasses.put(string, Class166.class);
				return true;
			} catch (Throwable throwable) {
				System.err.println(throwable);
				throwable.printStackTrace();
			}
		}
		return false;
	}

	public static final boolean loadNative(String string, byte i) {
		return AdvancedMemoryCache.loadNative(90, Class166.class, string);
	}

	public static final int method801(byte i, int i_8_, int i_9_, int i_10_) {
		if (i != -11) {
			return 96;
		}
		if ((i_10_ ^ 0xffffffff) < -244) {
			i_8_ >>= 4;
		} else if (i_10_ <= 217) {
			if (i_10_ > 192) {
				i_8_ >>= 2;
			} else if ((i_10_ ^ 0xffffffff) < -180) {
				i_8_ >>= 1;
			}
		} else {
			i_8_ >>= 3;
		}
		return (i_10_ >> -1942171039) + (i_8_ >> 1551398789 << 908165991) + ((i_9_ >> 466286402 & 0x3f) << 1714760906);

	}

	/* synthetic */ static Class method809(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public static final boolean unloadNatives(int i) {
		Hashtable hashtable = new Hashtable();
		Enumeration libraries = AdvancedMemoryCache.libraryClasses.keys();
		while (libraries.hasMoreElements()) {
			Object object = libraries.nextElement();
			hashtable.put(object, AdvancedMemoryCache.libraryClasses.get(object));
		}
		try {
			Class accessibleObject = Class.forName("java.lang.reflect.AccessibleObject");
			Class classLoader = Class.forName("java.lang.ClassLoader");
			Field field = classLoader.getDeclaredField("nativeLibraries");
			Method setAccessible = accessibleObject.getDeclaredMethod("setAccessible", Boolean.TYPE);
			setAccessible.invoke(field, Boolean.TRUE);
			try {
				libraries = AdvancedMemoryCache.libraryClasses.keys();
				while (libraries.hasMoreElements()) {
					String libraryName = (String) libraries.nextElement();
					try {
						File libraryPath = AdvancedMemoryCache.libraryPaths.get(libraryName);
						Class libraryClass = AdvancedMemoryCache.libraryClasses.get(libraryName);
						Vector vector = (Vector) field.get(libraryClass.getClassLoader());
						for (int i_34_ = 0; (i_34_ ^ 0xffffffff) > (vector.size() ^ 0xffffffff); i_34_++) {
							try {
								Object rtNativeLibs = vector.elementAt(i_34_);
								Field field_35_ = rtNativeLibs.getClass().getDeclaredField("name");
								setAccessible.invoke(field_35_, Boolean.TRUE);
								try {
									String nativeLibsName = (String) field_35_.get(rtNativeLibs);
									if (nativeLibsName != null && nativeLibsName.equalsIgnoreCase(libraryPath.getCanonicalPath())) {
										Field handle = rtNativeLibs.getClass().getDeclaredField("handle");
										Method finalize = rtNativeLibs.getClass().getDeclaredMethod("finalize");
										setAccessible.invoke(handle, Boolean.TRUE);
										setAccessible.invoke(finalize, Boolean.TRUE);
										try {
											finalize.invoke(rtNativeLibs);
											handle.set(rtNativeLibs, new Integer(0));
											hashtable.remove(libraryName);
										} catch (Throwable throwable) {
											/* empty */
										}
										setAccessible.invoke(finalize, Boolean.FALSE);
										setAccessible.invoke(handle, Boolean.FALSE);
									}
								} catch (Throwable throwable) {
									/* empty */
								}
								setAccessible.invoke(field_35_, Boolean.FALSE);
							} catch (Throwable throwable) {
								/* empty */
							}
						}
					} catch (Throwable throwable) {
						/* empty */
					}
				}
			} catch (Throwable throwable) {
				/* empty */
			}
			setAccessible.invoke(field, Boolean.FALSE);
		} catch (Throwable throwable) {
			/* empty */
		}
		libraryClasses = hashtable;
		return libraryClasses.isEmpty();
	}

	private int			free;

	private HashTable	hashTable;

	private int			maxSize;
	private Queue		removeOrder	= new Queue();

	public AdvancedMemoryCache(int i) {
		this(i, i);
	}

	public AdvancedMemoryCache(int size, int maxHashRes) {
		maxSize = size;
		free = size;
		int hashRes;
		for (hashRes = 1; (size ^ 0xffffffff) < (hashRes + hashRes ^ 0xffffffff) && (hashRes ^ 0xffffffff) > (maxHashRes ^ 0xffffffff); hashRes += hashRes) {
			/* empty */
		}
		hashTable = new HashTable(hashRes);
	}

	public final void clear(int i) {
		removeOrder.clear(16711680);
		hashTable.clear(-99);
		free = maxSize;
	}

	public final void freeSoftReferences(byte i) {
		for (MemoryCacheNode item = (MemoryCacheNode) removeOrder.getFirst(-1); item != null; item = (MemoryCacheNode) removeOrder.getNext(0)) {
			if (item.isSoftReference(122)) {
				item.unlink(55);
				item.uncache((byte) -90);
				free += item.size;
			}
		}
	}

	public final Object get(int i, long hash) {
		MemoryCacheNode oldItem = (MemoryCacheNode) hashTable.get(hash, -1);
		if (oldItem == null) {
			return null;
		}
		Object object = oldItem.getObject(true);
		if (object == null) {
			oldItem.unlink(100);
			oldItem.uncache((byte) -90);
			free += oldItem.size;
			return null;
		}
		if (oldItem.isSoftReference(119)) {
			StrongReferenceMCNode item = new StrongReferenceMCNode(object, oldItem.size);
			hashTable.put(item, oldItem.hash, -1);
			removeOrder.insert(item, -55);
			item.cachedKey = 0L;
			oldItem.unlink(123);
			oldItem.uncache((byte) -90);
		}
		removeOrder.insert(oldItem, -50);
		oldItem.cachedKey = 0L;
		return object;
	}

	public final int getFreeSpace(int i) {
		return free;

	}

	public final int getMaxSize(int i) {
		return maxSize;
	}

	public final int getStrongReferenceCount(int i) {
		int strongRefCount = 0;
		for (MemoryCacheNode memoryCacheNode = (MemoryCacheNode) removeOrder.getFirst(-1); memoryCacheNode != null; memoryCacheNode = (MemoryCacheNode) removeOrder.getNext(0)) {
			if (!memoryCacheNode.isSoftReference(127)) {
				strongRefCount++;
			}
		}
		return strongRefCount;

	}

	public final Object iterateNext(boolean bool) {
		MemoryCacheNode memoryCacheNode = (MemoryCacheNode) hashTable.iterateNext(-1);
		while (memoryCacheNode != null) {
			Object object = memoryCacheNode.getObject(true);
			if (object != null) {
				return object;
			}
			MemoryCacheNode item = memoryCacheNode;
			memoryCacheNode = (MemoryCacheNode) hashTable.iterateNext(-1);
			item.unlink(74);
			item.uncache((byte) -90);
			free += item.size;
		}
		return null;
	}

	public final void makeSoftReferences(byte i, int count) {
		if (softRefNodeFactory != null) {
			for (MemoryCacheNode insertSlot = (MemoryCacheNode) removeOrder.getFirst(-1); insertSlot != null; insertSlot = (MemoryCacheNode) removeOrder.getNext(0)) {
				if (!insertSlot.isSoftReference(124)) {
					if (count < ++insertSlot.cachedKey) {
						MemoryCacheNode softRefNode = softRefNodeFactory.createNode(0, insertSlot);
						hashTable.put(softRefNode, insertSlot.hash, -1);
						Cacheable.insertBefore(insertSlot, (byte) 37, softRefNode);
						insertSlot.unlink(i + 43);
						insertSlot.uncache((byte) -90);
					}
				} else if (insertSlot.getObject(true) == null) {
					insertSlot.unlink(98);
					insertSlot.uncache((byte) -90);
					free += insertSlot.size;
				}
			}
		}
	}

	public final void put(int i, long hash, int size, Object object) {
		if ((size ^ 0xffffffff) < (maxSize ^ 0xffffffff)) {
			throw new IllegalStateException("size > max size");
		}
		this.remove(hash, i + 67095427);
		free -= size;
		while (free < 0) {
			MemoryCacheNode item = (MemoryCacheNode) removeOrder.remove(i ^ ~0xff3483);
			this.remove(item, i ^ 0x347b);
		}
		StrongReferenceMCNode envelope = new StrongReferenceMCNode(object, size);
		hashTable.put(envelope, hash, -1);
		removeOrder.insert(envelope, -28);
		envelope.cachedKey = 0L;
	}

	public final void put(long hash, Object object, byte i) {
		this.put(13436, hash, 1, object);
	}

	private final void remove(long hash, int i) {
		MemoryCacheNode item = (MemoryCacheNode) hashTable.get(hash, -1);
		this.remove(item, 7);
	}

	private final void remove(MemoryCacheNode item, int i) {
		if (item != null) {
			item.unlink(56);
			item.uncache((byte) -90);
			free += item.size;
		}
	}

	public final Object startIteration(boolean bool) {
		MemoryCacheNode memoryCacheNode = (MemoryCacheNode) hashTable.startIteration(116);
		while (memoryCacheNode != null) {
			Object object = memoryCacheNode.getObject(true);
			if (object == null) {
				MemoryCacheNode item = memoryCacheNode;
				memoryCacheNode = (MemoryCacheNode) hashTable.iterateNext(-1);
				item.unlink(73);
				item.uncache((byte) -90);
				free += item.size;
			} else {
				return object;
			}
		}
		return null;
	}
}

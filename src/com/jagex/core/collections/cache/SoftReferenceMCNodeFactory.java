/* Class206_Sub1 - Decompiled by JODE
 */ package com.jagex.core.collections.cache; /*
												*/

public final class SoftReferenceMCNodeFactory extends MemoryCacheNodeFactory {
	@Override
	public final MemoryCacheNode createNode(int size, MemoryCacheNode memoryCacheNode) {
		return new SoftReferenceMCNode(memoryCacheNode.getObject(true), memoryCacheNode.size);
	}
}

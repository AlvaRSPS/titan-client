/* Class98_Sub46_Sub2 - Decompiled by JODE
 */ package com.jagex.core.collections.cache; /*
												*/

import com.Class105;
import com.Class169;
import com.WorldMap;
import com.jagex.core.collections.Cacheable;

public abstract class MemoryCacheNode extends Cacheable {
	public static Class105	aClass105_5951	= new Class105("", 12);
	public static int		anInt5952;

	public static final void setWorldMapZoom(int mapZoom, boolean dummy) {
		if (mapZoom != 37) {
			if (mapZoom != 50) {
				if (mapZoom == 75) {
					WorldMap.zoom = 6.0F;
				} else if (mapZoom != 100) {
					if ((mapZoom ^ 0xffffffff) == -201) {
						WorldMap.zoom = 16.0F;
					}
				} else {
					WorldMap.zoom = 8.0F;
				}
			} else {
				WorldMap.zoom = 4.0F;
			}
		} else {
			WorldMap.zoom = 3.0F;
		}
		Class169.anInt1307 = -1;
		Class169.anInt1307 = -1;
	}

	public int size;

	public MemoryCacheNode(int size) {
		this.size = size;
	}

	public abstract Object getObject(boolean bool);

	public abstract boolean isSoftReference(int i);
}

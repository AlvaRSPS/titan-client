/* Class206 - Decompiled by JODE
 */ package com.jagex.core.collections.cache; /*
												*/

import com.Class105;
import com.Class232;
import com.Class246_Sub2;
import com.Class252;
import com.Class98_Sub31_Sub2;
import com.Class98_Sub46_Sub20;
import com.FriendLoginUpdate;
import com.ParticleManager;

public abstract class MemoryCacheNodeFactory {
	public static int	anInt1567;
	public static int	tileZ;

	static {
		new Class105("", 73);
	}

	public static final MemoryCacheNodeFactory create(byte i) {
		try {
			return (MemoryCacheNodeFactory) Class.forName("com.jagex.core.collections.cache.StrongReferenceMCNode").newInstance();
		} catch (Throwable throwable) {
			return null;
		}
	}

	public static final void method2724(byte i) {
		if (Class98_Sub31_Sub2.anInt5822 <= 0) {
			ParticleManager.inputBuffer = "";
		} else {
			int i_3_ = 0;
			for (String element : Class98_Sub46_Sub20.aStringArray6073) {
				if (element.indexOf("--> ") != -1 && (Class98_Sub31_Sub2.anInt5822 ^ 0xffffffff) == (++i_3_ ^ 0xffffffff)) {
					ParticleManager.inputBuffer = element.substring(element.indexOf(">") + 2);
					break;
				}
			}
		}
		if (i <= 122) {
			method2725(-3, 116, -39);
		}
	}

	public static final boolean method2725(int i, int i_5_, int i_6_) {
		if (i != 32768) {
			anInt1567 = -40;
		}
		return (0x8000 & i_5_ ^ 0xffffffff) != -1;
	}

	public static final void method2727(int i) {
		if (i < 20) {
			anInt1567 = -40;
		}
		if (!Class232.aBoolean1744) {
			FriendLoginUpdate.method3105((byte) -89, Class246_Sub2.aClass172ArrayArrayArray5077);
			if (Class252.aClass172ArrayArrayArray1927 != null) {
				FriendLoginUpdate.method3105((byte) -121, Class252.aClass172ArrayArrayArray1927);
			}
			Class232.aBoolean1744 = true;
		}
	}

	public abstract MemoryCacheNode createNode(int i, MemoryCacheNode class98_sub46_sub2);
}

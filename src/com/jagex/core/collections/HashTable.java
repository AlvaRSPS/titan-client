/* Class377 - Decompiled by JODE
 */ package com.jagex.core.collections; /*
										*/

public final class HashTable {
	public static byte[][]	aByteArrayArray3182;
	public static int		anInt3183;
	public static int[]		regionUwObjectMapId;

	public Node[]			buckets;
	private long			currentHash;
	private Node			currentIterator;
	private Node			currentNode;

	public int				hashResolution;

	private int				iteratorPosition	= 0;

	public HashTable(int hashReso) {
		hashResolution = hashReso;
		buckets = new Node[hashReso];
		for (int count = 0; (count ^ 0xffffffff) > (hashReso ^ 0xffffffff); count++) {
			Node bucket = buckets[count] = new Node();
			bucket.previous = bucket;
			bucket.next = bucket;
		}
	}

	public final void clear(int i) {
		for (int hashStart = 0; (hashResolution ^ 0xffffffff) < (hashStart ^ 0xffffffff); hashStart++) {
			Node node = buckets[hashStart];
			for (;;) {
				Node current = node.next;
				if (current == node) {
					break;
				}
				current.unlink(79);
			}
		}
		currentNode = null;
		currentIterator = null;
	}

	public final Node get(long hash, int i) {
		currentHash = hash;
		Node bucket = buckets[(int) (hash & hashResolution + i)];
		for (currentNode = bucket.next; currentNode != bucket; currentNode = currentNode.next) {
			if ((hash ^ 0xffffffffffffffffL) == (currentNode.hash ^ 0xffffffffffffffffL)) {
				Node entry = currentNode;
				currentNode = currentNode.next;
				return entry;
			}
		}
		currentNode = null;
		return null;
	}

	public final int getHashResolution(byte i) {
		return hashResolution;
	}

	public final int getValues(Node[] nodes, byte i) {
		int outPtr = 0;
		for (int hashCtr = 0; hashResolution > hashCtr; hashCtr++) {
			Node rootNode = buckets[hashCtr];
			for (Node node = rootNode.next; rootNode != node; node = node.next) {
				nodes[outPtr++] = node;
			}
		}
		return outPtr;
	}

	public final Node iterateNext(int i) {
		if ((iteratorPosition ^ 0xffffffff) < i && buckets[iteratorPosition - 1] != currentIterator) {
			Node node = currentIterator;
			currentIterator = node.next;
			return node;
		}
		while ((iteratorPosition ^ 0xffffffff) > (hashResolution ^ 0xffffffff)) {
			Node node = buckets[iteratorPosition++].next;
			if (node != buckets[iteratorPosition + -1]) {
				currentIterator = node.next;
				return node;
			}
		}
		return null;
	}

	public final Node nextHashCollision(int i) {
		if (currentNode == null) {
			return null;
		}
		for (Node bucket = buckets[(int) (-1 + hashResolution & currentHash)]; currentNode != bucket; currentNode = currentNode.next) {
			if ((currentHash ^ 0xffffffffffffffffL) == (currentNode.hash ^ 0xffffffffffffffffL)) {
				Node node = currentNode;
				currentNode = currentNode.next;
				return node;
			}
		}
		currentNode = null;
		return null;
	}

	public final void put(Node node, long hash, int i) {
		if (node.previous != null) {
			node.unlink(58);
		}
		Node bucket = buckets[(int) (hash & i + hashResolution)];
		node.next = bucket;
		node.previous = bucket.previous;
		node.previous.next = node;
		node.hash = hash;
		node.next.previous = node;
	}

	public final int size(byte i) {
		int count = 0;
		for (int i_12_ = 0; (hashResolution ^ 0xffffffff) < (i_12_ ^ 0xffffffff); i_12_++) {
			Node bucket = buckets[i_12_];
			for (Node entry = bucket.next; bucket != entry; entry = entry.next) {
				count++;
			}
		}
		return count;
	}

	public final Node startIteration(int i) {
		iteratorPosition = 0;
		return iterateNext(-1);
	}
}

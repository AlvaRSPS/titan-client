package com.jagex.core.crypto;

public class CRC32 {

	public static int[] crcTable = new int[256];

	static {
		for (int n = 0; n < 256; n++) {
			int v = n;
			for (int i_15_ = 0; i_15_ < 8; i_15_++) {
				if ((v & 0x1) != 1) {
					v >>>= 1;
				} else {
					v = v >>> 1 ^ 0xEDB88320;
				}
			}
			crcTable[n] = v;
		}
	}

	public static final int calculate(int length, byte[] data, int i_5_) {
		return calculate(length, data, 0, false);
	}

	public static final int calculate(int end, byte[] data, int start, boolean bool) {
		int crc32 = -1;
		for (int i_2_ = start; i_2_ < end; i_2_++) {
			crc32 = crcTable[0xff & (data[i_2_] ^ crc32)] ^ crc32 >>> -26165528;
		}
		crc32 ^= 0xffffffff;
		return crc32;
	}
}

/* Class117 - Decompiled by JODE
 */ package com.jagex.core.crypto; /*
									*/

import com.Char;
import com.Class1;
import com.Class130;
import com.Class15;
import com.Class151_Sub8;
import com.Class172;
import com.Class174;
import com.Class18;
import com.Class202;
import com.Class21_Sub4;
import com.Class226;
import com.Class245;
import com.Class246_Sub2;
import com.Class246_Sub3_Sub4;
import com.Class246_Sub3_Sub5_Sub2;
import com.Class246_Sub4_Sub2;
import com.Class248;
import com.Class249;
import com.Class252;
import com.Class259;
import com.Class319;
import com.Class34;
import com.Class347;
import com.Class353;
import com.Class359;
import com.Class375;
import com.Class40;
import com.Class74;
import com.Class81;
import com.Class98_Sub10_Sub27;
import com.Class98_Sub10_Sub30;
import com.Class98_Sub10_Sub31;
import com.Class98_Sub43_Sub3;
import com.Class98_Sub46_Sub5;
import com.GlobalPlayer;
import com.OpenGLRenderEffectManager;
import com.OpenGLTexture2DSource;
import com.RSToolkit;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.LightIntensityDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;
import com.jagex.game.client.preferences.VolumePreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.input.impl.AwtMouseListener;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.matrix.NativeMatrix;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class ISAACPseudoRNG {
	public static int[] anIntArray974 = new int[50];

	public static final void method2164(RSToolkit var_ha, int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, boolean bool, boolean bool_5_) {
		Class98_Sub10_Sub30.activeToolkit = var_ha;
		RenderAnimDefinition.anInt2407 = i;
		Class375.aBoolean3170 = RenderAnimDefinition.anInt2407 > 1 && Class98_Sub10_Sub30.activeToolkit.method1810();
		Class151_Sub8.tileScale = i_0_;
		NativeShadow.anInt6333 = 1 << Class151_Sub8.tileScale;
		Js5.anInt1577 = NativeShadow.anInt6333 >> 1;
		Math.sqrt(Js5.anInt1577 * Js5.anInt1577 + Js5.anInt1577 * Js5.anInt1577);
		OpenGLTexture2DSource.anInt3103 = i_1_;
		BConfigDefinition.anInt3112 = i_2_;
		Class64_Sub9.anInt3662 = i_3_;
		Class259.anInt1959 = i_4_;
		GlobalPlayer.aClass142_3174 = NativeMatrix.method2133((byte) -20);
		VolumePreferenceField.method645((byte) 102);
		Class246_Sub2.aClass172ArrayArrayArray5077 = new Class172[i_1_][BConfigDefinition.anInt3112][Class64_Sub9.anInt3662];
		StrongReferenceMCNode.aSArray6298 = new Ground[i_1_];
		if (bool) {
			Class40.anIntArrayArray367 = new int[BConfigDefinition.anInt3112][Class64_Sub9.anInt3662];
			AwtMouseListener.aByteArrayArray5291 = new byte[BConfigDefinition.anInt3112][Class64_Sub9.anInt3662];
			GraphicsDefinitionParser.aShortArrayArray2534 = new short[BConfigDefinition.anInt3112][Class64_Sub9.anInt3662];
			Class252.aClass172ArrayArrayArray1927 = new Class172[1][BConfigDefinition.anInt3112][Class64_Sub9.anInt3662];
			Class81.aSArray618 = new Ground[1];
		} else {
			Class40.anIntArrayArray367 = null;
			AwtMouseListener.aByteArrayArray5291 = null;
			GraphicsDefinitionParser.aShortArrayArray2534 = null;
			Class252.aClass172ArrayArrayArray1927 = null;
			Class81.aSArray618 = null;
		}
		if (bool_5_) {
			SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476 = new long[i_1_][i_2_][i_3_];
			Class98_Sub10_Sub31.aClass1Array5717 = new Class1[65535];
			Class21_Sub4.aBooleanArray5399 = new boolean[65535];
			Class226.anInt1705 = 0;
		} else {
			SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476 = null;
			Class98_Sub10_Sub31.aClass1Array5717 = null;
			Class21_Sub4.aBooleanArray5399 = null;
			Class226.anInt1705 = 0;
		}
		Class248.method3158(false);
		LightIntensityDefinition.aClass246_Sub3Array3198 = new Char[2];
		Class359.aClass246_Sub3Array3056 = new Char[2];
		Class130.aClass246_Sub3Array1029 = new Char[2];
		FloorOverlayDefinitionParser.aClass246_Sub3Array307 = new Char[10000];
		GameObjectDefinitionParser.opaqueOnscreenCount = 0;
		Class246_Sub4_Sub2.aClass246_Sub3Array6173 = new Char[5000];
		Class353.transOnscreenCount = 0;
		Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273 = new Class246_Sub3_Sub4[5000];
		Class347.dynamicCount = 0;
		Class74.aBooleanArrayArray551 = new boolean[Class259.anInt1959 + Class259.anInt1959 + 1][Class259.anInt1959 + Class259.anInt1959 + 1];
		Class319.aBooleanArrayArray2702 = new boolean[Class259.anInt1959 + Class259.anInt1959 + 2][Class259.anInt1959 + Class259.anInt1959 + 2];
		Class347.anIntArray2906 = new int[Class259.anInt1959 + Class259.anInt1959 + 2];
		Class98_Sub10_Sub27.aClass84_5692 = Class98_Sub10_Sub27.aClass84_5693;
		if (Class375.aBoolean3170) {
			Class34.aBooleanArrayArrayArray325 = new boolean[i_1_][Class259.anInt1959 + Class259.anInt1959 + 1][Class259.anInt1959 + Class259.anInt1959 + 1];
			GraphicsLevelPreferenceField.aBooleanArrayArrayArray3673 = new boolean[i_1_][][];
			if (Class98_Sub46_Sub5.aClass174Array5970 != null) {
				Class249.method3162();
			}
			Class98_Sub46_Sub5.aClass174Array5970 = new Class174[RenderAnimDefinition.anInt2407];
			Class98_Sub10_Sub30.activeToolkit.createContexts(Class98_Sub46_Sub5.aClass174Array5970.length + 1);
			Class98_Sub10_Sub30.activeToolkit.attachContext(0);
			for (int i_6_ = 0; i_6_ < Class98_Sub46_Sub5.aClass174Array5970.length; i_6_++) {
				Class98_Sub46_Sub5.aClass174Array5970[i_6_] = new Class174(i_6_ + 1, Class98_Sub10_Sub30.activeToolkit);
				new Thread(Class98_Sub46_Sub5.aClass174Array5970[i_6_], "wr" + i_6_).start();
			}
			int i_7_;
			if (RenderAnimDefinition.anInt2407 == 2) {
				i_7_ = 4;
				Class18.anInt212 = 2;
			} else if (RenderAnimDefinition.anInt2407 == 3) {
				i_7_ = 6;
				Class18.anInt212 = 3;
			} else {
				i_7_ = 8;
				Class18.anInt212 = 4;
			}
			Class98_Sub43_Sub3.aClass245Array5922 = new Class245[i_7_];
			for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
				Class98_Sub43_Sub3.aClass245Array5922[i_8_] = new Class245(OpenGLRenderEffectManager.aStringArrayArray441[RenderAnimDefinition.anInt2407 - 2][i_8_]);
			}
		} else {
			Class18.anInt212 = 1;
		}
		Ground.anIntArray2205 = new int[Class18.anInt212 - 1];
		Class15.anIntArray182 = new int[Class18.anInt212 - 1];
	}

	public static final void method2169(int[] is, long[] ls, int i, int i_23_, boolean bool) {
		do {
			if (i < i_23_) {
				int i_24_ = (i_23_ + i) / 2;
				int i_25_ = i;
				long l = ls[i_24_];
				ls[i_24_] = ls[i_23_];
				ls[i_23_] = l;
				int i_26_ = is[i_24_];
				is[i_24_] = is[i_23_];
				is[i_23_] = i_26_;
				int i_27_ = l == 9223372036854775807L ? 0 : 1;
				for (int i_28_ = i; (i_23_ ^ 0xffffffff) < (i_28_ ^ 0xffffffff); i_28_++) {
					if (l - -(long) (i_27_ & i_28_) > ls[i_28_]) {
						long l_29_ = ls[i_28_];
						ls[i_28_] = ls[i_25_];
						ls[i_25_] = l_29_;
						int i_30_ = is[i_28_];
						is[i_28_] = is[i_25_];
						is[i_25_++] = i_30_;
					}
				}
				ls[i_23_] = ls[i_25_];
				ls[i_25_] = l;
				is[i_23_] = is[i_25_];
				is[i_25_] = i_26_;
				method2169(is, ls, i, i_25_ + -1, bool);
				method2169(is, ls, 1 + i_25_, i_23_, bool);
			}
			if (bool == false) {
				break;
			}
			method2164(null, 101, -71, 107, 35, -44, 75, true, true);
			break;
		} while (false);
	}

	private int		anInt968;
	private int		anInt971;
	private int		anInt972;

	private int[]	cryptSet;

	private int		keyArrayIndex;

	private int[]	keySet;

	public ISAACPseudoRNG(int[] keys) {
		keySet = new int[256];
		cryptSet = new int[256];
		for (int i = 0; (keys.length ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
			cryptSet[i] = keys[i];
		}
		isaac((byte) -123);
	}

	private final void isaac(byte i) {
		int i_10_;
		int i_11_;
		int i_12_;
		int i_13_;
		int i_14_;
		int i_15_;
		int i_16_;
		int i_9_ = i_10_ = i_11_ = i_12_ = i_13_ = i_14_ = i_15_ = i_16_ = -1640531527;
		for (int i_17_ = 0; (i_17_ ^ 0xffffffff) > -5; i_17_++) {
			i_9_ ^= i_10_ << -910493557;
			i_10_ += i_11_;
			i_12_ += i_9_;
			i_10_ ^= i_11_ >>> -424891550;
			i_13_ += i_10_;
			i_11_ += i_12_;
			i_11_ ^= i_12_ << -1200133080;
			i_12_ += i_13_;
			i_14_ += i_11_;
			i_12_ ^= i_13_ >>> -151076080;
			i_13_ += i_14_;
			i_15_ += i_12_;
			i_13_ ^= i_14_ << -958066198;
			i_16_ += i_13_;
			i_14_ += i_15_;
			i_14_ ^= i_15_ >>> 1081109060;
			i_9_ += i_14_;
			i_15_ += i_16_;
			i_15_ ^= i_16_ << -2066782968;
			i_16_ += i_9_;
			i_10_ += i_15_;
			i_16_ ^= i_9_ >>> -1653233111;
			i_9_ += i_10_;
			i_11_ += i_16_;
		}
		int i_18_ = 0;
		for (/**/; i_18_ < 256; i_18_ += 8) {
			i_10_ += cryptSet[i_18_ - -1];
			i_9_ += cryptSet[i_18_];
			i_16_ += cryptSet[i_18_ + 7];
			i_14_ += cryptSet[5 + i_18_];
			i_11_ += cryptSet[2 + i_18_];
			i_13_ += cryptSet[4 + i_18_];
			i_12_ += cryptSet[3 + i_18_];
			i_15_ += cryptSet[i_18_ - -6];
			i_9_ ^= i_10_ << 1494265387;
			i_10_ += i_11_;
			i_12_ += i_9_;
			i_10_ ^= i_11_ >>> 386271234;
			i_13_ += i_10_;
			i_11_ += i_12_;
			i_11_ ^= i_12_ << -459009784;
			i_12_ += i_13_;
			i_14_ += i_11_;
			i_12_ ^= i_13_ >>> 1744658032;
			i_15_ += i_12_;
			i_13_ += i_14_;
			i_13_ ^= i_14_ << -754939414;
			i_16_ += i_13_;
			i_14_ += i_15_;
			i_14_ ^= i_15_ >>> -1730138780;
			i_9_ += i_14_;
			i_15_ += i_16_;
			i_15_ ^= i_16_ << -949934008;
			i_10_ += i_15_;
			i_16_ += i_9_;
			i_16_ ^= i_9_ >>> -1543727799;
			i_11_ += i_16_;
			i_9_ += i_10_;
			keySet[i_18_] = i_9_;
			keySet[1 + i_18_] = i_10_;
			keySet[i_18_ + 2] = i_11_;
			keySet[i_18_ - -3] = i_12_;
			keySet[4 + i_18_] = i_13_;
			keySet[5 + i_18_] = i_14_;
			keySet[i_18_ - -6] = i_15_;
			keySet[i_18_ - -7] = i_16_;
		}
		for (i_18_ = 0; (i_18_ ^ 0xffffffff) > -257; i_18_ += 8) {
			i_16_ += keySet[i_18_ + 7];
			i_12_ += keySet[i_18_ - -3];
			i_13_ += keySet[4 + i_18_];
			i_10_ += keySet[1 + i_18_];
			i_15_ += keySet[6 + i_18_];
			i_9_ += keySet[i_18_];
			i_14_ += keySet[i_18_ + 5];
			i_11_ += keySet[i_18_ - -2];
			i_9_ ^= i_10_ << -542763221;
			i_10_ += i_11_;
			i_12_ += i_9_;
			i_10_ ^= i_11_ >>> -902646974;
			i_13_ += i_10_;
			i_11_ += i_12_;
			i_11_ ^= i_12_ << 334759432;
			i_14_ += i_11_;
			i_12_ += i_13_;
			i_12_ ^= i_13_ >>> 967114160;
			i_15_ += i_12_;
			i_13_ += i_14_;
			i_13_ ^= i_14_ << 755037322;
			i_16_ += i_13_;
			i_14_ += i_15_;
			i_14_ ^= i_15_ >>> -1484981788;
			i_15_ += i_16_;
			i_9_ += i_14_;
			i_15_ ^= i_16_ << 463243080;
			i_10_ += i_15_;
			i_16_ += i_9_;
			i_16_ ^= i_9_ >>> 1562519465;
			i_11_ += i_16_;
			i_9_ += i_10_;
			keySet[i_18_] = i_9_;
			keySet[i_18_ - -1] = i_10_;
			keySet[i_18_ + 2] = i_11_;
			keySet[i_18_ + 3] = i_12_;
			keySet[i_18_ + 4] = i_13_;
			keySet[5 + i_18_] = i_14_;
			keySet[6 + i_18_] = i_15_;
			keySet[i_18_ - -7] = i_16_;
		}
		method2166((byte) 94);
		keyArrayIndex = 256;
	}

	private final void method2166(byte i) {
		if (i == 94) {
			anInt968 += ++anInt972;
			for (int i_20_ = 0; i_20_ < 256; i_20_++) {
				int i_21_ = keySet[i_20_];
				if ((0x2 & i_20_ ^ 0xffffffff) != -1) {
					if ((0x1 & i_20_ ^ 0xffffffff) != -1) {
						anInt971 ^= anInt971 >>> -2137572944;
					} else {
						anInt971 ^= anInt971 << -255770270;
					}
				} else if ((i_20_ & 0x1) == 0) {
					anInt971 ^= anInt971 << -1951024339;
				} else {
					anInt971 ^= anInt971 >>> 1690224390;
				}
				anInt971 += keySet[0xff & 128 + i_20_];
				int i_22_;
				keySet[i_20_] = i_22_ = anInt971 + keySet[Class202.and(1020, i_21_) >> -875557438] - -anInt968;
				cryptSet[i_20_] = anInt968 = keySet[Class202.and(i_22_ >> -1052867128, 1020) >> -1635419422] - -i_21_;
			}
		}
	}

	public final int next(int i) {
		if (i <= 76) {
			anInt968 = -71;
		}
		if ((keyArrayIndex ^ 0xffffffff) == -1) {
			method2166((byte) 94);
			keyArrayIndex = 256;
		}
		return 0;
		// return anIntArray973[--anInt970];
	}

	public final int peek(int i) {
		if (i != 3) {
			return 20;
		}
		if (keyArrayIndex == 0) {
			method2166((byte) 94);
			keyArrayIndex = 256;
		}
		return 0;
		// return anIntArray973[-1 + anInt970];
	}
}

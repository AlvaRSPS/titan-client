package com.jagex.core.crypto;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.Formatter;
import java.util.Random;

public class DumpUtils {

	public static Random			random			= new Random();
	public static final char		blank			= ' ';

	private static StringBuilder	formatBuilder	= new StringBuilder();

	private static Formatter		formatter		= new Formatter(formatBuilder);

	public static void dumpStackTrace() {
		StackTraceElement[] elements = Thread.currentThread().getStackTrace();
		for (int i = 0; i < elements.length; i++) {
			if (elements.length >= i && elements[i] != null) {
				if (i > 2) {
					System.out.println(elements[i].getClassName() + "." + elements[i].getMethodName());
				} else if (i == 2) {
					System.out.println("[This method] " + elements[i].getClassName() + "." + elements[i].getMethodName());
				}
			}
		}
	}

	public static int getRandomInt() {
		if (random == null) {
			random = new Random();
		}
		return random.nextInt();
	}

	public static int inverse(int input) {
		BigInteger a = BigInteger.valueOf(input);
		BigInteger modulus = BigInteger.ONE.shiftLeft(32);
		return a.modInverse(modulus).intValue();
	}

	public static void printConsole() {
	}

	public static void println(String format, Object... args) {
		formatBuilder.setLength(0);
		formatter.format(format, args);
		System.out.println(formatBuilder.toString());
	}

	public static void reflectionDump(Class<?> clazz, Object inst) {
		try {
			Field[] fields = clazz.getDeclaredFields();

			System.out.println(clazz.getName() + " {\r\n\tFields: " + fields.length);
			Field[] arrayOfField1;
			int j = (arrayOfField1 = fields).length;
			for (int i = 0; i < j; i++) {
				Field field = arrayOfField1[i];

				field.setAccessible(true);

				System.out.print("\t" + field.getName() + ": ");
				if (field.getType().isArray()) {
					if (field.getName().startsWith("aShortArray")) {
						short[] val = (short[]) field.get(inst);
						if (val != null) {
							System.out.println("Short Array, Length: " + val.length);
						} else {
							System.out.println("Short Array, ~Null.");
						}
					} else if (field.getName().startsWith("anIntArray")) {
						int[] val = (int[]) field.get(inst);
						if (val != null) {
							System.out.println("Int Array, Length: " + val.length);
						} else {
							System.out.println("Int Array, ~Null.");
						}
					} else if (field.getName().startsWith("aByteArray")) {
						byte[] val = (byte[]) field.get(inst);
						if (val != null) {
							System.out.println("Byte Array, Length: " + val.length);
						} else {
							System.out.println("Byte Array, ~Null.");
						}
					} else {
						System.out.println("N/A");
					}
				} else if (field.getType() == String.class) {
					if (field.get(inst) == null) {
						System.out.println(" /null");
					} else {
						System.out.println("\"" + field.get(inst).toString() + "\"");
					}
				} else if (field.getType() == Integer.TYPE || field.getType() == Short.TYPE || field.getType() == Float.TYPE || field.getType() == Double.TYPE) {
					if (field.getType() == Integer.TYPE) {
						System.out.println(field.getInt(inst));
					} else if (field.getType() == Short.TYPE) {
						System.out.println(field.getShort(inst));
					} else if (field.getType() == Float.TYPE) {
						System.out.println(field.getFloat(inst));
					} else if (field.getType() == Double.TYPE) {
						System.out.println(field.getDouble(inst));
					}
				}
			}
			System.out.println("}");
		} catch (Exception e) {
			System.err.println("Failed to dump " + clazz.getName());
			e.printStackTrace();
		}
	}

}


/* Class196 - Decompiled by JODE
 */ package com.jagex.game.constants; /*
										*/

import java.io.IOException;
import java.net.Socket;

import com.Class164;
import com.Class53_Sub1;
import com.Class62;
import com.Class74;
import com.Class98_Sub30;
import com.NodeShort;
import com.IncomingOpcode;
import com.JavaNetworkHandler;
import com.OutgoingOpcode;
import com.StreamHandler;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatMessageParser;

public final class BuildLocation {
	public static OutgoingOpcode			aClass171_1508	= new OutgoingOpcode(81, 7);
	public static IncomingOpcode			aClass58_1507	= new IncomingOpcode(72, 0);
	public static int						anInt1511		= 0;
	public static GraphicsDefinitionParser	gfxDefinitionList;
	public static BuildLocation				LIVE			= new BuildLocation("LIVE", "", "", 0);
	public static BuildLocation				LOCAL			= new BuildLocation("LOCAL", "", "local", 4);
	public static Js5						midiInstrumentsJs5;
	public static BuildLocation				WTI				= new BuildLocation("WTI", "office", "_wti", 5);
	public static BuildLocation				WTQA			= new BuildLocation("WTQA", "office", "_qa", 2);
	public static BuildLocation				WTRC			= new BuildLocation("WTRC", "office", "_rc", 1);
	public static BuildLocation				WTWIP			= new BuildLocation("WTWIP", "office", "_wip", 3);

	public static final BuildLocation get(int unusedInt, int id) {
		BuildLocation[] buildLocationList = BuildLocation.getBuildLocationList(94);
		for (int index = 0; (buildLocationList.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
			BuildLocation buildLocation = buildLocationList[index];
			if ((buildLocation.index ^ 0xffffffff) == (id ^ 0xffffffff)) {
				return buildLocation;
			}
		}
		return null;
	}

	public static final BuildLocation[] getBuildLocationList(int i) {
		return new BuildLocation[] { LIVE, WTRC, WTQA, WTWIP, LOCAL, WTI };
	}

	public static final boolean isInHouseTest(boolean bool, BuildLocation location) {
		return !(location != WTRC && location != WTQA && WTWIP != location && location != WTI);
	}

	public static final int method2665(boolean bool, Class164 class164) {
		if (NodeShort.aClass164_4190 == class164) {
			return 6407;
		}
		if (Class62.aClass164_486 != class164) {
			if (Class53_Sub1.aClass164_3633 != class164) {
				if (Class98_Sub30.aClass164_4088 != class164) {
					if (Class74.aClass164_547 != class164) {
						if (class164 == QuickChatMessageParser.aClass164_2101) {
							return 6145;
						}
					} else {
						return 6410;
					}
				} else {
					return 6409;
				}
			} else {
				return 6406;
			}
		} else {
			return 6408;
		}
		throw new IllegalStateException();
	}

	public static final StreamHandler method2668(Socket socket, byte i, int i_5_) throws IOException {
		return new JavaNetworkHandler(socket, i_5_);
	}

	int index;

	public BuildLocation(String string, String string_6_, String string_7_, int i) {
		index = i;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

/* Class6 - Decompiled by JODE
 */ package com.jagex.game.constants; /*
										*/

import com.CacheFileRequest;
import com.Class154;
import com.Class272;
import com.Class85;
import com.Exception_Sub1;
import com.GameShell;
import com.jagex.core.collections.Cacheable;

public final class BuildType {
	public static int		anInt88;
	public static int[]		anIntArray90		= new int[] { 4, 2, 1, 1, 2, 2, 3, 1, 3, 3, 3, 2, 0 };
	public static int[]		anIntArray91		= new int[1000];
	public static BuildType	LIVE				= new BuildType("LIVE", 0);
	public static BuildType	RC					= new BuildType("RC", 1);
	public static Class85	SEND_GROUND_ITEM	= new Class85(0, 5);
	public static BuildType	WIP					= new BuildType("WIP", 2);

	public static final BuildType get(int id, int i_20_) {
		BuildType[] buildTypeList = BuildType.getBuildTypeList((byte) -63);
		for (BuildType buildType : buildTypeList) {
			if ((buildType.id ^ 0xffffffff) == (id ^ 0xffffffff)) {
				return buildType;
			}
		}
		return null;
	}

	public static final BuildType[] getBuildTypeList(byte i) {
		return new BuildType[] { LIVE, RC, WIP };
	}

	public static final void method181(byte i) throws Exception_Sub1 {
		do {
			if ((Cacheable.anInt4261 ^ 0xffffffff) != -2) {
				Class154.aHa1231.method1764(0, 0);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			Class154.aHa1231.method1764(CacheFileRequest.anInt6309, Class272.anInt2037);
		} while (false);
	}

	int id;

	BuildType(String string, int i) {
		id = i;
	}

	public final int getId(int i) {
		return id;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

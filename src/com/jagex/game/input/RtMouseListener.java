package com.jagex.game.input;

import java.awt.Component;
import java.lang.reflect.Constructor;

import com.GameShell;
import com.IncomingOpcode;
import com.client;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.input.impl.NewAwtMouseListener;

/* Class299 - Decompiled by JODE
 */

public abstract class RtMouseListener {
	public static IncomingOpcode		aClass58_2495	= new IncomingOpcode(47, -1);
	public static AdvancedMemoryCache	aClass79_2493	= new AdvancedMemoryCache(32);
	public static int					anInt2494;
	/* synthetic */ static Class		componentClass;

	public static final RtMouseListener create(Component component, boolean bool, int i) {
		try {
			Constructor<?> constructor = Class.forName("com.jagex.game.input.impl.NewAwtMouseListener").getDeclaredConstructor(componentClass != null ? componentClass : (componentClass = getClassByName("java.awt.Component")), Boolean.TYPE);
			return (RtMouseListener) constructor.newInstance(component, new Boolean(bool));
		} catch (Throwable throwable) {
			return new NewAwtMouseListener(component, bool);
		}
	}

	/* synthetic */
	public static Class getClassByName(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public static final void updateMargins(boolean bool) {
		int maxScreenSize = 0;
		if (client.preferences != null) {
			maxScreenSize = client.preferences.maxScreenSize.getValue((byte) 127);
		}
		if ((maxScreenSize ^ 0xffffffff) != -3) {
			if ((maxScreenSize ^ 0xffffffff) == -2) {
				int width = (GameShell.width ^ 0xffffffff) < -1025 ? 1024 : GameShell.width;
				GameShell.frameWidth = width;
				int height = GameShell.height <= 768 ? GameShell.height : 768;
				GameShell.leftMargin = (GameShell.width + -width) / 2;
				GameShell.topMargin = 0;
				GameShell.frameHeight = height;
			} else {
				GameShell.frameWidth = GameShell.width;
				GameShell.frameHeight = GameShell.height;
				GameShell.topMargin = 0;
				GameShell.leftMargin = 0;
			}
		} else {
			int width = GameShell.width <= 800 ? GameShell.width : 800;
			GameShell.leftMargin = (-width + GameShell.width) / 2;
			int height = GameShell.height > 600 ? 600 : GameShell.height;
			GameShell.frameWidth = width;
			GameShell.topMargin = 0;
			GameShell.frameHeight = height;
		}
	}

	public RtMouseListener() {
		/* empty */
	}

	public abstract int getMouseX(int i);

	public abstract int getMouseY(byte i);

	public abstract RtMouseEvent getNextEvent(int i);

	public final boolean isAnyButtonDown(byte i) {
		return !(!method3506((byte) 121) && !method3510((byte) -19) && !method3512(1));
	}

	public abstract boolean method3506(byte i);

	public abstract boolean method3510(byte i);

	public abstract boolean method3512(int i);

	public abstract void method3515(int i);

	public abstract void method3516(byte i);
}

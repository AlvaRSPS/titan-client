/* Class98_Sub17 - Decompiled by JODE
 */ package com.jagex.game.input; /*
									*/

import com.jagex.core.collections.Node;

public abstract class RtMouseEvent extends Node {
	public static boolean	aBoolean3944	= false;
	public static int		anInt3943		= 0;
	public static boolean	occlusion		= true;

	public RtMouseEvent() {
		/* empty */
	}

	public abstract int getEventType(int i);

	public abstract int getMouseX(int i);

	public abstract int getMouseY(int i);

	public abstract int method1152(int i);

	public abstract long method1154(boolean bool);
}

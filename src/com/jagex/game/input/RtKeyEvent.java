/* Interface7 - Decompiled by JODE
 */ package com.jagex.game.input; /*
									*/

public interface RtKeyEvent {
	int getEventType(int i);

	char getKeyChar(int i);

	int getKeyCode(boolean bool);

	int getModifiers(byte i);

	long getTimeStamp(int i);
}

package com.jagex.game.input;

import java.awt.Component;

import com.Class137;
import com.Class185;
import com.Class2;
import com.Class98_Sub46_Sub17;
import com.IncomingOpcode;
import com.RtInterface;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.preferences.BuildAreaPreferenceField;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.ProgressBarLoadingScreenElement;
import com.jagex.game.input.impl.AwtKeyListener;

/* Class77 - Decompiled by JODE
 */

public abstract class RtKeyListener {
	public static RtInterface		aClass293_593;
	public static IncomingOpcode	aClass58_591	= new IncomingOpcode(99, 0);
	public static IncomingOpcode	aClass58_592	= new IncomingOpcode(44, 4);

	public static final RtKeyListener create(byte i, Component component) {
		return new AwtKeyListener(component);
	}

	public static final void initialize(int i, Js5 glyphsStore, int[] fontIds, Js5 fontmetricsStore) {
		if (fontIds != null) {
			Class2.fontIds = fontIds;
		}
		BuildAreaPreferenceField.glyphsStore = glyphsStore;
		MaxScreenSizePreferenceField.fontmetricsStore = fontmetricsStore;
	}

	public static final void method778(int i, int i_1_) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_1_, i ^ ~0xd, 14);
		class98_sub46_sub17.method1621(i ^ i);
	}

	public static final int method780(int i, byte i_3_, int i_4_) {
		if ((i ^ 0xffffffff) == -2 || (i ^ 0xffffffff) == -4) {
			return ProgressBarLoadingScreenElement.anIntArray3479[i_4_ & 0x3];
		}
		if (i_3_ != -72) {
			return -111;
		}
		return Class137.anIntArray1081[i_4_ & 0x3];
	}

	public RtKeyListener() {
		/* empty */
	}

	public abstract RtKeyEvent getNextEvent(byte i);

	public abstract boolean isKeyDown(int i, int i_2_);

	public abstract void method773(byte i);

	public abstract void method774(byte i);
}

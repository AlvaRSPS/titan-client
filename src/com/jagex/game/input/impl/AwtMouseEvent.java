/* Class98_Sub17_Sub1 - Decompiled by JODE
 */ package com.jagex.game.input.impl; /*
										*/

import com.Class233;
import com.Class246_Sub3_Sub5_Sub2;
import com.Class27;
import com.Class314;
import com.Class359;
import com.Class81;
import com.Class98_Sub31_Sub3;
import com.GameShell;
import com.OutgoingOpcode;
import com.SystemInformation;
import com.client;
import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.matrix.OpenGlMatrix;

public final class AwtMouseEvent extends RtMouseEvent {
	public static boolean			consoleOpen						= false;
	public static OutgoingOpcode	SEND_PRIVATE_QUICK_CHAT_MESSAGE	= new OutgoingOpcode(9, -1);
	public static OutgoingOpcode	CHANGE_CHAT_STATUS				= new OutgoingOpcode(44, 3);
	public static Js5				interfaceJs5;

	public static final void method1157(int i) {
		try {
			if (i >= -115) {
				method1160(14);
			}
			GroupProgressMonitor.method2799(2, 22050, (client.preferences.monoOrStereo.getValue((byte) 123) ^ 0xffffffff) == -2, (byte) 117);
			SystemInformation.aClass268_1173 = FloorUnderlayDefinitionParser.method2484(22050, 0, (byte) -126, GameShell.signLink, GameShell.canvas);
			Class246_Sub3_Sub5_Sub2.method3098(OpenGlMatrix.method2115(-124, null), true, 28643);
			Class27.aClass268_276 = FloorUnderlayDefinitionParser.method2484(2048, 1, (byte) -126, GameShell.signLink, GameShell.canvas);
			Class81.aClass98_Sub31_Sub3_619 = new Class98_Sub31_Sub3();
			Class27.aClass268_276.method3252(0, Class81.aClass98_Sub31_Sub3_619);
			LinkedList.aClass314_1195 = new Class314(22050, RemoveRoofsPreferenceField.anInt3678);
			Class233.method2883((byte) 111);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gfa.C(" + i + ')');
		}
	}

	public static void method1159(int i) {
		do {
			try {
				SEND_PRIVATE_QUICK_CHAT_MESSAGE = null;
				CHANGE_CHAT_STATUS = null;
				interfaceJs5 = null;
				if (i == -15367) {
					break;
				}
				client.clientWidth = -111;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "gfa.H(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method1160(int i) {
		try {
			if (i <= 38) {
				return true;
			}
			return (Class359.actionCount ^ 0xffffffff) < -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gfa.D(" + i + ')');
		}
	}

	public long	aLong5777;

	public int	anInt5774;

	public int	anInt5775;

	public int	anInt5776;

	public int	anInt5779;

	@Override
	public final int getEventType(int i) {
		try {
			if (i != -5) {
				aLong5777 = 50L;
			}
			return anInt5779;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gfa.E(" + i + ')');
		}
	}

	@Override
	public final int getMouseX(int i) {
		try {
			return anInt5775;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gfa.I(" + i + ')');
		}
	}

	@Override
	public final int getMouseY(int i) {
		try {
			if (i < 34) {
				return -100;
			}
			return anInt5776;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gfa.G(" + i + ')');
		}
	}

	@Override
	public final int method1152(int i) {
		try {
			if (i != 2) {
				aLong5777 = -28L;
			}
			return anInt5774;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gfa.F(" + i + ')');
		}
	}

	@Override
	public final long method1154(boolean bool) {
		try {
			if (bool != true) {
				return -39L;
			}
			return aLong5777;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gfa.A(" + bool + ')');
		}
	}
}

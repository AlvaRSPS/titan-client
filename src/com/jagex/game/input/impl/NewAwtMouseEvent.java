/* Class98_Sub17_Sub2 - Decompiled by JODE
 */ package com.jagex.game.input.impl; /*
										*/

import com.jagex.game.input.RtMouseEvent;

public final class NewAwtMouseEvent extends RtMouseEvent {
	public long	aLong5788;
	public int	anInt5784;
	public int	anInt5785;
	public int	anInt5786;
	public int	anInt5787;

	@Override
	public final int getEventType(int i) {
		int i_0_;
		try {
			if (i != -5) {
				anInt5784 = -40;
			}
			i_0_ = anInt5786;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return i_0_;
	}

	@Override
	public final int getMouseX(int i) {
		int i_1_;
		try {
			i_1_ = anInt5787;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return i_1_;
	}

	@Override
	public final int getMouseY(int i) {
		int i_3_;
		try {
			if (i <= 34) {
				return 1;
			}
			i_3_ = anInt5785;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return i_3_;
	}

	@Override
	public final int method1152(int i) {
		int i_4_;
		try {
			if (i != 2) {
				return 57;
			}
			i_4_ = anInt5784;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return i_4_;
	}

	@Override
	public final long method1154(boolean bool) {
		long l;
		try {
			if (bool != true) {
				return 71L;
			}
			l = aLong5788;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return l;
	}
}


/* Class299_Sub1 - Decompiled by JODE
 */ package com.jagex.game.input.impl; /*
										*/

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import com.Class116;
import com.client;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.input.RtMouseListener;

public final class NewAwtMouseListener extends RtMouseListener implements MouseListener, MouseMotionListener, MouseWheelListener {
	public static MouseWheelEvent	MOUSE_WHEEL_EVENT;
	private boolean					aBoolean5286;
	private LinkedList				aClass148_5277	= new LinkedList();
	private LinkedList				aClass148_5284	= new LinkedList();
	private Component				aComponent5285;
	private int						anInt5278;
	private int						anInt5279;
	private int						anInt5280;
	private int						anInt5281;
	private int						anInt5282;
	private int						anInt5283;

	public NewAwtMouseListener(Component component, boolean bool) {
		try {
			method3518(component, (byte) 88);
			aBoolean5286 = bool;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	public final int getMouseX(int i) {
		int i_9_;
		try {
			if (i <= 4) {
				anInt5278 = 117;
			}
			i_9_ = anInt5279;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return i_9_;
	}

	@Override
	public final int getMouseY(byte i) {
		int i_2_;
		try {
			if (i < 24) {
				method3518(null, (byte) -4);
			}
			i_2_ = anInt5278;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return i_2_;
	}

	@Override
	public final RtMouseEvent getNextEvent(int i) {
		RtMouseEvent class98_sub17;
		try {
			if (i != 600) {
				anInt5281 = -94;
			}
			class98_sub17 = (RtMouseEvent) aClass148_5277.removeFirst(i ^ 0x1b06);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return class98_sub17;
	}

	@Override
	public final boolean method3506(byte i) {
		boolean bool;
		try {
			if ((anInt5280 & 0x1) != 0) {
				return true;
			}
			bool = false;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return bool;
	}

	@Override
	public final boolean method3510(byte i) {
		boolean bool;
		try {
			if (i != -19) {
				aBoolean5286 = true;
			}
			if ((0x2 & anInt5280 ^ 0xffffffff) != -1) {
				return true;
			}
			bool = false;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return bool;
	}

	@Override
	public final boolean method3512(int i) {
		boolean bool;
		try {
			if (i != 1) {
				mouseReleased(null);
			}
			if (0 != (anInt5280 & 0x4)) {
				return true;
			}
			bool = false;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return bool;
	}

	@Override
	public final void method3515(int i) {
		do {
			try {
				method3520(-87);
				if (i < -110) {
					break;
				}
				aBoolean5286 = true;
			} catch (RuntimeException runtimeException) {
				throw runtimeException;
			}
			break;
		} while (false);
	}

	@Override
	public final synchronized void method3516(byte i) {
		try {
			anInt5280 = anInt5283;
			anInt5278 = anInt5282;
			anInt5279 = anInt5281;
			LinkedList class148 = aClass148_5277;
			aClass148_5277 = aClass148_5284;
			aClass148_5284 = class148;
			if (i < 103) {
				anInt5281 = -46;
			}
			aClass148_5284.clear((byte) 47);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	private final void method3517(int i, byte i_3_, int i_4_, int i_5_, int i_6_) {
		do {
			try {
				NewAwtMouseEvent class98_sub17_sub2 = new NewAwtMouseEvent();
				class98_sub17_sub2.anInt5785 = i_6_;
				class98_sub17_sub2.anInt5786 = i_5_;
				class98_sub17_sub2.anInt5787 = i_4_;
				class98_sub17_sub2.aLong5788 = TimeTools.getCurrentTime(-47);
				class98_sub17_sub2.anInt5784 = i;
				aClass148_5284.addLast(class98_sub17_sub2, -20911);
				if (i_3_ <= -106) {
					break;
				}
				anInt5278 = -78;
			} catch (RuntimeException runtimeException) {
				throw runtimeException;
			}
			break;
		} while (false);
	}

	private final void method3518(Component component, byte i) {
		try {
			method3520(i + -153);
			aComponent5285 = component;
			if (i != 88) {
				aClass148_5284 = null;
			}
			aComponent5285.addMouseListener(this);
			aComponent5285.addMouseMotionListener(this);
			aComponent5285.addMouseWheelListener(this);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	private final int method3519(MouseEvent mouseevent, int i) {
		int i_7_;
		try {
			if (i != 2) {
				return 56;
			}
			if (mouseevent.getButton() == 1) {
				if (mouseevent.isMetaDown()) {
					return 4;
				}
				return 1;
			}
			if (mouseevent.getButton() == 2) {
				return 2;
			}
			if (mouseevent.getButton() == 3) {
				return 4;
			}
			i_7_ = 0;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return i_7_;
	}

	private final void method3520(int i) {
		try {
			if (null != aComponent5285) {
				aComponent5285.removeMouseWheelListener(this);
				aComponent5285.removeMouseMotionListener(this);
				aComponent5285.removeMouseListener(this);
				aClass148_5284 = null;
				anInt5281 = anInt5282 = anInt5283 = 0;
				anInt5279 = anInt5278 = anInt5280 = 0;
				aComponent5285 = null;
				aClass148_5277 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	private final void method3521(int i, int i_10_, int i_11_) {
		do {
			try {
				anInt5282 = i_11_;
				anInt5281 = i_10_;
				if (i != -11571) {
					anInt5279 = 31;
				}
				if (!aBoolean5286) {
					break;
				}
				method3517(0, (byte) -125, i_10_, -1, i_11_);
			} catch (RuntimeException runtimeException) {
				throw runtimeException;
			}
			break;
		} while (false);
	}

	@Override
	public final synchronized void mouseClicked(MouseEvent mouseevent) {
		try {
			if (mouseevent.isPopupTrigger()) {
				mouseevent.consume();
			}
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	public final synchronized void mouseDragged(MouseEvent mouseevent) {
		try {
			method3521(-11571, mouseevent.getX(), mouseevent.getY());
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	public final synchronized void mouseEntered(MouseEvent mouseevent) {
		try {
			method3521(-11571, mouseevent.getX(), mouseevent.getY());
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	public final synchronized void mouseExited(MouseEvent mouseevent) {
		try {
			method3521(-11571, mouseevent.getX(), mouseevent.getY());
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	public final synchronized void mouseMoved(MouseEvent mouseevent) {
		try {
			method3521(-11571, mouseevent.getX(), mouseevent.getY());
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	public final synchronized void mousePressed(MouseEvent mouseevent) {
		do {
			try {
				int i = method3519(mouseevent, 2);
				if (i == 1) {
					method3517(mouseevent.getClickCount(), (byte) -127, mouseevent.getX(), 0, mouseevent.getY());
				} else if (4 == i) {
					method3517(mouseevent.getClickCount(), (byte) -107, mouseevent.getX(), 2, mouseevent.getY());
				} else if (i == 2) {
					method3517(mouseevent.getClickCount(), (byte) -113, mouseevent.getX(), 1, mouseevent.getY());
				}
				anInt5283 |= i;
				if (!mouseevent.isPopupTrigger()) {
					break;
				}
				mouseevent.consume();
			} catch (RuntimeException runtimeException) {
				throw runtimeException;
			}
			break;
		} while (false);
	}

	@Override
	public final synchronized void mouseReleased(MouseEvent mouseevent) {
		try {
			int i = method3519(mouseevent, 2);
			if ((i & anInt5283 ^ 0xffffffff) == -1) {
				i = anInt5283;
			}
			if (0 != (i & 0x1)) {
				method3517(mouseevent.getClickCount(), (byte) -111, mouseevent.getX(), 3, mouseevent.getY());
			}
			if ((i & 0x4 ^ 0xffffffff) != -1) {
				method3517(mouseevent.getClickCount(), (byte) -114, mouseevent.getX(), 5, mouseevent.getY());
			}
			if (-1 != (0x2 & i ^ 0xffffffff)) {
				method3517(mouseevent.getClickCount(), (byte) -127, mouseevent.getX(), 4, mouseevent.getY());
			}
			anInt5283 &= i ^ 0xffffffff;
			if (mouseevent.isPopupTrigger()) {
				mouseevent.consume();
			}
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	public final synchronized void mouseWheelMoved(MouseWheelEvent mousewheelevent) {
		try {
			int i = mousewheelevent.getX();
			int i_0_ = mousewheelevent.getY();
			int i_1_ = mousewheelevent.getWheelRotation();
			method3517(i_1_, (byte) -127, i, 6, i_0_);
			mousewheelevent.consume();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		if (mousewheelevent.isShiftDown()) {
			int dir = mousewheelevent.getWheelRotation();
			if (dir == 1) {
				if (client.zoom >= 101)
					client.zoom -= 16;
			} else if (dir == -1 && client.zoom < 530) {
				client.zoom += 16;
			}
			System.out.println("Client Zoom: " + client.zoom);
			Class116.setVarpBit(client.zoom, 184, (byte) -120);
		}
	}

}

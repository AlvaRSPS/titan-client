/* Class98_Sub8 - Decompiled by JODE
 */ package com.jagex.game.input.impl; /*
										*/

import com.OutgoingOpcode;
import com.jagex.core.collections.Node;
import com.jagex.game.input.RtKeyEvent;

public final class AwtKeyEvent extends Node implements RtKeyEvent {
	public static OutgoingOpcode	aClass171_3264	= new OutgoingOpcode(74, -1);

	int								eventType;
	char							keyChar;
	int								keyCode;
	int								modifiers;
	long							timeStamp;

	@Override
	public final int getEventType(int i) {
		return eventType;
	}

	@Override
	public final char getKeyChar(int i) {
		return keyChar;
	}

	@Override
	public final int getKeyCode(boolean bool) {
		return keyCode;
	}

	@Override
	public final int getModifiers(byte i) {
		return modifiers;
	}

	@Override
	public final long getTimeStamp(int i) {
		return timeStamp;
	}
}

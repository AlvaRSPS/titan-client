
/* Class77_Sub1 - Decompiled by JODE
 */ package com.jagex.game.input.impl; /*
										*/

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Method;

import com.Class48_Sub1_Sub2;
import com.Class65;
import com.FriendLoginUpdate;
import com.RtAwtFontWrapper;
import com.SignLink;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.input.RtKeyEvent;
import com.jagex.game.input.RtKeyListener;

public final class AwtKeyListener extends RtKeyListener implements KeyListener, FocusListener {
	public static RtAwtFontWrapper		aClass326_3805;
	public static QuickChatMessageType	aClass348_3801;
	public static int					anInt3803	= -2;
	public static int[]					anIntArray3804;

	public static int[]					keyMap		= { 0, 0, 0, 0, 0, 0, 0, 0, 85, 80, 84, 0, 91, 0, 0, 0, 81, 82, 86, 0, 0, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 83, 104, 105, 103, 102, 96, 98, 97, 99, 0, 0, 0, 0, 0, 0, 0, 25, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 0, 0, 0, 0, 0, 0, 48, 68, 66, 50,
			34, 51, 52, 53, 39, 54, 55, 56, 70, 69, 40, 41, 32, 35, 49, 36, 38, 67, 33, 65, 37, 64, 0, 0, 0, 0, 0, 228, 231, 227, 233, 224, 219, 225, 230, 226, 232, 89, 87, 0, 88, 229, 90, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 0, 0, 101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	static {
		aClass348_3801 = new QuickChatMessageType(1, 2, 2, 0);
	}

	public static final void adaptKeyMap(boolean bool) {
		do {
			if (SignLink.java_vendor.toLowerCase().indexOf("microsoft") != -1) {
				keyMap[192] = 58;
				keyMap[191] = 73;
				keyMap[220] = 74;
				keyMap[221] = 43;
				keyMap[188] = 71;
				keyMap[190] = 72;
				keyMap[189] = 26;
				keyMap[223] = 28;
				keyMap[187] = 27;
				keyMap[219] = 42;
				keyMap[222] = 59;
				keyMap[186] = 57;
			} else {
				keyMap[45] = 26;
				keyMap[92] = 74;
				keyMap[46] = 72;
				keyMap[59] = 57;
				keyMap[47] = 73;
				keyMap[91] = 42;
				if (SignLink.setFocusTraversalKeysEnabled != null) {
					keyMap[222] = 58;
					keyMap[520] = 59;
					keyMap[192] = 28;
				} else {
					keyMap[222] = 59;
					keyMap[192] = 58;
				}
				keyMap[61] = 27;
				keyMap[93] = 43;
				keyMap[44] = 71;
			}
			break;
		} while (false);
	}

	public static final boolean isValidKeyChar(byte i, char character) {
		if (character > 0 && character < 128 || character >= 160 && (character ^ 0xffffffff) >= -256) {
			return true;
		}
		if (character != 0) {
			char[] unicodeEscapes = Class65.unicodeUnescapes;
			for (char _char : unicodeEscapes) {
				if (character == _char) {
					return true;
				}
			}
		}
		return false;
	}

	public static void method785(int i) {
		try {
			if (i != 3) {
				method785(48);
			}
			aClass348_3801 = null;
			anIntArray3804 = null;
			aClass326_3805 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oca.O(" + i + ')');
		}
	}

	public static final void method787(byte i, int i_8_) {
		try {
			if (i <= 88) {
				method787((byte) -39, 78);
			}
			FriendLoginUpdate.aClass79_6170.makeSoftReferences((byte) 62, i_8_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oca.I(" + i + ',' + i_8_ + ')');
		}
	}

	public static final boolean validateInteger(byte i, String string) {
		try {
			if (i != 53) {
				anIntArray3804 = null;
			}
			return Class48_Sub1_Sub2.method468(true, 30883, 10, string);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oca.J(" + i + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	private Component	comp;
	private LinkedList	eventQueque		= new LinkedList();

	private boolean[]	keyStatus		= new boolean[112];

	private LinkedList	processedEvents	= new LinkedList();

	public AwtKeyListener(Component component) {
		adaptKeyMap(false);
		setComponent(component, -124);
	}

	@Override
	public final void focusGained(FocusEvent focusevent) {
		/* empty */
	}

	@Override
	public final synchronized void focusLost(FocusEvent focusevent) {
		postEvent(-1, 0, '\0', false);
	}

	private final int getActiveModifiers(byte i) {
		int modifierWord = 0;
		if (keyStatus[81]) {
			modifierWord |= 0x1;
		}
		if (keyStatus[82]) {
			modifierWord |= 0x4;
		}
		if (keyStatus[86]) {
			modifierWord |= 0x2;
		}
		return modifierWord;
	}

	@Override
	public final RtKeyEvent getNextEvent(byte i) {
		return (RtKeyEvent) processedEvents.removeFirst(6494);
	}

	private final void handleLowLevelKeyEvent(byte i, int eventType, KeyEvent keyevent) {
		int keyCode = keyevent.getKeyCode();
		if (keyCode != 0) {
			if (keyCode >= 0 && (keyCode ^ 0xffffffff) > (keyMap.length ^ 0xffffffff)) {
				keyCode = keyMap[keyCode];
				if (eventType != 0 || (keyCode & 0x80 ^ 0xffffffff) == -1) {
					keyCode &= ~0x80;
				} else {
					keyCode = 0;
				}
			} else {
				keyCode = 0;
			}
		} else {
			keyCode = 0;
		}
		if ((keyCode ^ 0xffffffff) != -1) {
			postEvent(eventType, keyCode, '\0', false);
			keyevent.consume();
		}
	}

	@Override
	public final boolean isKeyDown(int keyCode, int i_10_) {
		if ((keyCode ^ 0xffffffff) > -1 || keyCode >= 112) {
			return false;
		}
		return keyStatus[keyCode];
	}

	@Override
	public final synchronized void keyPressed(KeyEvent keyevent) {
		handleLowLevelKeyEvent((byte) 61, 0, keyevent);
	}

	@Override
	public final synchronized void keyReleased(KeyEvent keyevent) {
		handleLowLevelKeyEvent((byte) 61, 1, keyevent);
	}

	@Override
	public final synchronized void keyTyped(KeyEvent keyevent) {
		char keyChar = keyevent.getKeyChar();
		if (keyChar != 0 && isValidKeyChar((byte) 102, keyChar)) {
			postEvent(3, -1, keyChar, false);
			keyevent.consume();
		}
	}

	@Override
	public final void method773(byte i) {
		if (i <= -9) {
			removeComponent(128);
		}
	}

	@Override
	public final synchronized void method774(byte i) {
		do {
			processedEvents.clear((byte) 47);
			for (AwtKeyEvent inEvent = (AwtKeyEvent) eventQueque.removeFirst(6494); inEvent != null; inEvent = (AwtKeyEvent) eventQueque.removeFirst(6494)) {
				inEvent.modifiers = getActiveModifiers((byte) 127);
				if (inEvent.eventType != 0) {
					if ((inEvent.eventType ^ 0xffffffff) == -2) {
						if (keyStatus[inEvent.keyCode]) {
							processedEvents.addLast(inEvent, -20911);
							keyStatus[inEvent.keyCode] = false;
						}
					} else if (inEvent.eventType == -1) {
						for (int keyCode = 0; (keyCode ^ 0xffffffff) > -113; keyCode++) {
							if (keyStatus[keyCode]) {
								AwtKeyEvent outEvent = new AwtKeyEvent();
								outEvent.modifiers = inEvent.modifiers;
								outEvent.keyCode = keyCode;
								outEvent.timeStamp = inEvent.timeStamp;
								outEvent.eventType = 1;
								outEvent.keyChar = '\0';
								processedEvents.addLast(outEvent, -20911);
								keyStatus[keyCode] = false;
							}
						}
					} else if (inEvent.eventType == 3) {
						processedEvents.addLast(inEvent, -20911);
					}
				} else {
					if (!keyStatus[inEvent.keyCode]) {
						AwtKeyEvent outEvent = new AwtKeyEvent();
						outEvent.timeStamp = inEvent.timeStamp;
						outEvent.modifiers = inEvent.modifiers;
						outEvent.eventType = 0;
						outEvent.keyChar = '\0';
						outEvent.keyCode = inEvent.keyCode;
						processedEvents.addLast(outEvent, -20911);
						keyStatus[inEvent.keyCode] = true;
					}
					inEvent.eventType = 2;
					processedEvents.addLast(inEvent, -20911);
				}
			}
			break;
		} while (false);
	}

	private final void postEvent(int eventType, int keyCode, char keyChar, boolean bool) {
		AwtKeyEvent event = new AwtKeyEvent();
		event.keyCode = keyCode;
		event.keyChar = keyChar;
		event.eventType = eventType;
		event.timeStamp = TimeTools.getCurrentTime(-47);
		eventQueque.addLast(event, -20911);
	}

	private final void removeComponent(int i) {
		if (comp != null) {
			comp.removeKeyListener(this);
			comp.removeFocusListener(this);
			comp = null;
			for (int i_0_ = 0; i_0_ < 112; i_0_++) {
				keyStatus[i_0_] = false;
			}
			processedEvents.clear((byte) 47);
			eventQueque.clear((byte) 47);
		}
	}

	private final void setComponent(Component component, int i) {
		do {
			removeComponent(128);
			comp = component;
			Method method = SignLink.setFocusTraversalKeysEnabled;
			if (method != null) {
				try {
					method.invoke(comp, Boolean.FALSE);
				} catch (Throwable throwable) {
					/* empty */
				}
			}
			comp.addKeyListener(this);
			comp.addFocusListener(this);
			break;
		} while (false);
	}
}


/* Class299_Sub2 - Decompiled by JODE
 */ package com.jagex.game.input.impl; /*
										*/

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.OutgoingOpcode;
import com.client;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.input.RtMouseListener;

public final class AwtMouseListener extends RtMouseListener implements MouseListener, MouseMotionListener {
	public static byte[][]			aByteArrayArray5291;
	public static OutgoingOpcode	aClass171_5296	= new OutgoingOpcode(72, 3);
	public static int				anInt5298		= 16777215;
	public static int[]				anIntArray5301	= { 1, 2, 4, 8 };

	public static void method3522(boolean bool) {
		try {
			aByteArrayArray5291 = null;
			client.lobbyServer = null;
			if (bool != false) {
				method3526(-120, -17);
			}
			anIntArray5301 = null;
			aClass171_5296 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.S(" + bool + ')');
		}
	}

	public static final void method3523(int i, int i_1_, int i_2_) {
		if ((RenderAnimDefinitionParser.anInt1948 ^ 0xffffffff) != -1) {
			if (i_1_ < 0) {
				for (int i_3_ = 0; (i_3_ ^ 0xffffffff) > -17; i_3_++) {
					AwtKeyListener.anIntArray3804[i_3_] = i;
				}
			} else {
				AwtKeyListener.anIntArray3804[i_1_] = i;
			}
		}
		BConfigDefinition.aClass98_Sub31_Sub2_3122.method1363(-17, i_1_, i);
	}

	public static final boolean method3526(int i, int i_5_) {
		try {
			if (i < 113) {
				return false;
			}
			if (i_5_ == 13 || i_5_ == 23 || (i_5_ ^ 0xffffffff) == -3 || i_5_ == 30 || i_5_ == 18) {
				return true;
			}
			return i_5_ == 58 || (i_5_ ^ 0xffffffff) == -1009;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.M(" + i + ',' + i_5_ + ')');
		}
	}

	public static final int method3527(int i, int i_6_) {
		try {
			if (i_6_ != 770356935) {
				client.lobbyServer = null;
			}
			return i >>> 770356935;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.V(" + i + ',' + i_6_ + ')');
		}
	}

	private boolean		aBoolean5299;
	private LinkedList	aClass148_5287	= new LinkedList();
	private LinkedList	aClass148_5292	= new LinkedList();
	private Component	aComponent5300;
	private int			anInt5288;

	private int			anInt5289;

	private int			anInt5290;

	private int			anInt5293;

	private int			anInt5294;

	private int			anInt5295;

	public AwtMouseListener(Component component, boolean bool) {
		try {
			method3525(-32, component);
			aBoolean5299 = bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.<init>(" + (component != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	@Override
	public final int getMouseX(int i) {
		try {
			if (i <= 4) {
				return 121;
			}
			return anInt5289;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.E(" + i + ')');
		}
	}

	@Override
	public final int getMouseY(byte i) {
		try {
			if (i < 24) {
				mouseExited(null);
			}
			return anInt5288;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.B(" + i + ')');
		}
	}

	@Override
	public final RtMouseEvent getNextEvent(int i) {
		try {
			if (i != 600) {
				aClass148_5292 = null;
			}
			return (RtMouseEvent) aClass148_5287.removeFirst(6494);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.G(" + i + ')');
		}
	}

	@Override
	public final boolean method3506(byte i) {
		try {
			return (0x1 & anInt5290 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.L(" + i + ')');
		}
	}

	@Override
	public final boolean method3510(byte i) {
		try {
			if (i != -19) {
				method3512(-127);
			}
			return (anInt5290 & 0x2 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.C(" + i + ')');
		}
	}

	@Override
	public final boolean method3512(int i) {
		try {
			if (i != 1) {
				return false;
			}
			return (0x4 & anInt5290 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.I(" + i + ')');
		}
	}

	@Override
	public final void method3515(int i) {
		try {
			if (i < -110) {
				method3530((byte) 119);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.H(" + i + ')');
		}
	}

	@Override
	public final synchronized void method3516(byte i) {
		try {
			anInt5288 = anInt5293;
			anInt5290 = anInt5294;
			anInt5289 = anInt5295;
			LinkedList class148 = aClass148_5287;
			aClass148_5287 = aClass148_5292;
			aClass148_5292 = class148;
			if (i <= 103) {
				anInt5290 = 55;
			}
			aClass148_5292.clear((byte) 47);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.A(" + i + ')');
		}
	}

	private final void method3525(int i, Component component) {
		try {
			method3530((byte) -40);
			aComponent5300 = component;
			aComponent5300.addMouseListener(this);
			aComponent5300.addMouseMotionListener(this);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.R(" + i + ',' + (component != null ? "{...}" : "null") + ')');
		}
	}

	private final int method3528(int i, MouseEvent mouseevent) {
		int i_7_ = mouseevent.getModifiers();
		boolean bool = (i_7_ & 0x10) != 0;
		boolean bool_8_ = (0x8 & i_7_) != 0;
		boolean bool_9_ = (0x4 & i_7_) != 0;
		if (bool_8_ && (bool || bool_9_)) {
			bool_8_ = false;
		}
		if (bool && bool_9_) {
			return 4;
		}
		if (bool) {
			return 1;
		}
		if (i != 23542) {
			anInt5290 = -41;
		}
		if (bool_8_) {
			return 2;
		}
		if (bool_9_) {
			return 4;
		}
		return 0;
	}

	private final void method3529(int i, int i_10_, int i_11_, int i_12_, int i_13_) {
		AwtMouseEvent class98_sub17_sub1 = new AwtMouseEvent();
		class98_sub17_sub1.anInt5774 = i_11_;
		class98_sub17_sub1.anInt5779 = i_13_;
		class98_sub17_sub1.anInt5775 = i_10_;
		class98_sub17_sub1.anInt5776 = i_12_;
		class98_sub17_sub1.aLong5777 = TimeTools.getCurrentTime(i ^ ~0x58ab);
		aClass148_5292.addLast(class98_sub17_sub1, i ^ ~0x92b);
	}

	private final void method3530(byte i) {
		if (aComponent5300 != null) {
			aComponent5300.removeMouseMotionListener(this);
			aComponent5300.removeMouseListener(this);
			anInt5295 = anInt5293 = anInt5294 = 0;
			anInt5289 = anInt5288 = anInt5290 = 0;
			aComponent5300 = null;
			aClass148_5287 = null;
			aClass148_5292 = null;
		}
	}

	private final void method3531(int i, int i_15_, int i_16_) {
		do {
			if (i != -22490) {
				method3525(-122, null);
			}
			anInt5293 = i_16_;
			anInt5295 = i_15_;
			if (!aBoolean5299) {
				break;
			}
			method3529(22661, i_15_, 0, i_16_, -1);
			break;
		} while (false);
	}

	@Override
	public final synchronized void mouseClicked(MouseEvent mouseevent) {
		do {
			try {
				if (!mouseevent.isPopupTrigger()) {
					break;
				}
				mouseevent.consume();
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "un.mouseClicked(" + (mouseevent != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final synchronized void mouseDragged(MouseEvent mouseevent) {
		try {
			method3531(-22490, mouseevent.getX(), mouseevent.getY());
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.mouseDragged(" + (mouseevent != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final synchronized void mouseEntered(MouseEvent mouseevent) {
		try {
			method3531(-22490, mouseevent.getX(), mouseevent.getY());
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.mouseEntered(" + (mouseevent != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final synchronized void mouseExited(MouseEvent mouseevent) {
		try {
			method3531(-22490, mouseevent.getX(), mouseevent.getY());
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.mouseExited(" + (mouseevent != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final synchronized void mouseMoved(MouseEvent mouseevent) {
		try {
			method3531(-22490, mouseevent.getX(), mouseevent.getY());
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "un.mouseMoved(" + (mouseevent != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final synchronized void mousePressed(MouseEvent mouseevent) {
		do {
			try {
				int i = method3528(23542, mouseevent);
				if ((i ^ 0xffffffff) == -2) {
					method3529(22661, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY(), 0);
				} else if ((i ^ 0xffffffff) != -5) {
					if ((i ^ 0xffffffff) == -3) {
						method3529(22661, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY(), 1);
					}
				} else {
					method3529(22661, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY(), 2);
				}
				anInt5294 |= i;
				if (!mouseevent.isPopupTrigger()) {
					break;
				}
				mouseevent.consume();
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "un.mousePressed(" + (mouseevent != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final synchronized void mouseReleased(MouseEvent mouseevent) {
		do {
			try {
				int i = method3528(23542, mouseevent);
				if ((i & anInt5294 ^ 0xffffffff) == -1) {
					i = anInt5294;
				}
				if ((0x1 & i) != 0) {
					method3529(22661, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY(), 3);
				}
				if ((0x4 & i) != 0) {
					method3529(22661, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY(), 5);
				}
				if ((0x2 & i ^ 0xffffffff) != -1) {
					method3529(22661, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY(), 4);
				}
				anInt5294 &= i ^ 0xffffffff;
				if (!mouseevent.isPopupTrigger()) {
					break;
				}
				mouseevent.consume();
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "un.mouseReleased(" + (mouseevent != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}
}


/* Class54 - Decompiled by JODE
 */ package com.jagex.game.client.loading.monitor; /*
													*/

import com.Class191;
import com.Class35;

public final class NativeProgressMonitor implements ProgressMonitor {
	public static int		anInt3391;
	public static int		anInt3394;
	public static int		anInt3395			= 0;
	public static String[]	aStringArray3393	= new String[100];

	static {
		anInt3394 = 0;
	}

	private boolean	hadError;

	private String	nativeName;

	public NativeProgressMonitor(String string) {
		nativeName = string;
	}

	@Override
	public final int getProgress(byte i) {
		int progress = Class35.getNativeProgress(-120, nativeName);
		if (progress >= 0 && (progress ^ 0xffffffff) >= -101) {
			return progress;
		}
		hadError = true;
		return 100;
	}

	@Override
	public final Class191 getType(int i) {
		return Class191.NATIVE_LIBRARY;
	}

	public final boolean hadError(boolean bool) {
		return hadError;
	}
}

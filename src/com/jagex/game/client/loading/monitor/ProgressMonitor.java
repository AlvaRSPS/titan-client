/* Interface14 - Decompiled by JODE
 */ package com.jagex.game.client.loading.monitor; /*
													*/

import com.Class191;

public interface ProgressMonitor {
	int getProgress(byte i);

	Class191 getType(int i);
}

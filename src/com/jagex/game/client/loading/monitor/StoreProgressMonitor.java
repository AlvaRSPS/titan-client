
/* PlayerUpdate - Decompiled by JODE
 */ package com.jagex.game.client.loading.monitor; /*
													*/

import com.Class151_Sub9;
import com.Class191;
import com.Class2;
import com.Class319;
import com.Class351;
import com.Class46;
import com.Class76_Sub9;
import com.Class98_Sub10_Sub20;
import com.OpenGlPointLight;
import com.Player;
import com.ProceduralTextureSource;
import com.RsBitsBuffers;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class StoreProgressMonitor implements ProgressMonitor {
	public static boolean				aBoolean3413	= false;
	public static AdvancedMemoryCache	aClass79_3411	= new AdvancedMemoryCache(5);

	public static final void method2855(int i) {
		OpenGlPointLight.aHa4185.method1791(Class46.aFloat388, ProceduralTextureSource.aFloat3260, AnimationDefinition.aFloat831);
		// System.out.println("A: " + Class46.aFloat388 + " B: " +
		// ProceduralTextureSource.aFloat3260 + " C: " +
		// AnimationDefinition.aFloat831);
	}

	public static final int method2859(int i, boolean bool, String string, int i_15_) {
		if (i < 2 || (i ^ 0xffffffff) < -37) {
			throw new IllegalArgumentException("Invalid radix:" + i);
		}
		boolean bool_16_ = false;
		if (i_15_ != -21972) {
			method2859(-69, true, null, 22);
		}
		boolean bool_17_ = false;
		int i_18_ = 0;
		int i_19_ = string.length();
		for (int i_20_ = 0; (i_19_ ^ 0xffffffff) < (i_20_ ^ 0xffffffff); i_20_++) {
			int i_21_ = string.charAt(i_20_);
			if (i_20_ == 0) {
				if (i_21_ == 45) {
					bool_16_ = true;
					continue;
				}
				if (i_21_ == 43 && bool) {
					continue;
				}
			}
			if (i_21_ >= 48 && (i_21_ ^ 0xffffffff) >= -58) {
				i_21_ -= 48;
			} else if ((i_21_ ^ 0xffffffff) <= -66 && i_21_ <= 90) {
				i_21_ -= 55;
			} else if (i_21_ >= 97 && (i_21_ ^ 0xffffffff) >= -123) {
				i_21_ -= 87;
			} else {
				throw new NumberFormatException();
			}
			if (i <= i_21_) {
				throw new NumberFormatException();
			}
			if (bool_16_) {
				i_21_ = -i_21_;
			}
			int i_22_ = i_18_ * i - -i_21_;
			if (i_22_ / i != i_18_) {
				throw new NumberFormatException();
			}
			i_18_ = i_22_;
			bool_17_ = true;
		}
		if (!bool_17_) {
			throw new NumberFormatException();
		}
		return i_18_;
	}

	public static final void updateGlobalPlayers(RsBitsBuffers packet, int i) {
		packet.startBitwiseAccess(0);
		boolean fullSync = packet.readBits((byte) -106, 1) == 1;
		if (fullSync) {
			Class98_Sub10_Sub20.decodeMovementBlock(OpenGLHeap.localPlayerIndex, 12, packet);
		}
		for (int index = 0; index < Class2.playerCount - 1; index++) {
			boolean bool_2_ = packet.readBits((byte) -106, 1) == 1;
			if (bool_2_) {
				int i_4_ = packet.readBits((byte) -106, 11);
				Class98_Sub10_Sub20.decodeMovementBlock(i_4_, 12, packet);
			}
		}
		while (packet.readBits((byte) -106, 1) != 0) {
			int i_5_ = packet.readBits((byte) -106, 11);
			Class351.processOutsideUpdate(-2, i_5_, packet);
		}
		packet.endBitwiseAccess((byte) 120);
		SimpleProgressBarLoadingScreenElement.anInt5473 = 0;
		Class2.playerCount = 0;
		for (int i_14_ = 1; (i_14_ ^ 0xffffffff) > -2049; i_14_++) {
			GamePreferences.aByteArray4075[i_14_] >>= 1;
			Player player = Class151_Sub9.players[i_14_];
			if (player == null) {
				Class76_Sub9.anIntArray3791[SimpleProgressBarLoadingScreenElement.anInt5473++] = i_14_;
			} else {
				Class319.playerIndices[Class2.playerCount++] = i_14_;
			}
		}
	}

	private Js5 js5;

	public StoreProgressMonitor(Js5 js5) {
		this.js5 = js5;
	}

	@Override
	public final int getProgress(byte i) {
		if (js5.isFullyCached((byte) 78)) {
			return 100;
		}
		return js5.getProgress((byte) -114);
	}

	@Override
	public final Class191 getType(int i) {
		return Class191.JS5_STORE;
	}
}

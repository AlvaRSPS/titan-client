/* Class180 - Decompiled by JODE
 */ package com.jagex.game.client.loading.monitor; /*
													*/

import com.Class191;
import com.GameShell;
import com.IncomingOpcode;
import com.OutgoingPacket;
import com.jagex.game.client.archive.Js5;

public final class FileProgressMonitor implements ProgressMonitor {
	public static IncomingOpcode	aClass58_3398;
	public static float				aFloat3405;
	private static short[][]		aShortArrayArray3401	= { { 6798, 12, 78, 8384, 14511, 9162, 5056, 939, 5025, 4760, 9108, 7719, 14241, 22443, 30247, -29781, -25675, -21568, -17472, -12373, -8256, -3545 }, { 8741, 12, 78, 8384, 14511, 9162, 5056, 939, 5025, 4760, 9108, 7719, 14241, 22443,
			30247, -29781, -25675, -21568, -17472, -12373, -8256, -3545 }, { 25238, 12, 78, 8384, 14511, 9162, 5056, 939, 5025, 4760, 9108, 7719, 14241, 22443, 30247, -29781, -25675, -21568, -17472, -12373, -8256, -3545 }, { 4626, 12, 78, 8384, 14511, 9162, 5056, 939, 5025, 4760, 9108, 7719, 14241,
					22443, 30247, -29781, -25675, -21568, -17472, -12373, -8256, -3545 }, { 4550, 12, 78, 8384, 14511, 9162, 5056, 939, 5025, 4760, 9108, 7719, 14241, 22443, 30247, -29781, -25675, -21568, -17472, -12373, -8256, -3545 } };
	private static short[][]		aShortArrayArray3402	= { new short[0], new short[0], new short[0], new short[0], new short[0] };
	private static short[][]		aShortArrayArray3403	= { new short[0], new short[0], new short[0], new short[0], new short[0] };
	public static short[][][]		aShortArrayArrayArray3397;

	static {
		aShortArrayArrayArray3397 = new short[][][] { aShortArrayArray3401, aShortArrayArray3402, aShortArrayArray3403 };
		aClass58_3398 = new IncomingOpcode(109, 11);
		GameShell.stopTimer = 0L;
		aFloat3405 = 0.0F;
	}

	public static final void method2604(int i, int i_0_, int i_1_, int i_2_, OutgoingPacket frame) {
		frame.packet.writeInt(1571862888, i_1_);
		frame.packet.writeLEShortA(i, i_0_ + 21696);
		frame.packet.writeShort(i_2_, 1571862888);
	}

	private Js5		js5;

	private String	name;

	public FileProgressMonitor(Js5 js5, String name) {
		this.js5 = js5;
		this.name = name;

	}

	@Override
	public final int getProgress(byte i) {
		if (js5.isCached(name, false)) {
			return 100;
		}
		return 0;
	}

	@Override
	public final Class191 getType(int i) {
		return Class191.JS5_FILE;
	}
}

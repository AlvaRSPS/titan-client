/* Class217 - Decompiled by JODE
 */ package com.jagex.game.client.loading.monitor; /*
													*/

import com.Class151_Sub7;
import com.Class191;
import com.IncomingOpcode;
import com.PlayerUpdateMask;
import com.Sprite;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.input.impl.AwtMouseEvent;

public final class GroupProgressMonitor implements ProgressMonitor {
	public static boolean[]			aBooleanArray3410	= new boolean[5];
	public static Sprite[]			aClass332Array3408;
	public static IncomingOpcode	aClass58_3406		= new IncomingOpcode(59, 3);

	public static final boolean isOpen(int i) {
		return AwtMouseEvent.consoleOpen;
	}

	public static final void method2799(int i, int i_0_, boolean bool, byte i_1_) {
		if ((i_0_ ^ 0xffffffff) > -8001 || (i_0_ ^ 0xffffffff) < -48001) {
			throw new IllegalArgumentException();
		}
		PlayerUpdateMask.anInt529 = i;
		RemoveRoofsPreferenceField.anInt3678 = i_0_;
		Class151_Sub7.aBoolean5007 = bool;
	}

	private Js5		js5;

	private String	name;

	public GroupProgressMonitor(Js5 js5, String string) {
		this.js5 = js5;
		name = string;
	}

	@Override
	public final int getProgress(byte i) {
		if (js5.isGroupCached(name, 0)) {
			return 100;
		}
		return js5.getGroupProgress(29952, name);
	}

	@Override
	public final Class191 getType(int i) {
		return Class191.JS5_GROUP;
	}
}

/* Class47 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading; /*
												*/

import com.Class305_Sub1;
import com.Class76_Sub4;
import com.Exception_Sub1;
import com.GameShell;
import com.Sprite;
import com.StartupStage;
import com.client;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.ui.loading.impl.AwtLoadingScreen;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public final class LoadingScreenRenderer implements Runnable {
	public static int anInt407 = 0;

	public static void method449(int i) {
		LoadingScreenElementType.SIMPLE_PROGRESS_BAR = null;
	}

	private int					frameCount;
	private LoadingScreen		loadingScreen			= new AwtLoadingScreen();
	private boolean				needsRefresh;
	private int					percentage;
	private LoadingScreen		previousLoadingScreen	= null;
	private String				progressText;
	private long				progressTimestamp;
	private long				screenTimestamp;

	private volatile boolean	shutDown;

	private StartupStage		startupStage;

	public LoadingScreenRenderer() {
		/* empty */
	}

	public final int getFrameCount(byte i) {
		return frameCount;
	}

	public final int getPercentage(int i) {
		return percentage;
	}

	public final long getPrgressTimestamp(byte i) {
		return progressTimestamp;
	}

	public final StartupStage getStage(byte i) {
		return startupStage;
	}

	public final int getStageEndProgress(int i) {
		if (startupStage == null) {
			return 0;
		}
		int stage = startupStage.getId((byte) -10);
		if (startupStage.gradualProgress && (percentage ^ 0xffffffff) > (startupStage.percentageStop ^ 0xffffffff)) {
			return percentage + 1;
		}
		if ((stage ^ 0xffffffff) > -1 || stage >= -1 + client.startupStages.length) {
			return 100;
		}
		if ((percentage ^ 0xffffffff) == (startupStage.percentageStart ^ 0xffffffff)) {
			return startupStage.percentageStop;
		}
		return startupStage.percentageStart;
	}

	public final String getText(byte i) {
		return progressText;
	}

	public final synchronized boolean isCurrentScreenActive(byte i) {
		return loadingScreen.isActive(-105, screenTimestamp);
	}

	public final synchronized void refresh(int i) {
		needsRefresh = true;
	}

	@Override
	public final void run() {
		while (!shutDown) {
			long currentTime = TimeTools.getCurrentTime(-47);
			synchronized (this) {
				try {
					frameCount++;
					if (!(loadingScreen instanceof AwtLoadingScreen)) {
						long currTime = TimeTools.getCurrentTime(-47);
						if (client.graphicsToolkit == null || previousLoadingScreen == null || previousLoadingScreen.getFadeDuration(-24591) == 0 || (screenTimestamp ^ 0xffffffffffffffffL) > (-(long) previousLoadingScreen.getFadeDuration(-24591) + currTime ^ 0xffffffffffffffffL)) {
							if (previousLoadingScreen != null) {
								needsRefresh = true;
								previousLoadingScreen.shutDown(-26363);
								previousLoadingScreen = null;
							}
							if (needsRefresh) {
								client.clearAwtGraphics(14059);
								if (client.graphicsToolkit != null) {
									client.graphicsToolkit.clearImage(0);// GA
																			// method
																			// is
																			// clearImage
								}
							}
							loadingScreen.drawLoadingScreen(32210, needsRefresh || client.graphicsToolkit != null && client.graphicsToolkit.method1819());
						} else {
							int alpha = (int) ((currTime - screenTimestamp) * 255L / previousLoadingScreen.getFadeDuration(-24591));
							int previousAlpha = -alpha + 255;
							client.clearAwtGraphics(14059);
							previousAlpha = 0xffffff | previousAlpha << 511614744;
							alpha = 0xffffff | alpha << -1078525736;
							client.graphicsToolkit.clearImage(0);
							Sprite sprite = client.graphicsToolkit.createSprite(GameShell.frameWidth, GameShell.frameHeight, true);
							client.graphicsToolkit.setRenderTarget(-74, sprite);
							previousLoadingScreen.drawLoadingScreen(32210, true);
							client.graphicsToolkit.resetRenderTarget();
							sprite.draw(0, 0, 0, previousAlpha, 1);
							client.graphicsToolkit.setRenderTarget(-123, sprite);
							client.graphicsToolkit.clearImage(0);
							loadingScreen.drawLoadingScreen(32210, true);
							client.graphicsToolkit.resetRenderTarget();
							sprite.draw(0, 0, 0, alpha, 1);
						}
						try {
							if (client.graphicsToolkit != null && !(loadingScreen instanceof AwtLoadingScreen)) {
								client.graphicsToolkit.renderFrame(-128);
							}
						} catch (Exception_Sub1 exception_sub1) {
							Class305_Sub1.reportError(exception_sub1, -121, exception_sub1.getMessage() + " (Recovered) " + client.activeClient.generateDebugInfoString(0));
							Class76_Sub4.method754(0, true, 57);
						}
					} else {
						loadingScreen.drawLoadingScreen(32210, needsRefresh);
					}
					java.awt.Container container;
					if (GameShell.frame == null) {
						if (GameShell.applet == null) {
							container = GameShell.activeGameShell;
						} else {
							container = GameShell.applet;
						}
					} else {
						container = GameShell.frame;
					}
					container.getSize();
					container.getSize();
					if (GameShell.frame == container) {
						GameShell.frame.getInsets();
					}
					needsRefresh = false;
					if (client.graphicsToolkit != null && !(loadingScreen instanceof AwtLoadingScreen) && (startupStage.getId((byte) -10) ^ 0xffffffff) > (StartupStage.aClass75_577.getId((byte) -10) ^ 0xffffffff)) {
						client.handleResize((byte) -90);
					}
				} catch (Exception exception) {
					continue;
				}
			}
			long currTime_ = TimeTools.getCurrentTime(-47);
			int i = (int) (20L + currentTime + -currTime_);
			if ((i ^ 0xffffffff) < -1) {
				TimeTools.sleep(0, i);
			}
		}
	}

	public final synchronized void setLoadingScreen(byte i, LoadingScreen screen) {
		previousLoadingScreen = loadingScreen;
		loadingScreen = screen;
		screenTimestamp = TimeTools.getCurrentTime(-47);
	}

	public final synchronized void setProgress(long timeStamp, byte i, int perc, StartupStage stage, String text) {
		progressTimestamp = timeStamp;
		startupStage = stage;
		percentage = perc;
		progressText = text;
		// System.out.println(text);
	}

	public final void shutDown(boolean bool) {
		shutDown = true;
	}
}

/* Interface10 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading; /*
												*/

public interface LoadingScreen {
	void drawLoadingScreen(int i, boolean bool);

	int getCacheProgress(int i);

	int getFadeDuration(int i);

	boolean isActive(int i, long l);

	void loadMedia(int i);

	void shutDown(int i);
}

/* Class133 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.Class21_Sub3;
import com.Class85;
import com.GameShell;
import com.client;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;

public final class RectangleLoadingScreenElement implements LoadingScreenElement {
	public static Class85	aClass85_3454	= new Class85(5, 3);
	public static long		aLong3455;
	public static int		chatLinesCount;

	public static final boolean method2239(int i, int i_0_, byte i_1_) {
		boolean bool = (i & 0x37) == 0 ? GamePreferences.method1292(i, (byte) 117, i_0_) : Class21_Sub3.method276(i_0_, 15123, i);
		return bool | SpriteProgressBarLoadingScreenElement.method3978(i, i_0_, (byte) 103) | (0x10000 & i_0_) != 0;
	}

	private BackgroundColourLSEConfig config;

	public RectangleLoadingScreenElement(BackgroundColourLSEConfig config) {
		this.config = config;
	}

	@Override
	public final void draw(boolean bool, byte i) {
		if (bool) {
			client.graphicsToolkit.fillRectangle(0, 0, GameShell.frameWidth, GameShell.frameHeight, config.colour, 0);
		}
	}

	@Override
	public final boolean isCached(int i) {
		return true;
	}

	@Override
	public final void loadMedia(byte i) {
	}
}

/* Interface18 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements; /*
															*/

public interface LoadingScreenElement {
	void draw(boolean bool, byte i);

	boolean isCached(int i);

	void loadMedia(byte i);
}

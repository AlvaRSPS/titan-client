/* Class373_Sub1_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.Class126;
import com.Class161;
import com.Class172;
import com.Class185;
import com.Class216;
import com.Class224_Sub1;
import com.Class228;
import com.Class242;
import com.Class246_Sub4_Sub2_Sub1;
import com.Class246_Sub5;
import com.Class258;
import com.Class273;
import com.Class351;
import com.Class89;
import com.ClientStream;
import com.PacketBufferManager;
import com.ParticleManager;
import com.SceneGraphNodeList;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.loading.impl.AwtLoadingScreen;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;

public final class AnimatedLoadingBarLoadingScreenElement extends DecoratedProgressBarLoadingScreenElement {
	public static Class126 aClass126_6216 = new Class126();

	public static final void method3970(Js5 class207, byte i) {
		ClientStream.anInt3089 = 0;
		Class351.anInt2922 = 0;
		ParticleManager.systems = new SceneGraphNodeList();
		Class185.aClass246_Sub4_Sub2_Sub1Array1445 = new Class246_Sub4_Sub2_Sub1[1024];
		SimpleProgressBarLoadingScreenElement.aClass246_Sub5Array5469 = new Class246_Sub5[Class224_Sub1.anIntArray5034[RotatingSpriteLSEConfig.anInt5497] + 1];
		Class273.anInt2039 = 0;
		Class258.anInt1952 = 0;
		Class242.method2935((byte) 40, class207);
		Class89.method880(-13258, class207);
	}

	public static final void method3972(int i, int i_0_, int i_1_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_][i_1_];
		if (class172 != null) {
			PacketBufferManager.method2227(class172.aClass246_Sub3_Sub3_1324);
			PacketBufferManager.method2227(class172.aClass246_Sub3_Sub3_1333);
			if (class172.aClass246_Sub3_Sub3_1324 != null) {
				class172.aClass246_Sub3_Sub3_1324 = null;
			}
			if (class172.aClass246_Sub3_Sub3_1333 != null) {
				class172.aClass246_Sub3_Sub3_1333 = null;
			}
		}
	}

	public static final boolean method3973(int i, int i_2_, int i_3_) {
		return !(!((i & 0x60000 ^ 0xffffffff) != -1 | Class161.method2514(i, 16, i_2_)) && !Class228.method2864(55, i, i_2_) && !Class216.method2793(i_2_, (byte) -112, i));
	}

	public AnimatedLoadingBarLoadingScreenElement(Js5 glyphStore, Js5 fontMetricsStore, AnimatedProgressBarLSEConfig config) {
		super(glyphStore, fontMetricsStore, config);
	}

	@Override
	public final void drawBarForeground(int height, int width, int y, int x, byte i_8_) {
		if (i_8_ >= 21) {
			int foregroundWidth = this.progressBarForeground.getRenderWidth();
			int animOffset = ((AnimatedProgressBarLSEConfig) this.config).animationSpeed * AwtLoadingScreen.getFrameCount(true) / 10 % foregroundWidth;
			this.progressBarForeground.drawRepeat(animOffset + x + -foregroundWidth, y, foregroundWidth + width + -animOffset, height);
		}
	}
}

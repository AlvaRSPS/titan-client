/* Class93_Sub2 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.Class195;
import com.Class42_Sub1;
import com.Class69_Sub2;
import com.Class98_Sub1;
import com.Class98_Sub10_Sub34;
import com.Class98_Sub46_Sub10;
import com.Class98_Sub46_Sub6;
import com.RSByteBuffer;
import com.RSToolkit;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public final class SpriteProgressBarLSEConfig extends ProgressBarLSEConfig {
	public static int anInt5491;

	public static final void loadFonts(RSToolkit toolkit, byte i) {
		Class69_Sub2.p11Full = Class98_Sub1.method945(HitmarksDefinitionParser.p11FullIndex, toolkit, (byte) 117, true);
		if (i <= -47) {
			StrongReferenceMCNode.p11FullMetrics = Class98_Sub46_Sub6.loadFontMetrics(HitmarksDefinitionParser.p11FullIndex, 18361, toolkit);
			Class195.p12Full = Class98_Sub1.method945(HitmarksDefinitionParser.p12FullIndex, toolkit, (byte) 119, true);
			Class98_Sub46_Sub10.p12FullMetrics = Class98_Sub46_Sub6.loadFontMetrics(HitmarksDefinitionParser.p12FullIndex, 18361, toolkit);
			Class98_Sub10_Sub34.p13Full = Class98_Sub1.method945(HitmarksDefinitionParser.b12FullIndex, toolkit, (byte) 88, true);
			Class42_Sub1.p13FullMetrics = Class98_Sub46_Sub6.loadFontMetrics(HitmarksDefinitionParser.b12FullIndex, 18361, toolkit);
		}
	}

	public static final boolean method911(char c, int i) {
		return !(c != 160 && (c ^ 0xffffffff) != -33 && c != 95 && (c ^ 0xffffffff) != -46);
	}

	public static final SpriteProgressBarLSEConfig unpack(int i, RSByteBuffer packet) {
		ProgressBarLSEConfig config = ProgressBarLSEConfig.unpack(-1, packet);
		int unused = packet.readInt(-2);
		int barColour = packet.readInt(-2);
		int fillId = packet.readShort((byte) 127);
		return new SpriteProgressBarLSEConfig(config.horizontalAlignment, config.verticalAlignment, config.xOffset, config.yOffset, config.width, config.height, config.textYOffset, config.fontId, config.textColour, unused, barColour, fillId);
	}

	public int	barFillId;

	public int	barOutlineColour;

	public SpriteProgressBarLSEConfig(HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, int xOffset, int yOffset, int width, int height, int textYOffset, int fontId, int textColourId, int unused, int barColour, int fillId) {
		super(horizontalAlignment, verticalAlignment, xOffset, yOffset, width, height, textYOffset, fontId, textColourId);
		barFillId = fillId;
		barOutlineColour = barColour;
	}

	@Override
	public final LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.SPRITE_PROGRESS_BAR;
	}
}

/* Class93 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.RSByteBuffer;
import com.Sprite;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public class ProgressBarLSEConfig implements LoadingScreenElementConfiguration {
	public static int[][]	anIntArrayArray3510	= { { 0, 1, 2, 3 }, { 1, -1, -1, 0 }, { -1, 2, -1, 0 }, { -1, 0, -1, 2 }, { 0, 1, -1, 2 }, { 1, 2, -1, 0 }, { -1, 4, -1, 1 }, { -1, 3, 4, -1 }, { -1, 0, 2, -1 }, { -1, -1, 2, 0 }, { 0, 2, 5, 3 }, { 0, -1, 6, -1 }, { 0, 1, 2, 3 } };
	public static Sprite[]	mapDots;

	public static final boolean method901(int i, int i_15_, int i_16_) {
		return (0x800 & i_15_ ^ 0xffffffff) != -1;
	}

	public static ProgressBarLSEConfig unpack(int i, RSByteBuffer packet) {
		HorizontalAlignment horizontal = HorizontalAlignment.getValues(122)[packet.readUnsignedByte((byte) -101)];
		VerticalAlignment vertical = VerticalAlignment.getValues(256)[packet.readUnsignedByte((byte) -98)];
		int xOffset = packet.readUShort(false);
		int yOffset = packet.readUShort(false);
		int width = packet.readShort((byte) 127);
		int height = packet.readShort((byte) 127);
		int textYOffset = packet.readUShort(false);
		int fontId = packet.readInt(-2);
		int textColour = packet.readInt(i ^ 0x1);
		return new ProgressBarLSEConfig(horizontal, vertical, xOffset, yOffset, width, height, textYOffset, fontId, textColour);
	}

	public int					fontId;
	public int					height;
	public HorizontalAlignment	horizontalAlignment;
	public int					textColour;
	public int					textYOffset;
	public VerticalAlignment	verticalAlignment;
	public int					width;
	public int					xOffset;

	public int					yOffset;

	protected ProgressBarLSEConfig(HorizontalAlignment horizontal, VerticalAlignment vertical, int xOffset, int yOffset, int width, int height, int textYOffset, int fontId, int textColour) {
		this.yOffset = yOffset;
		this.xOffset = xOffset;
		this.height = height;
		this.fontId = fontId;
		this.textYOffset = textYOffset;
		horizontalAlignment = horizontal;
		this.width = width;
		this.textColour = textColour;
		verticalAlignment = vertical;
	}

	@Override
	public LoadingScreenElementType getType(int i) {
		return null;
	}
}

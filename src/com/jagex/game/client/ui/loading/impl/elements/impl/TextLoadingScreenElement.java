
/* Class19 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import java.io.File;

import com.Image;
import com.client;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class TextLoadingScreenElement implements LoadingScreenElement {
	public static int		lobbyInterfaceId;
	public static int[]		anIntArray3451;
	public static int[][]	anIntArrayArray3443	= new int[6][];
	public static short[]	aShortArray3447		= { 20, 22, 44, 47, 51, 3, 45, 19 };
	public static String	aString3442;
	public static String	osName;

	static {
		String string = "Unknown";
		try {
			string = System.getProperty("java.vendor").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		string.toLowerCase();
		string = "Unknown";
		try {
			string = System.getProperty("java.version").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		string.toLowerCase();
		string = "Unknown";
		try {
			string = System.getProperty("os.name").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		osName = string.toLowerCase();
		string = "Unknown";
		try {
			string = System.getProperty("os.arch").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		aString3442 = string.toLowerCase();
		string = "Unknown";
		try {
			string = System.getProperty("os.version").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		string.toLowerCase();
		string = "~/";
		try {
			string = System.getProperty("user.home").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		new File(string);
		anIntArray3451 = new int[1000];
	}

	private Js5				aClass207_3445;
	private Js5				aClass207_3449;

	private TextLSEConfig	aClass315_3446;

	private Font			aClass43_3444;

	public TextLoadingScreenElement(Js5 class207, Js5 class207_2_, TextLSEConfig class315) {
		try {
			aClass207_3445 = class207;
			aClass315_3446 = class315;
			aClass207_3449 = class207_2_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bh.<init>(" + (class207 != null ? "{...}" : "null") + ',' + (class207_2_ != null ? "{...}" : "null") + ',' + (class315 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void draw(boolean bool, byte i) {
		try {
			if (i > -81) {
				isCached(-24);
			}
			if (bool) {
				int i_0_ = aClass315_3446.horizontalAlignment.getPosition(client.clientWidth, aClass315_3446.width, (byte) 41) - -aClass315_3446.x;
				int i_1_ = aClass315_3446.verticalAlignment.getPosition(aClass315_3446.height, client.clientHeight, (byte) -56) - -aClass315_3446.y;
				aClass43_3444.drawString(i_0_, null, aClass315_3446.width, aClass315_3446.text, 0, aClass315_3446.shadowColour, null, aClass315_3446.xTextAlign, (byte) -67, aClass315_3446.colour, null, aClass315_3446.lineHeight, 0, aClass315_3446.yTextAlign, i_1_, aClass315_3446.height);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bh.B(" + bool + ',' + i + ')');
		}
	}

	@Override
	public final boolean isCached(int i) {
		try {
			boolean bool = true;
			if (!aClass207_3445.isFileCached(-79, aClass315_3446.fontId)) {
				bool = false;
			}
			if (i != 14017) {
				aClass43_3444 = null;
			}
			if (!aClass207_3449.isFileCached(i + -14055, aClass315_3446.fontId)) {
				bool = false;
			}
			return bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bh.A(" + i + ')');
		}
	}

	@Override
	public final void loadMedia(byte i) {
		FontSpecifications class197 = FontSpecifications.load(aClass207_3449, true, aClass315_3446.fontId);
		aClass43_3444 = client.graphicsToolkit.createFont(class197, Image.loadImages(aClass207_3445, aClass315_3446.fontId), true);
	}
}

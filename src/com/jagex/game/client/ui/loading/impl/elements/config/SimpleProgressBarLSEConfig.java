/* Class93_Sub3 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.GameShell;
import com.IncomingOpcode;
import com.RSByteBuffer;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public final class SimpleProgressBarLSEConfig extends ProgressBarLSEConfig {
	public static IncomingOpcode	aClass58_5493;
	public static String			clanChatOwnerName	= null;

	static {
		aClass58_5493 = new IncomingOpcode(58, -1);
	}

	public static void method912(int i) {
		do {
			try {
				clanChatOwnerName = null;
				aClass58_5493 = null;
				if (i == 58) {
					break;
				}
				method912(108);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "uu.C(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final long method913(int i) {
		try {
			if (i != 58) {
				aClass58_5493 = null;
			}
			return GameShell.frameTimeBase.currentTimeNanos((byte) 53);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uu.B(" + i + ')');
		}
	}

	public static final SimpleProgressBarLSEConfig unpack(RSByteBuffer packet, int i) {
		ProgressBarLSEConfig config = ProgressBarLSEConfig.unpack(-1, packet);
		int fillColour = packet.readInt(-2);
		int borderColour = packet.readInt(-2);
		return new SimpleProgressBarLSEConfig(config.horizontalAlignment, config.verticalAlignment, config.xOffset, config.yOffset, config.width, config.height, config.textYOffset, config.fontId, config.textColour, fillColour, borderColour);
	}

	public int	barBorderColour;

	public int	barFillColour;

	SimpleProgressBarLSEConfig(HorizontalAlignment horizontal, VerticalAlignment vertical, int xOffset, int yOffset, int width, int height, int textYOffset, int fontId, int textColour, int fillColour, int borderColour) {
		super(horizontal, vertical, xOffset, yOffset, width, height, textYOffset, fontId, textColour);
		barBorderColour = borderColour;
		barFillColour = fillColour;

	}

	@Override
	public final LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.SIMPLE_PROGRESS_BAR;
	}
}

/* Class163 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.ActionGroup;
import com.ActionQueueEntry;
import com.Class15;
import com.Class189;
import com.Class246_Sub3_Sub2_Sub1;
import com.Class246_Sub3_Sub4_Sub4;
import com.Class248;
import com.Class249;
import com.Class308;
import com.Class320;
import com.Class33;
import com.Class351;
import com.Class359;
import com.Class38;
import com.Class98_Sub43_Sub4;
import com.DataFs;
import com.LoadingScreenSequence;
import com.NodeObject;
import com.OpenGlArrayBufferPointer;
import com.RSByteBuffer;
import com.RSToolkit;
import com.RtInterfaceAttachment;
import com.SceneGraphNode;
import com.TextResources;
import com.client;
import com.jagex.game.clanchat.ClanChatMember;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.definition.parser.StructsDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class BackgroundColourLSEConfig implements LoadingScreenElementConfiguration {
	public static NodeObject[]	aClass98_Sub39Array3516	= new NodeObject[1024];
	public static int			anInt3517;
	public static int			anInt3518;

	public static final void method2519(int i, RSToolkit toolkit) {
		int i_0_ = 0;
		int i_1_ = 0;
		if (OpenGLHeap.aBoolean6079) {
			i_0_ = Class189.method2642((byte) 42);
			i_1_ = MapScenesDefinitionParser.method3765(false);
		}
		int i_2_ = Class38.anInt355 + i_0_;
		int i_3_ = i_1_ + OpenGlArrayBufferPointer.anInt897;
		int i_4_ = Class246_Sub3_Sub4_Sub4.width;
		int i_5_ = Class15.anInt172 - 3;
		int i_6_ = 20;
		DataFs.drawOptionWindow(i_6_, toolkit, Class246_Sub3_Sub4_Sub4.width, Class15.anInt172, (byte) 58, Class38.anInt355 - -i_0_, OpenGlArrayBufferPointer.anInt897 + i_1_, TextResources.CHOOSE_OPTION.getText(client.gameLanguage, (byte) 25));
		int i_8_ = i_0_ + client.mouseListener.getMouseX(105);
		int i_9_ = client.mouseListener.getMouseY((byte) 60) - -i_1_;
		if (!Class248.aBoolean1896) {
			int i_10_ = 0;
			for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getFirst(32); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getNext(94)) {
				int i_11_ = (-i_10_ + -1 + Class359.actionCount) * 16 + 13 + i_3_ - -i_6_;
				if ((Class38.anInt355 - -i_0_ ^ 0xffffffff) > (i_8_ ^ 0xffffffff) && (i_8_ ^ 0xffffffff) > (i_0_ + Class38.anInt355 - -Class246_Sub3_Sub4_Sub4.width ^ 0xffffffff) && i_11_ - 13 < i_9_ && (4 + i_11_ ^ 0xffffffff) < (i_9_ ^ 0xffffffff) && class98_sub46_sub8.aBoolean5984) {
					toolkit.fillRectangle(i_0_ + Class38.anInt355, i_11_ - 12, Class246_Sub3_Sub4_Sub4.width, 16, AnimatedProgressBarLSEConfig.anInt6289 | 255 - StructsDefinitionParser.anInt1971 << 1099846840, 1);
				}
				i_10_++;
			}
		} else {
			int i_12_ = 0;
			for (ActionGroup class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getFirst(-1); class98_sub46_sub9 != null; class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getNext(0)) {
				int i_13_ = i_3_ + i_6_ + 13 - -(16 * i_12_);
				i_12_++;
				if (i_8_ > i_0_ + Class38.anInt355 && (i_0_ + Class38.anInt355 - -Class246_Sub3_Sub4_Sub4.width ^ 0xffffffff) < (i_8_ ^ 0xffffffff) && (i_9_ ^ 0xffffffff) < (-13 + i_13_ ^ 0xffffffff) && (i_9_ ^ 0xffffffff) > (i_13_ + 4 ^ 0xffffffff) && (class98_sub46_sub9.actionCount > 1
						|| ((ActionQueueEntry) class98_sub46_sub9.actions.head.nextCacheable).aBoolean5984)) {
					toolkit.fillRectangle(Class38.anInt355 + i_0_, -12 + i_13_, Class246_Sub3_Sub4_Sub4.width, 16, -StructsDefinitionParser.anInt1971 + 255 << 2128249016 | AnimatedProgressBarLSEConfig.anInt6289, 1);
				}
			}
			if (Class308.aClass98_Sub46_Sub9_2583 != null) {
				DataFs.drawOptionWindow(i_6_, toolkit, ScalingSpriteLoadingScreenElement.anInt3439, Class98_Sub43_Sub4.anInt5938, (byte) 58, LoadingScreenSequence.anInt2128, anInt3518, Class308.aClass98_Sub46_Sub9_2583.actionText);
				i_12_ = 0;
				for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class308.aClass98_Sub46_Sub9_2583.actions.getFirst(-1); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class308.aClass98_Sub46_Sub9_2583.actions.getNext(0)) {
					int i_14_ = i_12_ * 16 + 13 + anInt3518 - -i_6_;
					if ((i_8_ ^ 0xffffffff) < (LoadingScreenSequence.anInt2128 ^ 0xffffffff) && (i_8_ ^ 0xffffffff) > (LoadingScreenSequence.anInt2128 + ScalingSpriteLoadingScreenElement.anInt3439 ^ 0xffffffff) && i_14_ + -13 < i_9_ && (i_9_ ^ 0xffffffff) > (4 + i_14_ ^ 0xffffffff)
							&& class98_sub46_sub8.aBoolean5984) {
						toolkit.fillRectangle(LoadingScreenSequence.anInt2128, i_14_ + -12, ScalingSpriteLoadingScreenElement.anInt3439, 16, AnimatedProgressBarLSEConfig.anInt6289 | 255 - StructsDefinitionParser.anInt1971 << 2118045208, 1);
					}
					i_12_++;
				}
				Class249.method3164(ScalingSpriteLoadingScreenElement.anInt3439, i_6_, toolkit, anInt3518, LoadingScreenSequence.anInt2128, -22275, Class98_Sub43_Sub4.anInt5938);
			}
		}
		Class249.method3164(Class246_Sub3_Sub4_Sub4.width, i_6_, toolkit, OpenGlArrayBufferPointer.anInt897 - -i_1_, Class38.anInt355 - -i_0_, -22275, Class15.anInt172);
		if (!Class248.aBoolean1896) {
			int i_15_ = 0;
			for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getFirst(32); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getNext(122)) {
				int i_16_ = 16 * (-1 + Class359.actionCount + -i_15_) + i_6_ + i_3_ - -13;
				Class246_Sub3_Sub2_Sub1.method3009(i_5_, class98_sub46_sub8, SceneGraphNode.anInt1871 | ~0xffffff, i_4_, i_16_, i_3_, i_8_, toolkit, i_9_, 0, ClanChatMember.anInt1194 | ~0xffffff, i_2_);
				i_15_++;
			}
		} else {
			int i_17_ = 0;
			for (ActionGroup class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getFirst(-1); class98_sub46_sub9 != null; class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getNext(0)) {
				int i_18_ = OpenGlArrayBufferPointer.anInt897 + i_1_ - -i_6_ + 13 - -(i_17_ * 16);
				if ((class98_sub46_sub9.actionCount ^ 0xffffffff) != -2) {
					Class320.method3663(class98_sub46_sub9, ~0xffffff | ClanChatMember.anInt1194, i_8_, Class15.anInt172, i_9_, Class38.anInt355 - -i_0_, toolkit, i_18_, ~0xffffff | SceneGraphNode.anInt1871, 0, Class246_Sub3_Sub4_Sub4.width, i_1_ + OpenGlArrayBufferPointer.anInt897);
				} else {
					Class246_Sub3_Sub2_Sub1.method3009(Class15.anInt172, (ActionQueueEntry) class98_sub46_sub9.actions.head.nextCacheable, ~0xffffff | SceneGraphNode.anInt1871, Class246_Sub3_Sub4_Sub4.width, i_18_, OpenGlArrayBufferPointer.anInt897 + i_1_, i_8_, toolkit, i_9_, 0,
							ClanChatMember.anInt1194 | ~0xffffff, i_0_ + Class38.anInt355);
				}
				i_17_++;
			}
			if (Class308.aClass98_Sub46_Sub9_2583 != null) {
				i_17_ = 0;
				for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class308.aClass98_Sub46_Sub9_2583.actions.getFirst(-1); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class308.aClass98_Sub46_Sub9_2583.actions.getNext(0)) {
					int i_19_ = i_17_ * 16 + anInt3518 - (-i_6_ + -13);
					i_17_++;
					Class246_Sub3_Sub2_Sub1.method3009(Class98_Sub43_Sub4.anInt5938, class98_sub46_sub8, SceneGraphNode.anInt1871 | ~0xffffff, ScalingSpriteLoadingScreenElement.anInt3439, i_19_, anInt3518, i_8_, toolkit, i_9_, 0, ~0xffffff | ClanChatMember.anInt1194,
							LoadingScreenSequence.anInt2128);
				}
				Class351.method3849(Class98_Sub43_Sub4.anInt5938, -8, LoadingScreenSequence.anInt2128, ScalingSpriteLoadingScreenElement.anInt3439, anInt3518);
			}
		}
		Class351.method3849(Class15.anInt172, -8, Class38.anInt355 + i_0_, Class246_Sub3_Sub4_Sub4.width, OpenGlArrayBufferPointer.anInt897 - -i_1_);
	}

	public static final int method2520(int i, byte i_20_) {
		if (i_20_ < 34) {
			anInt3517 = 72;
		}
		return i & 0x7f;
	}

	public static final BackgroundColourLSEConfig unpack(RSByteBuffer packet, int i) {
		int colour = packet.readInt(-2);
		return new BackgroundColourLSEConfig(colour);
	}

	public int colour;

	BackgroundColourLSEConfig(int colour) {
		this.colour = colour;
	}

	@Override
	public final LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.BACKGROUND_COLOUR;
	}
}

/* Class373_Sub2 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.Class246_Sub5;
import com.client;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.SkyboxDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.config.SimpleProgressBarLSEConfig;

public final class SimpleProgressBarLoadingScreenElement extends ProgressBarLoadingScreenElement {
	public static Class246_Sub5[]			aClass246_Sub5Array5469;
	public static float						aFloat5472;
	public static int						anInt5471	= 1406;
	public static int						anInt5473	= 0;
	public static SkyboxDefinitionParser	skyboxDefinitionList;

	public static final boolean method3974(int i, int i_2_, int i_3_) {
		return (0x21 & i_2_ ^ 0xffffffff) != -1;
	}

	public SimpleProgressBarLoadingScreenElement(Js5 class207, Js5 class207_9_, SimpleProgressBarLSEConfig class93_sub3) {
		super(class207, class207_9_, class93_sub3);
	}

	@Override
	public final void drawBarFill(int i, int i_6_, int i_7_, boolean bool) {
		int progress = getProgressPer10k(100) * this.config.width / 10000;
		client.graphicsToolkit.fillRectangle(i_7_, 2 + i_6_, progress, -2 + this.config.height, ((SimpleProgressBarLSEConfig) this.config).barFillColour, 0);
		client.graphicsToolkit.fillRectangle(progress + i_7_, i_6_ + i, -progress + this.config.width, this.config.height - 2, 0, 0);
	}

	@Override
	public final void drawBarOutline(int i, int i_0_, byte i_1_, boolean bool) {
		client.graphicsToolkit.drawRectangle(-2 + i_0_, i, this.config.width - -4, this.config.height - -2, ((SimpleProgressBarLSEConfig) this.config).barBorderColour, 0);
		client.graphicsToolkit.drawRectangle(i_0_ + -1, 1 + i, 2 + this.config.width, this.config.height, 0, 0);
	}
}

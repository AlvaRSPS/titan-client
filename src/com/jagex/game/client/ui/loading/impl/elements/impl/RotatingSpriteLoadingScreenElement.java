/* Class134_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.AnimationSkeletonSet;
import com.Class116;
import com.Class140;
import com.Class150;
import com.Class151;
import com.Class151_Sub9;
import com.Class191;
import com.Class204;
import com.Class256;
import com.Class256_Sub1;
import com.Class265;
import com.Class268;
import com.Class284;
import com.Class303;
import com.Class308;
import com.Class372;
import com.WhirlpoolGenerator;
import com.Class39;
import com.Class39_Sub1;
import com.Class65;
import com.Class76_Sub5;
import com.Class76_Sub9;
import com.Class98_Sub10_Sub11;
import com.Class98_Sub10_Sub12;
import com.Class98_Sub10_Sub20;
import com.Class98_Sub10_Sub21;
import com.Class98_Sub10_Sub24;
import com.Class98_Sub10_Sub34;
import com.Class98_Sub10_Sub9;
import com.Class98_Sub23;
import com.Class98_Sub4;
import com.Class98_Sub41;
import com.Class98_Sub43;
import com.Class98_Sub48;
import com.ClientScript2Runtime;
import com.DummyOutputStream;
import com.Exception_Sub1;
import com.GZipDecompressor;
import com.GameShell;
import com.GrandExchangeOffer;
import com.NodeString;
import com.OpenGlArrayBufferPointer;
import com.OutgoingPacket;
import com.ProceduralTextureSource;
import com.RsFloatBuffer;
import com.RtInterfaceAttachment;
import com.SceneGraphNode;
import com.SceneGraphNodeList;
import com.Sound;
import com.StartupStage;
import com.TextResources;
import com.aa_Sub3;
import com.client;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.QuestDefinition;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.NPCDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.client.preferences.CpuUsagePreferenceField;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.preferences.ParticlesPreferenceField;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class RotatingSpriteLoadingScreenElement extends SpriteLoadingScreenElement {
	public static int anInt5459 = 0;

	public static final byte[] method2244(int i, float f, float f_0_, float f_1_, int i_2_, int i_3_, int i_4_, Class39 class39, float f_5_, float f_6_, int i_7_) {
		byte[] is = new byte[i_2_ * i_4_ * i_7_];
		Class98_Sub48.method1661(0, i_2_, class39, (byte) -19, f, f_1_, i_4_, f_5_, i_7_, i_3_, f_0_, is, f_6_);
		if (i != -31633) {
			AdvancedMemoryCache.loadNative(null, (byte) 101);
		}
		return is;
	}

	public static final boolean method2245(int i, char c) {
		return !((c ^ 0xffffffff) > -49 || c > 57);
	}

	public static final void method2247(int i) {
		client.miscellaneousJs5.discardUnpacked = 1;
		BConfigDefinition.anInt3111 = 0;
		client.anInt3548 = 0;
		VarClientDefinitionParser.anInt1046 = 0;
		Class76_Sub5.anInt3746 = 0;
		AnimationSkeletonSet.anInt6044 = 0;
		Class65.anInt498 = 0;
		ClientScript2Runtime.anInt1880 = 0;
		Class98_Sub23.anInt4001 = 0;
		Class98_Sub43.anInt4242 = 0;
		Class39_Sub1.anInt3594 = 0;
		NPCDefinitionParser.method3543(16535);
		GameShell.focus = true;
		GrandExchangeOffer.aBoolean856 = true;
		FloorUnderlayDefinitionParser.method2487(-1);
		for (int i_11_ = 0; (i_11_ ^ 0xffffffff) > (OpenGlArrayBufferPointer.aClass36Array903.length ^ 0xffffffff); i_11_++) {
			OpenGlArrayBufferPointer.aClass36Array903[i_11_] = null;
		}
		Class98_Sub10_Sub9.aBoolean5585 = false;
		QuestDefinition.method2820((byte) 125);
		Class98_Sub10_Sub9.anInt5581 = -40 + (int) (80.0 * Math.random());
		Class151.anInt1213 = (int) (Math.random() * 30.0) + -20;
		RsFloatBuffer.aFloat5794 = 0x3fff & (int) (Math.random() * 160.0) + -80;
		Class204.anInt1553 = (int) (120.0 * Math.random()) - 60;
		RtMouseEvent.anInt3943 = (int) (Math.random() * 110.0) - 55;
		GraphicsDefinitionParser.anInt2533 = (int) (Math.random() * 100.0) + -50;
		Class256.method3193(-1);
		for (int index = 0; index < 2048; index++) {
			Class151_Sub9.players[index] = null;
		}
		Class98_Sub10_Sub20.anInt5640 = 0;
		Class150.npcCount = 0;
		ProceduralTextureSource.npc.clear(-90);
		QuickChatMessageParser.projectiles.clear((byte) 47);
		Class98_Sub10_Sub11.animatedObjects.clear((byte) 47);
		TexturesPreferenceField.aClass218_3694.clear(20);
		ModelRenderer.groundItems.clear(-63);
		Class191.aClass148_1478 = new LinkedList();
		OutgoingPacket.aClass148_3866 = new LinkedList();
		StartupStage.varValues.method2288((byte) -88);
		Class268.method3248(0);
		Class98_Sub4.anInt3828 = 0;
		Class303.anInt2530 = 0;
		Class98_Sub10_Sub21.anInt5643 = 0;
		NewsFetcher.anInt3098 = 0;
		GZipDecompressor.anInt1967 = 0;
		NodeString.anInt3915 = 0;
		Class308.anInt2580 = 0;
		Class98_Sub41.anInt4202 = 0;
		Exception_Sub1.anInt44 = 0;
		StrongReferenceMCNode.anInt6300 = 0;
		for (int i_13_ = 0; i_13_ < Class76_Sub5.intGlobalConfigs.length; i_13_++) {
			if (!Class140.boolGlobalConfigs[i_13_]) {
				Class76_Sub5.intGlobalConfigs[i_13_] = -1;
			}
		}
		if (client.topLevelInterfaceId != -1) {
			RtInterfaceAttachment.uncacheInterface(false, client.topLevelInterfaceId);
		}
		for (RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(113); class98_sub18 != null; class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.iterateNext(-1)) {
			if (!class98_sub18.isLinked((byte) 78)) {
				class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(116);
				if (class98_sub18 == null) {
					break;
				}
			}
			RtInterfaceAttachment.detachInterface(16398, false, class98_sub18, true);
		}
		client.topLevelInterfaceId = -1;
		Class116.attachmentMap = new HashTable(8);
		Class76_Sub9.method768(123);
		DummyOutputStream.rtInterface = null;
		for (int option = 0; option < 8; option++) {
			LightIntensityDefinitionParser.playerOptions[option] = null;
			OpenGlModelRenderer.playerOptionReducedPriority[option] = false;
			Class151_Sub9.playerOptionCursors[option] = -1;
		}
		Class98_Sub10_Sub34.method1106((byte) -61);
		Class98_Sub10_Sub12.aBoolean5599 = true;
		for (int i_15_ = 0; (i_15_ ^ 0xffffffff) > -101; i_15_++) {
			aa_Sub3.isDirty[i_15_] = true;
		}
		for (int offer = 0; (offer ^ 0xffffffff) > -7; offer++) {
			Class98_Sub10_Sub24.offers[offer] = new GrandExchangeOffer();
		}
		for (int skillId = 0; (skillId ^ 0xffffffff) > -26; skillId++) {
			CpuUsagePreferenceField.levels[skillId] = 0;
			Class256_Sub1.skillLevels[skillId] = 0;
			NewsLSEConfig.skillsExperience[skillId] = 0;
		}
		WhirlpoolGenerator.method3980((byte) 125);
		SceneGraphNode.clientPalette = Class372.clientPalette = Class265.clientPalette = Sound.clientPalette = new short[256];
		ParticlesPreferenceField.aBoolean3656 = true;
		SceneGraphNodeList.optionText = TextResources.WALK_HERE.getText(client.gameLanguage, (byte) 25);
		client.preferences.setPreference((byte) -13, client.preferences.removeRoofs.getValue((byte) 120), client.preferences.removeRoofsMode);
		client.preferences.setPreference((byte) -13, client.preferences.aClass64_Sub3_4041.getValue((byte) 121), client.preferences.aClass64_Sub3_4076);
		StartupStage.anInt581 = 0;
		OrthoZoomPreferenceField.method622((byte) -38);
		OutgoingPacket.method1158(-2);
		client.miscellaneousJs5.discardUnpacked = 2;
		CursorDefinitionParser.aLong121 = 0L;
		Class284.aClass98_Sub4_2167 = null;
	}

	private int anInt5458 = 0;

	public RotatingSpriteLoadingScreenElement(Js5 class207, RotatingSpriteLSEConfig class337_sub1) {
		super(class207, class337_sub1);
	}

	@Override
	public final void draw(boolean bool, byte i) {
		try {
			int i_8_ = this.aClass337_3460.horizontalAlignment.getPosition(client.clientWidth, this.aClass332_3459.getRenderWidth(), (byte) -106) + this.aClass337_3460.xOffset;
			int i_9_ = this.aClass337_3460.verticalAlignment.getPosition(this.aClass332_3459.getRenderHeight(), client.clientHeight, (byte) -56) + this.aClass337_3460.yOffset;
			if (i >= -81) {
				anInt5459 = 121;
			}
			this.aClass332_3459.method3730(i_8_ + this.aClass332_3459.getRenderWidth() / 2, this.aClass332_3459.getRenderHeight() / 2 + i_9_, 4096, anInt5458);
			anInt5458 += ((RotatingSpriteLSEConfig) this.aClass337_3460).rotationStep;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mw.B(" + bool + ',' + i + ')');
		}
	}
}

/* Class134 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.Class237_Sub1;
import com.OpenGLHeightMapToNormalMapConverter;
import com.OutgoingOpcode;
import com.Sprite;
import com.client;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.VarPlayerDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteLSEConfig;

public class SpriteLoadingScreenElement implements LoadingScreenElement {
	public static OutgoingOpcode			aClass171_3466	= new OutgoingOpcode(11, 15);
	public static float						aFloat3463;
	public static int						yCamPosTile;
	public static int						anInt3464		= 0;
	public static boolean					resizeable		= true;
	public static VarPlayerDefinitionParser	varPlayerDefinitionList;

	public static final int method2243(boolean bool) {
		if (bool != true) {
			return -16;
		}
		return OpenGLHeightMapToNormalMapConverter.gameTipsIndexCrc;
	}

	private Js5		aClass207_3462;

	Sprite			aClass332_3459;

	SpriteLSEConfig	aClass337_3460;

	public SpriteLoadingScreenElement(Js5 class207, SpriteLSEConfig class337) {
		aClass337_3460 = class337;
		aClass207_3462 = class207;
	}

	@Override
	public void draw(boolean bool, byte i) {
		do {
			try {
				if (i < -81) {
					if (!bool) {
						break;
					}
					int i_0_ = aClass337_3460.horizontalAlignment.getPosition(client.clientWidth, aClass332_3459.getRenderWidth(), (byte) 83) + aClass337_3460.xOffset;
					int i_1_ = aClass337_3460.verticalAlignment.getPosition(aClass332_3459.getRenderHeight(), client.clientHeight, (byte) -56) + aClass337_3460.yOffset;
					aClass332_3459.draw(i_0_, i_1_);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "jca.B(" + bool + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final boolean isCached(int i) {
		try {
			if (i != 14017) {
				loadMedia((byte) 85);
			}
			return aClass207_3462.isFileCached(i ^ ~0x36fd, aClass337_3460.spriteId);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jca.A(" + i + ')');
		}
	}

	@Override
	public final void loadMedia(byte i) {
		try {
			if (i != -43) {
				aClass207_3462 = null;
			}
			aClass332_3459 = Class237_Sub1.loadSprite(aClass337_3460.spriteId, aClass207_3462, (byte) -89);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jca.C(" + i + ')');
		}
	}
}


/* Class93_Sub1_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import java.awt.Insets;

import com.Class112;
import com.Class191;
import com.Class230;
import com.Class259;
import com.Class268;
import com.Class277;
import com.Class281;
import com.Class310;
import com.Class40;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub38;
import com.GameShell;
import com.OutgoingPacket;
import com.RSByteBuffer;
import com.RtInterface;
import com.aa_Sub1;
import com.aa_Sub3;
import com.client;
import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.definition.parser.BConfigDefinitionParser;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class AnimatedProgressBarLSEConfig extends DecoratedProgressBarLSEConfig {
	public static int	anInt6289;
	public static int	anInt6291;

	public static final void method908(int i, int i_13_, boolean bool, int i_14_, int i_15_) {
		if (Cacheable.anInt4261 == 1) {
			int i_16_ = i_14_ / FontSpecifications.anInt1513;
			int i_17_ = i_15_ / FontSpecifications.anInt1513;
			int i_18_ = i_13_ / Class98_Sub10_Sub38.anInt5761;
			int i_19_ = i / Class98_Sub10_Sub38.anInt5761;
			if (bool != false) {
				anInt6291 = 95;
			}
			if (i_16_ < Class191.anInt1477 && (i_17_ ^ 0xffffffff) <= -1 && (i_18_ ^ 0xffffffff) > (HorizontalAlignment.anInt493 ^ 0xffffffff) && (i_19_ ^ 0xffffffff) <= -1) {
				if ((i_16_ ^ 0xffffffff) > -1) {
					i_16_ = 0;
				}
				if (i_18_ < 0) {
					i_18_ = 0;
				}
				if ((HorizontalAlignment.anInt493 ^ 0xffffffff) >= (i_19_ ^ 0xffffffff)) {
					i_19_ = -1 + HorizontalAlignment.anInt493;
				}
				if ((i_17_ ^ 0xffffffff) <= (Class191.anInt1477 ^ 0xffffffff)) {
					i_17_ = Class191.anInt1477 + -1;
				}
				for (int i_20_ = i_18_; i_20_ <= i_19_; i_20_++) {
					int i_21_ = BConfigDefinitionParser.method2678((byte) 6, i_20_ + Class268.anInt2007, HorizontalAlignment.anInt493) * Class191.anInt1477;
					for (int i_22_ = i_16_; i_22_ <= i_17_; i_22_++) {
						int i_23_ = i_21_ + BConfigDefinitionParser.method2678((byte) 6, i_22_ - -Class76_Sub8.anInt3780, Class191.anInt1477);
						OpenGlModelRenderer.anIntArray4873[i_23_] = Class230.anInt1732;
					}
				}
			}
		}
	}

	public static final void method909(int i, int i_24_, boolean bool, int i_25_, int i_26_, int i_27_) {
		if (GameShell.fullScreenFrame != null && (i != 3 || (VerticalAlignment.anInt946 ^ 0xffffffff) != (i_26_ ^ 0xffffffff) || (i_27_ ^ 0xffffffff) != (Class112.anInt949 ^ 0xffffffff))) {
			Class281.method3331(false, GameShell.fullScreenFrame, GameShell.signLink);
			GameShell.fullScreenFrame = null;
		}
		if ((i ^ 0xffffffff) == -4 && GameShell.fullScreenFrame == null) {
			GameShell.fullScreenFrame = Class259.method503(0, (byte) 105, i_26_, i_27_, 0, GameShell.signLink);
			if (GameShell.fullScreenFrame != null) {
				VerticalAlignment.anInt946 = i_26_;
				Class112.anInt949 = i_27_;
				Class310.method3618(-5964);
			}
		}
		if ((i ^ 0xffffffff) == -4 && GameShell.fullScreenFrame == null) {
			method909(client.preferences.screenSize.getValue((byte) 120), i_24_, true, i_25_, -1, -1);
		} else {
			java.awt.Container container;
			if (GameShell.fullScreenFrame == null) {
				if (GameShell.frame == null) {
					if (GameShell.applet != null) {
						container = GameShell.applet;
					} else {
						container = GameShell.activeGameShell;
					}
					GameShell.width = container.getSize().width;
					GameShell.height = container.getSize().height;
				} else {
					Insets insets = GameShell.frame.getInsets();
					GameShell.width = GameShell.frame.getSize().width + -insets.left + -insets.right;
					GameShell.height = GameShell.frame.getSize().height - (insets.bottom + insets.top);
					container = GameShell.frame;
				}
			} else {
				GameShell.width = i_26_;
				container = GameShell.fullScreenFrame;
				GameShell.height = i_27_;
			}
			if ((i ^ 0xffffffff) != -2) {
				RtMouseListener.updateMargins(false);
			} else {
				GameShell.topMargin = 0;
				GameShell.frameWidth = client.clientWidth;
				GameShell.frameHeight = client.clientHeight;
				GameShell.leftMargin = (GameShell.width + -client.clientWidth) / 2;
			}
			if (BuildLocation.LIVE != client.buildLocation) {
				if (GameShell.frameWidth < 1024 && GameShell.frameHeight < 768) {
					/* empty */
				}
			}
			if (!bool) {
				GameShell.canvas.setSize(GameShell.frameWidth, GameShell.frameHeight);
				if (OpenGLHeap.aBoolean6079) {
					RtInterface.method3471(GameShell.canvas, 0);
				} else {
					client.graphicsToolkit.resize(GameShell.canvas, GameShell.frameWidth, GameShell.frameHeight);
				}
				if (GameShell.frame != container) {
					GameShell.canvas.setLocation(GameShell.leftMargin, GameShell.topMargin);
				} else {
					Insets insets = GameShell.frame.getInsets();
					GameShell.canvas.setLocation(insets.left + GameShell.leftMargin, insets.top + GameShell.topMargin);
				}
			} else {
				Class277.method3292((byte) 107);
			}
			SpriteLoadingScreenElement.resizeable = i >= 2;
			if ((client.topLevelInterfaceId ^ 0xffffffff) != 0) {
				Class40.calculateLayout(i_24_ ^ 0x7448, true);
			}
			if (aa_Sub1.aClass123_3561 != null && OpenGLHeap.method1683(-11297, client.clientState)) {
				OutgoingPacket.method1158(-2);
			}
			if (i_24_ != -29758) {
				anInt6289 = 13;
			}
			for (int i_28_ = 0; i_28_ < 100; i_28_++) {
				aa_Sub3.isDirty[i_28_] = true;
			}
			GameShell.fullRedraw = true;
		}
	}

	public static final AnimatedProgressBarLSEConfig unpack(RSByteBuffer packet) {
		DecoratedProgressBarLSEConfig config = DecoratedProgressBarLSEConfig.unpack((byte) 94, packet);
		int animationSpeed = packet.readUShort(false);
		return new AnimatedProgressBarLSEConfig(config.horizontalAlignment, config.verticalAlignment, config.xOffset, config.yOffset, config.width,
				config.height, config.textYOffset, config.fontId, config.textColour, config.barForegroundId, config.barBackgroundId, config.outlineLeftId, config.outlineRightId, config.outlineTopId,
				config.outlineBottomId, animationSpeed);
	}

	public int animationSpeed;

	AnimatedProgressBarLSEConfig(HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, int xOffset, int yOffset, int width, int height, int textYOffset, int fontId, int textColour, int barForegroundId, int barBackgroundId, int outlineLeftId, int outlineRightId,
			int outlineTopId, int outlineBottomId, int animationSpeed) {
		super(horizontalAlignment, verticalAlignment, xOffset, yOffset, width, height, textYOffset, fontId, textColour, barForegroundId, barBackgroundId, outlineLeftId, outlineRightId, outlineTopId, outlineBottomId);
		this.animationSpeed = animationSpeed;
	}

	@Override
	public final LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.ANIMATED_PROGRESS_BAR;
	}
}

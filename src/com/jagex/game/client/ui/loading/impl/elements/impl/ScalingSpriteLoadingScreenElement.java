/* Class5 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.Class1;
import com.Class128;
import com.Class237_Sub1;
import com.Class249;
import com.Class28;
import com.Class288;
import com.Class323;
import com.GameShell;
import com.Sprite;
import com.client;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.config.ScalingSpriteLSEConfig;

public final class ScalingSpriteLoadingScreenElement implements LoadingScreenElement {
	public static int		anInt3439;
	public static String	aString3440;

	public static final boolean method176(int i, int i_0_, int i_1_) {
		try {
			if (i != 24578) {
				method176(-22, 97, 98);
			}
			return (i_0_ & 0x800 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "af.F(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method177(int i) {
		try {
			if (i != 7681) {
				method176(-63, 99, -33);
			}
			aString3440 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "af.E(" + i + ')');
		}
	}

	public static final int method178(Class128 class128, int i) {
		try {
			if (i != 0) {
				method178(null, 3);
			}
			if (class128 == Class288.aClass128_3381) {
				return 7681;
			}
			if (Class249.aClass128_1903 != class128) {
				if (Class323.aClass128_2715 != class128) {
					if (class128 == Class1.aClass128_64) {
						return 260;
					}
					if (Class28.aClass128_286 == class128) {
						return 34023;
					}
				} else {
					return 34165;
				}
			} else {
				return 8448;
			}
			throw new IllegalArgumentException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "af.D(" + (class128 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private Js5						aClass207_3437;

	private Sprite					aClass332_3438;

	private ScalingSpriteLSEConfig	aClass367_3441;

	public ScalingSpriteLoadingScreenElement(Js5 class207, ScalingSpriteLSEConfig class367) {
		try {
			aClass367_3441 = class367;
			aClass207_3437 = class207;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "af.<init>(" + (class207 != null ? "{...}" : "null") + ',' + (class367 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void draw(boolean bool, byte i) {
		try {
			if (bool) {
				int i_2_ = (GameShell.frameWidth ^ 0xffffffff) < (client.clientWidth ^ 0xffffffff) ? GameShell.frameWidth : client.clientWidth;
				int i_3_ = client.clientHeight >= GameShell.frameHeight ? client.clientHeight : GameShell.frameHeight;
				int i_4_ = aClass332_3438.getRenderWidth();
				int i_5_ = aClass332_3438.getRenderHeight();
				int i_6_ = 0;
				int i_7_ = i_2_;
				int i_8_ = i_2_ * i_5_ / i_4_;
				int i_9_ = (i_3_ - i_8_) / 2;
				if ((i_3_ ^ 0xffffffff) > (i_8_ ^ 0xffffffff)) {
					i_7_ = i_4_ * i_3_ / i_5_;
					i_9_ = 0;
					i_8_ = i_3_;
					i_6_ = (-i_7_ + i_2_) / 2;
				}
				aClass332_3438.method3726(i_6_, i_9_, i_7_, i_8_);
			}
			if (i >= -81) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "af.B(" + bool + ',' + i + ')');
		}
	}

	@Override
	public final boolean isCached(int i) {
		try {
			if (i != 14017) {
				aClass207_3437 = null;
			}
			return aClass207_3437.isFileCached(-77, aClass367_3441.spriteId);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "af.A(" + i + ')');
		}
	}

	@Override
	public final void loadMedia(byte i) {
		try {
			if (i == -43) {
				aClass332_3438 = Class237_Sub1.loadSprite(aClass367_3441.spriteId, aClass207_3437, (byte) -89);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "af.C(" + i + ')');
		}
	}
}

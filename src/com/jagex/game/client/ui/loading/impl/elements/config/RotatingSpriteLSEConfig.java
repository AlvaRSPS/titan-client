/* Class337_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.Class116;
import com.RSByteBuffer;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public final class RotatingSpriteLSEConfig extends SpriteLSEConfig {
	public static int	anInt5497		= 2;
	public static int	anInt5499		= 1;
	public static int[]	anIntArray5500	= new int[6];

	public static final void method3777(int i) {
		BConfigDefinition.aClass98_Sub31_Sub2_3122.method1364(96);
		RenderAnimDefinitionParser.anInt1948 = 1;
		LightIntensityDefinitionParser.aClass207_2025 = null;
		Class116.aClass98_Sub31_Sub2_965 = null;
	}

	public static final RotatingSpriteLSEConfig unpack(RSByteBuffer packet, int i) {
		SpriteLSEConfig config = SpriteLSEConfig.unpack(9342, packet);
		int rotationStep = packet.method1227((byte) -1);
		return new RotatingSpriteLSEConfig(config.spriteId, config.horizontalAlignment, config.verticalAlignment, config.xOffset, config.yOffset, rotationStep);
	}

	public int rotationStep;

	RotatingSpriteLSEConfig(int spriteId, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, int xOffset, int yOffset, int rotationStep) {
		super(spriteId, horizontalAlignment, verticalAlignment, xOffset, yOffset);
		this.rotationStep = rotationStep;
	}

	@Override
	public final LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.ROTATING_SPRITE;
	}
}

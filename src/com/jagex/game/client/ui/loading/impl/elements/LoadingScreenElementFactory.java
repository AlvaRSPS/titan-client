/* Class362 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements; /*
															*/

import com.Class151;
import com.Class154;
import com.Class200;
import com.Class268;
import com.Class287_Sub2;
import com.Class42_Sub3;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub38;
import com.NodeShort;
import com.Class98_Sub5_Sub3;
import com.RSToolkit;
import com.SocketWrapper;
import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.LoadingScreenElementConfiguration;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.ScalingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SimpleProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.AnimatedLoadingBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.DecoratedProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.NewsLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.RotatingSpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.TextLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class LoadingScreenElementFactory {
	public static int anInt3090 = 4;

	public static final void method3926(int i, int i_2_, int i_3_, int i_4_, RSToolkit var_ha, int i_5_, int i_6_, int i_7_) {
		do {
			Class154.aHa1231 = var_ha;
			Class98_Sub5_Sub3.aClass111_5540 = Class154.aHa1231.createMatrix();
			Class42_Sub3.aClass111_5364 = Class154.aHa1231.createMatrix();
			Class200.aClass111_1543 = Class154.aHa1231.createMatrix();
			Class151.anInt1214 = i_6_;
			SocketWrapper.anInterface17_301 = null;
			Class268.anInt2007 = 0;
			Cacheable.anInt4261 = 1;
			NodeShort.anInt4194 = i_2_;
			Class98_Sub10_Sub38.anInt5761 = i_5_;
			FontSpecifications.anInt1513 = i_4_;
			Class76_Sub8.anInt3780 = 0;
			Class287_Sub2.method3391(i_7_, i_3_, 2);
			if (i <= -112) {
				break;
			}
			AnimatedProgressBarLSEConfig.unpack(null);
			break;
		} while (false);
	}

	private Js5			fontmetricsJs5;

	private Js5			graphicsJs5;

	private NewsFetcher	newsFetcher;

	public LoadingScreenElementFactory(Js5 class207, Js5 class207_1_) {
		graphicsJs5 = class207;
		fontmetricsJs5 = class207_1_;
	}

	public final LoadingScreenElement createElement(byte i, LoadingScreenElementConfiguration elementConfiguration) {
		if (elementConfiguration == null) {
			return null;
		}
		LoadingScreenElementType elementType = elementConfiguration.getType(30778);
		if (LoadingScreenElementType.BACKGROUND_COLOUR == elementType) {
			return new RectangleLoadingScreenElement((BackgroundColourLSEConfig) elementConfiguration);
		}
		if (elementType == LoadingScreenElementType.NEWS) {
			return new NewsLoadingScreenElement(getNewsFetcher(false), (NewsLSEConfig) elementConfiguration);
		}
		if (elementType == LoadingScreenElementType.SPRITE) {
			return new SpriteLoadingScreenElement(graphicsJs5, (SpriteLSEConfig) elementConfiguration);
		}
		if (LoadingScreenElementType.ROTATING_SPRITE == elementType) {
			return new RotatingSpriteLoadingScreenElement(graphicsJs5, (RotatingSpriteLSEConfig) elementConfiguration);
		}
		if (elementType == LoadingScreenElementType.SIMPLE_PROGRESS_BAR) {
			return new SimpleProgressBarLoadingScreenElement(graphicsJs5, fontmetricsJs5, (SimpleProgressBarLSEConfig) elementConfiguration);
		}
		if (LoadingScreenElementType.SPRITE_PROGRESS_BAR == elementType) {
			return new SpriteProgressBarLoadingScreenElement(graphicsJs5, fontmetricsJs5, (SpriteProgressBarLSEConfig) elementConfiguration);
		}
		if (elementType == LoadingScreenElementType.DECORATED_PROGRESS_BAR) {
			return new DecoratedProgressBarLoadingScreenElement(graphicsJs5, fontmetricsJs5, (DecoratedProgressBarLSEConfig) elementConfiguration);
		}
		if (LoadingScreenElementType.TEXT == elementType) {
			return new TextLoadingScreenElement(graphicsJs5, fontmetricsJs5, (TextLSEConfig) elementConfiguration);
		}
		if (LoadingScreenElementType.SCALING_SPRITE == elementType) {
			return new ScalingSpriteLoadingScreenElement(graphicsJs5, (ScalingSpriteLSEConfig) elementConfiguration);
		}
		if (elementType == LoadingScreenElementType.ANIMATED_PROGRESS_BAR) {
			return new AnimatedLoadingBarLoadingScreenElement(graphicsJs5, fontmetricsJs5, (AnimatedProgressBarLSEConfig) elementConfiguration);
		}
		return null;
	}

	private final NewsFetcher getNewsFetcher(boolean bool) {
		if (newsFetcher == null) {
			newsFetcher = new NewsFetcher();
		}
		return newsFetcher;
	}
}

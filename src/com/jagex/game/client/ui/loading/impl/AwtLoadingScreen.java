
/* Class292 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl; /*
													*/

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.lang.reflect.Field;

import com.Class140;
import com.GameShell;
import com.OutgoingOpcode;
import com.client;
import com.jagex.game.client.ui.loading.LoadingScreen;

public final class AwtLoadingScreen implements LoadingScreen {
	public static OutgoingOpcode	aClass171_3339	= new OutgoingOpcode(75, 0);
	public static long				aLong3356		= -1L;
	public static int[]				anIntArray3355;

	public static Image				backBuffer;

	public static Color[]			BAR_COLOURS		= { new Color(9179409), new Color(3289650), new Color(3289650), new Color(3289650) };

	public static Color[]			BORDER_COLOURS	= new Color[] { new Color(9179409), new Color(16777215), new Color(16726277), new Color(16726277) };

	public static Font				font;

	public static String			loadingBarTitle	= null;
	public static Color[]			TEXT_COLOURS	= { new Color(16777215), new Color(16777215), new Color(16741381), new Color(16741381) };

	static {
		anIntArray3355 = new int[14];
		GameShell.maxHeap = 64;
	}

	public static final void cleanUp() {
		backBuffer = null;
		font = null;
	}

	public static final void drawPlainLoadingScreen(int percentage, Color borderColour, Color barColour, Color textColour, String string, int i_25_) {
		try {
			Graphics graphics = GameShell.canvas.getGraphics();
			if (font == null) {
				font = new Font("Helvetica", 1, 13);
			}
			if (barColour == null) {
				barColour = new Color(140, 17, 17);
			}
			if (borderColour == null) {
				borderColour = new Color(140, 17, 17);
			}
			if (textColour == null) {
				textColour = new Color(255, 255, 255);
			}
			try {
				if (backBuffer == null) {
					backBuffer = GameShell.canvas.createImage(GameShell.frameWidth, GameShell.frameHeight);
				}
				Graphics bufferGraphics = backBuffer.getGraphics();
				bufferGraphics.setColor(Color.black);
				bufferGraphics.fillRect(0, 0, GameShell.frameWidth, GameShell.frameHeight);
				int barX = GameShell.frameWidth / 2 + -152;
				int barY = GameShell.frameHeight / 2 - 18;
				bufferGraphics.setColor(borderColour);
				if (i_25_ == 90) {
					bufferGraphics.drawRect(barX, barY, 303, 33);
					bufferGraphics.setColor(barColour);
					bufferGraphics.fillRect(barX + 2, barY + 2, 3 * percentage, 30);
					bufferGraphics.setColor(Color.BLACK);
					bufferGraphics.drawRect(1 + barX, 1 + barY, 301, 31);
					bufferGraphics.fillRect(2 + barX - -(percentage * 3), barY + 2, -(percentage * 3) + 300, 30);
					bufferGraphics.setFont(font);
					bufferGraphics.setColor(textColour);
					bufferGraphics.drawString(string, barX - -((-(string.length() * 6) + 304) / 2), barY - -22);
					if (loadingBarTitle != null) {
						bufferGraphics.setFont(font);
						bufferGraphics.setColor(textColour);
						bufferGraphics.drawString(loadingBarTitle, GameShell.frameWidth / 2 - loadingBarTitle.length() * 6 / 2, GameShell.frameHeight / 2 - 26);
					}
					graphics.drawImage(backBuffer, 0, 0, null);
				}
			} catch (Exception exception) {
				graphics.setColor(Color.black);
				graphics.fillRect(0, 0, GameShell.frameWidth, GameShell.frameHeight);
				int barX = GameShell.frameWidth / 2 + -152;
				int barY = GameShell.frameHeight / 2 - 18;
				graphics.setColor(borderColour);
				graphics.drawRect(barX, barY, 303, 33);
				graphics.setColor(barColour);
				graphics.fillRect(barX + 2, 2 + barY, 3 * percentage, 30);
				graphics.setColor(Color.black);
				graphics.drawRect(barX - -1, barY + 1, 301, 31);
				graphics.fillRect(3 * percentage + barX + 2, 2 + barY, 300 + -(percentage * 3), 30);
				graphics.setFont(font);
				graphics.setColor(textColour);
				if (loadingBarTitle != null) {
					graphics.setFont(font);
					graphics.setColor(textColour);
					graphics.drawString(loadingBarTitle, GameShell.frameWidth / 2 + -(loadingBarTitle.length() * 6 / 2), GameShell.frameHeight / 2 + -26);
				}
				graphics.drawString(string, barX - -((304 + -(string.length() * 6)) / 2), barY - -22);
			}
		} catch (Exception exception) {
			GameShell.canvas.repaint();
		}
	}

	public static final int getFrameCount(boolean bool) {
		return Class140.loadingScreenRenderer.getFrameCount((byte) 3);
	}

	public static final int method3451(int i, int i_3_, boolean bool, int i_4_, int i_5_, int i_6_, int i_7_) {
		i_5_ &= 0x3;
		if ((i_3_ & 0x1 ^ 0xffffffff) == -2) {
			int i_8_ = i_6_;
			i_6_ = i_7_;
			i_7_ = i_8_;
		}
		if (bool != false) {
			GameShell.maxHeap = -70;
		}
		if ((i_5_ ^ 0xffffffff) == -1) {
			return i;
		}
		if (i_5_ == 1) {
			return i_4_;
		}
		if ((i_5_ ^ 0xffffffff) == -3) {
			return -i + 7 - i_6_ - -1;
		}
		return -i_7_ + 1 + -i_4_ + 7;
	}

	private Image		background;
	private Image		bar;
	private Font		barFont;
	private FontMetrics	barFontMetrics;
	private Image		bodyFill;
	private Image		bodyLeft;
	private Image		bodyRight;
	private Image		bottom;
	private int			boxWidth;
	private int			boxXOffset;
	private int			boxYOffset;
	private Color		colourText;

	private int			height;

	private Image		left;

	private Image		loadingBarBuffer;

	private boolean		noDecoratedLoadingBar;
	private int			offsetPerTenCycles;
	private Image		right;
	private int			textYOffset;
	private Image		top;
	private int			width;
	private boolean		xMiddle;

	private int			xOffset;

	private boolean		yMiddle;

	private int			yOffset;

	public AwtLoadingScreen() {
		/* empty */
	}

	@Override
	public final void drawLoadingScreen(int i, boolean bool) {
		if (!noDecoratedLoadingBar) {
			if (GameShell.applet != null) {
				if (barFont == null) {
					try {
						loadLoadingScreenFromLoader(i ^ 0x7dd0);
					} catch (Exception exception) {
						noDecoratedLoadingBar = true;
					}
				}
			} else {
				noDecoratedLoadingBar = true;
			}
		}
		if (noDecoratedLoadingBar) {
			this.drawPlainLoadingScreen(2);
		} else {
			Graphics graphics = GameShell.canvas.getGraphics();
			if (graphics != null) {
				try {
					int percentage = Class140.loadingScreenRenderer.getPercentage(-127);
					String text = Class140.loadingScreenRenderer.getText((byte) -46);
					if (backBuffer == null) {
						backBuffer = GameShell.canvas.createImage(GameShell.frameWidth, GameShell.frameHeight);
					}
					Graphics bufferGraphics = backBuffer.getGraphics();
					bufferGraphics.clearRect(0, 0, GameShell.frameWidth, GameShell.frameHeight);
					int bodyLeftWidth = bodyLeft.getWidth(null);
					int bodyRightWidth = bodyRight.getWidth(null);
					int bodyFillWidth = bodyFill.getWidth(null);
					int bodyLeftHeight = bodyLeft.getHeight(null);
					int bodyRightHeight = bodyRight.getHeight(null);
					int bodyFillHeight = bodyFill.getHeight(null);
					bufferGraphics.drawImage(bodyLeft, getXPosition(bodyLeftWidth, 7) - (-boxXOffset - -(boxWidth / 2)), getYPosition(bodyLeftHeight, 0) + boxYOffset, null);
					int bodyFillX = bodyLeftWidth + boxXOffset + -(boxWidth / 2);
					int bodyWidth = boxXOffset + boxWidth / 2;
					for (int bodyFillPartX = bodyFillX; (bodyWidth ^ 0xffffffff) <= (bodyFillPartX ^ 0xffffffff); bodyFillPartX += bodyFillWidth) {
						bufferGraphics.drawImage(bodyFill, getXPosition(bodyLeftWidth, 7) + boxXOffset - -bodyFillPartX, getYPosition(bodyFillHeight, 0) + boxYOffset, null);
					}
					bufferGraphics.drawImage(bodyRight, getXPosition(bodyRightWidth, 7) - (-boxXOffset + -(boxWidth / 2)), getYPosition(bodyRightHeight, 0) + boxYOffset, null);
					int leftWidth = left.getWidth(null);
					int leftHeight = left.getHeight(null);
					int rightWidth = right.getWidth(null);
					int rightHeight = right.getHeight(null);
					int bottomHeight = bottom.getHeight(null);
					int topWidth = top.getWidth(null);
					int topHeight = top.getHeight(null);
					int bottomWidth = bottom.getWidth(null);
					int barWidth = bar.getWidth(null);
					int backgroundWidth = background.getWidth(null);
					int containerX = getXPosition(width, i ^ 0x7dd5) + xOffset;
					int containerY = getYPosition(height, 0) + yOffset;
					bufferGraphics.drawImage(left, containerX, containerY - -((height - leftHeight) / 2), null);
					bufferGraphics.drawImage(right, -rightWidth + containerX + width, (-rightHeight + height) / 2 + containerY, null);
					if (loadingBarBuffer == null) {
						loadingBarBuffer = GameShell.canvas.createImage(-rightWidth + width - leftWidth, height);
					}
					Graphics lbBufferedGraphics = loadingBarBuffer.getGraphics();
					for (int partX = 0; partX < -rightWidth + -leftWidth + width; partX += topWidth) {
						lbBufferedGraphics.drawImage(top, partX, 0, null);
					}
					for (int _partX = 0; (_partX ^ 0xffffffff) > (-rightWidth + -leftWidth + width ^ 0xffffffff); _partX += bottomWidth) {
						lbBufferedGraphics.drawImage(bottom, _partX, height + -bottomHeight, null);
					}
					int progressBarWidth = percentage * (-rightWidth + -leftWidth + width) / 100;
					if (progressBarWidth > 0) {
						Image image = GameShell.canvas.createImage(progressBarWidth, -bottomHeight + -topHeight + height);
						int rpWidth = image.getWidth(null);
						Graphics pbGraphics = image.getGraphics();
						int i_41_ = offsetPerTenCycles * getFrameCount(true) / 10 % barWidth;
						for (int i_42_ = i_41_ + -barWidth; (i_42_ ^ 0xffffffff) > (rpWidth ^ 0xffffffff); i_42_ += barWidth) {
							pbGraphics.drawImage(bar, i_42_, 0, null);
						}
						lbBufferedGraphics.drawImage(image, 0, topHeight, null);
					}
					int backgroundX = progressBarWidth;
					progressBarWidth = -progressBarWidth + -rightWidth + width - leftWidth;
					if ((progressBarWidth ^ 0xffffffff) < -1) {
						Image image = GameShell.canvas.createImage(progressBarWidth, -bottomHeight + -topHeight + height);
						int rpWidth = image.getWidth(null);
						Graphics graphics_45_ = image.getGraphics();
						for (int i_46_ = 0; i_46_ < rpWidth; i_46_ += backgroundWidth) {
							graphics_45_.drawImage(background, i_46_, 0, null);
						}
						lbBufferedGraphics.drawImage(image, backgroundX, topHeight, null);
					}
					bufferGraphics.drawImage(loadingBarBuffer, leftWidth + containerX, containerY, null);
					bufferGraphics.setFont(barFont);
					bufferGraphics.setColor(colourText);
					bufferGraphics.drawString(text, (width - barFontMetrics.stringWidth(text)) / 2 + containerX, textYOffset + containerY + height / 2 - -4);
					graphics.drawImage(backBuffer, 0, 0, null);
				} catch (Exception exception) {
					noDecoratedLoadingBar = true;
				}
			} else {
				GameShell.canvas.repaint();
			}
		}
	}

	private final void drawPlainLoadingScreen(int i) {
		drawPlainLoadingScreen(Class140.loadingScreenRenderer.getPercentage(-119), BORDER_COLOURS[client.colourId], BAR_COLOURS[client.colourId], TEXT_COLOURS[client.colourId], Class140.loadingScreenRenderer.getText((byte) -46), 90);
	}

	@Override
	public final int getCacheProgress(int i) {
		return 100;
	}

	@Override
	public final int getFadeDuration(int i) {
		return 0;
	}

	private final int getFieldNameInteger(String fieldName, int i, Object instance, Class type) throws IllegalAccessException, NoSuchFieldException {
		Field field = type.getDeclaredField(fieldName);
		return field.getInt(instance);
	}

	public final boolean getFieldValueBoolean(Class type_class, byte i, String fieldName, Object instance) throws IllegalAccessException, NoSuchFieldException {
		Field field = type_class.getDeclaredField(fieldName);
		return field.getBoolean(instance);
	}

	private final Object getFieldValueObject(Class type, String fieldName, byte i) throws IllegalAccessException, NoSuchFieldException {
		Field field = type.getDeclaredField(fieldName);
		Object instance = field.get(GameShell.applet);
		field.set(GameShell.applet, null);
		return instance;
	}

	private final int getXPosition(int i, int i_10_) {
		if (xMiddle) {
			return (GameShell.frameWidth + -i) / 2;
		}
		return 0;
	}

	private final int getYPosition(int i, int i_2_) {
		if (yMiddle) {
			return (-i + GameShell.frameHeight) / 2;
		}
		return 0;
	}

	@Override
	public final boolean isActive(int i, long l) {
		return true;
	}

	private final void loadLoadingScreenFromLoader(int i) throws IllegalAccessException, NoSuchFieldException {
		Class appletClass = GameShell.applet.getClass();
		bar = (Image) getFieldValueObject(appletClass, "bar", (byte) 60);
		background = (Image) getFieldValueObject(appletClass, "background", (byte) 121);
		left = (Image) getFieldValueObject(appletClass, "left", (byte) -107);
		right = (Image) getFieldValueObject(appletClass, "right", (byte) 86);
		top = (Image) getFieldValueObject(appletClass, "top", (byte) 79);
		bottom = (Image) getFieldValueObject(appletClass, "bottom", (byte) -116);
		bodyLeft = (Image) getFieldValueObject(appletClass, "bodyLeft", (byte) 33);
		bodyRight = (Image) getFieldValueObject(appletClass, "bodyRight", (byte) -113);
		bodyFill = (Image) getFieldValueObject(appletClass, "bodyFill", (byte) -116);
		barFont = (Font) getFieldValueObject(appletClass, "bf", (byte) -125);
		barFontMetrics = (FontMetrics) getFieldValueObject(appletClass, "bfm", (byte) 49);
		if (i == 2) {
			colourText = (Color) getFieldValueObject(appletClass, "colourtext", (byte) -125);
			Object object = getFieldValueObject(appletClass, "lb", (byte) -116);
			Class objectClass = object.getClass();
			xMiddle = getFieldValueBoolean(objectClass, (byte) 103, "xMiddle", object);
			yMiddle = getFieldValueBoolean(objectClass, (byte) 97, "yMiddle", object);
			xOffset = getFieldNameInteger("xOffset", 2235, object, objectClass);
			yOffset = getFieldNameInteger("yOffset", 2235, object, objectClass);
			width = getFieldNameInteger("width", 2235, object, objectClass);
			height = getFieldNameInteger("height", 2235, object, objectClass);
			boxXOffset = getFieldNameInteger("boxXOffset", i + 2233, object, objectClass);
			boxYOffset = getFieldNameInteger("boxYOffset", 2235, object, objectClass);
			boxWidth = getFieldNameInteger("boxWidth", 2235, object, objectClass);
			textYOffset = getFieldNameInteger("textYOffset", i ^ 0x8b9, object, objectClass);
			offsetPerTenCycles = getFieldNameInteger("offsetPerTenCycles", i ^ 0x8b9, object, objectClass);
		}
	}

	@Override
	public final void loadMedia(int i) {

	}

	@Override
	public final void shutDown(int i) {
		cleanUp();
	}
}

/* Class93_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.Class151;
import com.Class151_Sub7;
import com.Class191;
import com.Class208;
import com.Class48_Sub1_Sub2;
import com.Class98_Sub10_Sub38;
import com.NodeShort;
import com.IncomingOpcode;
import com.TextureMetrics;
import com.RSByteBuffer;
import com.aa_Sub1;
import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.HashTable;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.matrix.NativeMatrix;

public class DecoratedProgressBarLSEConfig extends ProgressBarLSEConfig {
	public static boolean			aBoolean5479				= true;
	public static int				anInt5477					= 0;
	public static int				anInt5486					= 0;
	public static int				anInt5488					= 2;
	public static int				anInt5489;
	public static int[][]			anIntArrayArray5487			= { { 6, 6 }, { 6, 6 }, { 6, 5, 5 }, { 5, 6, 5 }, { 5, 5, 6 }, { 6, 5, 5 }, { 5, 0, 4, 1 }, { 7, 7, 1, 2 }, { 7, 1, 2, 7 }, { 8, 9, 4, 0, 8, 9 }, { 0, 8, 9, 8, 9, 4 }, { 11, 0, 10, 11, 4, 2 }, { 6, 6 }, { 7, 7, 1, 2 }, { 7, 7, 1, 2 } };
	public static IncomingOpcode	SEND_PUBLIC_CHAT_MESSAGE	= new IncomingOpcode(62, -1);

	public static final void method904(byte i) {
		try {
			if (Class208.HSL_TABLE == null) {
				Class208.HSL_TABLE = new int[65536];
				double d = 0.7 + (-0.015 + Math.random() * 0.03);
				for (int i_24_ = 0; (i_24_ ^ 0xffffffff) > -65537; i_24_++) {
					double d_25_ = (0x3f & i_24_ >> -239388310) / 64.0 + 0.0078125;
					double d_26_ = 0.0625 + ((0x3a8 & i_24_) >> 770038727) / 8.0;
					double d_27_ = (0x7f & i_24_) / 128.0;
					double d_28_ = d_27_;
					double d_29_ = d_27_;
					double d_30_ = d_27_;
					if (d_26_ != 0.0) {
						double d_31_;
						if (!(d_27_ < 0.5)) {
							d_31_ = -(d_27_ * d_26_) + (d_26_ + d_27_);
						} else {
							d_31_ = d_27_ * (d_26_ + 1.0);
						}
						double d_32_ = 2.0 * d_27_ - d_31_;
						double d_33_ = 0.3333333333333333 + d_25_;
						if (d_33_ > 1.0) {
							d_33_--;
						}
						double d_34_ = d_25_;
						double d_35_ = -0.3333333333333333 + d_25_;
						if (d_35_ < 0.0) {
							d_35_++;
						}
						if (!(d_34_ * 6.0 < 1.0)) {
							if (2.0 * d_34_ < 1.0) {
								d_29_ = d_31_;
							} else if (!(3.0 * d_34_ < 2.0)) {
								d_29_ = d_32_;
							} else {
								d_29_ = d_32_ + 6.0 * ((d_31_ - d_32_) * (0.6666666666666666 - d_34_));
							}
						} else {
							d_29_ = d_32_ + 6.0 * (d_31_ - d_32_) * d_34_;
						}
						if (!(6.0 * d_33_ < 1.0)) {
							if (!(2.0 * d_33_ < 1.0)) {
								if (3.0 * d_33_ < 2.0) {
									d_28_ = 6.0 * ((-d_32_ + d_31_) * (-d_33_ + 0.6666666666666666)) + d_32_;
								} else {
									d_28_ = d_32_;
								}
							} else {
								d_28_ = d_31_;
							}
						} else {
							d_28_ = d_33_ * (6.0 * (-d_32_ + d_31_)) + d_32_;
						}
						if (!(d_35_ * 6.0 < 1.0)) {
							if (d_35_ * 2.0 < 1.0) {
								d_30_ = d_31_;
							} else if (3.0 * d_35_ < 2.0) {
								d_30_ = d_32_ + (-d_32_ + d_31_) * (-d_35_ + 0.6666666666666666) * 6.0;
							} else {
								d_30_ = d_32_;
							}
						} else {
							d_30_ = d_32_ + d_35_ * (6.0 * (-d_32_ + d_31_));
						}
					}
					d_28_ = Math.pow(d_28_, d);
					d_29_ = Math.pow(d_29_, d);
					d_30_ = Math.pow(d_30_, d);
					int i_36_ = (int) (256.0 * d_28_);
					int i_37_ = (int) (d_29_ * 256.0);
					int i_38_ = (int) (256.0 * d_30_);
					int i_39_ = (i_36_ << -13557424) + (i_37_ << 394664072) + i_38_;
					Class208.HSL_TABLE[i_24_] = i_39_;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ij.F(" + i + ')');
		}
	}

	public static final boolean method905(int i, int i_40_, int i_41_) {
		try {
			if (i_40_ > -35) {
				anInt5477 = -88;
			}
			return !(!((0x70000 & i ^ 0xffffffff) != -1 | IncomingOpcode.method523(i_41_, -1, i)) && !TextureMetrics.method2919(-60, i, i_41_));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ij.G(" + i + ',' + i_40_ + ',' + i_41_ + ')');
		}
	}

	public static final void method907(int i, int i_42_, int i_43_) {
		try {
			Class151_Sub7.anInt5005 = i_43_;
			ParamDefinition.anInt1208 = i_42_;
			if (Cacheable.anInt4261 == 0) {
				aa_Sub1.anInt3556 = 2 * HashTable.anInt3183 + Class151_Sub7.anInt5005;
				Class48_Sub1_Sub2.anInt5519 = NativeMatrix.anInt4701 * 2 + ParamDefinition.anInt1208;
			} else if (Cacheable.anInt4261 != 1) {
				if (Cacheable.anInt4261 == 2) {
					Class48_Sub1_Sub2.anInt5519 = ParamDefinition.anInt1208;
					aa_Sub1.anInt3556 = Class151_Sub7.anInt5005;
				}
			} else {
				Class191.anInt1477 = NodeShort.anInt4194 + Class151_Sub7.anInt5005 / FontSpecifications.anInt1513 + 2;
				HorizontalAlignment.anInt493 = ParamDefinition.anInt1208 / Class98_Sub10_Sub38.anInt5761 + 2 + Class151.anInt1214;
				aa_Sub1.anInt3556 = FontSpecifications.anInt1513 * Class191.anInt1477;
				Class48_Sub1_Sub2.anInt5519 = HorizontalAlignment.anInt493 * Class98_Sub10_Sub38.anInt5761;
				HashTable.anInt3183 = -Class151_Sub7.anInt5005 + aa_Sub1.anInt3556 >> 881154753;
				NativeMatrix.anInt4701 = -ParamDefinition.anInt1208 + Class48_Sub1_Sub2.anInt5519 >> 1217660993;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ij.E(" + i + ',' + i_42_ + ',' + i_43_ + ')');
		}
	}

	public static final DecoratedProgressBarLSEConfig unpack(byte i, RSByteBuffer packet) {
		ProgressBarLSEConfig config = ProgressBarLSEConfig.unpack(i + -95, packet);
		int barForegroundId = packet.readShort((byte) 127);
		int barBackgroundId = packet.readShort((byte) 127);
		int outlineLeftId = packet.readShort((byte) 127);
		int outlineRightId = packet.readShort((byte) 127);
		int outlineTopId = packet.readShort((byte) 127);
		int outlineBottomId = packet.readShort((byte) 127);
		return new DecoratedProgressBarLSEConfig(config.horizontalAlignment, config.verticalAlignment, config.xOffset, config.yOffset, config.width, config.height, config.textYOffset, config.fontId, config.textColour, barForegroundId, barBackgroundId, outlineLeftId, outlineRightId, outlineTopId,
				outlineBottomId);
	}

	public int	barBackgroundId;
	public int	barForegroundId;
	public int	outlineBottomId;
	public int	outlineLeftId;
	public int	outlineRightId;

	public int	outlineTopId;

	protected DecoratedProgressBarLSEConfig(HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, int xOffset, int yOffset, int width, int height, int textYOffset, int fontId, int textColour, int barForegroundId, int barBackgroundId, int outlineLeftId, int outlineRightId,
			int outlineTopId, int outlineBottomId) {
		super(horizontalAlignment, verticalAlignment, xOffset, yOffset, width, height, textYOffset, fontId, textColour);
		this.outlineTopId = outlineTopId;
		this.outlineBottomId = outlineBottomId;
		this.barBackgroundId = barBackgroundId;
		this.outlineLeftId = outlineLeftId;
		this.barForegroundId = barForegroundId;
		this.outlineRightId = outlineRightId;
	}

	@Override
	public LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.DECORATED_PROGRESS_BAR;
	}

}

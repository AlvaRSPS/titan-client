
/* Class368 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.newsfeed; /*
															*/

public final class NewsItem {
	public static int	anInt3128		= -1;
	public static int	runEnergyLevel	= 0;

	public String		firstLine;

	public String		secondLine;

	public String		title;

	NewsItem(String title, String firstLine, String secondLine) {
		this.secondLine = secondLine;
		this.title = title;
		this.firstLine = firstLine;
	}
}

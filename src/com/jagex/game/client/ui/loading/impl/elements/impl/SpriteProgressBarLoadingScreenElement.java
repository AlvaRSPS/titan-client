/* Class373_Sub3 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.Class237_Sub1;
import com.Class246_Sub3_Sub4;
import com.Class331;
import com.Class76_Sub4;
import com.Class85;
import com.Class98_Sub10_Sub29;
import com.OpenGLHeightMapToNormalMapConverter;
import com.OutgoingPacket;
import com.PlayerUpdateMask;
import com.Sprite;
import com.client;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteProgressBarLSEConfig;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class SpriteProgressBarLoadingScreenElement extends ProgressBarLoadingScreenElement {
	public static Class85		aClass85_5474	= new Class85(1, 7);
	public static long[][][]	aLongArrayArrayArray5476;

	public static final void method3976(int i, byte i_0_, String string) {
		OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, PlayerUpdateMask.aClass171_524, Class331.aClass117_2811);
		frame.packet.writeByte(1 + NativeShadow.encodedStringLength(string, (byte) 97), -38);
		frame.packet.writePJStr1(string, (byte) 113);
		frame.packet.method1231(i, (byte) -108);
		Class98_Sub10_Sub29.sendPacket(false, frame);
	}

	public static final void method3977(boolean bool) {
		do {
			OpenGLHeightMapToNormalMapConverter.method2173(false, -125);
			if ((VarClientDefinitionParser.anInt1050 ^ 0xffffffff) > -1 || (VarClientDefinitionParser.anInt1050 ^ 0xffffffff) == -1) {
				break;
			}
			Class76_Sub4.method754(VarClientDefinitionParser.anInt1050, false, -117);
			VarClientDefinitionParser.anInt1050 = -1;
			break;
		} while (false);
	}

	public static final boolean method3978(int i, int i_7_, byte i_8_) {
		return (0x220 & i_7_ ^ 0xffffffff) == -545 | (0x18 & i_7_ ^ 0xffffffff) != -1;
	}

	private Sprite aClass332_5475;

	public SpriteProgressBarLoadingScreenElement(Js5 class207, Js5 class207_3_, SpriteProgressBarLSEConfig class93_sub2) {
		super(class207, class207_3_, class93_sub2);
	}

	@Override
	public final void drawBarFill(int i, int i_4_, int i_5_, boolean bool) {
		try {
			int i_6_ = getProgressPer10k(i + 98) * this.config.width / 10000;
			int[] is = new int[4];
			client.graphicsToolkit.getClip(is);
			client.graphicsToolkit.setClip(i_5_, i_4_ + i, i_5_ + i_6_, i_4_ - -this.config.height);
			aClass332_5475.drawRepeat(i_5_, 2 + i_4_, this.config.width, this.config.height);
			client.graphicsToolkit.setClip(is[0], is[1], is[2], is[3]);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sq.F(" + i + ',' + i_4_ + ',' + i_5_ + ',' + bool + ')');
		}
	}

	@Override
	public final void drawBarOutline(int i, int i_1_, byte i_2_, boolean bool) {
		try {
			client.graphicsToolkit.drawRectangle(i_1_ + -2, i, 4 + this.config.width, this.config.height - -2, ((SpriteProgressBarLSEConfig) this.config).barOutlineColour, 0);
			if (i_2_ == -36) {
				client.graphicsToolkit.drawRectangle(-1 + i_1_, i - -1, this.config.width + 2, this.config.height, 0, 0);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sq.D(" + i + ',' + i_1_ + ',' + i_2_ + ',' + bool + ')');
		}
	}

	@Override
	public final boolean isCached(int i) {
		try {
			if (!super.isCached(i)) {
				return false;
			}
			return this.glyphStore.isFileCached(-87, ((SpriteProgressBarLSEConfig) this.config).barFillId);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sq.A(" + i + ')');
		}
	}

	@Override
	public final void loadMedia(byte i) {
		try {
			super.loadMedia(i);
			aClass332_5475 = Class237_Sub1.loadSprite(((SpriteProgressBarLSEConfig) this.config).barFillId, this.glyphStore, (byte) -89);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sq.C(" + i + ')');
		}
	}
}

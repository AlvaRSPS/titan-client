
/* Class363 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.newsfeed; /*
															*/

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.net.URL;

import com.Char;
import com.Class165;
import com.Class76_Sub5;
import com.Class98_Sub10_Sub7;
import com.GameShell;
import com.Mob;
import com.Player;
import com.SignLinkRequest;
import com.StringConcatenator;
import com.client;
import com.jagex.core.crypto.ISAACPseudoRNG;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.constants.BuildLocation;

public final class NewsFetcher implements Runnable {
	public static int	anInt3095;
	public static int	anInt3098;
	public static int	anInt3099	= 0;

	public static final void method3930(boolean bool, byte i, Player player, int i_2_, int i_3_) {
		try {
			int i_4_ = player.pathX[0];
			int i_5_ = player.pathZ[0];
			if ((i_4_ ^ 0xffffffff) <= -1 && (i_4_ ^ 0xffffffff) > (Class165.mapWidth ^ 0xffffffff) && (i_5_ ^ 0xffffffff) <= -1 && (i_5_ ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) && (i_3_ ^ 0xffffffff) <= -1 && (i_3_ ^ 0xffffffff) > (Class165.mapWidth ^ 0xffffffff) && (i_2_
					^ 0xffffffff) <= -1 && (i_2_ ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff)) {
				int i_6_ = GameShell.method96(VarPlayerDefinition.clipMaps[player.plane], player.getSize(0), Class76_Sub5.anIntArray3743, 0, 0, i_2_, 0, i_5_, ISAACPseudoRNG.anIntArray974, true, i_3_, -4, 48, i_4_, 0);
				if ((i_6_ ^ 0xffffffff) <= -2 && i_6_ <= 3) {
					if (bool != false) {
						anInt3098 = 115;
					}
					for (int i_7_ = 0; -1 + i_6_ > i_7_; i_7_++) {
						player.step(i, 1, ISAACPseudoRNG.anIntArray974[i_7_], Class76_Sub5.anIntArray3743[i_7_]);
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vv.A(" + bool + ',' + i + ',' + (player != null ? "{...}" : "null") + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	private SignLinkRequest		newsDataRequest;
	private volatile boolean	newsFetched;
	private NewsItem[]			newsItems;

	private Thread				thread;

	public NewsFetcher() {
		/* empty */
	}

	public final NewsItem getNewsItem(int i, int id) {
		if (newsItems == null || id < 0 || id >= newsItems.length) {
			return null;
		}
		return newsItems[id];
	}

	public final boolean isReady(int i) {
		if (newsFetched) {
			return true;
		}
		if (newsDataRequest == null) {
			try {
				int httpPort = client.buildLocation == BuildLocation.LIVE ? 80 : 7000 + client.gameServer.index;
				newsDataRequest = GameShell.signLink.openUrlDataInputStream(-106, new URL("http://" + client.gameServer.host + ":" + httpPort + "/news.ws?game=" + client.game.id));
			} catch (java.net.MalformedURLException malformedurlexception) {
				return true;
			}
		}
		if (newsDataRequest == null || newsDataRequest.status == 2) {
			return true;
		}
		if ((newsDataRequest.status ^ 0xffffffff) != -2) {
			return false;
		}
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
		return newsFetched;
	}

	@Override
	public final void run() {
		try {
			BufferedReader newsStreamReader = new BufferedReader(new InputStreamReader((DataInputStream) newsDataRequest.result));
			String newsLine = newsStreamReader.readLine();
			StringConcatenator buffer = StringConcatenator.create(120);
			for (/**/; newsLine != null; newsLine = newsStreamReader.readLine()) {
				buffer.addString(newsLine, 0);
			}
			String[] strings = buffer.getStrings(20780);
			if ((strings.length % 3 ^ 0xffffffff) != -1) {
				return;
			}
			newsItems = new NewsItem[strings.length / 3];
			for (int part = 0; (part ^ 0xffffffff) > (strings.length ^ 0xffffffff); part += 3) {
				newsItems[part / 3] = new NewsItem(strings[part], strings[part + 1], strings[2 + part]);
			}
		} catch (java.io.IOException ioexception) {
			/* empty */
		}
		newsFetched = true;
	}
}

/* Class367 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.RSByteBuffer;
import com.jagex.core.collections.Queue;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public final class ScalingSpriteLSEConfig implements LoadingScreenElementConfiguration {
	public static Queue		aClass215_3545	= new Queue();
	public static double	aDouble3543;
	public static int[]		anIntArray3546	= new int[14];

	public static final ScalingSpriteLSEConfig unpack(RSByteBuffer packet, int i) {
		int spriteId = packet.readShort((byte) 127);
		return new ScalingSpriteLSEConfig(spriteId);
	}

	public int spriteId;

	ScalingSpriteLSEConfig(int spriteId) {
		this.spriteId = spriteId;
	}

	@Override
	public final LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.SCALING_SPRITE;
	}
}

/* Class334 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.Class232;
import com.Class98_Sub10_Sub34;
import com.OutgoingOpcode;
import com.Sprite;
import com.client;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsItem;
import com.jagex.game.toolkit.font.Font;

public final class NewsLoadingScreenElement implements LoadingScreenElement {
	public static OutgoingOpcode	aClass171_3470	= new OutgoingOpcode(28, 3);
	public static Class232			aClass232_3468	= new Class232();
	public static Sprite			aClass332_3471;

	private NewsLSEConfig			config;

	private NewsFetcher				newsFetcher;

	public NewsLoadingScreenElement(NewsFetcher newsFetcher, NewsLSEConfig config) {
		this.newsFetcher = newsFetcher;
		this.config = config;
	}

	@Override
	public final void draw(boolean bool, byte i) {
		do {
			NewsItem class368 = newsFetcher.getNewsItem(23885, config.newsItemId);
			if (class368 == null) {
				break;
			}
			int i_0_ = config.horizontalAlignment.getPosition(client.clientWidth, config.width, (byte) 110) - -config.xOffset;
			int i_1_ = config.verticalAlignment.getPosition(config.height, client.clientHeight, (byte) -56) + config.yOffset;
			if (config.borderEnabled) {
				client.graphicsToolkit.drawRectangle(i_0_, i_1_, config.width, config.height, config.borderColour, 0);
			}
			i_1_ += drawString(5, i_1_, Class98_Sub10_Sub34.p13Full, class368.title, true, i_0_) * 12;
			i_1_ += 8;
			if (config.borderEnabled) {
				client.graphicsToolkit.method1795(i_0_, i_1_, i_0_ + config.width - 1, i_1_, config.borderColour, 0);
			}
			i_1_ = ++i_1_ + 12 * drawString(5, i_1_, Class98_Sub10_Sub34.p13Full, class368.firstLine, true, i_0_);
			i_1_ += 5;
			i_1_ += drawString(5, i_1_, Class98_Sub10_Sub34.p13Full, class368.secondLine, true, i_0_) * 12;
			break;
		} while (false);
	}

	private final int drawString(int xyOffset, int y, Font font, String string, boolean bool, int x) {
		return font.drawString(xyOffset + x, null, -(xyOffset * 2) + config.width, string, 0, config.shadowColour, null, 0, (byte) -119, config.textColour, null, 0, 0, 0, xyOffset + y, -(xyOffset * 2) + config.height);
	}

	@Override
	public final boolean isCached(int i) {
		return newsFetcher.isReady(-1);
	}

	@Override
	public final void loadMedia(byte i) {

	}
}


/* Class124 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.config; /*
															*/

import com.RSByteBuffer;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.LoadingScreenElementConfiguration;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.ScalingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SimpleProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;

public final class LoadingScreenConfiguration {

	public int									duration;
	public LoadingScreenElementConfiguration[]	elementConfigs;
	public int									fadeDuration;

	public LoadingScreenConfiguration() {
		/* empty */
	}

	public final void unpack(int i, RSByteBuffer packet) {
		duration = packet.readMediumInt(-124);
		fadeDuration = packet.readShort((byte) 127);
		elementConfigs = new LoadingScreenElementConfiguration[packet.readUnsignedByte((byte) 124)];
		LoadingScreenElementType[] loadingScreenElementType = LoadingScreenElementType.getValues(false);
		for (int elementIndex = i; (elementConfigs.length ^ 0xffffffff) < (elementIndex ^ 0xffffffff); elementIndex++) {
			elementConfigs[elementIndex] = unpackElement(loadingScreenElementType[packet.readUnsignedByte((byte) -120)], packet, -8829);
		}
	}

	private final LoadingScreenElementConfiguration unpackElement(LoadingScreenElementType type, RSByteBuffer packet, int i) {
		if (type == LoadingScreenElementType.BACKGROUND_COLOUR) {
			return BackgroundColourLSEConfig.unpack(packet, -22);
		}
		if (type == LoadingScreenElementType.SIMPLE_PROGRESS_BAR) {
			return SimpleProgressBarLSEConfig.unpack(packet, -88);
		}
		if (type == LoadingScreenElementType.NEWS) {
			return NewsLSEConfig.unpack(-2, packet);
		}
		if (LoadingScreenElementType.ROTATING_SPRITE == type) {
			return RotatingSpriteLSEConfig.unpack(packet, i ^ ~0x2219);
		}
		if (type == LoadingScreenElementType.SPRITE) {
			return SpriteLSEConfig.unpack(9342, packet);
		}
		if (type == LoadingScreenElementType.SPRITE_PROGRESS_BAR) {
			return SpriteProgressBarLSEConfig.unpack(-36, packet);
		}
		if (type == LoadingScreenElementType.DECORATED_PROGRESS_BAR) {
			return DecoratedProgressBarLSEConfig.unpack((byte) 94, packet);
		}
		if (LoadingScreenElementType.TEXT == type) {
			return TextLSEConfig.unpack(packet, true);
		}
		if (LoadingScreenElementType.SCALING_SPRITE == type) {
			return ScalingSpriteLSEConfig.unpack(packet, 93);
		}
		if (LoadingScreenElementType.ANIMATED_PROGRESS_BAR == type) {
			return AnimatedProgressBarLSEConfig.unpack(packet);
		}
		return null;
	}
}

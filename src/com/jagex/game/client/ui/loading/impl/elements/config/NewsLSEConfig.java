/* Class52 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.Class151_Sub7;
import com.Class211;
import com.Class259;
import com.Class69_Sub2;
import com.Class76_Sub11;
import com.RSByteBuffer;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public final class NewsLSEConfig implements LoadingScreenElementConfiguration {
	public static Js5						clientScriptJs5;
	public static QuickChatMessageParser	quickChatMessageList;
	public static int[]						skillsExperience	= new int[25];

	public static final int method488(boolean bool) {
		if (bool != true) {
			Class259.method490(null, (byte) -28);
		}
		synchronized (Class211.aClass79_1594) {
			return Class211.aClass79_1594.getStrongReferenceCount(-127);
		}
	}

	public static final void method489(boolean bool) {
		Class69_Sub2.aClass79_5334.freeSoftReferences((byte) -127);
		Class64_Sub5.aClass79_3650.freeSoftReferences((byte) 60);
		Class76_Sub11.aClass79_3797.freeSoftReferences((byte) -97);
		if (bool == false) {
			Class151_Sub7.aClass79_5004.freeSoftReferences((byte) 116);
		}
	}

	public static void method492(boolean bool) {
		quickChatMessageList = null;
		if (bool != true) {
			skillsExperience = null;
		}
		clientScriptJs5 = null;
		skillsExperience = null;
	}

	public static final NewsLSEConfig unpack(int i, RSByteBuffer packet) {
		int newsItemId = packet.readUnsignedByte((byte) -105);
		HorizontalAlignment horizontalAlignment = HorizontalAlignment.getValues(i + 123)[packet.readUnsignedByte((byte) 79)];
		VerticalAlignment verticalAlignment = VerticalAlignment.getValues(256)[packet.readUnsignedByte((byte) -127)];
		int xOffset = packet.readUShort(false);
		int yOffset = packet.readUShort(false);
		int width = packet.readShort((byte) 127);
		int height = packet.readShort((byte) 127);
		int textColour = packet.readInt(-2);
		int shadowColour = packet.readInt(-2);
		int borderColour = packet.readInt(-2);
		boolean borderEnabled = (packet.readUnsignedByte((byte) 96) ^ 0xffffffff) == i;
		return new NewsLSEConfig(newsItemId, horizontalAlignment, verticalAlignment, xOffset, yOffset, width, height, textColour, shadowColour, borderColour, borderEnabled);
	}

	public int					borderColour;
	public boolean				borderEnabled;
	public int					height;
	public HorizontalAlignment	horizontalAlignment;
	public int					newsItemId;
	public int					shadowColour;
	public int					textColour;
	public VerticalAlignment	verticalAlignment;
	public int					width;
	public int					xOffset;

	public int					yOffset;

	NewsLSEConfig(int newsItemId, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, int xOffset, int yOffset, int width, int height, int textColour, int shadowColour, int borderColour, boolean borderEnabled) {
		this.textColour = textColour;
		this.yOffset = yOffset;
		this.height = height;
		this.borderColour = borderColour;
		this.width = width;
		this.newsItemId = newsItemId;
		this.verticalAlignment = verticalAlignment;
		this.xOffset = xOffset;
		this.shadowColour = shadowColour;
		this.horizontalAlignment = horizontalAlignment;
		this.borderEnabled = borderEnabled;
	}

	@Override
	public final LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.NEWS;
	}
}

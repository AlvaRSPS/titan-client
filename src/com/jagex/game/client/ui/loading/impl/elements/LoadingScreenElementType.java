/* Class113 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements; /*
															*/

import com.Class169;
import com.Class242;
import com.Class246_Sub4_Sub2;
import com.Class35;
import com.Class351;
import com.Class39;
import com.Class85;
import com.Class98_Sub10_Sub16;
import com.Class98_Sub23;
import com.NodeShort;
import com.Class98_Sub9;
import com.OpenGlToolkit;
import com.OutgoingPacket;
import com.SignLinkRequest;
import com.aa_Sub1;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.definition.SkyboxDefinition;
import com.jagex.game.client.preferences.LightningDetailPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.constants.BuildType;

public final class LoadingScreenElementType {
	public static SignLinkRequest			aClass143_953;
	public static long						aLong954;
	public static LoadingScreenElementType	ANIMATED_PROGRESS_BAR	= new LoadingScreenElementType(9, 2);
	public static int						anInt950;
	public static int[]						anIntArray951			= { 0, -1, 0, 1 };
	public static LoadingScreenElementType	BACKGROUND_COLOUR		= new LoadingScreenElementType(0, 1);
	public static LoadingScreenElementType	DECORATED_PROGRESS_BAR	= new LoadingScreenElementType(3, 2);
	public static LoadingScreenElementType	NEWS					= new LoadingScreenElementType(4, 1);
	public static LoadingScreenElementType	ROTATING_SPRITE			= new LoadingScreenElementType(6, 1);
	public static LoadingScreenElementType	SCALING_SPRITE			= new LoadingScreenElementType(8, 1);
	public static LoadingScreenElementType	SIMPLE_PROGRESS_BAR		= new LoadingScreenElementType(1, 2);
	public static LoadingScreenElementType	SPRITE					= new LoadingScreenElementType(5, 1);
	public static LoadingScreenElementType	SPRITE_PROGRESS_BAR		= new LoadingScreenElementType(2, 2);
	public static LoadingScreenElementType	TEXT					= new LoadingScreenElementType(7, 2);

	static {
		anInt950 = -1;
		aLong954 = -1L;
	}

	public static final LoadingScreenElementType[] getValues(boolean bool) {
		return new LoadingScreenElementType[] { BACKGROUND_COLOUR, SIMPLE_PROGRESS_BAR, SPRITE_PROGRESS_BAR, DECORATED_PROGRESS_BAR, NEWS, SPRITE, ROTATING_SPRITE, TEXT, SCALING_SPRITE, ANIMATED_PROGRESS_BAR };
	}

	public static final Class85[] method2143(int i) {
		return new Class85[] { BuildType.SEND_GROUND_ITEM, SpriteProgressBarLoadingScreenElement.aClass85_5474, OpenGlToolkit.aClass85_4299, Class98_Sub23.aClass85_4007, SkyboxDefinition.aClass85_471, RectangleLoadingScreenElement.aClass85_3454, Class98_Sub10_Sub16.REMOVE_GROUND_ITEM,
				Class35.aClass85_332, AdvancedMemoryCache.aClass85_600, Class351.aClass85_2921, OutgoingPacket.aClass85_3868, Class246_Sub4_Sub2.aClass85_6186, LightningDetailPreferenceField.aClass85_3667, Class242.aClass85_1849, Class39.aClass85_362 };
	}

	public static final void method2144(int i, int i_0_) {
		Class169.anInt1307 = -1;
		NodeShort.anInt4197 = i_0_;
		Class169.anInt1307 = -1;
		aa_Sub1.method155(i + 4364);
	}

	public static final void method2145(byte i) {
		if (i != -46) {
			aClass143_953 = null;
		}
		Class98_Sub9.aBoolean3851 = true;
	}

	public int id;

	public LoadingScreenElementType(int i, int id) {
		this.id = id;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

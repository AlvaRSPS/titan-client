/* Class315 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.Class98_Sub10_Sub32;
import com.FriendLoginUpdate;
import com.GameShell;
import com.IncomingOpcode;
import com.NPC;
import com.OpenGLTexture2DSource;
import com.RSByteBuffer;
import com.SignLink;
import com.SignLinkRequest;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public final class TextLSEConfig implements LoadingScreenElementConfiguration {
	public static IncomingOpcode	aClass58_3533		= new IncomingOpcode(93, 6);
	public static String[]			friendsDisplayNames	= new String[200];

	public static final void method3646(int i) {
		if (i <= -31) {
			FriendLoginUpdate.aClass79_6170.freeSoftReferences((byte) -118);
		}
	}

	public static final void method3647(boolean bool, boolean bool_0_, String string, boolean bool_1_, SignLink class88) {
		if (bool_1_ == true) {
			if (bool) {
				if (SignLink.osNameLowerCase.startsWith("win") && class88.is_signed) {
					String string_2_ = null;
					if (GameShell.applet != null) {
						string_2_ = GameShell.applet.getParameter("haveie6");
					}
					if (string_2_ == null || !string_2_.equals("1")) {
						SignLinkRequest class143 = Class98_Sub10_Sub32.method1097(-18871, string, class88, 0);
						OpenGLTexture2DSource.aClass88_3104 = class88;
						LoadingScreenElementType.aClass143_953 = class143;
						NPC.aString6507 = string;
						return;
					}
				}
				if (SignLink.osNameLowerCase.startsWith("mac")) {
					String string_3_ = null;
					if (GameShell.applet != null) {
						string_3_ = GameShell.applet.getParameter("havefirefox");
					}
					if (string_3_ != null && string_3_.equals("1") && bool_0_) {
						Class98_Sub10_Sub32.method1097(-18871, string, class88, 1);
						return;
					}
				}
				Class98_Sub10_Sub32.method1097(-18871, string, class88, 2);
			} else {
				Class98_Sub10_Sub32.method1097(-18871, string, class88, 3);
			}
		}
	}

	public static final TextLSEConfig unpack(RSByteBuffer packet, boolean bool) {
		String string = packet.readString((byte) 84);
		// string = "Titan brings you a new RSPS experience.";
		HorizontalAlignment horizontalAlignment = HorizontalAlignment.getValues(122)[packet.readUnsignedByte((byte) 69)];
		VerticalAlignment verticalAlignment = VerticalAlignment.getValues(256)[packet.readUnsignedByte((byte) -120)];
		int x = packet.readUShort(false);
		int y = packet.readUShort(!bool);
		int xTextAlign = packet.readUnsignedByte((byte) 106);
		int yTextAlign = packet.readUnsignedByte((byte) 51);
		int lineHeight = packet.readUnsignedByte((byte) 15);
		int width = packet.readShort((byte) 127);
		int height = packet.readShort((byte) 127);
		int fontId = packet.readInt(-2);
		int colour = packet.readInt(-2);
		int shadowColour = packet.readInt(-2);
		// System.out.println("TextLSEConfig:" + string);
		return new TextLSEConfig(string, horizontalAlignment, verticalAlignment, x, y, xTextAlign, yTextAlign, lineHeight, width, height, fontId, colour, shadowColour);
	}

	public int					colour;
	public int					fontId;
	public int					height;
	public HorizontalAlignment	horizontalAlignment;
	public int					lineHeight;
	public int					shadowColour;
	public String				text;
	public VerticalAlignment	verticalAlignment;
	public int					width;

	public int					x;

	public int					xTextAlign;

	public int					y;

	public int					yTextAlign;

	TextLSEConfig(String string, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, int x, int y, int xTextAlign, int yTextAlign, int lineHeight, int width, int height, int fontId, int colour, int shadowColour) {
		this.lineHeight = lineHeight;
		this.xTextAlign = xTextAlign;
		this.fontId = fontId;
		this.horizontalAlignment = horizontalAlignment;
		this.colour = colour;
		this.width = width;
		this.yTextAlign = yTextAlign;
		this.x = x;
		this.height = height;
		this.shadowColour = shadowColour;
		this.y = y;
		this.verticalAlignment = verticalAlignment;
		text = string;
	}

	@Override
	public final LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.TEXT;
	}
}

/* Class337 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.RSByteBuffer;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public class SpriteLSEConfig implements LoadingScreenElementConfiguration {
	public static int	anInt3539;
	public static int[]	anIntArray3536	= { 2047, 16383, 65535 };

	public static final SpriteLSEConfig unpack(int i, RSByteBuffer packet) {
		int spriteId = packet.readShort((byte) 127);
		HorizontalAlignment horizontalAlignment = HorizontalAlignment.getValues(112)[packet.readUnsignedByte((byte) 123)];
		VerticalAlignment verticalAlignment = VerticalAlignment.getValues(256)[packet.readUnsignedByte((byte) -127)];
		int xOffset = packet.readUShort(false);
		int yOffset = packet.readUShort(false);
		return new SpriteLSEConfig(spriteId, horizontalAlignment, verticalAlignment, xOffset, yOffset);
	}

	public HorizontalAlignment	horizontalAlignment;
	public int					spriteId;
	public VerticalAlignment	verticalAlignment;
	public int					xOffset;

	public int					yOffset;

	SpriteLSEConfig(int spriteId, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, int xOffset, int yOffset) {
		this.yOffset = yOffset;
		this.spriteId = spriteId;
		this.horizontalAlignment = horizontalAlignment;
		this.verticalAlignment = verticalAlignment;
		this.xOffset = xOffset;
	}

	@Override
	public LoadingScreenElementType getType(int i) {
		return LoadingScreenElementType.SPRITE;
	}

}

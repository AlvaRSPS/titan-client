/* Class210 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl; /*
													*/

import com.RSToolkit;
import com.client;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.ui.loading.LoadingScreen;
import com.jagex.game.client.ui.loading.impl.config.LoadingScreenConfiguration;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementFactory;

public final class ConfigurableLoadingScreen implements LoadingScreen {
	public static int[] anIntArray3329 = new int[3];

	public static void method2773(int i) {
		do {
			anIntArray3329 = null;
			if (i == 3) {
				break;
			}
			method2773(-119);
			break;
		} while (false);
	}

	private LoadingScreenConfiguration	config;
	private LoadingScreenElement[]		elements;
	private boolean						needsRefresh;
	private RSToolkit					toolkit;

	public ConfigurableLoadingScreen(LoadingScreenConfiguration config, LoadingScreenElementFactory factory) {
		this.config = config;
		elements = new LoadingScreenElement[this.config.elementConfigs.length];
		for (int i = 0; (elements.length ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
			elements[i] = factory.createElement((byte) 62, this.config.elementConfigs[i]);
		}
	}

	@Override
	public final void drawLoadingScreen(int i, boolean bool) {
		bool = true;
		LoadingScreenElement[] loadingScreenElements = elements;
		int elements = 0;
		if (i != 32210) {
			toolkit = null;
		}
		for (/**/; (elements ^ 0xffffffff) > (loadingScreenElements.length ^ 0xffffffff); elements++) {
			LoadingScreenElement element = loadingScreenElements[elements];
			if (element != null) {
				element.draw(bool || needsRefresh, (byte) -124);
			}
		}
		needsRefresh = false;
	}

	@Override
	public final int getCacheProgress(int i) {
		int cachedElements = 0;
		LoadingScreenElement[] loadingScreenElements = elements;
		for (LoadingScreenElement elements : loadingScreenElements) {
			if (elements == null || elements.isCached(14017)) {
				cachedElements++;
			}
		}
		return cachedElements * 100 / elements.length;
	}

	@Override
	public final int getFadeDuration(int i) {
		return config.fadeDuration;
	}

	@Override
	public final boolean isActive(int i, long l) {
		return (TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL) <= (config.duration + l ^ 0xffffffffffffffffL);
	}

	@Override
	public final void loadMedia(int i) {
		do {
			if (client.graphicsToolkit != toolkit) {
				needsRefresh = true;
				toolkit = client.graphicsToolkit;
			}
			toolkit.clearImage(0);
			LoadingScreenElement[] loadingScreenElements = elements;
			for (int i_0_ = 0; (loadingScreenElements.length ^ 0xffffffff) < (i_0_ ^ 0xffffffff); i_0_++) {
				LoadingScreenElement elements = loadingScreenElements[i_0_];
				if (elements != null) {
					elements.loadMedia((byte) -43);
				}
			}
			shutDown(75);
			break;
		} while (false);
	}

	@Override
	public final void shutDown(int i) {
		if (i != -26363) {
			drawLoadingScreen(-118, false);
		}
	}
}

/* Interface21 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.config; /*
																	*/

import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;

public interface LoadingScreenElementConfiguration {
	LoadingScreenElementType getType(int i);
}

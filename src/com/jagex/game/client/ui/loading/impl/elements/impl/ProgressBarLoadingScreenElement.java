/* Class373 - Decompiled by JODE
 */ package com.jagex.game.client.ui.loading.impl.elements.impl; /*
																	*/

import com.Class140;
import com.Class48_Sub2_Sub1;
import com.Image;
import com.StreamHandler;
import com.client;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.config.ProgressBarLSEConfig;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;

public abstract class ProgressBarLoadingScreenElement implements LoadingScreenElement {
	public static int	anInt3477		= Class48_Sub2_Sub1.method474(1600, (byte) 31);
	public static int[]	anIntArray3479	= { 16, 32, 64, 128 };

	public static final void method3962(byte i) {
		StreamHandler.aClass79_1010.clear(125);
	}

	public ProgressBarLSEConfig	config;
	private Font				font;
	private Js5					fontmetricsStore;

	public Js5					glyphStore;

	private int					progress;

	private long				timeStamp;

	protected ProgressBarLoadingScreenElement(Js5 glyphStore, Js5 fontmetricsStore, ProgressBarLSEConfig config) {
		this.glyphStore = glyphStore;
		this.fontmetricsStore = fontmetricsStore;
		this.config = config;
	}

	@Override
	public final void draw(boolean bool, byte i) {
		int x = config.horizontalAlignment.getPosition(client.clientWidth, config.width, (byte) 124) - -config.xOffset;
		int y = config.verticalAlignment.getPosition(config.height, client.clientHeight, (byte) -56) - -config.yOffset;
		drawBarOutline(y, x, (byte) -36, bool);
		drawBarFill(2, y, x, bool);
		String string = Class140.loadingScreenRenderer.getText((byte) -46);
		if (TimeTools.getCurrentTime(-47) - timeStamp > 10000L) {
			string += " (" + Class140.loadingScreenRenderer.getStage((byte) 54).getId((byte) -10) + ")";
		}
		font.drawStringCenterAligned(config.textColour, string, x + config.width / 2, -1, (byte) -98, 4 + y - -(config.height / 2) + config.textYOffset);
	}

	public abstract void drawBarFill(int i, int i_19_, int i_20_, boolean bool);

	public abstract void drawBarOutline(int i, int i_0_, byte i_1_, boolean bool);

	public final int getProgressPer10k(int i) {
		int progress = Class140.loadingScreenRenderer.getPercentage(-125);
		int value = progress * i;
		if ((progress ^ 0xffffffff) != (this.progress ^ 0xffffffff) || (progress ^ 0xffffffff) == -1) {
			this.progress = progress;
			timeStamp = TimeTools.getCurrentTime(i ^ ~0x4a);
		} else {
			int endProgress = Class140.loadingScreenRenderer.getStageEndProgress(6119);
			if ((endProgress ^ 0xffffffff) < (progress ^ 0xffffffff)) {
				long deltaTSet = timeStamp + -Class140.loadingScreenRenderer.getPrgressTimestamp((byte) -5);
				if (deltaTSet > 0L) {
					long l_7_ = (endProgress - progress) * (10000L * deltaTSet / progress);
					long deltaT = 10000L * (-timeStamp + TimeTools.getCurrentTime(i + -147));
					if ((deltaT ^ 0xffffffffffffffffL) > (l_7_ ^ 0xffffffffffffffffL)) {
						value = (int) (100L * (endProgress + -progress) * deltaT / l_7_ - -(long) (100 * progress));
					} else {
						value = endProgress * 100;
					}
				}
			}
		}
		return value;
	}

	@Override
	public boolean isCached(int i) {
		if (i != 14017) {
			return false;
		}
		boolean cached = true;
		if (!glyphStore.isFileCached(i + -14068, config.fontId)) {
			cached = false;
		}
		if (!fontmetricsStore.isFileCached(-112, config.fontId)) {
			cached = false;
		}
		return cached;
	}

	@Override
	public void loadMedia(byte i) {
		FontSpecifications fontMetrics = FontSpecifications.load(fontmetricsStore, true, config.fontId);
		font = client.graphicsToolkit.createFont(fontMetrics, Image.loadImages(glyphStore, config.fontId), true);
	}
}

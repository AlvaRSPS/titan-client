package com.jagex.game.client.ui.loading.impl.elements.impl;

import com.AnimationSkeletonSet;
import com.Class185;
import com.Class237_Sub1;
import com.Class98_Sub46_Sub17;
import com.Sprite;
import com.client;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;

public class DecoratedProgressBarLoadingScreenElement extends ProgressBarLoadingScreenElement {
	public static AnimationSkeletonSet[] aClass98_Sub46_Sub16Array5468 = new AnimationSkeletonSet[14];

	public static final void method3969(int i, int i_318_) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_318_, -114, 17);
		class98_sub46_sub17.method1621(0);
	}

	private Sprite	outlineBottom;
	private Sprite	outlineLeftEdge;
	private Sprite	outlineRightEdge;

	private Sprite	outlineTop;

	private Sprite	progressBarBackground;

	Sprite			progressBarForeground;

	public DecoratedProgressBarLoadingScreenElement(Js5 class207, Js5 class207_9_, DecoratedProgressBarLSEConfig class93_sub1) {
		super(class207, class207_9_, class93_sub1);
	}

	@Override
	public final void drawBarFill(int i, int y, int x, boolean bool) {
		int left = outlineLeftEdge.getRenderWidth() + x;
		int right = x + this.config.width + -outlineRightEdge.getRenderWidth();
		int top = y + outlineTop.getRenderHeight();
		int bottom = y + this.config.height + -outlineBottom.getRenderHeight();
		int maxWidth = right + -left;
		int maxHeight = -top + bottom;
		int fillWidth = maxWidth * getProgressPer10k(i ^ 0x66) / 10000;
		int[] clipBuffer = new int[4];
		client.graphicsToolkit.getClip(clipBuffer);
		client.graphicsToolkit.setClip(left, top, fillWidth + left, bottom);
		drawBarForeground(maxHeight, maxWidth, top, left, (byte) 88);
		client.graphicsToolkit.setClip(left - -fillWidth, top, right, bottom);
		progressBarBackground.drawRepeat(left, top, maxWidth, maxHeight);
		client.graphicsToolkit.setClip(clipBuffer[0], clipBuffer[1], clipBuffer[i], clipBuffer[3]);
	}

	void drawBarForeground(int height, int width, int y, int x, byte i_13_) {
		progressBarForeground.drawRepeat(x, y, width, height);
	}

	@Override
	public final void drawBarOutline(int y, int x, byte i_312_, boolean bool) {
		if (i_312_ == -36) {
			if (bool) {
				int[] clip = new int[4];
				client.graphicsToolkit.getClip(clip);// getclip
				client.graphicsToolkit.setClip(x, y, x + this.config.width, y - -this.config.height);// setclip
				int leftEdgeWidth = outlineLeftEdge.getRenderWidth();
				int leftEdgeHeight = outlineLeftEdge.getRenderHeight();
				int rightEdgeWidth = outlineRightEdge.getRenderWidth();
				int rightEdgeHeight = outlineRightEdge.getRenderHeight();
				outlineLeftEdge.draw(x, (-leftEdgeHeight + this.config.height) / 2 + y);
				outlineRightEdge.draw(-rightEdgeWidth + x - -this.config.width, (this.config.height + -rightEdgeHeight) / 2 + y);
				client.graphicsToolkit.setClip(x, y, this.config.width + x, outlineTop.getRenderHeight() + y);
				outlineTop.drawRepeat(x + leftEdgeWidth, y, -rightEdgeWidth + -leftEdgeWidth + this.config.width, this.config.height);
				int i_317_ = outlineBottom.getRenderHeight();
				client.graphicsToolkit.setClip(x, y + this.config.height - i_317_, this.config.width + x, y - -this.config.height);
				outlineBottom.drawRepeat(x + leftEdgeWidth, -i_317_ + y - -this.config.height, -rightEdgeWidth + this.config.width + -leftEdgeWidth, this.config.height);
				client.graphicsToolkit.setClip(clip[0], clip[1], clip[2], clip[3]);
			}
		}
	}

	@Override
	public final boolean isCached(int i) {
		if (!super.isCached(i)) {
			return false;
		}
		DecoratedProgressBarLSEConfig config = (DecoratedProgressBarLSEConfig) this.config;
		if (!this.glyphStore.isFileCached(-22, config.barForegroundId)) {
			return false;
		}
		if (!this.glyphStore.isFileCached(-81, config.barBackgroundId)) {
			return false;
		}
		if (!this.glyphStore.isFileCached(-87, config.outlineLeftId)) {
			return false;
		}
		if (!this.glyphStore.isFileCached(-58, config.outlineRightId)) {
			return false;
		}
		if (!this.glyphStore.isFileCached(-61, config.outlineTopId)) {
			return false;
		}
		return this.glyphStore.isFileCached(-61, config.outlineBottomId);
	}

	@Override
	public final void loadMedia(byte i) {
		super.loadMedia(i);
		DecoratedProgressBarLSEConfig config = (DecoratedProgressBarLSEConfig) this.config;
		progressBarForeground = Class237_Sub1.loadSprite(config.barForegroundId, this.glyphStore, (byte) -89);
		progressBarBackground = Class237_Sub1.loadSprite(config.barBackgroundId, this.glyphStore, (byte) -89);
		outlineLeftEdge = Class237_Sub1.loadSprite(config.outlineLeftId, this.glyphStore, (byte) -89);
		outlineRightEdge = Class237_Sub1.loadSprite(config.outlineRightId, this.glyphStore, (byte) -89);
		outlineTop = Class237_Sub1.loadSprite(config.outlineTopId, this.glyphStore, (byte) -89);
		outlineBottom = Class237_Sub1.loadSprite(config.outlineBottomId, this.glyphStore, (byte) -89);
	}
}

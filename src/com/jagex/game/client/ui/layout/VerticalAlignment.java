
/* Class110 - Decompiled by JODE
 */ package com.jagex.game.client.ui.layout; /*
												*/

import java.awt.Canvas;

import com.GameShell;
import com.OpenGlToolkit;
import com.RSToolkit;
import com.TextureMetricsList;

public final class VerticalAlignment {
	public static int				anInt946;
	public static VerticalAlignment	BOTTOM				= new VerticalAlignment();

	public static VerticalAlignment	CENTER				= new VerticalAlignment();

	public static String[]			ignoreDisplayNames	= new String[100];

	public static VerticalAlignment	TOP					= new VerticalAlignment();

	public static final RSToolkit create(Canvas canvas, int i, int antialiasCount, TextureMetricsList textureSource) {
		return new OpenGlToolkit(canvas, textureSource, antialiasCount);
	}

	public static final VerticalAlignment[] getValues(int i) {
		return new VerticalAlignment[] { TOP, CENTER, BOTTOM };
	}

	public VerticalAlignment() {
		/* empty */
	}

	public final int getPosition(int height, int clientHeight, byte i_2_) {
		int maxHeight = GameShell.frameHeight <= clientHeight ? clientHeight : GameShell.frameHeight;
		if (TOP == this) {
			return 0;
		}
		if (BOTTOM == this) {
			return -height + maxHeight;
		}
		if (this == CENTER) {
			return (maxHeight - height) / 2;
		}
		return 0;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

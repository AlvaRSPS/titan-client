
/* Class63 - Decompiled by JODE
 */ package com.jagex.game.client.ui.layout; /*
												*/

import java.util.Random;

import com.CacheFileRequest;
import com.Class151_Sub7;
import com.Class228;
import com.Class4;
import com.Class76_Sub5;
import com.Class81;
import com.Class98_Sub46_Sub14;
import com.FileOnDisk;
import com.GameShell;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.definition.parser.BConfigDefinitionParser;
import com.jagex.game.input.RtMouseEvent;

public final class HorizontalAlignment {
	public static int					anInt492;
	public static int					anInt493;
	public static int[]					anIntArray491	= new int[6];

	public static HorizontalAlignment	CENTER			= new HorizontalAlignment();

	public static HorizontalAlignment	LEFT			= new HorizontalAlignment();

	public static HorizontalAlignment	RIGHT			= new HorizontalAlignment();

	public static final HorizontalAlignment[] getValues(int i) {
		return new HorizontalAlignment[] { LEFT, CENTER, RIGHT };
	}

	public static final boolean method547(int i, int i_7_, Class228 class228, int i_8_, int i_9_, int i_10_, int i_11_) {
		if (i_8_ != 0) {
			anInt492 = 76;
		}
		if (!RtMouseEvent.occlusion || !Js5Client.aBoolean1052) {
			return false;
		}
		if ((Class4.pixelsOccludedCount ^ 0xffffffff) > -101) {
			return false;
		}
		if ((i_9_ ^ 0xffffffff) == (i ^ 0xffffffff) && i_10_ == i_7_) {
			if (!Class76_Sub5.method758((byte) 105, i_11_, i_7_, i)) {
				return false;
			}
			if (Class98_Sub46_Sub14.method1607(class228, (byte) 86)) {
				Class151_Sub7.cpsOccludedCount++;
				return true;
			}
			return false;
		}
		for (int i_12_ = i; (i_9_ ^ 0xffffffff) <= (i_12_ ^ 0xffffffff); i_12_++) {
			for (int i_13_ = i_7_; (i_13_ ^ 0xffffffff) >= (i_10_ ^ 0xffffffff); i_13_++) {
				if (CacheFileRequest.anIntArrayArrayArray6311[i_11_][i_12_][i_13_] == -FileOnDisk.anInt3020) {
					return false;
				}
			}
		}
		if (!Class98_Sub46_Sub14.method1607(class228, (byte) 112)) {
			return false;
		}
		Class151_Sub7.cpsOccludedCount++;
		return true;
	}

	public static final int randomNumber(int i, int i_4_, Random random) {
		if (i_4_ <= 0) {
			throw new IllegalArgumentException();
		}
		if (Class81.method815(i_4_, 0)) {
			return (int) ((0xffffffffL & random.nextInt()) * i_4_ >> -1163578144);
		}
		int i_5_ = -(int) (4294967296L % i_4_) + -2147483648;
		int i_6_;
		do {
			i_6_ = random.nextInt();
		} while (i_5_ <= i_6_);
		return BConfigDefinitionParser.method2678((byte) 6, i_6_, i_4_);
	}

	public HorizontalAlignment() {
		/* empty */
	}

	public final int getPosition(int clientWidth, int width, byte i_1_) {
		int maxWidth = GameShell.frameWidth > clientWidth ? GameShell.frameWidth : clientWidth;
		if (this == LEFT) {
			return 0;
		}
		if (this == RIGHT) {
			return -width + maxWidth;
		}
		if (this == CENTER) {
			return (maxWidth - width) / 2;
		}
		return 0;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

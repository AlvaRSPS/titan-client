/* Class98_Sub46_Sub11 - Decompiled by JODE
 */ package com.jagex.game.client.quickchat; /*
												*/

import com.Class112;
import com.Class41;
import com.Class98_Sub10;
import com.PlayerAppearence;
import com.RSByteBuffer;
import com.Sprite;
import com.jagex.core.collections.Cacheable;

public final class QuickChatMessage extends Cacheable {
	public static Sprite[]	aClass332Array6032;
	public static int		anInt6025;

	public static final Class98_Sub10 method1581(byte i, RSByteBuffer buffer) {
		buffer.readUnsignedByte((byte) -106);
		int i_13_ = buffer.readUnsignedByte((byte) -108);
		Class98_Sub10 class98_sub10 = PlayerAppearence.method3630(i_13_, 115);
		class98_sub10.anInt3860 = buffer.readUnsignedByte((byte) 61);
		int i_14_ = buffer.readUnsignedByte((byte) -116);
		for (int i_15_ = 0; (i_15_ ^ 0xffffffff) > (i_14_ ^ 0xffffffff); i_15_++) {
			int i_16_ = buffer.readUnsignedByte((byte) -104);
			class98_sub10.method991(i_16_, buffer, (byte) -116);
		}
		class98_sub10.method1001((byte) 66);
		return class98_sub10;
	}

	public int[][]					configs;
	public QuickChatMessageParser	loader;
	public String[]					message;
	public int[]					responses;

	public boolean					searchable	= true;

	public int[]					types;

	public QuickChatMessage() {
		/* empty */
	}

	public final void decode(byte i, RSByteBuffer buffer) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) -126);
			if ((opcode ^ 0xffffffff) == -1) {
				break;
			}
			this.decode(opcode, -3, buffer);
		}
	}

	private final void decode(int opcode, int i_2_, RSByteBuffer buffer) {
		if ((opcode ^ 0xffffffff) == -2) {
			message = Class112.splitString(buffer.readString((byte) 84), '<', false);
		} else if ((opcode ^ 0xffffffff) != -3) {
			if ((opcode ^ 0xffffffff) == -4) {
				int count = buffer.readUnsignedByte((byte) -103);
				configs = new int[count][];
				types = new int[count];
				for (int index = 0; count > index; index++) {
					int typeId = buffer.readShort((byte) 127);
					QuickChatMessageType type = QuickChatMessageType.valueOf(typeId, (byte) -96);
					if (type != null) {
						types[index] = typeId;
						configs[index] = new int[type.configs];
						for (int config = 0; (config ^ 0xffffffff) > (type.configs ^ 0xffffffff); config++) {
							configs[index][config] = buffer.readShort((byte) 127);
						}
					}
				}
			} else if ((opcode ^ 0xffffffff) == -5) {
				searchable = false;
			}
		} else {
			int count = buffer.readUnsignedByte((byte) 117);
			responses = new int[count];
			for (int index = 0; (count ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				responses[index] = buffer.readShort((byte) 127);
			}
		}
	}

	public final String decodeMessage(int i, RSByteBuffer buffer) {
		StringBuffer stringbuffer = new StringBuffer(80);
		if (types != null) {
			for (int index = 0; index < types.length; index++) {
				stringbuffer.append(message[index]);
				stringbuffer.append(loader.getInterpolant(getType(index, -41), configs[index], (byte) 70, buffer.readBytes(QuickChatMessageType.valueOf(types[index], (byte) -65).decodedSize, false)));
			}
		}
		stringbuffer.append(message[-1 + message.length]);
		return stringbuffer.toString();
	}

	public final void encode(RSByteBuffer buffer, int[] data, int i) {
		while_93_: do {
			if (i != -3) {
				this.decode((byte) 4, null);
			}
			if (types != null) {
				int index = 0;
				for (;;) {
					if (index >= types.length) {
						break while_93_;
					}
					if ((data.length ^ 0xffffffff) >= (index ^ 0xffffffff)) {
						break;
					}
					int size = getType(index, i ^ 0x47).anInt2913;
					if (size > 0) {
						buffer.method1213(31498, data[index], size);
					}
					index++;
				}
			}
			break;
		} while (false);
	}

	public final int fillerCount(byte i) {
		if (types == null) {
			return 0;
		}
		return types.length;
	}

	public final int getConfig(int i, int index, int type) {
		if (types == null || type < 0 || (type ^ 0xffffffff) < (types.length ^ 0xffffffff)) {
			return -1;
		}
		if (configs[type] == null || (index ^ 0xffffffff) > -1 || configs[type].length < index) {
			return -1;
		}
		return configs[type][index];
	}

	public final String getMessage(boolean bool) {
		StringBuffer stringbuffer = new StringBuffer(80);
		if (message == null) {
			return "";
		}
		stringbuffer.append(message[0]);
		for (int index = 1; (message.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
			stringbuffer.append("...");
			stringbuffer.append(message[index]);
		}
		return stringbuffer.toString();
	}

	public final QuickChatMessageType getType(int type, int i_11_) {
		if (types == null || (type ^ 0xffffffff) > -1 || (type ^ 0xffffffff) < (types.length ^ 0xffffffff)) {
			return null;
		}
		return QuickChatMessageType.valueOf(types[type], (byte) -109);
	}

	public final void relativise(byte i) {
		if (responses != null) {
			for (int index = 0; (index ^ 0xffffffff) > (responses.length ^ 0xffffffff); index++) {
				responses[index] = Class41.or(responses[index], 32768);
			}
		}
		configs = null;
	}
}

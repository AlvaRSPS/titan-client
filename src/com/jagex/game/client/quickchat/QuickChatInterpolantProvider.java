/* Interface1 - Decompiled by JODE
 */ package com.jagex.game.client.quickchat; /*
												*/

public interface QuickChatInterpolantProvider {
	String provide(int i, long data, QuickChatMessageType type, int[] configs);
}

/* Class98_Sub46_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.quickchat; /*
												*/

import com.Class172;
import com.Class41;
import com.RSByteBuffer;
import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.preferences.SceneryShadowsPreferenceField;

public final class QuickChatCategory extends Cacheable {
	public static boolean			aBoolean5943;
	public static Class172[][][]	aClass172ArrayArrayArray5948;
	public static int				anInt5945;
	public static int				keyReleaseCount	= 0;

	public char[]					messageHotkeys;
	public int[]					messages;
	public String					name;
	public int[]					subcategories;

	public char[]					subcategoryHotkeys;

	public QuickChatCategory() {
		/* empty */
	}

	private final void decode(int opcode, RSByteBuffer buffer, byte i_0_) {
		if ((opcode ^ 0xffffffff) != -2) {
			if ((opcode ^ 0xffffffff) == -3) {
				int count = buffer.readUnsignedByte((byte) 30);
				subcategoryHotkeys = new char[count];
				subcategories = new int[count];
				for (int index = 0; index < count; index++) {
					subcategories[index] = buffer.readShort((byte) 127);
					byte hotkey = buffer.readSignedByte((byte) -19);
					subcategoryHotkeys[index] = (hotkey ^ 0xffffffff) != -1 ? SceneryShadowsPreferenceField.method576(hotkey, (byte) 123) : '\0';
				}
			} else if (opcode == 3) {
				int count = buffer.readUnsignedByte((byte) 45);
				messages = new int[count];
				messageHotkeys = new char[count];
				for (int index = 0; (index ^ 0xffffffff) > (count ^ 0xffffffff); index++) {
					messages[index] = buffer.readShort((byte) 127);
					byte hotkey = buffer.readSignedByte((byte) -19);
					messageHotkeys[index] = hotkey == 0 ? '\0' : SceneryShadowsPreferenceField.method576(hotkey, (byte) 125);
				}
			}
		} else {
			name = buffer.readString((byte) 84);
		}
	}

	public final void decode(RSByteBuffer buffer, boolean bool) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) 29);
			if (opcode == 0) {
				break;
			}
			this.decode(opcode, buffer, (byte) 61);
		}
	}

	public final int getMessage(char hotkeys, byte i) {
		if (messages == null) {
			return -1;
		}
		for (int index = 0; messages.length > index; index++) {
			if (hotkeys == messageHotkeys[index]) {
				return messages[index];
			}
		}
		return -1;
	}

	public final int getSubcategory(int i, char hotkey) {
		if (subcategories == null) {
			return -1;
		}
		for (int index = 0; index < subcategories.length; index++) {
			if (hotkey == subcategoryHotkeys[index]) {
				return subcategories[index];
			}
		}
		return -1;
	}

	public final void relativise(int i) {
		do {
			if (messages != null) {
				for (int index = 0; index < messages.length; index++) {
					messages[index] = Class41.or(messages[index], 32768);
				}
			}
			if (subcategories == null) {
				break;
			}
			for (int index = 0; (subcategories.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				subcategories[index] = Class41.or(subcategories[index], 32768);
			}
			break;
		} while (false);
	}
}

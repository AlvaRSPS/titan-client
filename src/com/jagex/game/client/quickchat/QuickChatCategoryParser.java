/* Class212 - Decompiled by JODE
 */ package com.jagex.game.client.quickchat; /*
												*/

import com.GameShell;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;

public final class QuickChatCategoryParser {
	public static int			loadingProgress	= 0;
	public static int[]			anIntArray1597	= new int[5];

	private AdvancedMemoryCache	cache			= new AdvancedMemoryCache(64);

	private Js5					quickChat;

	private Js5					restrictedChat;

	public QuickChatCategoryParser(int i, Js5 quickChat, Js5 restrictedChat) {
		do {
			this.quickChat = quickChat;
			this.restrictedChat = restrictedChat;
			if (this.quickChat != null) {
				this.quickChat.getFileCount(0, 0);
			}
			if (this.restrictedChat == null) {
				break;
			}
			this.restrictedChat.getFileCount(0, 0);
			break;
		} while (false);
	}

	public final QuickChatCategory get(int i, int id) {
		QuickChatCategory category = (QuickChatCategory) cache.get(-124, id);
		if (category != null) {
			return category;
		}
		byte[] data;
		do {
			if (id >= 32768) {
				data = restrictedChat.getFile(0x7fff & id, 0, false);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			data = quickChat.getFile(id, 0, false);
		} while (false);
		category = new QuickChatCategory();
		if (data != null) {
			category.decode(new RSByteBuffer(data), true);
		}
		if ((id ^ 0xffffffff) <= -32769) {
			category.relativise(i ^ 0xef8f);
		}
		cache.put(id, category, (byte) -80);
		return category;
	}
}

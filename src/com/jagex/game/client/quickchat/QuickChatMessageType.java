/* Class348 - Decompiled by JODE
 */ package com.jagex.game.client.quickchat; /*
												*/

import com.Class151_Sub7;
import com.Class151_Sub9;
import com.Class186;
import com.Class283;
import com.Class359;
import com.Class4;
import com.Class42_Sub3;
import com.Class53_Sub1;
import com.Class98_Sub28_Sub1;
import com.Class98_Sub36;
import com.Class98_Sub5_Sub1;
import com.IncomingOpcode;
import com.TextureMetrics;
import com.SceneGraphNodeList;
import com.aa_Sub3;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.input.impl.AwtKeyListener;

public final class QuickChatMessageType {
	public static boolean			aBoolean2914;
	public static IncomingOpcode	aClass58_2912	= new IncomingOpcode(19, 2);
	public static int				anInt2911;

	public static final void method3836(int i, int i_0_, int i_1_, int i_2_, boolean bool, boolean bool_3_, byte i_4_) {
		do {
			if (i_0_ < i_1_) {
				int i_5_ = (i_0_ + i_1_) / 2;
				int i_6_ = i_0_;
				Class53_Sub1 class53_sub1 = Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_5_];
				Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_5_] = Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_1_];
				Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_1_] = class53_sub1;
				for (int i_7_ = i_0_; (i_1_ ^ 0xffffffff) < (i_7_ ^ 0xffffffff); i_7_++) {
					if ((Class283.method3348(Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_7_], 17, bool, class53_sub1, bool_3_, i_2_, i) ^ 0xffffffff) >= -1) {
						Class53_Sub1 class53_sub1_8_ = Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_7_];
						Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_7_] = Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_6_];
						Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_6_++] = class53_sub1_8_;
					}
				}
				Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_1_] = Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_6_];
				Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_6_] = class53_sub1;
				method3836(i, i_0_, i_6_ - 1, i_2_, bool, bool_3_, (byte) 93);
				method3836(i, 1 + i_6_, i_1_, i_2_, bool, bool_3_, (byte) 53);
			}
			if (i_4_ > 52) {
				break;
			}
			aBoolean2914 = false;
			break;
		} while (false);
	}

	public static void method3837(int i) {
		do {
			aClass58_2912 = null;
			if (i == 2) {
				break;
			}
			aBoolean2914 = false;
			break;
		} while (false);
	}

	public static final QuickChatMessageType valueOf(int i, byte i_66_) {
		QuickChatMessageType[] type = values((byte) 15);
		for (int index = 0; (index ^ 0xffffffff) > (type.length ^ 0xffffffff); index++) {
			if (i == type[index].id) {
				return type[index];
			}
		}
		return null;
	}

	public static final QuickChatMessageType[] values(byte i) {
		return new QuickChatMessageType[] { Class151_Sub9.aClass348_5023, AwtKeyListener.aClass348_3801, Class4.aClass348_82, EnumDefinition.aClass348_2565, Class42_Sub3.aClass348_5363, Class98_Sub36.aClass348_4156, Class186.aClass348_3433, aa_Sub3.aClass348_3573, Class359.aClass348_3046,
				Class151_Sub7.aClass348_5008, SceneGraphNodeList.aClass348_1630, Js5.aClass348_1569, Class98_Sub5_Sub1.aClass348_5534, TextureMetrics.aClass348_1834 };
	}

	int			anInt2913;

	int			configs;

	int			decodedSize;

	public int	id;

	public QuickChatMessageType(int id, int i_9_, int i_10_, int i_11_) {
		this.id = id;
		decodedSize = i_10_;
		configs = i_11_;
		anInt2913 = i_9_;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

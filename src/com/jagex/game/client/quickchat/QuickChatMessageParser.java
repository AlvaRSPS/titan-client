
/* Class280 - Decompiled by JODE
 */ package com.jagex.game.client.quickchat; /*
												*/

import java.awt.Rectangle;

import com.ActionGroup;
import com.ActionQueueEntry;
import com.Class142;
import com.Class164;
import com.Class186;
import com.Class211;
import com.Class359;
import com.Class37;
import com.Class76_Sub2;
import com.Class81;
import com.Class87;
import com.Class98_Sub10_Sub9;
import com.Class98_Sub13;
import com.Class98_Sub24_Sub1;
import com.Class98_Sub31_Sub5;
import com.Class98_Sub42;
import com.Class98_Sub47;
import com.GameShell;
import com.Player;
import com.RSByteBuffer;
import com.RtInterfaceAttachment;
import com.client;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.parser.NPCDefinitionParser;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;
import com.jagex.game.constants.BuildLocation;

public final class QuickChatMessageParser {
	public static LinkedList	projectiles;
	public static Class164		aClass164_2101		= new Class164(1);
	public static long			aLong2112;
	public static int			anInt2105;
	public static Rectangle[]	aRectangleArray2106	= new Rectangle[100];

	static {
		anInt2105 = 1;
		projectiles = new LinkedList();
		aLong2112 = 1L;
	}

	public static final void method3324(ActionQueueEntry entry, int i) {
		try {
			if (!Player.menuOpen) {
				entry.unlink(87);
				Class359.actionCount--;
				if (!entry.notGroupable) {
					long l = entry.groupHash;
					ActionGroup group;
					for (group = (ActionGroup) Class98_Sub47.groupMap.get(l, -1); group != null; group = (ActionGroup) Class98_Sub47.groupMap.nextHashCollision(125)) {
						if (group.actionText.equals(entry.targetText)) {
							break;
						}
					}
					if (group != null && group.method1557((byte) -105, entry)) {
						ActionGroup.addGroup(group, (byte) 87);
					}
				} else {
					for (ActionGroup group = (ActionGroup) RtInterfaceAttachment.actionGroups.getFirst(-1); group != null; group = (ActionGroup) RtInterfaceAttachment.actionGroups.getNext(0)) {
						if (group.actionText.equals(entry.targetText)) {
							boolean bool = false;
							for (ActionQueueEntry _entry = (ActionQueueEntry) group.actions.getFirst(-1); _entry != null; _entry = (ActionQueueEntry) group.actions.getNext(0)) {
								if (_entry == entry) {
									if (group.method1557((byte) -125, entry)) {
										ActionGroup.addGroup(group, (byte) 87);
									}
									bool = true;
									break;
								}
							}
							if (bool) {
								break;
							}
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rh.E(" + (entry != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static final void method3327(int i, AnimationDefinition class97, byte i_3_) {
		try {
			if ((EnumDefinition.anInt2566 ^ 0xffffffff) > -51 && class97 != null && class97.anIntArrayArray822 != null && (class97.anIntArrayArray822.length ^ 0xffffffff) < (i ^ 0xffffffff) && class97.anIntArrayArray822[i] != null) {
				int i_4_ = class97.anIntArrayArray822[i][0];
				int i_5_ = i_4_ >> -103665464;
				if (class97.anIntArrayArray822[i].length > 1) {
					int i_6_ = (int) (Math.random() * class97.anIntArrayArray822[i].length);
					if (i_6_ > 0) {
						i_5_ = class97.anIntArrayArray822[i][i_6_];
					}
				}
				int i_7_ = (0xf6 & i_4_) >> 264525829;
				int i_9_ = 256;
				if (class97.anIntArray810 != null && class97.anIntArray815 != null) {
					i_9_ = Class142.method2307(class97.anIntArray810[i], class97.anIntArray815[i], 52);
				}
				if (!class97.aBoolean812) {
					NPCDefinitionParser.method3537(i_9_, (byte) 1, i_5_, i_7_, 0, 255);
				} else {
					Class98_Sub10_Sub9.method1036(-1962608884, 255, i_7_, i_5_, false, 0, i_9_);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rh.D(" + i + ',' + (class97 != null ? "{...}" : "null") + ',' + i_3_ + ')');
		}
	}

	public static final void method3328(int i, int i_10_, int i_11_, int i_12_, int i_13_, Class98_Sub42 class98_sub42) {
		while_164_: do {
			do {
				try {
					if ((class98_sub42.anInt4210 ^ 0xffffffff) == 0 && class98_sub42.anIntArray4208 == null) {
						break;
					}
					int i_14_ = 0;
					int i_15_ = class98_sub42.anInt4236 * client.preferences.areaSoundsVolume.getValue((byte) 124) >> -884604664;
					if ((i_12_ ^ 0xffffffff) >= (class98_sub42.anInt4224 ^ 0xffffffff)) {
						if ((i_12_ ^ 0xffffffff) > (class98_sub42.anInt4229 ^ 0xffffffff)) {
							i_14_ += -i_12_ + class98_sub42.anInt4229;
						}
					} else {
						i_14_ += -class98_sub42.anInt4224 + i_12_;
					}
					if (class98_sub42.anInt4216 < i) {
						i_14_ += -class98_sub42.anInt4216 + i;
					} else if ((class98_sub42.anInt4225 ^ 0xffffffff) < (i ^ 0xffffffff)) {
						i_14_ += class98_sub42.anInt4225 + -i;
					}
					if (class98_sub42.anInt4228 == 0 || i_14_ + -256 > class98_sub42.anInt4228 || (client.preferences.areaSoundsVolume.getValue((byte) 120) ^ 0xffffffff) == -1 || (class98_sub42.anInt4220 ^ 0xffffffff) != (i_11_ ^ 0xffffffff)) {
						if (class98_sub42.aClass98_Sub31_Sub5_4232 != null) {
							Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4232);
							class98_sub42.aClass98_Sub24_Sub1_4214 = null;
							class98_sub42.aClass98_Sub31_Sub5_4232 = null;
							class98_sub42.aClass98_Sub13_4213 = null;
						}
						if (class98_sub42.aClass98_Sub31_Sub5_4230 != null) {
							Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4230);
							class98_sub42.aClass98_Sub24_Sub1_4211 = null;
							class98_sub42.aClass98_Sub13_4231 = null;
							class98_sub42.aClass98_Sub31_Sub5_4230 = null;
						}
						break;
					}
					i_14_ -= i_10_;
					if (i_14_ < 0) {
						i_14_ = 0;
					}
					int i_16_ = -class98_sub42.anInt4217 + class98_sub42.anInt4228;
					if ((i_16_ ^ 0xffffffff) > -1) {
						i_16_ = class98_sub42.anInt4228;
					}
					int i_17_ = i_15_;
					int i_18_ = i_14_ - class98_sub42.anInt4217;
					if ((i_18_ ^ 0xffffffff) < -1 && (i_16_ ^ 0xffffffff) < -1) {
						i_17_ = i_15_ * (i_16_ - i_18_) / i_16_;
					}
					Class87.localPlayer.getSize(i_10_ + -256);
					int i_19_ = 8192;
					int i_20_ = (class98_sub42.anInt4229 + class98_sub42.anInt4224) / 2 + -i_12_;
					int i_21_ = (class98_sub42.anInt4225 + class98_sub42.anInt4216) / 2 - i;
					if (i_20_ != 0 || (i_21_ ^ 0xffffffff) != -1) {
						int i_22_ = -4096 + -Class186.cameraX + -(int) (2607.5945876176133 * Math.atan2(i_20_, i_21_)) & 0x3fff;
						if ((i_22_ ^ 0xffffffff) < -8193) {
							i_22_ = 16384 + -i_22_;
						}
						int i_23_;
						if ((i_14_ ^ 0xffffffff) >= -1) {
							i_23_ = 8192;
						} else if (i_14_ >= 4096) {
							i_23_ = 16384;
						} else {
							i_23_ = 8192 - -(i_14_ * 8192 / 4096);
						}
						i_19_ = (16384 - i_23_ >> 461462369) + i_23_ * i_22_ / 8192;
					}
					if (class98_sub42.aClass98_Sub31_Sub5_4232 == null) {
						if (class98_sub42.anInt4210 >= 0) {
							int i_24_ = class98_sub42.anInt4237 != 256 || class98_sub42.anInt4223 != 256 ? Class142.method2307(class98_sub42.anInt4223, class98_sub42.anInt4237, i_10_ + -347) : 256;
							if (!class98_sub42.aBoolean4215) {
								Class37 class37 = Class37.method342(Class76_Sub2.soundEffectsJs5, class98_sub42.anInt4210, 0);
								if (class37 != null) {
									Class98_Sub24_Sub1 class98_sub24_sub1 = class37.method344().method1269(LinkedList.aClass314_1195);
									Class98_Sub31_Sub5 class98_sub31_sub5 = Class98_Sub31_Sub5.method1402(class98_sub24_sub1, i_24_, i_17_ << -44812154, i_19_);
									class98_sub31_sub5.method1422(-1);
									Class81.aClass98_Sub31_Sub3_619.method1371(class98_sub31_sub5);
									class98_sub42.aClass98_Sub31_Sub5_4232 = class98_sub31_sub5;
								}
							} else {
								if (class98_sub42.aClass98_Sub13_4213 == null) {
									class98_sub42.aClass98_Sub13_4213 = Class98_Sub13.method1137(BuildLocation.midiInstrumentsJs5, class98_sub42.anInt4210);
								}
								if (class98_sub42.aClass98_Sub13_4213 != null) {
									if (class98_sub42.aClass98_Sub24_Sub1_4214 == null) {
										class98_sub42.aClass98_Sub24_Sub1_4214 = class98_sub42.aClass98_Sub13_4213.method1132(new int[] { 22050 });
									}
									if (class98_sub42.aClass98_Sub24_Sub1_4214 != null) {
										Class98_Sub31_Sub5 class98_sub31_sub5 = Class98_Sub31_Sub5.method1402(class98_sub42.aClass98_Sub24_Sub1_4214, i_24_, i_17_ << -669300698, i_19_);
										class98_sub31_sub5.method1422(-1);
										Class81.aClass98_Sub31_Sub3_619.method1371(class98_sub31_sub5);
										class98_sub42.aClass98_Sub31_Sub5_4232 = class98_sub31_sub5;
									}
								}
							}
						}
					} else {
						class98_sub42.aClass98_Sub31_Sub5_4232.method1427(i_17_);
						class98_sub42.aClass98_Sub31_Sub5_4232.method1431(i_19_);
					}
					if (class98_sub42.aClass98_Sub31_Sub5_4230 == null) {
						if (class98_sub42.anIntArray4208 != null && ((class98_sub42.anInt4221 -= i_13_) ^ 0xffffffff) >= -1) {
							int i_25_ = class98_sub42.anInt4237 != 256 || class98_sub42.anInt4223 != 256 ? class98_sub42.anInt4223 + (int) (Math.random() * (-class98_sub42.anInt4223 + class98_sub42.anInt4237)) : 256;
							if (class98_sub42.aBoolean4226) {
								if (class98_sub42.aClass98_Sub13_4231 == null) {
									int i_26_ = (int) (Math.random() * class98_sub42.anIntArray4208.length);
									class98_sub42.aClass98_Sub13_4231 = Class98_Sub13.method1137(BuildLocation.midiInstrumentsJs5, class98_sub42.anIntArray4208[i_26_]);
								}
								if (class98_sub42.aClass98_Sub13_4231 != null) {
									if (class98_sub42.aClass98_Sub24_Sub1_4211 == null) {
										class98_sub42.aClass98_Sub24_Sub1_4211 = class98_sub42.aClass98_Sub13_4231.method1132(new int[] { 22050 });
									}
									if (class98_sub42.aClass98_Sub24_Sub1_4211 != null) {
										Class98_Sub31_Sub5 class98_sub31_sub5 = Class98_Sub31_Sub5.method1402(class98_sub42.aClass98_Sub24_Sub1_4211, i_25_, i_17_ << -1521283386, i_19_);
										class98_sub31_sub5.method1422(0);
										Class81.aClass98_Sub31_Sub3_619.method1371(class98_sub31_sub5);
										class98_sub42.aClass98_Sub31_Sub5_4230 = class98_sub31_sub5;
										class98_sub42.anInt4221 = (int) (Math.random() * (-class98_sub42.anInt4219 + class98_sub42.anInt4205)) + class98_sub42.anInt4219;
									}
								}
							} else {
								int i_27_ = (int) (Math.random() * class98_sub42.anIntArray4208.length);
								Class37 class37 = Class37.method342(Class76_Sub2.soundEffectsJs5, class98_sub42.anIntArray4208[i_27_], 0);
								if (class37 == null) {
									break;
								}
								Class98_Sub24_Sub1 class98_sub24_sub1 = class37.method344().method1269(LinkedList.aClass314_1195);
								Class98_Sub31_Sub5 class98_sub31_sub5 = Class98_Sub31_Sub5.method1402(class98_sub24_sub1, i_25_, i_17_ << 1722792038, i_19_);
								class98_sub31_sub5.method1422(0);
								Class81.aClass98_Sub31_Sub3_619.method1371(class98_sub31_sub5);
								class98_sub42.anInt4221 = (int) ((-class98_sub42.anInt4219 + class98_sub42.anInt4205) * Math.random()) + class98_sub42.anInt4219;
								class98_sub42.aClass98_Sub31_Sub5_4230 = class98_sub31_sub5;
							}
							break;
						}
					} else {
						class98_sub42.aClass98_Sub31_Sub5_4230.method1427(i_17_);
						class98_sub42.aClass98_Sub31_Sub5_4230.method1431(i_19_);
						if (class98_sub42.aClass98_Sub31_Sub5_4230.isLinked((byte) 78)) {
							break while_164_;
						}
						class98_sub42.aClass98_Sub31_Sub5_4230 = null;
						class98_sub42.aClass98_Sub13_4231 = null;
						class98_sub42.aClass98_Sub24_Sub1_4211 = null;
					}
				} catch (RuntimeException runtimeException) {
					throw Js5Exception.createJs5Exception(runtimeException, "rh.F(" + i + ',' + i_10_ + ',' + i_11_ + ',' + i_12_ + ',' + i_13_ + ',' + (class98_sub42 != null ? "{...}" : "null") + ')');
				}
			} while (false);
			break;
		} while (false);
	}

	public static final void method3330(int i) {
		try {
			synchronized (Class211.aClass79_1594) {
				if (i != 1) {
					projectiles = null;
				}
				Class211.aClass79_1594.clear(i + 30);
			}
			synchronized (StoreProgressMonitor.aClass79_3411) {
				StoreProgressMonitor.aClass79_3411.clear(16);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rh.B(" + i + ')');
		}
	}

	public AdvancedMemoryCache				cache;

	public int								count;

	private QuickChatInterpolantProvider	interpolantProvider;

	public Js5								quickChat;

	public Js5								restrictedChat;

	public int								restrictedCount	= 0;

	public QuickChatMessageParser(int i, Js5 quickChat, Js5 restrictedChat, QuickChatInterpolantProvider provider) {
		count = 0;
		cache = new AdvancedMemoryCache(64);
		interpolantProvider = null;
		do {
			this.quickChat = quickChat;
			interpolantProvider = provider;
			this.restrictedChat = restrictedChat;
			if (this.quickChat != null) {
				count = this.quickChat.getFileCount(0, 1);
			}
			if (this.restrictedChat == null) {
				break;
			}
			restrictedCount = this.restrictedChat.getFileCount(0, 1);
			break;
		} while (false);
	}

	public final QuickChatMessage get(int id, int i_1_) {
		QuickChatMessage message = (QuickChatMessage) cache.get(-125, id);
		if (message != null) {
			return message;
		}
		byte[] data;
		do {
			if ((id ^ 0xffffffff) <= -32769) {
				data = restrictedChat.getFile(0x7fff & id, 1, false);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			data = quickChat.getFile(id, 1, false);
		} while (false);
		message = new QuickChatMessage();
		message.loader = this;
		if (data != null) {
			message.decode((byte) -89, new RSByteBuffer(data));
		}
		if ((id ^ 0xffffffff) <= -32769) {
			message.relativise((byte) -123);
		}
		cache.put(id, message, (byte) -80);
		return message;
	}

	public final String getInterpolant(QuickChatMessageType type, int[] configs, byte i, long data) {
		if (interpolantProvider != null) {
			String string = interpolantProvider.provide(17438, data, type, configs);
			if (string != null) {
				return string;
			}
		}
		return Long.toString(data);
	}
}

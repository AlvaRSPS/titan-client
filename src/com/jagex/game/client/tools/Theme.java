package com.jagex.game.client.tools;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.jagex.Launcher;

public class Theme implements ActionListener {

	public static String		THEME;
	private static final String	MAGMA	= "org.jvnet.substance.skin.SubstanceMagmaLookAndFeel", EMERALD = "org.jvnet.substance.skin.SubstanceEmeraldDuskLookAndFeel", BLACK_STEEL = "org.jvnet.substance.skin.SubstanceBusinessBlackSteelLookAndFeel",
			RAVEN = "org.jvnet.substance.skin.SubstanceRavenGraphiteGlassLookAndFeel", CHALLENGER = "org.jvnet.substance.skin.SubstanceChallengerDeepLookAndFeel", SAHARA = "org.jvnet.substance.skin.SubstanceSaharaLookAndFeel", SILVER = "org.jvnet.substance.skin.SubstanceBusinessLookAndFeel",
			BLUE = "org.jvnet.substance.skin.SubstanceBusinessBlueSteelLookAndFeel";

	public static void setTheme() {
		try {
			if (THEME != null) {
				UIManager.setLookAndFeel(THEME);
			} else if (THEME == null) {
				UIManager.setLookAndFeel(EMERALD);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
	}

	private JFrame				jFrame;

	public JFileChooser			chooser;

	public JComboBox<String>	tList;

	public Theme() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		jFrame = new JFrame("Choose your desired Look & Feel Theme:");
		jFrame.setDefaultCloseOperation(2);
		jFrame.getContentPane().setLayout(new BorderLayout());
		jFrame.setBounds(100, 30, 337, 62);
		jFrame.setResizable(false);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		jFrame.getContentPane().add(panel, BorderLayout.CENTER);

		String[] themes = { "Magma", "Emerald", "Raven", "Black Steel", "Challenger", "Sahara", "Silver", "Blue" };
		tList = new JComboBox<String>(themes);
		jFrame.add(tList);
		tList.setBounds(10, 144, 301, 20);
		tList.addActionListener(this);

		chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.addActionListener(this);

		String lookAndFeel = UIManager.getLookAndFeel().toString();
		lookAndFeel = lookAndFeel.substring(49, lookAndFeel.indexOf("]"));
		//System.out.println(lookAndFeel);
		switch (lookAndFeel) {
		case MAGMA:
			tList.setSelectedItem("Magma");
			break;
		case EMERALD:
			tList.setSelectedItem("Emerald");
			break;
		case BLACK_STEEL:
			tList.setSelectedItem("Black Steel");
			break;
		case RAVEN:
			tList.setSelectedItem("Raven");
			break;
		case CHALLENGER:
			tList.setSelectedItem("Challenger");
			break;
		case SAHARA:
			tList.setSelectedItem("Sahara");
			break;
		case SILVER:
			tList.setSelectedItem("Silver");
			break;
		case BLUE:
			tList.setSelectedItem("Blue");
			break;
		}
		jFrame.setVisible(true);
		Launcher.save();

	}

	@Override
	public void actionPerformed(ActionEvent a) {
		String action = a.getActionCommand().toLowerCase();
		try {
			switch (action) {
			case "comboboxchanged":
				Object selectedTheme = tList.getSelectedItem();
				switch (selectedTheme.toString().toLowerCase()) {
				case "magma":
					UIManager.setLookAndFeel(MAGMA);
					THEME = MAGMA;
					break;
				case "emerald":
					UIManager.setLookAndFeel(EMERALD);
					THEME = EMERALD;
					break;
				case "black steel":
					UIManager.setLookAndFeel(BLACK_STEEL);
					THEME = BLACK_STEEL;
					break;
				case "raven":
					UIManager.setLookAndFeel(RAVEN);
					THEME = RAVEN;
					break;
				case "challenger":
					UIManager.setLookAndFeel(CHALLENGER);
					THEME = CHALLENGER;
					break;
				case "sahara":
					UIManager.setLookAndFeel(SAHARA);
					THEME = SAHARA;
					break;
				case "silver":
					UIManager.setLookAndFeel(SILVER);
					THEME = SILVER;
					break;
				case "blue":
					UIManager.setLookAndFeel(BLUE);
					THEME = BLUE;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Launcher.save();
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
	}

}

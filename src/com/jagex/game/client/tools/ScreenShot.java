package com.jagex.game.client.tools;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.codec.binary.Base64;

import com.jagex.Launcher;

public class ScreenShot implements ActionListener {
	private static String		URL_PATH	= null;
	private static final String	API_KEY		= "b84e430b4a65d16a6955358141f21a61";
	private JFrame				f;
	private JButton				s, d, u;
	private BufferedImage		image;

	public ScreenShot(BufferedImage image) {
		this.image = image;
		openFrame();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String a = e.getActionCommand().toLowerCase();
		switch (a) {
		case "save":
			try {
				ImageIO.write(image, "PNG", new File(Launcher.SCREENSHOT_PATH + "" + getFormattedDate() + ".png"));
				JOptionPane.showConfirmDialog(f, "Saved to: " + Launcher.SCREENSHOT_PATH, "Succesfully stored your capture", JOptionPane.PLAIN_MESSAGE);
				f.dispose();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			break;
		case "discard":
			if (JOptionPane.showConfirmDialog(f, "You are about to discard the screenshot. This is PERMANENT. Are you sure?", "Confirm exit.", JOptionPane.OK_OPTION, 0, new ImageIcon("")) != 0) {
				return;
			}
			f.dispose();
			break;
		case "upload":
			try {
				uploadToImageHost(image);
				new ScreenShotLink(URL_PATH);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			break;
		}
	}

	public String getFormattedDate() {
		DateFormat dateFormat = new SimpleDateFormat("hh.mm.ss a");
		Date date = new Date();
		String formattedDate = dateFormat.format(date);
		return formattedDate;
	}

	public void openFrame() {
		f = new JFrame("Save, discard or upload your screenshot");
		f.setBounds(99, 99, image.getWidth(), image.getHeight() + 56);//56: menu
		f.setResizable(false);
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (JOptionPane.showConfirmDialog(f, "Are you sure you want to discard this image(you will not be able to get it back)?", "Confirm exit.", JOptionPane.OK_OPTION, 0, new ImageIcon("")) != 0) {
					return;
				}
				f.dispose();
			}
		});
		f.setDefaultCloseOperation(0);

		JPanel cP = new JPanel();
		cP.setBorder(new EmptyBorder(5, 5, 5, 5));
		f.setContentPane(cP);
		cP.setLayout(null);

		JLabel picLabel = new JLabel(new ImageIcon(image));
		picLabel.setBounds(0, 0, image.getWidth(), image.getHeight());
		cP.add(picLabel);

		JMenuBar m = new JMenuBar();
		f.setJMenuBar(m);
		s = new JButton("Save");
		m.add(s);
		s.addActionListener(this);
		s.setToolTipText("Save the screenshot to your machine");
		d = new JButton("Discard");
		m.add(d);
		d.addActionListener(this);
		d.setToolTipText("Delete the screenshot");
		u = new JButton("Upload");
		m.add(u);
		u.addActionListener(this);
		u.setToolTipText("Upload the screenshot to an imagehost");

		JPanel p = new JPanel();
		p.setLayout(null);
		p.setBounds(0, 0, 484, 362);
		cP.add(p);

		f.setVisible(true);
	}

	public void uploadToImageHost(BufferedImage image) throws Exception {
		String POST_URI = "http://api.imgur.com/2/upload.xml";
		String readLine = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos);
			URL url = new URL(POST_URI);

			String data = URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(Base64.encodeBase64String(baos.toByteArray()).toString(), "UTF-8");
			data += "&" + URLEncoder.encode("key", "UTF-8") + "=" + URLEncoder.encode(API_KEY, "UTF-8");

			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(data);
			writer.flush();
			InputStream inputStream;
			if (((HttpURLConnection) conn).getResponseCode() == 400) {
				inputStream = ((HttpURLConnection) conn).getErrorStream();
			} else {
				inputStream = conn.getInputStream();
			}
			BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			while ((line = rd.readLine()) != null) {
				readLine = line;
			}
			writer.close();
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		URL_PATH = readLine.substring(readLine.indexOf("<original>") + 10, readLine.indexOf("</original>"));
	}

}

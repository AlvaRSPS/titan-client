package com.jagex.game.client.tools;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ScreenShotLink implements ActionListener {
	private JFrame		jFrame;
	private String		link;
	private JTextField	iLink;
	private JButton		copyButton, forumButton;
	private JTextField	forumLink;

	public ScreenShotLink(String link) {
		this.link = link;
		openFrame();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand().toLowerCase();
		switch (action) {
		case "copy link":
			String link = iLink.getText();
			StringSelection linkSelection = new StringSelection(link);
			Clipboard clipbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipbrd.setContents(linkSelection, linkSelection);
			break;
		case "close":
			jFrame.dispose();
			break;
		}
	}

	public void openFrame() {
		jFrame = new JFrame("Link for your image.");
		jFrame.setBounds(250, 250, 388, 223);
		jFrame.setResizable(false);
		jFrame.setDefaultCloseOperation(2);

		JPanel contentPane = new JPanel();
		jFrame.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 382, 195);
		contentPane.add(panel);

		JLabel lblDirectLink = new JLabel("Direct Link:");
		lblDirectLink.setBounds(10, 11, 362, 14);
		panel.add(lblDirectLink);

		iLink = new JTextField();
		iLink.setEditable(false);
		iLink.setBounds(10, 36, 263, 20);
		panel.add(iLink);
		iLink.setColumns(10);
		iLink.setText(link);

		copyButton = new JButton("Copy Link");
		copyButton.setBounds(283, 35, 89, 23);
		panel.add(copyButton);
		copyButton.addActionListener(this);

		JButton btnClose = new JButton("Close");
		btnClose.setBounds(283, 165, 89, 23);
		panel.add(btnClose);

		forumButton = new JButton("Copy Link");
		forumButton.setBounds(283, 113, 89, 23);
		panel.add(forumButton);
		forumButton.addActionListener(e -> {
			String link = forumLink.getText();
			StringSelection linkSelection = new StringSelection(link);

			Clipboard clipbrd = Toolkit.getDefaultToolkit().getSystemClipboard();

			clipbrd.setContents(linkSelection, linkSelection);
		});

		forumLink = new JTextField();
		forumLink.setEditable(false);
		forumLink.setColumns(10);
		forumLink.setBounds(10, 114, 263, 20);
		forumLink.setText("[IMG]" + link + "[/IMG]");
		panel.add(forumLink);

		JLabel lblForumLink = new JLabel("Forum Link:");
		lblForumLink.setBounds(10, 88, 362, 14);
		panel.add(lblForumLink);
		btnClose.addActionListener(this);

		jFrame.setVisible(true);
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(System.getProperty("user.home") + "\\TitanData\\Links.txt", true)))) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			//get current date time with Calendar()
			Calendar cal = Calendar.getInstance();
			out.println(dateFormat.format(cal.getTime()) + ": " + link);
			out.println("[IMG]" + link + "[/IMG]");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

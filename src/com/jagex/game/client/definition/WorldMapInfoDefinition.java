/* Class24 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class48;
import com.Image;
import com.NodeInteger;
import com.NodeString;
import com.RSByteBuffer;
import com.RSToolkit;
import com.Sprite;
import com.VarValues;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;

public final class WorldMapInfoDefinition {
	public static int	anInt223;
	public static int	logicTimer;
	public static int	anInt255	= 0;

	static {
		logicTimer = 1;
	}

	public boolean						morphisms;
	public boolean						aBoolean241;
	public boolean						aBoolean258;
	public boolean						aBoolean261;
	public byte[]						aByteArray229;
	private HashTable					aClass377_256;
	public int							anInt224;
	public int							anInt225;
	public int							anInt226;
	private int							anInt227;
	public int							anInt228;
	private int							anInt231;
	public int							anInt235;
	private int							anInt236;
	public int							anInt238;
	public int							anInt239;
	private int							anInt240	= -1;
	private int							anInt243;
	public int							anInt244	= 2147483647;
	public int							anInt245;
	public int							anInt246	= -1;
	public int							anInt247;
	public int							anInt248;
	public int							anInt249;
	public int							anInt250;
	private int							anInt251;
	public int							anInt252;
	public int							anInt253;
	private int							anInt254;
	public int							anInt257;
	private int							anInt259;
	private int							anInt260;
	public int							anInt262;
	public int							anInt264;
	public int[]						anIntArray234;
	public int[]						anIntArray265;
	public String						aString232;
	public String						locationName;
	public String[]						aStringArray237;

	public WorldMapInfoDefinitionParser	parser;

	public WorldMapInfoDefinition() {
		anInt245 = -1;
		anInt224 = -1;
		anInt248 = 2147483647;
		anInt243 = -1;
		anInt231 = -1;
		anInt247 = -2147483648;
		anInt238 = -1;
		anInt225 = -1;
		morphisms = false;
		aBoolean261 = true;
		aBoolean258 = true;
		anInt253 = -1;
		anInt259 = -1;
		aBoolean241 = true;
		anInt260 = -1;
		anInt262 = -2147483648;
		anInt264 = 0;
		anInt250 = -1;
		aStringArray237 = new String[5];
		// System.out.println(anInt262);
	}

	public final boolean method284(int i, VarValues interface6) {
		try {
			int i_0_;
			if (anInt260 == -1) {
				if ((anInt259 ^ 0xffffffff) == 0) {
					return true;
				}
				i_0_ = interface6.getClientVarpBit(anInt259, 7373);
			} else {
				i_0_ = interface6.getClientVarp(anInt260, 83);
			}
			if ((i_0_ ^ 0xffffffff) > (anInt251 ^ 0xffffffff) || anInt227 < i_0_) {
				return false;
			}
			if (i < 6) {
				anInt225 = -120;
			}
			int i_1_;
			if (anInt243 == -1) {
				if (anInt240 != -1) {
					i_1_ = interface6.getClientVarpBit(anInt240, 7373);
				} else {
					return true;
				}
			} else {
				i_1_ = interface6.getClientVarp(anInt243, 63);
			}
			return !((i_1_ ^ 0xffffffff) > (anInt254 ^ 0xffffffff) || (i_1_ ^ 0xffffffff) < (anInt236 ^ 0xffffffff));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bq.G(" + i + ',' + (interface6 != null ? "{...}" : "null") + ')');
		}
	}

	public final int method285(int i, int i_2_, int i_3_) {
		try {
			if (aClass377_256 == null) {
				return i_3_;
			}
			if (i < 47) {
				method289(67, null, 54);
			}
			NodeInteger class98_sub34 = (NodeInteger) aClass377_256.get(i_2_, -1);
			if (class98_sub34 == null) {
				return i_3_;
			}
			return class98_sub34.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bq.D(" + i + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public final Sprite method286(RSToolkit var_ha, byte i) {
		try {
			Sprite class332 = (Sprite) parser.aClass79_2857.get(-119, 0x20000 | anInt231 | var_ha.id << 947659261);
			if (class332 != null) {
				return class332;
			}
			parser.aClass207_2852.isFileCached(-84, anInt231);
			Image class324 = Image.loadImage(parser.aClass207_2852, anInt231, 0);
			if (class324 != null) {
				class332 = var_ha.createSprite(class324, true);
				parser.aClass79_2857.put(anInt231 | 0x20000 | var_ha.id << 160002877, class332, (byte) -80);
			}
			if (i != 92) {
				method290(null, -73);
			}
			return class332;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bq.B(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final Sprite getSprite(byte i, RSToolkit toolkit, boolean bool) {
		try {
			int i_4_ = bool ? anInt225 : anInt245;
			int toolkitId = toolkit.id << 548917245 | i_4_;
			if (i != 92) {
				anInt257 = 107;
			}
			Sprite sprite = (Sprite) parser.aClass79_2857.get(i + -214, toolkitId);
			if (sprite != null) {
				return sprite;
			}
			if (!parser.aClass207_2852.isFileCached(-52, i_4_)) {
				return null;
			}
			Image image = Image.loadImage(parser.aClass207_2852, i_4_, 0);
			if (image != null) {
				sprite = toolkit.createSprite(image, true);
				parser.aClass79_2857.put(toolkitId, sprite, (byte) -80);
			}
			return sprite;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bq.H(" + i + ',' + (toolkit != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	private final void method288(int i, int i_6_, RSByteBuffer class98_sub22) {
		try {
			if (i == 27177) {
				if (i_6_ != 1) {
					if ((i_6_ ^ 0xffffffff) != -3) {
						if (i_6_ != 3) {
							if (i_6_ != 4) {
								if (i_6_ != 5) {
									if ((i_6_ ^ 0xffffffff) != -7) {
										if (i_6_ != 7) {
											if (i_6_ == 8) {
												aBoolean261 = class98_sub22.readUnsignedByte((byte) 113) == 1;
											} else if ((i_6_ ^ 0xffffffff) == -10) {
												anInt259 = class98_sub22.readShort((byte) 127);
												if (anInt259 == 65535) {
													anInt259 = -1;
												}
												anInt260 = class98_sub22.readShort((byte) 127);
												if ((anInt260 ^ 0xffffffff) == -65536) {
													anInt260 = -1;
												}
												anInt251 = class98_sub22.readInt(-2);
												anInt227 = class98_sub22.readInt(-2);
											} else if (i_6_ >= 10 && i_6_ <= 14) {
												aStringArray237[-10 + i_6_] = class98_sub22.readString((byte) 84);
											} else if ((i_6_ ^ 0xffffffff) != -16) {
												if ((i_6_ ^ 0xffffffff) == -17) {
													aBoolean241 = false;
												} else if (i_6_ != 17) {
													if (i_6_ == 18) {
														anInt231 = class98_sub22.readShort((byte) 127);
													} else if ((i_6_ ^ 0xffffffff) != -20) {
														if ((i_6_ ^ 0xffffffff) == -21) {
															anInt240 = class98_sub22.readShort((byte) 127);
															if ((anInt240 ^ 0xffffffff) == -65536) {
																anInt240 = -1;
															}
															anInt243 = class98_sub22.readShort((byte) 127);
															if (anInt243 == 65535) {
																anInt243 = -1;
															}
															anInt254 = class98_sub22.readInt(-2);
															anInt236 = class98_sub22.readInt(-2);
														} else if ((i_6_ ^ 0xffffffff) != -22) {
															if ((i_6_ ^ 0xffffffff) != -23) {
																if (i_6_ != 23) {
																	if ((i_6_ ^ 0xffffffff) == -25) {
																		anInt235 = class98_sub22.readUShort(false);
																		anInt252 = class98_sub22.readUShort(false);
																	} else if ((i_6_ ^ 0xffffffff) == -250) {
																		int i_7_ = class98_sub22.readUnsignedByte((byte) -115);
																		if (aClass377_256 == null) {
																			int i_8_ = Class48.findNextGreaterPwr2(423660257, i_7_);
																			aClass377_256 = new HashTable(i_8_);
																		}
																		for (int i_9_ = 0; i_9_ < i_7_; i_9_++) {
																			boolean bool = class98_sub22.readUnsignedByte((byte) 76) == 1;
																			int i_10_ = class98_sub22.readMediumInt(i ^ ~0x6a52);
																			Node class98;
																			if (bool) {
																				class98 = new NodeString(class98_sub22.readString((byte) 84));
																			} else {
																				class98 = new NodeInteger(class98_sub22.readInt(-2));
																			}
																			aClass377_256.put(class98, i_10_, -1);
																		}
																	}
																} else {
																	anInt250 = class98_sub22.readUnsignedByte((byte) -3);
																	anInt253 = class98_sub22.readUnsignedByte((byte) -109);
																	anInt224 = class98_sub22.readUnsignedByte((byte) -116);
																}
															} else {
																anInt226 = class98_sub22.readInt(-2);
															}
														} else {
															anInt239 = class98_sub22.readInt(-2);
														}
													} else {
														anInt246 = class98_sub22.readShort((byte) 127);
													}
												} else {
													aString232 = class98_sub22.readString((byte) 84);
												}
											} else {
												int i_11_ = class98_sub22.readUnsignedByte((byte) -124);
												anIntArray265 = new int[i_11_ * 2];
												for (int i_12_ = 0; (2 * i_11_ ^ 0xffffffff) < (i_12_ ^ 0xffffffff); i_12_++) {
													anIntArray265[i_12_] = class98_sub22.readUShort(false);
												}
												anInt249 = class98_sub22.readInt(-2);
												int i_13_ = class98_sub22.readUnsignedByte((byte) 67);
												anIntArray234 = new int[i_13_];
												for (int i_14_ = 0; (anIntArray234.length ^ 0xffffffff) < (i_14_ ^ 0xffffffff); i_14_++) {
													anIntArray234[i_14_] = class98_sub22.readInt(i + -27179);
												}
												aByteArray229 = new byte[i_11_];
												for (int i_15_ = 0; (i_15_ ^ 0xffffffff) > (i_11_ ^ 0xffffffff); i_15_++) {
													aByteArray229[i_15_] = class98_sub22.readSignedByte((byte) -19);
												}
											}
										} else {
											int i_16_ = class98_sub22.readUnsignedByte((byte) 19);
											if ((0x2 & i_16_) == 2) {
												morphisms = true;
											}
											if ((0x1 & i_16_) == 0) {
												aBoolean258 = false;
											}
										}
									} else {
										anInt264 = class98_sub22.readUnsignedByte((byte) 48);
									}
								} else {
									anInt238 = class98_sub22.readMediumInt(-128);
								}
							} else {
								anInt257 = class98_sub22.readMediumInt(-124);
							}
						} else {
							locationName = class98_sub22.readString((byte) 84);
						}
					} else {
						anInt225 = class98_sub22.readShort((byte) 127);
					}
				} else {
					anInt245 = class98_sub22.readShort((byte) 127);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bq.E(" + i + ',' + i_6_ + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	public final String method289(int i, String string, int i_17_) {
		try {
			if (aClass377_256 == null) {
				return string;
			}
			NodeString class98_sub15 = (NodeString) aClass377_256.get(i_17_, -1);
			if (class98_sub15 == null) {
				return string;
			}
			if (i != -5911) {
				anInt248 = 76;
			}
			return class98_sub15.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bq.A(" + i + ',' + (string != null ? "{...}" : "null") + ',' + i_17_ + ')');
		}
	}

	public final void method290(RSByteBuffer class98_sub22, int i) {
		do {
			try {
				for (;;) {
					int i_18_ = class98_sub22.readUnsignedByte((byte) 115);
					if ((i_18_ ^ 0xffffffff) == -1) {
						break;
					}
					method288(i + 27172, i_18_, class98_sub22);
				}
				if (i == 5) {
					break;
				}
				method289(-65, null, 33);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "bq.F(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final void method291(int i) {
		do {
			try {
				if (anIntArray265 != null) {
					for (int i_19_ = 0; (anIntArray265.length ^ 0xffffffff) < (i_19_ ^ 0xffffffff); i_19_ += 2) {
						if (anIntArray265[i_19_] >= anInt244) {
							if (anIntArray265[i_19_] > anInt247) {
								anInt247 = anIntArray265[i_19_];
							}
						} else {
							anInt244 = anIntArray265[i_19_];
						}
						if ((anIntArray265[1 + i_19_] ^ 0xffffffff) <= (anInt248 ^ 0xffffffff)) {
							if (anInt262 < anIntArray265[i_19_ - -1]) {
								anInt262 = anIntArray265[i_19_ + 1];
							}
						} else {
							anInt248 = anIntArray265[i_19_ + 1];
						}
					}
				}
				if (i == -25798) {
					break;
				}
				method289(10, null, -15);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "bq.C(" + i + ')');
			}
			break;
		} while (false);
	}
}

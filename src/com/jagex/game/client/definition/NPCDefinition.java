/* Class141 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.AnimationSkeletonSet;
import com.ArrayUtils;
import com.Class119_Sub4;
import com.Class140;
import com.Class160;
import com.Billboard;
import com.Class2;
import com.Class226;
import com.Class251;
import com.Class254;
import com.Class256_Sub1;
import com.Class265;
import com.Class275;
import com.Class284_Sub2_Sub2;
import com.Class287_Sub2;
import com.Class290;
import com.Class319;
import com.Class340;
import com.Class357;
import com.Class39;
import com.Class40;
import com.Class48;
import com.Class65;
import com.Class76;
import com.Class76_Sub2;
import com.Class76_Sub7;
import com.Class98_Sub10_Sub26;
import com.Class98_Sub31_Sub4;
import com.Class98_Sub50;
import com.Class98_Sub6;
import com.ClipMap;
import com.DummyOutputStream;
import com.GameDefinition;
import com.GameShell;
import com.Image;
import com.BaseModel;
import com.NodeInteger;
import com.NodeString;
import com.OutgoingPacket;
import com.ProceduralTextureSource;
import com.RSByteBuffer;
import com.RSToolkit;
import com.RtInterfaceClip;
import com.Sprite;
import com.SystemInformation;
import com.VarValues;
import com.client;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.AnimationDefinitionParser;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.NPCDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.definition.parser.StructsDefinitionParser;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.preferences.FogPreferenceField;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessage;
import com.jagex.game.client.ui.loading.impl.elements.config.ProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.NewsLoadingScreenElement;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.matrix.JavaMatrix;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class NPCDefinition {
	public static float aFloat1150;

	public static final void loadSprites(RSToolkit var_ha, byte i, Js5 class207) {
		try {
			Image[] class324s = Image.loadImages(class207, RtInterfaceClip.hitmarksId, 0);
			Class287_Sub2.aClass332Array3275 = new Sprite[class324s.length];
			for (int i_107_ = 0; class324s.length > i_107_; i_107_++) {
				Class287_Sub2.aClass332Array3275[i_107_] = var_ha.createSprite(class324s[i_107_], true);
			}
			class324s = Image.loadImages(class207, Class140.hitbarDefaultId, 0);
			Class254.aClass332Array1943 = new Sprite[class324s.length];
			for (int i_108_ = 0; i_108_ < class324s.length; i_108_++) {
				Class254.aClass332Array1943[i_108_] = var_ha.createSprite(class324s[i_108_], true);
			}
			class324s = Image.loadImages(class207, Class65.timerBarDefaultId, 0);
			Billboard.aClass332Array1382 = new Sprite[class324s.length];
			for (int i_109_ = 0; (class324s.length ^ 0xffffffff) < (i_109_ ^ 0xffffffff); i_109_++) {
				Billboard.aClass332Array1382[i_109_] = var_ha.createSprite(class324s[i_109_], true);
			}
			class324s = Image.loadImages(class207, ProceduralTextureSource.headiconsPkId, 0);
			Class119_Sub4.aClass332Array4739 = new Sprite[class324s.length];
			for (int i_110_ = 0; class324s.length > i_110_; i_110_++) {
				Class119_Sub4.aClass332Array4739[i_110_] = var_ha.createSprite(class324s[i_110_], true);
			}
			class324s = Image.loadImages(class207, Class251.headiconPrayerId, 0);
			Class2.aClass332Array72 = new Sprite[class324s.length];
			for (int i_111_ = 0; (class324s.length ^ 0xffffffff) < (i_111_ ^ 0xffffffff); i_111_++) {
				Class2.aClass332Array72[i_111_] = var_ha.createSprite(class324s[i_111_], true);
			}
			class324s = Image.loadImages(class207, Class319.hintHeadiconsId, 0);
			EnumDefinition.aClass332Array2557 = new Sprite[class324s.length];
			for (int i_112_ = 0; i_112_ < class324s.length; i_112_++) {
				EnumDefinition.aClass332Array2557[i_112_] = var_ha.createSprite(class324s[i_112_], true);
			}
			class324s = Image.loadImages(class207, Class76_Sub2.hintMapmarkersId, 0);
			QuickChatMessage.aClass332Array6032 = new Sprite[class324s.length];
			for (int i_113_ = 0; (class324s.length ^ 0xffffffff) < (i_113_ ^ 0xffffffff); i_113_++) {
				QuickChatMessage.aClass332Array6032[i_113_] = var_ha.createSprite(class324s[i_113_], true);
			}
			class324s = Image.loadImages(class207, Class226.mapflagId, 0);
			Class340.mapFlag = new Sprite[class324s.length];
			for (int i_114_ = 0; (class324s.length ^ 0xffffffff) < (i_114_ ^ 0xffffffff); i_114_++) {
				Class340.mapFlag[i_114_] = var_ha.createSprite(class324s[i_114_], true);
			}
			class324s = Image.loadImages(class207, Class39.crossId, 0);
			Class76_Sub7.aClass332Array3764 = new Sprite[class324s.length];
			for (int i_115_ = 0; (i_115_ ^ 0xffffffff) > (class324s.length ^ 0xffffffff); i_115_++) {
				Class76_Sub7.aClass332Array3764[i_115_] = var_ha.createSprite(class324s[i_115_], true);
			}
			class324s = Image.loadImages(class207, DummyOutputStream.mapdotsId, 0);
			ProgressBarLSEConfig.mapDots = new Sprite[class324s.length];
			for (int i_116_ = 0; (i_116_ ^ 0xffffffff) > (class324s.length ^ 0xffffffff); i_116_++) {
				ProgressBarLSEConfig.mapDots[i_116_] = var_ha.createSprite(class324s[i_116_], true);
			}
			class324s = Image.loadImages(class207, ClipMap.scrollbarId, 0);
			FogPreferenceField.aClass332Array3675 = new Sprite[class324s.length];
			for (int i_117_ = 0; i_117_ < class324s.length; i_117_++) {
				FogPreferenceField.aClass332Array3675[i_117_] = var_ha.createSprite(class324s[i_117_], true);
			}
			class324s = Image.loadImages(class207, Class98_Sub31_Sub4.nameiconsId, 0);
			OrthoZoomPreferenceField.nameIcons = new Sprite[class324s.length];
			for (int i_119_ = 0; class324s.length > i_119_; i_119_++) {
				OrthoZoomPreferenceField.nameIcons[i_119_] = var_ha.createSprite(class324s[i_119_], true);
			}
			NewsLoadingScreenElement.aClass332_3471 = var_ha.createSprite(Image.loadImage(class207, JavaMatrix.compassId, 0), true);
			Class284_Sub2_Sub2.aClass332_6199 = var_ha.createSprite(Image.loadImage(class207, StructsDefinitionParser.otherlevelId, 0), true);
			class324s = Image.loadImages(class207, Class76.hintMapedgeId, 0);
			GameObjectDefinition.aClass332Array3000 = new Sprite[class324s.length];
			for (int i_120_ = 0; i_120_ < class324s.length; i_120_++) {
				GameObjectDefinition.aClass332Array3000[i_120_] = var_ha.createSprite(class324s[i_120_], true);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.D(" + (var_ha != null ? "{...}" : "null") + ',' + i + ',' + (class207 != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method2294(int i) {
		do {
			if (client.preferences.aClass64_Sub3_4076.getValue((byte) 125) == 0 && SunDefinitionParser.anInt963 != Font.localPlane) {
				Class251.updateMapArea(i + -6547, Class275.centreY, false, Class160.centreX, 11);
			} else {
				client.method103(i, client.graphicsToolkit);
				if (SystemInformation.anInt1170 == Font.localPlane) {
					break;
				}
				Js5Client.method2264((byte) -94);
			}
			break;
		} while (false);
	}

	public static final short[] method2304(byte i, short[] is) {
		try {
			if (i < 109) {
				method2294(72);
			}
			if (is == null) {
				return null;
			}
			short[] is_121_ = new short[is.length];
			ArrayUtils.arrayCopy(is, 0, is_121_, 0, is.length);
			return is_121_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.H(" + i + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	public boolean				aBoolean1093;
	public boolean				aBoolean1106;
	public boolean				visibleOnMinimap;
	public boolean				aBoolean1126;
	public boolean				aBoolean1130;
	public boolean				aBoolean1149;
	public boolean				aBoolean1153;
	public byte					aByte1103;
	private byte				aByte1111;
	private byte				aByte1119;
	public byte					aByte1122;
	public byte					aByte1129;
	public byte					aByte1134;
	private byte				aByte1138;
	private byte				aByte1139;
	public byte					aByte1141;
	private byte[]				aByteArray1136;
	public NPCDefinitionParser	aClass301_1133;
	private int					ambience;
	public int					anInt1090;
	public int					anInt1091;
	public int					anInt1092;
	public int					armyIcon;
	public int					anInt1096;
	public int					npcId;
	public int					anInt1099;
	public int					anInt1100;
	public int					anInt1101;
	public int					anInt1102;
	public int					anInt1110;
	public int					anInt1113;
	public int					anInt1114;
	public int					anInt1118;
	public int					anInt1120;
	public int					anInt1123;
	public int					anInt1125;
	public int					anInt1127;
	public int					anInt1128;
	private int					anInt1131;
	public int					anInt1132;
	public int					anInt1143;
	public int					renderId;			// durp
	private int					anInt1146;
	public int					anInt1147;
	private int					anInt1151;
	public int					anInt1154;
	public int					anInt1156;
	public int[]				anIntArray1109;
	public int[]				anIntArray1117;
	public int[]				anIntArray1152;
	private int[][]				anIntArrayArray1124;
	public short				aShort1094	= 0;
	public short				aShort1135;
	public short[]				aShortArray1105;
	private short[]				aShortArray1108;
	public short[]				aShortArray1137;
	private short[]				aShortArray1155;
	public int					boundSize;
	public int					combatLevel;
	public boolean				drawMapDot;
	public int[]				models;
	public String				name;
	public String[]				options;

	private HashTable			parameters;

	private int					scaleXY;

	private int					scaleY;

	public NPCDefinition() {
		anInt1092 = -1;
		ambience = 0;
		boundSize = 1;
		aByte1122 = (byte) -96;
		anInt1128 = 0;
		anInt1101 = 256;
		aBoolean1130 = true;
		aByte1129 = (byte) -1;
		aByte1103 = (byte) 0;
		anInt1113 = -1;
		armyIcon = -1;
		anInt1125 = 0;
		anInt1123 = 0;
		anInt1118 = -1;
		anInt1099 = -1;
		combatLevel = -1;
		aBoolean1126 = true;
		visibleOnMinimap = true;
		anInt1127 = -1;
		anInt1131 = 0;
		scaleXY = 128;
		anInt1114 = -1;
		anInt1143 = -1;
		aShort1135 = (short) 0;
		scaleY = 128;
		name = "null";
		aBoolean1149 = false;
		aByte1141 = (byte) 4;
		drawMapDot = true;
		anInt1096 = -1;
		anInt1090 = 256;
		aByte1134 = (byte) -16;
		anInt1120 = -1;
		anInt1110 = -1;
		anInt1091 = 32;
		aBoolean1106 = false;
		renderId = -1;
		anInt1102 = -1;
		anInt1100 = -1;
		options = new String[5];
		anInt1132 = -1;
		anInt1151 = -1;
		anInt1146 = -1;
		anInt1147 = -1;
		aByte1138 = (byte) 0;
		anInt1154 = -1;
		aBoolean1153 = false;
		anInt1156 = 255;
	}

	private final void method2293(int i, RSByteBuffer buffer, int opcode) {
		if ((opcode ^ 0xffffffff) == -2) {
			int count = buffer.readUnsignedByte((byte) 35);
			models = new int[count];
			for (int index = 0; count > index; index++) {
				models[index] = buffer.readShort((byte) 127);
				if (models[index] == 65535) {
					models[index] = -1;
				}
			}
		} else if ((opcode ^ 0xffffffff) != -3) {
			if (opcode == 12) {
				boundSize = buffer.readUnsignedByte((byte) -101);
			} else if (opcode < 30 || (opcode ^ 0xffffffff) <= -36) {
				if (opcode == 40) {
					int i_3_ = buffer.readUnsignedByte((byte) 23);
					aShortArray1105 = new short[i_3_];
					aShortArray1108 = new short[i_3_];
					for (int i_4_ = 0; (i_4_ ^ 0xffffffff) > (i_3_ ^ 0xffffffff); i_4_++) {
						aShortArray1108[i_4_] = (short) buffer.readShort((byte) 127);
						aShortArray1105[i_4_] = (short) buffer.readShort((byte) 127);
					}
				} else if ((opcode ^ 0xffffffff) == -42) {
					int i_5_ = buffer.readUnsignedByte((byte) -111);
					aShortArray1155 = new short[i_5_];
					aShortArray1137 = new short[i_5_];
					for (int i_6_ = 0; i_6_ < i_5_; i_6_++) {
						aShortArray1155[i_6_] = (short) buffer.readShort((byte) 127);
						aShortArray1137[i_6_] = (short) buffer.readShort((byte) 127);
					}
				} else if ((opcode ^ 0xffffffff) == -43) {
					int i_7_ = buffer.readUnsignedByte((byte) 84);
					aByteArray1136 = new byte[i_7_];
					for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
						aByteArray1136[i_8_] = buffer.readSignedByte((byte) -19);
					}
				} else if (opcode == 60) {
					int i_9_ = buffer.readUnsignedByte((byte) 108);
					anIntArray1117 = new int[i_9_];
					for (int i_10_ = 0; i_10_ < i_9_; i_10_++) {
						anIntArray1117[i_10_] = buffer.readShort((byte) 127);
					}
				} else if ((opcode ^ 0xffffffff) != -94) {
					if (opcode == 95) {
						combatLevel = buffer.readShort((byte) 127);
					} else if ((opcode ^ 0xffffffff) != -98) {
						if (opcode == 98) {
							scaleY = buffer.readShort((byte) 127);
						} else if (opcode != 99) {
							if (opcode != 100) {
								if ((opcode ^ 0xffffffff) == -102) {
									anInt1131 = buffer.readSignedByte((byte) -19) * 5;
								} else if (opcode == 102) {
									anInt1113 = buffer.readShort((byte) 127);
								} else if (opcode != 103) {
									if (opcode == 106 || (opcode ^ 0xffffffff) == -119) {
										anInt1146 = buffer.readShort((byte) 127);
										if (anInt1146 == 65535) {
											anInt1146 = -1;
										}
										anInt1151 = buffer.readShort((byte) 127);
										if (anInt1151 == 65535) {
											anInt1151 = -1;
										}
										int i_11_ = -1;
										if (opcode == 118) {
											i_11_ = buffer.readShort((byte) 127);
											if ((i_11_ ^ 0xffffffff) == -65536) {
												i_11_ = -1;
											}
										}
										int i_12_ = buffer.readUnsignedByte((byte) 116);
										anIntArray1109 = new int[i_12_ - -2];
										for (int i_13_ = 0; i_13_ <= i_12_; i_13_++) {
											anIntArray1109[i_13_] = buffer.readShort((byte) 127);
											if ((anIntArray1109[i_13_] ^ 0xffffffff) == -65536) {
												anIntArray1109[i_13_] = -1;
											}
										}
										anIntArray1109[1 + i_12_] = i_11_;
									} else if (opcode == 107) {
										visibleOnMinimap = false;
									} else if ((opcode ^ 0xffffffff) != -110) {
										if (opcode != 111) {
											if ((opcode ^ 0xffffffff) != -114) {
												if ((opcode ^ 0xffffffff) == -115) {
													aByte1122 = buffer.readSignedByte((byte) -19);
													aByte1134 = buffer.readSignedByte((byte) -19);
												} else if ((opcode ^ 0xffffffff) == -120) {
													aByte1103 = buffer.readSignedByte((byte) -19);
												} else if (opcode == 121) {
													anIntArrayArray1124 = new int[models.length][];
													int i_14_ = buffer.readUnsignedByte((byte) -126);
													for (int i_15_ = 0; i_15_ < i_14_; i_15_++) {
														int i_16_ = buffer.readUnsignedByte((byte) 92);
														int[] is = anIntArrayArray1124[i_16_] = new int[3];
														is[0] = buffer.readSignedByte((byte) -19);
														is[1] = buffer.readSignedByte((byte) -19);
														is[2] = buffer.readSignedByte((byte) -19);
													}
												} else if (opcode == 122) {
													anInt1127 = buffer.readShort((byte) 127);
												} else if (opcode == 123) {
													anInt1092 = buffer.readShort((byte) 127);
												} else if (opcode == 125) {
													aByte1141 = buffer.readSignedByte((byte) -19);
												} else if (opcode == 127) {
													renderId = buffer.readShort((byte) 127);
												} else if (opcode == 128) {
													buffer.readUnsignedByte((byte) 45);
												} else if ((opcode ^ 0xffffffff) == -135) {
													anInt1120 = buffer.readShort((byte) 127);
													if ((anInt1120 ^ 0xffffffff) == -65536) {
														anInt1120 = -1;
													}
													anInt1132 = buffer.readShort((byte) 127);
													if ((anInt1132 ^ 0xffffffff) == -65536) {
														anInt1132 = -1;
													}
													anInt1102 = buffer.readShort((byte) 127);
													if ((anInt1102 ^ 0xffffffff) == -65536) {
														anInt1102 = -1;
													}
													anInt1147 = buffer.readShort((byte) 127);
													if ((anInt1147 ^ 0xffffffff) == -65536) {
														anInt1147 = -1;
													}
													anInt1128 = buffer.readUnsignedByte((byte) 33);
												} else if (opcode == 135) {
													anInt1143 = buffer.readUnsignedByte((byte) -108);
													anInt1154 = buffer.readShort((byte) 127);
												} else if (opcode == 136) {
													anInt1114 = buffer.readUnsignedByte((byte) -98);
													anInt1110 = buffer.readShort((byte) 127);
												} else if ((opcode ^ 0xffffffff) != -138) {
													if (opcode == 138) {
														armyIcon = buffer.readShort((byte) 127);
													} else if (opcode != 139) {
														if ((opcode ^ 0xffffffff) != -141) {
															if ((opcode ^ 0xffffffff) == -142) {
																aBoolean1153 = true;
															} else if ((opcode ^ 0xffffffff) == -143) {
																anInt1118 = buffer.readShort((byte) 127);
															} else if (opcode != 143) {
																if ((opcode ^ 0xffffffff) <= -151 && opcode < 155) {
																	options[-150 + opcode] = buffer.readString((byte) 84);
																	if (!aClass301_1133.aBoolean2503) {
																		options[-150 + opcode] = null;
																	}
																} else if (opcode != 155) {
																	if ((opcode ^ 0xffffffff) == -159) {
																		aByte1129 = (byte) 1;
																	} else if (opcode != 159) {
																		if ((opcode ^ 0xffffffff) != -161) {
																			if (opcode != 162) {
																				if ((opcode ^ 0xffffffff) != -164) {
																					if ((opcode ^ 0xffffffff) == -165) {
																						anInt1101 = buffer.readShort((byte) 127);
																						anInt1090 = buffer.readShort((byte) 127);
																					} else if (opcode != 165) {
																						if (opcode != 168) {
																							if (opcode == 249) {
																								int count = buffer.readUnsignedByte((byte) 65);
																								if (parameters == null) {
																									int i_18_ = Class48.findNextGreaterPwr2(423660257, count);
																									parameters = new HashTable(i_18_);
																								}
																								for (int index = 0; index < count; index++) {
																									boolean bool = buffer.readUnsignedByte((byte) 34) == 1;
																									int i_20_ = buffer.readMediumInt(-125);
																									Node linkable;
																									if (!bool) {
																										linkable = new NodeInteger(buffer.readInt(-2));
																									} else {
																										linkable = new NodeString(buffer.readString((byte) 84));
																									}
																									parameters.put(linkable, i_20_, -1);
																								}
																							}
																						} else {
																							anInt1125 = buffer.readUnsignedByte((byte) 15);
																						}
																					} else {
																						anInt1123 = buffer.readUnsignedByte((byte) 108);
																					}
																				} else {
																					anInt1096 = buffer.readUnsignedByte((byte) 89);
																				}
																			} else {
																				aBoolean1093 = true;
																			}
																		} else {
																			int i_21_ = buffer.readUnsignedByte((byte) -108);
																			anIntArray1152 = new int[i_21_];
																			for (int i_22_ = 0; (i_22_ ^ 0xffffffff) > (i_21_ ^ 0xffffffff); i_22_++) {
																				anIntArray1152[i_22_] = buffer.readShort((byte) 127);
																			}
																		}
																	} else {
																		aByte1129 = (byte) 0;
																	}
																} else {
																	aByte1111 = buffer.readSignedByte((byte) -19);
																	aByte1139 = buffer.readSignedByte((byte) -19);
																	aByte1119 = buffer.readSignedByte((byte) -19);
																	aByte1138 = buffer.readSignedByte((byte) -19);
																}
															} else {
																aBoolean1149 = true;
															}
														} else {
															anInt1156 = buffer.readUnsignedByte((byte) -127);
														}
													} else {
														anInt1100 = buffer.readShort((byte) 127);
													}
												} else {
													anInt1099 = buffer.readShort((byte) 127);
												}
											} else {
												aShort1094 = (short) buffer.readShort((byte) 127);
												aShort1135 = (short) buffer.readShort((byte) 127);
											}
										} else {
											aBoolean1130 = false;
										}
									} else {
										aBoolean1126 = false;
									}
								} else {
									anInt1091 = buffer.readShort((byte) 127);
								}
							} else {
								ambience = buffer.readSignedByte((byte) -19);
							}
						} else {
							aBoolean1106 = true;
						}
					} else {
						scaleXY = buffer.readShort((byte) 127);
					}
				} else {
					drawMapDot = false;
				}
			} else {
				options[opcode + -30] = buffer.readString((byte) 84);
			}
		} else {
			name = buffer.readString((byte) 84);
		}
	}

	public final void method2295(byte i) {
		do {
			try {
				if (models == null) {
					models = new int[0];
				}
				if (aByte1129 == -1) {
					if (GameDefinition.RUNESCAPE == aClass301_1133.aClass279_2502) {
						aByte1129 = (byte) 1;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					aByte1129 = (byte) 0;
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "jl.K(" + i + ')');
			}
		} while (false);
	}

	public final boolean method2296(VarValues interface6, byte i) {
		try {
			if (anIntArray1109 == null) {
				return true;
			}
			int i_24_ = -1;
			do {
				if (anInt1146 != -1) {
					i_24_ = interface6.getClientVarpBit(anInt1146, 7373);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if ((anInt1151 ^ 0xffffffff) != 0) {
					i_24_ = interface6.getClientVarp(anInt1151, 122);
				}
			} while (false);
			if ((i_24_ ^ 0xffffffff) > -1 || i_24_ >= -1 + anIntArray1109.length || (anIntArray1109[i_24_] ^ 0xffffffff) == 0) {
				int i_25_ = anIntArray1109[anIntArray1109.length + -1];
				return i_25_ != -1;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.L(" + (interface6 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final void method2297(RSByteBuffer class98_sub22, boolean bool) {
		try {
			if (bool != true) {
				method2297(null, false);
			}
			for (;;) {
				int i = class98_sub22.readUnsignedByte((byte) -125);
				if (i == 0) {
					break;
				}
				method2293(0, class98_sub22, i);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.A(" + (class98_sub22 != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	public final String method2298(int i, int i_27_, String string) {
		try {
			if (i_27_ >= -14) {
				method2295((byte) -67);
			}
			if (parameters == null) {
				return string;
			}
			NodeString class98_sub15 = (NodeString) parameters.get(i, -1);
			if (class98_sub15 == null) {
				return string;
			}
			return class98_sub15.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.J(" + i + ',' + i_27_ + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	public final ModelRenderer method2299(AnimationDefinition class97, boolean bool, VarValues interface6, int i, int i_28_, AnimationDefinitionParser class183, int i_29_, RSToolkit var_ha, Class40 class40, int i_30_) {
		try {
			if (bool != false) {
				return null;
			}
			if (anIntArray1109 != null) {
				NPCDefinition class141_31_ = method2300(interface6, (byte) 78);
				if (class141_31_ == null) {
					return null;
				}
				return class141_31_.method2299(class97, false, interface6, i, i_28_, class183, i_29_, var_ha, class40, i_30_);
			}
			if (anIntArray1117 == null && (class40 == null || class40.anIntArray366 == null)) {
				return null;
			}
			int i_32_ = i_29_;
			if (class97 != null && (i_28_ ^ 0xffffffff) != 0) {
				i_32_ = i_32_ | class97.method932(true, i_28_, !bool, i_30_);
			}
			long l = var_ha.id << 431969328 | npcId;
			if (class40 != null) {
				l |= class40.aLong365 << -466776552;
			}
			ModelRenderer class146;
			synchronized (aClass301_1133.aClass79_2510) {
				class146 = (ModelRenderer) aClass301_1133.aClass79_2510.get(-119, l);
			}
			if (class146 == null || (class146.functionMask() & i_32_ ^ 0xffffffff) != (i_32_ ^ 0xffffffff)) {
				if (class146 != null) {
					i_32_ |= class146.functionMask();
				}
				int i_33_ = i_32_;
				int[] is = class40 != null && class40.anIntArray366 != null ? class40.anIntArray366 : anIntArray1117;
				boolean bool_34_ = false;
				synchronized (aClass301_1133.aClass207_2506) {
					for (int i_35_ = 0; (i_35_ ^ 0xffffffff) > (is.length ^ 0xffffffff); i_35_++) {
						if (!aClass301_1133.aClass207_2506.isFileCached(0, is[i_35_], -6329)) {
							bool_34_ = true;
						}
					}
				}
				if (bool_34_) {
					return null;
				}
				BaseModel[] class178s = new BaseModel[is.length];
				synchronized (aClass301_1133.aClass207_2506) {
					for (int i_36_ = 0; (i_36_ ^ 0xffffffff) > (is.length ^ 0xffffffff); i_36_++) {
						class178s[i_36_] = Class98_Sub6.fromFile(0, -9252, aClass301_1133.aClass207_2506, is[i_36_]);
					}
				}
				for (int i_37_ = 0; (is.length ^ 0xffffffff) < (i_37_ ^ 0xffffffff); i_37_++) {
					if (class178s[i_37_] != null && (class178s[i_37_].version ^ 0xffffffff) > -14) {
						class178s[i_37_].scaleLog2(13746, 2);
					}
				}
				BaseModel class178;
				if (class178s.length != 1) {
					class178 = new BaseModel(class178s, class178s.length);
				} else {
					class178 = class178s[0];
				}
				if (aShortArray1108 != null) {
					i_33_ |= 0x4000;
				}
				if (aShortArray1155 != null) {
					i_33_ |= 0x8000;
				}
				if ((aByte1138 ^ 0xffffffff) != -1) {
					i_33_ |= 0x80000;
				}
				class146 = var_ha.createModelRenderer(class178, i_33_, aClass301_1133.anInt2511, 64, 768);
				if (aShortArray1108 != null) {
					short[] is_38_;
					if (class40 == null || class40.aShortArray370 == null) {
						is_38_ = aShortArray1105;
					} else {
						is_38_ = class40.aShortArray370;
					}
					for (int i_39_ = 0; (i_39_ ^ 0xffffffff) > (aShortArray1108.length ^ 0xffffffff); i_39_++) {
						if (aByteArray1136 != null && aByteArray1136.length > i_39_) {
							class146.recolour(aShortArray1108[i_39_], Class265.clientPalette[0xff & aByteArray1136[i_39_]]);
						} else {
							class146.recolour(aShortArray1108[i_39_], is_38_[i_39_]);
						}
					}
				}
				if (aShortArray1155 != null) {
					short[] is_40_;
					if (class40 != null && class40.aShortArray368 != null) {
						is_40_ = class40.aShortArray368;
					} else {
						is_40_ = aShortArray1137;
					}
					for (int i_41_ = 0; i_41_ < aShortArray1155.length; i_41_++) {
						class146.retexture(aShortArray1155[i_41_], is_40_[i_41_]);
					}
				}
				if (aByte1138 != 0) {
					class146.method2337(aByte1111, aByte1139, aByte1119, 0xff & aByte1138);
				}
				class146.updateFunctionMask(i_32_);
				synchronized (aClass301_1133.aClass79_2510) {
					aClass301_1133.aClass79_2510.put(l, class146, (byte) -80);
				}
			}
			if (class97 != null && i_28_ != -1) {
				class146 = class97.method937(i_30_, i, i_32_, -94, class146, i_28_);
			}
			class146.updateFunctionMask(i_29_);
			return class146;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.M(" + (class97 != null ? "{...}" : "null") + ',' + bool + ',' + (interface6 != null ? "{...}" : "null") + ',' + i + ',' + i_28_ + ',' + (class183 != null ? "{...}" : "null") + ',' + i_29_ + ',' + (var_ha != null ? "{...}"
					: "null") + ',' + (class40 != null ? "{...}" : "null") + ',' + i_30_ + ')');
		}
	}

	public final NPCDefinition method2300(VarValues interface6, byte i) {
		try {
			int i_42_ = -1;
			do {
				if (anInt1146 != -1) {
					i_42_ = interface6.getClientVarpBit(anInt1146, 7373);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if (anInt1151 != -1) {
					i_42_ = interface6.getClientVarp(anInt1151, 28);
				}
			} while (false);
			if (i < 19) {
				return null;
			}
			if (i_42_ < 0 || (anIntArray1109.length - 1 ^ 0xffffffff) >= (i_42_ ^ 0xffffffff) || anIntArray1109[i_42_] == -1) {
				int i_43_ = anIntArray1109[-1 + anIntArray1109.length];
				if ((i_43_ ^ 0xffffffff) != 0) {
					return aClass301_1133.get(5, i_43_);
				}
				return null;
			}
			return aClass301_1133.get(5, anIntArray1109[i_42_]);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.B(" + (interface6 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final ModelRenderer method2301(int i, int i_44_, int[] is, int i_45_, byte i_46_, AnimationDefinition class97, Class226[] class226s, RSToolkit var_ha, int i_47_, int i_48_, VarValues interface6, Class40 class40, int i_49_, RenderAnimDefinitionParser class257, int i_50_, int i_51_,
			AnimationDefinitionParser class183, AnimationDefinition class97_52_) {
		try {
			if (anIntArray1109 != null) {
				NPCDefinition class141_53_ = method2300(interface6, (byte) 78);
				if (class141_53_ == null) {
					return null;
				}
				return class141_53_.method2301(i, i_44_, is, i_45_, (byte) 111, class97, class226s, var_ha, i_47_, i_48_, interface6, class40, i_49_, class257, i_50_, i_51_, class183, class97_52_);
			}
			int i_54_ = i_50_;
			if (scaleY != 128) {
				i_54_ |= 0x2;
			}
			if ((scaleXY ^ 0xffffffff) != -129) {
				i_54_ |= 0x5;
			}
			boolean bool = class97 != null || class97_52_ != null;
			boolean bool_55_ = false;
			boolean bool_56_ = false;
			boolean bool_57_ = false;
			boolean bool_58_ = false;
			int i_59_ = class226s != null ? class226s.length : 0;
			for (int i_60_ = 0; (i_60_ ^ 0xffffffff) > (i_59_ ^ 0xffffffff); i_60_++) {
				Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_60_] = null;
				if (class226s[i_60_] != null) {
					AnimationDefinition class97_61_ = class183.method2623(class226s[i_60_].anInt1700, 16383);
					if (class97_61_.frameIds != null) {
						bool = true;
						Class98_Sub50.aClass97Array4293[i_60_] = class97_61_;
						int i_62_ = class226s[i_60_].anInt1702;
						int i_63_ = class226s[i_60_].anInt1701;
						int i_64_ = class97_61_.frameIds[i_62_];
						Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_60_] = class183.method2624(2, i_64_ >>> -39687440);
						i_64_ &= 0xffff;
						Class290.anIntArray2198[i_60_] = i_64_;
						if (Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_60_] != null) {
							bool_56_ |= Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_60_].method1619(i_64_, 31239);
							bool_55_ |= Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_60_].method1617(false, i_64_);
							bool_58_ |= Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_60_].method1615(i_64_, false);
							bool_57_ |= class97_61_.aBoolean817;
						}
						if ((class97_61_.aBoolean825 || Class357.forcedTweening) && (i_63_ ^ 0xffffffff) != 0 && (class97_61_.frameIds.length ^ 0xffffffff) < (i_63_ ^ 0xffffffff)) {
							FloorOverlayDefinitionParser.anIntArray311[i_60_] = class97_61_.frameLengths[i_62_];
							Class256_Sub1.anIntArray5156[i_60_] = class226s[i_60_].anInt1707;
							int i_65_ = class97_61_.frameIds[i_63_];
							OutgoingPacket.aClass98_Sub46_Sub16Array3870[i_60_] = class183.method2624(2, i_65_ >>> -1998137744);
							i_65_ &= 0xffff;
							Class265.anIntArray1981[i_60_] = i_65_;
							if (OutgoingPacket.aClass98_Sub46_Sub16Array3870[i_60_] != null) {
								bool_56_ |= OutgoingPacket.aClass98_Sub46_Sub16Array3870[i_60_].method1619(i_65_, 31239);
								bool_55_ |= OutgoingPacket.aClass98_Sub46_Sub16Array3870[i_60_].method1617(false, i_65_);
								bool_58_ |= OutgoingPacket.aClass98_Sub46_Sub16Array3870[i_60_].method1615(i_65_, false);
							}
						} else {
							FloorOverlayDefinitionParser.anIntArray311[i_60_] = 0;
							Class256_Sub1.anIntArray5156[i_60_] = 0;
							OutgoingPacket.aClass98_Sub46_Sub16Array3870[i_60_] = null;
							Class265.anIntArray1981[i_60_] = -1;
						}
					}
				}
			}
			if (i_46_ <= 91) {
				return null;
			}
			int i_66_ = -1;
			int i_67_ = -1;
			int i_68_ = 0;
			AnimationSkeletonSet class98_sub46_sub16 = null;
			AnimationSkeletonSet class98_sub46_sub16_69_ = null;
			int i_70_ = -1;
			int i_71_ = -1;
			int i_72_ = 0;
			AnimationSkeletonSet class98_sub46_sub16_73_ = null;
			AnimationSkeletonSet class98_sub46_sub16_74_ = null;
			if (bool) {
				if (class97 != null) {
					i_66_ = class97.frameIds[i];
					int i_75_ = i_66_ >>> -329523440;
					i_66_ &= 0xffff;
					class98_sub46_sub16 = class183.method2624(2, i_75_);
					if (class98_sub46_sub16 != null) {
						bool_56_ |= class98_sub46_sub16.method1619(i_66_, 31239);
						bool_55_ |= class98_sub46_sub16.method1617(false, i_66_);
						bool_58_ |= class98_sub46_sub16.method1615(i_66_, false);
						bool_57_ |= class97.aBoolean817;
					}
					if ((class97.aBoolean825 || Class357.forcedTweening) && (i_48_ ^ 0xffffffff) != 0 && class97.frameIds.length > i_48_) {
						i_67_ = class97.frameIds[i_48_];
						i_68_ = class97.frameLengths[i];
						int i_76_ = i_67_ >>> -924031952;
						if (i_75_ != i_76_) {
							class98_sub46_sub16_69_ = class183.method2624(2, i_76_);
						} else {
							class98_sub46_sub16_69_ = class98_sub46_sub16;
						}
						i_67_ &= 0xffff;
						if (class98_sub46_sub16_69_ != null) {
							bool_56_ |= class98_sub46_sub16_69_.method1619(i_67_, 31239);
							bool_55_ |= class98_sub46_sub16_69_.method1617(false, i_67_);
							bool_58_ |= class98_sub46_sub16_69_.method1615(i_67_, false);
						}
					}
				}
				i_54_ |= 0x20;
				if (class97_52_ != null) {
					i_70_ = class97_52_.frameIds[i_45_];
					int i_77_ = i_70_ >>> 646999344;
					i_70_ &= 0xffff;
					class98_sub46_sub16_73_ = class183.method2624(2, i_77_);
					if (class98_sub46_sub16_73_ != null) {
						bool_56_ |= class98_sub46_sub16_73_.method1619(i_70_, 31239);
						bool_55_ |= class98_sub46_sub16_73_.method1617(false, i_70_);
						bool_58_ |= class98_sub46_sub16_73_.method1615(i_70_, false);
						bool_57_ |= class97_52_.aBoolean817;
					}
					if ((class97_52_.aBoolean825 || Class357.forcedTweening) && (i_51_ ^ 0xffffffff) != 0 && class97_52_.frameIds.length > i_51_) {
						i_72_ = class97_52_.frameLengths[i_45_];
						i_71_ = class97_52_.frameIds[i_51_];
						int i_78_ = i_71_ >>> -36661744;
						if ((i_78_ ^ 0xffffffff) != (i_77_ ^ 0xffffffff)) {
							class98_sub46_sub16_74_ = class183.method2624(2, i_78_);
						} else {
							class98_sub46_sub16_74_ = class98_sub46_sub16_73_;
						}
						i_71_ &= 0xffff;
						if (class98_sub46_sub16_74_ != null) {
							bool_56_ |= class98_sub46_sub16_74_.method1619(i_71_, 31239);
							bool_55_ |= class98_sub46_sub16_74_.method1617(false, i_71_);
							bool_58_ |= class98_sub46_sub16_74_.method1615(i_71_, false);
						}
					}
				}
				if (bool_56_) {
					i_54_ |= 0x80;
				}
				if (bool_55_) {
					i_54_ |= 0x100;
				}
				if (bool_57_) {
					i_54_ |= 0x200;
				}
				if (bool_58_) {
					i_54_ |= 0x400;
				}
			}
			long l = npcId | var_ha.id << -262111792;
			if (class40 != null) {
				l |= class40.aLong365 << -1822091880;
			}
			ModelRenderer class146;
			synchronized (aClass301_1133.aClass79_2509) {
				class146 = (ModelRenderer) aClass301_1133.aClass79_2509.get(-125, l);
			}
			RenderAnimDefinition class294 = null;
			if (renderId != -1) {
				class294 = class257.method3199(false, renderId);
			}
			if (class146 == null || (i_54_ & class146.functionMask()) != i_54_) {
				if (class146 != null) {
					i_54_ |= class146.functionMask();
				}
				int i_79_ = i_54_;
				int[] is_80_ = class40 != null && class40.anIntArray366 != null ? class40.anIntArray366 : models;
				boolean bool_81_ = false;
				synchronized (aClass301_1133.aClass207_2506) {
					for (int i_82_ = 0; i_82_ < is_80_.length; i_82_++) {
						if (is_80_[i_82_] != -1 && !aClass301_1133.aClass207_2506.isFileCached(0, is_80_[i_82_], -6329)) {
							bool_81_ = true;
						}
					}
				}
				if (bool_81_) {
					return null;
				}
				BaseModel[] class178s = new BaseModel[is_80_.length];
				for (int i_83_ = 0; i_83_ < is_80_.length; i_83_++) {
					if ((is_80_[i_83_] ^ 0xffffffff) != 0) {
						synchronized (aClass301_1133.aClass207_2506) {
							class178s[i_83_] = Class98_Sub6.fromFile(0, -9252, aClass301_1133.aClass207_2506, is_80_[i_83_]);
						}
						if (class178s[i_83_] != null) {
							if ((class178s[i_83_].version ^ 0xffffffff) > -14) {
								class178s[i_83_].scaleLog2(13746, 2);
							}
							if (anIntArrayArray1124 != null && anIntArrayArray1124[i_83_] != null) {
								class178s[i_83_].method2597(anIntArrayArray1124[i_83_][2], anIntArrayArray1124[i_83_][0], (byte) 89, anIntArrayArray1124[i_83_][1]);
							}
						}
					}
				}
				if (class294 != null && class294.anIntArrayArray2366 != null) {
					for (int i_84_ = 0; i_84_ < class294.anIntArrayArray2366.length; i_84_++) {
						if ((i_84_ ^ 0xffffffff) > (class178s.length ^ 0xffffffff) && class178s[i_84_] != null) {
							int i_85_ = 0;
							int i_86_ = 0;
							int i_87_ = 0;
							int i_88_ = 0;
							int i_89_ = 0;
							int i_90_ = 0;
							if (class294.anIntArrayArray2366[i_84_] != null) {
								i_85_ = class294.anIntArrayArray2366[i_84_][0];
								i_90_ = class294.anIntArrayArray2366[i_84_][5] << 591859491;
								i_86_ = class294.anIntArrayArray2366[i_84_][1];
								i_89_ = class294.anIntArrayArray2366[i_84_][4] << -915484413;
								i_87_ = class294.anIntArrayArray2366[i_84_][2];
								i_88_ = class294.anIntArrayArray2366[i_84_][3] << -1463601021;
							}
							if (i_88_ != 0 || i_89_ != 0 || (i_90_ ^ 0xffffffff) != -1) {
								class178s[i_84_].method2600(i_90_, i_88_, (byte) 117, i_89_);
							}
							if ((i_85_ ^ 0xffffffff) != -1 || i_86_ != 0 || (i_87_ ^ 0xffffffff) != -1) {
								class178s[i_84_].method2597(i_87_, i_85_, (byte) 60, i_86_);
							}
						}
					}
				}
				BaseModel class178;
				if ((class178s.length ^ 0xffffffff) == -2) {
					class178 = class178s[0];
				} else {
					class178 = new BaseModel(class178s, class178s.length);
				}
				if (aShortArray1108 != null) {
					i_79_ |= 0x4000;
				}
				if (aShortArray1155 != null) {
					i_79_ |= 0x8000;
				}
				if ((aByte1138 ^ 0xffffffff) != -1) {
					i_79_ |= 0x80000;
				}
				class146 = var_ha.createModelRenderer(class178, i_79_, aClass301_1133.anInt2511, 64 + ambience, anInt1131 + 850);
				if (aShortArray1108 != null) {
					short[] is_91_;
					if (class40 != null && class40.aShortArray370 != null) {
						is_91_ = class40.aShortArray370;
					} else {
						is_91_ = aShortArray1105;
					}
					for (int i_92_ = 0; aShortArray1108.length > i_92_; i_92_++) {
						if (aByteArray1136 != null && i_92_ < aByteArray1136.length) {
							class146.recolour(aShortArray1108[i_92_], Class265.clientPalette[aByteArray1136[i_92_] & 0xff]);
						} else {
							class146.recolour(aShortArray1108[i_92_], is_91_[i_92_]);
						}
					}
				}
				if (aShortArray1155 != null) {
					short[] is_93_;
					if (class40 == null || class40.aShortArray368 == null) {
						is_93_ = aShortArray1137;
					} else {
						is_93_ = class40.aShortArray368;
					}
					for (int i_94_ = 0; i_94_ < aShortArray1155.length; i_94_++) {
						class146.retexture(aShortArray1155[i_94_], is_93_[i_94_]);
					}
				}
				if ((aByte1138 ^ 0xffffffff) != -1) {
					class146.method2337(aByte1111, aByte1139, aByte1119, 0xff & aByte1138);
				}
				class146.updateFunctionMask(i_54_);
				synchronized (aClass301_1133.aClass79_2509) {
					aClass301_1133.aClass79_2509.put(l, class146, (byte) -80);
				}
			}
			ModelRenderer class146_95_ = class146.method2341((byte) 4, i_54_, true);
			boolean bool_96_ = false;
			if (is != null) {
				for (int i_97_ = 0; i_97_ < 12; i_97_++) {
					if ((is[i_97_] ^ 0xffffffff) != 0) {
						bool_96_ = true;
					}
				}
			}
			if (!bool && !bool_96_) {
				return class146_95_;
			}
			Matrix[] class111s = null;
			if (class294 != null) {
				class111s = class294.method3481(var_ha, (byte) -80);
			}
			if (bool_96_ && class111s != null) {
				for (int i_98_ = 0; i_98_ < 12; i_98_++) {
					if (class111s[i_98_] != null) {
						class146_95_.method2331(class111s[i_98_], 1 << i_98_, true);
					}
				}
			}
			int i_99_ = 0;
			int i_100_ = 1;
			for (/**/; (i_99_ ^ 0xffffffff) > (i_59_ ^ 0xffffffff); i_99_++) {
				if (Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_99_] != null) {
					class146_95_.method2323(Class290.anIntArray2198[i_99_], -1 + Class256_Sub1.anIntArray5156[i_99_], OutgoingPacket.aClass98_Sub46_Sub16Array3870[i_99_], -27033, i_100_, Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_99_], Class98_Sub50.aClass97Array4293[i_99_].aBoolean817,
							Class265.anIntArray1981[i_99_], null, 0, FloorOverlayDefinitionParser.anIntArray311[i_99_]);
				}
				i_100_ <<= 1;
			}
			if (bool_96_) {
				for (int i_101_ = 0; (i_101_ ^ 0xffffffff) > -13; i_101_++) {
					if ((is[i_101_] ^ 0xffffffff) != 0) {
						int i_102_ = -i_44_ + is[i_101_];
						i_102_ &= 0x3fff;
						Matrix class111 = var_ha.createMatrix();
						class111.method2101(i_102_);
						class146_95_.method2331(class111, 1 << i_101_, false);
					}
				}
			}
			if (bool_96_ && class111s != null) {
				for (int i_103_ = 0; i_103_ < 12; i_103_++) {
					if (class111s[i_103_] != null) {
						class146_95_.method2331(class111s[i_103_], 1 << i_103_, false);
					}
				}
			}
			if (class98_sub46_sub16 == null || class98_sub46_sub16_73_ == null) {
				if (class98_sub46_sub16 == null) {
					if (class98_sub46_sub16_73_ != null) {
						class146_95_.method2338(-1 + i_47_, class98_sub46_sub16_73_, i_70_, class98_sub46_sub16_74_, class97_52_.aBoolean817, 0, 126, i_72_, i_71_);
					}
				} else {
					class146_95_.method2338(i_49_ - 1, class98_sub46_sub16, i_66_, class98_sub46_sub16_69_, class97.aBoolean817, 0, 112, i_68_, i_67_);
				}
			} else {
				class146_95_.method2321(i_68_, i_66_, class98_sub46_sub16, class98_sub46_sub16_69_, class97.animationFlowControl, i_72_, 28777, i_70_, class98_sub46_sub16_73_, i_71_, i_49_ + -1, class97.aBoolean817 | class97_52_.aBoolean817, class98_sub46_sub16_74_, -1 + i_47_, i_67_);
			}
			for (int i_104_ = 0; (i_59_ ^ 0xffffffff) < (i_104_ ^ 0xffffffff); i_104_++) {
				Class98_Sub10_Sub26.aClass98_Sub46_Sub16Array5690[i_104_] = null;
				OutgoingPacket.aClass98_Sub46_Sub16Array3870[i_104_] = null;
				Class98_Sub50.aClass97Array4293[i_104_] = null;
			}
			if ((scaleXY ^ 0xffffffff) != -129 || scaleY != 128) {
				class146_95_.scale(scaleXY, scaleY, scaleXY);
			}
			class146_95_.updateFunctionMask(i_50_);
			return class146_95_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.C(" + i + ',' + i_44_ + ',' + (is != null ? "{...}" : "null") + ',' + i_45_ + ',' + i_46_ + ',' + (class97 != null ? "{...}" : "null") + ',' + (class226s != null ? "{...}" : "null") + ',' + (var_ha != null ? "{...}" : "null")
					+ ',' + i_47_ + ',' + i_48_ + ',' + (interface6 != null ? "{...}" : "null") + ',' + (class40 != null ? "{...}" : "null") + ',' + i_49_ + ',' + (class257 != null ? "{...}" : "null") + ',' + i_50_ + ',' + i_51_ + ',' + (class183 != null ? "{...}" : "null") + ','
					+ (class97_52_ != null ? "{...}" : "null") + ')');
		}
	}

	public final boolean method2302(byte i) {
		try {
			if (i < 16) {
				return false;
			}
			if (anIntArray1109 == null) {
				return !((anInt1120 ^ 0xffffffff) == 0 && (anInt1102 ^ 0xffffffff) == 0 && anInt1147 == -1);
			}
			for (int element : anIntArray1109) {
				if ((element ^ 0xffffffff) != 0) {
					NPCDefinition class141_106_ = aClass301_1133.get(5, element);
					if ((class141_106_.anInt1120 ^ 0xffffffff) != 0 || class141_106_.anInt1102 != -1 || (class141_106_.anInt1147 ^ 0xffffffff) != 0) {
						return true;
					}
				}
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.E(" + i + ')');
		}
	}

	public final int method2305(int i, int i_122_, byte i_123_) {
		try {
			if (i_123_ <= 113) {
				return 85;
			}
			if (parameters == null) {
				return i_122_;
			}
			NodeInteger class98_sub34 = (NodeInteger) parameters.get(i, -1);
			if (class98_sub34 == null) {
				return i_122_;
			}
			return class98_sub34.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jl.G(" + i + ',' + i_122_ + ',' + i_123_ + ')');
		}
	}
}

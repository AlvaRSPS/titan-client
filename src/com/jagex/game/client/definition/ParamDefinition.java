
/* Class149 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import com.Class151_Sub1;
import com.Class204;
import com.Class246_Sub3_Sub4;
import com.Class331;
import com.Class98_Sub10_Sub29;
import com.IncomingOpcode;
import com.OutgoingPacket;
import com.RSByteBuffer;
import com.client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.IdentikitDefinitionParser;
import com.jagex.game.client.preferences.SceneryShadowsPreferenceField;

public final class ParamDefinition {
	public static Class204					aClass204_1206	= new Class204();
	public static IncomingOpcode			SEND_MESSAGE	= new IncomingOpcode(53, -1);
	public static int						anInt1208;
	public static Calendar					GMT				= Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	public static IdentikitDefinitionParser	identikitDefinitionList;

	public static final void method2430(RSByteBuffer class98_sub22, byte i) {
		try {
			byte[] is = new byte[24];
			if (i == 0) {
				if (client.randomFile != null) {
					try {
						client.randomFile.seek(0L, 0);
						client.randomFile.method2842(is, i);
						int i_0_;
						for (i_0_ = 0; i_0_ < 24; i_0_++) {
							if ((is[i_0_] ^ 0xffffffff) != -1) {
								break;
							}
						}
						if (i_0_ >= 24) {
							throw new IOException();
						}
					} catch (Exception exception) {
						for (int i_1_ = 0; i_1_ < 24; i_1_++) {
							is[i_1_] = (byte) -1;
						}
					}
				}
				class98_sub22.method1217(is, 24, -1, 0);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kc.D(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static final void method2435(int i, int i_4_, int i_5_) {
		OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class151_Sub1.aClass171_4968, Class331.aClass117_2811);
		class98_sub11.packet.writeShortA(i, (byte) 126);
		class98_sub11.packet.writeLEInt(i_5_, 1046032984);
		Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
	}

	private char	aChar1201;

	public int		defaultInteger;

	public String	defaultString;

	public boolean	autoDisable	= true;

	public ParamDefinition() {
		/* empty */
	}

	public final boolean isString(boolean bool) {
		try {
			if (bool != false) {
				return false;
			}
			return (aChar1201 ^ 0xffffffff) == -116;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kc.C(" + bool + ')');
		}
	}

	public final void method2431(RSByteBuffer class98_sub22, int i) {
		try {
			if (i != -1) {
				identikitDefinitionList = null;
			}
			for (;;) {
				int i_2_ = class98_sub22.readUnsignedByte((byte) 5);
				if (i_2_ == 0) {
					break;
				}
				method2434(i_2_, class98_sub22, 1);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kc.A(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private final void method2434(int i, RSByteBuffer class98_sub22, int i_3_) {
		do {
			try {
				if (i_3_ == i) {
					aChar1201 = SceneryShadowsPreferenceField.method576(class98_sub22.readSignedByte((byte) -19), (byte) 127);
				} else if (i == 2) {
					defaultInteger = class98_sub22.readInt(-2);
				} else if (i == 4) {
					autoDisable = false;
				} else {
					if ((i ^ 0xffffffff) != -6) {
						break;
					}
					defaultString = class98_sub22.readString((byte) 84);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "kc.E(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_3_ + ')');
			}
			break;
		} while (false);
	}
}

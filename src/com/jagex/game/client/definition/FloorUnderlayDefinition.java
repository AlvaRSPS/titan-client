/* Class72 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.RSByteBuffer;

public final class FloorUnderlayDefinition {
	public boolean	aBoolean543		= true;
	public int		colour			= 0;
	public int		hue;
	public int		chroma;
	public int		lightness;
	public boolean	occlude			= true;
	public int		saturation;
	public int		textCoordScale	= 512;
	public int		textureId		= -1;

	public FloorUnderlayDefinition() {
		/* empty */
	}

	private final void decode(RSByteBuffer buffer, boolean bool, int opcode) {
		if ((opcode ^ 0xffffffff) != -2) {
			if (opcode == 2) {
				textureId = buffer.readShort((byte) 127);
				if ((textureId ^ 0xffffffff) == -65536) {
					textureId = -1;
				}
			} else if (opcode == 3) {
				textCoordScale = buffer.readShort((byte) 127) << -380251870;
			} else if (opcode != 4) {
				if (opcode == 5) {
					aBoolean543 = true;
				}
			} else {
				occlude = false;
			}
		} else {
			colour = buffer.readMediumInt(-126);
			rgbToHsl(0, colour);
		}
	}

	public final void decode(RSByteBuffer buffer, int i) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) 40);
			if (opcode == 0) {
				break;
			}
			this.decode(buffer, true, opcode);
		}
	}

	private final void rgbToHsl(int i, int rgb) {
		double r = (rgb >> -356542736 & 0xff) / 256.0;
		double g = ((rgb & 0xff10) >> -1757323064) / 256.0;
		double b = (0xff & rgb) / 256.0;
		double cmin = r;
		if (g < cmin) {
			cmin = g;
		}
		if (b < cmin) {
			cmin = b;
		}
		double cmax = r;
		if (g > cmax) {
			cmax = g;
		}
		if (cmax < b) {
			cmax = b;
		}
		double hue = 0.0;
		double saturation = 0.0;
		double lightness = (cmax + cmin) / 2.0;
		if (cmax != cmin) {
			if (lightness < 0.5) {
				saturation = (cmax - cmin) / (cmax + cmin);
			}
			if (r == cmax) {
				hue = (-b + g) / (cmax - cmin);
			} else if (g != cmax) {
				if (cmax == b) {
					hue = 4.0 + (r - g) / (cmax - cmin);
				}
			} else {
				hue = (-r + b) / (cmax - cmin) + 2.0;
			}
			if (lightness >= 0.5) {
				saturation = (-cmin + cmax) / (-cmax + 2.0 - cmin);
			}
		}
		hue /= 6.0;
		this.lightness = (int) (lightness * 256.0);
		this.saturation = (int) (saturation * 256.0);
		if ((this.lightness ^ 0xffffffff) <= -1) {
			if (this.lightness > 255) {
				this.lightness = 255;
			}
		} else {
			this.lightness = 0;
		}
		if (0 > this.saturation) {
			this.saturation = 0;
		} else if (this.saturation > 255) {
			this.saturation = 255;
		}
		if (!(lightness > 0.5)) {
			chroma = (int) (512.0 * (lightness * saturation));
		} else {
			chroma = (int) (saturation * (1.0 - lightness) * 512.0);
		}
		if (chroma < 1) {
			chroma = 1;
		}
		this.hue = (int) (hue * chroma);
	}
}

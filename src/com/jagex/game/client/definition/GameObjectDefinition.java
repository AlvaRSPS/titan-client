/* Class352 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.*;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsItem;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.shadow.Shadow;

import java.util.Arrays;

//GameObjectDefinition
public final class GameObjectDefinition {
	public static Sprite[]			aClass332Array3000;
	public static IncomingOpcode	aClass58_2943	= new IncomingOpcode(108, 2);
	public static IncomingOpcode	aClass58_2993	= new IncomingOpcode(41, 4);
	public static int[]				anIntArray3001;

	public static final boolean method2603(byte i, Interface19 interface19) {
		GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(interface19.method64(30472), (byte) 119);
		if ((class352.anInt2990 ^ 0xffffffff) == 0) {
			return true;
		}
		MapScenesDefinition class9 = Class98_Sub10_Sub23.mapScenesDefinitionList.method3766(class352.anInt2990, (byte) -73);
		if ((class9.anInt114 ^ 0xffffffff) == 0) {
			return true;
		}
		if (i != 76) {
			return true;
		}
		return class9.method191(-87);
	}

	public static final void method3856(byte i) {
		for (int i_37_ = 0; i_37_ < 5; i_37_++) {
			GroupProgressMonitor.aBooleanArray3410[i_37_] = false;
		}
		Class98_Sub10_Sub14.anInt5613 = Queue.timer;
		RtMouseListener.anInt2494 = Mob.anInt6357;
		Class98_Sub4.anInt3828 = 0;
		Class98_Sub46_Sub20_Sub2.cameraMode = 5;
		NPC.anInt6511 = Class98_Sub46_Sub10.xCamPosTile;
		Class96.anInt801 = Class186.cameraX;
		Class98_Sub41.anInt4202 = 0;
		Class98_Sub50.anInt4292 = SpriteLoadingScreenElement.yCamPosTile;
		NewsItem.anInt3128 = -1;
		Class53_Sub1.anInt3636 = -1;
		NewsFetcher.anInt3095 = AdvancedMemoryCache.anInt601;
		Class116.anInt967 = CharacterShadowsPreferenceField.anInt3712 = -1;
	}

	public boolean						aBoolean2925;
	public boolean						aBoolean2927;
	public boolean						aBoolean2935;
	public boolean						aBoolean2947;
	public boolean						aBoolean2957;
	public boolean						aBoolean2961;
	public boolean						aBoolean2969;
	public boolean						aBoolean2976;
	public boolean						aBoolean2982;
	public boolean						aBoolean2984;
	public boolean						aBoolean2992;
	public boolean						aBoolean3004;
	public boolean						aBoolean3005;
	public boolean						aBoolean3007;
	public byte							aByte2930;
	private byte						aByte2932;
	private byte						aByte2942;
	private byte						aByte2967;
	private byte						aByte2971;
	private byte[]						aByteArray2955;
	public byte[]						aByteArray2994;
	public GameObjectDefinitionParser parser;
	public HashTable					aClass377_2944;
	public int							actionCount;
	public int							anInt2929;
	public int							anInt2931	= 0;
	public int							anInt2933;
	public int							anInt2938;
	public int							anInt2940;
	public int							anInt2941;
	public int							anInt2945	= 0;
	public int							anInt2946;
	public int							anInt2948;
	public int							anInt2949;
	public int							anInt2950;
	public int							anInt2953;
	public int							anInt2954;
	public int							anInt2956;
	public int							anInt2962;
	public int							anInt2964;
	public int							anInt2966;
	public int							anInt2968;
	public int							anInt2970;
	public int							anInt2972;
	public int							anInt2973;
	public int							anInt2975;
	public int							anInt2977;
	public int							anInt2980;
	public int							anInt2981;
	public int							anInt2983;
	public int							anInt2985;
	public int							anInt2986;
	public int							anInt2987;
	public int							anInt2988;
	public int							anInt2989;
	public int							anInt2990;
	public int							anInt2996;
	public int							anInt2997;
	public int							anInt2998;
	public int							anInt3002;
	public int							anInt3006;
	public int							anInt3008;
	public int[]						anIntArray2926;
	public int[]						transformIDs;
	public int[]						anIntArray2934;
	public int[]						anIntArray2937;
	public int[]						anIntArray2979;
	public int[][]						anIntArrayArray2951;
	public short[]						aShortArray2965;
	public short[]						aShortArray2974;
	public short[]						aShortArray2995;
	public short[]						aShortArray3003;
	public boolean						clippingFlag;
	public int							id;
	public int							mapFunction;
	public String						name;
	public String[]						options;

	public int							sizeX;

	public int							sizeY;

	public boolean						walkable;

	public GameObjectDefinition() {
		anIntArray2937 = null;
		aBoolean2925 = true;
		anInt2941 = -1;
		anInt2949 = 0;
		aBoolean2935 = true;
		aBoolean2947 = true;
		anInt2929 = 128;
		aByte2932 = (byte) 0;
		anInt2954 = 128;
		aBoolean2957 = false;
		anInt2946 = 0;
		clippingFlag = false;
		aByte2971 = (byte) 0;
		aBoolean2927 = false;
		anInt2953 = 0;
		anInt2970 = 0;
		options = new String[5];
		anInt2962 = 0;
		anInt2966 = 64;
		anInt2968 = -1;
		anInt2933 = -1;
		anInt2973 = 0;
		anInt2940 = 0;
		anInt2977 = -1;
		sizeX = 1;
		anInt2983 = -1;
		name = "null";
		anInt2975 = -1;
		anInt2948 = 0;
		mapFunction = -1;
		aBoolean2976 = false;
		aBoolean2982 = false;
		anInt2938 = 128;
		anInt2988 = 0;
		sizeY = 1;
		aBoolean2984 = false;
		anInt2989 = 0;
		anInt2980 = 0;
		anIntArray2979 = null;
		anInt2990 = -1;
		aBoolean2992 = false;
		anInt2950 = 256;
		anInt2997 = 0;
		walkable = true;
		anInt2996 = -1;
		aBoolean2961 = false;
		anInt2986 = 960;
		anInt2964 = 0;
		anInt2972 = 0;
		aBoolean2969 = false;
		aBoolean3004 = false;
		anInt2985 = -1;
		aBoolean3005 = false;
		anInt2981 = 0;
		anInt2987 = 255;
		aBoolean3007 = false;
		anInt3002 = -1;
		actionCount = 2;
		anInt3006 = 256;
		anInt3008 = -1;
		anInt2956 = -1;
		anInt2998 = -1;
	}

	public final void decode(RSByteBuffer class98_sub22, boolean bool) {
		try {
			if (bool != false) {
				anIntArrayArray2951 = null;
			}
			for (;;) {
				int i = class98_sub22.readUnsignedByte((byte) 24);
				if (i == 0) {
					break;
				}
				method3863(class98_sub22, i, 7);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.F(" + (class98_sub22 != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	public final Class298 method3851(int i, boolean bool, Ground var_s, int i_0_, int i_1_, boolean bool_2_, int i_3_, int i_4_, Class185 class185, Ground var_s_5_, RSToolkit var_ha, int i_6_) {
		try {
			if (Ground.method3419(-95, i_6_)) {
				i_6_ = 4;
			}
			long l = i_0_ + (i_6_ << 300896803) + (id << -1555969174);
			if (bool != false) {
				method3857(-75);
			}
			l |= var_ha.id << -1993352131;
			if (class185 != null) {
				l |= class185.aLong1448 << -2073542368;
			}
			int i_7_ = i_4_;
			if (aByte2971 == 3) {
				i_7_ |= 0x7;
			} else {
				if ((aByte2971 ^ 0xffffffff) != -1 || anInt2988 != 0) {
					i_7_ |= 0x2;
				}
				if ((anInt2940 ^ 0xffffffff) != -1) {
					i_7_ |= 0x1;
				}
				if (anInt2989 != 0) {
					i_7_ |= 0x4;
				}
			}
			if (bool_2_) {
				i_7_ |= 0x40000;
			}
			Class298 class298;
			synchronized (parser.aClass79_2525) {
				class298 = (Class298) parser.aClass79_2525.get(-124, l);
			}
			ModelRenderer class146 = class298 != null ? class298.aClass146_2477 : null;
			Shadow var_r = null;
			if (class146 != null && (var_ha.c(class146.functionMask(), i_7_) ^ 0xffffffff) == -1) {
				var_r = class298.aR2479;
				class146 = class298.aClass146_2477;
				if (bool_2_ && var_r == null) {
					var_r = class298.aR2479 = class146.ba(null);
				}
			} else {
				if (class146 != null) {
					i_7_ = var_ha.mergeFunctionMask(i_7_, class146.functionMask());
				}
				int i_8_ = i_7_;
				if (i_6_ == 10 && (i_0_ ^ 0xffffffff) < -4) {
					i_8_ |= 0x5;
				}
				class146 = method3855(i_6_, i_0_, var_ha, i_8_, class185, (byte) -75);
				if (class146 == null) {
					return null;
				}
				if ((i_6_ ^ 0xffffffff) == -11 && i_0_ > 3) {
					class146.rotateYaw(2048);
				}
				if (bool_2_) {
					var_r = class146.ba(null);
				}
				class146.updateFunctionMask(i_7_);
				class298 = new Class298();
				class298.aR2479 = var_r;
				class298.aClass146_2477 = class146;
				synchronized (parser.aClass79_2525) {
					parser.aClass79_2525.put(l, class298, (byte) -80);
				}
			}
			boolean bool_9_ = (aByte2971 ^ 0xffffffff) != -1 && (var_s_5_ != null || var_s != null);
			boolean bool_10_ = anInt2940 != 0 || anInt2988 != 0 || (anInt2989 ^ 0xffffffff) != -1;
			if (!bool_9_ && !bool_10_) {
				class146 = class146.method2341((byte) 0, i_4_, true);
			} else {
				class146 = class146.method2341((byte) 0, i_7_, true);
				if (bool_9_) {
					class146.p(aByte2971, anInt2985, var_s_5_, var_s, i_3_, i_1_, i);
				}
				if (bool_10_) {
					class146.H(anInt2940, anInt2988, anInt2989);
				}
				class146.updateFunctionMask(i_4_);
			}
			Class224_Sub3_Sub1.aClass298_6145.aR2479 = var_r;
			Class224_Sub3_Sub1.aClass298_6145.aClass146_2477 = class146;
			return Class224_Sub3_Sub1.aClass298_6145;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.B(" + i + ',' + bool + ',' + (var_s != null ? "{...}" : "null") + ',' + i_0_ + ',' + i_1_ + ',' + bool_2_ + ',' + i_3_ + ',' + i_4_ + ',' + (class185 != null ? "{...}" : "null") + ',' + (var_s_5_ != null ? "{...}" : "null")
					+ ',' + (var_ha != null ? "{...}" : "null") + ',' + i_6_ + ')');
		}
	}

	public final GameObjectDefinition get(VarValues interface6, byte i) {
		try {
			if (i >= -37) {
				method3864((byte) 102, 45, null);
			}
			int i_11_ = -1;
			do {
				if (anInt2983 != -1) {
					i_11_ = interface6.getClientVarpBit(anInt2983, 7373);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if ((anInt2968 ^ 0xffffffff) != 0) {
					i_11_ = interface6.getClientVarp(anInt2968, -121);
				}
			} while (false);
			if (i_11_ < 0 || (~i_11_) <= (~transformIDs.length + -1) || (~transformIDs[i_11_]) == 0) {
				int i_12_ = transformIDs[transformIDs.length - 1];
				if ((i_12_ ^ 0xffffffff) != 0) {
					return parser.get(i_12_, (byte) 119);
				}
				return null;
			}
			return parser.get(transformIDs[i_11_], (byte) 119);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.K(" + (interface6 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final boolean method3853(byte i, int i_13_) {
		try {
			if (i != 49) {
				parser = null;
			}
			if (anIntArrayArray2951 == null) {
				return true;
			}
			synchronized (parser.aClass207_2512) {
				for (int i_14_ = 0; aByteArray2994.length > i_14_; i_14_++) {
					if ((i_13_ ^ 0xffffffff) == (aByteArray2994[i_14_] ^ 0xffffffff)) {
						for (int i_15_ = 0; anIntArrayArray2951[i_14_].length > i_15_; i_15_++) {
							if (!parser.aClass207_2512.isFileCached(0, anIntArrayArray2951[i_14_][i_15_], -6329)) {
								return false;
							}
						}
						return true;
					}
				}
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.L(" + i + ',' + i_13_ + ')');
		}
	}

	private final void method3854(RSByteBuffer class98_sub22, int i) {
		try {
			int i_16_ = class98_sub22.readUnsignedByte((byte) -113);
			for (int i_18_ = 0; i_18_ < i_16_; i_18_++) {
				class98_sub22.position++;
				int i_19_ = class98_sub22.readUnsignedByte((byte) 25);
				class98_sub22.position += 2 * i_19_;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.Q(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private final ModelRenderer method3855(int i, int i_20_, RSToolkit var_ha, int i_21_, Class185 class185, byte i_22_) {
		try {
			int i_23_ = 64 + anInt2931;
			int i_24_ = 850 + anInt2980;
			int i_25_ = i_21_;
			boolean bool = aBoolean2961 || (i ^ 0xffffffff) == -3 && i_20_ > 3;
			if (bool) {
				i_21_ |= 0x10;
			}
			if ((i_20_ ^ 0xffffffff) == -1) {
				if ((anInt2938 ^ 0xffffffff) != -129 || anInt2973 != 0) {
					i_21_ |= 0x1;
				}
				if ((anInt2929 ^ 0xffffffff) != -129 || anInt2946 != 0) {
					i_21_ |= 0x4;
				}
			} else {
				i_21_ |= 0xd;
			}
			if ((anInt2954 ^ 0xffffffff) != -129 || anInt2997 != 0) {
				i_21_ |= 0x2;
			}
			if (aShortArray3003 != null) {
				i_21_ |= 0x4000;
			}
			if (aShortArray2995 != null) {
				i_21_ |= 0x8000;
			}
			if ((aByte2932 ^ 0xffffffff) != -1) {
				i_21_ |= 0x80000;
			}
			ModelRenderer class146 = null;
			if (aByteArray2994 == null) {
				return null;
			}
			int i_26_ = -1;
			for (int i_27_ = 0; (aByteArray2994.length ^ 0xffffffff) < (i_27_ ^ 0xffffffff); i_27_++) {
				if ((i ^ 0xffffffff) == (aByteArray2994[i_27_] ^ 0xffffffff)) {
					i_26_ = i_27_;
					break;
				}
			}
			if ((i_26_ ^ 0xffffffff) == 0) {
				return null;
			}
			int[] is = class185 != null && class185.anIntArray1446 != null ? class185.anIntArray1446 : anIntArrayArray2951[i_26_];
			int i_28_ = is.length;
			if ((i_28_ ^ 0xffffffff) < -1) {
				long l = var_ha.id;
				for (int i_29_ = 0; i_29_ < i_28_; i_29_++) {
					l = is[i_29_] + l * 67783L;
				}
				synchronized (parser.aClass79_2522) {
					class146 = (ModelRenderer) parser.aClass79_2522.get(-122, l);
				}
				if (class146 != null) {
					if (i_23_ != class146.WA()) {
						i_21_ |= 0x1000;
					}
					if (i_24_ != class146.da()) {
						i_21_ |= 0x2000;
					}
				}
				if (class146 == null || (var_ha.c(class146.functionMask(), i_21_) ^ 0xffffffff) != -1) {
					int i_30_ = 0x1f01f | i_21_;
					if (class146 != null) {
						i_30_ = var_ha.mergeFunctionMask(i_30_, class146.functionMask());
					}
					BaseModel class178 = null;
					synchronized (TexturesPreferenceField.aClass178Array3699) {
						for (int i_31_ = 0; i_31_ < i_28_; i_31_++) {
							synchronized (parser.aClass207_2512) {
								class178 = Class98_Sub6.fromFile(0, -9252, parser.aClass207_2512, is[i_31_] & 0xffff);
							}
							if (class178 == null) {
								return null;
							}
							if (class178.version < 13) {
								class178.scaleLog2(13746, 2);
							}
							if (i_28_ > 1) {
								TexturesPreferenceField.aClass178Array3699[i_31_] = class178;
							}
						}
						if ((i_28_ ^ 0xffffffff) < -2) {
							class178 = new BaseModel(TexturesPreferenceField.aClass178Array3699, i_28_);
						}
					}
					class146 = var_ha.createModelRenderer(class178, i_30_, parser.anInt2528, i_23_, i_24_);
					synchronized (parser.aClass79_2522) {
						parser.aClass79_2522.put(l, class146, (byte) -80);
					}
				}
			}
			if (class146 == null) {
				return null;
			}
			ModelRenderer class146_32_ = class146.method2341((byte) 0, i_21_, true);
			if ((i_23_ ^ 0xffffffff) != (class146.WA() ^ 0xffffffff)) {
				class146_32_.C(i_23_);
			}
			if ((class146.da() ^ 0xffffffff) != (i_24_ ^ 0xffffffff)) {
				class146_32_.LA(i_24_);
			}
			if (bool) {
				class146_32_.v();
			}
			if (i == 4 && i_20_ > 3) {
				class146_32_.k(2048);
				class146_32_.H(180, 0, -180);
			}
			i_20_ &= 0x3;
			if (i_22_ != -75) {
				return null;
			}
			if ((i_20_ ^ 0xffffffff) != -2) {
				if (i_20_ != 2) {
					if ((i_20_ ^ 0xffffffff) == -4) {
						class146_32_.k(12288);
					}
				} else {
					class146_32_.k(8192);
				}
			} else {
				class146_32_.k(4096);
			}
			if (aShortArray3003 != null) {
				short[] is_33_;
				if (class185 == null || class185.aShortArray1447 == null) {
					is_33_ = aShortArray2965;
				} else {
					is_33_ = class185.aShortArray1447;
				}
				for (int i_34_ = 0; (i_34_ ^ 0xffffffff) > (aShortArray3003.length ^ 0xffffffff); i_34_++) {
					if (aByteArray2955 == null || (i_34_ ^ 0xffffffff) <= (aByteArray2955.length ^ 0xffffffff)) {
						class146_32_.recolour(aShortArray3003[i_34_], is_33_[i_34_]);
					} else {
						class146_32_.recolour(aShortArray3003[i_34_], Class372.clientPalette[0xff & aByteArray2955[i_34_]]);
					}
				}
			}
			if (aShortArray2995 != null) {
				short[] is_35_;
				if (class185 == null || class185.aShortArray1444 == null) {
					is_35_ = aShortArray2974;
				} else {
					is_35_ = class185.aShortArray1444;
				}
				for (int i_36_ = 0; (aShortArray2995.length ^ 0xffffffff) < (i_36_ ^ 0xffffffff); i_36_++) {
					class146_32_.retexture(aShortArray2995[i_36_], is_35_[i_36_]);
				}
			}
			if (aByte2932 != 0) {
				class146_32_.method2337(aByte2930, aByte2942, aByte2967, 0xff & aByte2932);
			}
			if (anInt2938 != 128 || anInt2954 != 128 || anInt2929 != 128) {
				class146_32_.scale(anInt2938, anInt2954, anInt2929);
			}
			if ((anInt2973 ^ 0xffffffff) != -1 || anInt2997 != 0 || (anInt2946 ^ 0xffffffff) != -1) {
				class146_32_.H(anInt2973, anInt2997, anInt2946);
			}
			class146_32_.updateFunctionMask(i_25_);
			return class146_32_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.H(" + i + ',' + i_20_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_21_ + ',' + (class185 != null ? "{...}" : "null") + ',' + i_22_ + ')');
		}
	}

	public final boolean method3857(int i) {
		try {
			if (anIntArrayArray2951 == null) {
				return true;
			}
			boolean bool = true;
			synchronized (parser.aClass207_2512) {
				for (int[] element : anIntArrayArray2951) {
					for (int i_39_ = 0; (element.length ^ 0xffffffff) < (i_39_ ^ 0xffffffff); i_39_++) {
						bool &= parser.aClass207_2512.isFileCached(0, element[i_39_], -6329);
					}
				}
			}
			if (i != 18182) {
				return true;
			}
			return bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.J(" + i + ')');
		}
	}

	public final boolean method3858(int i) {
		try {
			if (transformIDs == null) {
				return !(anInt2996 == -1 && anIntArray2926 == null);
			}
			if (i <= 91) {
				return false;
			}
			for (int i_40_ = 0; (transformIDs.length ^ 0xffffffff) < (i_40_ ^ 0xffffffff); i_40_++) {
				if (transformIDs[i_40_] != -1) {
					GameObjectDefinition class352_41_ = parser.get(transformIDs[i_40_], (byte) 119);
					if (class352_41_.anInt2996 != -1 || class352_41_.anIntArray2926 != null) {
						return true;
					}
				}
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.P(" + i + ')');
		}
	}

	public final ModelRenderer method3859(Ground var_s, int i, int i_42_, byte i_43_, RSToolkit var_ha, Ground var_s_44_, int i_45_, int i_46_, int i_47_, int i_48_, Class185 class185, int i_49_, AnimationDefinition class97, int i_50_, int i_51_) {
		if (Ground.method3419(112, i_48_)) {
			i_48_ = 4;
		}
		long l = i_46_ + (id << -1435947510) - -(i_48_ << -1440567773);
		int i_52_ = i_45_;
		l |= var_ha.id << 2020858845;
		if (class185 != null) {
			l |= class185.aLong1448 << -950504928;
		}
		if (class97 != null) {
			i_45_ |= class97.method932(false, i_49_, true, i_50_);
		}
		do {
			if ((aByte2971 ^ 0xffffffff) != -4) {
				if (aByte2971 != 0 || (anInt2988 ^ 0xffffffff) != -1) {
					i_45_ |= 0x2;
				}
				if ((anInt2940 ^ 0xffffffff) != -1) {
					i_45_ |= 0x1;
				}
				if (anInt2989 == 0) {
					break;
				}
				i_45_ |= 0x4;
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			i_45_ |= 0x7;
		} while (false);
		if (i_48_ == 10 && i_46_ > 3) {
			i_45_ |= 0x5;
		}
		ModelRenderer class146;
		synchronized (parser.aClass79_2527) {
			class146 = (ModelRenderer) parser.aClass79_2527.get(-127, l);
		}
		if (class146 == null || var_ha.c(class146.functionMask(), i_45_) != 0) {
			if (class146 != null) {
				i_45_ = var_ha.mergeFunctionMask(i_45_, class146.functionMask());
			}
			class146 = method3855(i_48_, i_46_, var_ha, i_45_, class185, (byte) -75);
			if (class146 == null) {
				return null;
			}
			synchronized (parser.aClass79_2527) {
				parser.aClass79_2527.put(l, class146, (byte) -80);
			}
		}
		boolean bool = false;
		if (class97 != null) {
			bool = true;
			class146 = class97.method930((byte) 1, 0x3 & i_46_, i_49_, i_45_, i_50_, (byte) 86, class146, i_51_);
		}
		if (i_48_ == 10 && i_46_ > 3) {
			if (!bool) {
				bool = true;
				class146 = class146.method2341((byte) 3, i_45_, true);
			}
			class146.rotateYaw(2048);
		}
		if ((aByte2971 ^ 0xffffffff) != -1) {
			if (!bool) {
				class146 = class146.method2341((byte) 3, i_45_, true);
				bool = true;
			}
			class146.p(aByte2971, anInt2985, var_s, var_s_44_, i, i_42_, i_47_);
		}
		if ((anInt2940 ^ 0xffffffff) != -1 || (anInt2988 ^ 0xffffffff) != -1 || anInt2989 != 0) {
			if (!bool) {
				bool = true;
				class146 = class146.method2341((byte) 3, i_45_, true);
			}
			class146.H(anInt2940, anInt2988, anInt2989);
		}
		if (bool) {
			class146.updateFunctionMask(i_52_);
		}
		return class146;
	}

	public final boolean method3860(int i, boolean bool) {
		if (bool != true) {
			anInt2929 = -32;
		}
		if ((i ^ 0xffffffff) == 0) {
			return false;
		}
		if ((anInt2941 ^ 0xffffffff) == (i ^ 0xffffffff)) {
			return true;
		}
		if (anIntArray2979 != null) {
			for (int element : anIntArray2979) {
				if ((element ^ 0xffffffff) == (i ^ 0xffffffff)) {
					return true;
				}
			}
		}
		return false;
	}

	public final int method3861(int i) {
		if (anIntArray2979 != null) {
			int i_55_ = (int) (anInt2964 * Math.random());
			int i_56_;
			for (i_56_ = 0; (anIntArray2937[i_56_] ^ 0xffffffff) >= (i_55_ ^ 0xffffffff); i_56_++) {
				i_55_ -= anIntArray2937[i_56_];
			}
			return anIntArray2979[i_56_];
		}
		return -1;
	}

	private final void method3863(RSByteBuffer buffer, int i, int i_57_) {
		do {
			try {
				if (i == 1 || i == 5) {
					if ((i ^ 0xffffffff) == -6 && parser.aBoolean2513) {
						method3854(buffer, 15);
					}
					int i_58_ = buffer.readUnsignedByte((byte) -117);
					aByteArray2994 = new byte[i_58_];
					anIntArrayArray2951 = new int[i_58_][];
					for (int i_59_ = 0; i_59_ < i_58_; i_59_++) {
						aByteArray2994[i_59_] = buffer.readSignedByte((byte) -19);
						int i_60_ = buffer.readUnsignedByte((byte) -101);
						anIntArrayArray2951[i_59_] = new int[i_60_];
						for (int i_61_ = 0; i_61_ < i_60_; i_61_++) {
							anIntArrayArray2951[i_59_][i_61_] = buffer.readShort((byte) 127);
						}
					}
					if ((i ^ 0xffffffff) == -6 && !parser.aBoolean2513) {
						method3854(buffer, i_57_ + 67);
					}
				} else if (i == 2) {
					name = buffer.readString((byte) 84);
				} else if ((i ^ 0xffffffff) == -15) {
					sizeY = buffer.readUnsignedByte((byte) 107);
				} else if (i != 15) {
					if ((i ^ 0xffffffff) != -18) {
						if (i != 18) {
							if (i == 19) {
								anInt2998 = buffer.readUnsignedByte((byte) 3);
							} else if (i != 21) {
								if ((i ^ 0xffffffff) == -23) {
									aBoolean3007 = true;
								} else if (i == 23) {
									anInt2956 = 1;
								} else if ((i ^ 0xffffffff) != -25) {
									if (i == 27) {
										actionCount = 1;
									} else if (i != 28) {
										if ((i ^ 0xffffffff) == -30) {
											anInt2931 = buffer.readSignedByte((byte) -19);
										} else if ((i ^ 0xffffffff) != -40) {
											if ((i ^ 0xffffffff) > -31 || i >= 35) {
												if ((i ^ 0xffffffff) == -41) {
													int i_62_ = buffer.readUnsignedByte((byte) 73);
													aShortArray3003 = new short[i_62_];
													aShortArray2965 = new short[i_62_];
													for (int i_63_ = 0; i_63_ < i_62_; i_63_++) {
														aShortArray3003[i_63_] = (short) buffer.readShort((byte) 127);
														aShortArray2965[i_63_] = (short) buffer.readShort((byte) 127);
													}
												} else if ((i ^ 0xffffffff) != -42) {
													if ((i ^ 0xffffffff) != -43) {
														if (i == 62) {
															aBoolean2961 = true;
														} else if (i != 64) {
															if ((i ^ 0xffffffff) == -66) {
																anInt2938 = buffer.readShort((byte) 127);
															} else if ((i ^ 0xffffffff) != -67) {
																if ((i ^ 0xffffffff) == -68) {
																	anInt2929 = buffer.readShort((byte) 127);
																} else if ((i ^ 0xffffffff) != -70) {
																	if ((i ^ 0xffffffff) == -71) {
																		anInt2973 = buffer.readUShort(false) << -836995390;
																	} else if ((i ^ 0xffffffff) == -72) {
																		anInt2997 = buffer.readUShort(false) << -1352000926;
																	} else if ((i ^ 0xffffffff) != -73) {
																		if (i != 73) {
																			if (i != 74) {
																				if (i != 75) {
																					if ((i ^ 0xffffffff) != -78 && (i ^ 0xffffffff) != -93) {
																						if (i == 78) {
																							anInt2996 = buffer.readShort((byte) 127);
																							anInt2981 = buffer.readUnsignedByte((byte) -108);
																						} else if (i != 79) {
																							if ((i ^ 0xffffffff) != -82) {
																								if (i == 82) {
																									aBoolean2982 = true;
																								} else if (i == 88) {
																									aBoolean2935 = false;
																								} else if ((i ^ 0xffffffff) == -90) {
																									aBoolean2925 = false;
																								} else if (i == 91) {
																									aBoolean2927 = true;
																								} else if (i != 93) {
																									if (i == 94) {
																										aByte2971 = (byte) 4;
																									} else if ((i ^ 0xffffffff) == -96) {
																										aByte2971 = (byte) 5;
																										anInt2985 = buffer.readUShort(false);
																									} else if ((i ^ 0xffffffff) == -98) {
																										aBoolean3004 = true;
																									} else if (i == 98) {
																										aBoolean3005 = true;
																									} else if ((i ^ 0xffffffff) != -100) {
																										if (i == 100) {
																											anInt2933 = buffer.readUnsignedByte((byte) -123);
																											anInt2977 = buffer.readShort((byte) 127);
																										} else if (i == 101) {
																											anInt2962 = buffer.readUnsignedByte((byte) -105);
																										} else if (i == 102) {
																											anInt2990 = buffer.readShort((byte) 127);
																										} else if ((i ^ 0xffffffff) != -104) {
																											if ((i ^ 0xffffffff) == -105) {
																												anInt2987 = buffer.readUnsignedByte((byte) -5);
																											} else if ((i ^ 0xffffffff) != -106) {
																												if (i == 106) {
																													int i_64_ = buffer.readUnsignedByte((byte) -112);
																													anIntArray2937 = new int[i_64_];
																													anIntArray2979 = new int[i_64_];
																													for (int i_65_ = 0; (i_65_ ^ 0xffffffff) > (i_64_ ^ 0xffffffff); i_65_++) {
																														anIntArray2979[i_65_] = buffer.readShort((byte) 127);
																														int i_66_ = buffer.readUnsignedByte((byte) -116);
																														anIntArray2937[i_65_] = i_66_;
																														anInt2964 += i_66_;
																													}
																												} else if ((i ^ 0xffffffff) != -108) {
																													if (i < 150 || (i ^ 0xffffffff) <= -156) {
																														if (i == 160) {
																															int i_67_ = buffer.readUnsignedByte((byte) -102);
																															anIntArray2934 = new int[i_67_];
																															for (int i_68_ = 0; (i_68_ ^ 0xffffffff) > (i_67_ ^ 0xffffffff); i_68_++) {
																																anIntArray2934[i_68_] = buffer.readShort((byte) 127);
																															}
																														} else if (i == 162) {
																															aByte2971 = (byte) 3;
																															anInt2985 = buffer.readInt(-2);
																														} else if (i == 163) {
																															aByte2930 = buffer.readSignedByte((byte) -19);
																															aByte2942 = buffer.readSignedByte((byte) -19);
																															aByte2967 = buffer.readSignedByte((byte) -19);
																															aByte2932 = buffer.readSignedByte((byte) -19);
																														} else if (i == 164) {
																															anInt2940 = buffer.readUShort(false);
																														} else if ((i ^ 0xffffffff) != -166) {
																															if ((i ^ 0xffffffff) != -167) {
																																if ((i ^ 0xffffffff) != -168) {
																																	if ((i ^ 0xffffffff) == -169) {
																																		aBoolean2992 = true;
																																	} else if (i == 169) {
																																		aBoolean2957 = true;
																																	} else if ((i ^ 0xffffffff) != -171) {
																																		if ((i ^ 0xffffffff) != -172) {
																																			if (i == 173) {
																																				anInt3006 = buffer.readShort((byte) 127);
																																				anInt2950 = buffer.readShort((byte) 127);
																																			} else if ((i ^ 0xffffffff) == -178) {
																																				aBoolean2984 = true;
																																			} else if ((i ^ 0xffffffff) == -179) {
																																				anInt2970 = buffer.readUnsignedByte((byte) -111);
																																			} else if (i == 249) {
																																				int i_69_ = buffer.readUnsignedByte((byte) 107);
																																				if (aClass377_2944 == null) {
																																					int i_70_ = Class48.findNextGreaterPwr2(423660257, i_69_);
																																					aClass377_2944 = new HashTable(i_70_);
																																				}
																																				for (int i_71_ = 0; i_71_ < i_69_; i_71_++) {
																																					boolean bool = buffer.readUnsignedByte((byte) -109) == 1;
																																					int i_72_ = buffer.readMediumInt(i_57_ + -133);
																																					Node class98;
																																					if (!bool) {
																																						class98 = new NodeInteger(buffer.readInt(-2));
																																					} else {
																																						class98 = new NodeString(buffer.readString((byte) 84));
																																					}
																																					aClass377_2944.put(class98, i_72_, -1);
																																				}
																																			}
																																		} else {
																																			anInt2953 = buffer.readSmart(1689622712);
																																		}
																																	} else {
																																		anInt2986 = buffer.readSmart(1689622712);
																																	}
																																} else {
																																	anInt2945 = buffer.readShort((byte) 127);
																																}
																															} else {
																																anInt2989 = buffer.readUShort(false);
																															}
																														} else {
																															anInt2988 = buffer.readUShort(false);
																														}
																													} else {
																														options[i + -150] = buffer.readString((byte) 84);
																														System.out.println(id + " " + Arrays.toString(options));

																														if (!parser.aBoolean2516) {
																															options[-150 + i] = null;
																															System.out.println(id + " " + Arrays.toString(options));
																														}
																													}
																												} else {
																													mapFunction = buffer.readShort((byte) 127);
																												}
																											} else {
																												aBoolean2976 = true;
																											}
																										} else {
																											anInt2956 = 0;
																										}
																									} else {
																										anInt3002 = buffer.readUnsignedByte((byte) -112);
																										anInt3008 = buffer.readShort((byte) 127);
																									}
																								} else {
																									aByte2971 = (byte) 3;
																									anInt2985 = buffer.readShort((byte) 127);
																								}
																							} else {
																								aByte2971 = (byte) 2;
																								anInt2985 = 256 * buffer.readUnsignedByte((byte) 76);
																							}
																						} else {
																							anInt2949 = buffer.readShort((byte) 127);
																							anInt2972 = buffer.readShort((byte) 127);
																							anInt2981 = buffer.readUnsignedByte((byte) -121);
																							int i_73_ = buffer.readUnsignedByte((byte) 121);
																							anIntArray2926 = new int[i_73_];
																							for (int i_74_ = 0; (i_73_ ^ 0xffffffff) < (i_74_ ^ 0xffffffff); i_74_++) {
																								anIntArray2926[i_74_] = buffer.readShort((byte) 127);
																							}
																						}
																					} else {
																						anInt2983 = buffer.readShort((byte) 127);
																						if ((anInt2983 ^ 0xffffffff) == -65536) {
																							anInt2983 = -1;
																						}
																						anInt2968 = buffer.readShort((byte) 127);
																						if (anInt2968 == 65535) {
																							anInt2968 = -1;
																						}
																						int i_75_ = -1;
																						if (i == 92) {
																							i_75_ = buffer.readShort((byte) 127);
																							if (i_75_ == 65535) {
																								i_75_ = -1;
																							}
																						}
																						int i_76_ = buffer.readUnsignedByte((byte) -106);
																						transformIDs = new int[i_76_ + 2];
																						for (int i_77_ = 0; i_76_ >= i_77_; i_77_++) {
																							transformIDs[i_77_] = buffer.readShort((byte) 127);
																							if (transformIDs[i_77_] == 65535) {
																								transformIDs[i_77_] = -1;
																							}
																						}
																						transformIDs[1 + i_76_] = i_75_;
																					}
																				} else {
																					anInt2975 = buffer.readUnsignedByte((byte) 74);
																				}
																			} else {
																				clippingFlag = true;
																			}
																		} else {
																			aBoolean2969 = true;
																		}
																	} else {
																		anInt2946 = buffer.readUShort(false) << -784917758;
																	}
																} else {
																	anInt2948 = buffer.readUnsignedByte((byte) 78);
																}
															} else {
																anInt2954 = buffer.readShort((byte) 127);
															}
														} else {
															aBoolean2947 = false;
														}
													} else {
														int i_78_ = buffer.readUnsignedByte((byte) 106);
														aByteArray2955 = new byte[i_78_];
														for (int i_79_ = 0; (i_78_ ^ 0xffffffff) < (i_79_ ^ 0xffffffff); i_79_++) {
															aByteArray2955[i_79_] = buffer.readSignedByte((byte) -19);
														}
													}
												} else {
													int i_80_ = buffer.readUnsignedByte((byte) 2);
													aShortArray2974 = new short[i_80_];
													aShortArray2995 = new short[i_80_];
													for (int i_81_ = 0; i_80_ > i_81_; i_81_++) {
														aShortArray2995[i_81_] = (short) buffer.readShort((byte) 127);
														aShortArray2974[i_81_] = (short) buffer.readShort((byte) 127);
													}
												}
											} else {
												options[i + -30] = buffer.readString((byte) 84);
											}
										} else {
											anInt2980 = buffer.readSignedByte((byte) -19) * 5;
										}
									} else {
										anInt2966 = buffer.readUnsignedByte((byte) 110) << -69774750;
									}
								} else {
									anInt2941 = buffer.readShort((byte) 127);
									if ((anInt2941 ^ 0xffffffff) == -65536) {
										anInt2941 = -1;
									}
								}
							} else {
								aByte2971 = (byte) 1;
							}
						} else {
							walkable = false;
						}
					} else {
						walkable = false;
						actionCount = 0;
					}
				} else {
					sizeX = buffer.readUnsignedByte((byte) 35);
				}
				if (i_57_ == 7) {
					break;
				}
				method3857(33);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vha.I(" + (buffer != null ? "{...}" : "null") + ',' + i + ',' + i_57_ + ')');
			}
			break;
		} while (false);
	}

	public final String method3864(byte i, int i_82_, String string) {
		try {
			if (i <= 85) {
				aShortArray2965 = null;
			}
			if (aClass377_2944 == null) {
				return string;
			}
			NodeString class98_sub15 = (NodeString) aClass377_2944.get(i_82_, -1);
			if (class98_sub15 == null) {
				return string;
			}
			return class98_sub15.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.M(" + i + ',' + i_82_ + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3865(int i) {
		do {
			try {
				if ((anInt2998 ^ 0xffffffff) == 0) {
					anInt2998 = 0;
					if (aByteArray2994 != null && (aByteArray2994.length ^ 0xffffffff) == -2 && (aByteArray2994[0] ^ 0xffffffff) == -11) {
						anInt2998 = 1;
					}
					for (int i_83_ = 0; i_83_ < 5; i_83_++) {
						if (options[i_83_] != null) {
							anInt2998 = 1;
							break;
						}
					}
				}
				if ((anInt2941 ^ 0xffffffff) != 0 || aBoolean3005 || transformIDs != null) {
					aBoolean2984 = true;
				}
				if (i <= 14) {
					anInt2931 = -53;
				}
				if (anInt2975 != -1) {
					break;
				}
				anInt2975 = actionCount == 0 ? 0 : 1;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vha.G(" + i + ')');
			}
			break;
		} while (false);
	}

	public final int method3866(int i, int i_84_, int i_85_) {
		try {
			if (i_85_ != 1) {
				method3865(87);
			}
			if (aClass377_2944 == null) {
				return i;
			}
			NodeInteger class98_sub34 = (NodeInteger) aClass377_2944.get(i_84_, -1);
			if (class98_sub34 == null) {
				return i;
			}
			return class98_sub34.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vha.O(" + i + ',' + i_84_ + ',' + i_85_ + ')');
		}
	}
}

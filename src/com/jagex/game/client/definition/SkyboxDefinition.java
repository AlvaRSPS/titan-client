
/* Class60 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import java.awt.Canvas;

import com.Class105;
import com.Class15;
import com.Class151_Sub8;
import com.Class174;
import com.Class18;
import com.Class208;
import com.Class21_Sub2;
import com.Class226;
import com.Class241;
import com.Class246_Sub1;
import com.Class248;
import com.Class252;
import com.Class259;
import com.Class288;
import com.Class319;
import com.Class34;
import com.Class340;
import com.Class347;
import com.Class35;
import com.Class375;
import com.Class42_Sub1_Sub1;
import com.Class67;
import com.Class69;
import com.Class74;
import com.Class81;
import com.Class85;
import com.Class98_Sub10_Sub10;
import com.Class98_Sub10_Sub27;
import com.Class98_Sub10_Sub30;
import com.Class98_Sub10_Sub31;
import com.Class98_Sub10_Sub39;
import com.Class98_Sub16;
import com.OpenGlPointLight;
import com.Class98_Sub46_Sub10;
import com.Class98_Sub46_Sub5;
import com.Class98_Sub48;
import com.IncomingOpcode;
import com.JavaNetworkReader;
import com.OpenGLTexture2DSource;
import com.OpenGLXToolkit;
import com.OutgoingOpcode;
import com.RSByteBuffer;
import com.RSToolkit;
import com.SceneGraphNodeList;
import com.TextureMetricsList;
import com.aa_Sub1;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;
import com.jagex.game.client.preferences.SceneryShadowsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.shadow.NativeShadow;

import jaggl.OpenGL;

public final class SkyboxDefinition {
	public static Class105			aClass105_474	= new Class105("", 13);
	public static Class85			aClass85_471	= new Class85(4, 4);
	public static long[]			aLongArray475;
	public static IncomingOpcode	VARP_LARGE		= new IncomingOpcode(27, 6);

	public static final RSToolkit create(int i, Js5 class207, TextureMetricsList var_d, int i_40_, Canvas canvas) {
		if (!Class319.loadedNativeLibs(true)) {
			throw new RuntimeException("");
		}
		if (!AdvancedMemoryCache.loadNative("jaggl", (byte) -36)) {
			throw new RuntimeException("");
		}
		OpenGL opengl = new OpenGL();
		long l = opengl.init(canvas, 8, 8, 8, 24, i_40_, i);
		if (l == 0L) {
			throw new RuntimeException("");
		}
		OpenGLXToolkit var_ha_Sub3_Sub2 = new OpenGLXToolkit(opengl, canvas, l, var_d, class207, i);
		var_ha_Sub3_Sub2.method1965(true);
		return var_ha_Sub3_Sub2;
	}

	public static final void method535(int i, int xCamPosTile, int cameraYorZ, int yCamPosTile, byte[][][] is, int[] is_6_, int[] is_7_, int[] is_8_, int[] is_9_, int[] is_10_, int i_11_, byte i_12_, int i_13_, int i_14_, boolean roofsRemoved, boolean alwaysTrue, int i_16_, int i_17_,
			boolean bool_18_) {
		Js5Client.aBoolean1052 = true;
		QuickChatMessageType.aBoolean2914 = Class98_Sub10_Sub30.activeToolkit.method1822() > 0;
		GameObjectDefinitionParser.aBoolean2526 = alwaysTrue;
		Class241.anInt1845 = xCamPosTile >> Class151_Sub8.tileScale;

		CharacterShadowsPreferenceField.anInt3714 = yCamPosTile >> Class151_Sub8.tileScale;

		JavaNetworkReader.anInt1018 = xCamPosTile;
		Class98_Sub48.anInt4280 = yCamPosTile;
		Class42_Sub1_Sub1.anInt6208 = cameraYorZ;

		EnumDefinition.anInt2561 = Class241.anInt1845 - Class259.anInt1959;
		if (EnumDefinition.anInt2561 < 0) {
			Class67.anInt521 = -EnumDefinition.anInt2561;
			EnumDefinition.anInt2561 = 0;
		} else {
			Class67.anInt521 = 0;
		}
		OutgoingOpcode.anInt1318 = CharacterShadowsPreferenceField.anInt3714 - Class259.anInt1959;
		if (OutgoingOpcode.anInt1318 < 0) {
			OpenGlPointLight.anInt4184 = -OutgoingOpcode.anInt1318;
			OutgoingOpcode.anInt1318 = 0;
		} else {
			OpenGlPointLight.anInt4184 = 0;
		}
		Class21_Sub2.anInt5388 = Class241.anInt1845 + Class259.anInt1959;
		if (Class21_Sub2.anInt5388 > BConfigDefinition.anInt3112) {
			Class21_Sub2.anInt5388 = BConfigDefinition.anInt3112;
		}
		FloorOverlayDefinitionParser.anInt303 = CharacterShadowsPreferenceField.anInt3714 + Class259.anInt1959;
		if (FloorOverlayDefinitionParser.anInt303 > Class64_Sub9.anInt3662) {
			FloorOverlayDefinitionParser.anInt303 = Class64_Sub9.anInt3662;
		}
		boolean[][] bools = Class74.aBooleanArrayArray551;
		boolean[][] bools_19_ = Class319.aBooleanArrayArray2702;

		if (GameObjectDefinitionParser.aBoolean2526) {

			for (int i_20_ = 0; i_20_ < Class259.anInt1959 + Class259.anInt1959 + 2; i_20_++) {
				int i_21_ = 0;
				int i_22_ = 0;
				for (int i_23_ = 0; i_23_ < Class259.anInt1959 + Class259.anInt1959 + 2; i_23_++) {
					if (i_23_ > 1) {
						Class347.anIntArray2906[i_23_ - 2] = i_21_;
					}
					i_21_ = i_22_;
					int i_24_ = Class241.anInt1845 - Class259.anInt1959 + i_20_;
					int i_25_ = CharacterShadowsPreferenceField.anInt3714 - Class259.anInt1959 + i_23_;
					if (i_24_ >= 0 && i_25_ >= 0 && i_24_ < BConfigDefinition.anInt3112 && i_25_ < Class64_Sub9.anInt3662) {
						int i_26_ = i_24_ << Class151_Sub8.tileScale;
						int i_27_ = i_25_ << Class151_Sub8.tileScale;
						int i_28_ = StrongReferenceMCNode.aSArray6298[StrongReferenceMCNode.aSArray6298.length - 1].getTileHeight(i_25_, -12639, i_24_) - (1000 << Class151_Sub8.tileScale - 7);
						int i_29_ = Class81.aSArray618 != null ? Class81.aSArray618[0].getTileHeight(i_25_, -12639, i_24_) + NativeShadow.anInt6333 : StrongReferenceMCNode.aSArray6298[0].getTileHeight(i_25_, -12639, i_24_) + NativeShadow.anInt6333;
						i_22_ = i_16_ >= 0 ? Class98_Sub10_Sub30.activeToolkit.r(i_26_, i_28_, i_27_, i_26_, i_29_, i_27_, i_16_) : Class98_Sub10_Sub30.activeToolkit.JA(i_26_, i_28_, i_27_, i_26_, i_29_, i_27_);
						Class319.aBooleanArrayArray2702[i_20_][i_23_] = i_22_ == 0;
						// System.out.println("i_20_" + i_27_);
						// System.out.println("i_23_" + i_23_);

					} else {
						i_22_ = -1;
						Class319.aBooleanArrayArray2702[i_20_][i_23_] = false;
					}
					if (i_20_ > 0 && i_23_ > 0) {
						int i_30_ = Class347.anIntArray2906[i_23_ - 1] & Class347.anIntArray2906[i_23_] & i_21_ & i_22_;
						Class74.aBooleanArrayArray551[i_20_ - 1][i_23_ - 1] = i_30_ == 0;
					}
				}
				Class347.anIntArray2906[Class259.anInt1959 + Class259.anInt1959] = i_21_;
				Class347.anIntArray2906[Class259.anInt1959 + Class259.anInt1959 + 1] = i_22_;
			}
			if (i_16_ >= 0) {
				Js5Client.aBoolean1052 = false;
			} else {
				VarClientDefinitionParser.anIntArray1044 = is_6_;
				Class288.anIntArray3376 = is_7_;
				Class98_Sub46_Sub10.anIntArray6015 = is_8_;
				Class98_Sub10_Sub10.anIntArray5589 = is_9_;
				OpenGLTexture2DSource.anIntArray3102 = is_10_;
				Class340.method3802(Class98_Sub10_Sub30.activeToolkit, i_11_, (byte) -59);
			}
		} else {
			if (aa_Sub1.aBooleanArrayArray3560 == null) {
				aa_Sub1.aBooleanArrayArray3560 = new boolean[BConfigDefinition.anInt3112 + BConfigDefinition.anInt3112 + 1][Class64_Sub9.anInt3662 + BConfigDefinition.anInt3112 + 1];
			}
			for (int i_31_ = 0; i_31_ < aa_Sub1.aBooleanArrayArray3560.length; i_31_++) {
				for (int i_32_ = 0; i_32_ < aa_Sub1.aBooleanArrayArray3560[0].length; i_32_++) {
					aa_Sub1.aBooleanArrayArray3560[i_31_][i_32_] = true;
				}
			}
			Class319.aBooleanArrayArray2702 = aa_Sub1.aBooleanArrayArray3560;
			Class74.aBooleanArrayArray551 = aa_Sub1.aBooleanArrayArray3560;
			EnumDefinition.anInt2561 = 0;
			OutgoingOpcode.anInt1318 = 0;
			Class21_Sub2.anInt5388 = BConfigDefinition.anInt3112;
			FloorOverlayDefinitionParser.anInt303 = Class64_Sub9.anInt3662;
			Js5Client.aBoolean1052 = false;
		}
		SceneryShadowsPreferenceField.method580(Class98_Sub10_Sub30.activeToolkit, 59);
		if (!Class98_Sub10_Sub27.aClass84_5692.aBoolean637) {
			SceneGraphNodeList class218 = Class98_Sub10_Sub27.aClass84_5692.aClass218_635;
			for (Class246_Sub1 class246_sub1 = (Class246_Sub1) class218.getFirst((byte) 15); class246_sub1 != null; class246_sub1 = (Class246_Sub1) class218.getNext(false)) {
				class246_sub1.unlink((byte) -37);
				Class35.method333(class246_sub1, 84);
			}
		}
		if (QuickChatMessageType.aBoolean2914) {
			for (int i_33_ = 0; i_33_ < Class226.anInt1705; i_33_++) {
				Class98_Sub10_Sub31.aClass1Array5717[i_33_].method161(roofsRemoved, i, 71);
			}
		}
		if (Class375.aBoolean3170) {
			GameObjectDefinitionParser.anIntArray2521 = Class98_Sub10_Sub30.activeToolkit.Y();
			Class98_Sub10_Sub30.activeToolkit.getClip(Class98_Sub16.anIntArray3933);
			int i_34_ = (Class98_Sub16.anIntArray3933[2] - Class98_Sub16.anIntArray3933[0]) / Class18.anInt212;
			for (int i_35_ = 0; i_35_ < Class18.anInt212 - 1; i_35_++) {
				Ground.anIntArray2205[i_35_] = i_34_ * (i_35_ + 1) + Class15.anIntArray182[i_35_];
			}
			for (Class174 element : Class98_Sub46_Sub5.aClass174Array5970) {
				element.method2566();
			}
		}
		if (Class252.aClass172ArrayArrayArray1927 != null) {
			if (Class375.aBoolean3170) {
				Class69.method701(0);
			}
			Class248.method3158(true);
			Class98_Sub10_Sub30.activeToolkit.ra(-1, 1583160, 40, 127);
			Class69.method700(true, is, i_11_, i_12_, i_16_, i_17_, bool_18_);
			if (Class375.aBoolean3170) {
				MapScenesDefinition.method194();
			}
			Class98_Sub10_Sub30.activeToolkit.pa();
			Class248.method3158(false);
		}
		Class69.method700(false, is, i_11_, i_12_, i_16_, i_17_, bool_18_);
		if (Class375.aBoolean3170) {
			for (int i_37_ = 0; i_37_ < OpenGLTexture2DSource.anInt3103; i_37_++) {
				GraphicsLevelPreferenceField.aBooleanArrayArrayArray3673[i_37_] = Class34.aBooleanArrayArrayArray325[i_37_];
			}
			Class69.method701(0);
			for (Class174 element : Class98_Sub46_Sub5.aClass174Array5970) {
				element.method2566();
			}
		}
		if (Class375.aBoolean3170) {
			MapScenesDefinition.method194();
			for (int i_39_ = 0; i_39_ < OpenGLTexture2DSource.anInt3103; i_39_++) {
				Class34.aBooleanArrayArrayArray325[i_39_] = GraphicsLevelPreferenceField.aBooleanArrayArrayArray3673[i_39_];
			}
			if (RenderAnimDefinition.anInt2407 == 2) {
				if (Class98_Sub10_Sub39.aLongArray5772[0] < Class98_Sub10_Sub39.aLongArray5772[1]) {
					if (Ground.anIntArray2205[0] + Class15.anIntArray182[0] > Class98_Sub16.anIntArray3933[0]) {
						Class15.anIntArray182[0]++;
					}
				} else if (Class98_Sub10_Sub39.aLongArray5772[0] > Class98_Sub10_Sub39.aLongArray5772[1] && Ground.anIntArray2205[0] + Class15.anIntArray182[0] < Class98_Sub16.anIntArray3933[2]) {
					Class15.anIntArray182[0]--;
				}
			}
		}
		if (!GameObjectDefinitionParser.aBoolean2526) {
			Class74.aBooleanArrayArray551 = bools;
			Class319.aBooleanArrayArray2702 = bools_19_;
		}
		Class208.method2767();
	}

	public int		anInt470	= -1;

	public int		anInt472	= -1;

	public int[]	suns;

	public SkyboxDefinition() {
		/* empty */
	}

	public final void decode(RSByteBuffer buffer, int i) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) -120);
			if (opcode == 0) {
				break;
			}
			method532((byte) 58, buffer, opcode);
		}
	}

	private final void method532(byte i, RSByteBuffer buffer, int opcode) {
		if ((opcode ^ 0xffffffff) == -2) {
			anInt470 = buffer.readShort((byte) 127);
		} else if (opcode != 2) {
			if (opcode == 3) {
				anInt472 = buffer.readUnsignedByte((byte) -101);
			}
		} else {
			suns = new int[buffer.readUnsignedByte((byte) -101)];
			for (int index = 0; index < suns.length; index++) {
				suns[index] = buffer.readShort((byte) 127);
			}
		}
	}
}

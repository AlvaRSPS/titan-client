/* Class220 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class245;
import com.Class48;
import com.Class98_Sub4;
import com.NodeInteger;
import com.NodeString;
import com.RSByteBuffer;
import com.Sound;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;

public final class QuestDefinition {
	public static Class98_Sub4 aClass98_Sub4_1657 = null;

	public static final void method2820(byte i) {
		Class245.aClass338Array1865 = new Sound[50];
		EnumDefinition.anInt2566 = 0;
	}

	public int[]		anIntArray1646;
	public int[]		anIntArray1647;
	public int[]		anIntArray1651;
	public int[]		anIntArray1652;
	public int[]		anIntArray1653;
	public int[]		anIntArray1655;
	public int[]		anIntArray1660;
	public int[]		anIntArray1661;
	public int[][]		anIntArrayArray1648;
	public int[][]		anIntArrayArray1658;
	public int[][]		anIntArrayArray1659;
	public String		aString1654;
	private String		aString1663;
	public String[]		aStringArray1656;
	private String[]	aStringArray1662;

	public int			menuSpriteId	= -1;

	public HashTable	params;

	public QuestDefinition() {
		/* empty */
	}

	public final void decode(byte i, RSByteBuffer buffer) {
		for (;;) {
			int field = buffer.readUnsignedByte((byte) 32);
			if ((field ^ 0xffffffff) == -1) {
				break;
			}
			this.decode(buffer, field, 98);
		}
	}

	private final void decode(RSByteBuffer buffer, int opcode, int i_2_) {
		if (opcode != 1) {
			if (opcode != 2) {
				if (opcode != 3) {
					if (opcode == 4) {
						int i_3_ = buffer.readUnsignedByte((byte) -1);
						anIntArrayArray1648 = new int[i_3_][3];
						for (int i_4_ = 0; (i_3_ ^ 0xffffffff) < (i_4_ ^ 0xffffffff); i_4_++) {
							anIntArrayArray1648[i_4_][0] = buffer.readShort((byte) 127);
							anIntArrayArray1648[i_4_][1] = buffer.readInt(-2);
							anIntArrayArray1648[i_4_][2] = buffer.readInt(-2);
						}
					} else if (opcode != 5) {
						if (opcode != 6) {
							if (opcode != 7) {
								if (opcode != 8) {
									if ((opcode ^ 0xffffffff) != -10) {
										if (opcode == 10) {
											int i_5_ = buffer.readUnsignedByte((byte) -126);
											anIntArray1652 = new int[i_5_];
											for (int i_6_ = 0; (i_5_ ^ 0xffffffff) < (i_6_ ^ 0xffffffff); i_6_++) {
												anIntArray1652[i_6_] = buffer.readInt(-2);
											}
										} else if (opcode != 12) {
											if (opcode == 13) {
												int i_7_ = buffer.readUnsignedByte((byte) 44);
												anIntArray1651 = new int[i_7_];
												for (int i_8_ = 0; (i_8_ ^ 0xffffffff) > (i_7_ ^ 0xffffffff); i_8_++) {
													anIntArray1651[i_8_] = buffer.readShort((byte) 127);
												}
											} else if ((opcode ^ 0xffffffff) == -15) {
												int i_9_ = buffer.readUnsignedByte((byte) 41);
												anIntArrayArray1658 = new int[i_9_][2];
												for (int i_10_ = 0; i_10_ < i_9_; i_10_++) {
													anIntArrayArray1658[i_10_][0] = buffer.readUnsignedByte((byte) -114);
													anIntArrayArray1658[i_10_][1] = buffer.readUnsignedByte((byte) -119);
												}
											} else if (opcode != 15) {
												if (opcode == 17) {
													menuSpriteId = buffer.readShort((byte) 127);
												} else if ((opcode ^ 0xffffffff) == -19) {
													int i_11_ = buffer.readUnsignedByte((byte) 87);
													anIntArray1653 = new int[i_11_];
													anIntArray1647 = new int[i_11_];
													aStringArray1662 = new String[i_11_];
													anIntArray1661 = new int[i_11_];
													for (int i_12_ = 0; (i_12_ ^ 0xffffffff) > (i_11_ ^ 0xffffffff); i_12_++) {
														anIntArray1647[i_12_] = buffer.readInt(-2);
														anIntArray1653[i_12_] = buffer.readInt(-2);
														anIntArray1661[i_12_] = buffer.readInt(-2);
														aStringArray1662[i_12_] = buffer.readString((byte) 84);
													}
												} else if ((opcode ^ 0xffffffff) != -20) {
													if (opcode == 249) {
														int paramCount = buffer.readUnsignedByte((byte) -109);
														if (params == null) {
															int htSz = Class48.findNextGreaterPwr2(423660257, paramCount);
															params = new HashTable(htSz);
														}
														for (int count = 0; count < paramCount; count++) {
															boolean isString = buffer.readUnsignedByte((byte) 3) == 1;
															int id = buffer.readMediumInt(-125);
															Node node;
															if (isString) {
																node = new NodeString(buffer.readString((byte) 84));
															} else {
																node = new NodeInteger(buffer.readInt(-2));
															}
															params.put(node, id, -1);
														}
													}
												} else {
													int i_17_ = buffer.readUnsignedByte((byte) -119);
													anIntArray1655 = new int[i_17_];
													aStringArray1656 = new String[i_17_];
													anIntArray1646 = new int[i_17_];
													anIntArray1660 = new int[i_17_];
													for (int i_18_ = 0; (i_18_ ^ 0xffffffff) > (i_17_ ^ 0xffffffff); i_18_++) {
														anIntArray1646[i_18_] = buffer.readInt(-2);
														anIntArray1655[i_18_] = buffer.readInt(-2);
														anIntArray1660[i_18_] = buffer.readInt(-2);
														aStringArray1656[i_18_] = buffer.readString((byte) 84);
													}
												}
											} else {
												buffer.readShort((byte) 127);
											}
										} else {
											buffer.readInt(-2);
										}
									} else {
										buffer.readUnsignedByte((byte) -102);
									}
								}
							} else {
								buffer.readUnsignedByte((byte) 95);
							}
						} else {
							buffer.readUnsignedByte((byte) -121);
						}
					} else {
						buffer.readShort((byte) 127);
					}
				} else {
					int i_19_ = buffer.readUnsignedByte((byte) 35);
					anIntArrayArray1659 = new int[i_19_][3];
					for (int i_20_ = 0; (i_19_ ^ 0xffffffff) < (i_20_ ^ 0xffffffff); i_20_++) {
						anIntArrayArray1659[i_20_][0] = buffer.readShort((byte) 127);
						anIntArrayArray1659[i_20_][1] = buffer.readInt(-2);
						anIntArrayArray1659[i_20_][2] = buffer.readInt(-2);
					}
				}
			} else {
				aString1654 = buffer.readPrefixedString(-1);
			}
		} else {
			aString1663 = buffer.readPrefixedString(-1);
		}
	}

	public final void postDecode(int i) {
		do {
			if (aString1654 != null) {
				break;
			}
			aString1654 = aString1663;
			break;
		} while (false);
	}
}

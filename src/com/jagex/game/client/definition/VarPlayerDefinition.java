/* Class167 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Char;
import com.Class18;
import com.Class200;
import com.Class289;
import com.Class375;
import com.Class98_Sub10_Sub37;
import com.Class98_Sub43_Sub3;
import com.ClipMap;
import com.GameShell;
import com.RSByteBuffer;
import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.preferences.ParticlesPreferenceField;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;

public final class VarPlayerDefinition {
	public static LinkedList	aClass148_1284	= new LinkedList();
	public static int			anInt1282		= 0;
	public static int			anInt1286		= -1;
	public static Object		anObject1285;
	public static ClipMap[]		clipMaps		= new ClipMap[4];

	public static final void method2529(Char class246_sub3, boolean bool, boolean bool_1_) {
		class246_sub3.aBoolean5082 = bool_1_;
		if (Class375.aBoolean3170) {
			if (bool) {
				Class98_Sub43_Sub3.aClass245Array5922[Class98_Sub43_Sub3.aClass245Array5922.length - 1].method2960(class246_sub3, 0);
			} else {
				int i = Class200.method2692(class246_sub3.anInt5085);
				int i_2_ = GameObjectDefinitionParser.anIntArray2521[2] * class246_sub3.method2985(false) / class246_sub3.anInt5083;
				int i_3_ = Class200.method2692(class246_sub3.anInt5085 - i_2_);
				int i_4_ = Class200.method2692(class246_sub3.anInt5085 + i_2_);
				if (i_3_ == i_4_) {
					Class98_Sub43_Sub3.aClass245Array5922[i].method2960(class246_sub3, 0);
				} else if (i_4_ - i_3_ == 1) {
					Class98_Sub43_Sub3.aClass245Array5922[Class18.anInt212 + i_3_].method2960(class246_sub3, 0);
				} else {
					Class98_Sub43_Sub3.aClass245Array5922[Class98_Sub43_Sub3.aClass245Array5922.length - 1].method2960(class246_sub3, 0);
				}
			}
		} else {
			Class289.method3407(class246_sub3, SoftwareNativeHeap.aClass98_Sub5Array6077);
		}
	}

	public static final byte[] method2531(String string, int i) {
		try {
			int i_6_ = string.length();
			if ((i_6_ ^ 0xffffffff) == -1) {
				return new byte[0];
			}
			int i_7_ = 3 + i_6_ & ~0x3;
			int i_8_ = i_7_ / 4 * 3;
			if (i != 12705) {
				return null;
			}
			do {
				if ((i_6_ ^ 0xffffffff) >= (i_7_ + -2 ^ 0xffffffff) || ParticlesPreferenceField.method574(112, string.charAt(i_7_ - 2)) == -1) {
					i_8_ -= 2;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if ((i_6_ ^ 0xffffffff) >= (-1 + i_7_ ^ 0xffffffff) || ParticlesPreferenceField.method574(74, string.charAt(i_7_ + -1)) == -1) {
					i_8_--;
				}
			} while (false);
			byte[] is = new byte[i_8_];
			Class98_Sub10_Sub37.method1115(0, is, false, string);
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ld.C(" + (string != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public int anInt1283 = 0;

	public VarPlayerDefinition() {
		/* empty */
	}

	public final void method2527(RSByteBuffer class98_sub22, int i) {
		do {
			try {
				for (;;) {
					int i_0_ = class98_sub22.readUnsignedByte((byte) 80);
					if ((i_0_ ^ 0xffffffff) == -1) {
						break;
					}
					method2530(i_0_, class98_sub22, i ^ ~0x3);
				}
				if (i == -2) {
					break;
				}
				method2527(null, -116);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ld.B(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	private final void method2530(int i, RSByteBuffer class98_sub22, int i_5_) {
		do {
			try {
				if ((i ^ 0xffffffff) == -6) {
					anInt1283 = class98_sub22.readShort((byte) 127);
				}
				if (i_5_ == 2) {
					break;
				}
				method2531(null, -110);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ld.D(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_5_ + ')');
			}
			break;
		} while (false);
	}
}

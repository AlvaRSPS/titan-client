/* Class86 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.ActionQueueEntry;
import com.ChatStatus;
import com.Class103;
import com.Class245;
import com.Class44;
import com.Class87;
import com.NodeShort;
import com.Class98_Sub43_Sub1;
import com.Class98_Sub47;
import com.Image;
import com.RSByteBuffer;
import com.RSToolkit;
import com.Sound;
import com.Sprite;
import com.TextResources;
import com.WorldMap;
import com.client;
import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;

public final class HitmarksDefinition {
	public static ChatStatus	chatStatus;
	public static float[]		GROUND_MATRIX	= new float[16];

	public static final String getMenuText(byte i, ActionQueueEntry action) {
		if (action.aString5985 != null && action.aString5985.length() != 0) {
			if (action.targetText == null || (action.targetText.length() ^ 0xffffffff) >= -1) {
				return action.actionText + TextResources.WHITE_SPACE.getText(client.gameLanguage, (byte) 25) + action.aString5985;
			}
			return action.actionText + TextResources.WHITE_SPACE.getText(client.gameLanguage, (byte) 25) + action.targetText + TextResources.WHITE_SPACE.getText(client.gameLanguage, (byte) 25) + action.aString5985;
		}
		if (action.targetText == null || (action.targetText.length() ^ 0xffffffff) >= -1) {
			return action.actionText;
		}
		return action.actionText + TextResources.WHITE_SPACE.getText(client.gameLanguage, (byte) 25) + action.targetText;
	}

	public static final void method843(int i, RSToolkit var_ha, LinkedList class148, int i_1_, int i_2_) {
		InventoriesDefinitionParser.aClass148_110.clear((byte) 47);
		if (!Class98_Sub43_Sub1.aBoolean5895) {
			for (Class98_Sub47 class98_sub47 = (Class98_Sub47) class148.getFirst(32); class98_sub47 != null; class98_sub47 = (Class98_Sub47) class148.getNext(i_1_ + 93)) {
				WorldMapInfoDefinition worldMap = WorldMap.aClass341_2057.get(i_1_ ^ ~0x65, class98_sub47.anInt4268);
				if (Class87.method855(87, worldMap)) {
					boolean bool = NodeShort.method1473(worldMap, i, class98_sub47, i_2_, 15924, var_ha);
					if (bool) {
						Class103.method1711(var_ha, (byte) 70, class98_sub47, worldMap);
					}
				}
			}
		}
	}

	public static final void method844(int i, boolean bool, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		if ((!bool ? client.preferences.soundEffectsVolume.getValue((byte) 122) : client.preferences.voiceOverVolume.getValue((byte) 127)) != 0 && i_4_ != 0 && EnumDefinition.anInt2566 < 50 && i_7_ != -1) {
			Class245.aClass338Array1865[EnumDefinition.anInt2566++] = new Sound(!bool ? (byte) 2 : (byte) 3, i_7_, i_4_, i_3_, i_5_, i_6_, i, null);
		}
	}

	private String					amount;
	public int						anInt646;
	public int						anInt650;
	public int						comparisonType	= -1;
	public int						duration;
	public int						fade;
	public int						font;
	private int						icon			= -1;
	private int						left;
	public HitmarksDefinitionParser	loader;

	private int						middle;

	public int						offsetX;

	private int						right			= -1;

	public int						textColour;

	public HitmarksDefinition() {
		fade = -1;
		anInt650 = 0;
		textColour = 16777215;
		duration = 70;
		left = -1;
		middle = -1;
		offsetX = 0;
		anInt646 = 0;
		font = -1;
		amount = "";
	}

	private final void cacheSprites(int i, RSToolkit toolkit) {
		do {
			Js5 images = loader.images;
			if (icon >= 0 && loader.cache.get(-120, icon) == null && images.isFileCached(-32, icon)) {
				Image image = Image.loadFirst(images, icon);
				loader.cache.put(icon, toolkit.createSprite(image, true), (byte) -80);
			}
			if (middle >= 0 && loader.cache.get(-119, middle) == null && images.isFileCached(-83, middle)) {
				Image image = Image.loadFirst(images, middle);
				loader.cache.put(middle, toolkit.createSprite(image, true), (byte) -80);
			}
			if ((left ^ 0xffffffff) <= -1 && loader.cache.get(-126, left) == null && images.isFileCached(-91, left)) {
				Image image = Image.loadFirst(images, left);
				loader.cache.put(left, toolkit.createSprite(image, true), (byte) -80);
			}
			if ((right ^ 0xffffffff) > -1 || loader.cache.get(-123, right) != null || !images.isFileCached(-79, right)) {
				break;
			}
			Image image = Image.loadFirst(images, right);
			loader.cache.put(right, toolkit.createSprite(image, true), (byte) -80);
			break;
		} while (false);
	}

	private final void decode(int i, int opcode, RSByteBuffer buffer) {
		if (opcode == 1) {
			font = buffer.readShort((byte) 127);
		} else if ((opcode ^ 0xffffffff) == -3) {
			textColour = buffer.readMediumInt(-128);
		} else if ((opcode ^ 0xffffffff) == -4) {
			icon = buffer.readShort((byte) 127);
		} else if (opcode == 4) {
			left = buffer.readShort((byte) 127);
		} else if ((opcode ^ 0xffffffff) == -6) {
			middle = buffer.readShort((byte) 127);
		} else if ((opcode ^ 0xffffffff) != -7) {
			if ((opcode ^ 0xffffffff) != -8) {
				if ((opcode ^ 0xffffffff) != -9) {
					if ((opcode ^ 0xffffffff) == -10) {
						duration = buffer.readShort((byte) 127);
					} else if (opcode == 10) {
						anInt650 = buffer.readUShort(false);
					} else if (opcode == 11) {
						fade = 0;
					} else if (opcode == 12) {
						comparisonType = buffer.readUnsignedByte((byte) -120);
					} else if (opcode == 13) {
						anInt646 = buffer.readUShort(false);
					} else if (opcode == 14) {
						fade = buffer.readShort((byte) 127);
					}
				} else {
					amount = buffer.readPrefixedString(-1);
				}
			} else {
				offsetX = buffer.readUShort(false);
			}
		} else {
			right = buffer.readShort((byte) 127);
		}
	}

	public final void decode(RSByteBuffer buffer, int i) {
		do {
			for (;;) {
				int opcode = buffer.readUnsignedByte((byte) 9);
				if ((opcode ^ 0xffffffff) == -1) {
					break;
				}
				this.decode(-120, opcode, buffer);
			}
			break;
		} while (false);
	}

	public final String formatAmount(int i, int amount) {
		String string = this.amount;
		for (;;) {
			int index = string.indexOf("%1");
			if ((index ^ 0xffffffff) > -1) {
				break;
			}
			string = string.substring(0, index) + Class44.decimalToString(amount, false, false) + string.substring(index + 2);
		}
		return string;
	}

	public final Sprite getIconSprite(boolean bool, RSToolkit toolkit) {
		if (icon < 0) {
			return null;
		}
		Sprite sprite = (Sprite) loader.cache.get(-125, icon);
		if (sprite == null) {
			cacheSprites(-109, toolkit);
			sprite = (Sprite) loader.cache.get(-125, icon);
		}
		// System.out.println("Icon: " + icon);
		return sprite;
	}

	public final Sprite getLeftSprite(RSToolkit toolkit, int i) {
		if ((left ^ 0xffffffff) > -1) {
			return null;
		}
		Sprite sprite = (Sprite) loader.cache.get(-127, left);
		if (sprite == null) {
			cacheSprites(-83, toolkit);
			sprite = (Sprite) loader.cache.get(-128, left);
		}
		// System.out.println("Left: " + left);
		return sprite;
	}

	public final Sprite getMiddleSprite(int i, RSToolkit toolkit) {
		if (middle < 0) {
			return null;
		}
		Sprite sprite = (Sprite) loader.cache.get(-122, middle);
		if (sprite == null) {
			cacheSprites(45, toolkit);
			sprite = (Sprite) loader.cache.get(-128, middle);
		}
		// System.out.println("Middle: " + middle);
		return sprite;
	}

	public final Sprite getRightSprite(int i, RSToolkit toolkit) {
		if (i < (right ^ 0xffffffff)) {
			return null;
		}
		Sprite sprite = (Sprite) loader.cache.get(-121, right);
		if (sprite == null) {
			cacheSprites(i + 61, toolkit);
			sprite = (Sprite) loader.cache.get(-125, right);
		}
		// System.out.println("Right: " + right);
		return sprite;
	}
}

/* Class97 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.AnimationSkeletonSet;
import com.Class232;
import com.Class357;
import com.Class369;
import com.Class372;
import com.GameShell;
import com.RSByteBuffer;
import com.jagex.game.client.definition.parser.AnimationDefinitionParser;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class AnimationDefinition {
	public static Class232	aClass232_806			= new Class232();
	public static float		aFloat831;
	public static int[][]	anIntArrayArray814;
	public static boolean	lightningDetailLevel	= false;

	public static final boolean method935(int i, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_) {
		for (int i_27_ = i_23_; (i_22_ ^ 0xffffffff) <= (i_27_ ^ 0xffffffff); i_27_++) {
			for (int i_28_ = i_25_; i_28_ <= i; i_28_++) {
				if ((Class372.anIntArrayArray3149[i_27_][i_28_] ^ 0xffffffff) == (i_26_ ^ 0xffffffff) && (WaterDetailPreferenceField.anIntArrayArray3719[i_27_][i_28_] ^ 0xffffffff) >= -2) {
					return true;
				}
			}
		}
		return false;
	}

	public static final void method936(int i, int i_29_) {
		if (i_29_ == 1024) {
			SoftwareNativeHeap.anInt6076 = i;
			synchronized (RtMouseListener.aClass79_2493) {
				RtMouseListener.aClass79_2493.clear(87);
			}
		}
	}

	public boolean						aBoolean812	= false;
	public boolean						aBoolean817;
	public boolean						aBoolean823;
	public boolean						aBoolean825;
	public boolean[]					animationFlowControl;
	public int							anInt807;
	public int							anInt819;
	public int							animation;
	public int							anInt829;
	public int[]						anIntArray808;
	public int[]						anIntArray810;
	public int[]						anIntArray815;
	private int[]						anIntArray827;
	public int[][]						anIntArrayArray822;
	public int[]						frameIds;
	public int[]						frameLengths;
	public int							frameStep;

	public AnimationDefinitionParser	loader;

	public int							mainhand	= -1;

	public int							offhand;

	public int							priority;

	public int							resetWhenWalk;

	public AnimationDefinition() {
		anInt807 = 99;
		resetWhenWalk = -1;
		anInt819 = 2;
		offhand = -1;
		priority = -1;
		aBoolean817 = false;
		aBoolean823 = false;
		aBoolean825 = false;
		anInt829 = 5;
		frameStep = -1;
	}

	private final void decode(int opcode, RSByteBuffer buffer, int i_44_) {
		if (opcode == 1) {
			int frameCount = buffer.readShort((byte) 127);
			frameLengths = new int[frameCount];

			for (int index = 0; frameCount > index; index++) {
				frameLengths[index] = buffer.readShort((byte) 127);
			}

			frameIds = new int[frameCount];
			for (int index = 0; (frameCount ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				frameIds[index] = buffer.readShort((byte) 127);
			}
			for (int index = 0; (frameCount ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				frameIds[index] = (buffer.readShort((byte) 127) << 1514351920) - -frameIds[index];
			}
		} else if ((opcode ^ 0xffffffff) == -3) {
			frameStep = buffer.readShort((byte) 127);
		} else if ((opcode ^ 0xffffffff) != -4) {
			if ((opcode ^ 0xffffffff) == -6) {
				anInt829 = buffer.readUnsignedByte((byte) -108);
			} else if ((opcode ^ 0xffffffff) == -7) {
				offhand = buffer.readShort((byte) 127);
			} else if (opcode != 7) {
				if ((opcode ^ 0xffffffff) != -9) {
					if ((opcode ^ 0xffffffff) == -10) {
						resetWhenWalk = buffer.readUnsignedByte((byte) 110);
					} else if ((opcode ^ 0xffffffff) == -11) {
						priority = buffer.readUnsignedByte((byte) 59);
					} else if ((opcode ^ 0xffffffff) != -12) {
						if (opcode != 12) {
							if ((opcode ^ 0xffffffff) != -14) {
								if (opcode != 14) {
									if ((opcode ^ 0xffffffff) == -16) {
										aBoolean825 = true;
									} else if ((opcode ^ 0xffffffff) != -17) {
										if (opcode != 18) {
											if (opcode != 19) {
												if (opcode == 20) {
													if (anIntArray810 == null || anIntArray815 == null) {
														anIntArray810 = new int[anIntArrayArray822.length];
														anIntArray815 = new int[anIntArrayArray822.length];
														for (int i_49_ = 0; (i_49_ ^ 0xffffffff) > (anIntArrayArray822.length ^ 0xffffffff); i_49_++) {
															anIntArray810[i_49_] = 256;
															anIntArray815[i_49_] = 256;
														}
													}
													int i_50_ = buffer.readUnsignedByte((byte) -108);
													anIntArray810[i_50_] = buffer.readShort((byte) 127);
													anIntArray815[i_50_] = buffer.readShort((byte) 127);
												}
											} else {
												if (anIntArray808 == null) {
													anIntArray808 = new int[anIntArrayArray822.length];
													for (int i_51_ = 0; (anIntArrayArray822.length ^ 0xffffffff) < (i_51_ ^ 0xffffffff); i_51_++) {
														anIntArray808[i_51_] = 255;
													}
												}
												anIntArray808[buffer.readUnsignedByte((byte) 25)] = buffer.readUnsignedByte((byte) -125);
											}
										} else {
											aBoolean812 = true;
										}
									} else {
										aBoolean823 = true;
									}
								} else {
									aBoolean817 = true;
								}
							} else {
								int i_52_ = buffer.readShort((byte) 127);
								anIntArrayArray822 = new int[i_52_][];
								for (int i_53_ = 0; (i_53_ ^ 0xffffffff) > (i_52_ ^ 0xffffffff); i_53_++) {
									int i_54_ = buffer.readUnsignedByte((byte) 125);
									if (i_54_ > 0) {
										anIntArrayArray822[i_53_] = new int[i_54_];
										anIntArrayArray822[i_53_][0] = buffer.readMediumInt(Class369.method3953(i_44_, -14837));
										for (int i_55_ = 1; (i_55_ ^ 0xffffffff) > (i_54_ ^ 0xffffffff); i_55_++) {
											anIntArrayArray822[i_53_][i_55_] = buffer.readShort((byte) 127);
										}
									}
								}
							}
						} else {
							int i_56_ = buffer.readUnsignedByte((byte) 11);
							anIntArray827 = new int[i_56_];
							for (int i_57_ = 0; i_57_ < i_56_; i_57_++) {
								anIntArray827[i_57_] = buffer.readShort((byte) 127);
							}
							for (int i_58_ = 0; i_58_ < i_56_; i_58_++) {
								anIntArray827[i_58_] = (buffer.readShort((byte) 127) << 1603208048) - -anIntArray827[i_58_];
							}
						}
					} else {
						anInt819 = buffer.readUnsignedByte((byte) -114);
					}
				} else {
					anInt807 = buffer.readUnsignedByte((byte) 17);
				}
			} else {
				mainhand = buffer.readShort((byte) 127);
			}
		} else {
			animationFlowControl = new boolean[256];
			int max = buffer.readUnsignedByte((byte) 33);
			for (int index = 0; max > index; index++) {
				animationFlowControl[buffer.readUnsignedByte((byte) -119)] = true;
			}
		}
	}

	public final void decode(RSByteBuffer buffer, int i) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) -99);
			if (opcode == 0) {
				break;
			}
			this.decode(opcode, buffer, 14735);
		}
	}

	public final ModelRenderer method930(byte i, int i_0_, int i_1_, int i_2_, int i_3_, byte i_4_, ModelRenderer model, int i_5_) {
		int i_6_ = frameLengths[i_1_];
		i_1_ = frameIds[i_1_];
		AnimationSkeletonSet skeleton = loader.method2624(2, i_1_ >> 2132424048);
		i_1_ &= 0xffff;
		if (skeleton == null) {
			return model.method2341(i, i_2_, true);
		}
		AnimationSkeletonSet skeleton1 = null;
		if ((aBoolean825 || Class357.forcedTweening) && (i_3_ ^ 0xffffffff) != 0 && (i_3_ ^ 0xffffffff) > (frameIds.length ^ 0xffffffff)) {
			i_3_ = frameIds[i_3_];
			skeleton1 = loader.method2624(2, i_3_ >> -768659376);
			i_3_ &= 0xffff;
		}
		if (aBoolean817) {
			i_2_ |= 0x200;
		}
		if (skeleton.method1619(i_1_, 31239)) {
			i_2_ |= 0x80;
		}
		if (skeleton.method1617(false, i_1_)) {
			i_2_ |= 0x100;
		}
		if (skeleton.method1615(i_1_, false)) {
			i_2_ |= 0x400;
		}
		if (skeleton1 != null) {
			if (skeleton1.method1619(i_3_, 31239)) {
				i_2_ |= 0x80;
			}
			if (skeleton1.method1617(false, i_3_)) {
				i_2_ |= 0x100;
			}
			if (skeleton1.method1615(i_3_, false)) {
				i_2_ |= 0x400;
			}
		}
		i_2_ |= 0x20;
		ModelRenderer rasterizer = model.method2341(i, i_2_, true);
		rasterizer.method2338(i_5_ - 1, skeleton, i_1_, skeleton1, aBoolean817, i_0_, -107, i_6_, i_3_);
		return rasterizer;
	}

	public final int method932(boolean bool, int i, boolean bool_11_, int i_12_) {
		int i_13_ = 0;
		int i_14_ = 0;
		int i_15_ = frameIds[i];
		AnimationSkeletonSet class98_sub46_sub16 = null;
		AnimationSkeletonSet class98_sub46_sub16_16_ = loader.method2624(2, i_15_ >> 729062096);
		i_15_ &= 0xffff;
		if (class98_sub46_sub16_16_ == null) {
			return i_13_;
		}
		if (bool_11_ != true) {
			return -128;
		}
		if ((aBoolean825 || Class357.forcedTweening) && i_12_ != -1 && frameIds.length > i_12_) {
			i_14_ = frameIds[i_12_];
			class98_sub46_sub16 = loader.method2624(2, i_14_ >> 1613080208);
			i_14_ &= 0xffff;
		}
		if (aBoolean817) {
			i_13_ |= 0x200;
		}
		if (class98_sub46_sub16_16_.method1619(i_15_, 31239)) {
			i_13_ |= 0x80;
		}
		if (class98_sub46_sub16_16_.method1617(false, i_15_)) {
			i_13_ |= 0x100;
		}
		if (class98_sub46_sub16_16_.method1615(i_15_, false)) {
			i_13_ |= 0x400;
		}
		if (class98_sub46_sub16 != null) {
			if (class98_sub46_sub16.method1619(i_14_, 31239)) {
				i_13_ |= 0x80;
			}
			if (class98_sub46_sub16.method1617(false, i_14_)) {
				i_13_ |= 0x100;
			}
			if (class98_sub46_sub16.method1615(i_14_, false)) {
				i_13_ |= 0x400;
			}
		}
		if (anIntArray827 != null && bool) {
			if ((i ^ 0xffffffff) > (anIntArray827.length ^ 0xffffffff)) {
				int i_17_ = anIntArray827[i];
				if ((i_17_ ^ 0xffffffff) != -65536) {
					AnimationSkeletonSet class98_sub46_sub16_18_ = loader.method2624(2, i_17_ >> -1481112880);
					i_17_ &= 0xffff;
					if (class98_sub46_sub16_18_ != null) {
						if (class98_sub46_sub16_18_.method1619(i_17_, 31239)) {
							i_13_ |= 0x80;
						}
						if (class98_sub46_sub16_18_.method1617(false, i_17_)) {
							i_13_ |= 0x100;
						}
						if (class98_sub46_sub16_18_.method1615(i_17_, false)) {
							i_13_ |= 0x400;
						}
					}
				}
			}
			if ((aBoolean825 || Class357.forcedTweening) && i_12_ != -1 && i_12_ < anIntArray827.length) {
				int i_19_ = anIntArray827[i_12_];
				if ((i_19_ ^ 0xffffffff) != -65536) {
					AnimationSkeletonSet class98_sub46_sub16_20_ = loader.method2624(2, i_19_ >> 1795578800);
					i_19_ &= 0xffff;
					if (class98_sub46_sub16_20_ != null) {
						if (class98_sub46_sub16_20_.method1619(i_19_, 31239)) {
							i_13_ |= 0x80;
						}
						if (class98_sub46_sub16_20_.method1617(false, i_19_)) {
							i_13_ |= 0x100;
						}
						if (class98_sub46_sub16_20_.method1615(i_19_, false)) {
							i_13_ |= 0x400;
						}
					}
				}
			}
		}
		return 0x20 | i_13_;
	}

	public final ModelRenderer method937(int i, int i_30_, int i_31_, int i_32_, ModelRenderer class146, int i_33_) {
		int i_34_ = frameLengths[i_33_];
		int i_35_ = frameIds[i_33_];
		AnimationSkeletonSet class98_sub46_sub16 = loader.method2624(2, i_35_ >> -2046821168);
		i_35_ &= 0xffff;
		if (class98_sub46_sub16 == null) {
			return class146.method2341((byte) 1, i_31_, true);
		}
		AnimationSkeletonSet class98_sub46_sub16_36_ = null;
		if ((aBoolean825 || Class357.forcedTweening) && i != -1 && (frameIds.length ^ 0xffffffff) < (i ^ 0xffffffff)) {
			i = frameIds[i];
			class98_sub46_sub16_36_ = loader.method2624(2, i >> 842978512);
			i &= 0xffff;
		}
		AnimationSkeletonSet class98_sub46_sub16_37_ = null;
		AnimationSkeletonSet class98_sub46_sub16_38_ = null;
		int i_39_ = 0;
		int i_40_ = 0;
		if (anIntArray827 != null) {
			if ((anIntArray827.length ^ 0xffffffff) < (i_33_ ^ 0xffffffff)) {
				i_39_ = anIntArray827[i_33_];
				if (i_39_ != 65535) {
					class98_sub46_sub16_37_ = loader.method2624(2, i_39_ >> -762103696);
					i_39_ &= 0xffff;
				}
			}
			if ((aBoolean825 || Class357.forcedTweening) && i != -1 && i < anIntArray827.length) {
				i_40_ = anIntArray827[i];
				if (i_40_ != 65535) {
					class98_sub46_sub16_38_ = loader.method2624(2, i_40_ >> 730805744);
					i_40_ &= 0xffff;
				}
			}
		}
		if (aBoolean817) {
			i_31_ |= 0x200;
		}
		if (class98_sub46_sub16.method1619(i_35_, 31239)) {
			i_31_ |= 0x80;
		}
		if (class98_sub46_sub16.method1617(false, i_35_)) {
			i_31_ |= 0x100;
		}
		if (class98_sub46_sub16.method1615(i_35_, false)) {
			i_31_ |= 0x400;
		}
		if (class98_sub46_sub16_37_ != null) {
			if (class98_sub46_sub16_37_.method1619(i_39_, 31239)) {
				i_31_ |= 0x80;
			}
			if (class98_sub46_sub16_37_.method1617(false, i_39_)) {
				i_31_ |= 0x100;
			}
			if (class98_sub46_sub16_37_.method1615(i_39_, false)) {
				i_31_ |= 0x400;
			}
		}
		if (class98_sub46_sub16_36_ != null) {
			if (class98_sub46_sub16_36_.method1619(i, 31239)) {
				i_31_ |= 0x80;
			}
			if (class98_sub46_sub16_36_.method1617(false, i)) {
				i_31_ |= 0x100;
			}
			if (class98_sub46_sub16_36_.method1615(i, false)) {
				i_31_ |= 0x400;
			}
		}
		if (class98_sub46_sub16_38_ != null) {
			if (class98_sub46_sub16_38_.method1619(i_40_, 31239)) {
				i_31_ |= 0x80;
			}
			if (class98_sub46_sub16_38_.method1617(false, i_40_)) {
				i_31_ |= 0x100;
			}
			if (class98_sub46_sub16_38_.method1615(i_40_, false)) {
				i_31_ |= 0x400;
			}
		}
		i_31_ |= 0x20;
		ModelRenderer class146_42_ = class146.method2341((byte) 1, i_31_, true);
		class146_42_.method2338(i_30_ - 1, class98_sub46_sub16, i_35_, class98_sub46_sub16_36_, aBoolean817, 0, 119, i_34_, i);
		if (class98_sub46_sub16_37_ != null) {
			class146_42_.method2338(-1 + i_30_, class98_sub46_sub16_37_, i_39_, class98_sub46_sub16_38_, aBoolean817, 0, -106, i_34_, i_40_);
		}
		return class146_42_;
	}

	public final void method938(int i) {
		do {
			do {
				if ((resetWhenWalk ^ 0xffffffff) == 0) {
					if (animationFlowControl == null) {
						resetWhenWalk = 0;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					resetWhenWalk = 2;
				}
			} while (false);
			if (priority != -1) {
				break;
			}
			if (animationFlowControl == null) {
				priority = 0;
			} else {
				priority = 2;
			}
			break;
		} while (false);
	}
}

/* Class379 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Char;
import com.RSByteBuffer;

public final class LightIntensityDefinition {
	public static boolean	aBoolean3192;
	public static Char[]	aClass246_Sub3Array3198;

	public int				anInt3193	= 0;
	public int				anInt3194;
	public int				anInt3195;

	public int				anInt3197	= 2048;

	public LightIntensityDefinition() {
		anInt3194 = 2048;
		anInt3195 = 0;
	}

	public final void decode(byte i, RSByteBuffer class98_sub22) {
		for (;;) {
			int i_0_ = class98_sub22.readUnsignedByte((byte) -108);
			if (i_0_ == 0) {
				break;
			}
			method4009(i_0_, class98_sub22, 116);
		}
	}

	private final void method4009(int i, RSByteBuffer class98_sub22, int i_1_) {
		if (i_1_ <= 88) {
			method4009(-8, null, -88);
		}
		if ((i ^ 0xffffffff) == -2) {
			anInt3195 = class98_sub22.readUnsignedByte((byte) 21);
		} else if (i != 2) {
			if ((i ^ 0xffffffff) != -4) {
				if (i == 4) {
					anInt3193 = class98_sub22.readUShort(false);
				}
			} else {
				anInt3194 = class98_sub22.readShort((byte) 127);
			}
		} else {
			anInt3197 = class98_sub22.readShort((byte) 127);
		}
	}
}

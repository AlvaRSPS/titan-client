/* Class90 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class105;
import com.Class138;
import com.Class246_Sub2;
import com.Class98_Sub50;
import com.GameShell;
import com.RSByteBuffer;
import com.jagex.core.crypto.ISAACPseudoRNG;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.SceneryShadowsPreferenceField;

public final class VarClientDefinition {
	public static Class105	aClass105_719	= new Class105("", 15);
	public static int		anInt721		= 4;

	public static void method881(byte i) {
		do {
			try {
				aClass105_719 = null;
				if (i == -27) {
					break;
				}
				method883(88);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fn.E(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method882(int[] is, long[] ls, byte i) {
		do {
			try {
				ISAACPseudoRNG.method2169(is, ls, 0, ls.length - 1, false);
				if (i == 118) {
					break;
				}
				aClass105_719 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fn.A(" + (is != null ? "{...}" : "null") + ',' + (ls != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public static final Class246_Sub2 method883(int i) {
		try {
			Class246_Sub2 class246_sub2 = (Class246_Sub2) Class138.aClass218_1084.removeFirst((byte) -72);
			if (class246_sub2 != null) {
				Class98_Sub50.anInt4294--;
				return class246_sub2;
			}
			if (i > 0) {
				return null;
			}
			return new Class246_Sub2();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fn.D(" + i + ')');
		}
	}

	public char	aChar720;

	public int	anInt718	= 1;

	public VarClientDefinition() {
		/* empty */
	}

	private final void method884(int i, RSByteBuffer class98_sub22, byte i_0_) {
		do {
			try {
				do {
					if ((i ^ 0xffffffff) == -2) {
						aChar720 = SceneryShadowsPreferenceField.method576(class98_sub22.readSignedByte((byte) -19), (byte) 122);
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					if ((i ^ 0xffffffff) == -3) {
						anInt718 = 0;
					}
				} while (false);
				if (i_0_ == -37) {
					break;
				}
				anInt718 = -63;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fn.B(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_0_ + ')');
			}
			break;
		} while (false);
	}

	public final void method885(RSByteBuffer class98_sub22, int i) {
		try {
			if (i == -23453) {
				for (;;) {
					int i_1_ = class98_sub22.readUnsignedByte((byte) 50);
					if (i_1_ == 0) {
						break;
					}
					method884(i_1_, class98_sub22, (byte) -37);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fn.C(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}
}

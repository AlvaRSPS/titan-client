/* Class98_Sub46_Sub12 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class48;
import com.NodeInteger;
import com.NodeString;
import com.RSByteBuffer;
import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;

public final class StructsDefinition extends Cacheable {
	public static boolean[][]	aBooleanArrayArray6034	= { new boolean[4], new boolean[4], { false, false, true, false }, { false, false, true, false }, { false, false, true, false }, { false, false, true, false }, { true, false, true, false }, { true, false, false, true }, { true, false, false,
			true }, new boolean[4], new boolean[4], new boolean[4], new boolean[4] };
	public static long			aLong6035				= 0L;

	public static void method1587(byte i) {
		do {
			try {
				aBooleanArrayArray6034 = null;
				if (i <= -67) {
					break;
				}
				aLong6035 = 71L;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "hd.D(" + i + ')');
			}
			break;
		} while (false);
	}

	private HashTable aClass377_6033;

	public StructsDefinition() {
		/* empty */
	}

	public final int method1585(int i, boolean bool, int i_0_) {
		try {
			if (aClass377_6033 == null) {
				return i_0_;
			}
			NodeInteger class98_sub34 = (NodeInteger) aClass377_6033.get(i, -1);
			if (bool != true) {
				method1586(62, (byte) 80, null);
			}
			if (class98_sub34 == null) {
				return i_0_;
			}
			return class98_sub34.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hd.B(" + i + ',' + bool + ',' + i_0_ + ')');
		}
	}

	public final String method1586(int i, byte i_1_, String string) {
		try {
			if (aClass377_6033 == null) {
				return string;
			}
			if (i_1_ != -19) {
				method1587((byte) 49);
			}
			NodeString class98_sub15 = (NodeString) aClass377_6033.get(i, i_1_ ^ 0x12);
			if (class98_sub15 == null) {
				return string;
			}
			return class98_sub15.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hd.C(" + i + ',' + i_1_ + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	public final void method1588(int i, RSByteBuffer class98_sub22) {
		try {
			if (i != 0) {
				aBooleanArrayArray6034 = null;
			}
			for (;;) {
				int i_2_ = class98_sub22.readUnsignedByte((byte) -98);
				if (i_2_ == 0) {
					break;
				}
				method1589(i_2_, true, class98_sub22);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hd.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	private final void method1589(int i, boolean bool, RSByteBuffer class98_sub22) {
		try {
			if (i == 249) {
				int i_3_ = class98_sub22.readUnsignedByte((byte) -128);
				if (aClass377_6033 == null) {
					int i_4_ = Class48.findNextGreaterPwr2(423660257, i_3_);
					aClass377_6033 = new HashTable(i_4_);
				}
				for (int i_5_ = 0; (i_3_ ^ 0xffffffff) < (i_5_ ^ 0xffffffff); i_5_++) {
					boolean bool_6_ = (class98_sub22.readUnsignedByte((byte) 41) ^ 0xffffffff) == -2;
					int i_7_ = class98_sub22.readMediumInt(-124);
					Node class98;
					if (bool_6_) {
						class98 = new NodeString(class98_sub22.readString((byte) 84));
					} else {
						class98 = new NodeInteger(class98_sub22.readInt(-2));
					}
					aClass377_6033.put(class98, i_7_, -1);
				}
			}
			if (bool != true) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hd.E(" + i + ',' + bool + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}
}

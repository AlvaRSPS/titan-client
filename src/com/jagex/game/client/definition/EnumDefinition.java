/* Class306 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.ChatStatus;
import com.Class48;
import com.Class98_Sub10_Sub31;
import com.Class98_Sub41;
import com.GlobalPlayer;
import com.NodeInteger;
import com.NodeString;
import com.PrefetchObject;
import com.RSByteBuffer;
import com.Sprite;
import com.SystemInformation;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.preferences.SceneryShadowsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageType;

public final class EnumDefinition {
	public static Sprite[]				aClass332Array2557;
	public static QuickChatMessageType	aClass348_2565		= new QuickChatMessageType(4, 1, 1, 1);
	public static GlobalPlayer[]		aClass376Array2562	= new GlobalPlayer[2048];
	public static int					anInt2561;
	public static int					anInt2566			= 0;

	public static final ChatStatus[] getAllStatuses(byte dummy) {
		return new ChatStatus[] { Class98_Sub10_Sub31.ON, GamePreferences.FRIENDS, PrefetchObject.OFF };
	}

	public char			aChar2560;
	public char			aChar2567;
	public HashTable	aClass377_2558;

	private HashTable	aClass377_2564;

	public int			anInt2563;

	public String		aString2559	= "null";

	public EnumDefinition() {
		/* empty */
	}

	public final void decode(RSByteBuffer buffer, int i) {
		for (;;) {
			int i_0_ = buffer.readUnsignedByte((byte) 115);
			if (i_0_ == 0) {
				break;
			}
			method3600(i + -4, buffer, i_0_);
		}
	}

	public final String get(int i, byte i_1_) {
		if (aClass377_2558 == null) {
			return aString2559;
		}
		NodeString class98_sub15 = (NodeString) aClass377_2558.get(i, -1);
		if (class98_sub15 == null) {
			return aString2559;
		}
		return class98_sub15.value;
	}

	public final boolean method3596(int i, byte i_2_) {
		if (aClass377_2558 == null) {
			return false;
		}
		if (aClass377_2564 == null) {
			method3599((byte) -59);
		}
		NodeInteger class98_sub34 = (NodeInteger) aClass377_2564.get(i, -1);
		return class98_sub34 != null;
	}

	public final int method3598(int i, int i_4_) {
		if (aClass377_2558 == null) {
			return anInt2563;
		}
		NodeInteger class98_sub34 = (NodeInteger) aClass377_2558.get(i, -1);
		if (i_4_ != -28629) {
			return -113;
		}
		if (class98_sub34 == null) {
			return anInt2563;
		}
		return class98_sub34.value;
	}

	private final void method3599(byte i) {
		aClass377_2564 = new HashTable(aClass377_2558.getHashResolution((byte) 106));
		NodeInteger class98_sub34 = (NodeInteger) aClass377_2558.startIteration(102);
		if (i < -19) {
			for (/**/; class98_sub34 != null; class98_sub34 = (NodeInteger) aClass377_2558.iterateNext(-1)) {
				NodeInteger class98_sub34_5_ = new NodeInteger((int) class98_sub34.hash);
				aClass377_2564.put(class98_sub34_5_, class98_sub34.value, -1);
			}
		}
	}

	private final void method3600(int i, RSByteBuffer buffer, int i_6_) {
		do {
			if (i_6_ == 1) {
				aChar2560 = SceneryShadowsPreferenceField.method576(buffer.readSignedByte((byte) -19), (byte) 121);
			} else if (i_6_ != 2) {
				if ((i_6_ ^ 0xffffffff) == -4) {
					aString2559 = buffer.readString((byte) 84);
				} else if (i_6_ == 4) {
					anInt2563 = buffer.readInt(-2);
				} else if ((i_6_ ^ 0xffffffff) == -6 || (i_6_ ^ 0xffffffff) == -7) {
					int i_7_ = buffer.readShort((byte) 127);
					aClass377_2558 = new HashTable(Class48.findNextGreaterPwr2(423660257, i_7_));
					for (int i_8_ = 0; i_7_ > i_8_; i_8_++) {
						int i_9_ = buffer.readInt(-2);
						Node class98;
						if ((i_6_ ^ 0xffffffff) == -6) {
							class98 = new NodeString(buffer.readString((byte) 84));
						} else {
							class98 = new NodeInteger(buffer.readInt(-2));
						}
						aClass377_2558.put(class98, i_9_, i ^ 0xffffffff);
					}
				}
			} else {
				aChar2567 = SceneryShadowsPreferenceField.method576(buffer.readSignedByte((byte) -19), (byte) 125);
			}
			break;
		} while (false);
	}

	private final void method3601(int i) {
		aClass377_2564 = new HashTable(aClass377_2558.getHashResolution((byte) 53));
		for (NodeString class98_sub15 = (NodeString) aClass377_2558.startIteration(i + 111); class98_sub15 != null; class98_sub15 = (NodeString) aClass377_2558.iterateNext(i + 3)) {
			Class98_Sub41 class98_sub41 = new Class98_Sub41(class98_sub15.value, (int) class98_sub15.hash);
			aClass377_2564.put(class98_sub41, SystemInformation.method2313((byte) -121, class98_sub15.value), -1);
		}
	}

	public final boolean method3602(String string, int i) {
		if (aClass377_2558 == null) {
			return false;
		}
		if (aClass377_2564 == null) {
			method3601(-4);
		}
		Class98_Sub41 class98_sub41 = (Class98_Sub41) aClass377_2564.get(SystemInformation.method2313((byte) -124, string), -1);
		if (i != -16972) {
			return false;
		}
		for (/**/; class98_sub41 != null; class98_sub41 = (Class98_Sub41) aClass377_2564.nextHashCollision(126)) {
			if (class98_sub41.aString4201.equals(string)) {
				return true;
			}
		}
		return false;
	}
}

/* Class266 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.ActionQueueEntry;
import com.Animable;
import com.Char;
import com.Class100;
import com.Class130;
import com.Class140;
import com.Class151_Sub7;
import com.Class151_Sub8;
import com.Class172;
import com.Class226;
import com.Class349;
import com.Class358;
import com.Class359;
import com.Class39_Sub1;
import com.Class76_Sub9;
import com.Class78;
import com.Class81;
import com.Class84;
import com.IncomingOpcode;
import com.JavaThreadResource;
import com.Mob;
import com.Player;
import com.RSByteBuffer;
import com.client;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.ui.loading.LoadingScreen;
import com.jagex.game.client.ui.loading.LoadingScreenRenderer;
import com.jagex.game.toolkit.matrix.Matrix;

public final class SunDefinition {
	public static Matrix			aClass111_1986;
	public static IncomingOpcode	aClass58_1992	= new IncomingOpcode(82, 0);
	public static Class84			aClass84_1988	= new Class84(true);
	public static ActionQueueEntry	aClass98_Sub46_Sub8_1994;

	public static final void method3234(Player player, int i, int[] is, int[] is_0_, int[] is_1_) {
		for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > (is_1_.length ^ 0xffffffff); i_2_++) {
			int i_3_ = is_1_[i_2_];
			int i_4_ = is[i_2_];
			int i_5_ = is_0_[i_2_];
			for (int i_6_ = 0; i_4_ != 0 && player.aClass226Array6387.length > i_6_; i_6_++) {
				if ((i_4_ & 0x1) != 0) {
					if ((i_3_ ^ 0xffffffff) == 0) {
						((Mob) player).aClass226Array6387[i_6_] = null;
					} else {
						AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_3_, 16383);
						int i_7_ = class97.anInt819;
						Class226 class226 = player.aClass226Array6387[i_6_];
						if (class226 != null) {
							if (i_3_ != class226.anInt1700) {
								if (class97.anInt829 >= Class151_Sub7.animationDefinitionList.method2623(class226.anInt1700, 16383).anInt829) {
									class226 = ((Mob) player).aClass226Array6387[i_6_] = null;
								}
							} else if (i_7_ == 0) {
								class226 = ((Mob) player).aClass226Array6387[i_6_] = null;
							} else if (i_7_ != 1) {
								if (i_7_ == 2) {
									class226.anInt1704 = 0;
								}
							} else {
								class226.anInt1707 = 0;
								class226.anInt1704 = 0;
								class226.anInt1703 = i_5_;
								class226.anInt1702 = 0;
								class226.anInt1701 = 1;
								if (!player.aBoolean6371) {
									Class349.method3840((byte) -128, player, 0, class97);
								}
							}
						}
						if (class226 == null) {
							class226 = ((Mob) player).aClass226Array6387[i_6_] = new Class226();
							class226.anInt1703 = i_5_;
							class226.anInt1707 = 0;
							class226.anInt1700 = i_3_;
							class226.anInt1702 = 0;
							class226.anInt1704 = 0;
							class226.anInt1701 = 1;
							if (!player.aBoolean6371) {
								Class349.method3840((byte) -126, player, 0, class97);
							}
						}
					}
				}
				i_4_ >>>= 1;
			}
		}
	}

	public static final void method3235(byte i) {
		try {
			if (i <= -17) {
				if (Class39_Sub1.loadingScreens != null) {
					LoadingScreen[] interface10s = Class39_Sub1.loadingScreens;
					for (LoadingScreen interface10 : interface10s) {
						interface10.loadMedia(-31295);
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ql.A(" + i + ')');
		}
	}

	public static final void method3238(int i) {
		try {
			if (client.LOAD_JS5_INDICES != null) {
				Class140.loadingScreenRenderer = new LoadingScreenRenderer();
				Class140.loadingScreenRenderer.setProgress(JavaThreadResource.aLong1753, (byte) 95, client.LOAD_JS5_INDICES.percentageStart, client.LOAD_JS5_INDICES, client.LOAD_JS5_INDICES.fetchingMessages.getText(client.gameLanguage, (byte) 25));
				Class76_Sub9.aThread3783 = new Thread(Class140.loadingScreenRenderer, "");
				if (i == 0) {
					Class76_Sub9.aThread3783.start();
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ql.B(" + i + ')');
		}
	}

	public static final void method3239(int i, int i_10_, int i_11_, int i_12_, Animable class246_sub3_sub2) {
		Class172 class172 = Class100.method1693(i, i_10_, i_11_);
		if (class172 != null) {
			class246_sub3_sub2.boundExtentsX = (i_10_ << Class151_Sub8.tileScale) + Js5.anInt1577;
			class246_sub3_sub2.anInt5089 = i_12_;
			class246_sub3_sub2.boundExtentsZ = (i_11_ << Class151_Sub8.tileScale) + Js5.anInt1577;
			class172.aClass246_Sub3_Sub2_1331 = class246_sub3_sub2;
			int i_13_ = Class78.aSArray594 == Class81.aSArray618 ? 1 : 0;
			if (class246_sub3_sub2.method2978(-2)) {
				if (class246_sub3_sub2.method2987(6540)) {
					class246_sub3_sub2.animator = Class359.aClass246_Sub3Array3056[i_13_];
					Class359.aClass246_Sub3Array3056[i_13_] = class246_sub3_sub2;
				} else {
					class246_sub3_sub2.animator = LightIntensityDefinition.aClass246_Sub3Array3198[i_13_];
					LightIntensityDefinition.aClass246_Sub3Array3198[i_13_] = class246_sub3_sub2;
					Class358.aBoolean3033 = true;
				}
			} else {
				class246_sub3_sub2.animator = Class130.aClass246_Sub3Array1029[i_13_];
				Class130.aClass246_Sub3Array1029[i_13_] = class246_sub3_sub2;
			}
		}
	}

	public boolean	aBoolean1985;
	public int		anInt1984	= 8;
	public int		anInt1987;

	public int		anInt1989;

	public int		anInt1990;

	public int		anInt1991	= 16777215;

	public int		anInt1993;

	public int		anInt1995;

	public SunDefinition() {
		/* empty */
	}

	public final void method3236(RSByteBuffer class98_sub22, byte i) {
		try {
			if (i != -16) {
				method3239(-39, -58, -29, 127, null);
			}
			for (;;) {
				int i_9_ = class98_sub22.readUnsignedByte((byte) 15);
				if (i_9_ == 0) {
					break;
				}
				method3237(false, i_9_, class98_sub22);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ql.E(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private final void method3237(boolean bool, int i, RSByteBuffer class98_sub22) {
		do {
			try {
				if (i == 1) {
					anInt1984 = class98_sub22.readShort((byte) 127);
				} else if (i == 2) {
					aBoolean1985 = true;
				} else if (i == 3) {
					anInt1990 = class98_sub22.readUShort(bool);
					anInt1989 = class98_sub22.readUShort(false);
					anInt1987 = class98_sub22.readUShort(false);
				} else if ((i ^ 0xffffffff) != -5) {
					if (i == 5) {
						anInt1995 = class98_sub22.readShort((byte) 127);
					} else if (i == 6) {
						anInt1991 = class98_sub22.readMediumInt(-123);
					}
				} else {
					anInt1993 = class98_sub22.readUnsignedByte((byte) -108);
				}
				if (bool == false) {
					break;
				}
				aClass98_Sub46_Sub8_1994 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ql.C(" + bool + ',' + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}
}

/* Class9 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class69;
import com.Class98_Sub10_Sub39;
import com.Class98_Sub46_Sub5;
import com.Image;
import com.RSByteBuffer;
import com.RSToolkit;
import com.Sprite;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;

public final class MapScenesDefinition {

	public static final void method194() {
		for (;;) {
			boolean bool = true;
			for (int i = 0; i < Class98_Sub46_Sub5.aClass174Array5970.length; i++) {
				if (!Class98_Sub46_Sub5.aClass174Array5970[i].method2563()) {
					synchronized (Class98_Sub46_Sub5.aClass174Array5970[i]) {
						Class98_Sub46_Sub5.aClass174Array5970[i].notify();
					}
					bool = false;
				} else {
					Class98_Sub10_Sub39.aLongArray5772[i] = Class98_Sub46_Sub5.aClass174Array5970[i].method2562();
				}
			}
			if (bool) {
				break;
			}
			try {
				TimeTools.sleep(0, 1L);
			} catch (Exception exception) {
				/* empty */
			}
		}
		Class98_Sub46_Sub5.aClass174Array5970[Class98_Sub46_Sub5.aClass174Array5970.length - 1].method2561();
		Class69.method701(1);
		for (;;) {
			boolean bool = true;
			for (int i = 0; i < Class98_Sub46_Sub5.aClass174Array5970.length - 1; i++) {
				if (!Class98_Sub46_Sub5.aClass174Array5970[i].method2563()) {
					synchronized (Class98_Sub46_Sub5.aClass174Array5970[i]) {
						Class98_Sub46_Sub5.aClass174Array5970[i].notify();
					}
					bool = false;
				}
			}
			if (bool) {
				break;
			}
			try {
				TimeTools.sleep(0, 1L);
			} catch (Exception exception) {
				/* empty */
			}
		}
		for (int i = 1; i < Class98_Sub46_Sub5.aClass174Array5970.length - 2; i++) {
			Class98_Sub46_Sub5.aClass174Array5970[i].method2561();
		}
		Class69.method701(2);
		while (!Class98_Sub46_Sub5.aClass174Array5970[0].method2563()) {
			synchronized (Class98_Sub46_Sub5.aClass174Array5970[0]) {
				Class98_Sub46_Sub5.aClass174Array5970[0].notify();
			}
			try {
				TimeTools.sleep(0, 1L);
			} catch (Exception exception) {
				/* empty */
			}
		}
		Class98_Sub46_Sub5.aClass174Array5970[0].method2561();
	}

	public boolean						aBoolean116	= false;

	public MapScenesDefinitionParser	aClass335_117;

	public int							anInt114;

	public int							anInt115;

	public MapScenesDefinition() {
		/* empty */
	}

	public final Sprite method190(boolean bool, int i, int i_1_, RSToolkit var_ha) {
		try {
			long l = i_1_ << -529389104 | anInt114 | (!bool ? 0 : 262144) | var_ha.id << -1675288589;
			Sprite class332 = (Sprite) aClass335_117.aClass79_2818.get(-119, l);
			if (class332 != null) {
				return class332;
			}
			if (!aClass335_117.aClass207_2814.isFileCached(-51, anInt114)) {
				return null;
			}
			Image class324 = Image.loadImage(aClass335_117.aClass207_2814, anInt114, i);
			if (class324 != null) {
				class324.anInt2724 = class324.anInt2725 = class324.anInt2719 = class324.anInt2721 = 0;
				if (bool) {
					class324.flipVertical();
				}
				for (int i_2_ = 0; i_2_ < i_1_; i_2_++) {
					class324.method3687();
				}
			}
			class332 = var_ha.createSprite(class324, true);
			if (class332 != null) {
				aClass335_117.aClass79_2818.put(l, class332, (byte) -80);
			}
			return class332;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "an.C(" + bool + ',' + i + ',' + i_1_ + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	public final boolean method191(int i) {
		try {
			return aClass335_117.aClass207_2814.isFileCached(-68, anInt114);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "an.F(" + i + ')');
		}
	}

	public final void method192(RSByteBuffer class98_sub22, boolean bool) {
		for (;;) {
			int i = class98_sub22.readUnsignedByte((byte) -118);
			if ((i ^ 0xffffffff) == -1) {
				break;
			}
			method193(class98_sub22, (byte) -43, i);
		}
	}

	private final void method193(RSByteBuffer class98_sub22, byte i, int i_4_) {
		do {
			if ((i_4_ ^ 0xffffffff) == -2) {
				anInt114 = class98_sub22.readShort((byte) 127);
			} else if ((i_4_ ^ 0xffffffff) == -3) {
				anInt115 = class98_sub22.readMediumInt(i ^ 0x56);
			} else if (i_4_ == 3) {
				aBoolean116 = true;
			} else {
				if (i_4_ != 4) {
					break;
				}
				anInt114 = -1;
			}
			break;
		} while (false);
	}
}

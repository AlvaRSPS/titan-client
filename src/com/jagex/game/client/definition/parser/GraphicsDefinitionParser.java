/* Class304 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Class314;
import com.Class329;
import com.GameDefinition;
import com.LoadingScreenSequence;
import com.RSByteBuffer;
import com.RtInterface;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.GraphicsDefinition;

public final class GraphicsDefinitionParser {
	public static int		anInt2533	= 0;
	public static short[][]	aShortArrayArray2534;
	public static int[]		maleParts	= { 0, 1, 2, 3, 4, 5, 6, 14 };

	public static final void method3563(int i, RtInterface class293, int i_3_, int i_4_) {
		WorldMapInfoDefinitionParser.anInt2858 = i_3_;
		if (i_4_ == 60) {
			Class314.anInt2690 = i;
			LoadingScreenSequence.aClass293_2129 = class293;
		}
	}

	public int					anInt2539;
	public AdvancedMemoryCache	cache			= new AdvancedMemoryCache(60);

	private Js5					configJs5;

	public Js5					models;

	private AdvancedMemoryCache	recentlyUsed	= new AdvancedMemoryCache(64);

	public GraphicsDefinitionParser(GameDefinition class279, int i, Js5 class207, Js5 class207_7_) {
		models = class207_7_;
		configJs5 = class207;
		int i_8_ = configJs5.getGroupCount((byte) -11) + -1;
		configJs5.getFileCount(0, i_8_);
	}

	public final void clear(byte i) {
		synchronized (recentlyUsed) {
			recentlyUsed.clear(46);
		}
		if (i == 0) {
			synchronized (cache) {
				cache.clear(89);
			}
		}
	}

	public final void freeSoftReferences(int i) {
		synchronized (recentlyUsed) {
			recentlyUsed.freeSoftReferences((byte) -126);
		}
		synchronized (cache) {
			cache.freeSoftReferences((byte) 23);
		}
	}

	public final void makeSoftReferences(byte i, int i_6_) {
		synchronized (recentlyUsed) {
			recentlyUsed.makeSoftReferences((byte) 62, i_6_);
		}
		synchronized (cache) {
			cache.makeSoftReferences((byte) 62, i_6_);
		}
	}

	public final void method3560(int i, int i_0_) {
		anInt2539 = i;
		synchronized (cache) {
			cache.clear(49);
		}
	}

	public final GraphicsDefinition method3564(int i, int i_5_) {
		GraphicsDefinition definition;
		synchronized (recentlyUsed) {
			definition = (GraphicsDefinition) recentlyUsed.get(-123, i_5_);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (configJs5) {
			is = configJs5.getFile(Class314.method3637(i ^ ~0x3a87, i_5_), Class329.method3711((byte) 117, i_5_), false);
			if (i != 2) {
				anInt2539 = 42;
			}
		}
		definition = new GraphicsDefinition();
		definition.parser = this;
		definition.anInt925 = i_5_;
		if (is != null) {
			definition.decode(i + -2, new RSByteBuffer(is));
		}
		synchronized (recentlyUsed) {
			recentlyUsed.put(i_5_, definition, (byte) -80);
		}
		return definition;
	}
}

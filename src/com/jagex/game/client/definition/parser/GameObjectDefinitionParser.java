/* Class302 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import java.util.Arrays;

import com.Class151;
import com.Class65;
import com.GameDefinition;
import com.IncomingOpcode;
import com.OutgoingOpcode;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.crypto.DumpUtils;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.toolkit.heap.Heap;

public final class GameObjectDefinitionParser {
	public static boolean			aBoolean2526;
	public static OutgoingOpcode	aClass171_2520		= new OutgoingOpcode(47, 3);
	public static int				groundOccludedCount;
	public static int				opaqueOnscreenCount;
	public static int				anInt2524;
	public static int[]				anIntArray2521		= new int[4];
	public static int[]				regionUwTerrainId;
	public static IncomingOpcode	SEND_GROUND_ITEM	= new IncomingOpcode(29, 5);

	public static void method3551(int i) {
		try {
			aClass171_2520 = null;
			anIntArray2521 = null;
			SEND_GROUND_ITEM = null;
			if (i > -91) {
				method3551(92);
			}
			regionUwTerrainId = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sia.C(" + i + ')');
		}
	}

	public static final boolean method3553(int i, byte i_4_) {
		try {
			int i_5_ = i_4_ & 0xff;
			if ((i_5_ ^ 0xffffffff) == -1) {
				return false;
			}
			if ((i_5_ ^ 0xffffffff) <= -129 && i_5_ < 160 && (Class65.unicodeUnescapes[i_5_ - 128] ^ 0xffffffff) == -1) {
				return false;
			}
			if (i > -116) {
				anInt2524 = 55;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sia.I(" + i + ',' + i_4_ + ')');
		}
	}

	public boolean				aBoolean2513	= false;
	public boolean				aBoolean2516;
	public Js5					aClass207_2512;
	public Js5					aClass207_2515;
	public AdvancedMemoryCache	aClass79_2522	= new AdvancedMemoryCache(500);
	public AdvancedMemoryCache	aClass79_2525	= new AdvancedMemoryCache(30);
	public AdvancedMemoryCache	aClass79_2527	= new AdvancedMemoryCache(50);

	public int					anInt2528;

	public AdvancedMemoryCache	cache			= new AdvancedMemoryCache(64);

	public GameObjectDefinitionParser(GameDefinition game, int i, boolean bool, Js5 class207, Js5 class207_7_) {
		do {
			aBoolean2516 = bool;
			aClass207_2515 = class207;
			aClass207_2512 = class207_7_;
			if (aClass207_2515 == null) {
				break;
			}
			int i_8_ = -1 + aClass207_2515.getGroupCount((byte) -11);
			aClass207_2515.getFileCount(0, i_8_);
			break;
		} while (false);
	}

	public final GameObjectDefinition get(int i, byte x) {
		GameObjectDefinition definition;
		synchronized (cache) {
			definition = (GameObjectDefinition) cache.get(-125, i);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (aClass207_2515) {
			is = aClass207_2515.getFile(Class151.method2444(i, -119), Heap.method1674(-1035933944, i), false);
		}
		definition = new GameObjectDefinition();
		definition.id = i;
		definition.parser = this;
		if (is != null) {
			definition.decode(new RSByteBuffer(is), false);
		}
		definition.method3865(118);
		if (!aBoolean2516 && definition.aBoolean2927) {
			definition.anIntArray2934 = null;
			definition.options = null;

			//System.out.println("length of " + definition.id + " is " + Arrays.toString(definition.options) + " bytes");
			if (definition.id == 12267)
				System.out.println("length of " + definition.id + " is " + Arrays.toString(definition.transformIDs) + " bytes");
		}
		if (definition.clippingFlag) {
			definition.walkable = false;
			definition.actionCount = 0;
		}
		if (definition.id == 12266)
			DumpUtils.reflectionDump(GameObjectDefinition.class, definition);
		if (definition.id == 12266)
			System.out.println("length of " + definition.id + " is " + Arrays.toString(definition.transformIDs) + " bytes");
		synchronized (cache) {
			cache.put(i, definition, (byte) -80);
		}
		return definition;
	}

	public final void method3547(byte i) {
		synchronized (cache) {
			cache.freeSoftReferences((byte) -90);
		}
		if (i > 126) {
			synchronized (aClass79_2522) {
				aClass79_2522.freeSoftReferences((byte) -105);
			}
			synchronized (aClass79_2525) {
				aClass79_2525.freeSoftReferences((byte) 99);
			}
			synchronized (aClass79_2527) {
				aClass79_2527.freeSoftReferences((byte) 8);
			}
		}
	}

	public final void method3548(int i, byte i_1_) {
		synchronized (cache) {
			cache.makeSoftReferences((byte) 62, i);
		}
		synchronized (aClass79_2522) {
			aClass79_2522.makeSoftReferences((byte) 62, i);
		}
		synchronized (aClass79_2525) {
			aClass79_2525.makeSoftReferences((byte) 62, i);
		}
		if (i_1_ > 36) {
			synchronized (aClass79_2527) {
				aClass79_2527.makeSoftReferences((byte) 62, i);
			}
		}
	}

	public final void method3549(byte i) {
		synchronized (cache) {
			cache.clear(72);
		}
		synchronized (aClass79_2522) {
			aClass79_2522.clear(103);
		}
		synchronized (aClass79_2525) {
			aClass79_2525.clear(119);
		}
		synchronized (aClass79_2527) {
			aClass79_2527.clear(123);
		}
	}

	public final void method3550(int i, int i_3_) {
		if (i != -129) {
			opaqueOnscreenCount = -114;
		}
		cache = new AdvancedMemoryCache(i_3_);
	}

	public final void method3552(boolean bool, int i) {
		do {
			if (aBoolean2513 != bool) {
				aBoolean2513 = bool;
				method3549((byte) 100);
				if (i > 18) {
					break;
				}
				aClass79_2525 = null;
			}
			break;
		} while (false);
	}

	public final void method3554(boolean bool, int i) {
		anInt2528 = i;
		synchronized (aClass79_2522) {
			aClass79_2522.clear(91);
		}
		synchronized (aClass79_2525) {
			aClass79_2525.clear(102);
			if (bool != true) {
				groundOccludedCount = 125;
			}
		}
		synchronized (aClass79_2527) {
			aClass79_2527.clear(14);
		}
	}

	public final void setAllowMembers(byte i, boolean bool) {
		if (bool == !aBoolean2516) {
			aBoolean2516 = bool;
			method3549((byte) 123);
		}
	}
}

/* Class121 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Class2;
import com.Class224_Sub3_Sub1;
import com.Class98_Sub10_Sub21;
import com.GameDefinition;
import com.OutgoingOpcode;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class HitmarksDefinitionParser {
	public static OutgoingOpcode		aClass171_1001	= new OutgoingOpcode(64, 3);
	public static FontSpecifications	aClass197_1004;
	public static int[]					regionPositionHash;
	public static int					b12FullIndex;
	public static int					p11FullIndex;
	public static int					p12FullIndex;

	public static final int[] getFontIds() {
		return new int[] { p11FullIndex, p12FullIndex, b12FullIndex };
	}

	public static final void method2195(int i) {
		Class2.playerCount = 0;
		for (int i_3_ = 0; (i_3_ ^ 0xffffffff) > -2049; i_3_++) {
			Class224_Sub3_Sub1.aClass98_Sub22Array6146[i_3_] = null;
			Class98_Sub10_Sub21.playerMovementSpeeds[i_3_] = (byte) 1;
			EnumDefinition.aClass376Array2562[i_3_] = null;
		}
	}

	public static final boolean method2198(int i, int i_4_, int i_5_, byte[] is, int i_6_, int i_7_, int i_8_) {
		int i_9_ = i_5_ % i_6_;
		int i_10_;
		if (i_9_ == 0) {
			i_10_ = 0;
		} else {
			i_10_ = i_6_ - i_9_;
		}
		int i_11_ = -((i_8_ - (-i_6_ + 1)) / i_6_);
		int i_12_ = -((-1 + i_5_ + i_6_) / i_6_);
		if (i_7_ != 14849) {
			aClass171_1001 = null;
		}
		for (int i_13_ = i_11_; i_13_ < 0; i_13_++) {
			for (int i_14_ = i_12_; i_14_ < 0; i_14_++) {
				if ((is[i] ^ 0xffffffff) == -1) {
					return true;
				}
				i += i_6_;
			}
			i -= i_10_;
			if ((is[-1 + i] ^ 0xffffffff) == -1) {
				return true;
			}
			i += i_4_;
		}
		return false;
	}

	public AdvancedMemoryCache	cache			= new AdvancedMemoryCache(20);

	private Js5					configJs5;

	public Js5					images;

	private AdvancedMemoryCache	recentlyUsed	= new AdvancedMemoryCache(64);

	public HitmarksDefinitionParser(GameDefinition class279, int i, Js5 class207, Js5 images) {
		configJs5 = class207;
		this.images = images;
		configJs5.getFileCount(0, 46);
	}

	public final void clear(byte i) {
		synchronized (recentlyUsed) {
			recentlyUsed.clear(95);
		}
		synchronized (cache) {
			cache.clear(94);
		}
	}

	public final void freeSoftReferences(int i) {
		synchronized (recentlyUsed) {
			recentlyUsed.freeSoftReferences((byte) -104);
		}
		synchronized (cache) {
			cache.freeSoftReferences((byte) 49);
		}
	}

	public final void makeSoftReferences(int i, byte i_0_) {
		synchronized (recentlyUsed) {
			recentlyUsed.makeSoftReferences((byte) 62, i);
		}
		synchronized (cache) {
			cache.makeSoftReferences((byte) 62, i);
		}
	}

	public final HitmarksDefinition method2194(int i, int i_1_) {
		HitmarksDefinition definition;
		synchronized (recentlyUsed) {
			definition = (HitmarksDefinition) recentlyUsed.get(-120, i_1_);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (configJs5) {
			is = configJs5.getFile(i_1_, 46, false);
		}
		definition = new HitmarksDefinition();
		definition.loader = this;
		if (is != null) {
			definition.decode(new RSByteBuffer(is), -1);
		}
		synchronized (recentlyUsed) {
			recentlyUsed.put(i_1_, definition, (byte) -80);
		}
		return definition;
	}
}

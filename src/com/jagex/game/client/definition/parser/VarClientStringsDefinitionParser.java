/* Class239 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Class151_Sub8;
import com.Class155;
import com.Class172;
import com.DummyOutputStream;
import com.GameDefinition;
import com.RSToolkit;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.VolumePreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class VarClientStringsDefinitionParser {
	public static boolean	aBoolean1839	= false;
	public static boolean	aBoolean1840	= false;
	public static int		anInt1841;
	public static int		anInt1843		= 999999;
	public static int		anInt1844;

	static {
		anInt1841 = 1401;
		anInt1844 = 0;
	}

	public static final void method2921(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		try {
			if (i_5_ != 8) {
				method2921(-3, 122, -57, -125, 9, -70, 106);
			}
			if ((i_4_ ^ 0xffffffff) == -9 || i_4_ == 16) {
				if (i_4_ != 8) {
					int i_6_ = NativeShadow.anInt6333 + (i << Class151_Sub8.tileScale);
					int i_7_ = -NativeShadow.anInt6333 + i_6_;
					int i_8_ = i_1_ << Class151_Sub8.tileScale;
					int i_9_ = NativeShadow.anInt6333 + i_8_;
					int i_10_ = StrongReferenceMCNode.aSArray6298[i_0_].getTileHeight(i_1_, i_5_ ^ ~0x3156, 1 + i);
					int i_11_ = StrongReferenceMCNode.aSArray6298[i_0_].getTileHeight(1 + i_1_, -12639, i);
					AwtGraphicsBuffer.aClass155Array5889[RSToolkit.anInt936++] = new Class155(i_4_, i_0_, i_6_, i_7_, i_7_, i_6_, i_10_, i_11_, -i_3_ + i_11_, i_10_ - i_3_, i_8_, i_9_, i_9_, i_8_);
				} else {
					int i_12_ = i << Class151_Sub8.tileScale;
					int i_13_ = NativeShadow.anInt6333 + i_12_;
					int i_14_ = i_1_ << Class151_Sub8.tileScale;
					int i_15_ = i_14_ - -NativeShadow.anInt6333;
					int i_16_ = StrongReferenceMCNode.aSArray6298[i_0_].getTileHeight(i_1_, -12639, i);
					int i_17_ = StrongReferenceMCNode.aSArray6298[i_0_].getTileHeight(i_1_ - -1, -12639, i - -1);
					AwtGraphicsBuffer.aClass155Array5889[RSToolkit.anInt936++] = new Class155(i_4_, i_0_, i_12_, i_13_, i_13_, i_12_, i_16_, i_17_, i_17_ + -i_3_, i_16_ + -i_3_, i_14_, i_15_, i_15_, i_14_);
				}
			} else {
				Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i_0_][i][i_1_];
				if (class172 == null) {
					class172 = new Class172(i_0_);
				}
				if ((i_4_ ^ 0xffffffff) == -2) {
					class172.aShort1335 = (short) i_3_;
					class172.aShort1329 = (short) i_2_;
				} else if (i_4_ == 2) {
					class172.aShort1328 = (short) i_3_;
					class172.aShort1323 = (short) i_2_;
				}
				if (DummyOutputStream.aBoolean35) {
					VolumePreferenceField.method644(-44);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pc.A(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ')');
		}
	}

	private Js5	aClass207_1842;

	public int	anInt1838;

	public VarClientStringsDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		new AdvancedMemoryCache(64);
		aClass207_1842 = class207;
		anInt1838 = aClass207_1842.getFileCount(0, 15);
	}
}


/* Class8 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.ActionQueueEntry;
import com.Class170;
import com.Class246_Sub3_Sub3;
import com.Class287_Sub2;
import com.Class336;
import com.Class38;
import com.Class98_Sub10_Sub9;
import com.Class98_Sub4;
import com.Class98_Sub43;
import com.Class98_Sub43_Sub1;
import com.GameDefinition;
import com.IncomingOpcode;
import com.RSByteBuffer;
import com.RtInterface;
import com.TextResources;
import com.client;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.InventoriesDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.input.impl.AwtMouseEvent;

public final class InventoriesDefinitionParser {
	public static LinkedList		aClass148_110;
	public static IncomingOpcode	SEND_CLAN_CHAT_MESSAGE	= new IncomingOpcode(35, -1);

	public static final void method186(int i, RtInterface class293, int i_1_, int i_2_) {
		do {
			try {
				if (Class98_Sub10_Sub9.aBoolean5585) {
					ParamDefinition class149 = (QuickChatCategory.anInt5945 ^ 0xffffffff) != 0 ? Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, QuickChatCategory.anInt5945) : null;
					if (client.method116(class293).method1667((byte) 74) && (Class98_Sub4.anInt3826 & 0x20 ^ 0xffffffff) != -1 && (class149 == null || (class293.method3472(i ^ 0x56da, class149.defaultInteger, QuickChatCategory.anInt5945) ^ 0xffffffff) != (class149.defaultInteger ^ 0xffffffff))) {
						ActionQueueEntry.addAction(false, true, 0L, Class336.anInt2823, class293.componentIndex, Class246_Sub3_Sub3.applyMenuText + " -> " + class293.applyText, false, class293.idDword, 59, class293.componentIndex << 1322373568 | class293.idDword, class293.unknown, false,
								Class287_Sub2.aString3272);
					}
				}
				if (i == 59) {
					for (int i_3_ = 9; i_3_ >= 5; i_3_--) {
						String string = Class38.method347(class293, (byte) 66, i_3_);
						if (string != null) {
							VarClientDefinitionParser.anInt1046++;
							ActionQueueEntry.addAction(false, true, i_3_ + 1, RtInterface.method3991(class293, -127, i_3_), class293.componentIndex, class293.applyText, false, class293.idDword, 1006, class293.idDword | class293.componentIndex << 75346272, class293.unknown, false, string);
						}
					}
					String string = Class170.method2538(-1, class293);
					if (string != null) {
						ActionQueueEntry.addAction(false, true, 0L, class293.anInt2254, class293.componentIndex, class293.applyText, false, class293.idDword, 21, class293.componentIndex << -1538321408 | class293.idDword, class293.unknown, false, string);
					}
					for (int i_4_ = 4; i_4_ >= 0; i_4_--) {
						String string_5_ = Class38.method347(class293, (byte) 65, i_4_);
						if (string_5_ != null) {
							VarClientDefinitionParser.anInt1046++;
							ActionQueueEntry.addAction(false, true, i_4_ - -1, RtInterface.method3991(class293, i ^ ~0x49, i_4_), class293.componentIndex, class293.applyText, false, class293.idDword, 49, class293.idDword | class293.componentIndex << -2042533440, class293.unknown, false, string_5_);
						}
					}
					if (!client.method116(class293).method1669(1964468)) {
						break;
					}
					if (class293.aString2333 == null) {
						ActionQueueEntry.addAction(false, true, 0L, -1, class293.componentIndex, "", false, class293.idDword, 9, class293.idDword | class293.componentIndex << -35313376, class293.unknown, false, TextResources.CONTINUE.getText(client.gameLanguage, (byte) 25));
					} else {
						ActionQueueEntry.addAction(false, true, 0L, -1, class293.componentIndex, "", false, class293.idDword, 9, class293.idDword | class293.componentIndex << -1733821568, class293.unknown, false, class293.aString2333);
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ai.A(" + i + ',' + (class293 != null ? "{...}" : "null") + ',' + i_1_ + ',' + i_2_ + ')');
			}
			break;
		} while (false);
	}

	public static final void method187(boolean bool) {
		do {
			try {
				AwtMouseEvent.consoleOpen = false;
				Class98_Sub43.setAllDirty(2);
				if (bool == true) {
					break;
				}
				SEND_CLAN_CHAT_MESSAGE = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ai.B(" + bool + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method188(boolean bool) {
		try {
			if (bool != false) {
				aClass148_110 = null;
			}
			if (RenderAnimDefinitionParser.anInt1948 != 0) {
				return true;
			}
			return BConfigDefinition.aClass98_Sub31_Sub2_3122.method1354(-3619);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ai.E(" + bool + ')');
		}
	}

	private Js5					aClass207_111;

	private AdvancedMemoryCache	aClass79_109	= new AdvancedMemoryCache(64);

	public InventoriesDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		aClass207_111 = class207;
		aClass207_111.getFileCount(0, 5);
	}

	public final InventoriesDefinition method185(int i, int i_0_) {
		InventoriesDefinition definition;
		synchronized (aClass79_109) {
			definition = (InventoriesDefinition) aClass79_109.get(i + -133, i_0_);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (aClass207_111) {
			is = aClass207_111.getFile(i_0_, 5, false);
		}
		definition = new InventoriesDefinition();
		if (is != null) {
			definition.decode(new RSByteBuffer(is), -125);
		}
		synchronized (aClass79_109) {
			aClass79_109.put(i_0_, definition, (byte) -80);
		}
		return definition;
	}
}

/* Class139 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.GameDefinition;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.VarPlayerDefinition;

public final class VarPlayerDefinitionParser {
	public static int			anInt1087		= 0;
	private Js5					aClass207_1088;
	private AdvancedMemoryCache	aClass79_1089	= new AdvancedMemoryCache(64);
	public int					anInt1086;

	public VarPlayerDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		aClass207_1088 = class207;
		if (aClass207_1088 == null) {
			anInt1086 = 0;
		} else {
			anInt1086 = aClass207_1088.getFileCount(0, 16);
		}
	}

	public final void method2281(int i) {
		try {
			synchronized (aClass79_1089) {
				aClass79_1089.clear(4);
				if (i > -102) {
					anInt1086 = 56;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jj.D(" + i + ')');
		}
	}

	public final VarPlayerDefinition method2282(int i, int i_0_) {
		try {
			VarPlayerDefinition class167;
			synchronized (aClass79_1089) {
				class167 = (VarPlayerDefinition) aClass79_1089.get(-125, i);
			}
			if (class167 != null) {
				return class167;
			}
			byte[] is;
			synchronized (aClass207_1088) {
				if (i_0_ != 16) {
					anInt1086 = 120;
				}
				is = aClass207_1088.getFile(i, 16, false);
			}
			class167 = new VarPlayerDefinition();
			if (is != null) {
				class167.method2527(new RSByteBuffer(is), -2);
			}
			synchronized (aClass79_1089) {
				aClass79_1089.put(i, class167, (byte) -80);
			}
			return class167;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jj.C(" + i + ',' + i_0_ + ')');
		}
	}

	public final void method2283(byte i) {
		try {
			if (i <= 9) {
				method2282(-33, -125);
			}
			synchronized (aClass79_1089) {
				aClass79_1089.freeSoftReferences((byte) 30);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jj.A(" + i + ')');
		}
	}

	public final void method2284(byte i, int i_1_) {
		try {
			synchronized (aClass79_1089) {
				aClass79_1089.makeSoftReferences((byte) 62, i_1_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jj.B(" + i + ',' + i_1_ + ')');
		}
	}
}

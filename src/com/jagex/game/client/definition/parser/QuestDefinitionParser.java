
/* Class13 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import com.Class349;
import com.Class48;
import com.OpenGlPointLight;
import com.GameDefinition;
import com.IncomingOpcode;
import com.OutgoingOpcode;
import com.RSByteBuffer;
import com.StreamHandler;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.QuestDefinition;
import com.jagex.game.client.preferences.GamePreferences;

public final class QuestDefinitionParser {
	public static OutgoingOpcode	aClass171_164	= new OutgoingOpcode(58, 8);
	public static IncomingOpcode	aClass58_161	= new IncomingOpcode(15, -2);

	public static final String generateDebugInfo(Throwable throwable, byte i) throws IOException {
		String string;
		if (!(throwable instanceof Js5Exception)) {
			string = "";
		} else {
			Js5Exception runtimeException_sub1 = (Js5Exception) throwable;
			throwable = runtimeException_sub1.cause;
			string = runtimeException_sub1.jsMessage + " | ";
		}
		StringWriter stringwriter = new StringWriter();
		PrintWriter printwriter = new PrintWriter(stringwriter);
		if (i != -24) {
			aClass58_161 = null;
		}
		throwable.printStackTrace(printwriter);
		printwriter.close();
		String string_7_ = stringwriter.toString();
		BufferedReader bufferedreader = new BufferedReader(new StringReader(string_7_));
		String string_8_ = bufferedreader.readLine();
		for (;;) {
			String string_9_ = bufferedreader.readLine();
			if (string_9_ == null) {
				break;
			}
			int i_10_ = string_9_.indexOf('(');
			int i_11_ = string_9_.indexOf(')', i_10_ - -1);
			String string_12_;
			if (i_10_ != -1) {
				string_12_ = string_9_.substring(0, i_10_);
			} else {
				string_12_ = string_9_;
			}
			string_12_ = string_12_.trim();
			string_12_ = string_12_.substring(1 + string_12_.lastIndexOf(' '));
			string_12_ = string_12_.substring(1 + string_12_.lastIndexOf('\t'));
			string += string_12_;
			if (i_10_ != -1 && i_11_ != -1) {
				int i_13_ = string_9_.indexOf(".java:", i_10_);
				if ((i_13_ ^ 0xffffffff) <= -1) {
					string += string_9_.substring(5 + i_13_, i_11_);
				}
			}
			string += ' ';
		}
		string += "| " + string_8_;
		return string;
	}

	public static final Class48 method217(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		if (i != 5) {
			aClass171_164 = null;
		}
		long l = i_1_ * 76724863L ^ 986053L * i_0_ ^ 67481L * i_4_ ^ 97549L * i_5_ ^ i_3_ * 475427L ^ i_2_ * 32147369L;
		Class48 class48 = (Class48) StreamHandler.aClass79_1010.get(-122, l);
		if (class48 != null) {
			return class48;
		}
		class48 = OpenGlPointLight.aHa4185.method1803(i_4_, i_5_, i_3_, i_0_, i_2_, i_1_);
		StreamHandler.aClass79_1010.put(l, class48, (byte) -80);
		return class48;
	}

	public static final boolean method224(int i, int i_15_, int i_16_) {
		return !(!Class349.method3842(i_16_, i_15_, -18021) && !GamePreferences.method1292(i_16_, (byte) 117, i_15_));
	}

	private Js5					questInfoJs5;

	private AdvancedMemoryCache	recentlyUsed	= new AdvancedMemoryCache(64);

	public QuestDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		do {
			questInfoJs5 = class207;
			if (questInfoJs5 == null) {
				break;
			}
			questInfoJs5.getFileCount(0, 35);
			break;
		} while (false);
	}

	public final void clear(byte i) {
		synchronized (recentlyUsed) {
			recentlyUsed.clear(93);
		}
	}

	public final void freeSoftReferences(boolean bool) {
		synchronized (recentlyUsed) {
			recentlyUsed.freeSoftReferences((byte) 96);
		}
	}

	public final QuestDefinition list(int i, int i_6_) {
		QuestDefinition definition;
		synchronized (recentlyUsed) {
			definition = (QuestDefinition) recentlyUsed.get(-127, i);
		}
		if (definition != null) {
			return definition;
		}
		byte[] buffer;
		synchronized (questInfoJs5) {
			buffer = questInfoJs5.getFile(i, 35, false);
		}
		definition = new QuestDefinition();
		if (buffer != null) {
			definition.decode((byte) -113, new RSByteBuffer(buffer));
		}
		definition.postDecode(-9639);
		synchronized (recentlyUsed) {
			recentlyUsed.put(i, definition, (byte) -80);
		}
		return definition;
	}

	public final void makeSoftReferences(int i, int i_14_) {
		synchronized (recentlyUsed) {
			recentlyUsed.makeSoftReferences((byte) 62, i_14_);
		}
	}
}

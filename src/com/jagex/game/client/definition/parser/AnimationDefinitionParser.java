/* Class183 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.AnimationSkeletonSet;
import com.ArrayUtils;
import com.Class185;
import com.Class186;
import com.Class187;
import com.Class246_Sub3_Sub4_Sub4;
import com.Class272;
import com.Class308;
import com.Class319;
import com.Class50;
import com.Class53_Sub1;
import com.Class98_Sub10_Sub32;
import com.Class98_Sub46_Sub10;
import com.Class98_Sub46_Sub17;
import com.DummyInputStream;
import com.GameDefinition;
import com.Mob;
import com.RSByteBuffer;
import com.RSToolkit;
import com.SceneGraphNode;
import com.aa_Sub2;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsItem;
import com.jagex.game.input.impl.AwtMouseListener;

public final class AnimationDefinitionParser {
	public static final void method2620(int i) {
		try {
			if (NewsItem.anInt3128 != -1 && (Class53_Sub1.anInt3636 ^ 0xffffffff) != 0) {
				int i_0_ = SceneGraphNode.anInt1872 + (NativeProgressMonitor.anInt3394 * (-SceneGraphNode.anInt1872 + Class98_Sub10_Sub32.anInt5718) >> 1233746160);
				NativeProgressMonitor.anInt3394 += i_0_;
				if ((NativeProgressMonitor.anInt3394 ^ 0xffffffff) <= -65536) {
					Class187.aBoolean1451 = !Class319.aBoolean2700;
					NativeProgressMonitor.anInt3394 = 65535;
					Class319.aBoolean2700 = true;
				} else {
					Class187.aBoolean1451 = false;
					Class319.aBoolean2700 = false;
				}
				float f = NativeProgressMonitor.anInt3394 / 65535.0F;
				float[] fs = new float[3];
				int i_1_ = 2 * Class50.anInt418;
				for (int i_2_ = 0; i_2_ < 3; i_2_++) {
					int i_3_ = 3 * DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_][i_2_];
					int i_4_ = 3 * DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_ + 1][i_2_];
					int i_5_ = 3 * (DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_ + 2][i_2_] - (-DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_ - -2][i_2_] + DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_ - -3][i_2_]));
					int i_6_ = DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_][i_2_];
					int i_7_ = i_4_ + -i_3_;
					int i_8_ = i_5_ + i_3_ - 2 * i_4_;
					int i_9_ = i_4_ + -i_6_ + DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_ + 2][i_2_] - i_5_;
					fs[i_2_] = (i_7_ + f * (i_8_ + f * i_9_)) * f + i_6_;
				}
				AdvancedMemoryCache.anInt601 = -1 * (int) fs[1];
				SpriteLoadingScreenElement.yCamPosTile = (int) fs[2] - 512 * aa_Sub2.gameSceneBaseY;
				Class98_Sub46_Sub10.xCamPosTile = -(Class272.gameSceneBaseX * 512) + (int) fs[0];
				float[] fs_10_ = new float[3];
				int i_11_ = 2 * RSToolkit.anInt943;
				for (int i_12_ = 0; i_12_ < 3; i_12_++) {
					int i_13_ = DummyInputStream.animatedBackgroundCoords[Class53_Sub1.anInt3636][i_11_][i_12_] * 3;
					int i_14_ = 3 * DummyInputStream.animatedBackgroundCoords[Class53_Sub1.anInt3636][i_11_ + 1][i_12_];
					int i_15_ = 3 * (DummyInputStream.animatedBackgroundCoords[Class53_Sub1.anInt3636][i_11_ + 2][i_12_] - (DummyInputStream.animatedBackgroundCoords[Class53_Sub1.anInt3636][i_11_ - -3][i_12_] + -DummyInputStream.animatedBackgroundCoords[Class53_Sub1.anInt3636][i_11_ + 2][i_12_]));
					int i_16_ = DummyInputStream.animatedBackgroundCoords[Class53_Sub1.anInt3636][i_11_][i_12_];
					int i_17_ = i_14_ - i_13_;
					int i_18_ = i_15_ + -(i_14_ * 2) + i_13_;
					int i_19_ = -i_15_ + i_14_ + DummyInputStream.animatedBackgroundCoords[Class53_Sub1.anInt3636][2 + i_11_][i_12_] + -i_16_;
					fs_10_[i_12_] = i_16_ + (i_17_ + (f * i_19_ + i_18_) * f) * f;
				}
				float f_20_ = fs_10_[0] - fs[i];
				float f_21_ = (-fs[1] + fs_10_[1]) * -1.0F;
				float f_22_ = fs_10_[2] - fs[2];
				double d = Math.sqrt(f_22_ * f_22_ + f_20_ * f_20_);
				Mob.anInt6357 = (int) (2607.5945876176133 * Math.atan2(f_21_, d)) & 0x3fff;
				Class186.cameraX = 0x3fff & (int) (2607.5945876176133 * -Math.atan2(f_20_, f_22_));
				Class308.anInt2584 = DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_][3] - -((-DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][i_1_][3] + DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128][2 + i_1_][3])
						* NativeProgressMonitor.anInt3394 >> -1587239088);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "me.C(" + i + ')');
		}
	}

	public static final byte[] method2622(byte[] is, byte i, int i_24_) {
		try {
			byte[] is_25_ = new byte[i_24_];
			ArrayUtils.method2894(is, 0, is_25_, 0, i_24_);
			if (i > -100) {
				method2622(null, (byte) 61, 124);
			}
			return is_25_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "me.D(" + (is != null ? "{...}" : "null") + ',' + i + ',' + i_24_ + ')');
		}
	}

	public static final void method2625(boolean bool, int i) {
		try {
			if (bool != false) {
				method2620(104);
			}
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i, -105, 4);
			class98_sub46_sub17.method1621(0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "me.A(" + bool + ',' + i + ')');
		}
	}

	private AdvancedMemoryCache	cache			= new AdvancedMemoryCache(100);

	private Js5					configJs5;

	private AdvancedMemoryCache	recentlyUsed	= new AdvancedMemoryCache(64);

	public AnimationDefinitionParser(GameDefinition game, int i, Js5 class207, Js5 class207_28_, Js5 class207_29_) {
		configJs5 = class207;
		if (configJs5 != null) {
			int i_30_ = configJs5.getGroupCount((byte) -11) - 1;
			configJs5.getFileCount(0, i_30_);
		}
		Class246_Sub3_Sub4_Sub4.method3079((byte) 33, class207_29_, 2, class207_28_);
	}

	public final void method2618(boolean bool) {
		synchronized (recentlyUsed) {
			recentlyUsed.clear(109);
		}
		synchronized (cache) {
			if (bool != true) {
				method2624(-55, 58);
			}
			cache.clear(86);
		}
	}

	public final void method2619(int i) {
		synchronized (recentlyUsed) {
			recentlyUsed.freeSoftReferences((byte) 18);
		}
		synchronized (cache) {
			cache.freeSoftReferences((byte) 109);
			if (i != -2118) {
				cache = null;
			}
		}
	}

	public final void method2621(int i, int i_23_) {
		synchronized (recentlyUsed) {
			recentlyUsed.makeSoftReferences((byte) 62, i);
		}
		if (i_23_ == 3) {
			synchronized (cache) {
				cache.makeSoftReferences((byte) 62, i);
			}
		}
	}

	public final AnimationDefinition method2623(int animation, int priority) {
		AnimationDefinition definition;
		synchronized (recentlyUsed) {
			definition = (AnimationDefinition) recentlyUsed.get(-128, animation);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (configJs5) {
			is = configJs5.getFile(Class98_Sub10_Sub32.method1096(animation, 127), AwtMouseListener.method3527(animation, priority ^ 0x2dea8938), false);
		}
		definition = new AnimationDefinition();
		if (priority != 16383) {
			return null;
		}
		definition.loader = this;
		definition.animation = animation;
		if (is != null) {
			definition.decode(new RSByteBuffer(is), -125);
		}
		definition.method938(priority + -16508);
		synchronized (recentlyUsed) {
			recentlyUsed.put(animation, definition, (byte) -80);
		}
		return definition;
	}

	public final AnimationSkeletonSet method2624(int i, int i_27_) {
		AnimationSkeletonSet class98_sub46_sub16;
		synchronized (cache) {
			class98_sub46_sub16 = (AnimationSkeletonSet) cache.get(-120, i_27_);
			if (class98_sub46_sub16 == null) {
				class98_sub46_sub16 = new AnimationSkeletonSet(i_27_);
				cache.put(i_27_, class98_sub46_sub16, (byte) -80);
			}
			if (!class98_sub46_sub16.method1614((byte) 66)) {
				return null;
			}
		}
		return class98_sub46_sub16;
	}
}


/* Class264 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import java.io.FileOutputStream;

import com.AsyncCache;
import com.Class42_Sub1;
import com.Class98_Sub10_Sub12;
import com.Class98_Sub46_Sub10;
import com.Class98_Sub46_Sub20;
import com.GameDefinition;
import com.RSByteBuffer;
import com.TextResources;
import com.client;
import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.StructsDefinition;

public final class StructsDefinitionParser {
	public static FileOutputStream	aFileOutputStream1969;
	public static int				anInt1971;
	public static int				otherlevelId;

	public static final void method3222(byte i) {
		try {
			AsyncCache.anInt1934 = Class98_Sub46_Sub10.p12FullMetrics.anInt1517 - (-Class98_Sub46_Sub10.p12FullMetrics.anInt1514 - 2);
			Class98_Sub46_Sub20.aStringArray6073 = new String[500];
			Class98_Sub10_Sub12.anInt5598 = 2 + Class42_Sub1.p13FullMetrics.anInt1517 + Class42_Sub1.p13FullMetrics.anInt1514;
			int i_0_ = 0;
			if (i != -43) {
				anInt1971 = 15;
			}
			for (/**/; (Class98_Sub46_Sub20.aStringArray6073.length ^ 0xffffffff) < (i_0_ ^ 0xffffffff); i_0_++) {
				Class98_Sub46_Sub20.aStringArray6073[i_0_] = "";
			}
			Cacheable.write(TextResources.THIS_IS_THE_DEVELOPER_CONSOLE.getText(client.gameLanguage, (byte) 25), i ^ ~0x7b);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qi.C(" + i + ')');
		}
	}

	public static void method3225(boolean bool) {
		do {
			try {
				aFileOutputStream1969 = null;
				if (bool == true) {
					break;
				}
				anInt1971 = 51;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qi.B(" + bool + ')');
			}
			break;
		} while (false);
	}

	private Js5					aClass207_1970;

	private AdvancedMemoryCache	aClass79_1968	= new AdvancedMemoryCache(256);

	public StructsDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		try {
			aClass207_1970 = class207;
			aClass207_1970.getFileCount(0, 26);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qi.<init>(" + (class279 != null ? "{...}" : "null") + ',' + i + ',' + (class207 != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3223(byte i) {
		try {
			if (i != 17) {
				method3223((byte) 86);
			}
			synchronized (aClass79_1968) {
				aClass79_1968.clear(i + 58);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qi.D(" + i + ')');
		}
	}

	public final StructsDefinition method3224(int i, int i_1_) {
		try {
			StructsDefinition class98_sub46_sub12;
			synchronized (aClass79_1968) {
				class98_sub46_sub12 = (StructsDefinition) aClass79_1968.get(-127, i_1_);
			}
			if (class98_sub46_sub12 != null) {
				return class98_sub46_sub12;
			}
			byte[] is;
			synchronized (aClass207_1970) {
				is = aClass207_1970.getFile(i_1_, i, false);
			}
			class98_sub46_sub12 = new StructsDefinition();
			if (is != null) {
				class98_sub46_sub12.method1588(0, new RSByteBuffer(is));
			}
			synchronized (aClass79_1968) {
				aClass79_1968.put(i_1_, class98_sub46_sub12, (byte) -80);
			}
			return class98_sub46_sub12;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qi.E(" + i + ',' + i_1_ + ')');
		}
	}

	public final void method3226(int i) {
		try {
			synchronized (aClass79_1968) {
				aClass79_1968.freeSoftReferences((byte) -115);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qi.A(" + i + ')');
		}
	}

	public final void method3227(int i, int i_3_) {
		do {
			try {
				synchronized (aClass79_1968) {
					aClass79_1968.makeSoftReferences((byte) 62, i);
				}
				if (i_3_ <= -28) {
					break;
				}
				aClass207_1970 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qi.F(" + i + ',' + i_3_ + ')');
			}
			break;
		} while (false);
	}
}


/* Class269 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import java.lang.reflect.Field;

import com.Class116;
import com.Class151_Sub7;
import com.Class162;
import com.Class165;
import com.Class191;
import com.Class21_Sub2;
import com.Class360;
import com.WhirlpoolGenerator;
import com.Class40;
import com.Class69_Sub2;
import com.Class76_Sub11;
import com.Class76_Sub9;
import com.Class87;
import com.Class98_Sub10_Sub21;
import com.Class98_Sub10_Sub7;
import com.Class98_Sub33;
import com.Class98_Sub43;
import com.Class98_Sub46_Sub10;
import com.Class98_Sub46_Sub20_Sub2;
import com.ClientScript2Runtime;
import com.GameDefinition;
import com.Mob;
import com.OutgoingPacket;
import com.OutputStream_Sub2;
import com.Player;
import com.PointLight;
import com.RSByteBuffer;
import com.RtInterfaceAttachment;
import com.aa_Sub3;
import com.client;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.LightIntensityDefinition;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.toolkit.font.Font;

public final class LightIntensityDefinitionParser {
	/* synthetic */ static Class	aClass2029;
	public static Js5				aClass207_2025;
	public static float[]			aFloatArray2023	= { 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F };
	public static float[]			aFloatArray2027	= new float[4];
	public static int				anInt2024		= -1;
	/* synthetic */ static Class	component;
	public static String[]			playerOptions	= new String[8];

	/* synthetic */
	public static Class getClassByName(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public static final void method3266(int i, byte i_0_) {
		try {
			Class69_Sub2.aClass79_5334.makeSoftReferences((byte) 62, i);
			Class64_Sub5.aClass79_3650.makeSoftReferences((byte) 62, i);
			Class76_Sub11.aClass79_3797.makeSoftReferences((byte) 62, i);
			Class151_Sub7.aClass79_5004.makeSoftReferences((byte) 62, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qq.H(" + i + ',' + i_0_ + ')');
		}
	}

	public static void method3267(byte i) {
		do {
			try {
				playerOptions = null;
				aClass207_2025 = null;
				aFloatArray2027 = null;
				aFloatArray2023 = null;
				if (i > 36) {
					break;
				}
				method3266(-41, (byte) 0);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qq.E(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method3269(boolean bool, int i) {
		System.out.println("Hellohellohello" + bool);
		if (bool) {
			if (client.topLevelInterfaceId != -1) {
				RtInterfaceAttachment.uncacheInterface(false, client.topLevelInterfaceId);
			}
			for (RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(94); class98_sub18 != null; class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.iterateNext(-1)) {
				if (!class98_sub18.isLinked((byte) 78)) {
					class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(121);
					if (class98_sub18 == null) {
						break;
					}
				}
				RtInterfaceAttachment.detachInterface(16398, false, class98_sub18, true);
			}
			client.topLevelInterfaceId = -1;
			Class116.attachmentMap = new HashTable(8);
			Class76_Sub9.method768(118);
			client.topLevelInterfaceId = ItemDefinition.loginInterfaceId;
			Class40.calculateLayout(i + 83, false);
			Class98_Sub43.setAllDirty(2);
			ClientScript2Runtime.sendWindowPane(client.topLevelInterfaceId);
		}
		PointLight.username = Class360.password = "";
		System.out.println("Username" + PointLight.username);
		aa_Sub3.aBoolean3569 = false;
		Class162.method2516(-96);
		Class21_Sub2.cursorId = -1;
		Font.method401(OutputStream_Sub2.anInt39, true);
		Class87.localPlayer = new Player();
		Class87.localPlayer.boundExtentsX = Class165.mapWidth * 512 / 2;
		((Mob) Class87.localPlayer).pathX[i] = Class165.mapWidth / 2;
		Class87.localPlayer.boundExtentsZ = Class98_Sub10_Sub7.mapLength * 512 / 2;
		((Mob) Class87.localPlayer).pathZ[0] = Class98_Sub10_Sub7.mapLength / 2;
		Class98_Sub46_Sub10.xCamPosTile = SpriteLoadingScreenElement.yCamPosTile = 0;
		if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) != -3) {
			AnimationDefinitionParser.method2620(0);
		} else {
			SpriteLoadingScreenElement.yCamPosTile = Class98_Sub10_Sub21.anInt5643 << 1006717769;
			Class98_Sub46_Sub10.xCamPosTile = NewsFetcher.anInt3098 << 1638996777;
		}
		WhirlpoolGenerator.method3980((byte) 121);
	}

	public static final int method3270(int i) {
		try {
			int i_3_ = 0;
			Field[] fields = (aClass2029 != null ? aClass2029 : (aClass2029 = getClassByName("com.jagex.game.client.preferences.GamePreferences"))).getDeclaredFields();
			Field[] fields_4_ = fields;
			for (int i_5_ = 0; (fields_4_.length ^ 0xffffffff) < (i_5_ ^ 0xffffffff); i_5_++) {
				Field field = fields_4_[i_5_];
				if ((component != null ? component : (component = getClassByName("com.jagex.game.client.preferences.IntegerPreferenceField"))).isAssignableFrom(field.getType())) {
					i_3_++;
				}
			}
			if (i <= 85) {
				method3273(true);
			}
			return 1 + i_3_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qq.F(" + i + ')');
		}
	}

	public static final void method3273(boolean bool) {
		try {
			if (bool == true) {
				for (Class98_Sub33 class98_sub33 = (Class98_Sub33) Class191.aClass148_1478.getFirst(32); class98_sub33 != null; class98_sub33 = (Class98_Sub33) Class191.aClass148_1478.getNext(109)) {
					Class98_Sub33.method2428(class98_sub33, false, 15697);
				}
				for (Class98_Sub33 class98_sub33 = (Class98_Sub33) OutgoingPacket.aClass148_3866.getFirst(32); class98_sub33 != null; class98_sub33 = (Class98_Sub33) OutgoingPacket.aClass148_3866.getNext(93)) {
					Class98_Sub33.method2428(class98_sub33, true, 15697);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qq.D(" + bool + ')');
		}
	}

	private Js5					aClass207_2022;

	private AdvancedMemoryCache	aClass79_2028	= new AdvancedMemoryCache(64);

	public LightIntensityDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		aClass207_2022 = class207;
		aClass207_2022.getFileCount(0, 31);
	}

	public final void method3265(int i) {
		synchronized (aClass79_2028) {
			aClass79_2028.clear(85);
		}
	}

	public final LightIntensityDefinition method3268(int i, int i_2_) {
		LightIntensityDefinition definition;
		synchronized (aClass79_2028) {
			definition = (LightIntensityDefinition) aClass79_2028.get(-126, i_2_);
		}
		if (i != -37) {
			method3269(false, 104);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (aClass207_2022) {
			is = aClass207_2022.getFile(i_2_, 31, false);
		}
		definition = new LightIntensityDefinition();
		if (is != null) {
			definition.decode((byte) 126, new RSByteBuffer(is));
		}
		synchronized (aClass79_2028) {
			aClass79_2028.put(i_2_, definition, (byte) -80);
		}
		return definition;
	}

	public final void method3271(boolean bool) {
		synchronized (aClass79_2028) {
			aClass79_2028.freeSoftReferences((byte) 63);
		}
	}

	public final void method3272(int i, int i_6_) {
		synchronized (aClass79_2028) {
			aClass79_2028.makeSoftReferences((byte) 62, i_6_);
		}
	}
}

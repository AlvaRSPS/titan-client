
/* Class153 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import java.awt.Component;

import com.Char;
import com.Class103;
import com.Class119_Sub4;
import com.Class151_Sub7;
import com.Billboard;
import com.Class224_Sub3_Sub1;
import com.Class268;
import com.Class268_Sub1;
import com.Class42_Sub2;
import com.Class87;
import com.GameDefinition;
import com.GameShell;
import com.MapRegion;
import com.Minimap;
import com.PlayerUpdateMask;
import com.RSByteBuffer;
import com.RSToolkit;
import com.RsFloatBuffer;
import com.SignLink;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.anticheat.ReflectionAntiCheat;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.FloorUnderlayDefinition;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.input.RtMouseEvent;

public final class FloorUnderlayDefinitionParser {
	public static RSToolkit	aHa1225;
	public static String	clanChatName	= null;
	public static boolean	clipComponents	= false;

	public static final Class268 method2484(int i, int i_1_, byte i_2_, SignLink class88, Component component) {
		if (RemoveRoofsPreferenceField.anInt3678 == 0) {
			throw new IllegalStateException();
		}
		if ((i_1_ ^ 0xffffffff) > -1 || i_1_ >= 2) {
			throw new IllegalArgumentException();
		}
		if (i < 256) {
			i = 256;
		}
		try {
			Class268 class268 = (Class268) Class.forName("com.Class268_Sub2").newInstance();
			class268.anInt2016 = i;
			class268.anIntArray2005 = new int[(!Class151_Sub7.aBoolean5007 ? 1 : 2) * 256];
			class268.method3253(component);
			class268.anInt2010 = 1024 + (i & ~0x3ff);
			if (class268.anInt2010 > 16384) {
				class268.anInt2010 = 16384;
			}
			class268.method3250(class268.anInt2010);
			if ((PlayerUpdateMask.anInt529 ^ 0xffffffff) < -1 && Billboard.aClass103_1375 == null) {
				Billboard.aClass103_1375 = new Class103();
				Billboard.aClass103_1375.aClass88_891 = class88;
				class88.startThread(PlayerUpdateMask.anInt529, Billboard.aClass103_1375, 1);
			}
			if (Billboard.aClass103_1375 != null) {
				if (Billboard.aClass103_1375.aClass268Array894[i_1_] != null) {
					throw new IllegalArgumentException();
				}
				Billboard.aClass103_1375.aClass268Array894[i_1_] = class268;
			}
			return class268;
		} catch (Throwable throwable) {
			try {
				Class268_Sub1 class268_sub1 = new Class268_Sub1(class88, i_1_);
				class268_sub1.anInt2016 = i;
				class268_sub1.anIntArray2005 = new int[256 * (Class151_Sub7.aBoolean5007 ? 2 : 1)];
				class268_sub1.method3253(component);
				class268_sub1.anInt2010 = 16384;
				if (i_2_ != -126) {
					GameShell.focusIn = true;
				}
				class268_sub1.method3250(class268_sub1.anInt2010);
				if (PlayerUpdateMask.anInt529 > 0 && Billboard.aClass103_1375 == null) {
					Billboard.aClass103_1375 = new Class103();
					Billboard.aClass103_1375.aClass88_891 = class88;
					class88.startThread(PlayerUpdateMask.anInt529, Billboard.aClass103_1375, 1);
				}
				if (Billboard.aClass103_1375 != null) {
					if (Billboard.aClass103_1375.aClass268Array894[i_1_] != null) {
						throw new IllegalArgumentException();
					}
					Billboard.aClass103_1375.aClass268Array894[i_1_] = class268_sub1;
				}
				return class268_sub1;
			} catch (Throwable throwable_3_) {
				return new Class268();
			}
		}
	}

	public static final void method2487(int i) {
		ReflectionAntiCheat.requestQueue = new LinkedList();
	}

	public static final void method2489(long l, byte i) {
		int xPos = GraphicsDefinitionParser.anInt2533 + Class87.localPlayer.boundExtentsX;
		int yPos = Class87.localPlayer.boundExtentsZ - -RtMouseEvent.anInt3943;
		if ((-xPos + Minimap.cameraXPosition ^ 0xffffffff) > 1999 || (Minimap.cameraXPosition + -xPos ^ 0xffffffff) < -2001 || Class224_Sub3_Sub1.cameraYPosition + -yPos < -2000 || (-yPos + Class224_Sub3_Sub1.cameraYPosition ^ 0xffffffff) < -2001) {
			Minimap.cameraXPosition = xPos;
			Class224_Sub3_Sub1.cameraYPosition = yPos;
		}
		if (Minimap.cameraXPosition != xPos) {
			int difference = xPos + -Minimap.cameraXPosition;
			int i_9_ = (int) (difference * l / 320L);
			if ((difference ^ 0xffffffff) >= -1) {
				if (i_9_ == 0) {
					i_9_ = -1;
				} else if ((i_9_ ^ 0xffffffff) > (difference ^ 0xffffffff)) {
					i_9_ = difference;
				}
			} else if (i_9_ != 0) {
				if ((i_9_ ^ 0xffffffff) < (difference ^ 0xffffffff)) {
					i_9_ = difference;
				}
			} else {
				i_9_ = 1;
			}
			Minimap.cameraXPosition += i_9_;
		}
		if (yPos != Class224_Sub3_Sub1.cameraYPosition) {
			int difference = -Class224_Sub3_Sub1.cameraYPosition + yPos;
			int i_11_ = (int) (l * difference / 320L);
			if ((difference ^ 0xffffffff) >= -1) {
				if ((i_11_ ^ 0xffffffff) != -1) {
					if ((difference ^ 0xffffffff) < (i_11_ ^ 0xffffffff)) {
						i_11_ = difference;
					}
				} else {
					i_11_ = -1;
				}
			} else if (i_11_ == 0) {
				i_11_ = 1;
			} else if (difference < i_11_) {
				i_11_ = difference;
			}
			Class224_Sub3_Sub1.cameraYPosition += i_11_;
		}
		Class119_Sub4.aFloat4740 += l * MapRegion.aFloat2545 / 6.0F;
		RsFloatBuffer.aFloat5794 += l * FileProgressMonitor.aFloat3405 / 6.0F;
		Class42_Sub2.method388(true);
	}

	public static final int method2490(int i, boolean bool) {
		return i >>> -1052998200;
	}

	private Js5					floorUnderlayJs5;

	private AdvancedMemoryCache	recentlyUsed	= new AdvancedMemoryCache(128);

	public FloorUnderlayDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		floorUnderlayJs5 = class207;
		floorUnderlayJs5.getFileCount(0, 1);
	}

	public final void clear(byte i) {
		synchronized (recentlyUsed) {
			recentlyUsed.clear(97);
		}
	}

	public final void freeSoftReferences(byte i) {
		synchronized (recentlyUsed) {
			recentlyUsed.freeSoftReferences((byte) 61);
		}
	}

	public final FloorUnderlayDefinition get(int i, int i_0_) {
		FloorUnderlayDefinition definition;
		synchronized (recentlyUsed) {
			definition = (FloorUnderlayDefinition) recentlyUsed.get(-128, i);
		}
		if (definition != null) {
			return definition;
		}
		byte[] data;
		synchronized (floorUnderlayJs5) {
			data = floorUnderlayJs5.getFile(i, 1, false);
		}
		definition = new FloorUnderlayDefinition();
		if (data != null) {
			definition.decode(new RSByteBuffer(data), -52);
		}
		synchronized (recentlyUsed) {
			recentlyUsed.put(i, definition, (byte) -80);
		}
		return definition;
	}

	public final void makeSoftReferences(int i, int i_4_) {
		synchronized (recentlyUsed) {
			recentlyUsed.makeSoftReferences((byte) 62, i);
		}
	}
}

/* Class59 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.ArrayUtils;
import com.Class175;
import com.Class228;
import com.Class346;
import com.Class360;
import com.GameDefinition;
import com.IncomingOpcode;
import com.OutgoingOpcode;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.SkyboxDefinition;
import com.jagex.game.client.definition.SunDefinition;

public final class SkyboxDefinitionParser {
	public static OutgoingOpcode	aClass171_468			= new OutgoingOpcode(36, -1);
	public static IncomingOpcode	aClass58_469			= new IncomingOpcode(113, -2);
	public static int				activeOccludersCount	= 0;
	public static int				anInt466;

	public static final boolean method524(int i, int i_0_, int i_1_) {
		try {
			if (i_1_ < 34) {
				method526((byte) 105, 80, null);
			}
			return !(!((i & 0x800) != 0 | Class360.method3905(-12, i, i_0_)) && !Class228.method2864(55, i, i_0_));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "eaa.A(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static final float[] method526(byte i, int i_3_, float[] fs) {
		try {
			if (i != -64) {
				anInt466 = -4;
			}
			float[] fs_4_ = new float[i_3_];
			ArrayUtils.method2897(fs, 0, fs_4_, 0, i_3_);
			return fs_4_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "eaa.C(" + i + ',' + i_3_ + ',' + (fs != null ? "{...}" : "null") + ')');
		}
	}

	public static void method531(byte i) {
		do {
			try {
				aClass171_468 = null;
				aClass58_469 = null;
				if (i < -114) {
					break;
				}
				method526((byte) -56, -98, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "eaa.F(" + i + ')');
			}
			break;
		} while (false);
	}

	private Js5					aClass207_465;

	private AdvancedMemoryCache	aClass79_467	= new AdvancedMemoryCache(16);

	public SkyboxDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		aClass207_465 = class207;
		aClass207_465.getFileCount(0, 29);
	}

	private final SkyboxDefinition list(int i, byte i_5_) {
		SkyboxDefinition definition;
		synchronized (aClass79_467) {
			definition = (SkyboxDefinition) aClass79_467.get(-125, i);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (aClass207_465) {
			is = aClass207_465.getFile(i, 29, false);
		}
		definition = new SkyboxDefinition();
		if (is != null) {
			definition.decode(new RSByteBuffer(is), 0);
		}
		synchronized (aClass79_467) {
			aClass79_467.put(i, definition, (byte) -80);
		}
		System.out.println("i: " + definition.anInt470 + " i:" + definition.anInt472);
		return definition;
	}

	public final void method525(int i) {
		synchronized (aClass79_467) {
			aClass79_467.freeSoftReferences((byte) 117);
		}
	}

	public final Class346 method528(int i, int i_6_, int i_7_, int i_8_, SunDefinitionParser class115, int i_9_) {
		Class175[] class175s = null;
		SkyboxDefinition skyDefinition = list(i_6_, (byte) -120);
		if (skyDefinition.suns != null) {
			class175s = new Class175[skyDefinition.suns.length];
			for (int i_10_ = 0; i_10_ < class175s.length; i_10_++) {
				SunDefinition class266 = class115.method2157(skyDefinition.suns[i_10_], (byte) -87);
				class175s[i_10_] = new Class175(class266.anInt1993, class266.anInt1995, class266.anInt1990, class266.anInt1989, class266.anInt1987, class266.anInt1984, class266.anInt1991, class266.aBoolean1985);
			}
		}
		return new Class346(skyDefinition.anInt470, class175s, skyDefinition.anInt472, i_8_, i_7_, i_9_);
	}

	public final void method529(byte i) {
		synchronized (aClass79_467) {
			aClass79_467.clear(37);
		}
	}

	public final void method530(byte i, int i_12_) {
		synchronized (aClass79_467) {
			aClass79_467.makeSoftReferences((byte) 62, i_12_);
		}
	}
}

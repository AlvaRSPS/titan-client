/* Class335 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Class272;
import com.GameDefinition;
import com.IncomingOpcode;
import com.OutgoingOpcode;
import com.RSByteBuffer;
import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.MapScenesDefinition;

public final class MapScenesDefinitionParser {
	public static boolean			aBoolean2817		= false;
	public static OutgoingOpcode	aClass171_2812		= new OutgoingOpcode(56, 4);
	public static int				chunkY;
	public static IncomingOpcode	NPC_ON_INTERFACE	= new IncomingOpcode(71, 6);

	public static final int method3765(boolean bool) {
		if (bool != false) {
			return -46;
		}
		if (Cacheable.anInt4261 == 1) {
			return Class272.anInt2037;
		}
		return 0;
	}

	public Js5					aClass207_2814;

	private Js5					aClass207_2815;

	private AdvancedMemoryCache	aClass79_2813	= new AdvancedMemoryCache(64);

	public AdvancedMemoryCache	aClass79_2818	= new AdvancedMemoryCache(64);

	public MapScenesDefinitionParser(GameDefinition class279, int i, Js5 class207, Js5 class207_5_) {
		aClass207_2815 = class207;
		aClass207_2814 = class207_5_;
		aClass207_2815.getFileCount(0, 34);
	}

	public final MapScenesDefinition method3766(int i, byte i_0_) {
		MapScenesDefinition definition;
		synchronized (aClass79_2813) {
			definition = (MapScenesDefinition) aClass79_2813.get(-121, i);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (aClass207_2815) {
			is = aClass207_2815.getFile(i, 34, false);
		}
		definition = new MapScenesDefinition();
		definition.aClass335_117 = this;
		if (is != null) {
			definition.method192(new RSByteBuffer(is), false);
		}
		synchronized (aClass79_2813) {
			aClass79_2813.put(i, definition, (byte) -80);
		}
		return definition;
	}

	public final void method3767(int i, int i_2_) {
		try {
			synchronized (aClass79_2813) {
				aClass79_2813.makeSoftReferences((byte) 62, i_2_);
				if (i != 56) {
					method3768(-12);
				}
			}
			synchronized (aClass79_2818) {
				aClass79_2818.makeSoftReferences((byte) 62, i_2_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ui.F(" + i + ',' + i_2_ + ')');
		}
	}

	public final void method3768(int i) {
		try {
			synchronized (aClass79_2813) {
				aClass79_2813.freeSoftReferences((byte) 36);
			}
			synchronized (aClass79_2818) {
				if (i != 10673) {
					method3771(65, -107, -65);
				}
				aClass79_2818.freeSoftReferences((byte) -117);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ui.D(" + i + ')');
		}
	}

	public final void method3770(int i) {
		try {
			synchronized (aClass79_2813) {
				aClass79_2813.clear(21);
			}
			synchronized (aClass79_2818) {
				aClass79_2818.clear(108);
			}
			if (i != 34) {
				aClass79_2818 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ui.A(" + i + ')');
		}
	}

	public final void method3771(int i, int i_3_, int i_4_) {
		try {
			aClass79_2813 = new AdvancedMemoryCache(i_3_);
			if (i < 107) {
				method3767(37, 14);
			}
			aClass79_2818 = new AdvancedMemoryCache(i_4_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ui.E(" + i + ',' + i_3_ + ',' + i_4_ + ')');
		}
	}
}

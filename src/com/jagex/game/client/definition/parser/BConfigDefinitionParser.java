/* Class198 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Class234;
import com.GameDefinition;
import com.OutgoingOpcode;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.BConfigDefinition;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public final class BConfigDefinitionParser {
	public static OutgoingOpcode outgoingOpcode = new OutgoingOpcode(27, 3);

	public static long				scriptResumeTimeStamp;
	public static int				anInt1524		= 0;

	public static final int method2678(byte i, int i_0_, int i_1_) {
		int i_2_ = -1 + i_1_ & i_0_ >> -1893021153;
		return ((i_0_ >>> 1656906079) + i_0_) % i_1_ + i_2_;
	}

	private Js5 js5;
	private AdvancedMemoryCache memoryCache = new AdvancedMemoryCache(64);
	private int					count;

	public BConfigDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		do {
			js5 = class207;
			if (js5 == null) {
				break;
			}
			int groupCount = js5.getGroupCount((byte) -11) + -1;
			count = js5.getFileCount(0, groupCount);
			dump();
			break;
		} while (false);
	}

	public void dump() {
		try {
			PrintStream ps = new PrintStream("BConfigDefinitions.txt");
			for (int id = 0; id < count; id++) {
				ps.println("[BConfigId] = " + list(id, (byte) 0).bConfigId + " Start: " + list(id, (byte) 0).start + " End: " + list(id, (byte) 0).end);
			}
			ps.flush();
			ps.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public final BConfigDefinition list(int i, byte i_4_) {
		BConfigDefinition definition;
		synchronized (memoryCache) {
			definition = (BConfigDefinition) memoryCache.get(-125, i);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (js5) {
			is = js5.getFile(FloorOverlayDefinitionParser.method318(i, (byte) -79), Class234.method2886(i, -123), false);
		}
		definition = new BConfigDefinition();
		if (is != null) {
			definition.decode(new RSByteBuffer(is), -6364);
		}
		synchronized (memoryCache) {
			memoryCache.put(i, definition, (byte) -80);
		}
		return definition;
	}

	public final void method2679(int i, byte i_3_) {
		synchronized (memoryCache) {
			memoryCache.clear(92);
			memoryCache = new AdvancedMemoryCache(i);
		}
	}

	public final void method2681(byte i, int i_5_) {
		if (i >= 125) {
			synchronized (memoryCache) {
				memoryCache.makeSoftReferences((byte) 62, i_5_);
			}
		}
	}

	public final void method2683(int i) {
		synchronized (memoryCache) {
			memoryCache.freeSoftReferences((byte) -127);
			if (i != 0) {
				outgoingOpcode = null;
			}
		}
	}

	public final void method2684(int i) {
		synchronized (memoryCache) {
			if (i != -4742) {
				/* empty */
			} else {
				memoryCache.clear(64);
			}
		}
	}
}

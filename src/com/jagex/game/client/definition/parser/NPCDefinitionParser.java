
/* Class301 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.CacheFileRequest;
import com.NativeOpenGlElementArrayBuffer;
import com.Class191;
import com.Class224_Sub1;
import com.Class245;
import com.Class314;
import com.WhirlpoolGenerator;
import com.Class49;
import com.Class98_Sub10_Sub21;
import com.Class98_Sub10_Sub32;
import com.Class98_Sub10_Sub6;
import com.Class98_Sub28;
import com.Class98_Sub30;
import com.Class98_Sub36;
import com.GameDefinition;
import com.IncomingOpcode;
import com.NPC;
import com.OpenGLTexture2DSource;
import com.OutgoingPacket;
import com.PacketParser;
import com.ProceduralTextureSource;
import com.RSByteBuffer;
import com.Sound;
import com.client;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;

public final class NPCDefinitionParser {
	public static IncomingOpcode aClass58_2507 = new IncomingOpcode(100, 6);

	public static final void method3537(int speed, byte i_0_, int soundId, int timesPlayed, int delay, int volume) {
		if (client.preferences.soundEffectsVolume.getValue((byte) 127) != 0 && timesPlayed != 0 && (EnumDefinition.anInt2566 ^ 0xffffffff) > -51 && soundId != -1) {
			Class245.aClass338Array1865[EnumDefinition.anInt2566++] = new Sound((byte) 1, soundId, timesPlayed, delay, volume, 0, speed, null);
		}
	}

	public static final void method3540(int i) {
		if (LoadingScreenElementType.aClass143_953 != null) {
			if ((LoadingScreenElementType.aClass143_953.status ^ 0xffffffff) == -2) {
				LoadingScreenElementType.aClass143_953 = null;
			} else if (LoadingScreenElementType.aClass143_953.status == 2) {
				Class98_Sub10_Sub32.method1097(-18871, NPC.aString6507, OpenGLTexture2DSource.aClass88_3104, 2);
				LoadingScreenElementType.aClass143_953 = null;
			}
		}
	}

	public static final void method3542(int i) {
		for (Class98_Sub36 class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.startIteration(97); class98_sub36 != null; class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.iterateNext(-1)) {
			OutgoingPacket.method1127((byte) 67, class98_sub36.anInt4160);
		}
	}

	public static final void method3543(int i) {
		client.activeClient.loadJagMisc(0);
		Class49.method477(-5788);
		Class98_Sub10_Sub6.anInt5569 = 0;
		PacketParser.buffer.position = 0;
		Class224_Sub1.anInt5031 = 0;
		Class98_Sub10_Sub21.aClass58_5641 = null;
		Class98_Sub30.aClass58_4094 = null;
		ProceduralTextureSource.aClass58_3262 = null;
		Class191.method2650((byte) -126);
		Class314.friendListSize = 0;
		FloorUnderlayDefinitionParser.clanChatName = null;
		WhirlpoolGenerator.clanChat = null;
		Class98_Sub28.totalBoxVisibility = 0;
		FloorOverlayDefinitionParser.clanChatSize = 0;
	}

	public boolean				aBoolean2503;
	public Js5					aClass207_2504;
	public Js5					aClass207_2506;

	public GameDefinition		aClass279_2502;

	private AdvancedMemoryCache	aClass79_2505	= new AdvancedMemoryCache(64);

	public AdvancedMemoryCache	aClass79_2509	= new AdvancedMemoryCache(50);

	public AdvancedMemoryCache	aClass79_2510	= new AdvancedMemoryCache(5);

	public int					anInt2511;

	public NPCDefinitionParser(GameDefinition class279, int i, boolean bool, Js5 class207, Js5 class207_8_) {
		do {
			try {
				aClass207_2504 = class207;
				aClass207_2506 = class207_8_;
				aBoolean2503 = bool;
				aClass279_2502 = class279;
				if (aClass207_2504 == null) {
					break;
				}
				int i_9_ = aClass207_2504.getGroupCount((byte) -11) - 1;
				aClass207_2504.getFileCount(0, i_9_);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sha.<init>(" + (class279 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + (class207 != null ? "{...}" : "null") + ',' + (class207_8_ != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final NPCDefinition get(int i, int i_5_) {
		try {
			NPCDefinition class141;
			synchronized (aClass79_2505) {
				class141 = (NPCDefinition) aClass79_2505.get(-124, i_5_);
			}
			if (class141 != null) {
				return class141;
			}
			byte[] is;
			synchronized (aClass207_2504) {
				is = aClass207_2504.getFile(BackgroundColourLSEConfig.method2520(i_5_, (byte) 123), CacheFileRequest.method1598(i_5_, i + -22650), false);
			}
			class141 = new NPCDefinition();

			class141.npcId = i_5_;
			class141.aClass301_1133 = this;
			if (is != null) {
				class141.method2297(new RSByteBuffer(is), true);
			}
			// DumpUtils.reflectionDump(NPCDefinition.class, class141);

			class141.method2295((byte) 70);
			synchronized (aClass79_2505) {
				aClass79_2505.put(i_5_, class141, (byte) -80);
			}
			return class141;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sha.D(" + i + ',' + i_5_ + ')');
		}
	}

	public final void method3534(int i) {
		try {
			synchronized (aClass79_2509) {
				aClass79_2509.clear(91);
			}
			if (i <= -22) {
				synchronized (aClass79_2510) {
					aClass79_2510.clear(43);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sha.A(" + i + ')');
		}
	}

	public final void method3535(byte i) {
		try {
			if (i < -5) {
				synchronized (aClass79_2505) {
					aClass79_2505.freeSoftReferences((byte) 113);
				}
				synchronized (aClass79_2509) {
					aClass79_2509.freeSoftReferences((byte) -103);
				}
				synchronized (aClass79_2510) {
					aClass79_2510.freeSoftReferences((byte) -104);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sha.E(" + i + ')');
		}
	}

	public final void method3539(int i, byte i_6_) {
		try {
			synchronized (aClass79_2505) {
				aClass79_2505.makeSoftReferences((byte) 62, i);
			}
			synchronized (aClass79_2509) {
				aClass79_2509.makeSoftReferences((byte) 62, i);
			}
			synchronized (aClass79_2510) {
				aClass79_2510.makeSoftReferences((byte) 62, i);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sha.H(" + i + ',' + i_6_ + ')');
		}
	}

	public final void method3541(boolean bool, int i) {
		try {
			anInt2511 = i;
			synchronized (aClass79_2509) {
				if (bool != true) {
					method3539(127, (byte) -119);
				}
				aClass79_2509.clear(24);
			}
			synchronized (aClass79_2510) {
				aClass79_2510.clear(85);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sha.J(" + bool + ',' + i + ')');
		}
	}

	public final void method3544(byte i) {
		try {
			if (i == 1) {
				synchronized (aClass79_2505) {
					aClass79_2505.clear(67);
				}
				synchronized (aClass79_2509) {
					aClass79_2509.clear(i + 119);
				}
				synchronized (aClass79_2510) {
					aClass79_2510.clear(i + 59);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sha.G(" + i + ')');
		}
	}

	public final void setAllowMembers(int i, boolean bool) {
		try {
			if (bool != aBoolean2503) {
				aBoolean2503 = bool;
				method3544((byte) 1);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sha.K(" + i + ',' + bool + ')');
		}
	}
}

/* Class115 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Class98_Sub10_Sub27;
import com.GameDefinition;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.SunDefinition;

public final class SunDefinitionParser {
	public static float[]	aFloatArray961	= new float[4];
	public static int		anInt960		= -1;
	public static int		anInt963		= 0;

	public static final void method2155() {
		Class98_Sub10_Sub27.aClass84_5692 = Class98_Sub10_Sub27.aClass84_5693;
	}

	private Js5					aClass207_962;

	private AdvancedMemoryCache	aClass79_959	= new AdvancedMemoryCache(16);

	public SunDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		aClass207_962 = class207;
		aClass207_962.getFileCount(0, 30);
	}

	public final void method2152(int i) {
		synchronized (aClass79_959) {
			aClass79_959.clear(97);
		}
	}

	public final void method2153(int i, byte i_0_) {
		synchronized (aClass79_959) {
			aClass79_959.makeSoftReferences((byte) 62, i);
		}
	}

	public final SunDefinition method2157(int i, byte i_1_) {
		SunDefinition class266;
		synchronized (aClass79_959) {
			class266 = (SunDefinition) aClass79_959.get(-124, i);
		}
		if (class266 != null) {
			return class266;
		}
		if (i_1_ != -87) {
			aClass207_962 = null;
		}
		byte[] is;
		synchronized (aClass207_962) {
			is = aClass207_962.getFile(i, 30, false);
		}
		class266 = new SunDefinition();

		if (is != null) {
			class266.method3236(new RSByteBuffer(is), (byte) -16);
		}
		synchronized (aClass79_959) {
			aClass79_959.put(i, class266, (byte) -80);
		}
		return class266;
	}

	public final void method2158(boolean bool) {
		if (bool == true) {
			synchronized (aClass79_959) {
				aClass79_959.freeSoftReferences((byte) -104);
			}
		}
	}
}

/* Class83 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.ActionGroup;
import com.ActionQueueEntry;
import com.Class126;
import com.Class157;
import com.Class246_Sub3_Sub4_Sub4;
import com.Class248;
import com.Class252;
import com.Class255;
import com.Class308;
import com.Class33;
import com.Class359;
import com.Class38;
import com.Class98_Sub31_Sub2;
import com.Class98_Sub5_Sub3;
import com.GameDefinition;
import com.LoadingScreenSequence;
import com.OpenGlArrayBufferPointer;
import com.Player;
import com.RSByteBuffer;
import com.RtInterfaceAttachment;
import com.client;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.IdentikitDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.preferences.CpuUsagePreferenceField;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;

public final class IdentikitDefinitionParser {
	public static Class126 aClass126_632 = new Class126();

	public static final int method824(byte i) {
		try {
			if (i > -59) {
				return -112;
			}
			if (Class255.aClass293_3208 == null) {
				if (Player.menuOpen || SunDefinition.aClass98_Sub46_Sub8_1994 == null) {
					int i_0_ = client.mouseListener.getMouseX(116);
					int i_1_ = client.mouseListener.getMouseY((byte) 58);
					if (!Class248.aBoolean1896) {
						if (Class38.anInt355 < i_0_ && (i_0_ ^ 0xffffffff) > (Class246_Sub3_Sub4_Sub4.width + Class38.anInt355 ^ 0xffffffff)) {
							int i_2_ = -1;
							for (int i_3_ = 0; i_3_ < Class359.actionCount; i_3_++) {
								if (!Class98_Sub5_Sub3.aBoolean5539) {
									int i_4_ = (Class359.actionCount + -1 + -i_3_) * 16 + 31 + OpenGlArrayBufferPointer.anInt897;
									if ((i_4_ - 13 ^ 0xffffffff) > (i_1_ ^ 0xffffffff) && (i_4_ + 3 ^ 0xffffffff) <= (i_1_ ^ 0xffffffff)) {
										i_2_ = i_3_;
									}
								} else {
									int i_5_ = 33 + OpenGlArrayBufferPointer.anInt897 + (-i_3_ + Class359.actionCount - 1) * 16;
									if ((i_1_ ^ 0xffffffff) < (i_5_ - 13 ^ 0xffffffff) && (i_1_ ^ 0xffffffff) >= (i_5_ + 3 ^ 0xffffffff)) {
										i_2_ = i_3_;
									}
								}
							}
							if (i_2_ != -1) {
								int i_6_ = 0;
								Class157 class157 = new Class157(Class33.actionList);
								for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) class157.method2504((byte) -121); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) class157.method2503(1000)) {
									if (i_6_++ == i_2_) {
										return class98_sub46_sub8.anInt5986;
									}
								}
							}
						}
					} else if (Class38.anInt355 >= i_0_ || Class38.anInt355 + Class246_Sub3_Sub4_Sub4.width <= i_0_) {
						if (Class308.aClass98_Sub46_Sub9_2583 != null && (i_0_ ^ 0xffffffff) < (LoadingScreenSequence.anInt2128 ^ 0xffffffff) && (ScalingSpriteLoadingScreenElement.anInt3439 + LoadingScreenSequence.anInt2128 ^ 0xffffffff) < (i_0_ ^ 0xffffffff)) {
							int i_7_ = -1;
							for (int i_8_ = 0; (i_8_ ^ 0xffffffff) > (Class308.aClass98_Sub46_Sub9_2583.actionCount ^ 0xffffffff); i_8_++) {
								if (!Class98_Sub5_Sub3.aBoolean5539) {
									int i_9_ = 31 + BackgroundColourLSEConfig.anInt3518 + i_8_ * 16;
									if (-13 + i_9_ < i_1_ && i_9_ - -3 >= i_1_) {
										i_7_ = i_8_;
									}
								} else {
									int i_10_ = BackgroundColourLSEConfig.anInt3518 - -33 - -(i_8_ * 16);
									if (-13 + i_10_ < i_1_ && i_10_ + 3 >= i_1_) {
										i_7_ = i_8_;
									}
								}
							}
							if (i_7_ != -1) {
								int i_11_ = 0;
								Class252 class252 = new Class252(Class308.aClass98_Sub46_Sub9_2583.actions);
								for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) class252.method3173(true); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) class252.method3174(0)) {
									if ((i_7_ ^ 0xffffffff) == (i_11_++ ^ 0xffffffff)) {
										return class98_sub46_sub8.anInt5986;
									}
								}
							}
						}
					} else {
						int i_12_ = -1;
						for (int i_13_ = 0; GraphicsLevelPreferenceField.groupCount > i_13_; i_13_++) {
							if (!Class98_Sub5_Sub3.aBoolean5539) {
								int i_14_ = OpenGlArrayBufferPointer.anInt897 + 31 - -(16 * i_13_);
								if ((i_14_ - 13 ^ 0xffffffff) > (i_1_ ^ 0xffffffff) && 3 + i_14_ >= i_1_) {
									i_12_ = i_13_;
								}
							} else {
								int i_15_ = 33 + OpenGlArrayBufferPointer.anInt897 - -(i_13_ * 16);
								if (i_15_ - 13 < i_1_ && (i_15_ + 3 ^ 0xffffffff) <= (i_1_ ^ 0xffffffff)) {
									i_12_ = i_13_;
								}
							}
						}
						if ((i_12_ ^ 0xffffffff) != 0) {
							int i_16_ = 0;
							Class252 class252 = new Class252(RtInterfaceAttachment.actionGroups);
							for (ActionGroup class98_sub46_sub9 = (ActionGroup) class252.method3173(true); class98_sub46_sub9 != null; class98_sub46_sub9 = (ActionGroup) class252.method3174(0)) {
								if (i_16_++ == i_12_) {
									return ((ActionQueueEntry) class98_sub46_sub9.actions.head.nextCacheable).anInt5986;
								}
							}
						}
					}
				} else {
					return SunDefinition.aClass98_Sub46_Sub8_1994.anInt5986;
				}
			}
			return -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fga.A(" + i + ')');
		}
	}

	public static final void method825(int i) {
		CpuUsagePreferenceField.anInt3702 = i;
	}

	public static final Class98_Sub31_Sub2 method831(int i) {
		return BConfigDefinition.aClass98_Sub31_Sub2_3122;
	}

	private Js5					configJs5;

	public Js5					meshJs5;

	private AdvancedMemoryCache	recentlyUsed	= new AdvancedMemoryCache(64);

	public IdentikitDefinitionParser(GameDefinition class279, int i, Js5 class207, Js5 class207_20_) {
		configJs5 = class207;
		meshJs5 = class207_20_;
		configJs5.getFileCount(0, 3);
	}

	public final IdentikitDefinition get(int i, int i_17_) {
		IdentikitDefinition class152;
		synchronized (recentlyUsed) {
			class152 = (IdentikitDefinition) recentlyUsed.get(i_17_ + -123, i);
		}
		if (class152 != null) {
			return class152;
		}
		byte[] is;
		synchronized (configJs5) {
			is = configJs5.getFile(i, i_17_, false);
		}
		class152 = new IdentikitDefinition();
		class152.aClass83_1220 = this;
		if (is != null) {
			class152.decode(false, new RSByteBuffer(is));
		}
		synchronized (recentlyUsed) {
			recentlyUsed.put(i, class152, (byte) -80);
		}
		return class152;
	}

	public final void method827(byte i, int i_18_) {
		synchronized (recentlyUsed) {
			recentlyUsed.makeSoftReferences((byte) 62, i_18_);
		}
	}

	public final void method828(int i) {
		synchronized (recentlyUsed) {
			recentlyUsed.freeSoftReferences((byte) -107);
		}
		if (i >= -92) {
			method827((byte) -66, 21);
		}
	}

	public final void method829(int i) {
		synchronized (recentlyUsed) {
			recentlyUsed.clear(5);
			if (i <= 107) {
				recentlyUsed = null;
			}
		}
	}
}

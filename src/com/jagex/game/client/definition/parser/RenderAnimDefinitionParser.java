/* Class257 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Class128;
import com.Class246_Sub3_Sub4;
import com.Class331;
import com.Class76_Sub5;
import com.Class76_Sub9;
import com.Class95;
import com.Class98_Sub10_Sub29;
import com.NodeShort;
import com.GameDefinition;
import com.OutgoingPacket;
import com.RSByteBuffer;
import com.aa_Sub1;
import com.client;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class RenderAnimDefinitionParser {
	public static int	anInt1946	= -2;
	public static int	anInt1948	= 0;
	public static Js5	worldMapJs5;

	public static final void method3201(byte i, boolean bool) {
		Class128.method2224(22696);
		if (OpenGLHeap.method1683(-11297, client.clientState)) {
			BuildLocation.anInt1511++;
			if ((BuildLocation.anInt1511 ^ 0xffffffff) <= -51 || bool) {
				BuildLocation.anInt1511 = 0;
				if (!Class76_Sub9.aBoolean3788 && aa_Sub1.aClass123_3561 != null) {
					Class76_Sub5.anInt3746++;
					OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, NodeShort.KEEP_ALIVE, Class331.aClass117_2811);
					Class98_Sub10_Sub29.sendPacket(false, frame);
					try {
						Class95.method920((byte) 81);
					} catch (java.io.IOException ioexception) {
						Class76_Sub9.aBoolean3788 = true;
					}
				}
				Class128.method2224(22696);
			}
		}
	}

	private AdvancedMemoryCache	cache	= new AdvancedMemoryCache(64);

	private Js5					configJs5;

	public RenderAnimDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		configJs5 = class207;
		configJs5.getFileCount(0, 32);
	}

	public final void method3197(byte i, int i_0_) {
		synchronized (cache) {
			cache.makeSoftReferences((byte) 62, i_0_);
		}
	}

	public final RenderAnimDefinition method3199(boolean bool, int i) {
		RenderAnimDefinition definition;
		synchronized (cache) {
			definition = (RenderAnimDefinition) cache.get(-123, i);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (configJs5) {
			is = configJs5.getFile(i, 32, bool);
		}
		definition = new RenderAnimDefinition();
		if (is != null) {
			definition.decode(-22400, new RSByteBuffer(is));
		}
		synchronized (cache) {
			cache.put(i, definition, (byte) -80);
		}
		return definition;
	}

	public final void method3200(byte i) {
		synchronized (cache) {
			cache.clear(102);
		}
	}

	public final void method3202(byte i) {
		synchronized (cache) {
			cache.freeSoftReferences((byte) -121);
		}
	}
}

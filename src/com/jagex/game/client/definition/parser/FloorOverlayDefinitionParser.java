/* Class32 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Char;
import com.GameDefinition;
import com.GameShell;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.FloorOverlayDefinition;

public final class FloorOverlayDefinitionParser {
	public static Char[]	aClass246_Sub3Array307;
	public static int		anInt303;
	public static int		anInt305		= 0;
	public static int[]		anIntArray311	= new int[14];
	public static String[]	aStringArray304	= { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
	public static int		clanChatSize;

	public static final void method316(boolean bool) {
		GameShell.frameTimeBase.reset(bool);
		for (int i = 0; i < 32; i++) {
			GameShell.renderTimeTracker[i] = 0L;
		}
		for (int i = 0; (i ^ 0xffffffff) > -33; i++) {
			GameShell.logicTimeTracker[i] = 0L;
		}
		GameShell.missedLogicSteps = 0;
	}

	public static final int method318(int i, byte i_2_) {
		return i & 0x3ff;
	}

	public int					anInt312	= 0;

	public AdvancedMemoryCache	cache		= new AdvancedMemoryCache(64);

	public int					count;

	public Js5					floorOverlayJs5;

	public FloorOverlayDefinitionParser(GameDefinition game, int i, Js5 js5Config) {
		floorOverlayJs5 = js5Config;
		count = floorOverlayJs5.getFileCount(0, 4);
	}

	public final void cacheClear(byte i) {
		synchronized (cache) {
			cache.clear(92);
		}
	}

	public final void cacheMakeSoftReferences(byte i, int i_0_) {
		if (i == 30) {
			synchronized (cache) {
				cache.makeSoftReferences((byte) 62, i_0_);
			}
		}
	}

	public final void cacheRemoveSoftReferences(byte i) {
		synchronized (cache) {
			cache.freeSoftReferences((byte) -118);
		}
	}

	public final FloorOverlayDefinition get(int i, int id) {
		FloorOverlayDefinition floorType;
		synchronized (cache) {
			floorType = (FloorOverlayDefinition) cache.get(-124, id);
		}
		if (floorType != null) {
			return floorType;
		}
		byte[] floorData;
		synchronized (floorOverlayJs5) {
			floorData = floorOverlayJs5.getFile(id, i, false);
		}
		floorType = new FloorOverlayDefinition();
		floorType.id = id;
		floorType.parser = this;
		if (floorData != null) {
			floorType.decode(i + 107, new RSByteBuffer(floorData));
		}
		floorType.method2691((byte) 80);
		synchronized (cache) {
			cache.put(id, floorType, (byte) -80);
		}
		return floorType;
	}
}

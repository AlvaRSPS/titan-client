/* Class132 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.ActionGroup;
import com.Class21_Sub4;
import com.Class287;
import com.Class310;
import com.Class372;
import com.Class42_Sub2;
import com.Class76_Sub4;
import com.Class98_Sub10_Sub29;
import com.Class98_Sub48;
import com.Class98_Sub50;
import com.GameDefinition;
import com.GroundItem;
import com.LoginOpcode;
import com.MapRegion;
import com.OpenGLRenderEffectManager;
import com.OutgoingPacket;
import com.RSByteBuffer;
import com.RSToolkit;
import com.client;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.VarClientDefinition;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;

public final class VarClientDefinitionParser {
	public static Class98_Sub48	aClass98_Sub48_1048	= new Class98_Sub48(0, 0);
	public static int			anInt1043			= 0;
	public static int			anInt1046;
	public static int			anInt1050			= -1;
	public static int[]			anIntArray1044;

	public static void method2234(int i) {
		try {
			aClass98_Sub48_1048 = null;
			if (i == 0) {
				anIntArray1044 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iu.B(" + i + ')');
		}
	}

	public static final int method2235(int i, int i_0_, byte i_1_) {
		try {
			if (i_1_ != 98) {
				anIntArray1044 = null;
			}
			int i_2_;
			if ((i ^ 0xffffffff) >= -20001) {
				if (i > 10000) {
					i_2_ = 3;
					Class98_Sub50.method1672((byte) 19);
				} else if ((i ^ 0xffffffff) < -5001) {
					Class287.setClientPreferences((byte) 66);
					i_2_ = 2;
				} else {
					GraphicsBuffer.method1436(i_1_ + -216, true);
					i_2_ = 1;
				}
			} else {
				GamePreferences.method1284(1);
				i_2_ = 4;
			}
			if ((client.preferences.currentToolkit.getValue((byte) 125) ^ 0xffffffff) != (i_0_ ^ 0xffffffff)) {
				client.preferences.setPreference((byte) -13, i_0_, client.preferences.desiredToolkit);
				Class76_Sub4.method754(i_0_, false, i_1_ + -215);
			}
			Class310.method3618(-5964);
			return i_2_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iu.C(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static final void method2236(boolean bool, int i, String string, String string_3_, int i_4_) {
		do {
			try {
				OutgoingPacket frame = ActionGroup.method1556(false);
				frame.packet.writeByte(LoginOpcode.aClass222_2485.opcode, -86);
				frame.packet.writeShort(0, 1571862888);
				int i_5_ = frame.packet.position;
				frame.packet.writeShort(637, i ^ 0x5db0b968);
				int[] is = Class42_Sub2.method389(12206, frame);
				int i_6_ = frame.packet.position;
				frame.packet.writePJStr1(string_3_, (byte) 113);
				frame.packet.writeShort(client.affiliateId, 1571862888);
				frame.packet.writePJStr1(string, (byte) 113);
				frame.packet.method1221(-75, client.userFlow);
				frame.packet.writeByte(client.gameLanguage, 62);
				frame.packet.writeByte(client.game.id, 126);
				ParamDefinition.method2430(frame.packet, (byte) 0);
				String string_7_ = client.additionalInfo;
				frame.packet.writeByte(string_7_ == null ? 0 : 1, i + -61);
				if (string_7_ != null) {
					frame.packet.writePJStr1(string_7_, (byte) 113);
				}
				frame.packet.writeByte(i_4_, -114);
				frame.packet.writeByte(bool ? 1 : 0, 93);
				frame.packet.position += 7;
				frame.packet.method1235(true, is, i_6_, frame.packet.position);
				frame.packet.method1207((byte) 90, -i_5_ + frame.packet.position);
				Class98_Sub10_Sub29.sendPacket(false, frame);
				Class21_Sub4.anInt5394 = 1;
				Class372.anInt3150 = i;
				GroundItem.anInt4028 = 0;
				OpenGLRenderEffectManager.anInt442 = -3;
				if (i_4_ >= 13) {
					break;
				}
				RSToolkit.aBoolean940 = true;
				MapRegion.method3571(-97);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "iu.D(" + bool + ',' + i + ',' + (string != null ? "{...}" : "null") + ',' + (string_3_ != null ? "{...}" : "null") + ',' + i_4_ + ')');
			}
			break;
		} while (false);
	}

	private Js5					aClass207_1045;

	private AdvancedMemoryCache	aClass79_1049	= new AdvancedMemoryCache(64);

	public int					anInt1047;

	public VarClientDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		aClass207_1045 = class207;
		anInt1047 = aClass207_1045.getFileCount(0, 19);
	}

	public final VarClientDefinition method2237(int i, int i_8_) {
		VarClientDefinition class90;
		synchronized (aClass79_1049) {
			class90 = (VarClientDefinition) aClass79_1049.get(-121, i);
		}
		if (class90 != null) {
			return class90;
		}
		byte[] is;
		synchronized (aClass207_1045) {
			is = aClass207_1045.getFile(i, 19, false);
		}
		class90 = new VarClientDefinition();
		if (is != null) {
			class90.method885(new RSByteBuffer(is), -23453);
		}
		synchronized (aClass79_1049) {
			aClass79_1049.put(i, class90, (byte) -80);
		}
		return class90;
	}
}

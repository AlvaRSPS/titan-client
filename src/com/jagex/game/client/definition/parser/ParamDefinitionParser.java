/* Class365 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Class223;
import com.Class242;
import com.GameDefinition;
import com.RSByteBuffer;
import com.RtInterface;
import com.SignLink;
import com.client;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.crypto.CRC32;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.impl.TextLoadingScreenElement;

public final class ParamDefinitionParser {
	public static boolean					aBoolean3110	= true;
	public static LoadingScreenElementType	aClass113_3109;

	static {
		aClass113_3109 = new LoadingScreenElementType(4, 1);
	}

	public static final void initialize(Js5 class207, SignLink class88, int i) {
		try {
			Class242.aClass88_1848 = class88;
			TimeTools.aString2863 = "";
			Class223.aClass207_1681 = class207;
			if (TextLoadingScreenElement.osName.startsWith("win")) {
				TimeTools.aString2863 += "windows/";
			} else if (!TextLoadingScreenElement.osName.startsWith("linux")) {
				if (TextLoadingScreenElement.osName.startsWith("mac")) {
					TimeTools.aString2863 += "macos/";
				}
			} else {
				TimeTools.aString2863 += "linux/";
			}
			if (i != -1) {
				CRC32.calculate(63, null, -15, false);
			}
			if (Class242.aClass88_1848.msJava) {
				TimeTools.aString2863 += "msjava/";
			} else if (!TextLoadingScreenElement.aString3442.startsWith("amd64") && !TextLoadingScreenElement.aString3442.startsWith("x86_64")) {
				if (!TextLoadingScreenElement.aString3442.startsWith("i386") && !TextLoadingScreenElement.aString3442.startsWith("i486") && !TextLoadingScreenElement.aString3442.startsWith("i586") && !TextLoadingScreenElement.aString3442.startsWith("x86")) {
					if (TextLoadingScreenElement.aString3442.startsWith("ppc")) {
						TimeTools.aString2863 += "ppc/";
					} else {
						TimeTools.aString2863 += "universal/";
					}
				} else {
					TimeTools.aString2863 += "x86/";
				}
			} else {
				TimeTools.aString2863 += "x86_64/";
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wba.G(" + (class207 != null ? "{...}" : "null") + ',' + (class88 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static final RtInterface method3939(int i, RtInterface class293) {
		try {
			RtInterface class293_4_ = client.method102(class293);
			if (class293_4_ == null) {
				class293_4_ = class293.aClass293_2219;
			}
			if (i != 4456) {
				method3939(54, null);
			}
			return class293_4_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wba.H(" + i + ',' + (class293 != null ? "{...}" : "null") + ')');
		}
	}

	public static void method3942(int i) {
		try {
			aClass113_3109 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wba.B(" + i + ')');
		}
	}

	private Js5					configJs5;

	private AdvancedMemoryCache	recentlyUsed	= new AdvancedMemoryCache(64);

	public ParamDefinitionParser(GameDefinition class279, int i, Js5 class207) {
		do {
			try {
				configJs5 = class207;
				if (configJs5 == null) {
					break;
				}
				configJs5.getFileCount(0, 11);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wba.<init>(" + (class279 != null ? "{...}" : "null") + ',' + i + ',' + (class207 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final ParamDefinition list(byte i, int i_5_) {
		try {
			if (i != 31) {
				return null;
			}
			ParamDefinition class149;
			synchronized (recentlyUsed) {
				class149 = (ParamDefinition) recentlyUsed.get(-128, i_5_);
			}
			if (class149 != null) {
				return class149;
			}
			byte[] is;
			synchronized (configJs5) {
				is = configJs5.getFile(i_5_, 11, false);
			}
			class149 = new ParamDefinition();
			if (is != null) {
				class149.method2431(new RSByteBuffer(is), -1);
			}
			synchronized (recentlyUsed) {
				recentlyUsed.put(i_5_, class149, (byte) -80);
			}
			return class149;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wba.F(" + i + ',' + i_5_ + ')');
		}
	}

	public final void method3938(int i) {
		try {
			synchronized (recentlyUsed) {
				recentlyUsed.clear(55);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wba.E(" + i + ')');
		}
	}

	public final void method3943(int i, boolean bool) {
		try {
			if (bool != false) {
				aBoolean3110 = false;
			}
			synchronized (recentlyUsed) {
				recentlyUsed.makeSoftReferences((byte) 62, i);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wba.D(" + i + ',' + bool + ')');
		}
	}

	public final void method3944(int i) {
		try {
			synchronized (recentlyUsed) {
				if (i != -1) {
					/* empty */
				} else {
					recentlyUsed.freeSoftReferences((byte) 48);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wba.A(" + i + ')');
		}
	}
}

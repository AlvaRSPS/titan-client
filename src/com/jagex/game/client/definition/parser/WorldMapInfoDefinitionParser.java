/* Class341 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Char;
import com.Class150;
import com.Class4;
import com.Class65;
import com.Class76_Sub11;
import com.Class87;
import com.Class98_Sub10_Sub20;
import com.Class98_Sub31_Sub4;
import com.Class98_Sub43_Sub4;
import com.GameDefinition;
import com.Minimap;
import com.Mob;
import com.NPC;
import com.NodeObject;
import com.Orientation;
import com.OutgoingOpcode;
import com.PacketParser;
import com.ProceduralTextureSource;
import com.RSByteBuffer;
import com.RtInterface;
import com.aa_Sub3;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.input.impl.AwtKeyListener;

public final class WorldMapInfoDefinitionParser {
	public static OutgoingOpcode	aClass171_2853	= new OutgoingOpcode(17, -1);
	public static int				anInt2856		= 100;
	public static int				anInt2858		= -1;

	public static final void method3810(byte i) {
		try {
			while ((PacketParser.buffer.bitDifference(Class65.currentPacketSize, 75) ^ 0xffffffff) <= -16) {
				int i_4_ = PacketParser.buffer.readBits((byte) -111, 15);
				if (i_4_ == 32767) {
					break;
				}
				boolean bool = false;
				NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(i_4_, -1);
				if (class98_sub39 == null) {
					NPC class246_sub3_sub4_sub2_sub1 = new NPC();
					class246_sub3_sub4_sub2_sub1.index = i_4_;
					class98_sub39 = new NodeObject(class246_sub3_sub4_sub2_sub1);
					ProceduralTextureSource.npc.put(class98_sub39, i_4_, -1);
					bool = true;
					BackgroundColourLSEConfig.aClass98_Sub39Array3516[Class98_Sub10_Sub20.anInt5640++] = class98_sub39;
				}
				NPC npc = class98_sub39.npc;
				Orientation.npcIndices[Class150.npcCount++] = i_4_;
				npc.anInt6406 = Minimap.anInt1544;
				if (npc.definition != null && npc.definition.method2302((byte) 65)) {
					Class98_Sub43_Sub4.method1504(npc, -16255);
				}
				npc.setDefinition(Class4.npcDefinitionList.get(5, PacketParser.buffer.readBits((byte) -64, 14)), 1);
				int i_5_ = PacketParser.buffer.readBits((byte) -67, 1);
				int i_6_ = PacketParser.buffer.readBits((byte) -24, 5);
				if ((i_6_ ^ 0xffffffff) < -16) {
					i_6_ -= 32;
				}
				int i_7_ = PacketParser.buffer.readBits((byte) -34, 5);
				if ((i_7_ ^ 0xffffffff) < -16) {
					i_7_ -= 32;
				}
				int i_8_ = PacketParser.buffer.readBits((byte) -25, 2);
				int i_9_ = PacketParser.buffer.readBits((byte) -68, 1);
				if (i_9_ == 1) {
					Class76_Sub11.anIntArray3796[Class65.anInt502++] = i_4_;
				}
				int i_10_ = 4 + PacketParser.buffer.readBits((byte) -70, 3) << 280873515 & 0x3ee3;
				npc.setSize((byte) 87, npc.definition.boundSize);
				npc.anInt6414 = npc.definition.anInt1091 << -90290813;
				if (bool) {
					npc.method3047(i_10_, true, 31);
				}
				npc.method3049(npc.getSize(0), i_6_ + Class87.localPlayer.pathX[0], (i_5_ ^ 0xffffffff) == -2, (byte) -106, i_7_ + Class87.localPlayer.pathZ[0], i_8_);
				if (npc.definition.method2302((byte) 24)) {
					Class98_Sub31_Sub4.method1383(null, null, npc.pathX[0], 0, 3, npc.pathZ[0], npc.plane, npc);
				}
			}
			if (i >= 112) {
				PacketParser.buffer.endBitwiseAccess((byte) 120);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "up.G(" + i + ')');
		}
	}

	public static void method3811(int i) {
		try {
			if (i == -8433) {
				aClass171_2853 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "up.H(" + i + ')');
		}
	}

	public static final void setDirty(int i, RtInterface class293) {
		if (AwtKeyListener.anInt3803 == class293.anInt2250) {
			aa_Sub3.isDirty[class293.anInt2238] = true;
		}
	}

	public Js5					aClass207_2852;

	private Js5					aClass207_2855;

	private AdvancedMemoryCache	aClass79_2854	= new AdvancedMemoryCache(128);

	public AdvancedMemoryCache	aClass79_2857	= new AdvancedMemoryCache(64);

	public WorldMapInfoDefinitionParser(GameDefinition class279, int i, Js5 class207, Js5 class207_11_) {
		aClass207_2852 = class207_11_;
		aClass207_2855 = class207;
		aClass207_2855.getFileCount(0, 36);
	}

	public final void method3806(int i, boolean bool) {
		if (bool != false) {
			anInt2856 = 20;
		}
		synchronized (aClass79_2854) {
			aClass79_2854.makeSoftReferences((byte) 62, i);
		}
		synchronized (aClass79_2857) {
			aClass79_2857.makeSoftReferences((byte) 62, i);
		}
	}

	public final WorldMapInfoDefinition get(int i, int i_0_) {
		try {
			WorldMapInfoDefinition definition;
			synchronized (aClass79_2854) {
				definition = (WorldMapInfoDefinition) aClass79_2854.get(-126, i_0_);
			}
			if (definition != null) {
				return definition;
			}
			byte[] is;
			synchronized (aClass207_2855) {
				is = aClass207_2855.getFile(i_0_, 36, false);
			}
			definition = new WorldMapInfoDefinition();
			definition.anInt228 = i_0_;
			definition.parser = this;
			if (is != null) {
				definition.method290(new RSByteBuffer(is), 5);
			}
			definition.method291(-25798);
			synchronized (aClass79_2854) {
				aClass79_2854.put(i_0_, definition, (byte) -80);
			}
			return definition;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "up.B(" + i + ',' + i_0_ + ')');
		}
	}

	public final void method3808(int i) {
		try {
			synchronized (aClass79_2854) {
				aClass79_2854.clear(84);
				if (i != 0) {
					anInt2856 = 80;
				}
			}
			synchronized (aClass79_2857) {
				aClass79_2857.clear(i ^ 0x3d);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "up.F(" + i + ')');
		}
	}

	public final void method3809(int i, int i_2_, int i_3_) {
		try {
			if (i_2_ != -30502) {
				aClass171_2853 = null;
			}
			aClass79_2854 = new AdvancedMemoryCache(i_3_);
			aClass79_2857 = new AdvancedMemoryCache(i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "up.A(" + i + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public final void method3813(int i) {
		try {
			synchronized (aClass79_2854) {
				if (i != 36) {
					return;
				}
				aClass79_2854.freeSoftReferences((byte) -118);
			}
			synchronized (aClass79_2857) {
				aClass79_2857.freeSoftReferences((byte) 61);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "up.E(" + i + ')');
		}
	}
}

/* Class11 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.GameDefinition;
import com.RSByteBuffer;
import com.RtInterface;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.CursorDefinition;

public final class CursorDefinitionParser {
	public static RtInterface	aClass293_120;
	public static long			aLong121;
	public static int[]			anIntArray123	= new int[1];

	private Js5					aClass207_122;
	public Js5					aClass207_124;
	private AdvancedMemoryCache	aClass79_125	= new AdvancedMemoryCache(64);

	public AdvancedMemoryCache	aClass79_126	= new AdvancedMemoryCache(2);

	public CursorDefinitionParser(GameDefinition class279, int i, Js5 class207, Js5 class207_2_) {
		aClass207_122 = class207;
		aClass207_124 = class207_2_;
		aClass207_122.getFileCount(0, 33);
	}

	public final void method200(int i) {
		synchronized (aClass79_125) {
			aClass79_125.freeSoftReferences((byte) -108);
		}
		synchronized (aClass79_126) {
			aClass79_126.freeSoftReferences((byte) -100);
		}
	}

	public final void method201(byte i) {
		synchronized (aClass79_125) {
			aClass79_125.clear(123);
		}
		synchronized (aClass79_126) {
			aClass79_126.clear(42);
		}
	}

	public final CursorDefinition method202(int i, int i_1_) {
		CursorDefinition definition;
		synchronized (aClass79_125) {
			definition = (CursorDefinition) aClass79_125.get(-123, i);
		}
		if (definition != null) {
			return definition;
		}
		byte[] is;
		synchronized (aClass207_122) {
			is = aClass207_122.getFile(i, 33, false);
		}
		definition = new CursorDefinition();
		definition.aClass11_1737 = this;
		if (is != null) {
			definition.decode(true, new RSByteBuffer(is));
		}
		synchronized (aClass79_125) {
			aClass79_125.put(i, definition, (byte) -80);
		}
		return definition;
	}

	public final void method203(boolean bool, int i) {
		synchronized (aClass79_125) {
			aClass79_125.makeSoftReferences((byte) 62, i);
		}
		synchronized (aClass79_126) {
			aClass79_126.makeSoftReferences((byte) 62, i);
		}
	}
}

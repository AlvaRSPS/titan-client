
/* Class205 - Decompiled by JODE
 */ package com.jagex.game.client.definition.parser; /*
														*/

import com.Char;
import com.Class119_Sub3;
import com.Class150;
import com.Class151_Sub9;
import com.Class165;
import com.Class172;
import com.Class230;
import com.Class246_Sub3_Sub3;
import com.Class246_Sub3_Sub4_Sub4;
import com.Class74;
import com.Class82;
import com.Class87;
import com.Class98_Sub10_Sub7;
import com.Class98_Sub46_Sub5;
import com.GameDefinition;
import com.GameShell;
import com.ItemSpriteCacheKey;
import com.LoginOpcode;
import com.TextureMetrics;
import com.NPC;
import com.NodeObject;
import com.OpenGLXToolkit;
import com.Player;
import com.PlayerAppearence;
import com.ProceduralTextureSource;
import com.RSByteBuffer;
import com.RSToolkit;
import com.Sprite;
import com.TextResources;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.heap.OpenGLHeap;

import jaggl.OpenGL;

public final class ItemDefinitionParser {
	public static final Class246_Sub3_Sub3 method2711(int i, int i_5_, int i_6_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_5_][i_6_];
		if (class172 == null) {
			return null;
		}
		return class172.aClass246_Sub3_Sub3_1333;
	}

	public static final Class82 method2713(OpenGLXToolkit var_ha_Sub3_Sub2, int i, Class230[] class230s) {
		for (Class230 class230 : class230s) {
			if (class230 == null || (class230.aLong1723 ^ 0xffffffffffffffffL) >= -1L) {
				return null;
			}
		}
		long l = OpenGL.glCreateProgramObjectARB();
		for (int i_10_ = 0; (class230s.length ^ 0xffffffff) < (i_10_ ^ 0xffffffff); i_10_++) {
			OpenGL.glAttachObjectARB(l, class230s[i_10_].aLong1723);
		}
		OpenGL.glLinkProgramARB(l);
		OpenGL.glGetObjectParameterivARB(l, 35714, TextureMetrics.anIntArray1836, 0);
		if (i == TextureMetrics.anIntArray1836[0]) {
			if (TextureMetrics.anIntArray1836[0] == 0) {
				System.out.println("Shader linking failed:");
			}
			OpenGL.glGetObjectParameterivARB(l, 35716, TextureMetrics.anIntArray1836, 1);
			if ((TextureMetrics.anIntArray1836[1] ^ 0xffffffff) < -2) {
				byte[] is = new byte[TextureMetrics.anIntArray1836[1]];
				OpenGL.glGetInfoLogARB(l, TextureMetrics.anIntArray1836[1], TextureMetrics.anIntArray1836, 0, is, 0);
				System.out.println(new String(is));
			}
			if ((TextureMetrics.anIntArray1836[0] ^ 0xffffffff) == -1) {
				for (int i_11_ = 0; (i_11_ ^ 0xffffffff) > (class230s.length ^ 0xffffffff); i_11_++) {
					OpenGL.glDetachObjectARB(l, class230s[i_11_].aLong1723);
				}
				OpenGL.glDeleteObjectARB(l);
				return null;
			}
		}
		return new Class82(var_ha_Sub3_Sub2, l, class230s);
	}

	public static final void method2716(int i) {
		for (Class98_Sub46_Sub5 class98_sub46_sub5 = (Class98_Sub46_Sub5) QuickChatMessageParser.projectiles.getFirst(32); class98_sub46_sub5 != null; class98_sub46_sub5 = (Class98_Sub46_Sub5) QuickChatMessageParser.projectiles.getNext(i ^ ~0x25ed)) {
			Class246_Sub3_Sub4_Sub4 class246_sub3_sub4_sub4 = class98_sub46_sub5.aClass246_Sub3_Sub4_Sub4_5969;
			if (Queue.timer > class246_sub3_sub4_sub4.anInt6466) {
				class98_sub46_sub5.unlink(81);
				class246_sub3_sub4_sub4.method3078(i ^ 0x3254);
			} else if (Queue.timer >= class246_sub3_sub4_sub4.anInt6479) {
				class246_sub3_sub4_sub4.method3080((byte) 109);
				if (class246_sub3_sub4_sub4.anInt6482 > 0) {
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(class246_sub3_sub4_sub4.anInt6482 - 1, -1);
					if (class98_sub39 != null) {
						NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
						if ((class246_sub3_sub4_sub2_sub1.boundExtentsX ^ 0xffffffff) <= -1 && (class246_sub3_sub4_sub2_sub1.boundExtentsX ^ 0xffffffff) > (Class165.mapWidth * 512 ^ 0xffffffff) && (class246_sub3_sub4_sub2_sub1.boundExtentsZ ^ 0xffffffff) <= -1
								&& class246_sub3_sub4_sub2_sub1.boundExtentsZ < 512 * Class98_Sub10_Sub7.mapLength) {
							class246_sub3_sub4_sub4.method3074(class246_sub3_sub4_sub2_sub1.boundExtentsX, StrongReferenceMCNode.getHeight(class246_sub3_sub4_sub4.plane, class246_sub3_sub4_sub2_sub1.boundExtentsZ, class246_sub3_sub4_sub2_sub1.boundExtentsX, i
									+ 33738) - class246_sub3_sub4_sub4.anInt6463, Queue.timer, (byte) 108, class246_sub3_sub4_sub2_sub1.boundExtentsZ);
						}
					}
				}
				if (class246_sub3_sub4_sub4.anInt6482 < 0) {
					int i_14_ = -class246_sub3_sub4_sub4.anInt6482 - 1;
					Player player;
					if (i_14_ != OpenGLHeap.localPlayerIndex) {
						player = Class151_Sub9.players[i_14_];
					} else {
						player = Class87.localPlayer;
					}
					if (player != null && (player.boundExtentsX ^ 0xffffffff) <= -1 && (player.boundExtentsX ^ 0xffffffff) > (512 * Class165.mapWidth ^ 0xffffffff) && (player.boundExtentsZ ^ 0xffffffff) <= -1 && (player.boundExtentsZ
							^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength * 512 ^ 0xffffffff)) {
						class246_sub3_sub4_sub4.method3074(player.boundExtentsX, StrongReferenceMCNode.getHeight(class246_sub3_sub4_sub4.plane, player.boundExtentsZ, player.boundExtentsX, 24111) - class246_sub3_sub4_sub4.anInt6463, Queue.timer, (byte) 108,
								player.boundExtentsZ);
					}
				}
				class246_sub3_sub4_sub4.method3075(-10462, GameDefinition.anInt2099);
				LoginOpcode.method2826(class246_sub3_sub4_sub4, true);
			}
		}
	}

	private ItemSpriteCacheKey		aClass73_1562	= new ItemSpriteCacheKey();
	private boolean					allowMembers;
	public int						anInt1564;
	private Js5						configJs5;
	public int						count;
	private String[]				defaultInterfaceOptions;
	private String[]				defaultOptions;
	public AdvancedMemoryCache		fL				= new AdvancedMemoryCache(50);
	public int						language;
	public Js5						meshJs5;

	private ParamDefinitionParser	paramDefinitionLoader;

	private AdvancedMemoryCache		recentlyUsed	= new AdvancedMemoryCache(64);

	public Class74					spriteCache		= new Class74(250);

	public ItemDefinitionParser(GameDefinition game, int language, boolean allowMembers, ParamDefinitionParser paramList, Js5 configJs5, Js5 meshJs5) {
		this.language = language;
		paramDefinitionLoader = paramList;
		this.meshJs5 = meshJs5;
		this.allowMembers = allowMembers;
		this.configJs5 = configJs5;
		do {
			if (this.configJs5 == null) {
				count = 0;
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			int groupCount = -1 + this.configJs5.getGroupCount((byte) -11);
			count = this.configJs5.getFileCount(0, groupCount) + 256 * groupCount;
		} while (false);
		defaultOptions = new String[] { null, null, TextResources.TAKE.getText(this.language, (byte) 25), null, null };
		defaultInterfaceOptions = new String[] { null, null, null, null, TextResources.DROP.getText(this.language, (byte) 25) };
	}

	public final void cacheClear(int i) {
		synchronized (recentlyUsed) {
			recentlyUsed.clear(110);
		}
		synchronized (fL) {
			fL.clear(64);
		}
		synchronized (spriteCache) {
			spriteCache.clear(true);
		}
	}

	public final void clear(int i) {
		synchronized (fL) {
			if (i <= 13) {
				fL = null;
			}
			fL.clear(38);
		}
	}

	public final void freeSoftReferences(byte i) {
		synchronized (recentlyUsed) {
			recentlyUsed.freeSoftReferences((byte) -86);
		}
		synchronized (fL) {
			fL.freeSoftReferences((byte) 104);
		}
		synchronized (spriteCache) {
			spriteCache.freeSoftReferences(13937);
		}
	}

	public final ItemDefinition get(int id, byte i_12_) {
		ItemDefinition definition;
		synchronized (recentlyUsed) {
			definition = (ItemDefinition) recentlyUsed.get(-126, id);
		}
		if (definition != null) {
			return definition;
		}
		byte[] data;
		synchronized (configJs5) {
			data = configJs5.getFile(Class119_Sub3.method2187((byte) -84, id), Class150.method2437((byte) 124, id), false);
		}
		definition = new ItemDefinition();
		definition.loader = this;
		definition.id = id;
		definition.options = new String[] { null, null, TextResources.TAKE.getText(language, (byte) 25), null, null };
		definition.interfaceOptions = new String[] { null, null, null, null, TextResources.DROP.getText(language, (byte) 25) };
		if (data != null) {
			definition.decode(new RSByteBuffer(data), (byte) -114);
		}
		definition.method3485(850);
		if ((definition.certTemplate ^ 0xffffffff) != 0) {
			definition.generateCert(get(definition.certLink, (byte) -120), 118, get(definition.certTemplate, (byte) -122));
		}
		if (definition.lentTemplate != -1) {
			definition.generateBought(get(definition.lentLink, (byte) -123), get(definition.lentTemplate, (byte) -123), 123);
		}
		if (!allowMembers && definition.members) {
			definition.name = TextResources.MEMBERS_OBJECT.getText(language, (byte) 25);
			definition.options = defaultOptions;
			definition.stockMarket = false;
			definition.interfaceOptions = defaultInterfaceOptions;
			definition.team = 0;
			definition.campaigns = null;
			if (definition.params != null) {
				boolean bool = false;
				for (Node entry = definition.params.startIteration(124); entry != null; entry = definition.params.iterateNext(-1)) {
					ParamDefinition paramDefinition = paramDefinitionLoader.list((byte) 31, (int) entry.hash);
					if (paramDefinition.autoDisable) {
						entry.unlink(44);
					} else {
						bool = true;
					}
				}
				if (!bool) {
					definition.params = null;
				}
			}
		}
		synchronized (recentlyUsed) {
			recentlyUsed.put(id, definition, (byte) -80);
		}
		return definition;
	}

	public final Sprite getCachedSprite(int i, int i_0_, RSToolkit toolkit, PlayerAppearence appearence, int i_1_, int i_2_, int i_3_, int i_4_) {
		aClass73_1562.appearence = appearence != null;
		aClass73_1562.outlineColour = i_3_;
		aClass73_1562.itemId = i_0_;
		aClass73_1562.stackMode = i_2_;
		aClass73_1562.toolkitId = toolkit.id;
		aClass73_1562.stackSize = i_1_;
		aClass73_1562.e = i;
		return (Sprite) spriteCache.method732(aClass73_1562, i_4_ ^ 0x5df8);
	}

	public final Sprite getSprite(Font font, boolean bool, int itemId, int i_17_, int outlineColour, PlayerAppearence appearence, RSToolkit spriteToolkit, int stackMode, boolean nested, int stackSize, RSToolkit toolkit, boolean bool_23_) {
		Sprite sprite = getCachedSprite(i_17_, itemId, toolkit, appearence, stackSize, stackMode, outlineColour, 24056);
		if (sprite != null) {
			return sprite;
		}
		ItemDefinition definition = get(itemId, (byte) -125);
		if (stackSize > 1 && definition.countItems != null) {
			int countItemId = -1;
			for (int idx = 0; idx < 10; idx++) {
				if (definition.countCo[idx] <= stackSize && (definition.countCo[idx] ^ 0xffffffff) != -1) {
					countItemId = definition.countItems[idx];
				}
			}
			if (countItemId != -1) {
				definition = get(countItemId, (byte) -116);
			}
		}
		int[] pixels = definition.renderSprite(outlineColour, stackMode, bool_23_, stackSize, i_17_, toolkit, spriteToolkit, (byte) -125, appearence, font);
		if (pixels == null) {
			return null;
		}
		Sprite sprite_;
		if (nested) {
			sprite_ = spriteToolkit.createSprite(-7962, 0, 36, 32, pixels, 36);
		} else {
			sprite_ = toolkit.createSprite(-7962, 0, 36, 32, pixels, 36);
		}
		if (!nested) {
			ItemSpriteCacheKey itemSpriteCacheKey = new ItemSpriteCacheKey();
			itemSpriteCacheKey.e = i_17_;
			itemSpriteCacheKey.itemId = itemId;
			itemSpriteCacheKey.appearence = appearence != null;
			itemSpriteCacheKey.outlineColour = outlineColour;
			itemSpriteCacheKey.stackSize = stackSize;
			itemSpriteCacheKey.toolkitId = toolkit.id;
			itemSpriteCacheKey.stackMode = stackMode;
			spriteCache.method729(sprite_, itemSpriteCacheKey, false);
		}
		return sprite_;
	}

	public final void makeSoftReferences(int i, int i_16_) {
		synchronized (recentlyUsed) {
			recentlyUsed.makeSoftReferences((byte) 62, i);
		}
		synchronized (fL) {
			fL.makeSoftReferences((byte) 62, i);
		}
		synchronized (spriteCache) {
			spriteCache.makeSoftReferences((byte) 96, i);
		}
	}

	public final void method2712(int i, int i_7_) {
		anInt1564 = i_7_;
		synchronized (fL) {
			fL.clear(103);
		}
	}

	public final void method2717(int i) {
		do {
			synchronized (spriteCache) {
				spriteCache.clear(true);
			}
			if (i == 64) {
				break;
			}
			spriteCache = null;
			break;
		} while (false);
	}

	public final void setAllowMembers(int i, boolean allowMembers) {
		if (!allowMembers != !this.allowMembers) {
			this.allowMembers = allowMembers;
			cacheClear(94);
		}
	}
}

/* Class231 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Char;
import com.Class154;
import com.Class172;
import com.Class246_Sub3_Sub4;
import com.Class331;
import com.GameShell;
import com.HashTableIterator;
import com.Image;
import com.Mob;
import com.Player;
import com.RSByteBuffer;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;

public final class CursorDefinition {
	public static Image	aClass324_1733;
	public static int	anInt1739;
	public static int[]	anIntArray1734	= new int[2];

	public static final String method2873(int i) {
		try {
			if (i != 0) {
				aClass324_1733 = null;
			}
			if (Player.menuOpen || SunDefinition.aClass98_Sub46_Sub8_1994 == null) {
				return "";
			}
			return SunDefinition.aClass98_Sub46_Sub8_1994.actionText;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "op.C(" + i + ')');
		}
	}

	public static final Mob method2874(int i, int i_0_, int i_1_, int i_2_) {
		try {
			Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_][i_1_];
			if (i_2_ <= 42) {
				method2877(-69);
			}
			if (class172 == null) {
				return null;
			}
			Mob class246_sub3_sub4_sub2 = null;
			int i_3_ = -1;
			for (Class154 class154 = class172.aClass154_1325; class154 != null; class154 = class154.aClass154_1233) {
				Class246_Sub3_Sub4 class246_sub3_sub4 = class154.aClass246_Sub3_Sub4_1232;
				if (class246_sub3_sub4 instanceof Mob) {
					Mob mob = (Mob) class246_sub3_sub4;
					int i_5_ = mob.getSize(0) * 256 + -256 - -252;
					int i_6_ = -i_5_ + mob.boundExtentsX >> 734785737;
					int i_7_ = mob.boundExtentsZ + -i_5_ >> -1810572375;
					int i_8_ = mob.boundExtentsX + i_5_ >> 1460607529;
					int i_9_ = mob.boundExtentsZ + i_5_ >> -397287991;
					if (i_0_ >= i_6_ && i_1_ >= i_7_ && (i_0_ ^ 0xffffffff) >= (i_8_ ^ 0xffffffff) && (i_1_ ^ 0xffffffff) >= (i_9_ ^ 0xffffffff)) {
						int i_10_ = (i_9_ + 1 - i_1_) * (i_8_ + 1 + -i_0_);
						if ((i_10_ ^ 0xffffffff) < (i_3_ ^ 0xffffffff)) {
							i_3_ = i_10_;
							class246_sub3_sub4_sub2 = mob;
						}
					}
				}
			}
			return class246_sub3_sub4_sub2;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "op.E(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public static final void method2875(int i, int i_11_) {
		try {
			if (Class331.anIntArray2810 == null || (Class331.anIntArray2810.length ^ 0xffffffff) > (i_11_ ^ 0xffffffff)) {
				Class331.anIntArray2810 = new int[i_11_];
			}
			if (i != 256) {
				method2875(-98, 95);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "op.F(" + i + ',' + i_11_ + ')');
		}
	}

	public static void method2877(int i) {
		try {
			aClass324_1733 = null;
			if (i <= 0) {
				method2878(16);
			}
			anIntArray1734 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "op.H(" + i + ')');
		}
	}

	public static final void method2878(int i) {
		try {
			if (i != 2) {
				anInt1739 = -105;
			}
			HashTableIterator.setClientState(11, false);
			SpriteProgressBarLoadingScreenElement.method3977(true);
			System.gc();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "op.D(" + i + ')');
		}
	}

	public CursorDefinitionParser	aClass11_1737;

	private int						anInt1735;

	public int						anInt1736;

	public int						anInt1738;

	public CursorDefinition() {
		/* empty */
	}

	public final void decode(boolean bool, RSByteBuffer class98_sub22) {
		try {
			if (bool == true) {
				for (;;) {
					int i = class98_sub22.readUnsignedByte((byte) -119);
					if (i == 0) {
						break;
					}
					method2879(i, class98_sub22, -127);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "op.A(" + bool + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	public final synchronized Image method2876(byte i) {
		try {
			if (i != 126) {
				anInt1739 = -116;
			}
			Image class324 = (Image) aClass11_1737.aClass79_126.get(-126, anInt1735);
			if (class324 != null) {
				return class324;
			}
			class324 = Image.loadImage(aClass11_1737.aClass207_124, anInt1735, 0);
			if (class324 != null) {
				aClass11_1737.aClass79_126.put(anInt1735, class324, (byte) -80);
			}
			return class324;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "op.B(" + i + ')');
		}
	}

	private final void method2879(int i, RSByteBuffer class98_sub22, int i_12_) {
		try {
			do {
				if (i != 1) {
					if ((i ^ 0xffffffff) != -3) {
						break;
					}
					anInt1738 = class98_sub22.readUnsignedByte((byte) 84);
					anInt1736 = class98_sub22.readUnsignedByte((byte) -109);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				anInt1735 = class98_sub22.readShort((byte) 127);
			} while (false);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "op.G(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_12_ + ')');
		}
	}
}

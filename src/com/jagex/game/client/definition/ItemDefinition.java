/* Class297 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class284_Sub2_Sub2;
import com.Class41;
import com.Class48;
import com.Class98_Sub10_Sub11;
import com.Class98_Sub6;
import com.HashTableIterator;
import com.ItemAppearanceOverride;
import com.BaseModel;
import com.NodeInteger;
import com.NodeString;
import com.PlayerAppearence;
import com.RSByteBuffer;
import com.RSToolkit;
import com.Sound;
import com.Sprite;
import com.TextResources;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.definition.parser.ItemDefinitionParser;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class ItemDefinition {
	public static int			loginInterfaceId;
	private int					ambient;
	public int[]				campaigns;
	public int					certLink;
	public int					certTemplate;
	public int					colourEquip1;
	private int					contrast	= 0;
	public int					cost;
	public int[]				countCo;
	public int[]				countItems;
	public int					curser1op;
	public int					cursor1;
	public int					cursor1InterfaceOptions;
	public int					cursor2;
	public int					cursor2InterfaceOptions;
	public int					cursor2Options;
	public int					dummyItem;
	public int					id;
	public int					interfaceCursor2;
	public int					interfaceCursor1;
	public String[]				interfaceOptions;
	public int					lentLink;
	public int					lentTemplate;
	public ItemDefinitionParser	loader;
	public int					manHead;
	public int					manHead2;
	public int					manWear;
	public int					manWear2;
	private int					manWearXOff;
	private int					manWearYOff;
	private int					manWearZOff;
	public boolean				members;
	private int					mesh;
	public int					multiStackSize;
	public String				name;
	public String[]				options;
	public HashTable			params;
	public int					pickSizeShift;
	public short[]				recolD;
	private byte[]				recolDPalette;
	private short[]				recolS;
	private int					resizeX;
	private int					resizeY;
	private int					resizeZ;
	public short[]				retexD;
	private short[]				retexS;
	public int					stackable;
	public boolean				stockMarket;
	public int					team;
	public int					womanHead;
	public int					womanHead2;
	public int					womanWear;
	public int					womanWear2;
	public int					womanWear3;
	private int					womanWearXOff;
	private int					womanWearYOff;
	private int					womanWearZOff;
	public int					xAngle2D;
	public int					xOffset2D;
	public int					yAngle2D;
	public int					yOffset2D;
	public int					zAngle2D;
	public int					zoom2D;

	public ItemDefinition() {
		certTemplate = -1;
		certLink = -1;
		cursor2InterfaceOptions = -1;
		womanWear2 = -1;
		team = 0;
		xOffset2D = 0;
		manHead = -1;
		colourEquip1 = -1;
		interfaceCursor1 = -1;
		resizeX = 128;
		manWearXOff = 0;
		resizeY = 128;
		cursor1 = -1;
		xAngle2D = 0;
		multiStackSize = -1;
		manWear = -1;
		cursor2Options = -1;
		curser1op = -1;
		pickSizeShift = 0;
		resizeZ = 128;
		zAngle2D = 0;
		womanHead2 = -1;
		lentTemplate = -1;
		members = false;
		ambient = 0;
		manWearZOff = 0;
		stockMarket = false;
		interfaceCursor2 = -1;
		manWear2 = -1;
		manHead2 = -1;
		zoom2D = 2000;
		name = "null";
		stackable = 0;
		cursor1InterfaceOptions = -1;
		womanHead = -1;
		womanWear = -1;
		yOffset2D = 0;
		manWearYOff = 0;
		cursor2 = -1;
		womanWearYOff = 0;
		womanWear3 = -1;
		womanWearXOff = 0;
		dummyItem = 0;
		womanWearZOff = 0;
		cost = 1;
		yAngle2D = 0;
		lentLink = -1;
	}

	private final void addOutline(int i, int outlineColour, int[] pixels) {
		int i_65_ = 31;
		for (/**/; i_65_ > 0; i_65_--) {
			int i_66_ = i_65_ * 36;
			for (int i_67_ = 35; (i_67_ ^ 0xffffffff) < -1; i_67_--) {
				if ((pixels[i_67_ + i_66_] ^ 0xffffffff) == -1 && (pixels[i_67_ + i_66_ - 37] ^ 0xffffffff) != -1) {
					pixels[i_67_ + i_66_] = outlineColour;
				}
			}
		}
	}

	private final void decode(int opcode, RSByteBuffer buffer, int i_46_) {
		if (opcode != 1) {
			if ((opcode ^ 0xffffffff) == -3) {
				name = buffer.readString((byte) 84);
			} else if (opcode != 4) {
				if (opcode == 5) {
					xAngle2D = buffer.readShort((byte) 127);
				} else if (opcode == 6) {
					yAngle2D = buffer.readShort((byte) 127);
				} else if (opcode != 7) {
					if (opcode != 8) {
						if (opcode != 11) {
							if ((opcode ^ 0xffffffff) == -13) {
								cost = buffer.readInt(-2);
							} else if (opcode == 16) {
								members = true;
							} else if ((opcode ^ 0xffffffff) != -19) {
								if ((opcode ^ 0xffffffff) != -24) {
									if (opcode == 24) {
										manWear2 = buffer.readShort((byte) 127);
									} else if ((opcode ^ 0xffffffff) == -26) {
										womanWear = buffer.readShort((byte) 127);
									} else if (opcode != 26) {
										if (opcode < 30 || opcode >= 35) {
											if ((opcode ^ 0xffffffff) > -36 || opcode >= 40) {
												if (opcode != 40) {
													if ((opcode ^ 0xffffffff) == -42) {
														int length = buffer.readUnsignedByte((byte) 123);
														retexS = new short[length];
														retexD = new short[length];
														for (int index = 0; length > index; index++) {
															retexS[index] = (short) buffer.readShort((byte) 127);
															retexD[index] = (short) buffer.readShort((byte) 127);
														}
													} else if ((opcode ^ 0xffffffff) != -43) {
														if (opcode == 65) {
															stockMarket = true;
														} else if (opcode == 78) {
															colourEquip1 = buffer.readShort((byte) 127);
														} else if ((opcode ^ 0xffffffff) != -80) {
															if (opcode != 90) {
																if (opcode != 91) {
																	if ((opcode ^ 0xffffffff) == -93) {
																		manHead2 = buffer.readShort((byte) 127);
																	} else if ((opcode ^ 0xffffffff) != -94) {
																		if ((opcode ^ 0xffffffff) == -96) {
																			zAngle2D = buffer.readShort((byte) 127);
																		} else if (opcode != 96) {
																			if (opcode != 97) {
																				if ((opcode ^ 0xffffffff) == -99) {
																					certTemplate = buffer.readShort((byte) 127);
																				} else if (opcode < 100 || (opcode ^ 0xffffffff) <= -111) {
																					if (opcode != 110) {
																						if ((opcode ^ 0xffffffff) == -112) {
																							resizeY = buffer.readShort((byte) 127);
																						} else if (opcode == 112) {
																							resizeZ = buffer.readShort((byte) 127);
																						} else if (opcode == 113) {
																							ambient = buffer.readSignedByte((byte) -19);
																						} else if ((opcode ^ 0xffffffff) != -115) {
																							if ((opcode ^ 0xffffffff) == -116) {
																								team = buffer.readUnsignedByte((byte) -108);
																							} else if (opcode != 121) {
																								if ((opcode ^ 0xffffffff) != -123) {
																									if ((opcode ^ 0xffffffff) == -126) {
																										manWearXOff = buffer.readSignedByte((byte) -19) << 119149218;
																										manWearYOff = buffer.readSignedByte((byte) -19) << -1266678398;
																										manWearZOff = buffer.readSignedByte((byte) -19) << 1583045954;
																									} else if ((opcode ^ 0xffffffff) != -127) {
																										if (opcode == 127) {
																											curser1op = buffer.readUnsignedByte((byte) -126);
																											cursor1 = buffer.readShort((byte) 127);
																										} else if ((opcode ^ 0xffffffff) != -129) {
																											if ((opcode ^ 0xffffffff) != -130) {
																												if ((opcode ^ 0xffffffff) != -131) {
																													if (opcode != 132) {
																														if (opcode == 134) {
																															pickSizeShift = buffer.readUnsignedByte((byte) 118);
																														} else if ((opcode ^ 0xffffffff) == -250) {
																															int length = buffer.readUnsignedByte((byte) -115);
																															if (params == null) {
																																int i_50_ = Class48.findNextGreaterPwr2(423660257, length);
																																params = new HashTable(i_50_);
																															}
																															for (int index = 0; (length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
																																boolean stringInstance = (buffer.readUnsignedByte((byte) 18) ^ 0xffffffff) == -2;
																																int key = buffer.readMediumInt(i_46_ ^ ~0xf8);

																																Node node;
																																if (!stringInstance) {
																																	node = new NodeInteger(buffer.readInt(-2));
																																} else {
																																	node = new NodeString(buffer.readString((byte) 84));
																																}
																																params.put(node, key, -1);
																															}
																														}
																													} else {
																														int length = buffer.readUnsignedByte((byte) -114);
																														campaigns = new int[length];
																														for (int index = 0; length > index; index++) {
																															campaigns[index] = buffer.readShort((byte) 127);
																														}
																													}
																												} else {
																													cursor2InterfaceOptions = buffer.readUnsignedByte((byte) -110);
																													interfaceCursor2 = buffer.readShort((byte) 127);
																												}
																											} else {
																												cursor1InterfaceOptions = buffer.readUnsignedByte((byte) 79);
																												interfaceCursor1 = buffer.readShort((byte) 127);
																											}
																										} else {
																											cursor2Options = buffer.readUnsignedByte((byte) -101);
																											cursor2 = buffer.readShort((byte) 127);
																										}
																									} else {
																										womanWearXOff = buffer.readSignedByte((byte) -19) << 1304107298;
																										womanWearYOff = buffer.readSignedByte((byte) -19) << 294608354;
																										womanWearZOff = buffer.readSignedByte((byte) -19) << -629362526;
																									}
																								} else {
																									lentTemplate = buffer.readShort((byte) 127);
																								}
																							} else {
																								lentLink = buffer.readShort((byte) 127);
																							}
																						} else {
																							contrast = 5 * buffer.readSignedByte((byte) -19);
																						}
																					} else {
																						resizeX = buffer.readShort((byte) 127);
																					}
																				} else {
																					if (countItems == null) {
																						countItems = new int[10];
																						countCo = new int[10];
																					}
																					countItems[-100 + opcode] = buffer.readShort((byte) 127);
																					countCo[-100 + opcode] = buffer.readShort((byte) 127);
																				}
																			} else {
																				certLink = buffer.readShort((byte) 127);
																			}
																		} else {
																			dummyItem = buffer.readUnsignedByte((byte) -124);
																		}
																	} else {
																		womanHead2 = buffer.readShort((byte) 127);
																	}
																} else {
																	womanHead = buffer.readShort((byte) 127);
																}
															} else {
																manHead = buffer.readShort((byte) 127);
															}
														} else {
															womanWear3 = buffer.readShort((byte) 127);
														}
													} else {
														int length = buffer.readUnsignedByte((byte) 56);
														recolDPalette = new byte[length];
														for (int index = 0; length > index; index++) {
															recolDPalette[index] = buffer.readSignedByte((byte) -19);
														}
													}
												} else {
													int length = buffer.readUnsignedByte((byte) -100);
													recolD = new short[length];
													recolS = new short[length];
													for (int index = 0; (index ^ 0xffffffff) > (length ^ 0xffffffff); index++) {
														recolS[index] = (short) buffer.readShort((byte) 127);
														recolD[index] = (short) buffer.readShort((byte) 127);
													}
												}
											} else {
												interfaceOptions[-35 + opcode] = buffer.readString((byte) 84);
											}
										} else {
											options[opcode - 30] = buffer.readString((byte) 84);
										}
									} else {
										womanWear2 = buffer.readShort((byte) 127);
									}
								} else {
									manWear = buffer.readShort((byte) 127);
								}
							} else {
								multiStackSize = buffer.readShort((byte) 127);
							}
						} else {
							stackable = 1;
						}
					} else {
						yOffset2D = buffer.readShort((byte) 127);
						if (yOffset2D > 32767) {
							yOffset2D -= 65536;
						}
					}
				} else {
					xOffset2D = buffer.readShort((byte) 127);
					if (xOffset2D > 32767) {
						xOffset2D -= 65536;
					}
				}
			} else {
				zoom2D = buffer.readShort((byte) 127);
			}
		} else {
			mesh = buffer.readShort((byte) 127);
		}
	}

	public final void decode(RSByteBuffer buffer, byte i) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) -123);
			if ((opcode ^ 0xffffffff) == -1) {
				break;
			}
			this.decode(opcode, buffer, 132);
		}

	}

	private final String formatStackSize(byte i, int amount) {
		if (amount < 100000) {
			return "<col=ffff00>" + amount + "</col>";
		}
		if ((amount ^ 0xffffffff) > -10000001) {
			return "<col=ffffff>" + amount / 1000 + TextResources.THOUSAND_SHORT.getText(loader.language, (byte) 25) + "</col>";
		}
		return "<col=00ff80>" + amount / 1000000 + TextResources.MILLION_SHORT.getText(loader.language, (byte) 25) + "</col>";
	}

	public final void generateBought(ItemDefinition definition, ItemDefinition def, int i) {
		cost = 0;
		recolS = definition.recolS;
		womanWear3 = definition.womanWear3;
		colourEquip1 = definition.colourEquip1;
		womanWearXOff = definition.womanWearXOff;
		members = definition.members;
		manWearZOff = definition.manWearZOff;
		womanHead2 = definition.womanHead2;
		womanWear2 = definition.womanWear2;
		manWearXOff = definition.manWearXOff;
		xOffset2D = def.xOffset2D;
		manHead = definition.manHead;
		womanWearZOff = definition.womanWearZOff;
		interfaceOptions = new String[5];
		zoom2D = def.zoom2D;
		recolDPalette = definition.recolDPalette;
		manWear2 = definition.manWear2;
		yAngle2D = def.yAngle2D;
		manHead2 = definition.manHead2;
		name = definition.name;
		params = definition.params;
		mesh = def.mesh;
		manWearYOff = definition.manWearYOff;
		retexD = definition.retexD;
		manWear = definition.manWear;
		womanHead = definition.womanHead;
		recolD = definition.recolD;
		zAngle2D = def.zAngle2D;
		xAngle2D = def.xAngle2D;
		womanWear = definition.womanWear;
		options = definition.options;
		retexS = definition.retexS;
		womanWearYOff = definition.womanWearYOff;
		yOffset2D = def.yOffset2D;
		team = definition.team;
		if (definition.interfaceOptions != null) {
			for (int option = 0; option < 4; option++) {
				interfaceOptions[option] = definition.interfaceOptions[option];
			}
		}
		interfaceOptions[4] = TextResources.DISCARD.getText(loader.language, (byte) 25);
	}

	public final void generateCert(ItemDefinition definition, int i, ItemDefinition newDefinition) {
		retexD = newDefinition.retexD;
		xOffset2D = newDefinition.xOffset2D;
		zAngle2D = newDefinition.zAngle2D;
		recolS = newDefinition.recolS;
		yOffset2D = newDefinition.yOffset2D;
		name = definition.name;
		yAngle2D = newDefinition.yAngle2D;
		stackable = 1;
		retexS = newDefinition.retexS;
		mesh = newDefinition.mesh;
		recolDPalette = newDefinition.recolDPalette;
		xAngle2D = newDefinition.xAngle2D;
		cost = definition.cost;
		recolD = newDefinition.recolD;
		members = definition.members;
		zoom2D = newDefinition.zoom2D;
	}

	public final String getParam(String string, int i, int i_45_) {
		if (params == null) {
			return string;
		}
		NodeString class98_sub15 = (NodeString) params.get(i_45_, i);
		if (class98_sub15 == null) {
			return string;
		}
		return class98_sub15.value;
	}

	public final boolean isHeadModelCached(boolean female, int i, ItemAppearanceOverride override) {
		int head1;
		int head2;
		if (female) {
			if (override == null || override.womanhead == null) {
				head2 = womanHead2;
				head1 = womanHead;
			} else {
				head2 = override.womanhead[1];
				head1 = override.womanhead[0];
			}
		} else if (override == null || override.manhead == null) {
			head1 = manHead;
			head2 = manHead2;
		} else {
			head2 = override.manhead[1];
			head1 = override.manhead[0];
		}
		if (head1 == -1)
			return true;
		boolean is_head_cached = true;
		if (!loader.meshJs5.isFileCached(0, head1, -6329))
			is_head_cached = false;
		if (head2 != -1 && !loader.meshJs5.isFileCached(0, head2, -6329))
			is_head_cached = false;
		return is_head_cached;
	}

	public final boolean isWornModelCached(int i, boolean female, ItemAppearanceOverride override) {
		int wear3;
		int wear2;
		int wear;
		if (female) {
			if (override == null || override.wear2 == null) {
				wear = womanWear;
				wear2 = womanWear2;
				wear3 = womanWear3;
			} else {
				wear = override.wear2[0];
				wear2 = override.wear2[1];
				wear3 = override.wear2[2];
			}
		} else if (override == null || override.wear == null) {
			wear3 = colourEquip1;
			wear2 = manWear2;
			wear = manWear;
		} else {
			wear2 = override.wear[1];
			wear = override.wear[0];
			wear3 = override.wear[2];
		}
		if (wear == -1)
			return true;
		boolean is_worn_model_cached = true;
		if (!loader.meshJs5.isFileCached(i, wear, -6329))
			is_worn_model_cached = false;
		if (wear2 != -1 && !loader.meshJs5.isFileCached(0, wear2, -6329))
			is_worn_model_cached = false;
		if (wear3 != -1 && !loader.meshJs5.isFileCached(0, wear3, -6329))
			is_worn_model_cached = false;
		return is_worn_model_cached;
	}

	public final void method3485(int i) {
		do {
			if (i == 850) {
				break;
			}
			campaigns = null;
			break;
		} while (false);
	}

	public final BaseModel method3486(boolean female, int i, ItemAppearanceOverride override) {
		int head1;
		int head2;
		if (female) {
			if (override == null || override.womanhead == null) {
				head2 = womanHead2;
				head1 = womanHead;
			} else {
				head1 = override.womanhead[0];
				head2 = override.womanhead[1];
			}
		} else if (override == null || override.manhead == null) {
			head1 = manHead;
			head2 = manHead2;
		} else {
			head1 = override.manhead[0];
			head2 = override.manhead[1];
		}
		if (head1 == -1)
			return null;
		BaseModel model = Class98_Sub6.fromFile(i, i ^ ~0x2423, loader.meshJs5, head1);
		if (model.version < 13)
			model.scaleLog2(2, 2);
		if (head2 != -1) {
			BaseModel model2 = Class98_Sub6.fromFile(0, -9252, loader.meshJs5, head2);
			if (model2.version < 13)
				model2.scaleLog2(2, 2);
			BaseModel[] models = { model, model2 };
			model = new BaseModel(models, 2);
		}
		if (recolS != null) {
			short[] recol_d;
			if (override != null && override.recol_d != null)
				recol_d = override.recol_d;
			else
				recol_d = recolD;
			for (int idx = 0; idx < recolS.length; idx++)
				model.replaceColour(2, recolS[idx], recol_d[idx]);
		}
		if (retexS != null) {
			short[] retex_d;
			if (override == null || override.retex_d == null)
				retex_d = retexD;
			else
				retex_d = override.retex_d;
			for (int idx = 0; idx < retexS.length; idx++)
				model.replaceTexture(retex_d[idx], (byte) 115, retexS[idx]);
		}
		return model;
	}

	private final int[] method3491(int[] in, int i, int i_29_) {
		int[] out = new int[1152];
		int ptr = 0;
		for (int y = 0; (y ^ 0xffffffff) > -33; y++) {
			for (int x = 0; x < 36; x++) {
				int valueIn = in[ptr];
				if ((valueIn ^ 0xffffffff) == -1) {
					if (x <= 0 || (in[-1 + ptr] ^ 0xffffffff) == -1) {
						if ((y ^ 0xffffffff) >= -1 || (in[-36 + ptr] ^ 0xffffffff) == -1) {
							if (x < 35 && in[ptr + 1] != 0) {
								valueIn = i_29_;
							} else if (y < 31 && (in[36 + ptr] ^ 0xffffffff) != -1) {
								valueIn = i_29_;
							}
						} else {
							valueIn = i_29_;
						}
					} else {
						valueIn = i_29_;
					}
				}
				out[ptr++] = valueIn;
			}
		}
		return out;
	}

	public final ItemDefinition method3493(byte i, int i_39_) {
		if (countItems != null && (i_39_ ^ 0xffffffff) < -2) {
			int i_41_ = -1;
			for (int i_42_ = 0; i_42_ < 10; i_42_++) {
				if (countCo[i_42_] <= i_39_ && countCo[i_42_] != 0) {
					i_41_ = countItems[i_42_];
				}
			}
			if ((i_41_ ^ 0xffffffff) != 0) {
				return loader.get(i_41_, (byte) -124);
			}
		}
		return this;
	}

	public final int method3494(int i, byte i_43_, int i_44_) {
		if (params == null) {
			return i_44_;
		}
		NodeInteger class98_sub34 = (NodeInteger) params.get(i, -1);
		if (class98_sub34 == null) {
			return i_44_;
		}
		return class98_sub34.value;
	}

	public final BaseModel method3500(boolean bool, ItemAppearanceOverride override, int i) {
		int i_65_;
		int i_66_;
		int i_67_;
		if (!bool) {
			if (override != null && override.wear != null) {
				i_66_ = override.wear[1];
				i_67_ = override.wear[0];
				i_65_ = override.wear[2];
			} else {
				i_65_ = colourEquip1;
				i_67_ = manWear;
				i_66_ = manWear2;
			}
		} else if (override != null && override.wear2 != null) {
			i_65_ = override.wear2[2];
			i_66_ = override.wear2[1];
			i_67_ = override.wear2[0];
		} else {
			i_67_ = womanWear;
			i_66_ = womanWear2;
			i_65_ = womanWear3;
		}
		if (i_67_ == -1)
			return null;
		BaseModel model = Class98_Sub6.fromFile(0, -9252, loader.meshJs5, i_67_);
		if (model == null)
			return null;
		if (model.version < 13)
			model.scaleLog2(13746, 2);
		if (i_66_ != -1) {
			BaseModel var_hg_68_ = Class98_Sub6.fromFile(0, -9252, loader.meshJs5, i_66_);
			if (var_hg_68_.version < 13)
				var_hg_68_.scaleLog2(13746, 2);
			if (i_65_ != -1) {
				BaseModel var_hg_69_ = Class98_Sub6.fromFile(0, -9252, loader.meshJs5, i_65_);
				if (var_hg_69_.version < 13)
					var_hg_69_.scaleLog2(13746, 2);
				BaseModel[] var_hgs = { model, var_hg_68_, var_hg_69_ };
				model = new BaseModel(var_hgs, 3);
			} else {
				BaseModel[] var_hgs = { model, var_hg_68_ };
				model = new BaseModel(var_hgs, 2);
			}
		}
		if (!bool && (manWearXOff != 0 || manWearYOff != 0 || manWearZOff != 0))
			model.method2597(manWearZOff, manWearXOff, (byte) 122, manWearYOff);
		if (bool && (womanWearXOff != 0 || womanWearYOff != 0 || womanWearZOff != 0))
			model.method2597(womanWearZOff, womanWearXOff, (byte) 63, womanWearYOff);
		if (recolS != null) {
			short[] recol_D;
			if (override != null && override.recol_d != null)
				recol_D = override.recol_d;
			else
				recol_D = recolD;
			for (int index = 0; (index ^ 0xffffffff) > (recolS.length ^ 0xffffffff); index++)
				model.replaceColour(0, recolS[index], recol_D[index]);
		}
		if (retexS != null) {
			short[] retex_D;
			if (override == null || override.retex_d == null)
				retex_D = retexD;
			else
				retex_D = override.retex_d;
			for (int index = 0; (index ^ 0xffffffff) > (retexS.length ^ 0xffffffff); index++)
				model.replaceTexture(retex_D[index], (byte) 107, retexS[index]);
		}
		return model;
	}

	public final ModelRenderer method3501(int i, int i_75_, int i_76_, AnimationDefinition class97, int i_77_, RSToolkit var_ha, int i_78_, int i_79_, PlayerAppearence class313) {
		if (countItems != null && i_78_ > 1) {
			int i_80_ = -1;
			for (int i_81_ = 0; (i_81_ ^ 0xffffffff) > -11; i_81_++) {
				if ((i_78_ ^ 0xffffffff) <= (countCo[i_81_] ^ 0xffffffff) && (countCo[i_81_] ^ 0xffffffff) != -1) {
					i_80_ = countItems[i_81_];
				}
			}
			if ((i_80_ ^ 0xffffffff) != 0) {
				return loader.get(i_80_, (byte) -119).method3501(i, i_75_, i_76_, class97, i_77_, var_ha, 1, i_79_, class313);
			}
		}
		int i_82_ = i_75_;
		if (class97 != null) {
			i_82_ |= class97.method932(true, i_76_, true, i_77_);
		}
		ModelRenderer class146;
		synchronized (loader.fL) {
			class146 = (ModelRenderer) loader.fL.get(-123, id | var_ha.id << -1658708323);
			if (i_79_ != 128) {
				cursor2InterfaceOptions = -112;
			}
		}
		if (class146 == null || (var_ha.c(class146.functionMask(), i_82_) ^ 0xffffffff) != -1) {
			if (class146 != null) {
				i_82_ = var_ha.mergeFunctionMask(i_82_, class146.functionMask());
			}
			int mask = i_82_;
			if (retexS != null) {
				mask |= 0x8000;
			}
			if (recolS != null || class313 != null) {
				mask |= 0x4000;
			}
			if (resizeX != 128) {
				mask |= 0x1;
			}
			if ((resizeX ^ 0xffffffff) != -129) {
				mask |= 0x2;
			}
			if ((resizeX ^ 0xffffffff) != -129) {
				mask |= 0x4;
			}
			BaseModel class178 = Class98_Sub6.fromFile(0, -9252, loader.meshJs5, mesh);
			if (class178 == null) {
				return null;
			}
			if ((class178.version ^ 0xffffffff) > -14) {
				class178.scaleLog2(13746, 2);
			}
			class146 = var_ha.createModelRenderer(class178, mask, loader.anInt1564, ambient + 64, 850 - -contrast);
			if (resizeX != 128 || resizeY != 128 || (resizeZ ^ 0xffffffff) != -129) {
				class146.scale(resizeX, resizeY, resizeZ);
			}
			if (recolS != null) {
				for (int i_84_ = 0; recolS.length > i_84_; i_84_++) {
					if (recolDPalette == null || (recolDPalette.length ^ 0xffffffff) >= (i_84_ ^ 0xffffffff)) {
						class146.recolour(recolS[i_84_], recolD[i_84_]);
					} else {
						class146.recolour(recolS[i_84_], Sound.clientPalette[recolDPalette[i_84_] & 0xff]);
					}
				}
			}
			if (retexS != null) {
				for (int i_85_ = 0; (retexS.length ^ 0xffffffff) < (i_85_ ^ 0xffffffff); i_85_++) {
					class146.retexture(retexS[i_85_], retexD[i_85_]);
				}
			}
			if (class313 != null) {
				for (int i_86_ = 0; (i_86_ ^ 0xffffffff) > -6; i_86_++) {
					for (int i_87_ = 0; (HashTableIterator.colourToReplace.length ^ 0xffffffff) < (i_87_ ^ 0xffffffff); i_87_++) {
						if ((class313.colours[i_86_] ^ 0xffffffff) > (HashTableIterator.colourToReplace[i_87_][i_86_].length ^ 0xffffffff)) {
							class146.recolour(Class98_Sub10_Sub11.aShortArrayArray5597[i_87_][i_86_], HashTableIterator.colourToReplace[i_87_][i_86_][class313.colours[i_86_]]);
						}
					}
				}
			}
			class146.updateFunctionMask(i_82_);
			synchronized (loader.fL) {
				loader.fL.put(id | var_ha.id << -1883478627, class146, (byte) -80);
			}
		}
		if (class97 != null) {
			class146 = class97.method937(i_77_, i, i_82_, 42, class146, i_76_);
		}
		class146.updateFunctionMask(i_75_);
		return class146;
	}

	public final int[] renderSprite(int outlineColour, int stackMode, boolean bool, int stackSize, int i_10_, RSToolkit var_ha, RSToolkit spriteToolkit, byte i_12_, PlayerAppearence appearence, Font font) {
		BaseModel inventoryModel = Class98_Sub6.fromFile(0, -9252, loader.meshJs5, mesh);
		if (inventoryModel == null) {
			return null;
		}
		if (inventoryModel.version < 13) {
			inventoryModel.scaleLog2(13746, 2);
		}
		if (recolS != null) {
			for (int recIdx = 0; (recolS.length ^ 0xffffffff) < (recIdx ^ 0xffffffff); recIdx++) {
				if (recolDPalette != null && (recIdx ^ 0xffffffff) > (recolDPalette.length ^ 0xffffffff)) {
					inventoryModel.replaceColour(0, recolS[recIdx], Sound.clientPalette[0xff & recolDPalette[recIdx]]);
				} else {
					inventoryModel.replaceColour(i_12_ + 125, recolS[recIdx], recolD[recIdx]);
				}
			}
		}
		if (retexS != null) {
			for (int retIdx = 0; (retexS.length ^ 0xffffffff) < (retIdx ^ 0xffffffff); retIdx++) {
				inventoryModel.replaceTexture(retexD[retIdx], (byte) -99, retexS[retIdx]);
			}
		}
		if (appearence != null) {
			for (int colour = 0; colour < 5; colour++) {
				for (int index = 0; (HashTableIterator.colourToReplace.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
					if (HashTableIterator.colourToReplace[index][colour].length > appearence.colours[colour]) {
						inventoryModel.replaceColour(0, Class98_Sub10_Sub11.aShortArrayArray5597[index][colour], HashTableIterator.colourToReplace[index][colour][appearence.colours[colour]]);
					}
				}
			}
		}
		int value = 2048;
		boolean doScale = false;
		if (resizeX != 128 || resizeY != 128 || resizeZ != 128) {
			doScale = true;
			value |= 0x7;
		}
		ModelRenderer renderer = spriteToolkit.createModelRenderer(inventoryModel, value, 64, 64 - -ambient, contrast + 768);
		if (!renderer.isFullyCached()) {
			return null;
		}
		if (doScale) {
			renderer.scale(resizeX, resizeY, resizeZ);
		}
		Sprite backgroundSprite = null;
		if (certTemplate != -1) {
			backgroundSprite = loader.getSprite(font, false, certLink, 1, 0, appearence, spriteToolkit, 0, true, 10, var_ha, true);
			if (backgroundSprite == null) {
				return null;
			}
		} else if ((lentTemplate ^ 0xffffffff) != 0) {
			backgroundSprite = loader.getSprite(font, false, lentLink, i_10_, outlineColour, appearence, spriteToolkit, 0, true, stackSize, var_ha, false);
			if (backgroundSprite == null) {
				return null;
			}
		}
		int zoomValue;
		if (bool) {
			zoomValue = (int) (zoom2D * 1.5) << -21057662;
		} else if (i_10_ == 2) {
			zoomValue = (int) (1.04 * zoom2D) << 839018274;
		} else {
			zoomValue = zoom2D << -512379550;
		}
		spriteToolkit.DA(16, 16, 512, 512);
		Matrix class111 = spriteToolkit.createMatrix();
		class111.initIdentity();
		spriteToolkit.a(class111);
		spriteToolkit.setAmbientIntensity(1.0F);
		spriteToolkit.setSun(16777215, 1.0F, 1.0F, -50.0F, -10.0F, -50.0F);
		Matrix class111_20_ = spriteToolkit.method1793();
		class111_20_.initRotZ(-zAngle2D << 3);
		class111_20_.initRotY(yAngle2D << 3);
		class111_20_.translate(xOffset2D << 2, (zoomValue * Class284_Sub2_Sub2.SINE[xAngle2D << 3] >> 14) + -(renderer.fa() / 2) + (yOffset2D << 2), (zoomValue * Class284_Sub2_Sub2.COSINE[xAngle2D << 3] >> 14) - -(yOffset2D << 2));
		class111_20_.initRotX(xAngle2D << 3);
		int nearPlane = spriteToolkit.getNearPlane();
		int farPlane = spriteToolkit.getFarPlane();
		spriteToolkit.setClipPlanes(50, 2147483647);
		spriteToolkit.ya();
		spriteToolkit.clearClip();
		spriteToolkit.fillRectangle(0, 0, 36, 32, 0, 0);
		renderer.method2325(class111_20_, null, 1);
		spriteToolkit.setClipPlanes(nearPlane, farPlane);
		int[] pixelData = spriteToolkit.getPixels(0, 0, 36, 32);
		if ((i_10_ ^ 0xffffffff) <= -2) {
			pixelData = method3491(pixelData, i_12_ + 3, -16777214);
			if (i_10_ >= 2) {
				pixelData = method3491(pixelData, -109, -1);
			}
		}
		if ((outlineColour ^ 0xffffffff) != -1) {
			addOutline(-76, outlineColour, pixelData);
		}
		spriteToolkit.createSprite(-7962, 0, 36, 32, pixelData, 36).draw(0, 0);
		if (certTemplate == -1) {
			if (lentTemplate != -1) {
				backgroundSprite.draw(0, 0);
			}
		} else {
			backgroundSprite.draw(0, 0);
		}
		if ((stackMode ^ 0xffffffff) == -2 || stackMode == 2 && (stackable == 1 || stackSize != 1) && (stackSize ^ 0xffffffff) != 0) {
			font.drawString((byte) 76, 9, formatStackSize((byte) -93, stackSize), -256, -16777215, 0);
		}
		pixelData = spriteToolkit.getPixels(0, 0, 36, 32);
		for (int index = 0; (pixelData.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
			if ((0xffffff & pixelData[index]) == 0) {
				pixelData[index] = 0;
			} else {
				pixelData[index] = Class41.or(pixelData[index], -16777216);
			}
		}
		return pixelData;
	}
}

/* Class199 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class165;
import com.Class76_Sub9;
import com.Class98_Sub10_Sub7;
import com.OutgoingOpcode;
import com.OutputStream_Sub2;
import com.RSByteBuffer;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.preferences.IdleAnimationsPreferenceField;

public final class FloorOverlayDefinition {
	public static OutgoingOpcode				aClass171_1533	= new OutgoingOpcode(63, -1);
	public static int							anInt1539;
	public static int							anInt1541		= -1;
	public static FloorOverlayDefinitionParser	floorOverlayDefinitionList;

	public static final void method2685(int i, byte i_0_) {
		if (OutputStream_Sub2.aByteArrayArrayArray41 == null) {
			OutputStream_Sub2.aByteArrayArrayArray41 = new byte[4][Class165.mapWidth][Class98_Sub10_Sub7.mapLength];
		}
		for (int i_1_ = 0; i_1_ < 4; i_1_++) {
			for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > (Class165.mapWidth ^ 0xffffffff); i_2_++) {
				for (int i_3_ = 0; (i_3_ ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff); i_3_++) {
					OutputStream_Sub2.aByteArrayArrayArray41[i_1_][i_2_][i_3_] = i_0_;
				}
			}
		}
	}

	public static final boolean method2690(int i, int i_7_) {
		if (i_7_ != 8) {
			return false;
		}
		return !((i ^ 0xffffffff) != -5 && i != 8 && (i ^ 0xffffffff) != -12);
	}

	public static final void printError(String string, byte i) {
		System.out.println("Error: " + Class76_Sub9.replace("\n", 4185, "%0a", string));
	}

	public boolean						aBoolean1526;
	public boolean						aBoolean1527;
	public int							waterScale	= 64;
	public int							waterColour	= 1190717;
	public int							anInt1535;
	public int							colourHsl;
	public int							id;
	public int							blendColour;
	public boolean						occlude;

	public FloorOverlayDefinitionParser	parser;

	public int							textcoordScale;

	public int							textureId;

	public int							waterIntensity;

	public FloorOverlayDefinition() {
		aBoolean1526 = false;
		aBoolean1527 = true;
		waterIntensity = 127;
		colourHsl = 0;
		anInt1535 = 8;
		blendColour = -1;
		textcoordScale = 512;
		occlude = true;
		textureId = -1;
	}

	private final void decode(int i, int opcode, RSByteBuffer buffer) {
		if ((opcode ^ 0xffffffff) != -2) {
			if ((opcode ^ 0xffffffff) == -3) {
				textureId = buffer.readUnsignedByte((byte) -108);
			} else if ((opcode ^ 0xffffffff) == -4) {
				textureId = buffer.readShort((byte) 127);
				if ((textureId ^ 0xffffffff) == -65536) {
					textureId = -1;
				}
			} else if ((opcode ^ 0xffffffff) != -6) {
				if (opcode == 7) {
					blendColour = IdleAnimationsPreferenceField.rgbToHsl(buffer.readMediumInt(i ^ ~0xb7e), i + -2733);
				} else if (opcode == 8) {
					parser.anInt312 = id;
				} else if ((opcode ^ 0xffffffff) == -10) {
					textcoordScale = buffer.readShort((byte) 127) << -242726558;
				} else if ((opcode ^ 0xffffffff) == -11) {
					occlude = false;
				} else if (opcode == 11) {
					anInt1535 = buffer.readUnsignedByte((byte) -118);
				} else if ((opcode ^ 0xffffffff) == -13) {
					aBoolean1526 = true;
				} else if (opcode == 13) {
					waterColour = buffer.readMediumInt(-128);
				} else if ((opcode ^ 0xffffffff) != -15) {
					if ((opcode ^ 0xffffffff) == -17) {
						waterIntensity = buffer.readUnsignedByte((byte) 12);
					}
				} else {
					waterScale = buffer.readUnsignedByte((byte) -127) << 33077314;
				}
			} else {
				aBoolean1527 = false;
			}
		} else {
			colourHsl = IdleAnimationsPreferenceField.rgbToHsl(buffer.readMediumInt(i + -2942), 76);
		}
	}

	public final void decode(int i, RSByteBuffer buffer) {
		for (;;) {
			int field = buffer.readUnsignedByte((byte) 126);
			if (field == 0) {
				break;
			}
			this.decode(2818, field, buffer);
		}
	}

	public final void method2691(byte i) {
		anInt1535 = anInt1535 << -1953787544 | id;

	}
}

/* Class107 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class98_Sub6;
import com.BaseModel;
import com.RSByteBuffer;
import com.RSToolkit;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.AnimationDefinitionParser;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class GraphicsDefinition {
	public static String aString912;

	public static final int method1720(int i, int i_0_, int i_1_, int i_2_) {
		try {
			i_2_ &= 0x3;
			if ((i_2_ ^ 0xffffffff) == -1) {
				return i;
			}
			if ((i_2_ ^ 0xffffffff) == -2) {
				return i_1_;
			}
			if (i_0_ != 0) {
				aString912 = null;
			}
			if (i_2_ == 2) {
				return -i + 7;
			}
			return -i_1_ + 7;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gp.F(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public static final int method1724(int i, int i_29_, int i_30_, int i_31_) {
		try {
			i_29_ &= 0x3;
			if ((i_29_ ^ 0xffffffff) == -1) {
				return i_30_;
			}
			if (i != 7) {
				return -123;
			}
			if (i_29_ == 1) {
				return 4095 - i_31_;
			}
			if ((i_29_ ^ 0xffffffff) == -3) {
				return -i_30_ + 4095;
			}
			return i_31_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gp.G(" + i + ',' + i_29_ + ',' + i_30_ + ',' + i_31_ + ')');
		}
	}

	public static void method1726(boolean bool) {
		try {
			if (bool == false) {
				aString912 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "gp.E(" + bool + ')');
		}
	}

	public boolean					aBoolean909;
	public byte						aByte923;
	private int						ambience;
	public int						animation	= -1;
	private int						anInt918;
	public int						anInt925;
	private int						contrast;
	private int						modelId;
	private short[]					originalColours;
	private short[]					originalTextures;
	public GraphicsDefinitionParser	parser;
	private short[]					replacementColours;
	private short[]					replacementTextures;

	private int						rotation;

	private int						scaleXZ;

	private int						scaleY;

	public GraphicsDefinition() {
		aBoolean909 = false;
		contrast = 0;
		scaleY = 128;
		anInt918 = -1;
		rotation = 0;
		aByte923 = (byte) 0;
		ambience = 0;
		scaleXZ = 128;
	}

	public final void decode(int i, RSByteBuffer buffer) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) -114);
			if (opcode == 0) {
				break;
			}
			this.decode(i + -120, buffer, opcode);
		}
	}

	private final void decode(int i, RSByteBuffer buffer, int opcode) {
		if (opcode == 1) {
			modelId = buffer.readShort((byte) 127);
		} else if (opcode == 2) {
			animation = buffer.readShort((byte) 127);
		} else if (opcode == 4) {
			scaleXZ = buffer.readShort((byte) 127);
		} else if (opcode != 5) {
			if ((opcode ^ 0xffffffff) == -7) {
				rotation = buffer.readShort((byte) 127);
			} else if (opcode == 7) {
				ambience = buffer.readUnsignedByte((byte) -120);
			} else if ((opcode ^ 0xffffffff) == -9) {
				contrast = buffer.readUnsignedByte((byte) 62);
			} else if (opcode == 9) {
				anInt918 = 8224;
				aByte923 = (byte) 3;
			} else if ((opcode ^ 0xffffffff) == -11) {
				aBoolean909 = true;
			} else if (opcode != 11) {
				if ((opcode ^ 0xffffffff) == -13) {
					aByte923 = (byte) 4;
				} else if (opcode != 13) {
					if ((opcode ^ 0xffffffff) == -15) {
						aByte923 = (byte) 2;
						anInt918 = 256 * buffer.readUnsignedByte((byte) 91);
					} else if ((opcode ^ 0xffffffff) == -16) {
						aByte923 = (byte) 3;
						anInt918 = buffer.readShort((byte) 127);
					} else if (opcode == 16) {
						aByte923 = (byte) 3;
						anInt918 = buffer.readInt(-2);
					} else if (opcode != 40) {
						if (opcode == 41) {
							int count = buffer.readUnsignedByte((byte) 95);
							originalTextures = new short[count];
							replacementTextures = new short[count];
							for (int index = 0; count > index; index++) {
								originalTextures[index] = (short) buffer.readShort((byte) 127);
								replacementTextures[index] = (short) buffer.readShort((byte) 127);
							}
						}
					} else {
						int count = buffer.readUnsignedByte((byte) 75);
						originalColours = new short[count];
						replacementColours = new short[count];
						for (int index = 0; index < count; index++) {
							replacementColours[index] = (short) buffer.readShort((byte) 127);
							originalColours[index] = (short) buffer.readShort((byte) 127);
						}
					}
				} else {
					aByte923 = (byte) 5;
				}
			} else {
				aByte923 = (byte) 1;
			}
		} else {
			scaleY = buffer.readShort((byte) 127);
		}
	}

	public final ModelRenderer method1721(RSToolkit toolkit, int i, int i_3_, int i_4_, AnimationDefinitionParser animationParser, int i_5_, int i_6_) {
		return method1723(i_4_, i_6_, 0, i, null, null, 0, (byte) 5, animationParser, false, toolkit, false, 0, i_5_);
	}

	public final ModelRenderer method1722(RSToolkit toolkit, AnimationDefinitionParser animationParser, int i, int i_7_, int i_8_, boolean bool, Ground var_s, int i_9_, int i_10_, Ground var_s_11_, int i_12_, int i_13_, byte i_14_) {
		return method1723(i_7_, i, i_9_, i_13_, var_s_11_, var_s, i_8_, (byte) 2, animationParser, false, toolkit, bool, i_12_, i_10_);
	}

	private final ModelRenderer method1723(int functionMask, int i_15_, int i_16_, int i_17_, Ground var_s, Ground var_s_18_, int i_19_, byte i_20_, AnimationDefinitionParser class183, boolean bool, RSToolkit toolkit, boolean bool_21_, int i_22_, int i_23_) {
		int mask = functionMask;
		AnimationDefinition animationDefinition = animation == -1 || (i_15_ ^ 0xffffffff) == 0 ? null : class183.method2623(animation, 16383);
		bool_21_ = bool_21_ & (aByte923 ^ 0xffffffff) != -1;
		if (bool != false) {
			anInt918 = 18;
		}
		if (animationDefinition != null) {
			mask |= animationDefinition.method932(false, i_15_, true, i_17_);
		}
		if (bool_21_) {
			mask = mask | ((aByte923 ^ 0xffffffff) != -4 ? 2 : 7);
		}
		if (scaleY != 128) {
			mask |= 0x2;
		}
		if ((scaleXZ ^ 0xffffffff) != -129 || (rotation ^ 0xffffffff) != -1) {
			mask |= 0x5;
		}
		ModelRenderer renderer;
		synchronized (parser.cache) {
			renderer = (ModelRenderer) parser.cache.get(-125, anInt925 |= toolkit.id << 1821407229);
		}
		if (renderer == null || toolkit.c(renderer.functionMask(), mask) != 0) {
			if (renderer != null) {
				mask = toolkit.mergeFunctionMask(mask, renderer.functionMask());
			}
			int attributes = mask;
			if (replacementColours != null) {
				attributes |= 0x4000;
			}
			if (originalTextures != null) {
				attributes |= 0x8000;
			}
			BaseModel base = Class98_Sub6.fromFile(0, -9252, parser.models, modelId);
			if (base == null) {
				return null;
			}
			if (base.version < 13) {
				base.scaleLog2(13746, 2);
			}
			renderer = toolkit.createModelRenderer(base, attributes, parser.anInt2539, 64 + ambience, 850 + contrast);
			if (replacementColours != null) {
				for (int index = 0; index < replacementColours.length; index++) {
					renderer.recolour(replacementColours[index], originalColours[index]);
				}
			}
			if (originalTextures != null) {
				for (int index = 0; originalTextures.length > index; index++) {
					renderer.retexture(originalTextures[index], replacementTextures[index]);
				}
			}
			renderer.updateFunctionMask(mask);
			synchronized (parser.cache) {
				parser.cache.put(anInt925 |= toolkit.id << 378441757, renderer, (byte) -80);
			}
		}
		ModelRenderer renderer_ = animationDefinition != null ? animationDefinition.method930(i_20_, 0, i_15_, mask, i_17_, (byte) 86, renderer, i_23_) : renderer.method2341(i_20_, mask, true);
		if ((scaleXZ ^ 0xffffffff) != -129 || scaleY != 128) {
			renderer_.scale(scaleXZ, scaleY, scaleXZ);
		}
		if (rotation != 0) {
			if ((rotation ^ 0xffffffff) == -91) {
				renderer_.rotateYaw(4096);
			}
			if (rotation == 180) {
				renderer_.rotateYaw(8192);
			}
			if (rotation == 270) {
				renderer_.rotateYaw(12288);
			}
		}
		if (bool_21_) {
			renderer_.p(aByte923, anInt918, var_s, var_s_18_, i_19_, i_16_, i_22_);
		}
		renderer_.updateFunctionMask(functionMask);
		return renderer_;
	}

	public final ModelRenderer method1728(int i, AnimationDefinitionParser class183, int i_38_, int i_39_, byte i_40_, int i_41_, RSToolkit var_ha) {
		return method1723(i_38_, i, 0, i_41_, null, null, 0, (byte) 2, class183, false, var_ha, false, 0, i_39_);
	}
}

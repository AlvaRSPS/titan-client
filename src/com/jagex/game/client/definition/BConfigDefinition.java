/* Class366 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class98_Sub31_Sub2;
import com.RSByteBuffer;
import com.jagex.core.collections.HashTable;

public final class BConfigDefinition {
	public static HashTable				aClass377_3114	= new HashTable(8);
	public static Class98_Sub31_Sub2	aClass98_Sub31_Sub2_3122;
	public static float					aFloat3120;
	public static int					anInt3111;
	public static int					anInt3112;
	public static int					anInt3117		= 0;
	public static int					anInt3121;
	public static String[]				languageCodes	= { "en", "de", "fr", "pt", "nl" };

	public int							bConfigId;

	public int							end;

	public int							start;

	public BConfigDefinition() {
		/* empty */
	}

	private final void decode(int opcode, RSByteBuffer buffer) {
        if (opcode == 1) {
            bConfigId = buffer.readShort((byte) 127);
            start = buffer.readUnsignedByte((byte) 125);
            end = buffer.readUnsignedByte((byte) -121);
        }
	}

	public final void decode(RSByteBuffer buffer, int i) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) 125);
			if ((opcode ^ 0xffffffff) == -1) {
				break;
			}
			this.decode(opcode, buffer);
		}
	}
}

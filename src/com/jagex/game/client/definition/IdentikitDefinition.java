/* Class152 - Decompiled by JODE
 */ package com.jagex.game.client.definition; /*
												*/

import com.Class105;
import com.Class185;
import com.Class98_Sub33;
import com.Class98_Sub6;
import com.BaseModel;
import com.OutgoingPacket;
import com.Player;
import com.RSByteBuffer;
import com.Sprite;
import com.jagex.game.client.definition.parser.IdentikitDefinitionParser;

public final class IdentikitDefinition {
	public static Sprite aClass332_1221;

	static {
		new Class105("", 76);
	}

	public static final String method2477(int i) {
		if (Player.menuOpen || SunDefinition.aClass98_Sub46_Sub8_1994 == null) {
			return "";
		}
		if ((SunDefinition.aClass98_Sub46_Sub8_1994.targetText == null || (SunDefinition.aClass98_Sub46_Sub8_1994.targetText.length() ^ 0xffffffff) == -1) && SunDefinition.aClass98_Sub46_Sub8_1994.aString5985 != null && SunDefinition.aClass98_Sub46_Sub8_1994.aString5985.length() > 0) {
			return SunDefinition.aClass98_Sub46_Sub8_1994.aString5985;
		}
		if (i != 29558) {
			aClass332_1221 = null;
		}
		return SunDefinition.aClass98_Sub46_Sub8_1994.targetText;
	}

	public static final void method2478(int i, int i_12_, Class185 class185, int i_13_, int i_14_, int i_15_) {
		if (i_14_ <= -1) {
			Class98_Sub33 class98_sub33 = null;
			for (Class98_Sub33 class98_sub33_16_ = (Class98_Sub33) OutgoingPacket.aClass148_3866.getFirst(32); class98_sub33_16_ != null; class98_sub33_16_ = (Class98_Sub33) OutgoingPacket.aClass148_3866.getNext(106)) {
				if (class98_sub33_16_.anInt4116 == i_15_ && i_13_ == class98_sub33_16_.localX && i == class98_sub33_16_.localZ && (class98_sub33_16_.anInt4118 ^ 0xffffffff) == (i_12_ ^ 0xffffffff)) {
					class98_sub33 = class98_sub33_16_;
					break;
				}
			}
			if (class98_sub33 == null) {
				class98_sub33 = new Class98_Sub33();
				class98_sub33.localZ = i;
				class98_sub33.anInt4118 = i_12_;
				class98_sub33.localX = i_13_;
				class98_sub33.anInt4116 = i_15_;
				OutgoingPacket.aClass148_3866.addLast(class98_sub33, -20911);
			}
			class98_sub33.aClass185_4125 = class185;
			class98_sub33.aBoolean4124 = false;
			class98_sub33.aBoolean4123 = true;
		}
	}

	public IdentikitDefinitionParser	aClass83_1220;
	private int[]						anIntArray1218;
	private int[]						anIntArray1222	= { -1, -1, -1, -1, -1 };

	private short[]						aShortArray1217;

	private short[]						aShortArray1219;

	private short[]						aShortArray1223;

	private short[]						aShortArray1224;

	public IdentikitDefinition() {
		/* empty */
	}

	public final void decode(boolean bool, RSByteBuffer buffer) {
		for (;;) {
			int i = buffer.readUnsignedByte((byte) 95);
			if ((i ^ 0xffffffff) == -1) {
				break;
			}
			this.decode(buffer, i, (byte) 115);
		}
	}

	private final void decode(RSByteBuffer buffer, int i, byte i_17_) {
		if ((i ^ 0xffffffff) != -2) {
			if ((i ^ 0xffffffff) != -3) {
				if (i != 3) {
					if ((i ^ 0xffffffff) == -41) {
						int i_19_ = buffer.readUnsignedByte((byte) 107);
						aShortArray1217 = new short[i_19_];
						aShortArray1219 = new short[i_19_];
						for (int i_20_ = 0; (i_20_ ^ 0xffffffff) > (i_19_ ^ 0xffffffff); i_20_++) {
							aShortArray1219[i_20_] = (short) buffer.readShort((byte) 127);
							aShortArray1217[i_20_] = (short) buffer.readShort((byte) 127);
						}
					} else if (i != 41) {
						if ((i ^ 0xffffffff) <= -61 && (i ^ 0xffffffff) > -71) {
							anIntArray1222[-60 + i] = buffer.readShort((byte) 127);
						}
					} else {
						int i_21_ = buffer.readUnsignedByte((byte) -123);
						aShortArray1223 = new short[i_21_];
						aShortArray1224 = new short[i_21_];
						for (int i_22_ = 0; i_21_ > i_22_; i_22_++) {
							aShortArray1224[i_22_] = (short) buffer.readShort((byte) 127);
							aShortArray1223[i_22_] = (short) buffer.readShort((byte) 127);
						}
					}
				}
			} else {
				int i_23_ = buffer.readUnsignedByte((byte) 100);
				anIntArray1218 = new int[i_23_];
				for (int i_24_ = 0; (i_24_ ^ 0xffffffff) > (i_23_ ^ 0xffffffff); i_24_++) {
					anIntArray1218[i_24_] = buffer.readShort((byte) 127);
				}
			}
		} else {
			buffer.readUnsignedByte((byte) -111);
		}
	}

	public final BaseModel method2473(int i) {
		if (anIntArray1218 == null) {
			return null;
		}
		BaseModel[] class178s = new BaseModel[anIntArray1218.length];
		synchronized (aClass83_1220.meshJs5) {
			if (i != 2) {
				aClass332_1221 = null;
			}
			for (int i_0_ = 0; anIntArray1218.length > i_0_; i_0_++) {
				class178s[i_0_] = Class98_Sub6.fromFile(0, -9252, aClass83_1220.meshJs5, anIntArray1218[i_0_]);
			}
		}
		for (int i_1_ = 0; (i_1_ ^ 0xffffffff) > (anIntArray1218.length ^ 0xffffffff); i_1_++) {
			if (class178s[i_1_].version < 13) {
				class178s[i_1_].scaleLog2(i ^ 0x35b0, 2);
			}
		}
		BaseModel class178;
		if ((class178s.length ^ 0xffffffff) != -2) {
			class178 = new BaseModel(class178s, class178s.length);
		} else {
			class178 = class178s[0];
		}
		if (class178 == null) {
			return null;
		}
		if (aShortArray1219 != null) {
			for (int i_2_ = 0; (aShortArray1219.length ^ 0xffffffff) < (i_2_ ^ 0xffffffff); i_2_++) {
				class178.replaceColour(0, aShortArray1219[i_2_], aShortArray1217[i_2_]);
			}
		}
		if (aShortArray1224 != null) {
			for (int i_3_ = 0; i_3_ < aShortArray1224.length; i_3_++) {
				class178.replaceTexture(aShortArray1223[i_3_], (byte) -114, aShortArray1224[i_3_]);
			}
		}
		return class178;
	}

	public final boolean method2474(int i) {
		boolean bool = true;
		synchronized (aClass83_1220.meshJs5) {
			for (int i_4_ = 0; (i_4_ ^ 0xffffffff) > -6; i_4_++) {
				if ((anIntArray1222[i_4_] ^ 0xffffffff) != 0 && !aClass83_1220.meshJs5.isFileCached(0, anIntArray1222[i_4_], -6329)) {
					bool = false;
				}
			}
		}
		return bool;
	}

	public final boolean method2475(int i) {
		if (anIntArray1218 == null) {
			return true;
		}
		boolean bool = true;
		synchronized (aClass83_1220.meshJs5) {
			for (int i_5_ = i; i_5_ < anIntArray1218.length; i_5_++) {
				if (!aClass83_1220.meshJs5.isFileCached(0, anIntArray1218[i_5_], i + -6329)) {
					bool = false;
				}
			}
		}
		return bool;
	}

	public final BaseModel method2476(byte i) {
		BaseModel[] class178s = new BaseModel[5];
		int i_6_ = 0;
		synchronized (aClass83_1220.meshJs5) {
			for (int i_7_ = 0; (i_7_ ^ 0xffffffff) > -6; i_7_++) {
				if ((anIntArray1222[i_7_] ^ 0xffffffff) != 0) {
					class178s[i_6_++] = Class98_Sub6.fromFile(0, -9252, aClass83_1220.meshJs5, anIntArray1222[i_7_]);
				}
			}
		}
		for (int i_8_ = 0; (i_8_ ^ 0xffffffff) > -6; i_8_++) {
			if (class178s[i_8_] != null && class178s[i_8_].version < 13) {
				class178s[i_8_].scaleLog2(13746, 2);
			}
		}
		BaseModel class178 = new BaseModel(class178s, i_6_);
		if (aShortArray1219 != null) {
			for (int i_10_ = 0; (i_10_ ^ 0xffffffff) > (aShortArray1219.length ^ 0xffffffff); i_10_++) {
				class178.replaceColour(0, aShortArray1219[i_10_], aShortArray1217[i_10_]);
			}
		}
		if (aShortArray1224 != null) {
			for (int i_11_ = 0; aShortArray1224.length > i_11_; i_11_++) {
				class178.replaceTexture(aShortArray1223[i_11_], (byte) -93, aShortArray1224[i_11_]);
			}
		}
		return class178;
	}
}

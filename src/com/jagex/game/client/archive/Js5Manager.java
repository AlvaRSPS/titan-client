
/* Class109 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import java.math.BigInteger;

import com.AsyncCache;
import com.DataFs;
import com.RSByteBuffer;

public final class Js5Manager {
	public static boolean	aBoolean933			= false;
	public static int		anInt926			= -1;
	public static boolean	shiftClickEnabled	= false;

	private Js5Archive[]	archives;
	private RSByteBuffer	masterIndexPacket;
	private AsyncCache		async_cache;
	private Js5Client		js5ActiveClient;
	private Js5FileRequest	masterIndexRequest;
	private BigInteger		privateKey;
	private BigInteger		rsaModulo;

	public Js5Manager(Js5Client js5client, AsyncCache asyncCache, BigInteger privateKey, BigInteger rsaModulus) {
		do {
			js5ActiveClient = js5client;
			async_cache = asyncCache;
			if (js5ActiveClient.moreThan20PriorityRequests()) {
				break;
			}
			masterIndexRequest = js5ActiveClient.requestFile(255, (byte) 0, 255, 105, true);
			break;
		} while (false);
	}

	private final Js5Archive createArchive(boolean bool, DataFs archiveDataFs, int archiveId, DataFs fitDataFs, int i_17_) {
		if (masterIndexPacket == null) {
			throw new RuntimeException();
		}
		if (archiveId < 0 || archives.length <= archiveId) {
			throw new RuntimeException();
		}
		if (archives[archiveId] != null) {
			return archives[archiveId];
		}
		masterIndexPacket.position = 72 * archiveId - -6;
		int fitCrc32 = masterIndexPacket.readInt(-2);
		int fitRevision = masterIndexPacket.readInt(-2);
		byte[] fitHash = new byte[64];
		masterIndexPacket.getData(fitHash, true, 64, 0);
		Js5Archive js5Archive = new Js5Archive(archiveId, archiveDataFs, fitDataFs, js5ActiveClient, async_cache, fitCrc32, fitHash, fitRevision, bool);
		archives[archiveId] = js5Archive;
		return js5Archive;
	}

	public final Js5Archive createArchive(int dummy, DataFs archiveDataFs, DataFs fitDataFs, int archiveId) {
		return createArchive(true, fitDataFs, archiveId, archiveDataFs, 96);
	}

	public final boolean isMasterIndexReady() {
		if (masterIndexPacket != null) {
			return true;
		}
		if (masterIndexRequest == null) {
			if (js5ActiveClient.moreThan20PriorityRequests()) {
				return false;
			}
			masterIndexRequest = js5ActiveClient.requestFile(255, (byte) 0, 255, 112, true);
		}
		if (masterIndexRequest.inProgress) {
			return false;
		}
		RSByteBuffer packet = new RSByteBuffer(masterIndexRequest.getData(87));
		packet.position = 5;
		int fitCount = packet.readUnsignedByte((byte) -99);
		packet.position += 72 * fitCount;
		byte[] is = new byte[packet.payload.length - packet.position];
		packet.getData(is, true, is.length, 0);
		masterIndexPacket = packet;
		archives = new Js5Archive[fitCount];
		return true;
	}

	public final void process(int i) {
		if (archives != null) {
			for (int archive = i; (archives.length ^ 0xffffffff) < (archive ^ 0xffffffff); archive++) {
				if (archives[archive] != null) {
					archives[archive].process(-1);
				}
			}
			for (Js5Archive element : archives) {
				if (element != null) {
					element.fetchGroup(7899);
				}
			}
		}
	}
}

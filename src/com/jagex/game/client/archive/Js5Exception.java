/* RuntimeException_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import com.Class140;
import com.Class157;
import com.Class159;
import com.Class162;
import com.Class234;
import com.Class284_Sub1_Sub2;
import com.Class287;
import com.Class3;
import com.Class333;
import com.Class346;
import com.Class46;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub38;
import com.Class98_Sub10_Sub6;
import com.Class98_Sub12;
import com.Class98_Sub28_Sub1;
import com.OpenGlPointLight;
import com.Class98_Sub46_Sub4;
import com.Class98_Sub46_Sub6;
import com.GZipDecompressor;
import com.GameShell;
import com.ProceduralTextureSource;
import com.ProxyException;
import com.RtInterfaceClip;
import com.SceneGraphNodeList;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.StructsDefinition;
import com.jagex.game.toolkit.matrix.Matrix;

@SuppressWarnings("serial")
public final class Js5Exception extends RuntimeException {
	public static Matrix				aClass111_3203;
	public static AdvancedMemoryCache	aClass79_3204	= new AdvancedMemoryCache(64);
	public static int					anInt3201;
	public static int					anInt3205		= -1;
	public static int[]					anIntArray3200	= new int[1];

	static {

	}

	public static final Js5Exception createJs5Exception(Throwable throwable, String string) {
		Js5Exception exception;
		if (!(throwable instanceof Js5Exception)) {
			exception = new Js5Exception(throwable, string);
		} else {
			exception = (Js5Exception) throwable;
			exception.jsMessage += ' ' + string;
		}
		return exception;
	}

	public static final void method4010(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		if ((SceneGraphNodeList.anInt1635 ^ 0xffffffff) <= (i_2_ ^ 0xffffffff) && i_0_ >= Class98_Sub10_Sub38.anInt5753) {
			boolean bool;
			if ((Class76_Sub8.anInt3778 ^ 0xffffffff) < (i_1_ ^ 0xffffffff)) {
				bool = false;
				i_1_ = Class76_Sub8.anInt3778;
			} else if (Class3.anInt77 >= i_1_) {
				bool = true;
			} else {
				i_1_ = Class3.anInt77;
				bool = false;
			}
			boolean bool_5_;
			if (i_3_ < Class76_Sub8.anInt3778) {
				bool_5_ = false;
				i_3_ = Class76_Sub8.anInt3778;
			} else if (i_3_ <= Class3.anInt77) {
				bool_5_ = true;
			} else {
				i_3_ = Class3.anInt77;
				bool_5_ = false;
			}
			if (i_2_ < Class98_Sub10_Sub38.anInt5753) {
				i_2_ = Class98_Sub10_Sub38.anInt5753;
			} else {
				Class333.method3761(i_4_, AnimationDefinition.anIntArrayArray814[i_2_++], i_1_, i_3_, (byte) 113);
			}
			if ((i_0_ ^ 0xffffffff) < (SceneGraphNodeList.anInt1635 ^ 0xffffffff)) {
				i_0_ = SceneGraphNodeList.anInt1635;
			} else {
				Class333.method3761(i_4_, AnimationDefinition.anIntArrayArray814[i_0_--], i_1_, i_3_, (byte) 26);
			}
			if (bool && bool_5_) {
				for (int i_6_ = i_2_; i_0_ >= i_6_; i_6_++) {
					int[] is = AnimationDefinition.anIntArrayArray814[i_6_];
					is[i_1_] = is[i_3_] = i_4_;
				}
			} else if (!bool) {
				if (bool_5_) {
					for (int i_7_ = i_2_; i_7_ <= i_0_; i_7_++) {
						AnimationDefinition.anIntArrayArray814[i_7_][i_3_] = i_4_;
					}
				}
			} else {
				for (int i_8_ = i_2_; i_8_ <= i_0_; i_8_++) {
					AnimationDefinition.anIntArrayArray814[i_8_][i_1_] = i_4_;
				}
			}
		}
	}

	public static final void method4011(int i) {
		if ((Class98_Sub10_Sub6.anInt5570 ^ 0xffffffff) <= -1) {
			long l = TimeTools.getCurrentTime(-47);
			Class98_Sub10_Sub6.anInt5570 -= -StructsDefinition.aLong6035 + l;
			do {
				if (Class98_Sub10_Sub6.anInt5570 <= 0) {
					Class98_Sub46_Sub6.anInt5979 = RtInterfaceClip.aClass28_50.anInt290;
					Class159.aFloat1254 = RtInterfaceClip.aClass28_50.aFloat281;
					Queue.aFloat1613 = RtInterfaceClip.aClass28_50.aFloat289;
					GZipDecompressor.anInt1965 = RtInterfaceClip.aClass28_50.anInt283;
					NPCDefinition.aFloat1150 = RtInterfaceClip.aClass28_50.aFloat288;
					Class98_Sub10_Sub6.anInt5570 = -1;
					Class284_Sub1_Sub2.layerColour = RtInterfaceClip.aClass28_50.anInt285;
					Class46.aFloat388 = RtInterfaceClip.aClass28_50.aFloat293;
					Class98_Sub46_Sub4.aClass48_5962 = RtInterfaceClip.aClass28_50.aClass48_287;
					ProceduralTextureSource.aFloat3260 = RtInterfaceClip.aClass28_50.aFloat295;
					AnimationDefinition.aFloat831 = RtInterfaceClip.aClass28_50.aFloat291;

					if (!GameShell.cleanedStatics) {
						break;
					}

				}
				int i_9_ = (Class98_Sub10_Sub6.anInt5570 << -869608184) / Class287.anInt2196;
				int i_10_ = -i_9_ + 255;
				float f = i_9_ / 255.0F;
				float f_11_ = 1.0F - f;
				Class98_Sub46_Sub6.anInt5979 = (0xff0000 & i_9_ * (Class98_Sub28_Sub1.anInt5811 & 0xff00) - -(i_10_ * (RtInterfaceClip.aClass28_50.anInt290 & 0xff00))) + (i_10_ * (0xff00ff & RtInterfaceClip.aClass28_50.anInt290) + (Class98_Sub28_Sub1.anInt5811 & 0xff00ff) * i_9_
						& ~0xff00ff) >>> -1850804568;
				Class46.aFloat388 = f_11_ * (-ProxyException.aFloat31 + RtInterfaceClip.aClass28_50.aFloat293) + ProxyException.aFloat31;
				Queue.aFloat1613 = Class3.aFloat78 + f_11_ * (RtInterfaceClip.aClass28_50.aFloat289 - Class3.aFloat78);
				AnimationDefinition.aFloat831 = Class157.aFloat1249 + f_11_ * (-Class157.aFloat1249 + RtInterfaceClip.aClass28_50.aFloat291);
				ProceduralTextureSource.aFloat3260 = (RtInterfaceClip.aClass28_50.aFloat295 - Class346.aFloat2900) * f_11_ + Class346.aFloat2900;
				Class159.aFloat1254 = (-Js5Client.aFloat1053 + RtInterfaceClip.aClass28_50.aFloat281) * f_11_ + Js5Client.aFloat1053;
				Class284_Sub1_Sub2.layerColour = (i_9_ * (Class162.anInt1271 & 0xff00) - -(i_10_ * (0xff00 & RtInterfaceClip.aClass28_50.anInt285)) & 0xff0000) + (~0xff00ff & (Class162.anInt1271 & 0xff00ff) * i_9_ + (RtInterfaceClip.aClass28_50.anInt285 & 0xff00ff) * i_10_) >>> 1466852232;
				GZipDecompressor.anInt1965 = i_9_ * Class98_Sub12.anInt3872 + RtInterfaceClip.aClass28_50.anInt283 * i_10_ >> 40165512;
				NPCDefinition.aFloat1150 = Class234.aFloat1749 + (RtInterfaceClip.aClass28_50.aFloat288 - Class234.aFloat1749) * f_11_;
				if (RtInterfaceClip.aClass28_50.aClass48_287 != Class140.aClass48_3245) {
					Class98_Sub46_Sub4.aClass48_5962 = OpenGlPointLight.aHa4185.method1769(Class140.aClass48_3245, RtInterfaceClip.aClass28_50.aClass48_287, f_11_, Class98_Sub46_Sub4.aClass48_5962);
					// System.out.println("A: " + Class284_Sub1_Sub2.layerColour
					// + " B: " + Class46.aFloat388 + " C: " +
					// NPCDefinition.aFloat1150 + " D: " +
					// GZipDecompressor.anInt1965);
					// System.out.println(" a: " + Class98_Sub10_Sub6.anInt5570
					// + " b: " + Class98_Sub46_Sub6.anInt5979 + " c: " +
					// GZipDecompressor.anInt1965 + " d: " +
					// Class284_Sub1_Sub2.layerColour);
				}

			} while (false);
			StructsDefinition.aLong6035 = l;
		}
	}

	public Throwable	cause;

	public String		jsMessage;

	Js5Exception(Throwable throwable, String string) {
		cause = throwable;
		jsMessage = string;
	}
}

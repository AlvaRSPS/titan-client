/* Class339_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import com.*;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Node;
import com.jagex.core.timetools.general.TimeTools;

public final class Js5Archive extends Archive {
	public static RtAwtFontWrapper	aClass326_5308;
	public static RtAwtFontWrapper	aClass326_5315;
	public static float				aFloat5316;

	public static final void method3799(int i) {
		Class39_Sub1.anInt3594++;
		OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i + 260, Class98_Sub23.aClass171_3998, Class331.aClass117_2811);
		class98_sub11.packet.writeByte(i, 121);
		Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
	}

	private boolean		aBoolean5324;
	private boolean		aBoolean5326;
	private boolean		aBoolean5328;
	private byte[]		groupStatus;
	private byte[]		indexWhirlpool;
	private Js5Client	js5Client;
	private LinkedList	aClass148_5322;
	private LinkedList	aClass148_5325;
	private DataFs		archiveDataFs;
	private DataFs		indexDataFs;
	private AsyncCache	aClass253_5312;
	private Js5Index	index;
	private HashTable	requestMap;
	private FileRequest	indexRequest;
	private long		aLong5327;
	private int			indexCrc32;
	private int			archiveId;
	private int			anInt5311	= 0;

	private int			indexVersion;

	private int			anInt5323;

	public Js5Archive(int i, DataFs class17, DataFs class17_17_, Js5Client class135, AsyncCache class253, int indexCrc32, byte[] is, int i_19_, boolean bool) {
		requestMap = new HashTable(16);
		anInt5323 = 0;
		aClass148_5325 = new LinkedList();
		aLong5327 = 0L;
		do {
			archiveDataFs = class17;
			archiveId = i;
			do {
				if (archiveDataFs != null) {
					aBoolean5324 = true;
					aClass148_5322 = new LinkedList();
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				aBoolean5324 = false;
			} while (false);
			js5Client = class135;
			indexDataFs = class17_17_;
			aBoolean5328 = bool;
			this.indexCrc32 = indexCrc32;
			aClass253_5312 = class253;
			indexVersion = i_19_;
			indexWhirlpool = is;
			if (indexDataFs == null) {
				break;
			}
			indexRequest = aClass253_5312.get(1, indexDataFs, archiveId);
			break;
		} while (false);
	}

	public final void fetchGroup(int i) {
		do {
			if (i == 7899) {
				if (aClass148_5322 != null) {
					if (getIndex((byte) -128) == null) {
						break;
					}
					if (aBoolean5324) {
						boolean bool = true;
						for (Node class98 = aClass148_5322.getFirst(32); class98 != null; class98 = aClass148_5322.getNext(123)) {
							int i_14_ = (int) class98.hash;
							if ((groupStatus[i_14_] ^ 0xffffffff) == -1) {
								fetchGroupInner(-2, i_14_, 1);
							}
							if ((groupStatus[i_14_] ^ 0xffffffff) != -1) {
								class98.unlink(90);
							} else {
								bool = false;
							}
						}
						while ((index.groupEntryCount.length ^ 0xffffffff) < (anInt5323 ^ 0xffffffff)) {
							if ((index.groupEntryCount[anInt5323] ^ 0xffffffff) == -1) {
								anInt5323++;
							} else {
								if ((aClass253_5312.requestCount ^ 0xffffffff) <= -251) {
									bool = false;
									break;
								}
								if ((groupStatus[anInt5323] ^ 0xffffffff) == -1) {
									fetchGroupInner(-2, anInt5323, 1);
								}
								if ((groupStatus[anInt5323] ^ 0xffffffff) == -1) {
									Node class98 = new Node();
									class98.hash = anInt5323;
									bool = false;
									aClass148_5322.addLast(class98, -20911);
								}
								anInt5323++;
							}
						}
						if (bool) {
							anInt5323 = 0;
							aBoolean5324 = false;
						}
					} else if (!aBoolean5326) {
						aClass148_5322 = null;
					} else {
						boolean bool = true;
						for (Node class98 = aClass148_5322.getFirst(32); class98 != null; class98 = aClass148_5322.getNext(112)) {
							int i_15_ = (int) class98.hash;
							if ((groupStatus[i_15_] ^ 0xffffffff) != -2) {
								fetchGroupInner(-2, i_15_, 2);
							}
							if (groupStatus[i_15_] == 1) {
								class98.unlink(i ^ 0x1e9f);
							} else {
								bool = false;
							}
						}
						while (anInt5323 < index.groupEntryCount.length) {
							if ((index.groupEntryCount[anInt5323] ^ 0xffffffff) == -1) {
								anInt5323++;
							} else {
								if (js5Client.moreThan20NormalRequests(i ^ 0x1ecf)) {
									bool = false;
									break;
								}
								if (groupStatus[anInt5323] != 1) {
									fetchGroupInner(-2, anInt5323, 2);
								}
								if (groupStatus[anInt5323] != 1) {
									Node class98 = new Node();
									class98.hash = anInt5323;
									aClass148_5322.addLast(class98, -20911);
									bool = false;
								}
								anInt5323++;
							}
						}
						if (bool) {
							anInt5323 = 0;
							aBoolean5326 = false;
						}
					}
				}
				if (!aBoolean5328 || (aLong5327 ^ 0xffffffffffffffffL) < (TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL)) {
					break;
				}
				for (FileRequest class98_sub46_sub13 = (FileRequest) requestMap.startIteration(122); class98_sub46_sub13 != null; class98_sub46_sub13 = (FileRequest) requestMap.iterateNext(-1)) {
					if (!class98_sub46_sub13.inProgress) {
						if (!class98_sub46_sub13.aBoolean6036) {
							class98_sub46_sub13.aBoolean6036 = true;
						} else {
							if (!class98_sub46_sub13.aBoolean6037) {
								throw new RuntimeException();
							}
							class98_sub46_sub13.unlink(51);
						}
					}
				}
				aLong5327 = TimeTools.getCurrentTime(-47) + 1000L;
			}
			break;
		} while (false);
	}

	private final FileRequest fetchGroupInner(int i, int i_0_, int i_1_) {
		FileRequest request = (FileRequest) requestMap.get(i_0_, i ^ 0x1);
		if (request != null && (i_1_ ^ 0xffffffff) == -1 && !request.aBoolean6037 && request.inProgress) {
			request.unlink(71);
			request = null;
		}
		if (request == null) {
			if ((i_1_ ^ 0xffffffff) == -1) {
				if (archiveDataFs != null && groupStatus[i_0_] != -1) {
					request = aClass253_5312.get(1, archiveDataFs, i_0_);
				} else {
					if (js5Client.moreThan20PriorityRequests()) {
						return null;
					}
					request = js5Client.requestFile(archiveId, (byte) 2, i_0_, 119, true);
				}
			} else if (i_1_ != 1) {
				if (i_1_ != 2) {
					throw new RuntimeException();
				}
				if (archiveDataFs == null) {
					throw new RuntimeException();
				}
				if (groupStatus[i_0_] != -1) {
					throw new RuntimeException();
				}
				if (js5Client.moreThan20NormalRequests(20)) {
					return null;
				}
				request = js5Client.requestFile(archiveId, (byte) 2, i_0_, 108, false);
			} else {
				if (archiveDataFs == null) {
					throw new RuntimeException();
				}
				request = aClass253_5312.method3176((byte) -38, i_0_, archiveDataFs);
			}
			requestMap.put(request, i_0_, -1);
		}
		if (request.inProgress) {
			return null;
		}
		byte[] is = request.getData(90);
		if (i != -2) {
			getIndex((byte) -125);
		}
		if (request instanceof CacheFileRequest) {
			try {
				if (is == null || (is.length ^ 0xffffffff) >= -3) {
					throw new RuntimeException();
				}
				GameDefinition.aCRC32_2097.reset();
				GameDefinition.aCRC32_2097.update(is, 0, -2 + is.length);
				int i_2_ = (int) GameDefinition.aCRC32_2097.getValue();
				if ((i_2_ ^ 0xffffffff) != (index.groupCrc32[i_0_] ^ 0xffffffff)) {
					throw new RuntimeException();
				}
				if (index.groupWhirlpool != null && index.groupWhirlpool[i_0_] != null) {
					byte[] is_3_ = index.groupWhirlpool[i_0_];
					byte[] is_4_ = WhirlpoolGenerator.whirlpool(0, (byte) -122, is, -2 + is.length);
					for (int i_5_ = 0; i_5_ < 64; i_5_++) {
						if (is_4_[i_5_] != is_3_[i_5_]) {
							throw new RuntimeException();
						}
					}
				}
				int i_6_ = ((0xff & is[-2 + is.length]) << 1865901960) - -(0xff & is[is.length - 1]);
				if ((i_6_ ^ 0xffffffff) != (0xffff & index.groupRevision[i_0_] ^ 0xffffffff)) {
					throw new RuntimeException();
				}
				if ((groupStatus[i_0_] ^ 0xffffffff) != -2) {
					anInt5311++;
					groupStatus[i_0_] = (byte) 1;
				}
				if (!request.aBoolean6037) {
					request.unlink(i + 74);
				}
				return request;
			} catch (Exception exception) {
				groupStatus[i_0_] = (byte) -1;
				request.unlink(52);
				if (request.aBoolean6037 && !js5Client.moreThan20PriorityRequests()) {
					Js5FileRequest class98_sub46_sub13_sub1 = js5Client.requestFile(archiveId, (byte) 2, i_0_, 122, true);
					requestMap.put(class98_sub46_sub13_sub1, i_0_, -1);
				}
				return null;
			}
		}
		try {
			if (is == null || (is.length ^ 0xffffffff) >= -3) {
				throw new RuntimeException();
			}
			GameDefinition.aCRC32_2097.reset();
			GameDefinition.aCRC32_2097.update(is, 0, is.length + -2);
			int crcCalculated = (int) GameDefinition.aCRC32_2097.getValue();
			if ((index.groupCrc32[i_0_] ^ 0xffffffff) != (crcCalculated ^ 0xffffffff)) {
				throw new RuntimeException();
			}
			if (index.groupWhirlpool != null && index.groupWhirlpool[i_0_] != null) {
				byte[] is_8_ = index.groupWhirlpool[i_0_];
				byte[] is_9_ = WhirlpoolGenerator.whirlpool(0, (byte) -124, is, -2 + is.length);
				for (int i_10_ = 0; (i_10_ ^ 0xffffffff) > -65; i_10_++) {
					if ((is_8_[i_10_] ^ 0xffffffff) != (is_9_[i_10_] ^ 0xffffffff)) {
						throw new RuntimeException();
					}
				}
			}
			js5Client.reconnectCount = 0;
			js5Client.status = 0;
		} catch (RuntimeException runtimeException) {
			js5Client.enableEncryption(67);
			request.unlink(76);
			if (request.aBoolean6037 && !js5Client.moreThan20PriorityRequests()) {
				Js5FileRequest class98_sub46_sub13_sub1 = js5Client.requestFile(archiveId, (byte) 2, i_0_, 127, true);
				requestMap.put(class98_sub46_sub13_sub1, i_0_, i ^ 0x1);
			}
			return null;
		}
		is[is.length - 2] = (byte) (index.groupRevision[i_0_] >>> 969573768);
		is[-1 + is.length] = (byte) index.groupRevision[i_0_];
		if (archiveDataFs != null) {
			aClass253_5312.put(is, (byte) 61, archiveDataFs, i_0_);
			if (groupStatus[i_0_] != 1) {
				anInt5311++;
				groupStatus[i_0_] = (byte) 1;
			}
		}
		if (!request.aBoolean6037) {
			request.unlink(102);
		}
		return request;
	}

	@Override
	public final byte[] getGroupData(int groupData, int i_11_) {
		FileRequest fileRequest = fetchGroupInner(i_11_ + -2, groupData, i_11_);
		if (fileRequest == null) {
			return null;
		}
		byte[] data = fileRequest.getData(i_11_ + 113);
		fileRequest.unlink(52);
		return data;
	}

	@Override
	public final int getGroupProgress(int i, byte i_12_) {
		FileRequest fileRequest = (FileRequest) requestMap.get(i, -1);
		if (fileRequest != null) {
			return fileRequest.getProgress(100);
		}
		return 0;
	}

	@Override
	public final Js5Index getIndex(byte i) {
		if (index != null) {
			return index;
		}
		if (indexRequest == null) {
			if (js5Client.moreThan20PriorityRequests()) {
				return null;
			}
			indexRequest = js5Client.requestFile(255, (byte) 0, archiveId, 126, true);
		}
		if (indexRequest.inProgress) {
			return null;
		}
		byte[] indexData = indexRequest.getData(25);
		if (!(indexRequest instanceof CacheFileRequest)) {
			try {
				if (indexData == null) {
					throw new RuntimeException("Failed to download index from server!");
				}
				index = new Js5Index(indexData, indexCrc32, indexWhirlpool);
			} catch (RuntimeException runtimeException) {
				js5Client.enableEncryption(89);
				index = null;
				if (js5Client.moreThan20PriorityRequests()) {
					indexRequest = null;
				} else {
					indexRequest = js5Client.requestFile(255, (byte) 0, archiveId, 111, true);
				}
				return null;
			}
			if (indexDataFs != null) {
				aClass253_5312.put(indexData, (byte) 21, indexDataFs, archiveId);
			}
		} else {
			try {
				if (indexData == null) {
					throw new RuntimeException("Index not found in disk cache");
				}
				index = new Js5Index(indexData, indexCrc32, indexWhirlpool);
				if (indexVersion != index.indexVersion) {
					throw new RuntimeException("Index version wrong - index.indexversion:" + index.indexVersion + " expected:" + indexVersion);
				}
			} catch (RuntimeException runtimeException) {
				index = null;
				if (js5Client.moreThan20PriorityRequests()) {
					indexRequest = null;
				} else {
					indexRequest = js5Client.requestFile(255, (byte) 0, archiveId, 117, true);
				}
				return null;
			}
		}
		indexRequest = null;
		if (archiveDataFs != null) {
			groupStatus = new byte[index.groupCount];
			anInt5311 = 0;
		}
		return index;
	}

	public final int getInitializationProgress(int i) {
		if (getIndex((byte) -116) == null) {
			if (indexRequest == null) {
				return 0;
			}
			return indexRequest.getProgress(100);
		}
		return 100;
	}

	public final int method3791(byte i) {
		if (index == null) {
			return 0;
		}
		if (!aBoolean5324) {
			return index.fitEntryCount;
		}
		Node class98 = aClass148_5322.getFirst(32);
		if (class98 == null) {
			return 0;
		}
		return (int) class98.hash;
	}

	public final void method3793(int i) {
		do {
			if (archiveDataFs != null) {
				aBoolean5326 = true;
				if (aClass148_5322 != null) {
					break;
				}
				aClass148_5322 = new LinkedList();
			}
		} while (false);
	}

	public final int method3798(int i) {
		return anInt5311;
	}

	public final int method3800(int i) {
		if (index == null) {
			return 0;
		}
		return index.fitEntryCount;
	}

	public final void process(int i) {
		if (aClass148_5322 != null && getIndex((byte) -110) != null) {
			for (Node class98 = aClass148_5325.getFirst(i + 33); class98 != null; class98 = aClass148_5325.getNext(117)) {
				int i_16_ = (int) class98.hash;
				if ((i_16_ ^ 0xffffffff) > -1 || index.groupCount <= i_16_ || index.groupEntryCount[i_16_] == 0) {
					class98.unlink(84);
				} else {
					if ((groupStatus[i_16_] ^ 0xffffffff) == -1) {
						fetchGroupInner(i + -1, i_16_, 1);
					}
					if (groupStatus[i_16_] == -1) {
						fetchGroupInner(i ^ 0x1, i_16_, 2);
					}
					if ((groupStatus[i_16_] ^ 0xffffffff) == -2) {
						class98.unlink(113);
					}
				}
			}
		}
	}

	@Override
	public final void requestGroup(int i, boolean bool) {
		if (archiveDataFs != null) {
			for (Node class98 = aClass148_5325.getFirst(32); class98 != null; class98 = aClass148_5325.getNext(97)) {
				if (i == class98.hash) {
					return;
				}
			}
			Node class98 = new Node();
			class98.hash = i;
			aClass148_5325.addLast(class98, -20911);
		}
	}
}

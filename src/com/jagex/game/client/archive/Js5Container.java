package com.jagex.game.client.archive;

import com.Class330;
import com.Class333;
import com.GZipDecompressor;
import com.RSByteBuffer;

public class Js5Container {

	public static GZipDecompressor gzipDecompressor = new GZipDecompressor();

	public static final byte[] getPayload(byte[] containerData, byte i) {
		RSByteBuffer packet = new RSByteBuffer(containerData);
		int compression = packet.readUnsignedByte((byte) -103);
		int packedLength = packet.readInt(-2);
		if (packedLength < 0 || Class333.anInt3390 != 0 && packedLength > Class333.anInt3390) {
			throw new RuntimeException();
		}
		if (compression == 0) {
			byte[] data = new byte[packedLength];
			packet.getData(data, true, packedLength, 0);
			return data;
		} else {
			int unpackedLength = packet.readInt(-2);
			/*
			 * if ((packedLength > 1000000) || (packedLength < 0)) { return new
			 * byte[100]; }
			 */
			if ((unpackedLength ^ 0xffffffff) > -1 || (Class333.anInt3390 ^ 0xffffffff) != -1 && unpackedLength > Class333.anInt3390) {
				throw new RuntimeException();
			}
			byte[] unpackedData = new byte[unpackedLength];
			if (compression == 1) {
				Class330.decompressBz2(unpackedData, unpackedLength, containerData, packedLength, 9);
			} else {
				synchronized (Js5Container.gzipDecompressor) {
					Js5Container.gzipDecompressor.decompress(unpackedData, packet, 18762);
				}
			}
			return unpackedData;
		}
	}
}

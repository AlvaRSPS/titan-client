
/* Class135 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import com.*;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.preferences.SafeModePreferenceField;
import com.jagex.game.toolkit.font.Font;

import java.io.IOException;

public final class Js5Client {
	public static boolean				aBoolean1052	= true;
	public static float					aFloat1053;
	public static int					anInt1051		= 0;
	public static int[]					anIntArray1056	= new int[13];
	public static short					clientZoom		= 320;
	public static AdvancedMemoryCache	titleCache		= new AdvancedMemoryCache(128, 4);

	public static final int[][] method2258(int i, int i_18_, boolean bool, int i_19_, float f, int i_20_, int i_21_, byte i_22_, int i_23_) {
		int[][] is = new int[i_18_][i_19_];
		Class98_Sub10_Sub35 class98_sub10_sub35 = new Class98_Sub10_Sub35();
		class98_sub10_sub35.anInt5734 = i_23_;
		class98_sub10_sub35.anInt5737 = i_21_;
		class98_sub10_sub35.anInt5733 = i;
		class98_sub10_sub35.aBoolean5731 = bool;
		class98_sub10_sub35.anInt5739 = (int) (4096.0F * f);
		class98_sub10_sub35.method1001((byte) 66);
		SafeModePreferenceField.method559(true, i_19_, i_18_);
		for (int i_24_ = 0; i_18_ > i_24_; i_24_++) {
			class98_sub10_sub35.method1107((byte) -127, is[i_24_], i_24_);
		}
		if (i_22_ != -63) {
			anIntArray1056 = null;
		}
		return is;
	}

	public static final void method2264(byte i) {
		if (i > -77) {
			aFloat1053 = -0.715336F;
		}
		int i_25_ = client.preferences.removeRoofsMode.getValue((byte) 125);
		if ((i_25_ ^ 0xffffffff) == -1) {
			OutputStream_Sub2.aByteArrayArrayArray41 = null;
			Class36.method341(0, -1003);
		} else if ((i_25_ ^ 0xffffffff) == -2) {
			FloorOverlayDefinition.method2685(-15, (byte) 0);
			Class36.method341(512, -1003);
			if (Class281.flags != null) {
				Class254.method3186(59);
			}
		} else {
			FloorOverlayDefinition.method2685(-15, (byte) (0xff & -4 + RemoveRoofsPreferenceField.anInt3676));
			Class36.method341(2, -1003);
		}
		SystemInformation.anInt1170 = Font.localPlane;
	}

	private ClientStream	clientStream;
	private Js5FileRequest	currentRequest;
	private RSByteBuffer	inputPacket		= new RSByteBuffer(8);
	private long			lastTime;
	private int				latency;
	private Queue			normalRequest	= new Queue();
	private Queue			normalSent		= new Queue();
	private RSByteBuffer	outputPacket	= new RSByteBuffer(4);
	private Queue			priorityRequest	= new Queue();
	private Queue			prioritySent	= new Queue();

	public volatile int		reconnectCount	= 0;

	public volatile int		status			= 0;

	private byte			xorCode			= 0;

	public Js5Client() {
		/* empty */
	}

	public final void connect(ClientStream stream, boolean loggedIn, byte i) {
		if (clientStream != null) {
			try {
				clientStream.shutDown(i ^ ~0x7416);
			} catch (Exception exception) {
				/* empty */
			}
			clientStream = null;
		}
		clientStream = stream;
		sendConnected((byte) 63);
		notifyLogin(2, loggedIn);
		inputPacket.position = 0;
		currentRequest = null;
		for (;;) {
			Js5FileRequest request = (Js5FileRequest) prioritySent.remove(-16711936);
			if (request == null) {
				break;
			}
			priorityRequest.insert(request, -29);
		}
		for (;;) {
			Js5FileRequest request = (Js5FileRequest) normalSent.remove(-16711936);
			if (request == null) {
				break;
			}
			normalRequest.insert(request, -82);
		}
		if ((xorCode ^ 0xffffffff) != -1) {
			try {
				outputPacket.position = 0;
				outputPacket.writeByte(4, 67);
				outputPacket.writeByte(xorCode, -41);
				outputPacket.writeShort(0, 1571862888);
				clientStream.write((byte) 77, 0, 4, outputPacket.payload);
			} catch (IOException ioexception) {
				try {
					clientStream.shutDown(-29789);
				} catch (Exception exception) {
					/* empty */
				}
				clientStream = null;
				reconnectCount++;
				status = -2;
			}
		}
		latency = 0;
		lastTime = TimeTools.getCurrentTime(-47);
	}

	public final void enableEncryption(int i) {
		try {
			clientStream.shutDown(-29789);
		} catch (Exception exception) {
			/* empty */
		}
		reconnectCount++;
		clientStream = null;
		status = -1;
		xorCode = (byte) (int) (1.0 + Math.random() * 255.0);
	}

	private final int getNormalRequestCount(byte i) {
		return normalRequest.size(-126) + normalSent.size(-109);
	}

	public final int getPriorityRequestCount(int i) {
		if (i != -1) {
			setDummyStreamMode(15);
		}
		return priorityRequest.size(-118) + prioritySent.size(-121);
	}

	public final boolean moreThan20NormalRequests(int i) {
		return getNormalRequestCount((byte) 53) >= 20;
	}

	public final boolean moreThan20PriorityRequests() {
		return getPriorityRequestCount(-1) >= 20;
	}

	public final void notifyLogin(int i, boolean login) {
		if (clientStream != null) {
			try {
				outputPacket.position = 0;
				outputPacket.writeByte(!login ? 3 : 2, 121);
				outputPacket.method1225(-24472, 0);
				clientStream.write((byte) 77, 0, 4, outputPacket.payload);
			} catch (IOException ioexception) {
				try {
					clientStream.shutDown(-29789);
				} catch (Exception exception) {
					/* empty */
				}
				status = -2;
				reconnectCount++;
				clientStream = null;
			}
		}
	}

	public final boolean process(int i) {
		if (clientStream != null) {
			long currentTime = TimeTools.getCurrentTime(-47);
			int deltaTime = (int) (currentTime - lastTime);
			lastTime = currentTime;
			if (deltaTime > 200) {
				deltaTime = 200;
			}
			latency += deltaTime;
			if (latency > 30000) {
				try {
					clientStream.shutDown(-29789);
				} catch (Exception exception) {
					/* empty */
				}
				clientStream = null;
			}
		}
		if (clientStream == null) {
			return getPriorityRequestCount(-1) == 0 && getNormalRequestCount((byte) 49) == 0;
		}
		try {
			clientStream.handleError(true);
			for (Js5FileRequest request = (Js5FileRequest) priorityRequest.getFirst(-1); request != null; request = (Js5FileRequest) priorityRequest.getNext(0)) {
				outputPacket.position = 0;
				outputPacket.writeByte(1, i ^ 0x104c);
				outputPacket.method1225(-24472, (int) request.cachedKey);
				clientStream.write((byte) 77, 0, 4, outputPacket.payload);
				prioritySent.insert(request, -84);
			}
			for (Js5FileRequest request = (Js5FileRequest) normalRequest.getFirst(-1); request != null; request = (Js5FileRequest) normalRequest.getNext(i ^ 0x1000)) {
				outputPacket.position = 0;
				outputPacket.writeByte(0, i ^ 0x102a);
				outputPacket.method1225(i + -28568, (int) request.cachedKey);
				clientStream.write((byte) 77, 0, 4, outputPacket.payload);
				normalSent.insert(request, -77);
			}
			for (int blockCtr = 0; (blockCtr ^ 0xffffffff) > -101; blockCtr++) {
				int available = clientStream.available(75);
				if (available < 0) {
					throw new IOException();
				}
				if (available == 0) {
					break;
				}
				latency = 0;
				int requiredBytes = 0;
				if (currentRequest != null) {
					if (currentRequest.blockPosition == 0) {
						requiredBytes = 1;
					}
				} else {
					requiredBytes = 8;
				}
				if ((requiredBytes ^ 0xffffffff) >= -1) {
					int fileLength = currentRequest.packet.payload.length + -currentRequest.extraSize;
					int readLength = -currentRequest.blockPosition + 512;
					if (-currentRequest.packet.position + fileLength < readLength) {
						readLength = fileLength + -currentRequest.packet.position;
					}
					if (readLength > available) {
						readLength = available;
					}
					clientStream.read(currentRequest.packet.position, true, readLength, currentRequest.packet.payload);
					if (xorCode != 0) {
						for (int decryptPosition = 0; readLength > decryptPosition; decryptPosition++) {
							currentRequest.packet.payload[decryptPosition + currentRequest.packet.position] = (byte) Class369.method3953(currentRequest.packet.payload[decryptPosition + currentRequest.packet.position], xorCode);
						}
					}
					currentRequest.packet.position += readLength;
					currentRequest.blockPosition += readLength;
					if ((fileLength ^ 0xffffffff) != (currentRequest.packet.position ^ 0xffffffff)) {
						if (currentRequest.blockPosition == 512) {
							currentRequest.blockPosition = 0;
						}
					} else {
						currentRequest.uncache((byte) -90);
						currentRequest.inProgress = false;
						currentRequest = null;
					}
				} else {
					int readLength = requiredBytes + -inputPacket.position;
					if ((available ^ 0xffffffff) > (readLength ^ 0xffffffff)) {
						readLength = available;
					}
					clientStream.read(inputPacket.position, true, readLength, inputPacket.payload);
					if ((xorCode ^ 0xffffffff) != -1) {
						for (int decryptPosition = 0; (decryptPosition ^ 0xffffffff) > (readLength ^ 0xffffffff); decryptPosition++) {
							inputPacket.payload[decryptPosition + inputPacket.position] = (byte) Class369.method3953(inputPacket.payload[decryptPosition + inputPacket.position], xorCode);
						}
					}
					inputPacket.position += readLength;
					if ((requiredBytes ^ 0xffffffff) >= (inputPacket.position ^ 0xffffffff)) {
						if (currentRequest == null) {
							inputPacket.position = 0;
							int receivedIdx = inputPacket.readUnsignedByte((byte) 29);
							int receivedFileId = inputPacket.readShort((byte) 127);
							int receivedAttributes = inputPacket.readUnsignedByte((byte) -110);
							int dataLength = inputPacket.readInt(i + -4098);
							int compressionType = receivedAttributes & 0x7f;
							boolean bool = (0x80 & receivedAttributes) != 0;
							long fileId = (receivedIdx << 1231200656) - -receivedFileId;
							Js5FileRequest request;
							if (bool) {
								for (request = (Js5FileRequest) normalSent.getFirst(i ^ ~0x1000); request != null; request = (Js5FileRequest) normalSent.getNext(0)) {
									if ((fileId ^ 0xffffffffffffffffL) == (request.cachedKey ^ 0xffffffffffffffffL)) {
										break;
									}
								}
							} else {
								for (request = (Js5FileRequest) prioritySent.getFirst(-1); request != null; request = (Js5FileRequest) prioritySent.getNext(0)) {
									if ((request.cachedKey ^ 0xffffffffffffffffL) == (fileId ^ 0xffffffffffffffffL)) {
										break;
									}
								}
							}
							if (request == null) {
								throw new IOException("No request was sent for file (" + receivedIdx + ", " + receivedFileId + ")");
							}
							int headerSize = (compressionType ^ 0xffffffff) != -1 ? 9 : 5;
							currentRequest = request;
							currentRequest.packet = new RSByteBuffer(currentRequest.extraSize + (dataLength - -headerSize));
							currentRequest.packet.writeByte(compressionType, 107);
							currentRequest.packet.writeInt(1571862888, dataLength);
							inputPacket.position = 0;
							currentRequest.blockPosition = 8;
						} else if ((currentRequest.blockPosition ^ 0xffffffff) == -1) {
							if (inputPacket.payload[0] != -1) {
								currentRequest = null;
							} else {
								currentRequest.blockPosition = 1;
								inputPacket.position = 0;
							}
						} else {
							throw new IOException();
						}
					}
				}
			}
			return true;
		} catch (IOException ioexception) {
			try {
				clientStream.shutDown(i ^ ~0x645c);
			} catch (Exception exception) {
				/* empty */
			}
			reconnectCount++;
			status = -2;
			clientStream = null;
			return (getPriorityRequestCount(i ^ ~0x1000) ^ 0xffffffff) == -1 && (getNormalRequestCount((byte) 51) ^ 0xffffffff) == -1;
		}
	}

	public final void requestClientJs5Drop(int i) {
		do {
			if (clientStream != null) {
				clientStream.shutDown(-29789);
			}
			lastTime = 81L;
			break;
		} while (false);
	}

	public final Js5FileRequest requestFile(int cacheId, byte extraSize, int fileIndex, int i_2_, boolean priority) {
		long fileId = (cacheId << -1790531408) - -fileIndex;
		Js5FileRequest request = new Js5FileRequest();
		request.extraSize = extraSize;
		request.cachedKey = fileId;
		request.aBoolean6037 = priority;
		do {
			if (!priority) {
				if ((getNormalRequestCount((byte) 94) ^ 0xffffffff) <= -21) {
					throw new RuntimeException();
				}
				normalRequest.insert(request, -28);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			if (getPriorityRequestCount(-1) >= 20) {
				throw new RuntimeException();
			}
			priorityRequest.insert(request, -106);
		} while (false);
		return request;
	}

	public final void requestServerDrop(int i) {
		if (clientStream != null) {
			try {
				outputPacket.position = 0;
				outputPacket.writeByte(7, 77);
				outputPacket.method1225(-24472, 0);
				if (i == 29043) {
					clientStream.write((byte) 77, 0, 4, outputPacket.payload);
				}
			} catch (IOException ioexception) {
				try {
					clientStream.shutDown(i ^ ~0x52f);
				} catch (Exception exception) {
					/* empty */
				}
				clientStream = null;
				status = -2;
				reconnectCount++;
			}
		}
	}

	private final void sendConnected(byte i) {
		if (clientStream != null) {
			try {
				outputPacket.position = 0;
				outputPacket.writeByte(6, -111);
				outputPacket.method1225(-24472, 3);
				clientStream.write((byte) 77, 0, 4, outputPacket.payload);
			} catch (IOException ioexception) {
				try {
					clientStream.shutDown(-29789);
				} catch (Exception exception) {
					/* empty */
				}
				reconnectCount++;
				status = -2;
				clientStream = null;
			}
		}
	}

	public final void setDummyStreamMode(int i) {
		do {
			if (clientStream == null) {
				break;
			}
			clientStream.setDummyMode(-69);
			break;
		} while (false);
	}
}

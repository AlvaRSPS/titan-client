/* Class339 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import com.Char;
import com.Class137;
import com.Class151_Sub7;
import com.Class211;
import com.Class224;
import com.Class246_Sub3_Sub4_Sub3;
import com.Class271;
import com.Class272;
import com.Class287_Sub2;
import com.Class308;
import com.Class370;
import com.Class42_Sub4;
import com.Class81;
import com.Class87;
import com.NodeShort;
import com.Class98_Sub43_Sub4;
import com.DataFs;
import com.GameShell;
import com.IncomingOpcode;
import com.InputStream_Sub2;
import com.LoadingScreenSequence;
import com.ProceduralTextureSource;
import com.RSToolkit;
import com.RtAwtFontWrapper;
import com.TextureMetricsList;
import com.WorldMap;
import com.aa_Sub1;
import com.aa_Sub2;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;
import com.jagex.game.input.RtKeyEvent;
import com.jagex.game.input.impl.AwtKeyListener;

public abstract class Archive {
	public static IncomingOpcode	aClass58_2844			= new IncomingOpcode(74, 11);
	public static int[][]			anIntArrayArray2846		= new int[128][128];
	public static RtKeyEvent[]		anInterface7Array2845	= new RtKeyEvent[75];

	public static final void method3783(RSToolkit toolkit, TextureMetricsList var_d, int i) {
		if (WorldMap.aClass98_Sub46_Sub10_2056 != null && i >= 47) {
			if (QuickChatCategoryParser.loadingProgress < 10) {
				if (!WorldMap.archive.isGroupCached(WorldMap.aClass98_Sub46_Sub10_2056.aString6017, 0)) {
					QuickChatCategoryParser.loadingProgress = RenderAnimDefinitionParser.worldMapJs5.getGroupProgress(29952, WorldMap.aClass98_Sub46_Sub10_2056.aString6017) / 10;
					return;
				}
				Class81.method817(false);
				QuickChatCategoryParser.loadingProgress = 10;
			}
			if (QuickChatCategoryParser.loadingProgress == 10) {
				WorldMap.anInt2078 = WorldMap.aClass98_Sub46_Sub10_2056.anInt6009 >> -91123994 << 2115773958;
				WorldMap.anInt2075 = WorldMap.aClass98_Sub46_Sub10_2056.anInt6008 >> 1322363302 << 1884411078;
				WorldMap.anInt2084 = -WorldMap.anInt2078 + (WorldMap.aClass98_Sub46_Sub10_2056.anInt6023 >> 1885364678 << 969285766) - -64;
				WorldMap.anInt2089 = 64 + (WorldMap.aClass98_Sub46_Sub10_2056.anInt6016 >> 1668330822 << 910225286) - WorldMap.anInt2075;
				int[] is = new int[3];
				int i_0_ = -1;
				int i_1_ = -1;
				if (WorldMap.aClass98_Sub46_Sub10_2056.method1573(Class87.localPlayer.plane, is, -90, aa_Sub2.gameSceneBaseY + (Class87.localPlayer.boundExtentsZ >> 219261193), (Class87.localPlayer.boundExtentsX >> -86706999) + Class272.gameSceneBaseX)) {
					i_1_ = -WorldMap.anInt2078 + is[2];
					i_0_ = is[1] + -WorldMap.anInt2075;
				}
				if (Class211.aBoolean1593 || (i_0_ ^ 0xffffffff) > -1 || (WorldMap.anInt2089 ^ 0xffffffff) >= (i_0_ ^ 0xffffffff) || (i_1_ ^ 0xffffffff) > -1 || (WorldMap.anInt2084 ^ 0xffffffff) >= (i_1_ ^ 0xffffffff)) {
					if (Js5Exception.anInt3205 != -1 && (Node.anInt835 ^ 0xffffffff) != 0) {
						WorldMap.aClass98_Sub46_Sub10_2056.method1570((byte) 100, Node.anInt835, is, Js5Exception.anInt3205);
						if (is != null) {
							Class42_Sub4.anInt5371 = is[1] - WorldMap.anInt2075;
							NodeShort.anInt4197 = is[2] - WorldMap.anInt2078;
						}
						Js5Exception.anInt3205 = Node.anInt835 = -1;
						Class211.aBoolean1593 = false;
					} else {
						WorldMap.aClass98_Sub46_Sub10_2056.method1570((byte) 117, WorldMap.aClass98_Sub46_Sub10_2056.anInt6006 & 0x3fff, is, (WorldMap.aClass98_Sub46_Sub10_2056.anInt6006 & 0xfffc780) >> 809556622);
						NodeShort.anInt4197 = -WorldMap.anInt2078 + is[2];
						Class42_Sub4.anInt5371 = -WorldMap.anInt2075 + is[1];
					}
				} else {
					i_1_ += -5 + (int) (Math.random() * 10.0);
					i_0_ += -5 + (int) (10.0 * Math.random());
					Class42_Sub4.anInt5371 = i_0_;
					NodeShort.anInt4197 = i_1_;
				}
				if (WorldMap.aClass98_Sub46_Sub10_2056.anInt6007 != 37) {
					if (WorldMap.aClass98_Sub46_Sub10_2056.anInt6007 != 50) {
						if ((WorldMap.aClass98_Sub46_Sub10_2056.anInt6007 ^ 0xffffffff) != -76) {
							if ((WorldMap.aClass98_Sub46_Sub10_2056.anInt6007 ^ 0xffffffff) == -101) {
								WorldMap.zoom = WorldMap.aFloat2064 = 8.0F;
							} else if (WorldMap.aClass98_Sub46_Sub10_2056.anInt6007 == 200) {
								WorldMap.zoom = WorldMap.aFloat2064 = 16.0F;
							} else {
								WorldMap.zoom = WorldMap.aFloat2064 = 8.0F;
							}
						} else {
							WorldMap.zoom = WorldMap.aFloat2064 = 6.0F;
						}
					} else {
						WorldMap.zoom = WorldMap.aFloat2064 = 4.0F;
					}
				} else {
					WorldMap.zoom = WorldMap.aFloat2064 = 3.0F;
				}
				WorldMap.anInt2069 = (int) WorldMap.aFloat2064 >> 898065889;
				WorldMap.aByteArrayArrayArray2072 = Class287_Sub2.method3392(WorldMap.anInt2069, (byte) 126);
				aa_Sub1.method155(-1);
				WorldMap.method3297();
				InventoriesDefinitionParser.aClass148_110 = new LinkedList();
				WorldMap.anInt2063 += -2 + (int) (Math.random() * 5.0);
				if (WorldMap.anInt2063 < -8) {
					WorldMap.anInt2063 = -8;
				}
				WorldMap.anInt2071 += (int) (Math.random() * 5.0) + -2;
				if (WorldMap.anInt2063 > 8) {
					WorldMap.anInt2063 = 8;
				}
				if ((WorldMap.anInt2071 ^ 0xffffffff) > 15) {
					WorldMap.anInt2071 = -16;
				}
				if ((WorldMap.anInt2071 ^ 0xffffffff) < -17) {
					WorldMap.anInt2071 = 16;
				}
				WorldMap.method3304(var_d, WorldMap.anInt2063 >> -2097260862 << 776330122, WorldMap.anInt2071 >> 386662625);
				WorldMap.aClass341_2057.method3809(256, -30502, 1024);
				WorldMap.mapScenesLoader.method3771(119, 256, 256);
				WorldMap.aClass302_2062.method3550(-129, 4096);
				DataFs.bConfigDefinitionList.method2679(256, (byte) -90);
				QuickChatCategoryParser.loadingProgress = 20;
			} else if ((QuickChatCategoryParser.loadingProgress ^ 0xffffffff) == -21) {
				RenderAnimDefinitionParser.method3201((byte) 86, true);
				WorldMap.method3305(toolkit, WorldMap.anInt2063, WorldMap.anInt2071);
				QuickChatCategoryParser.loadingProgress = 60;
				RenderAnimDefinitionParser.method3201((byte) 85, true);
				FloorOverlayDefinitionParser.method316(false);
			} else if ((QuickChatCategoryParser.loadingProgress ^ 0xffffffff) == -61) {
				if (!WorldMap.archive.method2728(WorldMap.aClass98_Sub46_Sub10_2056.aString6017 + "_staticelements", 0)) {
					WorldMap.aClass370_2066 = new Class370(0);
				} else {
					if (!WorldMap.archive.isGroupCached(WorldMap.aClass98_Sub46_Sub10_2056.aString6017 + "_staticelements", 0)) {
						return;
					}
					WorldMap.aClass370_2066 = Class370.method491(77, AdvancedMemoryCache.isMembersOnly, WorldMap.archive, WorldMap.aClass98_Sub46_Sub10_2056.aString6017 + "_staticelements");
				}
				WorldMap.method3302();
				QuickChatCategoryParser.loadingProgress = 70;
				RenderAnimDefinitionParser.method3201((byte) 65, true);
				FloorOverlayDefinitionParser.method316(false);
			} else if (QuickChatCategoryParser.loadingProgress == 70) {
				Js5Archive.aClass326_5315 = new RtAwtFontWrapper(toolkit, 11, true, GameShell.canvas);
				QuickChatCategoryParser.loadingProgress = 73;
				RenderAnimDefinitionParser.method3201((byte) 80, true);
				FloorOverlayDefinitionParser.method316(false);
			} else if ((QuickChatCategoryParser.loadingProgress ^ 0xffffffff) == -74) {
				AwtKeyListener.aClass326_3805 = new RtAwtFontWrapper(toolkit, 12, true, GameShell.canvas);
				QuickChatCategoryParser.loadingProgress = 76;
				RenderAnimDefinitionParser.method3201((byte) 46, true);
				FloorOverlayDefinitionParser.method316(false);
			} else if (QuickChatCategoryParser.loadingProgress == 76) {
				Js5Archive.aClass326_5308 = new RtAwtFontWrapper(toolkit, 14, true, GameShell.canvas);
				QuickChatCategoryParser.loadingProgress = 79;
				RenderAnimDefinitionParser.method3201((byte) 96, true);
				FloorOverlayDefinitionParser.method316(false);
			} else if ((QuickChatCategoryParser.loadingProgress ^ 0xffffffff) == -80) {
				Class137.aClass326_1080 = new RtAwtFontWrapper(toolkit, 17, true, GameShell.canvas);
				QuickChatCategoryParser.loadingProgress = 82;
				RenderAnimDefinitionParser.method3201((byte) 94, true);
				FloorOverlayDefinitionParser.method316(false);
			} else if (QuickChatCategoryParser.loadingProgress == 82) {
				Class151_Sub7.aClass326_5009 = new RtAwtFontWrapper(toolkit, 19, true, GameShell.canvas);
				QuickChatCategoryParser.loadingProgress = 85;
				RenderAnimDefinitionParser.method3201((byte) 49, true);
				FloorOverlayDefinitionParser.method316(false);
			} else if (QuickChatCategoryParser.loadingProgress == 85) {
				Class271.aClass326_2033 = new RtAwtFontWrapper(toolkit, 22, true, GameShell.canvas);
				QuickChatCategoryParser.loadingProgress = 88;
				RenderAnimDefinitionParser.method3201((byte) 104, true);
				FloorOverlayDefinitionParser.method316(false);
			} else if ((QuickChatCategoryParser.loadingProgress ^ 0xffffffff) == -89) {
				Class224.aClass326_1686 = new RtAwtFontWrapper(toolkit, 26, true, GameShell.canvas);
				QuickChatCategoryParser.loadingProgress = 91;
				RenderAnimDefinitionParser.method3201((byte) 60, true);
				FloorOverlayDefinitionParser.method316(false);
			} else {
				ProceduralTextureSource.aClass326_3263 = new RtAwtFontWrapper(toolkit, 30, true, GameShell.canvas);
				QuickChatCategoryParser.loadingProgress = 100;
				RenderAnimDefinitionParser.method3201((byte) 62, true);
				FloorOverlayDefinitionParser.method316(false);
				System.gc();
			}
		}
	}

	public static final void method3788(int i, int i_3_, int i_4_, int i_5_, byte i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_) {
		if (i_5_ != i || (i_10_ ^ 0xffffffff) != (i_11_ ^ 0xffffffff) || i_8_ != i_7_ || (i_9_ ^ 0xffffffff) != (i_4_ ^ 0xffffffff)) {
			int i_12_ = i;
			int i_13_ = i_10_;
			int i_14_ = i * 3;
			int i_15_ = i_10_ * 3;
			int i_16_ = 3 * i_5_;
			int i_17_ = i_11_ * 3;
			int i_18_ = 3 * i_8_;
			int i_19_ = i_4_ * 3;
			int i_20_ = i_16_ + i_7_ + -i_18_ - i;
			int i_21_ = i_9_ + -i_19_ + i_17_ - i_10_;
			int i_22_ = i_18_ + -i_16_ - (i_16_ + -i_14_);
			int i_23_ = -i_17_ + -i_17_ + i_19_ - -i_15_;
			int i_24_ = i_16_ + -i_14_;
			int i_25_ = i_17_ + -i_15_;
			for (int i_26_ = 128; (i_26_ ^ 0xffffffff) >= -4097; i_26_ += 128) {
				int i_27_ = i_26_ * i_26_ >> 671351852;
				int i_28_ = i_26_ * i_27_ >> 962258476;
				int i_29_ = i_20_ * i_28_;
				int i_30_ = i_28_ * i_21_;
				int i_31_ = i_27_ * i_22_;
				int i_32_ = i_23_ * i_27_;
				int i_33_ = i_26_ * i_24_;
				int i_34_ = i_25_ * i_26_;
				int i_35_ = i - -(i_29_ - (-i_31_ - i_33_) >> -1199794356);
				int i_36_ = (i_34_ + i_30_ + i_32_ >> 962061708) + i_10_;
				InputStream_Sub2.method125(i_35_, i_3_, i_36_, i_13_, i_12_, 21597);
				i_13_ = i_36_;
				i_12_ = i_35_;
			}
		} else {
			InputStream_Sub2.method125(i_7_, i_3_, i_9_, i_10_, i, 21597);
		}
		if (i_6_ != -67) {
			return;
		}
	}

	public static final void method3789(int i) {
		if (Class308.aClass98_Sub46_Sub9_2583 != null) {
			Class308.aClass98_Sub46_Sub9_2583 = null;
			Class246_Sub3_Sub4_Sub3.method3071(Class98_Sub43_Sub4.anInt5938, -1, ScalingSpriteLoadingScreenElement.anInt3439, LoadingScreenSequence.anInt2128, BackgroundColourLSEConfig.anInt3518);
		}
	}

	public Archive() {
		/* empty */
	}

	public abstract byte[] getGroupData(int i, int i_2_);

	public abstract int getGroupProgress(int i, byte i_37_);

	public abstract Js5Index getIndex(byte i);

	public abstract void requestGroup(int i, boolean bool);
}

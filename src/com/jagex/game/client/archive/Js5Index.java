/* Class312 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import com.*;
import com.jagex.core.crypto.CRC32;
import com.jagex.game.client.definition.GameObjectDefinition;

public final class Js5Index {
	public static IncomingOpcode aClass58_2661 = new IncomingOpcode(89, 0);

	public static final void method3620(GameObjectDefinition class352, int i, int i_0_, int i_1_, int i_2_) {
		Class98_Sub42 class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub37.aClass148_5748.getFirst(32);
		if (i != -22015) {
			aClass58_2661 = null;
		}
		for (/**/; class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub37.aClass148_5748.getNext(97)) {
			if (i_1_ == class98_sub42.anInt4220 && i_2_ << -172547319 == class98_sub42.anInt4229 && class98_sub42.anInt4225 == i_0_ << -244251415 && (class352.id ^ 0xffffffff) == (class98_sub42.aClass352_4233.id ^ 0xffffffff)) {
				if (class98_sub42.aClass98_Sub31_Sub5_4232 != null) {
					Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4232);
					class98_sub42.aClass98_Sub31_Sub5_4232 = null;
				}
				if (class98_sub42.aClass98_Sub31_Sub5_4230 != null) {
					Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4230);
					class98_sub42.aClass98_Sub31_Sub5_4230 = null;
				}
				class98_sub42.unlink(87);
				break;
			}
		}
	}

	public static void method3621(byte i) {
		if (i != 7) {
			method3620(null, -45, -88, 50, 116);
		}
		aClass58_2661 = null;
	}

	public static final boolean method3623(int i, int i_28_) {
		if (i > -68) {
			aClass58_2661 = null;
		}
		return !((i_28_ ^ 0xffffffff) != -4 && i_28_ != 4);
	}

	private byte[]	_hash;
	int				crc32;
	int[][]			fileEntryFileId;
	int				fitEntryCount;
	int[]			fitEntryGroupId;
	int				groupCount;
	int[]			groupCrc32;
	int[]			groupEntryCount;
	int[]			groupFileCount;
	int[][]			groupFileNameRaw;
	NameHashTable[]	groupFileNames;
	NameHashTable	groupName;
	int[]			groupNameRaw;

	int[]			groupRevision;

	byte[][]		groupWhirlpool;

	int				indexVersion;

	Js5Index(byte[] data, int crc, byte[] hash) {
		crc32 = CRC32.calculate(data.length, data, -30091);
		if (crc != crc32) {
			throw new RuntimeException("Invalid CRC - expected:");
		}
		if (hash != null) {
			if (hash.length != 64) {
				throw new RuntimeException("Invalid expectedwhirlpool - must be 64 bytes long");
			}
			_hash = WhirlpoolGenerator.whirlpool(0, (byte) -121, data, data.length);
			for (int ptr = 0; ptr < 64; ptr++) {
				if ((_hash[ptr] ^ 0xffffffff) != (hash[ptr] ^ 0xffffffff)) {
					throw new RuntimeException("Invalid Whirlpool - expected:");
				}
			}
		}
		deserialize(data, -7572);
	}

	private final void deserialize(byte[] data, int i) {
		do {
			RSByteBuffer packet = new RSByteBuffer(Js5Container.getPayload(data, (byte) -84));
			int version = packet.readUnsignedByte((byte) 23);
			if (version < 5 || version > 6) {
				throw new RuntimeException("Incorrect JS5 protocol number: ");
			}
			if ((version ^ 0xffffffff) > -7) {
				indexVersion = 0;
			} else {
				indexVersion = packet.readInt(-2);
			}
			int flags = packet.readUnsignedByte((byte) -127);
			boolean hasNames = (0x1 & flags) != 0;
			fitEntryCount = packet.readShort((byte) 127);
			boolean hasHashes = (0x2 & flags ^ 0xffffffff) != -1;
			int fileIdBuf = 0;
			fitEntryGroupId = new int[fitEntryCount];
			int maxGroupId = -1;
			for (int entryId = 0; fitEntryCount > entryId; entryId++) {
				fitEntryGroupId[entryId] = fileIdBuf += packet.readShort((byte) 127);
				if (fitEntryGroupId[entryId] > maxGroupId) {
					maxGroupId = fitEntryGroupId[entryId];
				}
			}
			groupCount = 1 + maxGroupId;
			if (hasHashes) {
				groupWhirlpool = new byte[groupCount][];
			}
			groupEntryCount = new int[groupCount];
			fileEntryFileId = new int[groupCount][];
			groupFileCount = new int[groupCount];
			groupRevision = new int[groupCount];
			groupCrc32 = new int[groupCount];
			if (hasNames) {
				groupNameRaw = new int[groupCount];
				for (int groupId = 0; (groupId ^ 0xffffffff) > (groupCount ^ 0xffffffff); groupId++) {
					groupNameRaw[groupId] = -1;
				}
				for (int entryId = 0; entryId < fitEntryCount; entryId++) {
					groupNameRaw[fitEntryGroupId[entryId]] = packet.readInt(-2);
				}
				groupName = new NameHashTable(groupNameRaw);
			}
			for (int entryId = 0; fitEntryCount > entryId; entryId++) {
				groupCrc32[fitEntryGroupId[entryId]] = packet.readInt(-2);
			}
			if (hasHashes) {
				for (int entryId = 0; (entryId ^ 0xffffffff) > (fitEntryCount ^ 0xffffffff); entryId++) {
					byte[] hash = new byte[64];
					packet.getData(hash, true, 64, 0);
					groupWhirlpool[fitEntryGroupId[entryId]] = hash;
				}
			}
			if (i == -7572) {
				for (int groupId = 0; fitEntryCount > groupId; groupId++) {
					groupRevision[fitEntryGroupId[groupId]] = packet.readInt(-2);
				}
				for (int entryId = 0; fitEntryCount > entryId; entryId++) {
					groupEntryCount[fitEntryGroupId[entryId]] = packet.readShort((byte) 127);
				}
				for (int i_16_ = 0; (fitEntryCount ^ 0xffffffff) < (i_16_ ^ 0xffffffff); i_16_++) {
					int groupId = fitEntryGroupId[i_16_];
					int fileCount = groupEntryCount[groupId];
					fileIdBuf = 0;
					fileEntryFileId[groupId] = new int[fileCount];
					int maxFileId = -1;
					for (int fileEntryId = 0; (fileEntryId ^ 0xffffffff) > (fileCount ^ 0xffffffff); fileEntryId++) {
						int fileId = fileEntryFileId[groupId][fileEntryId] = fileIdBuf += packet.readShort((byte) 127);
						if ((fileId ^ 0xffffffff) < (maxFileId ^ 0xffffffff)) {
							maxFileId = fileId;
						}
					}
					groupFileCount[groupId] = 1 + maxFileId;
					if ((fileCount ^ 0xffffffff) == (1 + maxFileId ^ 0xffffffff)) {
						fileEntryFileId[groupId] = null;
					}
				}
				if (!hasNames) {
					break;
				}
				groupFileNames = new NameHashTable[1 + maxGroupId];
				groupFileNameRaw = new int[1 + maxGroupId][];
				for (int i_22_ = 0; fitEntryCount > i_22_; i_22_++) {
					int groupId = fitEntryGroupId[i_22_];
					int entryCount = groupEntryCount[groupId];
					groupFileNameRaw[groupId] = new int[groupFileCount[groupId]];
					for (int fileId = 0; (fileId ^ 0xffffffff) > (groupFileCount[groupId] ^ 0xffffffff); fileId++) {
						groupFileNameRaw[groupId][fileId] = -1;
					}
					for (int fileEntryId = 0; (fileEntryId ^ 0xffffffff) > (entryCount ^ 0xffffffff); fileEntryId++) {
						int fileId;
						if (fileEntryFileId[groupId] != null) {
							fileId = fileEntryFileId[groupId][fileEntryId];
						} else {
							fileId = fileEntryId;
						}
						groupFileNameRaw[groupId][fileId] = packet.readInt(i + 7570);
					}
					groupFileNames[groupId] = new NameHashTable(groupFileNameRaw[groupId]);
				}
			}
			break;
		} while (false);
	}
}

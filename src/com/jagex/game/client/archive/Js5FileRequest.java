/* Class98_Sub46_Sub13_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import com.AnimationSkeletonSet;
import com.Class130;
import com.Class189;
import com.Class258;
import com.Class33;
import com.Class4;
import com.Class65;
import com.Class76_Sub7;
import com.Class98_Sub10;
import com.Class98_Sub10_Sub32;
import com.Class98_Sub46_Sub19;
import com.Class98_Sub49;
import com.OpenGLRenderEffectManager;
import com.PacketBufferManager;
import com.RSByteBuffer;
import com.Sprite;
import com.client;
import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.preferences.FlickeringEffectsPreferenceField;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Js5FileRequest extends FileRequest {
	public static Class258 aClass258_6307 = new Class258();

	public static final void method1593(byte i) {
		int i_0_ = 0;
		if ((client.preferences.lightningDetail.getValue((byte) 127) ^ 0xffffffff) == -2) {
			i_0_ |= 0x1;
			i_0_ |= 0x10;
			i_0_ |= 0x20;
			i_0_ |= 0x2;
			i_0_ |= 0x4;
		}
		if (client.preferences.textures.getValue((byte) 124) == 0) {
			i_0_ |= 0x40;
		}
		AnimationSkeletonSet.method1618(i_0_, (byte) -85);
		Class130.gameObjectDefinitionList.method3554(true, i_0_);
		Class98_Sub46_Sub19.itemDefinitionList.method2712(60, i_0_);
		Class4.npcDefinitionList.method3541(true, i_0_);
		BuildLocation.gfxDefinitionList.method3560(i_0_, 0);
		FlickeringEffectsPreferenceField.method606(18279, i_0_);
		AnimationDefinition.method936(i_0_, 1024);
		Class65.method678(i_0_, false);
		LinkedList.method2429(117, i_0_);
		Class98_Sub10.method999((byte) 124);
	}

	public static final void method1594(int i, int i_1_, int i_2_, int i_3_, byte i_4_) {
		int i_5_ = BConfigDefinition.anInt3117;
		int i_6_ = Class98_Sub49.anInt4286;
		if (OpenGLHeap.aBoolean6079) {
			i_5_ += Class189.method2642((byte) 42);
			i_6_ += MapScenesDefinitionParser.method3765(false);
		}
		if (i_4_ <= 115) {
			PacketBufferManager.poolVarPtr = null;
		}
		if ((OpenGLRenderEffectManager.anInt440 ^ 0xffffffff) == -2) {
			Sprite class332 = Class76_Sub7.aClass332Array3764[Class98_Sub10_Sub32.anInt5720 / 100];
			class332.draw(i_5_ + -8, -8 + i_6_);
			AnimatedProgressBarLSEConfig.method908(class332.getRenderHeight() + -8 + i_6_, -8 + i_6_, false, i_5_ - 8, -8 + i_5_ - -class332.getRenderWidth());
		}
		if (OpenGLRenderEffectManager.anInt440 == 2) {
			Sprite class332 = Class76_Sub7.aClass332Array3764[4 - -(Class98_Sub10_Sub32.anInt5720 / 100)];
			class332.draw(-8 + i_5_, -8 + i_6_);
			AnimatedProgressBarLSEConfig.method908(class332.getRenderHeight() + -8 + i_6_, -8 + i_6_, false, i_5_ - 8, -8 + i_5_ + class332.getRenderWidth());
		}
		OrthoZoomPreferenceField.method623(-23196);
	}

	public static void method1596(byte i) {
		if (i > 6) {
			PacketBufferManager.poolVarPtr = null;
			aClass258_6307 = null;
		}
	}

	public static final void method1597(int[] is, Object[] objects, int i) {
		Class33.method323(is, objects, i, -1 + is.length, 0);
	}

	int				blockPosition;

	byte			extraSize;

	RSByteBuffer	packet;

	@Override
	public final byte[] getData(int i) {
		if (this.inProgress || packet.payload.length - extraSize > packet.position) {
			throw new RuntimeException();
		}
		return packet.payload;
	}

	@Override
	public final int getProgress(int i) {
		if (packet == null) {
			return 0;
		}
		return packet.position * 100 / (-extraSize + packet.payload.length);
	}
}

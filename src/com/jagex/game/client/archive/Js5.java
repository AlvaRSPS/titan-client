/* Class207 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import com.ArrayUtils;
import com.Char;
import com.Class154;
import com.Class159;
import com.Class165;
import com.Class18;
import com.Class187;
import com.Class200;
import com.Class281;
import com.Class284_Sub1;
import com.Class349;
import com.Class85;
import com.Class87;
import com.Class94;
import com.Class96;
import com.Class98_Sub10_Sub38;
import com.Class98_Sub10_Sub7;
import com.Class98_Sub28_Sub1;
import com.Class98_Sub48;
import com.GameShell;
import com.IncomingOpcode;
import com.OutgoingPacket;
import com.RSByteBuffer;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.core.crypto.CRC32;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessage;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.font.Font;

public final class Js5 {
	public static QuickChatMessageType	aClass348_1569	= new QuickChatMessageType(13, 0, 1, 0);
	public static IncomingOpcode		aClass58_1576	= new IncomingOpcode(92, 16);
	public static int					anInt1577;

	public static final int hashName(String string, boolean bool) {
		int i = string.length();
		int i_306_ = 0;
		for (int i_307_ = 0; i > i_307_; i_307_++) {
			i_306_ = (i_306_ << 1982319173) - (i_306_ - Class349.method3843((byte) 88, string.charAt(i_307_)));
		}
		return i_306_;
	}

	public static final void method2746(Font class43) {
		Class284_Sub1.font = class43;
	}

	public static final float method2753(int i, int i_71_, float f, float f_72_, float f_73_, float f_74_, float f_75_, float f_76_) {
		float f_77_ = 0.0F;
		if (i_71_ != 10665) {
			anInt1577 = -101;
		}
		float f_78_ = -f_76_ + f_72_;
		float f_79_ = -f_74_ + f_73_;
		float f_80_ = -f + f_75_;
		float f_81_ = 0.0F;
		float f_82_ = 0.0F;
		float f_83_ = 0.0F;
		while (f_77_ < 1.1F) {
			float f_84_ = f_76_ + f_78_ * f_77_;
			float f_85_ = f_74_ + f_79_ * f_77_;
			float f_86_ = f + f_77_ * f_80_;
			int i_87_ = (int) f_84_ >> -1052735575;
			int i_88_ = (int) f_86_ >> 1278945001;
			if (i_87_ > 0 && i_88_ > 0 && i_87_ < Class165.mapWidth && i_88_ < Class98_Sub10_Sub7.mapLength) {
				int i_89_ = Class87.localPlayer.plane;
				if ((i_89_ ^ 0xffffffff) > -4 && (0x2 & Class281.flags[1][i_87_][i_88_] ^ 0xffffffff) != -1) {
					i_89_++;
				}
				int i_90_ = StrongReferenceMCNode.aSArray6298[i_89_].averageHeight((int) f_84_, (int) f_86_, true);
				if (f_85_ > i_90_) {
					if (i >= 2) {
						return 0.1F * method2753(-1 + i, 10665, f_83_, f_84_, f_85_, f_82_, f_86_, f_81_) + (f_77_ - 0.1F);
					}
					return f_77_;
				}
			}
			f_77_ += 0.1F;
			f_83_ = f_86_;
			f_81_ = f_84_;
			f_82_ = f_85_;
		}
		return -1.0F;
	}

	public static final void method2764(int i, int interfaceId, int i_110_) {
		if (i_110_ < -24 && Class85.loadInterface(interfaceId, 124)) {
			Class187.method2634(0, Class159.interfaceStore[interfaceId], i);
		}
	}

	public static final void method2765(byte i) {
		Class154.aHa1231.a(Class200.aClass111_1543);
		if (i < 1) {
			BuildLocation.WTWIP = null;
		}
		Class154.aHa1231.DA(Class98_Sub48.anInt4279, NativeProgressMonitor.anInt3391, Class96.anInt802, Class98_Sub10_Sub38.anInt5752);
	}

	public static final void searchQuickChatMessage(boolean restricted, String message, byte i) {
		message = message.toLowerCase();
		short[] is = new short[16];
		int i_93_ = 0;
		int maxCount = !restricted ? 0 : 32768;
		int i_95_ = maxCount - -(restricted ? NewsLSEConfig.quickChatMessageList.restrictedCount : NewsLSEConfig.quickChatMessageList.count);
		if (i > 51) {
			for (int i_96_ = maxCount; (i_95_ ^ 0xffffffff) < (i_96_ ^ 0xffffffff); i_96_++) {
				QuickChatMessage qcMessage = NewsLSEConfig.quickChatMessageList.get(i_96_, 83);
				if (qcMessage.searchable && (qcMessage.getMessage(false).toLowerCase().indexOf(message) ^ 0xffffffff) != 0) {
					if ((i_93_ ^ 0xffffffff) <= -51) {
						Class18.resultBufferSize = -1;
						MaxScreenSizePreferenceField.resultIndexBuffer = null;
						return;
					}
					if ((is.length ^ 0xffffffff) >= (i_93_ ^ 0xffffffff)) {
						short[] is_97_ = new short[2 * is.length];
						for (int i_98_ = 0; i_98_ < i_93_; i_98_++) {
							is_97_[i_98_] = is[i_98_];
						}
						is = is_97_;
					}
					is[i_93_++] = (short) i_96_;
				}
			}
			Class18.resultBufferSize = i_93_;
			Class85.resultBufferPtr = 0;
			MaxScreenSizePreferenceField.resultIndexBuffer = is;
			String[] strings = new String[Class18.resultBufferSize];
			for (int i_99_ = 0; i_99_ < Class18.resultBufferSize; i_99_++) {
				strings[i_99_] = NewsLSEConfig.quickChatMessageList.get(is[i_99_], 48).getMessage(false);
			}
			OutgoingPacket.quickSort(true, MaxScreenSizePreferenceField.resultIndexBuffer, strings);
		}
	}

	private boolean		aBoolean1570;

	private Archive		aClass339_1574;

	private Object[]	anObjectArray1572;

	public int			discardUnpacked;

	private Object[][]	fileData;

	private Js5Index	index	= null;

	public Js5(Archive class339, boolean bool, int i) {
		if ((i ^ 0xffffffff) > -1 || i > 2) {
			throw new IllegalArgumentException("js5: Invalid value " + i + " supplied for discardunpacked");
		}
		aClass339_1574 = class339;
		aBoolean1570 = bool;
		discardUnpacked = i;
	}

	public final void discardAllUnpacked(byte i) {
		do {
			if (i > -114) {
				isGroupCached(null, -42);
			}
			if (fileData == null) {
				break;
			}
			for (int i_100_ = 0; fileData.length > i_100_; i_100_++) {
				fileData[i_100_] = null;
			}
			break;
		} while (false);
	}

	public final void discardNames(boolean bool, boolean bool_63_, byte i) {
		if (isReady((byte) -123)) {
			if (bool) {
				index.groupName = null;
				index.groupNameRaw = null;
			}
			index.groupFileNames = null;
			index.groupFileNameRaw = null;
		}
	}

	public final void discardUnpacked(int i, int i_91_) {
		do {
			if (i <= -6 && isValidGroup(i_91_, false)) {
				if (fileData == null) {
					break;
				}
				fileData[i_91_] = null;
			}
			break;
		} while (false);
	}

	public final byte[] getFile(int i, int i_43_) {
		if (!isReady((byte) -123)) {
			return null;
		}
		if ((index.groupFileCount.length ^ 0xffffffff) == -2) {
			return this.getFile(i, 0, false);
		}
		if (!isValidGroup(i, false)) {
			return null;
		}
		if (index.groupFileCount[i] == 1) {
			return this.getFile(0, i, false);
		}
		throw new RuntimeException();
	}

	public final byte[] getFile(int fileId, int groupId, boolean bool) {
		return this.getFile(null, 5, fileId, groupId);
	}

	public final byte[] getFile(int[] xteaKeys, int dummy, int fileId, int groupId) {
		if (!isValidFile(0, groupId, fileId)) {
			return null;
		}
		if (fileData[groupId] == null || fileData[groupId][fileId] == null) {
			boolean succeeded = unpackGroup(xteaKeys, fileId, (byte) -66, groupId);
			if (!succeeded) {
				requestGroup(groupId, 103);
				succeeded = unpackGroup(xteaKeys, fileId, (byte) -95, groupId);
				if (!succeeded) {
					return null;
				}
			}
		}
		byte[] data = Class98_Sub28_Sub1.unpackDataBuffer(false, fileData[groupId][fileId], false);
		do {
			if (discardUnpacked == 1) {
				fileData[groupId][fileId] = null;
				if (index.groupFileCount[groupId] != 1) {
					break;
				}
				fileData[groupId] = null;
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			if (discardUnpacked == 2) {
				fileData[groupId] = null;
			}
		} while (false);
		return data;
	}

	public final int getFileCount(int i, int groupId) {
		if (!isValidGroup(groupId, false)) {
			return 0;
		}
		return index.groupFileCount[groupId];
	}

	public final int getGroupCount(byte i) {
		if (!isReady((byte) -123)) {
			return -1;
		}
		return index.groupFileCount.length;
	}

	public final int getGroupId(byte i, String groupName) {
		if (!isReady((byte) -125)) {
			return -1;
		}
		groupName = groupName.toLowerCase();
		if (i > -49) {
			return -53;
		}
		int groupIndex = index.groupName.getIndex((byte) -26, hashName(groupName, false));
		if (!isValidGroup(groupIndex, false)) {
			return -1;
		}
		return groupIndex;
	}

	public final int getGroupProgress(int i, String string) {
		if (!isReady((byte) -127)) {
			return 0;
		}
		string = string.toLowerCase();
		int i_65_ = index.groupName.getIndex((byte) -26, hashName(string, false));
		return method2740(117, i_65_);
	}

	public final int getIndexCrc32(byte i) {
		if (i > -2) {
			isValidFile(-36, -101, 7);
		}
		if (!isReady((byte) -124)) {
			throw new IllegalStateException("");
		}
		return index.crc32;
	}

	public final int getProgress(byte i) {
		if (!isReady((byte) -126)) {
			return 0;
		}
		int i_102_ = 0;
		int i_103_ = 0;
		for (int i_104_ = 0; (anObjectArray1572.length ^ 0xffffffff) < (i_104_ ^ 0xffffffff); i_104_++) {
			if (index.groupEntryCount[i_104_] > 0) {
				i_103_ += method2740(-48, i_104_);
				i_102_ += 100;
			}
		}
		if (i_102_ == 0) {
			return 100;
		}
		int i_106_ = i_103_ * 100 / i_102_;
		return i_106_;
	}

	public final boolean isCached(String string, boolean bool) {
		if (bool != false) {
			fileData = null;
		}
		int i = getGroupId((byte) -60, "");
		if ((i ^ 0xffffffff) != 0) {
			return method2744("", string, 1);
		}
		return method2744(string, "", 1);
	}

	public final boolean isFileCached(int i, int i_56_) {
		if (!isReady((byte) -126)) {
			return false;
		}
		if ((index.groupFileCount.length ^ 0xffffffff) == -2) {
			return this.isFileCached(i_56_, 0, -6329);
		}
		if (i > -18) {
			return false;
		}
		if (!isValidGroup(i_56_, false)) {
			return false;
		}
		if ((index.groupFileCount[i_56_] ^ 0xffffffff) == -2) {
			return this.isFileCached(0, i_56_, -6329);
		}
		throw new RuntimeException();
	}

	public final boolean isFileCached(int i, int i_69_, int i_70_) {
		if (!isValidFile(i_70_ ^ i_70_, i_69_, i)) {
			return false;
		}
		if (fileData[i_69_] != null && fileData[i_69_][i] != null) {
			return true;
		}
		if (anObjectArray1572[i_69_] != null) {
			return true;
		}
		requestGroup(i_69_, 103);
		return anObjectArray1572[i_69_] != null;
	}

	public final boolean isFullyCached(byte i) {
		if (i <= 8) {
			anInt1577 = 40;
		}
		if (!isReady((byte) -127)) {
			return false;
		}
		boolean bool = true;
		for (int i_39_ : index.fitEntryGroupId) {
			if (anObjectArray1572[i_39_] == null) {
				requestGroup(i_39_, 126);
				if (anObjectArray1572[i_39_] == null) {
					bool = false;
				}
			}
		}
		return bool;
	}

	public final boolean isGroupCached(boolean bool, int i) {
		if (!isValidGroup(i, bool)) {
			return false;
		}
		if (anObjectArray1572[i] != null) {
			return true;
		}
		requestGroup(i, 112);
		return anObjectArray1572[i] != null;
	}

	public final boolean isGroupCached(String string, int i) {
		if (i != 0) {
			aBoolean1570 = true;
		}
		if (!isReady((byte) -127)) {
			return false;
		}
		string = string.toLowerCase();
		int i_55_ = index.groupName.getIndex((byte) -26, hashName(string, false));
		return isGroupCached(false, i_55_);
	}

	public final boolean isReady(byte i) {
		if (i > -122) {
			anObjectArray1572 = null;
		}
		if (index == null) {
			index = aClass339_1574.getIndex((byte) -124);
			if (index == null) {
				return false;
			}
			fileData = new Object[index.groupCount][];
			anObjectArray1572 = new Object[index.groupCount];
		}
		return true;
	}

	public final boolean isValidFile(int i, int i_40_, int i_41_) {
		if (!isReady((byte) -124)) {
			return false;
		}
		if (i_40_ < i || i_41_ < 0 || i_40_ >= index.groupFileCount.length || i_41_ >= index.groupFileCount[i_40_]) {
			if (Class94.aBoolean797) {
				throw new IllegalArgumentException(String.valueOf(i_40_) + "," + i_41_);
			}
			return false;
		}
		return true;
	}

	public final boolean isValidGroup(int i, boolean bool) {
		if (!isReady((byte) -126)) {
			return false;
		}
		if ((i ^ 0xffffffff) > -1 || (i ^ 0xffffffff) <= (index.groupFileCount.length ^ 0xffffffff) || index.groupFileCount[i] == 0) {
			if (!Class94.aBoolean797) {
				return false;
			}
			throw new IllegalArgumentException(Integer.toString(i));
		}
		if (bool != false) {
			aClass348_1569 = null;
		}
		return true;
	}

	public final boolean method2728(String string, int i) {
		if (!isReady((byte) -123)) {
			return false;
		}
		string = string.toLowerCase();
		int i_0_ = index.groupName.getIndex((byte) -26, hashName(string, false));
		return i <= i_0_;

	}

	private final void method2732(int i, int i_42_) {
		aClass339_1574.requestGroup(i, false);

	}

	public final boolean method2737(boolean bool, String string, String string_48_) {
		if (!isReady((byte) -127)) {
			return false;
		}
		if (bool != true) {
			return false;
		}
		string_48_ = string_48_.toLowerCase();
		string = string.toLowerCase();
		int i = index.groupName.getIndex((byte) -26, hashName(string_48_, !bool));
		if ((i ^ 0xffffffff) > -1) {
			return false;
		}
		int i_49_ = index.groupFileNames[i].getIndex((byte) -26, hashName(string, false));
		return (i_49_ ^ 0xffffffff) <= -1;
	}

	public final byte[] method2739(String string, String string_50_, int i) {
		if (!isReady((byte) -124)) {
			return null;
		}
		string = string.toLowerCase();
		string_50_ = string_50_.toLowerCase();
		int i_51_ = index.groupName.getIndex((byte) -26, hashName(string, false));
		if (!isValidGroup(i_51_, false)) {
			return null;
		}
		if (i != -32734) {
			return null;
		}
		int i_52_ = index.groupFileNames[i_51_].getIndex((byte) -26, hashName(string_50_, false));
		return this.getFile(i_52_, i_51_, false);
	}

	private final int method2740(int i, int i_53_) {
		if (!isValidGroup(i_53_, false)) {
			return 0;
		}
		if (anObjectArray1572[i_53_] != null) {
			return 100;
		}
		return aClass339_1574.getGroupProgress(i_53_, (byte) -106);
	}

	public final int[] method2743(int i, int i_57_) {
		if (i_57_ != 6341) {
			return null;
		}
		if (!isValidGroup(i, false)) {
			return null;
		}
		int[] is = index.fileEntryFileId[i];
		if (is == null) {
			is = new int[index.groupEntryCount[i]];
			for (int i_58_ = 0; is.length > i_58_; i_58_++) {
				is[i_58_] = i_58_;
			}
		}
		return is;
	}

	public final boolean method2744(String string, String string_59_, int i) {
		if (!isReady((byte) -128)) {
			return false;
		}
		string = string.toLowerCase();
		string_59_ = string_59_.toLowerCase();
		int i_60_ = index.groupName.getIndex((byte) -26, hashName(string, false));
		if (!isValidGroup(i_60_, false)) {
			return false;
		}
		int i_61_ = index.groupFileNames[i_60_].getIndex((byte) -26, hashName(string_59_, false));
		if (i != 1) {
			isFullyCached((byte) 3);
		}
		return this.isFileCached(i_61_, i_60_, i + -6330);
	}

	public final void method2755(String string, int i) {
		if (i <= -90 && isReady((byte) -127)) {
			string = string.toLowerCase();
			int i_92_ = index.groupName.getIndex((byte) -26, hashName(string, false));
			method2732(i_92_, 32768);
		}
	}

	public final int method2763(int i, int i_107_) {
		if (!isReady((byte) -126)) {
			return -1;
		}
		int i_108_ = index.groupName.getIndex((byte) -26, i_107_);
		if (!isValidGroup(i_108_, false)) {
			return -1;
		}
		if (i <= 35) {
			method2746(null);
		}
		return i_108_;
	}

	public final void method2766(int i) {
		if (anObjectArray1572 != null) {
			for (int i_111_ = 0; (i_111_ ^ 0xffffffff) > (anObjectArray1572.length ^ 0xffffffff); i_111_++) {
				anObjectArray1572[i_111_] = null;
			}
		}
	}

	private final void requestGroup(int i, int i_66_) {
		do {
			if (aBoolean1570) {
				anObjectArray1572[i] = aClass339_1574.getGroupData(i, 0);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			anObjectArray1572[i] = GroundBlendingPreferenceField.method654(2, aClass339_1574.getGroupData(i, 0), false);
		} while (false);
	}

	public final boolean unpackGroup(int[] is, int i, byte i_1_, int i_2_) {
		if (!isValidGroup(i_2_, false)) {
			return false;
		}
		if (anObjectArray1572[i_2_] == null) {
			return false;
		}
		int i_3_ = index.groupEntryCount[i_2_];
		int[] is_4_ = index.fileEntryFileId[i_2_];
		if (fileData[i_2_] == null) {
			fileData[i_2_] = new Object[index.groupFileCount[i_2_]];
		}
		Object[] objects = fileData[i_2_];
		boolean bool = true;
		if (i_1_ >= -62) {
			method2728(null, 43);
		}
		for (int i_5_ = 0; (i_5_ ^ 0xffffffff) > (i_3_ ^ 0xffffffff); i_5_++) {
			int i_6_;
			if (is_4_ == null) {
				i_6_ = i_5_;
			} else {
				i_6_ = is_4_[i_5_];
			}
			if (objects[i_6_] == null) {
				bool = false;
				break;
			}
		}
		if (bool) {
			return true;
		}
		byte[] is_7_;
		if (is != null && ((is[0] ^ 0xffffffff) != -1 || (is[1] ^ 0xffffffff) != -1 || (is[2] ^ 0xffffffff) != -1 || is[3] != 0)) {
			is_7_ = Class98_Sub28_Sub1.unpackDataBuffer(false, anObjectArray1572[i_2_], true);
			RSByteBuffer class98_sub22 = new RSByteBuffer(is_7_);
			class98_sub22.method1215(is, 5, class98_sub22.payload.length, (byte) 30);
		} else {
			is_7_ = Class98_Sub28_Sub1.unpackDataBuffer(false, anObjectArray1572[i_2_], false);
		}
		byte[] is_8_;
		try {
			is_8_ = Js5Container.getPayload(is_7_, (byte) -84);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "T3 - " + (is != null) + "," + i_2_ + "," + is_7_.length + "," + CRC32.calculate(is_7_.length, is_7_, -30091) + "," + CRC32.calculate(-2 + is_7_.length, is_7_, -30091) + "," + index.groupCrc32[i_2_] + "," + index.crc32);
		}
		if (aBoolean1570) {
			anObjectArray1572[i_2_] = null;
		}
		if ((i_3_ ^ 0xffffffff) >= -2) {
			int i_9_;
			if (is_4_ == null) {
				i_9_ = 0;
			} else {
				i_9_ = is_4_[0];
			}
			if ((discardUnpacked ^ 0xffffffff) != -1) {
				objects[i_9_] = is_8_;
			} else {
				objects[i_9_] = GroundBlendingPreferenceField.method654(2, is_8_, false);
			}
		} else if (discardUnpacked != 2) {
			int i_10_ = is_8_.length;
			int i_11_ = is_8_[--i_10_] & 0xff;
			i_10_ -= 4 * i_11_ * i_3_;
			RSByteBuffer class98_sub22 = new RSByteBuffer(is_8_);
			class98_sub22.position = i_10_;
			int[] is_12_ = new int[i_3_];
			for (int i_13_ = 0; (i_13_ ^ 0xffffffff) > (i_11_ ^ 0xffffffff); i_13_++) {
				int i_14_ = 0;
				for (int i_15_ = 0; (i_3_ ^ 0xffffffff) < (i_15_ ^ 0xffffffff); i_15_++) {
					i_14_ += class98_sub22.readInt(-2);
					is_12_[i_15_] += i_14_;
				}
			}
			byte[][] is_16_ = new byte[i_3_][];
			for (int i_17_ = 0; i_17_ < i_3_; i_17_++) {
				is_16_[i_17_] = new byte[is_12_[i_17_]];
				is_12_[i_17_] = 0;
			}
			class98_sub22.position = i_10_;
			int i_18_ = 0;
			for (int i_19_ = 0; i_19_ < i_11_; i_19_++) {
				int i_20_ = 0;
				for (int i_21_ = 0; i_21_ < i_3_; i_21_++) {
					i_20_ += class98_sub22.readInt(-2);
					ArrayUtils.method2894(is_8_, i_18_, is_16_[i_21_], is_12_[i_21_], i_20_);
					i_18_ += i_20_;
					is_12_[i_21_] += i_20_;
				}
			}
			for (int i_22_ = 0; (i_22_ ^ 0xffffffff) > (i_3_ ^ 0xffffffff); i_22_++) {
				int i_23_;
				if (is_4_ == null) {
					i_23_ = i_22_;
				} else {
					i_23_ = is_4_[i_22_];
				}
				if ((discardUnpacked ^ 0xffffffff) != -1) {
					objects[i_23_] = is_16_[i_22_];
				} else {
					objects[i_23_] = GroundBlendingPreferenceField.method654(2, is_16_[i_22_], false);
				}
			}
		} else {
			int i_24_ = is_8_.length;
			int i_25_ = is_8_[--i_24_] & 0xff;
			i_24_ -= 4 * i_25_ * i_3_;
			RSByteBuffer class98_sub22 = new RSByteBuffer(is_8_);
			int i_26_ = 0;
			class98_sub22.position = i_24_;
			int i_27_ = 0;
			for (int i_28_ = 0; i_25_ > i_28_; i_28_++) {
				int i_29_ = 0;
				for (int i_30_ = 0; i_30_ < i_3_; i_30_++) {
					i_29_ += class98_sub22.readInt(-2);
					int i_31_;
					if (is_4_ != null) {
						i_31_ = is_4_[i_30_];
					} else {
						i_31_ = i_30_;
					}
					if (i_31_ == i) {
						i_26_ += i_29_;
						i_27_ = i_31_;
					}
				}
			}
			if ((i_26_ ^ 0xffffffff) == -1) {
				return true;
			}
			byte[] is_32_ = new byte[i_26_];
			class98_sub22.position = i_24_;
			i_26_ = 0;
			int i_33_ = 0;
			for (int i_34_ = 0; i_34_ < i_25_; i_34_++) {
				int i_35_ = 0;
				for (int i_36_ = 0; i_36_ < i_3_; i_36_++) {
					i_35_ += class98_sub22.readInt(-2);
					int i_37_;
					if (is_4_ != null) {
						i_37_ = is_4_[i_36_];
					} else {
						i_37_ = i_36_;
					}
					if (i == i_37_) {
						ArrayUtils.method2894(is_8_, i_33_, is_32_, i_26_, i_35_);
						i_26_ += i_35_;
					}
					i_33_ += i_35_;
				}
			}
			objects[i_27_] = is_32_;
		}
		return true;
	}
}

/* Class98_Sub46_Sub13 - Decompiled by JODE
 */ package com.jagex.game.client.archive; /*
											*/

import com.Class116;
import com.Class186;
import com.Class303;
import com.Class308;
import com.Class98_Sub4;
import com.Class98_Sub41;
import com.Class98_Sub46_Sub10;
import com.Class98_Sub46_Sub20_Sub2;
import com.Exception_Sub1;
import com.Mob;
import com.NodeString;
import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.toolkit.font.Font;

public abstract class FileRequest extends Cacheable {
	public static final void method1592(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		Class98_Sub4.anInt3828 = i_3_;
		NodeString.anInt3915 = i_4_;
		if (i == -25686) {
			Class98_Sub41.anInt4202 = i_0_;
			Class303.anInt2530 = i_2_;
			Exception_Sub1.anInt44 = i_1_;
			if (Class98_Sub4.anInt3828 >= 100) {
				int i_5_ = 256 + Exception_Sub1.anInt44 * 512;
				int i_6_ = NodeString.anInt3915 * 512 + 256;
				int i_7_ = StrongReferenceMCNode.getHeight(Font.localPlane, i_6_, i_5_, 24111) + -Class303.anInt2530;
				int i_8_ = i_5_ + -Class98_Sub46_Sub10.xCamPosTile;
				int i_9_ = -AdvancedMemoryCache.anInt601 + i_7_;
				int i_10_ = -SpriteLoadingScreenElement.yCamPosTile + i_6_;
				int i_11_ = (int) Math.sqrt(i_8_ * i_8_ + i_10_ * i_10_);
				Mob.anInt6357 = 0x3fff & (int) (2607.5945876176133 * Math.atan2(i_9_, i_11_));
				Class186.cameraX = 0x3fff & (int) (Math.atan2(i_8_, i_10_) * -2607.5945876176133);
				Class308.anInt2584 = 0;
				if ((Mob.anInt6357 ^ 0xffffffff) > -1025) {
					Mob.anInt6357 = 1024;
				}
				if ((Mob.anInt6357 ^ 0xffffffff) < -3073) {
					Mob.anInt6357 = 3072;
				}
			}
			Class98_Sub46_Sub20_Sub2.cameraMode = 2;
			Class116.anInt967 = CharacterShadowsPreferenceField.anInt3712 = -1;
		}
	}

	public boolean			aBoolean6036;
	public boolean			aBoolean6037;

	public volatile boolean	inProgress	= true;

	public FileRequest() {
		/* empty */
	}

	public abstract byte[] getData(int i);

	public abstract int getProgress(int i);
}

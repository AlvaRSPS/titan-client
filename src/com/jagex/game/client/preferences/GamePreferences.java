
/* GamePreferences - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import java.io.IOException;
import java.lang.reflect.Field;

import com.ChatStatus;
import com.Class151_Sub1;
import com.Class159;
import com.Class165;
import com.Class170;
import com.Class21_Sub2;
import com.Class246_Sub3_Sub4_Sub5;
import com.OpenGlElementBufferPointer;
import com.Class33;
import com.Class349;
import com.Class372;
import com.WhirlpoolGenerator;
import com.Class53_Sub1;
import com.Class98_Sub10_Sub34;
import com.Class98_Sub10_Sub7;
import com.ClipMap;
import com.FileOnDisk;
import com.GameDefinition;
import com.GameShell;
import com.OutgoingOpcode;
import com.OutputStream_Sub2;
import com.RSByteBuffer;
import com.SignLink;
import com.SignLinkRequest;
import com.SystemInformation;
import com.client;
import com.jagex.core.collections.Node;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5FileRequest;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;

public final class GamePreferences extends Node {
	public static byte[]			aByteArray4075	= new byte[2048];
	public static OutgoingOpcode	aClass171_4045	= new OutgoingOpcode(59, 2);
	public static ChatStatus		FRIENDS			= new ChatStatus(1);
	public static int				anInt4060;
	/* synthetic */ static Class	component;

	/* synthetic */ static Class getClassByName(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public static final GamePreferences loadPreferences() {
		FileOnDisk fileOnDisk = null;
		GamePreferences preferences = new GamePreferences(client.game, 0);
		try {
			SignLinkRequest request = GameShell.signLink.openPreferences("", true, 21516);
			while ((request.status ^ 0xffffffff) == -1) {
				TimeTools.sleep(0, 1L);
			}
			if (request.status == 1) {
				fileOnDisk = (FileOnDisk) request.result;
				byte[] buffer = new byte[(int) fileOnDisk.method3878((byte) -112)];
				int readLength;
				for (int readPosition = 0; (buffer.length ^ 0xffffffff) < (readPosition ^ 0xffffffff); readPosition += readLength) {
					readLength = fileOnDisk.method3879(buffer.length - readPosition, (byte) -26, readPosition, buffer);
					if (readLength == -1) {
						throw new IOException("EOF");
					}
				}
				preferences = new GamePreferences(new RSByteBuffer(buffer), client.game, 0);
			}
		} catch (Exception exception) {
			/* empty */
		}
		try {
			if (fileOnDisk != null) {
				fileOnDisk.close(true);
			}
		} catch (Exception exception) {
			/* empty */
		}
		return preferences;
	}

	public static void method1280(boolean bool) {
		do {
			try {
				FRIENDS = null;
				aByteArray4075 = null;
				aClass171_4045 = null;
				if (bool == true) {
					break;
				}
				anInt4060 = 95;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "kda.L(" + bool + ')');
			}
			break;
		} while (false);
	}

	public static final void method1284(int i) {
		try {
			client.preferences.setPreference((byte) -13, 1, client.preferences.aClass64_Sub3_4041);
			client.preferences.setPreference((byte) -13, 1, client.preferences.aClass64_Sub3_4076);
			client.preferences.setPreference((byte) -13, 2, client.preferences.removeRoofs);
			client.preferences.setPreference((byte) -13, 2, client.preferences.removeRoofsMode);
			client.preferences.setPreference((byte) -13, 1, client.preferences.groundDecoration);
			client.preferences.setPreference((byte) -13, 1, client.preferences.groundBlending);
			client.preferences.setPreference((byte) -13, 1, client.preferences.idleAnimations);
			client.preferences.setPreference((byte) -13, 1, client.preferences.flickeringEffects);
			client.preferences.setPreference((byte) -13, i, client.preferences.characterShadows);
			client.preferences.setPreference((byte) -13, 1, client.preferences.textures);
			client.preferences.setPreference((byte) -13, 2, client.preferences.sceneryShadows);
			client.preferences.setPreference((byte) -13, 1, client.preferences.lightningDetail);
			client.preferences.setPreference((byte) -13, 2, client.preferences.waterDetail);
			client.preferences.setPreference((byte) -13, 1, client.preferences.fog);
			client.preferences.setPreference((byte) -13, 0, client.preferences.antiAliasing);
			client.preferences.setPreference((byte) -13, 0, client.preferences.multiSample);
			client.preferences.setPreference((byte) -13, 2, client.preferences.particles);
			client.preferences.setPreference((byte) -13, 0, client.preferences.buildArea);
			client.preferences.setPreference((byte) -13, 0, client.preferences.unknown3);
			Class151_Sub1.method2450((byte) 106);
			client.preferences.setPreference((byte) -13, 0, client.preferences.maxScreenSize);
			client.preferences.setPreference((byte) -13, 4, client.preferences.graphicsLevel);
			Js5FileRequest.method1593((byte) 110);
			WhirlpoolGenerator.method3980((byte) 125);
			Class33.aBoolean316 = true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kda.A(" + i + ')');
		}
	}

	public static final boolean method1292(int i, byte i_9_, int i_10_) {
		try {
			if (!Class53_Sub1.method502(i, i_10_, (byte) 116)) {
				return false;
			}
			if ((i_10_ & 0x9000 ^ 0xffffffff) != -1 | Class349.method3842(i, i_10_, -18021) | RSByteBuffer.method1241(false, i, i_10_)) {
				return true;
			}
			if (i_9_ < 110) {
				aClass171_4045 = null;
			}
			return ((0x2000 & i_10_ ^ 0xffffffff) != -1 | SpriteProgressBarLoadingScreenElement.method3978(i, i_10_, (byte) 74) | Class21_Sub2.method271((byte) -112, i_10_, i)) & (i & 0x37) == 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kda.G(" + i + ',' + i_9_ + ',' + i_10_ + ')');
		}
	}

	public static final void setSize(byte i, int size) {
		if (size != RSByteBuffer.anInt3994) {
			Class165.mapWidth = Class98_Sub10_Sub7.mapLength = Class246_Sub3_Sub4_Sub5.mapSizes[size];
			// System.out.println("MapWidth: " + Class165.mapWidth + " A: " +
			// Arrays.toString(Class246_Sub3_Sub4_Sub5.mapSizes));
			Class98_Sub10_Sub34.method1104(112);
			Class170.regionData = new int[4][Class165.mapWidth >> 3][Class98_Sub10_Sub7.mapLength >> 3];
			Class372.anIntArrayArray3149 = new int[Class165.mapWidth][Class98_Sub10_Sub7.mapLength];
			WaterDetailPreferenceField.anIntArrayArray3719 = new int[Class165.mapWidth][Class98_Sub10_Sub7.mapLength];
			for (int i_5_ = 0; (i_5_ ^ 0xffffffff) > -5; i_5_++) {
				VarPlayerDefinition.clipMaps[i_5_] = ClipMap.create(2742, Class98_Sub10_Sub7.mapLength, Class165.mapWidth);
			}
			OutputStream_Sub2.aByteArrayArrayArray41 = new byte[4][Class165.mapWidth][Class98_Sub10_Sub7.mapLength];
			OpenGlElementBufferPointer.method3673(Class98_Sub10_Sub7.mapLength, Class165.mapWidth, 4, true);
			Class159.method2508(Class165.mapWidth >> 2012820739, Class98_Sub10_Sub7.mapLength >> 1345943043, (byte) -50, client.graphicsToolkit);
			RSByteBuffer.anInt3994 = size;
		}
	}

	public Class64_Sub3							aClass64_Sub3_4041;
	public Class64_Sub3							aClass64_Sub3_4076;
	public AntialiasPreferenceField				antiAliasing;
	public VolumePreferenceField				areaSoundsVolume;
	public BrightnessPreferenceField			brightnessLevel;
	public BuildAreaPreferenceField				buildArea;
	public CharacterShadowsPreferenceField		characterShadows;
	public CpuUsagePreferenceField				cpuUsage;
	public ScreenSizePreferenceField			currentScreenSize;
	public ToolkitPreferenceField				currentToolkit;
	public ToolkitPreferenceField				desiredToolkit;
	public FlickeringEffectsPreferenceField		flickeringEffects;
	public FogPreferenceField					fog;
	public GameDefinition						gameType;
	public VolumePreferenceField				generalMusicVolume;
	public GraphicsLevelPreferenceField			graphicsLevel;
	public GroundBlendingPreferenceField		groundBlending;
	public GroundDecorationPreferenceField		groundDecoration;
	public IdleAnimationsPreferenceField		idleAnimations;
	public LightningDetailPreferenceField		lightningDetail;
	public LoadingScreenSequencePreferenceField	loadingScreenSequence;
	public MaxScreenSizePreferenceField			maxScreenSize;
	public MonoOrStereoPreferenceField			monoOrStereo;
	public AntialiasPreferenceField				multiSample;
	public VolumePreferenceField				musicVolume;
	public OrthoZoomPreferenceField				orthoZoom;
	public ParticlesPreferenceField				particles;
	public RemoveRoofsPreferenceField			removeRoofs;
	public RemoveRoofsPreferenceField			removeRoofsMode;
	public Class64_Sub5							unknown3;
	public SceneryShadowsPreferenceField		sceneryShadows;
	public ScreenSizePreferenceField			screenSize;
	public VolumePreferenceField				soundEffectsVolume;
	public SystemInformation					systemInformation;
	public TexturesPreferenceField				textures;
	public Class64_Sub9							unknown;
	public SafeModePreferenceField				safeMode;
	public UseCustomCursorPreferenceField		useCustomCursor;
	public VolumePreferenceField				voiceOverVolume;
	public WaterDetailPreferenceField			waterDetail;

	GamePreferences(GameDefinition game, int i) {
		gameType = game;
		systemInformation = new SystemInformation(GameShell.signLink.is_signed, GameShell.maxHeap, GameShell.availableProcessors, SignLink.osArch.toLowerCase().indexOf("arm") != -1);
		currentToolkit = new ToolkitPreferenceField(i, this);
		setDefaults(true, 0);
	}

	GamePreferences(RSByteBuffer class98_sub22, GameDefinition class279, int i) {
		gameType = class279;
		systemInformation = new SystemInformation(GameShell.signLink.is_signed, GameShell.maxHeap, GameShell.availableProcessors, (SignLink.osArch.indexOf("arm") ^ 0xffffffff) != 0);
		currentToolkit = new ToolkitPreferenceField(i, this);
		read(class98_sub22, 10148);
	}

	public final RSByteBuffer encode(boolean bool) {
		RSByteBuffer packet = new RSByteBuffer(LightIntensityDefinitionParser.method3270(97));
		packet.writeByte(24, -51);
		packet.writeByte(antiAliasing.getValue((byte) 125), 126);
		packet.writeByte(unknown3.getValue((byte) 122), 92);// TODO:
		packet.writeByte(brightnessLevel.getValue((byte) 123), 48);
		packet.writeByte(buildArea.getValue((byte) 126), -116);
		packet.writeByte(flickeringEffects.getValue((byte) 122), 85);
		packet.writeByte(fog.getValue((byte) 124), -44);
		packet.writeByte(groundBlending.getValue((byte) 121), 124);
		packet.writeByte(groundDecoration.getValue((byte) 121), -49);
		packet.writeByte(idleAnimations.getValue((byte) 123), 114);
		packet.writeByte(lightningDetail.getValue((byte) 122), -68);
		packet.writeByte(sceneryShadows.getValue((byte) 122), -124);
		packet.writeByte(orthoZoom.getValue((byte) 122), -77);
		packet.writeByte(particles.getValue((byte) 122), 40);
		packet.writeByte(removeRoofs.getValue((byte) 122), -112);
		packet.writeByte(maxScreenSize.getValue((byte) 120), 77);
		packet.writeByte(characterShadows.getValue((byte) 122), -99);
		packet.writeByte(unknown.getValue((byte) 121), -86);// TODO:
		packet.writeByte(textures.getValue((byte) 127), -119);
		packet.writeByte(desiredToolkit.getValue((byte) 125), 47);
		packet.writeByte(aClass64_Sub3_4041.getValue((byte) 124), -103);// TODO:
		packet.writeByte(waterDetail.getValue((byte) 122), -104);
		packet.writeByte(screenSize.getValue((byte) 125), 43);
		packet.writeByte(useCustomCursor.getValue((byte) 127), 50);
		packet.writeByte(graphicsLevel.getValue((byte) 122), 66);
		packet.writeByte(cpuUsage.getValue((byte) 126), -53);
		packet.writeByte(loadingScreenSequence.getValue((byte) 120), 115);
		packet.writeByte(safeMode.getValue((byte) 126), 111);
		packet.writeByte(soundEffectsVolume.getValue((byte) 124), -81);
		packet.writeByte(areaSoundsVolume.getValue((byte) 120), 47);
		packet.writeByte(voiceOverVolume.getValue((byte) 125), -54);// Or
																	// themeMusicVolume?
		packet.writeByte(musicVolume.getValue((byte) 122), -101);
		packet.writeByte(generalMusicVolume.getValue((byte) 121), 118);
		packet.writeByte(monoOrStereo.getValue((byte) 121), -97);
		return packet;
	}

	public final GameDefinition getGameType(byte i) {
		return gameType;
	}

	public final SystemInformation getSystemInformation(int i) {
		return systemInformation;
	}

	private final void method1281(RSByteBuffer packet, int i, int i_0_) {
		do {
			brightnessLevel = new BrightnessPreferenceField(packet.readUnsignedByte((byte) -109), this);
			aClass64_Sub3_4041 = new Class64_Sub3(packet.readUnsignedByte((byte) 107), this);
			removeRoofs = new RemoveRoofsPreferenceField(1 + packet.readUnsignedByte((byte) 3), this);
			groundDecoration = new GroundDecorationPreferenceField(packet.readUnsignedByte((byte) 14), this);
			unknown = new Class64_Sub9(packet.readUnsignedByte((byte) 29), this);
			idleAnimations = new IdleAnimationsPreferenceField(packet.readUnsignedByte((byte) -119), this);
			flickeringEffects = new FlickeringEffectsPreferenceField(packet.readUnsignedByte((byte) 53), this);
			packet.readUnsignedByte((byte) 5);
			characterShadows = new CharacterShadowsPreferenceField(packet.readUnsignedByte((byte) -113), this);
			int i_1_ = packet.readUnsignedByte((byte) -5);
			int i_2_ = 0;
			if ((i ^ 0xffffffff) <= -18) {
				i_2_ = packet.readUnsignedByte((byte) -114);
			}
			sceneryShadows = new SceneryShadowsPreferenceField((i_2_ ^ 0xffffffff) > (i_1_ ^ 0xffffffff) ? i_1_ : i_2_, this);
			boolean bool = true;
			boolean bool_3_ = true;
			if ((i ^ 0xffffffff) <= -3) {
				bool = (packet.readUnsignedByte((byte) 77) ^ 0xffffffff) == -2;
				if (i >= 17) {
					bool_3_ = packet.readUnsignedByte((byte) -128) == 1;
				}
			} else {
				bool = (packet.readUnsignedByte((byte) -112) ^ 0xffffffff) == -2;
				packet.readUnsignedByte((byte) -98);
			}
			lightningDetail = new LightningDetailPreferenceField(!(bool | bool_3_) ? 0 : 1, this);
			waterDetail = new WaterDetailPreferenceField(packet.readUnsignedByte((byte) -126), this);
			fog = new FogPreferenceField(packet.readUnsignedByte((byte) -101), this);
			antiAliasing = new AntialiasPreferenceField(packet.readUnsignedByte((byte) 11), this);
			monoOrStereo = new MonoOrStereoPreferenceField(packet.readUnsignedByte((byte) 80), this);
			soundEffectsVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) -119), this);
			if (i >= 20) {
				voiceOverVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) -115), this);
			} else {
				voiceOverVolume = new VolumePreferenceField(soundEffectsVolume.getValue((byte) 124), this);
			}
			musicVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) 81), this);
			areaSoundsVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) 10), this);
			if ((i ^ 0xffffffff) <= -22) {
				generalMusicVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) 42), this);
			} else {
				generalMusicVolume = new VolumePreferenceField(musicVolume.getValue((byte) 125), this);
			}
			if (i >= 1) {
				packet.readShort((byte) 127);
				packet.readShort((byte) 127);
			}
			if ((i ^ 0xffffffff) <= -4 && i < 6) {
				packet.readUnsignedByte((byte) -114);
			}
			if (i_0_ != 4311) {
				method1281(null, -107, -6);
			}
			if ((i ^ 0xffffffff) <= -5) {
				particles = new ParticlesPreferenceField(packet.readUnsignedByte((byte) 35), this);
			}
			packet.readInt(-2);
			if (i >= 6) {
				screenSize = new ScreenSizePreferenceField(packet.readUnsignedByte((byte) 123), this);
			}
			if ((i ^ 0xffffffff) <= -8) {
				safeMode = new SafeModePreferenceField(packet.readUnsignedByte((byte) 35), this);
			}
			if ((i ^ 0xffffffff) <= -9) {
				packet.readUnsignedByte((byte) -120);
			}
			if ((i ^ 0xffffffff) <= -10) {
				buildArea = new BuildAreaPreferenceField(packet.readUnsignedByte((byte) -121), this);
			}
			if (i >= 10) {
				unknown3 = new Class64_Sub5(packet.readUnsignedByte((byte) 79), this);
			}
			if (i >= 11) {
				useCustomCursor = new UseCustomCursorPreferenceField(packet.readUnsignedByte((byte) 47), this);
			}
			if ((i ^ 0xffffffff) <= -13) {
				idleAnimations = new IdleAnimationsPreferenceField(packet.readUnsignedByte((byte) -115), this);
			}
			if ((i ^ 0xffffffff) <= -14) {
				groundBlending = new GroundBlendingPreferenceField(packet.readUnsignedByte((byte) 39), this);
			}
			if ((i ^ 0xffffffff) <= -15) {
				desiredToolkit = new ToolkitPreferenceField(packet.readUnsignedByte((byte) -115), this);
			}
			if ((i ^ 0xffffffff) <= -16) {
				cpuUsage = new CpuUsagePreferenceField(packet.readUnsignedByte((byte) -115), this);
			}
			if (i >= 16) {
				textures = new TexturesPreferenceField(packet.readUnsignedByte((byte) 24), this);
			}
			if ((i ^ 0xffffffff) <= -19) {
				graphicsLevel = new GraphicsLevelPreferenceField(packet.readUnsignedByte((byte) -100), this);
			}
			if (i >= 19) {
				maxScreenSize = new MaxScreenSizePreferenceField(packet.readUnsignedByte((byte) -110), this);
			}
			if ((i ^ 0xffffffff) > -23) {
				break;
			}
			loadingScreenSequence = new LoadingScreenSequencePreferenceField(packet.readUnsignedByte((byte) 120), this);
			break;
		} while (false);
	}

	public final boolean method1291(byte i) {
		return !(!currentToolkit.isLive(true) || currentToolkit.getValue((byte) 122) != 0 || systemInformation.getMaxHeapMb(-1) >= 96);
	}

	private final void read(RSByteBuffer packet, int i) {
		if (packet != null && packet.payload != null) {
			int version = packet.readUnsignedByte((byte) -127);
			if (version < 23) {
				try {
					method1281(packet, version, 4311);
				} catch (Exception exception) {
					setDefaults(true, i ^ 0x27a4);
				}
				setDefaults(false, 0);
			} else if ((version ^ 0xffffffff) < -25) {
				setDefaults(true, i + -10148);
			} else {
				antiAliasing = new AntialiasPreferenceField(packet.readUnsignedByte((byte) 92), this);
				multiSample = new AntialiasPreferenceField(antiAliasing.getValue((byte) 124), this);
				unknown3 = new Class64_Sub5(packet.readUnsignedByte((byte) 74), this);
				brightnessLevel = new BrightnessPreferenceField(packet.readUnsignedByte((byte) 74), this);
				buildArea = new BuildAreaPreferenceField(packet.readUnsignedByte((byte) 74), this);
				flickeringEffects = new FlickeringEffectsPreferenceField(packet.readUnsignedByte((byte) 104), this);
				fog = new FogPreferenceField(packet.readUnsignedByte((byte) -127), this);
				groundBlending = new GroundBlendingPreferenceField(packet.readUnsignedByte((byte) 34), this);
				groundDecoration = new GroundDecorationPreferenceField(packet.readUnsignedByte((byte) -6), this);
				idleAnimations = new IdleAnimationsPreferenceField(packet.readUnsignedByte((byte) -107), this);
				lightningDetail = new LightningDetailPreferenceField(packet.readUnsignedByte((byte) -109), this);
				sceneryShadows = new SceneryShadowsPreferenceField(packet.readUnsignedByte((byte) 26), this);
				if ((version ^ 0xffffffff) <= -25) {
					orthoZoom = new OrthoZoomPreferenceField(packet.readUnsignedByte((byte) 26), this);
				}
				particles = new ParticlesPreferenceField(packet.readUnsignedByte((byte) -8), this);
				removeRoofs = new RemoveRoofsPreferenceField(packet.readUnsignedByte((byte) -124), this);
				removeRoofsMode = new RemoveRoofsPreferenceField(removeRoofs.getValue((byte) 123), this);
				maxScreenSize = new MaxScreenSizePreferenceField(packet.readUnsignedByte((byte) 72), this);
				characterShadows = new CharacterShadowsPreferenceField(packet.readUnsignedByte((byte) 4), this);
				unknown = new Class64_Sub9(packet.readUnsignedByte((byte) 71), this);
				textures = new TexturesPreferenceField(packet.readUnsignedByte((byte) 36), this);
				desiredToolkit = new ToolkitPreferenceField(packet.readUnsignedByte((byte) 76), this);
				currentToolkit = new ToolkitPreferenceField(desiredToolkit.getValue((byte) 122), this);
				aClass64_Sub3_4041 = new Class64_Sub3(packet.readUnsignedByte((byte) -99), this);
				aClass64_Sub3_4076 = new Class64_Sub3(aClass64_Sub3_4041.getValue((byte) 122), this);
				waterDetail = new WaterDetailPreferenceField(packet.readUnsignedByte((byte) 67), this);
				screenSize = new ScreenSizePreferenceField(packet.readUnsignedByte((byte) -125), this);
				currentScreenSize = new ScreenSizePreferenceField(screenSize.getValue((byte) 127), this);
				useCustomCursor = new UseCustomCursorPreferenceField(packet.readUnsignedByte((byte) -107), this);
				graphicsLevel = new GraphicsLevelPreferenceField(packet.readUnsignedByte((byte) -106), this);
				cpuUsage = new CpuUsagePreferenceField(packet.readUnsignedByte((byte) -124), this);
				loadingScreenSequence = new LoadingScreenSequencePreferenceField(packet.readUnsignedByte((byte) 99), this);
				safeMode = new SafeModePreferenceField(packet.readUnsignedByte((byte) 114), this);
				soundEffectsVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) 127), this);
				areaSoundsVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) -118), this);
				voiceOverVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) 74), this);
				musicVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) -102), this);
				generalMusicVolume = new VolumePreferenceField(packet.readUnsignedByte((byte) 111), this);
				monoOrStereo = new MonoOrStereoPreferenceField(packet.readUnsignedByte((byte) -123), this);
				setDefaults(false, i ^ 0x27a4);
			}
		} else {
			setDefaults(true, 0);
		}
		validateAllFields(false);
	}

	private final void setDefaults(boolean defaults, int i) {
		if (defaults || antiAliasing == null) {
			antiAliasing = new AntialiasPreferenceField(this);
		}
		if (defaults || multiSample == null) {
			multiSample = new AntialiasPreferenceField(antiAliasing.getValue((byte) 125), this);
		}
		if (defaults || unknown3 == null) {
			unknown3 = new Class64_Sub5(this);
		}
		if (defaults || brightnessLevel == null) {
			brightnessLevel = new BrightnessPreferenceField(this);
		}
		if (defaults || buildArea == null) {
			buildArea = new BuildAreaPreferenceField(this);
		}
		if (defaults || flickeringEffects == null) {
			flickeringEffects = new FlickeringEffectsPreferenceField(this);
		}
		if (defaults || fog == null) {
			fog = new FogPreferenceField(this);
		}
		if (defaults || groundBlending == null) {
			groundBlending = new GroundBlendingPreferenceField(this);
		}
		if (defaults || groundDecoration == null) {
			groundDecoration = new GroundDecorationPreferenceField(this);
		}
		if (defaults || idleAnimations == null) {
			idleAnimations = new IdleAnimationsPreferenceField(this);
		}
		if (defaults || lightningDetail == null) {
			lightningDetail = new LightningDetailPreferenceField(this);
		}
		if (defaults || sceneryShadows == null) {
			sceneryShadows = new SceneryShadowsPreferenceField(this);
		}
		if (defaults || orthoZoom == null) {
			orthoZoom = new OrthoZoomPreferenceField(this);
		}
		if (defaults || particles == null) {
			particles = new ParticlesPreferenceField(this);
		}
		if (defaults || removeRoofs == null) {
			removeRoofs = new RemoveRoofsPreferenceField(this);
		}
		if (defaults || removeRoofsMode == null) {
			removeRoofsMode = new RemoveRoofsPreferenceField(removeRoofs.getValue((byte) 120), this);
		}
		if (defaults || maxScreenSize == null) {
			maxScreenSize = new MaxScreenSizePreferenceField(this);
		}
		if (defaults || characterShadows == null) {
			characterShadows = new CharacterShadowsPreferenceField(this);
		}
		if (defaults || unknown == null) {
			unknown = new Class64_Sub9(this);
		}
		if (defaults || textures == null) {
			textures = new TexturesPreferenceField(this);
		}
		if (defaults || desiredToolkit == null) {
			desiredToolkit = new ToolkitPreferenceField(this);
		}
		if (defaults || currentToolkit == null) {
			currentToolkit = new ToolkitPreferenceField(desiredToolkit.getValue((byte) 125), this);
		}
		if (defaults || aClass64_Sub3_4041 == null) {
			aClass64_Sub3_4041 = new Class64_Sub3(this);
		}
		if (defaults || aClass64_Sub3_4076 == null) {
			aClass64_Sub3_4076 = new Class64_Sub3(aClass64_Sub3_4041.getValue((byte) 121), this);
		}
		if (defaults || waterDetail == null) {
			waterDetail = new WaterDetailPreferenceField(this);
		}
		if (defaults || screenSize == null) {
			screenSize = new ScreenSizePreferenceField(this);
		}
		if (defaults || currentScreenSize == null) {
			currentScreenSize = new ScreenSizePreferenceField(screenSize.getValue((byte) 127), this);
		}
		if (defaults || useCustomCursor == null) {
			useCustomCursor = new UseCustomCursorPreferenceField(this);
		}
		if (defaults || graphicsLevel == null) {
			graphicsLevel = new GraphicsLevelPreferenceField(this);
		}
		if (defaults || cpuUsage == null) {
			cpuUsage = new CpuUsagePreferenceField(this);
		}
		if (defaults || loadingScreenSequence == null) {
			loadingScreenSequence = new LoadingScreenSequencePreferenceField(this);
		}
		if (i == 0) {
			if (defaults || safeMode == null) {
				safeMode = new SafeModePreferenceField(this);
			}
			if (defaults || soundEffectsVolume == null) {
				soundEffectsVolume = new VolumePreferenceField(this);
			}
			if (defaults || areaSoundsVolume == null) {
				areaSoundsVolume = new VolumePreferenceField(this);
			}
			if (defaults || voiceOverVolume == null) {
				voiceOverVolume = new VolumePreferenceField(this);
			}
			if (defaults || musicVolume == null) {
				musicVolume = new VolumePreferenceField(this);
			}
			if (defaults || generalMusicVolume == null) {
				generalMusicVolume = new VolumePreferenceField(this);
			}
			if (defaults || monoOrStereo == null) {
				monoOrStereo = new MonoOrStereoPreferenceField(this);
			}
		}
	}

	public final void setPreference(byte i, int toolkit, IntegerPreferenceField preference) {
		if (i != -13) {
			buildArea = null;
		}
		preference.setValue(toolkit, i + 9861);
		validateAllFields(false);
	}

	private final void validateAllFields(boolean bool) {
		try {
			Field[] fields = this.getClass().getDeclaredFields();
			Field[] fields_7_ = fields;
			for (Field field : fields_7_) {
				if ((component != null ? component : (component = getClassByName("com.jagex.game.client.preferences.IntegerPreferenceField"))).isAssignableFrom(field.getType())) {
					IntegerPreferenceField preferenceField = (IntegerPreferenceField) field.get(this);
					preferenceField.validate((byte) 119);
				}
			}
		} catch (IllegalAccessException illegalaccessexception) {
			illegalaccessexception.printStackTrace();
		}
	}
}

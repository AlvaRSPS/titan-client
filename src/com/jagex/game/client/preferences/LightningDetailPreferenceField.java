/* Class64_Sub10 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class85;
import com.IncomingOpcode;
import com.jagex.game.client.archive.Js5Exception;

public final class LightningDetailPreferenceField extends IntegerPreferenceField {
	public static IncomingOpcode	aClass58_3665;
	public static Class85			aClass85_3667;
	public static int				anInt3664;
	public static int				anInt3666	= -1;

	static {
		anInt3664 = -1;
		aClass58_3665 = new IncomingOpcode(64, 6);
		aClass85_3667 = new Class85(12, -1);
	}

	public static void method593(int i) {
		try {
			aClass58_3665 = null;
			aClass85_3667 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fe.G(" + i + ')');
		}
	}

	public static final boolean method594(int i, int i_4_, int i_5_) {
		if (i_4_ != 6) {
			return true;
		}
		return (i & 0x100100 ^ 0xffffffff) != -1;
	}

	LightningDetailPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	LightningDetailPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_2_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_0_) {
		value = i_0_;
	}

	@Override
	public final void validate(byte i) {
		do {
			if ((value ^ 0xffffffff) == -2 || (value ^ 0xffffffff) == -1) {
				break;
			}
			value = getDefaultValue(0);
			break;
		} while (false);
	}
}

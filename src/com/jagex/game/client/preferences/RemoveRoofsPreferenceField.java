/* Class64_Sub15 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class98_Sub10_Sub35;
import com.IncomingOpcode;
import com.jagex.game.client.archive.Js5;

public final class RemoveRoofsPreferenceField extends IntegerPreferenceField {
	public static Js5				aClass207_3679;
	public static int				anInt3676	= 0;
	public static int				anInt3678;
	public static IncomingOpcode	SEND_AMASK	= new IncomingOpcode(119, 12);

	public static final int[] method610(int i, int i_0_, float f, int i_1_, boolean bool, boolean bool_2_, int i_3_, int i_4_) {
		int[] is = new int[i_0_];
		Class98_Sub10_Sub35 class98_sub10_sub35 = new Class98_Sub10_Sub35();
		class98_sub10_sub35.anInt5733 = i_1_;
		class98_sub10_sub35.anInt5739 = (int) (f * 4096.0F);
		class98_sub10_sub35.aBoolean5731 = bool;
		class98_sub10_sub35.anInt5737 = i_4_;
		class98_sub10_sub35.anInt5734 = i_3_;
		class98_sub10_sub35.anInt5740 = i;
		class98_sub10_sub35.method1001((byte) 66);
		SafeModePreferenceField.method559(bool_2_, i_0_, 1);
		class98_sub10_sub35.method1107((byte) -76, is, 0);
		return is;
	}

	RemoveRoofsPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	RemoveRoofsPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_5_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {

		return 2;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_6_) {
		value = i_6_;
	}

	@Override
	public final void validate(byte i) {
		if (preferenceStore.orthoZoom.supported((byte) -123) && value == 2) {
			value = 1;
		}
		if (i >= 118) {
			if (value < 0 || value > 2) {
				value = getDefaultValue(0);
			}
		}
	}
}

/* Class64_Sub17 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class151_Sub1;
import com.Class3;
import com.Class333;
import com.Class76_Sub8;
import com.Class96;
import com.Class98_Sub10_Sub38;
import com.GZipDecompressor;
import com.GameShell;
import com.SceneGraphNodeList;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.AnimationDefinition;

public final class BuildAreaPreferenceField extends IntegerPreferenceField {
	public static float	aFloat3686;
	public static int	anInt3684		= 0;
	public static int[]	anIntArray3685	= { 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3 };
	public static Js5	glyphsStore;

	public static final void method618(int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		int i_7_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_6_);
		int i_8_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i);
		int i_9_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_2_);
		int i_10_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_4_);
		if (i_3_ != -21327) {
			glyphsStore = null;
		}
		for (int i_11_ = i_7_; i_8_ >= i_11_; i_11_++) {
			Class333.method3761(i_5_, AnimationDefinition.anIntArrayArray814[i_11_], i_9_, i_10_, (byte) 13);
		}
	}

	public static final void method620(int i, int i_26_, boolean bool, int i_27_, int i_28_) {
		do {
			if (-i_26_ + i_27_ < Class76_Sub8.anInt3778 || i_26_ + i_27_ > Class3.anInt77 || Class98_Sub10_Sub38.anInt5753 > i - i_26_ || i - -i_26_ > SceneGraphNodeList.anInt1635) {
				Class96.method924((byte) -109, i, i_26_, i_27_, i_28_);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			Class151_Sub1.method2446((byte) -127, i_27_, i_28_, i_26_, i);
		} while (false);
	}

	BuildAreaPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	BuildAreaPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_24_) {
		int i_25_ = preferenceStore.getSystemInformation(-108).getMaxHeapMb(-1);
		if (i_25_ < 96) {
			return 3;
		}
		if (i > 1 && i_25_ < 128) {
			return 3;
		}
		if (i_24_ != 29053) {
			method621(125);
		}
		if (i > 3 && i_25_ < 192) {
			return 3;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 0;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method621(int i) {
		int i_29_ = preferenceStore.getSystemInformation(-103).getMaxHeapMb(i);
		return (i_29_ ^ 0xffffffff) <= -97;
	}

	@Override
	public final void setNewValue(int i, int i_12_) {
		value = i_12_;
	}

	@Override
	public final void validate(byte i) {
		int i_1_ = preferenceStore.getSystemInformation(-101).getMaxHeapMb(-1);
		if (i_1_ < 96) {
			value = 0;
		}
		if (i < 118) {
			aFloat3686 = 0.5277947F;
		}
		if (value > 1 && (i_1_ ^ 0xffffffff) > -129) {
			value = 1;
		}
		if (value > 2 && i_1_ < 192) {
			value = 2;
		}
		if (value < 0 || (value ^ 0xffffffff) < -4) {
			value = getDefaultValue(0);
		}
	}
}

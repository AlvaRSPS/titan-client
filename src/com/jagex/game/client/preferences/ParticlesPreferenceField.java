/* Class64_Sub6 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class119_Sub4;
import com.Class224_Sub3_Sub1;
import com.Class42_Sub2;
import com.Class98_Sub10_Sub1;
import com.Class98_Sub43_Sub3;
import com.MapRegion;
import com.Minimap;
import com.RsFloatBuffer;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;

public final class ParticlesPreferenceField extends IntegerPreferenceField {
	public static boolean	aBoolean3656	= true;
	public static int		anInt3655;

	public static final void method573(int i, long l) {
		try {
			int i_0_ = StrongReferenceMCNode.anInt6295;
			if ((i_0_ ^ 0xffffffff) != (Minimap.cameraXPosition ^ 0xffffffff)) {
				int i_1_ = -Minimap.cameraXPosition + i_0_;
				int i_2_ = (int) (l * i_1_ / 320L);
				if (i_1_ <= 0) {
					if (i_2_ != 0) {
						if (i_1_ > i_2_) {
							i_2_ = i_1_;
						}
					} else {
						i_2_ = -1;
					}
				} else if (i_2_ == 0) {
					i_2_ = 1;
				} else if ((i_1_ ^ 0xffffffff) > (i_2_ ^ 0xffffffff)) {
					i_2_ = i_1_;
				}
				Minimap.cameraXPosition += i_2_;
			}
			int i_3_ = Js5Client.anInt1051;
			if (i == -1) {
				Class119_Sub4.aFloat4740 += 8.0F * (l * MapRegion.aFloat2545 / 40.0F);
				if ((i_3_ ^ 0xffffffff) != (Class224_Sub3_Sub1.cameraYPosition ^ 0xffffffff)) {
					int i_4_ = -Class224_Sub3_Sub1.cameraYPosition + i_3_;
					int i_5_ = (int) (l * i_4_ / 320L);
					if ((i_4_ ^ 0xffffffff) >= -1) {
						if (i_5_ == 0) {
							i_5_ = -1;
						} else if ((i_5_ ^ 0xffffffff) > (i_4_ ^ 0xffffffff)) {
							i_5_ = i_4_;
						}
					} else if (i_5_ != 0) {
						if (i_5_ > i_4_) {
							i_5_ = i_4_;
						}
					} else {
						i_5_ = 1;
					}
					Class224_Sub3_Sub1.cameraYPosition += i_5_;
				}
				RsFloatBuffer.aFloat5794 += 8.0F * (FileProgressMonitor.aFloat3405 * l / 40.0F);
				Class42_Sub2.method388(true);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dea.H(" + i + ',' + l + ')');
		}
	}

	public static final int method574(int i, char c) {
		try {
			if (i <= 72) {
				return 52;
			}
			if (c >= 0 && Class98_Sub43_Sub3.anIntArray5919.length > c) {
				return Class98_Sub43_Sub3.anIntArray5919[c];
			}
			return -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dea.D(" + i + ',' + c + ')');
		}
	}

	ParticlesPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
		Class98_Sub10_Sub1.setParticleSetting(-119, value);
	}

	ParticlesPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
		Class98_Sub10_Sub1.setParticleSetting(-44, value);
	}

	@Override
	public final int compatible(int i, int i_8_) {
		if ((preferenceStore.getSystemInformation(-119).getMaxHeapMb(i_8_ ^ ~0x717d) ^ 0xffffffff) > -97) {
			return 3;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		if ((preferenceStore.getSystemInformation(i ^ ~0x61).getMaxHeapMb(i + -1) ^ 0xffffffff) > -97) {
			return 0;
		}
		return 2;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_6_) {
		value = i_6_;
		Class98_Sub10_Sub1.setParticleSetting(-42, value);
	}

	public final boolean supported(int i) {
		return preferenceStore.getSystemInformation(-100).getMaxHeapMb(i) >= 96;
	}

	@Override
	public final void validate(byte i) {
		if (i > 118) {
			if ((preferenceStore.getSystemInformation(-123).getMaxHeapMb(-1) ^ 0xffffffff) > -97) {
				value = 0;
			}
			if (value < 0 || value > 2) {
				value = getDefaultValue(0);
			}
		}
	}
}

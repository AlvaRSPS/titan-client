/* Class64_Sub26 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class185;
import com.Class98_Sub46_Sub17;
import com.Image;

public final class CharacterShadowsPreferenceField extends IntegerPreferenceField {
	public static Image	aClass324_3713;
	public static int	anInt3712;
	public static int	anInt3714;

	public static final int method658(int i, int i_0_, int i_1_) {
		int i_2_ = i_0_ >>> 749908671;
		if (i != 749908671) {
			return -120;
		}
		return (i_2_ + i_0_) / i_1_ + -i_2_;
	}

	public static void method659(int i) {
		if (i != 3) {
			method660(61, 91, -74, 72);
		}
		aClass324_3713 = null;
	}

	public static final void method660(int i, int i_5_, int i_6_, int i_7_) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i, -98, 9);
		class98_sub46_sub17.method1626((byte) -103);
		class98_sub46_sub17.anInt6054 = i_7_;
		class98_sub46_sub17.headId = i_5_;
	}

	CharacterShadowsPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	CharacterShadowsPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_9_) {
		if (preferenceStore.method1291((byte) 123)) {
			return 3;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method661(int i) {
		return !preferenceStore.method1291((byte) 102);
	}

	@Override
	public final void setNewValue(int i, int i_3_) {
		value = i_3_;
	}

	@Override
	public final void validate(byte i) {
		do {
			if (preferenceStore.method1291((byte) 124)) {
				value = 0;
			}
			if (value == 1 || (value ^ 0xffffffff) == -1) {
				break;
			}
			value = getDefaultValue(0);
			break;
		} while (false);
	}
}

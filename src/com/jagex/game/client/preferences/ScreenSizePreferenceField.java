/* Class64_Sub27 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class165;
import com.Class281;
import com.Class78;
import com.Class98_Sub10_Sub7;
import com.IncomingOpcode;

public final class ScreenSizePreferenceField extends IntegerPreferenceField {
	public static IncomingOpcode	aClass58_3715	= new IncomingOpcode(16, -2);
	public static int				anInt3716;

	public static final int method664(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		if (Class78.aSArray594 == null) {
			return 0;
		}
		if (i_5_ < 59) {
			aClass58_3715 = null;
		}
		if (i_6_ < 3) {
			int i_9_ = i_7_ >> -1338599447;
			int i_10_ = i >> -623073207;
			if ((i_8_ ^ 0xffffffff) > -1 || (i_4_ ^ 0xffffffff) > -1 || i_8_ > -1 + Class165.mapWidth || (Class98_Sub10_Sub7.mapLength + -1 ^ 0xffffffff) > (i_4_ ^ 0xffffffff)) {
				return 0;
			}
			if ((i_9_ ^ 0xffffffff) > -2 || i_10_ < 1 || i_9_ > Class165.mapWidth - 1 || (-1 + Class98_Sub10_Sub7.mapLength ^ 0xffffffff) > (i_10_ ^ 0xffffffff)) {
				return 0;
			}
			boolean bool = (0x2 & Class281.flags[1][i_7_ >> 117366825][i >> 1053911209]) != 0;
			if ((i_7_ & 0x1ff ^ 0xffffffff) == -1) {
				boolean bool_11_ = (Class281.flags[1][i_9_ + -1][i >> 772758441] & 0x2) != 0;
				boolean bool_12_ = (Class281.flags[1][i_9_][i >> -174615703] & 0x2 ^ 0xffffffff) != -1;
				if (!bool_12_ == bool_11_) {
					bool = (0x2 & Class281.flags[1][i_8_][i_4_] ^ 0xffffffff) != -1;
				}
			}
			if ((0x1ff & i ^ 0xffffffff) == -1) {
				boolean bool_13_ = (Class281.flags[1][i_7_ >> -1641555127][i_10_ - 1] & 0x2) != 0;
				boolean bool_14_ = (0x2 & Class281.flags[1][i_7_ >> -1729018327][i_10_]) != 0;
				if (!bool_13_ != !bool_14_) {
					bool = (0x2 & Class281.flags[1][i_8_][i_4_]) != 0;
				}
			}
			if (bool) {
				i_6_++;
			}
		}
		return Class78.aSArray594[i_6_].averageHeight(i_7_, i, true);
	}

	public static void method665(int i) {
		if (i != 2) {
			anInt3716 = 3;
		}
		aClass58_3715 = null;
	}

	ScreenSizePreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	ScreenSizePreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_3_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		if (!preferenceStore.getSystemInformation(-120).isArm(true)) {
			return 2;
		}
		return 3;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_15_) {
		value = i_15_;
	}

	@Override
	public final void validate(byte i) {
		if (value < 1 || (value ^ 0xffffffff) < -4) {
			value = getDefaultValue(0);
		}
	}
}

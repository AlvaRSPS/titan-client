/* Class64_Sub24 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.GameDefinition;
import com.HslUtils;
import com.jagex.game.client.archive.Js5Exception;

public final class IdleAnimationsPreferenceField extends IntegerPreferenceField {
	public static boolean	aBoolean3710	= false;
	public static Object	anObject3709;

	public static void method650(int i) {
		try {
			anObject3709 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uga.G(" + i + ')');
		}
	}

	public static final int rgbToHsl(int i, int i_4_) {
		if (i_4_ <= 66) {
			return 62;
		}
		if (i == 16711935) {
			return -1;
		}
		return HslUtils.rgbToHsl(i, -24);
	}

	IdleAnimationsPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	IdleAnimationsPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_3_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_1_) {
		value = i_1_;
	}

	@Override
	public final void validate(byte i) {
		if (preferenceStore.getGameType((byte) 104) == GameDefinition.STELLAR_DAWN) {
			value = 2;
		}
		if (i >= 118) {
			if ((value ^ 0xffffffff) > -1 || value > 2) {
				value = getDefaultValue(0);
			}
		}
	}
}

/* Class64_Sub5 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class144;
import com.jagex.core.collections.cache.AdvancedMemoryCache;

public final class Class64_Sub5 extends IntegerPreferenceField {
	public static AdvancedMemoryCache	aClass79_3650	= new AdvancedMemoryCache(50);
	public static float[]				aFloatArray3651	= new float[16384];
	public static float[]				aFloatArray3653	= new float[16384];
	public static int					anInt3654;
	public static int[]					anIntArray3652;

	static {
		double d = 3.834951969714103E-4;
		for (int i = 0; i < 16384; i++) {
			aFloatArray3651[i] = (float) Math.sin(i * d);
			aFloatArray3653[i] = (float) Math.cos(i * d);
		}
	}

	Class64_Sub5(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	Class64_Sub5(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_2_) {
		if (!Class144.method2311(false, preferenceStore.currentToolkit.getValue((byte) 125))) {
			return 3;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 0;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method571(int i) {
		return Class144.method2311(false, preferenceStore.currentToolkit.getValue((byte) 124));
	}

	@Override
	public final void setNewValue(int i, int val) {
		value = val;
	}

	@Override
	public final void validate(byte i) {
		if (preferenceStore.currentToolkit.isLive(true) && !Class144.method2311(false, preferenceStore.currentToolkit.getValue((byte) 124))) {
			value = 0;
		}
		if ((value ^ 0xffffffff) > -1 || value > 1) {
			value = getDefaultValue(0);
		}
	}
}

/* Class64_Sub13 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class1;
import com.Class116;
import com.Class165;
import com.Class185;
import com.Class22;
import com.Class224_Sub3;
import com.Class246_Sub3_Sub1_Sub2;
import com.Class246_Sub3_Sub3_Sub1;
import com.Class246_Sub3_Sub4_Sub5;
import com.Class246_Sub3_Sub5_Sub1;
import com.Class4;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub13;
import com.Class98_Sub10_Sub7;
import com.FriendLoginUpdate;
import com.Interface19;
import com.RtInterface;
import com.RtInterfaceAttachment;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;
import com.jagex.game.toolkit.model.NativeModelRenderer;

public final class FlickeringEffectsPreferenceField extends IntegerPreferenceField {
	public static RtInterface[][] interfaceOverride;

	public static final void method601(Class185 class185, int i, int i_0_, int i_1_, int i_2_, int i_3_) {
		do {
			try {
				if ((i_1_ ^ 0xffffffff) <= -2 && i >= 1 && -2 + Class165.mapWidth >= i_1_ && (i ^ 0xffffffff) >= (-2 + Class98_Sub10_Sub7.mapLength ^ 0xffffffff)) {
					if (QuickChatCategory.aClass172ArrayArrayArray5948 == null) {
						break;
					}
					Interface19 interface19 = NativeModelRenderer.aClass305_Sub1_4952.method3583(i, i_2_, i_0_, i_3_ + -6094, i_1_);
					if (interface19 != null) {
						if (!(interface19 instanceof Class246_Sub3_Sub4_Sub5)) {
							if (interface19 instanceof Class246_Sub3_Sub1_Sub2) {
								((Class246_Sub3_Sub1_Sub2) interface19).method3002(class185, (byte) -109);
							} else if (interface19 instanceof Class246_Sub3_Sub3_Sub1) {
								((Class246_Sub3_Sub3_Sub1) interface19).method3016(2048, class185);
							} else if (interface19 instanceof Class246_Sub3_Sub5_Sub1) {
								((Class246_Sub3_Sub5_Sub1) interface19).method3091(class185, false);
							}
						} else {
							((Class246_Sub3_Sub4_Sub5) interface19).method3083(class185, (byte) 32);
						}
					}
				}
				if (i_3_ == 6093) {
					break;
				}
				interfaceOverride = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "iq.G(" + (class185 != null ? "{...}" : "null") + ',' + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ')');
			}
			break;
		} while (false);
	}

	public static final void method603(byte i) {
		try {
			if (!Class4.aBoolean79) {
				FileProgressMonitor.aFloat3405 += (-FileProgressMonitor.aFloat3405 + 24.0F) / 2.0F;
				ParticlesPreferenceField.aBoolean3656 = true;
				if (i <= -93) {
					Class4.aBoolean79 = true;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iq.I(" + i + ')');
		}
	}

	public static final void method604(int i, boolean bool, boolean bool_5_, Js5 class207, int i_6_, int i_7_, int i_8_, long l) {
		try {
			Class116.aClass98_Sub31_Sub2_965 = null;
			Class224_Sub3.anInt5037 = i_7_;
			RtInterfaceAttachment.anInt3951 = i_8_;
			RenderAnimDefinitionParser.anInt1948 = 1;
			LightIntensityDefinitionParser.aClass207_2025 = class207;
			QuickChatMessageType.anInt2911 = 10000;
			Class76_Sub8.anInt3770 = i_6_;
			RectangleLoadingScreenElement.aLong3455 = l;
			Class1.aBoolean66 = bool;
			if (bool_5_ == false) {
				Class22.anInt219 = i;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iq.M(" + i + ',' + bool + ',' + bool_5_ + ',' + (class207 != null ? "{...}" : "null") + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + l + ')');
		}
	}

	public static void method605(int i) {
		try {
			if (i != -2) {
				interfaceOverride = null;
			}
			interfaceOverride = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iq.H(" + i + ')');
		}
	}

	public static final void method606(int i, int i_9_) {
		do {
			try {
				Class98_Sub10_Sub13.anInt5600 = i_9_;
				FriendLoginUpdate.aClass79_6170.clear(110);
				if (i == 18279) {
					break;
				}
				method603((byte) -100);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "iq.D(" + i + ',' + i_9_ + ')');
			}
			break;
		} while (false);
	}

	FlickeringEffectsPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	FlickeringEffectsPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_4_) {
		if (i_4_ != 29053) {
			interfaceOverride = null;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int val) {
		value = val;
	}

	@Override
	public final void validate(byte i) {
		if (value != 1 && (value ^ 0xffffffff) != -1) {
			value = getDefaultValue(0);
		}
	}
}

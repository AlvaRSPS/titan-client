/* Class64_Sub16 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class151_Sub5;
import com.Class65;
import com.jagex.game.client.archive.Js5;

public final class MaxScreenSizePreferenceField extends IntegerPreferenceField {
	public static Class65	aClass65_3681	= new Class65();
	public static int		anInt3680		= 0;
	public static short[]	resultIndexBuffer;
	public static Js5		fontmetricsStore;

	public static void method615(int i) {
		do {
			resultIndexBuffer = null;
			aClass65_3681 = null;
			fontmetricsStore = null;
			if (i == 3) {
				break;
			}
			method615(19);
			break;
		} while (false);
	}

	MaxScreenSizePreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	MaxScreenSizePreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_0_) {
		if (preferenceStore.method1291((byte) 117)) {
			return 3;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		if (!preferenceStore.method1291((byte) 118)) {
			if (preferenceStore.currentToolkit.isLive(true) && Class151_Sub5.method2462(preferenceStore.currentToolkit.getValue((byte) 124), (byte) 20)) {
				return 1;
			}
			return 0;
		}
		return 2;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method613(int i) {
		return !preferenceStore.method1291((byte) 122);
	}

	@Override
	public final void setNewValue(int i, int i_1_) {
		value = i_1_;
	}

	@Override
	public final void validate(byte i) {

		if (preferenceStore.method1291((byte) 117)) {
			value = 2;
		}
		if ((value ^ 0xffffffff) > -1 || value > 2) {
			value = getDefaultValue(0);
		}
	}
}

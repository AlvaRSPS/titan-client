/* Class64_Sub12 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.jagex.game.client.archive.Js5Exception;

public final class GraphicsLevelPreferenceField extends IntegerPreferenceField {
	public static boolean		aBoolean3671;
	public static boolean[][][]	aBooleanArrayArrayArray3673;
	public static int			groupCount	= 0;

	static {
		aBoolean3671 = false;
	}

	public static void method599(byte i) {
		do {
			try {
				aBooleanArrayArrayArray3673 = null;
				if (i == -13) {
					break;
				}
				aBooleanArrayArrayArray3673 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "hp.D(" + i + ')');
			}
			break;
		} while (false);
	}

	GraphicsLevelPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	GraphicsLevelPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_2_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 0;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_0_) {
		value = i_0_;
	}

	@Override
	public final void validate(byte i) {
		if (value < 0 || value > 4) {
			value = getDefaultValue(0);
		}
	}
}

/* Class64_Sub20 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.CacheFileRequest;
import com.Class21_Sub3;
import com.Class258;
import com.Class336;
import com.Class98_Sub30;
import com.GameDefinition;
import com.GameShell;
import com.Huffman;
import com.IncomingOpcode;
import com.BaseModel;
import com.RSToolkit;
import com.SceneGraphNodeList;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;
import com.jagex.game.toolkit.matrix.NativeMatrix;

public final class TexturesPreferenceField extends IntegerPreferenceField {
	public static BaseModel[]				aClass178Array3699		= new BaseModel[4];
	public static SceneGraphNodeList	aClass218_3694			= new SceneGraphNodeList();
	public static int					anInt3696;
	public static int					CONTENT_TYPE_GAME_VIEW	= 1403;
	public static Js5					interfacesJs5;
	public static Js5					materialsJs5;

	public static final void method633(int i) {
		if (Class258.aClass155Array1951 != null) {
			for (int i_1_ = 0; (Class21_Sub3.anInt5389 ^ 0xffffffff) < (i_1_ ^ 0xffffffff); i_1_++) {
				Class258.aClass155Array1951[i_1_] = null;
			}
			Class258.aClass155Array1951 = null;
		}
		if (i != 0) {
			aClass178Array3699 = null;
		}
		if (Class98_Sub30.aClass155Array4099 != null) {
			for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > (Class336.anInt2820 ^ 0xffffffff); i_2_++) {
				Class98_Sub30.aClass155Array4099[i_2_] = null;
			}
			Class98_Sub30.aClass155Array4099 = null;
		}
		if (AwtGraphicsBuffer.aClass155Array5889 != null) {
			for (int i_3_ = 0; i_3_ < RSToolkit.anInt936; i_3_++) {
				AwtGraphicsBuffer.aClass155Array5889[i_3_] = null;
			}
			AwtGraphicsBuffer.aClass155Array5889 = null;
		}
		Huffman.aClass155Array1611 = null;
		NativeMatrix.anIntArray4707 = null;
		Class64_Sub3.anInt3646 = IncomingOpcode.anInt461 = -1;
		CacheFileRequest.anIntArrayArrayArray6311 = null;
	}

	TexturesPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	TexturesPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_0_) {
		if (preferenceStore.getGameType((byte) 104) == GameDefinition.RUNESCAPE) {
			if (preferenceStore.method1291((byte) 114)) {
				return 3;
			}
			if (i == 0 || preferenceStore.groundBlending.getValue((byte) 126) == 1) {
				return 1;
			}
			return 2;
		}
		return 3;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method635(int i) {
		if (preferenceStore.getGameType((byte) 104) == GameDefinition.RUNESCAPE) {
			return !preferenceStore.method1291((byte) 120);
		}
		return false;
	}

	@Override
	public final void setNewValue(int i, int i_4_) {
		value = i_4_;
	}

	@Override
	public final void validate(byte i) {
		do {
			do {
				if (preferenceStore.getGameType((byte) 104) == GameDefinition.RUNESCAPE) {
					if (!preferenceStore.method1291((byte) 117)) {
						break;
					}
					value = 0;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				value = 1;
			} while (false);
			if (value == 0 || value == 1) {
				break;
			}
			value = getDefaultValue(0);
			break;
		} while (false);
	}
}

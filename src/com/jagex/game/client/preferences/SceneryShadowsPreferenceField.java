/* Class64_Sub7 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.NativeOpenGlElementArrayBuffer;
import com.Class246_Sub5;
import com.Class305_Sub1;
import com.Class65;
import com.Class81;
import com.Class98_Sub31_Sub4;
import com.Class98_Sub36;
import com.Class98_Sub43_Sub1;
import com.OutgoingOpcode;
import com.OutgoingPacket;
import com.ParticleManager;
import com.RSToolkit;
import com.jagex.game.client.archive.Js5Exception;

public final class SceneryShadowsPreferenceField extends IntegerPreferenceField {
	public static OutgoingOpcode SEND_REPORT_PLAYER = new OutgoingOpcode(21, -1);

	public static final char method576(byte i, byte i_0_) {
		try {
			int i_1_ = 0xff & i;
			if ((i_1_ ^ 0xffffffff) == -1) {
				throw new IllegalArgumentException("Non cp1252 character 0x" + Integer.toString(i_1_, 16) + " provided");
			}
			if (i_1_ >= 128 && (i_1_ ^ 0xffffffff) > -161) {
				int i_2_ = Class65.unicodeUnescapes[-128 + i_1_];
				if ((i_2_ ^ 0xffffffff) == -1) {
					i_2_ = 63;
				}
				i_1_ = i_2_;
			}
			if (i_0_ < 118) {
				return '\023';
			}
			return (char) i_1_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ds.D(" + i + ',' + i_0_ + ')');
		}
	}

	public static void method577(int i) {
		do {
			try {
				SEND_REPORT_PLAYER = null;
				if (i == 16) {
					break;
				}
				SEND_REPORT_PLAYER = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ds.I(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method578(int i) {
		try {
			if (i != 16543) {
				method578(-104);
			}
			for (Class98_Sub36 class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.startIteration(123); class98_sub36 != null; class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.iterateNext(i + -16544)) {
				if (!class98_sub36.aClass237_Sub1_4157.method2902(true)) {
					class98_sub36.aClass237_Sub1_4157.method2916((byte) 44);
					try {
						class98_sub36.aClass237_Sub1_4157.method2904(false);
					} catch (Exception exception) {
						Class305_Sub1.reportError(exception, -121, "TV: " + class98_sub36.anInt4160);
						OutgoingPacket.method1127((byte) 67, class98_sub36.anInt4160);
					}
					if (!class98_sub36.aBoolean4155 && !class98_sub36.aBoolean4153) {
						Class98_Sub43_Sub1 class98_sub43_sub1 = class98_sub36.aClass237_Sub1_4157.method2900((byte) -79);
						if (class98_sub43_sub1 != null) {
							Class98_Sub31_Sub4 class98_sub31_sub4 = class98_sub43_sub1.method1488((byte) 92);
							if (class98_sub31_sub4 != null) {
								class98_sub31_sub4.method1392(i + 255170282, class98_sub36.anInt4152);
								Class81.aClass98_Sub31_Sub3_619.method1371(class98_sub31_sub4);
								class98_sub36.aBoolean4155 = true;
							}
						}
					}
				} else {
					OutgoingPacket.method1127((byte) 67, class98_sub36.anInt4160);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ds.H(" + i + ')');
		}
	}

	public static final void method580(RSToolkit var_ha, int i) {
		try {
			for (Class246_Sub5 class246_sub5 = (Class246_Sub5) ParticleManager.systems.getFirst((byte) 15); class246_sub5 != null; class246_sub5 = (Class246_Sub5) ParticleManager.systems.getNext(false)) {
				if (class246_sub5.aBoolean5108) {
					class246_sub5.method3118(var_ha);
				}
			}
			if (i < 15) {
				method577(15);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ds.M(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	SceneryShadowsPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	SceneryShadowsPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_5_) {
		if (preferenceStore.method1291((byte) 127)) {
			return 3;
		}
		if (preferenceStore.textures.getValue((byte) 123) == 0) {
			return 3;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 2;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method581(int i) {
		if (preferenceStore.method1291((byte) 106)) {
			return false;
		}
		return preferenceStore.textures.getValue((byte) 120) != 0;
	}

	@Override
	public final void setNewValue(int i, int i_3_) {
		value = i_3_;
	}

	@Override
	public final void validate(byte i) {
		if (preferenceStore.method1291((byte) 119)) {
			value = 0;
		}
		if ((preferenceStore.textures.getValue((byte) 120) ^ 0xffffffff) == -1) {
			value = 0;
		}
		if (value < 0 || value > 2) {
			value = getDefaultValue(0);
		}
	}
}

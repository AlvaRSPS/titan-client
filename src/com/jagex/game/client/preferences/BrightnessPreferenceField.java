/* Class64_Sub19 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class3;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub19;
import com.Class98_Sub10_Sub38;
import com.GroundItem;
import com.SceneGraphNodeList;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class BrightnessPreferenceField extends IntegerPreferenceField {
	public static int	anInt3693	= 0;
	public static short	aShort3692;

	static {
		aShort3692 = (short) 256;
	}

	public static final boolean method631(int i, int i_2_, int i_3_) {
		if (i_3_ != -2) {
			return true;
		}
		return OpenGlGround.method3433(i_2_, i_3_ ^ ~0x3de8, i) & OpenGLHeap.method1682(i_2_, 0, i);
	}

	public static final void method632(int i, int i_5_, byte i_6_, int i_7_, int i_8_, int i_9_, int i_10_) {
		do {
			if ((Class76_Sub8.anInt3778 ^ 0xffffffff) >= (i_8_ ^ 0xffffffff) && Class3.anInt77 >= i_5_ && (i_9_ ^ 0xffffffff) <= (Class98_Sub10_Sub38.anInt5753 ^ 0xffffffff) && SceneGraphNodeList.anInt1635 >= i_10_) {
				if ((i_7_ ^ 0xffffffff) == -2) {
					Class64_Sub3.method565(i_9_, i_10_, i_5_, i_8_, i, -10194);
				} else {
					GroundItem.method1279(i_9_, i_5_, i_7_, i, i_10_, i_8_, true);
				}
			} else if ((i_7_ ^ 0xffffffff) != -2) {
				Class98_Sub10_Sub19.method1059(false, i_8_, i, i_9_, i_7_, i_5_, i_10_);
			} else {
				Js5Exception.method4010(-31085, i_10_, i_8_, i_9_, i_5_, i);
			}
			if (i_6_ == -51) {
				break;
			}
			aShort3692 = (short) 74;
			break;
		} while (false);
	}

	BrightnessPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	BrightnessPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_4_) {
		if (i_4_ != 29053) {
			setNewValue(-113, 77);
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 3;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int val) {
		value = val;
	}

	@Override
	public final void validate(byte i) {
		if (value < 0 || (value ^ 0xffffffff) < -5) {
			value = getDefaultValue(0);
		}
	}
}

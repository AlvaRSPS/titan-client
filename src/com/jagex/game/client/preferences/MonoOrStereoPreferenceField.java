/* Class64_Sub1 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.IncomingOpcode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;

public final class MonoOrStereoPreferenceField extends IntegerPreferenceField {
	public static Js5				aClass207_3641;
	public static IncomingOpcode	aClass58_3637;
	public static int[]				anIntArray3639;
	public static int[]				anIntArray3640;
	public static int[][]			anIntArrayArray3638	= { { 0, 1, 2, 3 }, { 1, 2, 3, 0 }, { 1, 2, -1, 0 }, { 2, 0, -1, 1 }, { 0, 1, -1, 2 }, { 1, 2, -1, 0 }, { -1, 4, -1, 1 }, { -1, 1, 3, -1 }, { -1, 0, 2, -1 }, { 3, 5, 2, 0 }, { 0, 2, 5, 3 }, { 0, 2, 3, 5 }, { 0, 1, 2, 3 } };

	static {
		aClass58_3637 = new IncomingOpcode(34, 6);
	}

	public static void method557(int i) {
		try {
			anIntArrayArray3638 = null;
			if (i <= 79) {
				anIntArray3639 = null;
			}
			aClass207_3641 = null;
			anIntArray3640 = null;
			aClass58_3637 = null;
			anIntArray3639 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aga.D(" + i + ')');
		}
	}

	MonoOrStereoPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	MonoOrStereoPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_0_) {
		return 1;

	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_1_) {
		value = i_1_;
	}

	@Override
	public final void validate(byte i) {
		do {
			if (i > 118) {
				if (value == 1 || (value ^ 0xffffffff) == -1) {
					break;
				}
				value = getDefaultValue(0);
			}
			break;
		} while (false);
	}
}

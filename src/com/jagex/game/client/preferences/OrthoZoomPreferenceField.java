
/* Class64_Sub18 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import java.io.IOException;
import java.net.Socket;

import com.ActionGroup;
import com.ActionQueueEntry;
import com.AnimationSkeletonSet;
import com.Char;
import com.Class21_Sub4;
import com.Class22;
import com.Class246_Sub3_Sub3;
import com.Class246_Sub4_Sub2;
import com.Class272;
import com.Class33;
import com.Class333;
import com.Class359;
import com.Class372;
import com.Class375;
import com.Class87;
import com.Class95;
import com.Class98_Sub47;
import com.GameShell;
import com.GroundItem;
import com.OpenGLRenderEffectManager;
import com.PacketParser;
import com.Player;
import com.RSByteBuffer;
import com.RtInterfaceAttachment;
import com.Sprite;
import com.aa_Sub1;
import com.aa_Sub2;
import com.client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.constants.BuildLocation;

public final class OrthoZoomPreferenceField extends IntegerPreferenceField {
	public static float		aFloat3691;
	public static int[]		anIntArray3688	= { 1, -1, -1, 1 };
	public static Sprite[]	nameIcons;

	public static final void method622(byte i) {
		try {
			ActionGroup class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getFirst(-1);
			if (i != -38) {
				anIntArray3688 = null;
			}
			for (/**/; class98_sub46_sub9 != null; class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getNext(0)) {
				if (class98_sub46_sub9.actionCount > 1) {
					class98_sub46_sub9.actionCount = 0;
					AnimationSkeletonSet.aClass79_6046.put(((ActionQueueEntry) class98_sub46_sub9.actions.head.nextCacheable).groupHash, class98_sub46_sub9, (byte) -80);
					class98_sub46_sub9.actions.clear(16711680);
				}
			}
			GraphicsLevelPreferenceField.groupCount = 0;
			Class359.actionCount = 0;
			Class33.actionList.clear((byte) 47);
			Class98_Sub47.groupMap.clear(-99);
			RtInterfaceAttachment.actionGroups.clear(i ^ ~0xff0025);
			Player.menuOpen = false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oc.O(" + i + ')');
		}
	}

	public static final void method623(int i) {
		do {
			try {
				Class22.anInt216 = 0;
				if (i != -23196) {
					method623(27);
				}
				int i_2_ = (Class87.localPlayer.boundExtentsX >> 1513969193) + Class272.gameSceneBaseX;
				int i_3_ = (Class87.localPlayer.boundExtentsZ >> 1695912137) + aa_Sub2.gameSceneBaseY;
				if (i_2_ >= 3053 && i_2_ <= 3156 && (i_3_ ^ 0xffffffff) <= -3057 && i_3_ <= 3136) {
					Class22.anInt216 = 1;
				}
				if (i_2_ >= 3072 && (i_2_ ^ 0xffffffff) >= -3119 && i_3_ >= 9492 && (i_3_ ^ 0xffffffff) >= -9536) {
					Class22.anInt216 = 1;
				}
				if (Class22.anInt216 != 1 || (i_2_ ^ 0xffffffff) > -3140 || i_2_ > 3199 || i_3_ < 3008 || (i_3_ ^ 0xffffffff) < -3063) {
					break;
				}
				Class22.anInt216 = 0;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "oc.H(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method624(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_) {
		try {
			int i_11_ = 0;
			int i_12_ = i_10_;
			int i_13_ = 0;
			int i_14_ = -i_6_ + i_7_;
			int i_15_ = -i_6_ + i_10_;
			int i_16_ = i_7_ * i_7_;
			int i_17_ = i_10_ * i_10_;
			int i_18_ = i_14_ * i_14_;
			int i_19_ = i_15_ * i_15_;
			int i_20_ = i_17_ << -909163295;
			int i_21_ = i_16_ << -1295639423;
			int i_22_ = i_19_ << 631613089;
			int i_23_ = i_18_ << 2112910369;
			int i_24_ = i_10_ << -1139343487;
			int i_25_ = i_15_ << 1840275937;
			int i_26_ = i_20_ + i_16_ * (-i_24_ + 1);
			int i_27_ = -((-1 + i_24_) * i_21_) + i_17_;
			int i_28_ = i_22_ + (1 + -i_25_) * i_18_;
			int i_29_ = -(i_23_ * (i_25_ + -1)) + i_19_;
			int i_30_ = i_16_ << -87112830;
			int i_31_ = i_17_ << -1476324542;
			int i_32_ = i_18_ << 2049619074;
			int i_33_ = i_19_ << 167149762;
			int i_34_ = 3 * i_20_;
			int i_35_ = i_21_ * (-3 + i_24_);
			int i_36_ = i_22_ * 3;
			int i_37_ = (i_25_ + -3) * i_23_;
			int i_38_ = i_31_;
			int i_39_ = (-1 + i_10_) * i_30_;
			int i_40_ = i_33_;
			int i_41_ = (-1 + i_15_) * i_32_;
			int[] is = AnimationDefinition.anIntArrayArray814[i_8_];
			Class333.method3761(i_9_, is, i_5_ - i_7_, i_5_ + -i_14_, (byte) -123);
			Class333.method3761(i, is, i_5_ - i_14_, i_14_ + i_5_, (byte) -127);
			Class333.method3761(i_9_, is, i_14_ + i_5_, i_5_ + i_7_, (byte) 49);
			if (i_4_ == -2211) {
				while ((i_12_ ^ 0xffffffff) < -1) {
					boolean bool = i_12_ <= i_15_;
					if (bool) {
						if (i_28_ < 0) {
							while ((i_28_ ^ 0xffffffff) > -1) {
								i_29_ += i_40_;
								i_28_ += i_36_;
								i_40_ += i_33_;
								i_36_ += i_33_;
								i_13_++;
							}
						}
						if (i_29_ < 0) {
							i_29_ += i_40_;
							i_28_ += i_36_;
							i_36_ += i_33_;
							i_40_ += i_33_;
							i_13_++;
						}
						i_29_ += -i_37_;
						i_28_ += -i_41_;
						i_37_ -= i_32_;
						i_41_ -= i_32_;
					}
					if (i_26_ < 0) {
						while ((i_26_ ^ 0xffffffff) > -1) {
							i_27_ += i_38_;
							i_26_ += i_34_;
							i_11_++;
							i_34_ += i_31_;
							i_38_ += i_31_;
						}
					}
					if ((i_27_ ^ 0xffffffff) > -1) {
						i_26_ += i_34_;
						i_27_ += i_38_;
						i_34_ += i_31_;
						i_11_++;
						i_38_ += i_31_;
					}
					i_26_ += -i_39_;
					i_27_ += -i_35_;
					i_39_ -= i_30_;
					i_35_ -= i_30_;
					i_12_--;
					int i_42_ = i_8_ - i_12_;
					int i_43_ = i_8_ + i_12_;
					int i_44_ = i_5_ - -i_11_;
					int i_45_ = -i_11_ + i_5_;
					if (bool) {
						int i_46_ = i_13_ + i_5_;
						int i_47_ = i_5_ + -i_13_;
						Class333.method3761(i_9_, AnimationDefinition.anIntArrayArray814[i_42_], i_45_, i_47_, (byte) -124);
						Class333.method3761(i, AnimationDefinition.anIntArrayArray814[i_42_], i_47_, i_46_, (byte) -126);
						Class333.method3761(i_9_, AnimationDefinition.anIntArrayArray814[i_42_], i_46_, i_44_, (byte) -124);
						Class333.method3761(i_9_, AnimationDefinition.anIntArrayArray814[i_43_], i_45_, i_47_, (byte) -125);
						Class333.method3761(i, AnimationDefinition.anIntArrayArray814[i_43_], i_47_, i_46_, (byte) 91);
						Class333.method3761(i_9_, AnimationDefinition.anIntArrayArray814[i_43_], i_46_, i_44_, (byte) -128);
					} else {
						Class333.method3761(i_9_, AnimationDefinition.anIntArrayArray814[i_42_], i_45_, i_44_, (byte) 60);
						Class333.method3761(i_9_, AnimationDefinition.anIntArrayArray814[i_43_], i_45_, i_44_, (byte) -124);
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oc.N(" + i + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + i_9_ + ',' + i_10_ + ')');
		}
	}

	public static final void method628(int i) {
		try {
			if ((Class21_Sub4.anInt5394 ^ 0xffffffff) != -1) {
				try {
					if ((++GroundItem.anInt4028 ^ 0xffffffff) < -2001) {
						if (aa_Sub1.aClass123_3561 != null) {
							aa_Sub1.aClass123_3561.close(-114);
							aa_Sub1.aClass123_3561 = null;
						}
						if (Class372.anInt3150 >= 2) {
							OpenGLRenderEffectManager.anInt442 = -5;
							Class21_Sub4.anInt5394 = 0;
							return;
						}
						client.lobbyServer.rotateConnectionMethod(0);
						Class372.anInt3150++;
						GroundItem.anInt4028 = 0;
						Class21_Sub4.anInt5394 = 1;
					}
					if ((Class21_Sub4.anInt5394 ^ 0xffffffff) == -2) {
						Class246_Sub3_Sub3.aClass143_6155 = client.lobbyServer.openSocket(-127, GameShell.signLink);
						Class21_Sub4.anInt5394 = 2;
					}
					if ((Class21_Sub4.anInt5394 ^ 0xffffffff) == -3) {
						if ((Class246_Sub3_Sub3.aClass143_6155.status ^ 0xffffffff) == -3) {
							throw new IOException();
						}
						if ((Class246_Sub3_Sub3.aClass143_6155.status ^ 0xffffffff) != -2) {
							return;
						}
						aa_Sub1.aClass123_3561 = BuildLocation.method2668((Socket) Class246_Sub3_Sub3.aClass143_6155.result, (byte) 11, 7500);
						Class246_Sub3_Sub3.aClass143_6155 = null;
						Class95.method920((byte) 127);
						Class21_Sub4.anInt5394 = 4;
					}
					if (i != 19700) {
						method622((byte) -71);
					}
					if ((Class21_Sub4.anInt5394 ^ 0xffffffff) == -5) {
						if (aa_Sub1.aClass123_3561.isBuffered(i + -21649, 1)) {
							aa_Sub1.aClass123_3561.read(PacketParser.buffer.payload, 0, 2047, 1);
							int i_49_ = PacketParser.buffer.payload[0] & 0xff;
							OpenGLRenderEffectManager.anInt442 = i_49_;
							Class21_Sub4.anInt5394 = 0;
							aa_Sub1.aClass123_3561.close(-113);
							aa_Sub1.aClass123_3561 = null;
						}
					}
				} catch (IOException ioexception) {
					if (aa_Sub1.aClass123_3561 != null) {
						aa_Sub1.aClass123_3561.close(-100);
						aa_Sub1.aClass123_3561 = null;
					}
					if ((Class372.anInt3150 ^ 0xffffffff) <= -3) {
						OpenGLRenderEffectManager.anInt442 = -4;
						Class21_Sub4.anInt5394 = 0;
					} else {
						client.lobbyServer.rotateConnectionMethod(0);
						Class372.anInt3150++;
						GroundItem.anInt4028 = 0;
						Class21_Sub4.anInt5394 = 1;
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oc.D(" + i + ')');
		}
	}

	public static void method629(int i) {
		try {
			if (i == -32294) {
				anIntArray3688 = null;
				nameIcons = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oc.M(" + i + ')');
		}
	}

	OrthoZoomPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	OrthoZoomPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_48_) {
		if (Class375.method3986(i, (byte) -108)) {
			if (preferenceStore.currentToolkit.isLive(true) && !Class246_Sub4_Sub2.method3107(preferenceStore.currentToolkit.getValue((byte) 127), (byte) -85)) {
				return 3;
			}
			if (preferenceStore.currentScreenSize.getValue((byte) 120) == 1) {
				return 3;
			}
		}
		if (i == 3) {
			return 3;
		}
		if (i_48_ != 29053) {
			nameIcons = null;
		}
		if (Class375.method3986(i, (byte) -108)) {
			return 2;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 0;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method626(int i) {
		return i == -1;
	}

	@Override
	public final void setNewValue(int i, int i_0_) {
		value = i_0_;
	}

	public final boolean supported(byte i) {
		if (i >= -41) {
			getDefaultValue(-65);
		}
		return Class375.method3986(value, (byte) -108);
	}

	@Override
	public final void validate(byte i) {
		if (supported((byte) -111)) {
			if (preferenceStore.currentToolkit.isLive(true) && !Class246_Sub4_Sub2.method3107(preferenceStore.currentToolkit.getValue((byte) 121), (byte) -119)) {
				value = 1;
			}
			if ((preferenceStore.currentScreenSize.getValue((byte) 123) ^ 0xffffffff) == -2) {
				value = 1;
			}
		}
		if (value == 3) {
			value = 2;
		}
		if (value < 0 || (value ^ 0xffffffff) < -4) {
			value = getDefaultValue(0);
		}
	}
}

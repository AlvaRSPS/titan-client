/* Class64_Sub8 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class185;
import com.Class48_Sub1_Sub1;
import com.Class98_Sub46_Sub17;
import com.OutgoingOpcode;
import com.jagex.core.collections.LinkedList;

public final class ToolkitPreferenceField extends IntegerPreferenceField {
	public static LinkedList		aClass148_3659	= new LinkedList();
	public static OutgoingOpcode	aClass171_3661	= new OutgoingOpcode(70, 3);

	public static final void method585(int i, byte i_4_, int i_5_) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_5_, -49, 12);
		class98_sub46_sub17.method1626((byte) -103);
		class98_sub46_sub17.anInt6054 = i;
	}

	public boolean	defaultValue	= false;

	private boolean	live			= true;

	ToolkitPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	ToolkitPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_3_) {
		if (!preferenceStore.getSystemInformation(i_3_ + -29179).method2317(false)) {
			return 3;
		}
		if (i == 3 && !Class48_Sub1_Sub1.method463(-1, "jagdx")) {
			return 3;
		}
		return 2;
	}

	@Override
	public final int getDefaultValue(int i) {
		defaultValue = true;
		if (!preferenceStore.getSystemInformation(-125).method2317(false)) {
			return 0;
		}
		return 2;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean isLive(boolean bool) {
		return live;
	}

	public final boolean method587(int i) {
		return preferenceStore.getSystemInformation(-114).method2317(false);
	}

	public final void setLive(boolean _live, boolean bool_0_) {
		live = _live;
	}

	@Override
	public final void setNewValue(int i, int value) {
		this.value = value;
		defaultValue = false;
	}

	@Override
	public final void validate(byte i) {
		if (!preferenceStore.getSystemInformation(-117).method2317(false)) {
			value = 0;
		}
		if (value < 0 || (value ^ 0xffffffff) < -6) {
			value = getDefaultValue(0);
		}
	}
}

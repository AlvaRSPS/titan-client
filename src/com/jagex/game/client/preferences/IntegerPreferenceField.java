/* Class64 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.WorldMap;

public abstract class IntegerPreferenceField {
	public static final int getWorldMapZoom(int i) {
		if (WorldMap.zoom == 3.0) {
			return 37;
		}
		if (WorldMap.zoom == 4.0) {
			return 50;
		}
		if (WorldMap.zoom == 6.0) {
			return 75;
		}
		if (WorldMap.zoom == 8.0) {
			return 100;
		}
		return 200;
	}

	public static final boolean method555(int i, int i_2_, int i_3_) {
		if (i_3_ >= -33) {
			return false;
		}
		return (0x400 & i_2_ ^ 0xffffffff) != -1;
	}

	protected GamePreferences	preferenceStore;

	protected int				value;

	protected IntegerPreferenceField(GamePreferences gamePreferences) {
		preferenceStore = gamePreferences;
		value = getDefaultValue(0);
	}

	protected IntegerPreferenceField(int val, GamePreferences gamePreferences) {
		preferenceStore = gamePreferences;
		value = val;
	}

	public abstract int compatible(int i, int i_4_);

	public abstract int getDefaultValue(int i);

	public abstract void setNewValue(int i, int i_0_);

	final void setValue(int i, int i_1_) {
		do {
			if (compatible(i, 29053) != 3) {
				setNewValue(i_1_ ^ ~0x2608, i);
			}
			if (i_1_ == 9848) {
				break;
			}
			value = 1;
			break;
		} while (false);
	}

	public abstract void validate(byte i);
}

/* Class64_Sub3 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class333;
import com.GameDefinition;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.AnimationDefinition;

public final class Class64_Sub3 extends IntegerPreferenceField {
	public static Js5	aClass207_3648;
	public static int	anInt3646;
	public static int	anInt3647	= -1;

	static {
		anInt3646 = -1;
	}

	static final void method565(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		Class333.method3761(i_3_, AnimationDefinition.anIntArrayArray814[i++], i_2_, i_1_, (byte) -123);
		Class333.method3761(i_3_, AnimationDefinition.anIntArrayArray814[i_0_--], i_2_, i_1_, (byte) 125);
		if (i_4_ != -10194) {
			anInt3647 = 18;
		}
		for (int i_5_ = i; (i_5_ ^ 0xffffffff) >= (i_0_ ^ 0xffffffff); i_5_++) {
			int[] is = AnimationDefinition.anIntArrayArray814[i_5_];
			is[i_2_] = is[i_1_] = i_3_;
		}
	}

	public static void method566(int i) {
		if (i == -3623) {
			aClass207_3648 = null;
		}
	}

	Class64_Sub3(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	Class64_Sub3(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_6_) {
		return 3;
	}

	@Override
	public final int getDefaultValue(int i) {
		if (preferenceStore.getGameType((byte) 104) != GameDefinition.RUNESCAPE || !preferenceStore.method1291((byte) 104)) {
			return 1;
		}
		return 0;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_7_) {
		value = i_7_;
	}

	@Override
	public final void validate(byte i) {
		do {
			value = getDefaultValue(0);
			break;
		} while (false);
	}
}

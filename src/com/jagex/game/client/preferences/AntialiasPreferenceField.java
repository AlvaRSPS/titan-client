
/* Class64_Sub23 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class144;
import com.Class370;

public final class AntialiasPreferenceField extends IntegerPreferenceField {
	public static Class370	aClass370_3707;
	public static int		anInt3708;

	AntialiasPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	AntialiasPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_4_) {
		if (i_4_ != 29053) {
			validate((byte) -94);
		}
		if (!Class144.method2311(false, preferenceStore.currentToolkit.getValue((byte) 125))) {
			return 3;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 0;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method647(int i) {
		return Class144.method2311(false, preferenceStore.currentToolkit.getValue((byte) 127));
	}

	@Override
	public final void setNewValue(int i, int i_2_) {
		value = i_2_;
	}

	@Override
	public final void validate(byte i) {
		if (preferenceStore.currentToolkit.isLive(true) && !Class144.method2311(false, preferenceStore.currentToolkit.getValue((byte) 120))) {
			value = 0;
		}
		if ((value ^ 0xffffffff) > -1 || value > 2) {
			value = getDefaultValue(0);
		}
	}
}

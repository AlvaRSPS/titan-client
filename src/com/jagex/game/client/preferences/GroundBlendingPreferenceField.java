/* Class64_Sub25 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class246_Sub10;
import com.Class303;
import com.Class317;
import com.Class76_Sub9;
import com.Class98_Sub6;
import com.GameDefinition;
import com.GameShell;
import com.Image;
import com.TextureMetrics;
import com.Sprite;
import com.client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.QuestDefinition;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;

public final class GroundBlendingPreferenceField extends IntegerPreferenceField {
	public static int anInt3711;

	public static final String method653(int i, int[] is) {
		StringBuffer stringbuffer = new StringBuffer();
		int i_1_ = Js5Exception.anInt3201;
		for (int i_2_ = i; (is.length ^ 0xffffffff) < (i_2_ ^ 0xffffffff); i_2_++) {
			QuestDefinition class220 = Class303.questDefinitionList.list(is[i_2_], -11180);
			if ((class220.menuSpriteId ^ 0xffffffff) != 0) {
				Sprite class332 = (Sprite) Class98_Sub6.aClass79_3847.get(-126, class220.menuSpriteId);
				if (class332 == null) {
					Image class324 = Image.loadImage(client.spriteJs5, class220.menuSpriteId, 0);
					if (class324 != null) {
						class332 = client.graphicsToolkit.createSprite(class324, true);
						Class98_Sub6.aClass79_3847.put(class220.menuSpriteId, class332, (byte) -80);
					}
				}
				if (class332 != null) {
					GroupProgressMonitor.aClass332Array3408[i_1_] = class332;
					stringbuffer.append(" <img=").append(i_1_).append(">");
					i_1_++;
				}
			}
		}
		return stringbuffer.toString();
	}

	public static final Object method654(int i, byte[] is, boolean bool) {
		if (i != 2) {
			anInt3711 = 82;
		}
		if (is == null) {
			return null;
		}
		if (is.length > 136 && !StoreProgressMonitor.aBoolean3413) {
			try {
				Class317 class317 = (Class317) Class.forName("com.Class317_Sub1").newInstance();
				class317.method3652((byte) -101, is);
				return class317;
			} catch (Throwable throwable) {
				StoreProgressMonitor.aBoolean3413 = true;
			}
		}
		if (bool) {
			return Class246_Sub10.method3140(is, 0);
		}
		return is;
	}

	public static final boolean method656(int i, byte i_3_, int i_4_) {
		if (i_3_ != -123) {
			method656(11, (byte) -48, 84);
		}
		return !(!(Class76_Sub9.method766(-91, i, i_4_) | (0x70000 & i_4_) != 0) && !TextureMetrics.method2919(-120, i_4_, i));
	}

	GroundBlendingPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	GroundBlendingPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_0_) {
		if (preferenceStore.method1291((byte) 119)) {
			return 3;
		}
		if (preferenceStore.getGameType((byte) 104) == GameDefinition.RUNESCAPE) {
			if (i == 0) {
				if ((preferenceStore.fog.getValue((byte) 124) ^ 0xffffffff) == -2) {
					return 2;
				}
				if ((preferenceStore.textures.getValue((byte) 124) ^ 0xffffffff) == -2) {
					return 2;
				}
				if ((preferenceStore.waterDetail.getValue((byte) 125) ^ 0xffffffff) < -1) {
					return 2;
				}
			}
			return 1;
		}
		return 3;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method657(int i) {
		if (preferenceStore.method1291((byte) 112)) {
			return false;
		}
		return preferenceStore.getGameType((byte) 104) == GameDefinition.RUNESCAPE;
	}

	@Override
	public final void setNewValue(int i, int i_5_) {
		value = i_5_;
	}

	@Override
	public final void validate(byte i) {
		do {
			do {
				if (preferenceStore.getGameType((byte) 104) != GameDefinition.RUNESCAPE) {
					value = 1;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if (preferenceStore.method1291((byte) 106)) {
					value = 0;
				}
			} while (false);
			if (value == 0 || value == 1) {
				break;
			}
			value = getDefaultValue(0);
			break;
		} while (false);
	}
}

/* Class64_Sub22 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.CacheFileRequest;
import com.Class119_Sub1;
import com.Class151_Sub4;
import com.Class151_Sub8;
import com.Class155;
import com.Class172;
import com.Class21_Sub3;
import com.Class258;
import com.Class336;
import com.Class370;
import com.Class98_Sub30;
import com.DummyOutputStream;
import com.Huffman;
import com.OpenGLTexture2DSource;
import com.RSToolkit;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.parser.SkyboxDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;
import com.jagex.game.toolkit.model.NativeModelRenderer;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class VolumePreferenceField extends IntegerPreferenceField {
	public static RenderAnimDefinition	aClass294_3706	= new RenderAnimDefinition();
	public static int					anInt3705		= 0;

	public static final void method640(double d, int i) {
		try {
			if (d != LinkedList.aDouble1197) {
				for (int i_0_ = 0; i_0_ < 256; i_0_++) {
					int i_1_ = (int) (Math.pow(i_0_ / 255.0, d) * 255.0);
					Class151_Sub4.anIntArray4985[i_0_] = (i_1_ ^ 0xffffffff) < -256 ? 255 : i_1_;
				}
				LinkedList.aDouble1197 = d;
			}
			if (i != 0) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rv.H(" + d + ',' + i + ')');
		}
	}

	public static void method642(int i) {
		do {
			try {
				aClass294_3706 = null;
				if (i >= 83) {
					break;
				}
				method642(66);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rv.I(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method643(int i, Huffman class213) {
		try {
			if (i != -256) {
				method644(116);
			}
			NativeModelRenderer.aClass213_4949 = class213;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rv.D(" + i + ',' + (class213 != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method644(int i) {
		try {
			for (int i_4_ = 0; (Class336.anInt2820 ^ 0xffffffff) < (i_4_ ^ 0xffffffff); i_4_++) {
				Class98_Sub30.aClass155Array4099[i_4_] = null;
			}
			Class336.anInt2820 = 0;
			for (int i_5_ = 0; (OpenGLTexture2DSource.anInt3103 ^ 0xffffffff) < (i_5_ ^ 0xffffffff); i_5_++) {
				for (int i_6_ = 0; (BConfigDefinition.anInt3112 ^ 0xffffffff) < (i_6_ ^ 0xffffffff); i_6_++) {
					for (int i_7_ = 0; i_7_ < Class64_Sub9.anInt3662; i_7_++) {
						Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i_5_][i_7_][i_6_];
						if (class172 != null) {
							if (class172.aShort1328 > 0) {
								class172.aShort1328 *= -1;
							}
							if (class172.aShort1335 > 0) {
								class172.aShort1335 *= -1;
							}
						}
					}
				}
			}
			int i_8_ = 0;
			if (i >= -36) {
				anInt3705 = 62;
			}
			for (/**/; OpenGLTexture2DSource.anInt3103 > i_8_; i_8_++) {
				for (int i_9_ = 0; (BConfigDefinition.anInt3112 ^ 0xffffffff) < (i_9_ ^ 0xffffffff); i_9_++) {
					for (int i_10_ = 0; Class64_Sub9.anInt3662 > i_10_; i_10_++) {
						Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][i_10_][i_9_];
						if (class172 != null) {
							boolean bool = QuickChatCategory.aClass172ArrayArrayArray5948[0][i_10_][i_9_] != null && QuickChatCategory.aClass172ArrayArrayArray5948[0][i_10_][i_9_].aClass172_1330 != null;
							if (class172.aShort1335 < 0) {
								int i_11_ = i_9_;
								int i_12_ = i_9_;
								int i_13_ = i_8_;
								int i_14_ = i_8_;
								Class172 class172_15_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][i_10_][-1 + i_11_];
								int i_16_;
								for (i_16_ = StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_9_, -12639, i_10_); (i_11_ ^ 0xffffffff) < -1 && class172_15_ != null && class172_15_.aShort1335 < 0 && (class172_15_.aShort1335 ^ 0xffffffff) == (class172.aShort1335 ^ 0xffffffff)
										&& (class172_15_.aShort1329 ^ 0xffffffff) == (class172.aShort1329 ^ 0xffffffff) && (i_16_ ^ 0xffffffff) == (StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_11_ + -1, -12639, i_10_)
												^ 0xffffffff); class172_15_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][i_10_][-1 + i_11_]) {
									if ((i_11_ - 1 ^ 0xffffffff) < -1 && i_16_ != StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_11_ - 2, -12639, i_10_)) {
										break;
									}
									i_11_--;
								}
								for (class172_15_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][i_10_][i_12_ - -1]; (i_12_ ^ 0xffffffff) > (Class64_Sub9.anInt3662 ^ 0xffffffff) && class172_15_ != null && (class172_15_.aShort1335 ^ 0xffffffff) > -1
										&& class172.aShort1335 == class172_15_.aShort1335 && (class172.aShort1329 ^ 0xffffffff) == (class172_15_.aShort1329 ^ 0xffffffff) && StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_12_ + 1, -12639,
												i_10_) == i_16_; class172_15_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][i_10_][1 + i_12_]) {
									if ((1 + i_12_ ^ 0xffffffff) > (Class64_Sub9.anInt3662 ^ 0xffffffff) && (i_16_ ^ 0xffffffff) != (StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_12_ + 2, -12639, i_10_) ^ 0xffffffff)) {
										break;
									}
									i_12_++;
								}
								int i_17_ = -i_13_ + i_14_ + 1;
								int i_18_ = StrongReferenceMCNode.aSArray6298[!bool ? i_13_ : 1 + i_13_].getTileHeight(i_11_, -12639, i_10_);
								int i_19_ = class172.aShort1335 * i_17_ + i_18_;
								int i_20_ = StrongReferenceMCNode.aSArray6298[bool ? i_13_ - -1 : i_13_].getTileHeight(1 + i_12_, -12639, i_10_);
								int i_21_ = i_20_ + class172.aShort1335 * i_17_;
								int i_22_ = i_10_ << Class151_Sub8.tileScale;
								int i_23_ = i_11_ << Class151_Sub8.tileScale;
								int i_24_ = NativeShadow.anInt6333 + (i_12_ << Class151_Sub8.tileScale);
								Class98_Sub30.aClass155Array4099[Class336.anInt2820++] = new Class155(1, i_14_, i_22_ - -class172.aShort1329, class172.aShort1329 + i_22_, class172.aShort1329 + i_22_, class172.aShort1329 + i_22_, i_18_, i_20_, i_21_, i_19_, i_23_, i_24_, i_24_, i_23_);
								for (int i_25_ = i_13_; (i_25_ ^ 0xffffffff) >= (i_14_ ^ 0xffffffff); i_25_++) {
									for (int i_26_ = i_11_; (i_26_ ^ 0xffffffff) >= (i_12_ ^ 0xffffffff); i_26_++) {
										QuickChatCategory.aClass172ArrayArrayArray5948[i_25_][i_10_][i_26_].aShort1335 *= -1;
									}
								}
							}
							if ((class172.aShort1328 ^ 0xffffffff) > -1) {
								int i_27_ = i_10_;
								int i_28_ = i_10_;
								int i_29_ = i_8_;
								int i_30_ = i_8_;
								Class172 class172_31_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][-1 + i_27_][i_9_];
								int i_32_;
								for (i_32_ = StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_9_, -12639, i_10_); (i_27_ ^ 0xffffffff) < -1 && class172_31_ != null && (class172_31_.aShort1328 ^ 0xffffffff) > -1 && class172.aShort1328 == class172_31_.aShort1328 && (class172.aShort1323
										^ 0xffffffff) == (class172_31_.aShort1323 ^ 0xffffffff) && i_32_ == StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_9_, -12639, -1 + i_27_); class172_31_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][i_27_ - 1][i_9_]) {
									if (-1 + i_27_ > 0 && (StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_9_, -12639, i_27_ - 2) ^ 0xffffffff) != (i_32_ ^ 0xffffffff)) {
										break;
									}
									i_27_--;
								}
								for (class172_31_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][i_28_ + 1][i_9_]; (i_28_ ^ 0xffffffff) > (BConfigDefinition.anInt3112 ^ 0xffffffff) && class172_31_ != null && (class172_31_.aShort1328 ^ 0xffffffff) > -1 && (class172.aShort1328
										^ 0xffffffff) == (class172_31_.aShort1328 ^ 0xffffffff) && class172_31_.aShort1323 == class172.aShort1323 && StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_9_, -12639, i_28_
												+ 1) == i_32_; class172_31_ = QuickChatCategory.aClass172ArrayArrayArray5948[i_8_][i_28_ + 1][i_9_]) {
									if ((i_28_ + 1 ^ 0xffffffff) > (BConfigDefinition.anInt3112 ^ 0xffffffff) && (i_32_ ^ 0xffffffff) != (StrongReferenceMCNode.aSArray6298[i_8_].getTileHeight(i_9_, -12639, i_28_ - -2) ^ 0xffffffff)) {
										break;
									}
									i_28_++;
								}
								int i_33_ = -i_29_ + i_30_ - -1;
								int i_34_ = StrongReferenceMCNode.aSArray6298[bool ? 1 + i_29_ : i_29_].getTileHeight(i_9_, -12639, i_27_);
								int i_35_ = i_34_ - -(i_33_ * class172.aShort1328);
								int i_36_ = StrongReferenceMCNode.aSArray6298[bool ? 1 + i_29_ : i_29_].getTileHeight(i_9_, -12639, i_28_ + 1);
								int i_37_ = class172.aShort1328 * i_33_ + i_36_;
								int i_38_ = i_27_ << Class151_Sub8.tileScale;
								int i_39_ = NativeShadow.anInt6333 + (i_28_ << Class151_Sub8.tileScale);
								int i_40_ = i_9_ << Class151_Sub8.tileScale;
								Class98_Sub30.aClass155Array4099[Class336.anInt2820++] = new Class155(2, i_30_, i_38_, i_39_, i_39_, i_38_, i_34_, i_36_, i_37_, i_35_, i_40_ - -class172.aShort1323, class172.aShort1323 + i_40_, i_40_ + class172.aShort1323, i_40_ + class172.aShort1323);
								for (int i_41_ = i_29_; (i_30_ ^ 0xffffffff) <= (i_41_ ^ 0xffffffff); i_41_++) {
									for (int i_42_ = i_27_; (i_42_ ^ 0xffffffff) >= (i_28_ ^ 0xffffffff); i_42_++) {
										QuickChatCategory.aClass172ArrayArrayArray5948[i_41_][i_42_][i_9_].aShort1328 *= -1;
									}
								}
							}
						}
					}
				}
			}
			DummyOutputStream.aBoolean35 = true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rv.G(" + i + ')');
		}
	}

	public static final void method645(byte i) {
		DummyOutputStream.aBoolean35 = false;
		SkyboxDefinitionParser.activeOccludersCount = 0;
		CacheFileRequest.anIntArrayArrayArray6311 = new int[OpenGLTexture2DSource.anInt3103][1 + BConfigDefinition.anInt3112][Class64_Sub9.anInt3662 - -1];
		Class21_Sub3.anInt5389 = 0;
		AwtGraphicsBuffer.aClass155Array5889 = new Class155[1000];
		Class98_Sub30.aClass155Array4099 = new Class155[2000];
		Huffman.aClass155Array1611 = new Class155[500];
		Class119_Sub1.anInt4716 = NativeShadow.anInt6333;
		Class336.anInt2820 = 0;
		RSToolkit.anInt936 = 0;
		Class370.anInt3139 = NativeShadow.anInt6333;
		Class258.aClass155Array1951 = new Class155[500];
		RtMouseEvent.occlusion = true;
	}

	VolumePreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	VolumePreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_43_) {
		if (i_43_ != 29053) {
			return 3;
		}
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 127;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_2_) {
		value = i_2_;
	}

	@Override
	public final void validate(byte i) {
		do {
			if (i > 118) {
				if (value >= 0 || value <= 127) {
					break;
				}
				value = getDefaultValue(0);
			}
			break;
		} while (false);
	}
}

/* Class64_Sub11 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.GameDefinition;
import com.GameShell;
import com.jagex.game.client.archive.Js5Exception;

public final class GroundDecorationPreferenceField extends IntegerPreferenceField {
	public static double	aDouble3669;
	public static int		anInt3668	= 0;

    public static void method598(int i) {
		do {
			try {
				// pool30000 = null;
				if (i == 0) {
					break;
				}
				aDouble3669 = -0.004191568579013829;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "hn.H(" + i + ')');
			}
			break;
		} while (false);
	}

	GroundDecorationPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	GroundDecorationPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_5_) {
		if (preferenceStore.method1291((byte) 121)) {
			return 3;
		}
		if (preferenceStore.getGameType((byte) 104) == GameDefinition.RUNESCAPE) {
			return 1;
		}
		return 3;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method597(int i) {
		if (preferenceStore.method1291((byte) 117)) {
			return false;
		}
		return preferenceStore.getGameType((byte) 104) == GameDefinition.RUNESCAPE;
	}

	@Override
	public final void setNewValue(int i, int i_6_) {
		value = i_6_;
	}

	@Override
	public final void validate(byte i) {
		do {
			do {
				if (preferenceStore.getGameType((byte) 104) != GameDefinition.RUNESCAPE) {
					value = 1;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if (preferenceStore.method1291((byte) 112)) {
					value = 0;
				}
			} while (false);
			if (value == 0 || (value ^ 0xffffffff) == -2) {
				break;
			}
			value = getDefaultValue(0);
			break;
		} while (false);
	}
}

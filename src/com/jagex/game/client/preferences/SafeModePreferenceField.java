/* Class64_Sub2 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class246_Sub3_Sub4_Sub1;
import com.Class25;
import com.Class329;
import com.IncomingOpcode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;

public final class SafeModePreferenceField extends IntegerPreferenceField {
	public static Js5				aClass207_3644;
	public static IncomingOpcode	aClass58_3642	= new IncomingOpcode(48, 3);
	public static IncomingOpcode	aClass58_3645	= new IncomingOpcode(73, 4);

	public static final void method559(boolean bool, int i, int i_0_) {
		if (bool != true) {
			aClass58_3642 = null;
		}
		if (i != Class25.anInt268) {
			MonoOrStereoPreferenceField.anIntArray3640 = new int[i];
			for (int i_1_ = 0; (i ^ 0xffffffff) < (i_1_ ^ 0xffffffff); i_1_++) {
				MonoOrStereoPreferenceField.anIntArray3640[i_1_] = (i_1_ << 715435980) / i;
			}
			Class329.anInt2761 = -1 + i;
			Class246_Sub3_Sub4_Sub1.anInt6241 = i * 32;
			Class25.anInt268 = i;
		}
		if ((Class25.anInt268 ^ 0xffffffff) != (i_0_ ^ 0xffffffff)) {
			GameObjectDefinition.anIntArray3001 = new int[i_0_];
			for (int i_2_ = 0; i_2_ < i_0_; i_2_++) {
				GameObjectDefinition.anIntArray3001[i_2_] = (i_2_ << 1275810284) / i_0_;
			}
		} else {
			GameObjectDefinition.anIntArray3001 = MonoOrStereoPreferenceField.anIntArray3640;
		}
		HorizontalAlignment.anInt492 = i_0_;
		SoftwareNativeHeap.anInt6075 = -1 + i_0_;
	}

	public static final int method561(byte i, int i_3_) {
		int i_4_ = i_3_ & 0x3f;
		int i_5_ = 0x3 & i_3_ >> -1593233914;
		if ((i_4_ ^ 0xffffffff) == -19) {
			if (i_5_ == 0) {
				return 1;
			}
			if (i_5_ == 1) {
				return 2;
			}
			if (i_5_ == 2) {
				return 4;
			}
			if (i_5_ == 3) {
				return 8;
			}
		} else if ((i_4_ ^ 0xffffffff) == -20 || i_4_ == 21) {
			if (i_5_ == 0) {
				return 16;
			}
			if ((i_5_ ^ 0xffffffff) == -2) {
				return 32;
			}
			if ((i_5_ ^ 0xffffffff) == -3) {
				return 64;
			}
			if ((i_5_ ^ 0xffffffff) == -4) {
				return 128;
			}
		}
		return 0;
	}

	public SafeModePreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	public SafeModePreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_23_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 0;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int newValue) {
		value = newValue;
	}

	@Override
	public final void validate(byte i) {
		if ((value ^ 0xffffffff) != -2 && value != 0) {
			value = getDefaultValue(0);
		}
	}
}

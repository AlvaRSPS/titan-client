/* Class64_Sub14 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Sprite;

public final class FogPreferenceField extends IntegerPreferenceField {
	public static Sprite[] aClass332Array3675;

	public FogPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	public FogPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_0_) {
		if ((i ^ 0xffffffff) == -1 || preferenceStore.groundBlending.getValue((byte) 126) == 1) {
			return 1;
		}
		return 2;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method607(int i) {
		if (i != -1) {
			aClass332Array3675 = null;
		}
		return true;
	}

	@Override
	public final void setNewValue(int i, int i_1_) {
		value = i_1_;
	}

	@Override
	public final void validate(byte i) {
		if ((value ^ 0xffffffff) != -1 && preferenceStore.groundBlending.getValue((byte) 122) != 1) {
			value = 0;
		}
		if (value < 0 || (value ^ 0xffffffff) < -2) {
			value = getDefaultValue(0);
		}
	}
}

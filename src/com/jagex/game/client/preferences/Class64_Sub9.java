/* Class64_Sub9 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class130;
import com.Class284_Sub1_Sub1;
import com.Class284_Sub2_Sub1;
import com.Class39_Sub1;
import com.Class98_Sub41;
import com.OpenGLHeightMapToNormalMapConverter;
import com.OpenGlToolkit;
import com.Sprite;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.ui.loading.impl.elements.impl.RotatingSpriteLoadingScreenElement;

public final class Class64_Sub9 extends IntegerPreferenceField {
	public static Sprite	aClass332_3663;
	public static int		anInt3662;

	public static void method589(int i) {
		do {
			try {
				aClass332_3663 = null;
				if (i == 8) {
					break;
				}
				method589(15);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "eu.D(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method590(boolean bool, OpenGlToolkit var_ha_Sub1) {
		do {
			try {
				if (VarPlayerDefinition.anObject1285 == null) {
					Class284_Sub2_Sub1 class284_sub2_sub1 = new Class284_Sub2_Sub1();
					byte[] is = class284_sub2_sub1.method3377(16, 20283, 128, 128);
					VarPlayerDefinition.anObject1285 = GroundBlendingPreferenceField.method654(2, is, false);
				}
				if (Class130.anObject1030 == null) {
					Class284_Sub1_Sub1 class284_sub1_sub1 = new Class284_Sub1_Sub1();
					byte[] is = class284_sub1_sub1.method3366(true, 128, 16, 128);
					Class130.anObject1030 = GroundBlendingPreferenceField.method654(2, is, false);
				}
				OpenGLHeightMapToNormalMapConverter class118 = var_ha_Sub1.hmToNormalCvtrMap;
				if (!class118.method2171(bool) || Class98_Sub41.anObject4203 != null) {
					break;
				}
				byte[] is = RotatingSpriteLoadingScreenElement.method2244(-31633, 16.0F, 0.5F, 0.6F, 16, 8, 128, new Class39_Sub1(419684), 4.0F, 4.0F, 128);
				Class98_Sub41.anObject4203 = GroundBlendingPreferenceField.method654(2, is, false);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "eu.G(" + bool + ',' + (var_ha_Sub1 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public Class64_Sub9(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	public Class64_Sub9(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_11_) {
		return 3;
	}

	@Override
	public final int getDefaultValue(int i) {
		if (!preferenceStore.method1291((byte) 111)) {
			return 0;
		}
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_0_) {
		value = i_0_;
	}

	@Override
	public final void validate(byte i) {
		value = getDefaultValue(0);
	}
}

/* Class64_Sub21 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5Exception;

public final class CpuUsagePreferenceField extends IntegerPreferenceField {
	public static LinkedList	aClass148_3703;
	public static int			anInt3700	= 0;
	public static int			anInt3702;
	public static int			anInt3704	= 1407;
	public static int[]			levels		= new int[25];

	static {
		anInt3702 = 0;
		aClass148_3703 = new LinkedList();
	}

	public static void method638(int i) {
		do {
			try {
				levels = null;
				aClass148_3703 = null;
				if (i == 4) {
					break;
				}
				method638(3);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rha.D(" + i + ')');
			}
			break;
		} while (false);
	}

	CpuUsagePreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	CpuUsagePreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_2_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		if (preferenceStore.getSystemInformation(-108).getAvailableProcessors(32755) > 1) {
			return 4;
		}
		return 2;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_0_) {
		value = i_0_;
	}

	@Override
	public final void validate(byte i) {
		if (value < 0 && value > 4) {
			value = getDefaultValue(0);
		}
	}
}

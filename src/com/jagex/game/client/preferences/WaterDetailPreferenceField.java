/* Class64_Sub28 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class142;
import com.Class151_Sub2;
import com.Class246_Sub1;
import com.Class246_Sub3_Sub5_Sub2;
import com.ClientInventory;

public final class WaterDetailPreferenceField extends IntegerPreferenceField {
	public static int		anInt3717;
	public static int[][]	anIntArrayArray3718	= { { 0, 2 }, { 0, 2 }, { 0, 0, 2 }, { 2, 0, 0 }, { 0, 2, 0 }, { 0, 0, 2 }, { 0, 5, 1, 4 }, { 0, 4, 4, 4 }, { 4, 4, 4, 0 }, { 6, 6, 6, 2, 2, 2 }, { 2, 2, 2, 6, 6, 6 }, { 0, 11, 6, 6, 6, 4 }, { 0, 2 }, { 0, 4, 4, 4 }, { 0, 4, 4, 4 } };
	public static int[][]	anIntArrayArray3719;

	public static final ClientInventory get(int i, boolean bool, int i_2_) {
		long l = (!bool ? 0 : -2147483648) | i;
		return (ClientInventory) Class142.aClass377_1157.get(l, -1);
	}

	public static final boolean method670(int i, int i_3_, int i_4_) {
		return (Class151_Sub2.method2451(i, 544, i_4_) | (i & 0x2000) != 0 | Class246_Sub1.method2967(i_4_, i, (byte) 91)) & Class246_Sub3_Sub5_Sub2.method3096(-27984, i_4_, i);
	}

	WaterDetailPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	WaterDetailPreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_5_) {
		if (preferenceStore.method1291((byte) 113)) {
			return 3;
		}
		if ((i ^ 0xffffffff) == -1 || (preferenceStore.groundBlending.getValue((byte) 121) ^ 0xffffffff) == -2) {
			return 1;
		}
		return 2;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	public final boolean method671(int i) {
		if (i != -1) {
			validate((byte) 19);
		}
		return !preferenceStore.method1291((byte) 109);
	}

	@Override
	public final void setNewValue(int i, int i_0_) {
		value = i_0_;
	}

	@Override
	public final void validate(byte i) {
		do {
			if (preferenceStore.method1291((byte) 118)) {
				value = 0;
			}
			if (value >= 0 || (value ^ 0xffffffff) >= -3) {
				break;
			}
			value = getDefaultValue(0);
			break;
		} while (false);
	}
}

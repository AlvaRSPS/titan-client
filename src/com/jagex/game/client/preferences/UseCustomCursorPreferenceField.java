/* Class64_Sub29 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class1;
import com.Class116;
import com.Class166;
import com.Class195;
import com.Class224_Sub3;
import com.Class246_Sub3_Sub5_Sub2;
import com.Class287;
import com.Class42_Sub4;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub34;
import com.NodeShort;
import com.GameShell;
import com.RSToolkit;
import com.RtInterfaceAttachment;
import com.RtInterfaceClip;
import com.TextResources;
import com.TextureMetricsList;
import com.WorldMap;
import com.client;
import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Archive;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.CursorDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.AwtLoadingScreen;
import com.jagex.game.toolkit.ground.OpenGlGround;

public final class UseCustomCursorPreferenceField extends IntegerPreferenceField {

	public static final void drawWorldMapUnderground(RSToolkit toolkit, TextureMetricsList metrics, int y, int x, int screenHeight, byte i_2_, int screenWidth) {
		do {
			if (QuickChatCategoryParser.loadingProgress < 100) {
				Archive.method3783(toolkit, metrics, 107);
			}
			toolkit.setClip(x, y, screenWidth + x, y + screenHeight);
			if (QuickChatCategoryParser.loadingProgress < 100) {
				int barHeight = 20;
				int progressWidth = screenWidth / 2 + x;
				int progressHeight = -18 + y + screenHeight / 2 + -barHeight;
				toolkit.fillRectangle(x, y, screenWidth, screenHeight, -16777216, 0);
				toolkit.drawRectangle(-152 + progressWidth, progressHeight, 304, 34, AwtLoadingScreen.BORDER_COLOURS[client.colourId].getRGB(), 0);
				toolkit.fillRectangle(progressWidth - 150, progressHeight - -2, QuickChatCategoryParser.loadingProgress * 3, 30, AwtLoadingScreen.BAR_COLOURS[client.colourId].getRGB(), 0);
				Class98_Sub10_Sub34.p13Full.drawStringCenterAligned(AwtLoadingScreen.TEXT_COLOURS[client.colourId].getRGB(), TextResources.LOADING_BOX.getText(client.gameLanguage, (byte) 25), progressWidth, -1, (byte) -50, barHeight + progressHeight);
			} else {
				int i_7_ = Class42_Sub4.anInt5371 - (int) (screenWidth / WorldMap.aFloat2064);
				int i_8_ = (int) (screenHeight / WorldMap.aFloat2064) + NodeShort.anInt4197;
				int i_9_ = (int) (screenWidth / WorldMap.aFloat2064) + Class42_Sub4.anInt5371;
				CursorDefinition.anInt1739 = -(int) (screenHeight / WorldMap.aFloat2064) + NodeShort.anInt4197;
				RtInterfaceClip.anInt48 = (int) (screenWidth * 2 / WorldMap.aFloat2064);
				Class246_Sub3_Sub5_Sub2.anInt6268 = (int) (2 * screenHeight / WorldMap.aFloat2064);
				int i_11_ = -(int) (screenHeight / WorldMap.aFloat2064) + NodeShort.anInt4197;
				Class166.anInt1279 = -(int) (screenWidth / WorldMap.aFloat2064) + Class42_Sub4.anInt5371;
				WorldMap.method3308(WorldMap.anInt2075 + i_7_, WorldMap.anInt2078 + i_8_, i_9_ + WorldMap.anInt2075, WorldMap.anInt2078 + i_11_, x, y, screenWidth + x, screenHeight + y + 1);
				WorldMap.method3309(toolkit);
				LinkedList class148 = WorldMap.method3315(toolkit);
				HitmarksDefinition.method843(0, toolkit, class148, -1, 0);
				if ((GroundBlendingPreferenceField.anInt3711 ^ 0xffffffff) < -1) {
					Class287.anInt2186--;
					if (Class287.anInt2186 == 0) {
						GroundBlendingPreferenceField.anInt3711--;
						Class287.anInt2186 = 20;
					}
				}
				if (!client.displayFps) {
					break;
				}
				int i_12_ = screenWidth + x - 5;
				int i_13_ = screenHeight + y + -8;
				Class195.p12Full.drawStringRightAnchor(16776960, 0, i_12_, -1, "Fps:" + GameShell.fpsValue, i_13_);
				i_13_ -= 15;
				Runtime runtime = Runtime.getRuntime();
				int memory = (int) ((runtime.totalMemory() + -runtime.freeMemory()) / 1024L);
				int i_15_ = 16776960;
				if ((memory ^ 0xffffffff) < -65537) {
					i_15_ = 16711680;
				}
				Class195.p12Full.drawStringRightAnchor(i_15_, 0, i_12_, -1, "Mem:" + memory + "k", i_13_);
				i_13_ -= 15;
			}
			break;
		} while (false);
	}

	public static final String formatItemStackSize(int i, int i_23_) {
		String string = Integer.toString(i);
		for (int index = string.length() + -3; (index ^ 0xffffffff) < -1; index -= 3) {
			string = string.substring(0, index) + "," + string.substring(index);
		}
		if (string.length() > 9) {
			return " <col=00ff80>" + string.substring(0, -8 + string.length()) + TextResources.MILLION.getText(client.gameLanguage, (byte) 25) + " (" + string + ")</col>";
		}
		if ((string.length() ^ 0xffffffff) < -7) {
			return " <col=ffffff>" + string.substring(0, -4 + string.length()) + TextResources.THOUSAND.getText(client.gameLanguage, (byte) 25) + " (" + string + ")</col>";
		}
		return " <col=ffff00>" + string + "</col>";
	}

	public static final void method674(int i, int i_18_, int i_19_, Js5 class207, int i_20_, boolean bool, byte i_21_) {
		do {
			if ((i_20_ ^ 0xffffffff) >= -1) {
				OpenGlGround.method3434(class207, bool, i, i_19_, i_18_, -16523);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			Class116.aClass98_Sub31_Sub2_965 = null;
			RtInterfaceAttachment.anInt3951 = i_19_;
			RenderAnimDefinitionParser.anInt1948 = 1;
			LightIntensityDefinitionParser.aClass207_2025 = class207;
			Class76_Sub8.anInt3770 = i_18_;
			Class1.aBoolean66 = bool;
			Class224_Sub3.anInt5037 = i;
			QuickChatMessageType.anInt2911 = BConfigDefinition.aClass98_Sub31_Sub2_3122.method1360(-16257) / i_20_;
			if (QuickChatMessageType.anInt2911 < 1) {
				QuickChatMessageType.anInt2911 = 1;
			}
		} while (false);
	}

	public static void method676(byte i) {
		if (i != -58) {
			client.buildLocation = null;
		}
		client.buildLocation = null;
	}

	UseCustomCursorPreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	UseCustomCursorPreferenceField(int i, GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_25_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 1;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_16_) {
		value = i_16_;
	}

	@Override
	public final void validate(byte i) {
		if ((value ^ 0xffffffff) != -2 && (value ^ 0xffffffff) != -1) {
			value = getDefaultValue(0);
		}
	}
}

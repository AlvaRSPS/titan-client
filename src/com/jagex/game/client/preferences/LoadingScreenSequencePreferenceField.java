/* Class64_Sub4 - Decompiled by JODE
 */ package com.jagex.game.client.preferences; /*
												*/

import com.Class204;
import com.jagex.game.client.archive.Js5Exception;

public final class LoadingScreenSequencePreferenceField extends IntegerPreferenceField {
	public static Class204 aClass204_3649 = new Class204();

	public static void method567(byte i) {
		do {
			try {
				aClass204_3649 = null;
				if (i == 81) {
					break;
				}
				method567((byte) 73);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cq.D(" + i + ')');
			}
			break;
		} while (false);
	}

	LoadingScreenSequencePreferenceField(GamePreferences gamePreferences) {
		super(gamePreferences);
	}

	LoadingScreenSequencePreferenceField(int i, GamePreferences gamePreferences) {
		super(i, gamePreferences);
	}

	@Override
	public final int compatible(int i, int i_0_) {
		return 1;
	}

	@Override
	public final int getDefaultValue(int i) {
		return 0;
	}

	public final int getValue(byte i) {
		return value;
	}

	@Override
	public final void setNewValue(int i, int i_1_) {
		value = i_1_;
	}

	@Override
	public final void validate(byte i) {
		if (i < 118) {
			return;
		}
	}
}

package com.jagex.game.anticheat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.Class369;
import com.RSByteBuffer;
import com.ReflectionRequest;
import com.RsBitsBuffers;
import com.SignLink;
import com.SignLinkRequest;
import com.jagex.core.collections.LinkedList;

public class ReflectionAntiCheat {

	public static LinkedList requestQueue;

	public static final void decodeRequest(int i, SignLink signLink, RSByteBuffer buffer, int i_84_) {
		ReflectionRequest request = new ReflectionRequest();
		request.opCount = buffer.readUnsignedByte((byte) 5);
		request.link = buffer.readInt(-2);
		request.parameterData = new byte[request.opCount][][];
		request.fieldRequests = new SignLinkRequest[request.opCount];
		request.status = new int[request.opCount];
		request.opcode = new int[request.opCount];
		request.methodRequests = new SignLinkRequest[request.opCount];
		request.integerValues = new int[request.opCount];
		for (int inPtr = i; (request.opCount ^ 0xffffffff) < (inPtr ^ 0xffffffff); inPtr++) {
			try {
				int req = buffer.readUnsignedByte((byte) 35);
				if (req != 0 && (req ^ 0xffffffff) != -2 && req != 2) {
					if ((req ^ 0xffffffff) == -4 || req == 4) {
						String type = buffer.readString((byte) 84);
						String name = buffer.readString((byte) 84);
						int i_88_ = buffer.readUnsignedByte((byte) -108);
						String[] parTypeNames = new String[i_88_];
						for (int i_89_ = 0; (i_89_ ^ 0xffffffff) > (i_88_ ^ 0xffffffff); i_89_++) {
							parTypeNames[i_89_] = buffer.readString((byte) 84);
						}
						byte[][] is = new byte[i_88_][];
						if (req == 3) {
							for (int i_90_ = 0; i_90_ < i_88_; i_90_++) {
								int i_91_ = buffer.readInt(-2);
								is[i_90_] = new byte[i_91_];
								buffer.getData(is[i_90_], true, i_91_, 0);
							}
						}
						request.opcode[inPtr] = req;
						Class[] var_classes = new Class[i_88_];
						for (int parPtr = 0; (i_88_ ^ 0xffffffff) < (parPtr ^ 0xffffffff); parPtr++) {
							var_classes[parPtr] = getTypeClass(107, parTypeNames[parPtr]);
						}
						request.methodRequests[inPtr] = signLink.getDeclaredMethod(name, getTypeClass(-113, type), Class369.method3953(i, 0), var_classes);
						request.parameterData[inPtr] = is;
					}
				} else {
					String string = buffer.readString((byte) 84);
					String string_93_ = buffer.readString((byte) 84);
					int i_94_ = 0;
					if ((req ^ 0xffffffff) == -2) {
						i_94_ = buffer.readInt(i + -2);
					}
					request.opcode[inPtr] = req;
					request.integerValues[inPtr] = i_94_;
					request.fieldRequests[inPtr] = signLink.getDeclaredField(string_93_, getTypeClass(-128, string), -27303);
				}
			} catch (ClassNotFoundException classnotfoundexception) {
				request.status[inPtr] = -1;
			} catch (SecurityException securityexception) {
				request.status[inPtr] = -2;
			} catch (NullPointerException nullpointerexception) {
				request.status[inPtr] = -3;
			} catch (Exception exception) {
				request.status[inPtr] = -4;
			} catch (Throwable throwable) {
				request.status[inPtr] = -5;
			}
		}
		requestQueue.addLast(request, -20911);
	}

	public static final Class getTypeClass(int i, String string) throws ClassNotFoundException {
		if (string.equals("B")) {
			return Byte.TYPE;
		}
		if (string.equals("I")) {
			return Integer.TYPE;
		}
		if (string.equals("S")) {
			return Short.TYPE;
		}
		if (string.equals("J")) {
			return Long.TYPE;
		}
		if (string.equals("Z")) {
			return Boolean.TYPE;
		}
		if (string.equals("F")) {
			return Float.TYPE;
		}
		if (string.equals("D")) {
			return Double.TYPE;
		}
		if (string.equals("C")) {
			return Character.TYPE;
		}
		return Class.forName(string);
	}

	public static final void process(byte i, RsBitsBuffers outputPacket) {
		ReflectionRequest in = (ReflectionRequest) requestQueue.getFirst(32);
		if (in != null) {
			boolean waitingOnSignLink = false;
			for (int inPtr = 0; (inPtr ^ 0xffffffff) > (in.opCount ^ 0xffffffff); inPtr++) {
				if (in.fieldRequests[inPtr] != null) {
					if (in.fieldRequests[inPtr].status == 2) {
						in.status[inPtr] = -5;
					}
					if ((in.fieldRequests[inPtr].status ^ 0xffffffff) == -1) {
						waitingOnSignLink = true;
					}
				}
				if (in.methodRequests[inPtr] != null) {
					if (in.methodRequests[inPtr].status == 2) {
						in.status[inPtr] = -6;
					}
					if ((in.methodRequests[inPtr].status ^ 0xffffffff) == -1) {
						waitingOnSignLink = true;
					}
				}
			}
			if (!waitingOnSignLink) {
				int outStart = outputPacket.position;
				outputPacket.writeInt(i + 1571862949, in.link);
				for (int inPtr = 0; in.opCount > inPtr; inPtr++) {
					if ((in.status[inPtr] ^ 0xffffffff) != -1) {
						outputPacket.writeByte(in.status[inPtr], i ^ 0x9);
					} else {
						try {
							int opCode = in.opcode[inPtr];
							if (opCode == 0) {
								Field field = (Field) in.fieldRequests[inPtr].result;
								int value = field.getInt(null);
								outputPacket.writeByte(0, -99);
								outputPacket.writeInt(i ^ ~0x5db0b954, value);
							} else if ((opCode ^ 0xffffffff) == -2) {
								Field field = (Field) in.fieldRequests[inPtr].result;
								field.setInt(null, in.integerValues[inPtr]);
								outputPacket.writeByte(0, -48);
							} else if (opCode == 2) {
								Field field = (Field) in.fieldRequests[inPtr].result;
								int modifiers = field.getModifiers();
								outputPacket.writeByte(0, -52);
								outputPacket.writeInt(1571862888, modifiers);
							}
							if ((opCode ^ 0xffffffff) == -4) {
								Method method = (Method) in.methodRequests[inPtr].result;
								byte[][] parData = in.parameterData[inPtr];
								Object[] objects = new Object[parData.length];
								for (int parPtr = 0; (parData.length ^ 0xffffffff) < (parPtr ^ 0xffffffff); parPtr++) {
									ObjectInputStream objectinputstream = new ObjectInputStream(new ByteArrayInputStream(parData[parPtr]));
									objects[parPtr] = objectinputstream.readObject();
								}
								Object object = method.invoke(null, objects);
								if (object != null) {
									if (!(object instanceof Number)) {
										if (object instanceof String) {
											outputPacket.writeByte(2, -124);
											outputPacket.writePJStr1((String) object, (byte) 113);
										} else {
											outputPacket.writeByte(4, 101);
										}
									} else {
										outputPacket.writeByte(1, i + -53);
										outputPacket.method1221(i + -65, ((Number) object).longValue());
									}
								} else {
									outputPacket.writeByte(0, 115);
								}
							} else if (opCode == 4) {
								Method method = (Method) in.methodRequests[inPtr].result;
								int modifiers = method.getModifiers();
								outputPacket.writeByte(0, 48);
								outputPacket.writeInt(1571862888, modifiers);
							}
						} catch (ClassNotFoundException classnotfoundexception) {
							outputPacket.writeByte(-10, i + -20);
						} catch (java.io.InvalidClassException invalidclassexception) {
							outputPacket.writeByte(-11, 85);
						} catch (java.io.StreamCorruptedException streamcorruptedexception) {
							outputPacket.writeByte(-12, -87);
						} catch (java.io.OptionalDataException optionaldataexception) {
							outputPacket.writeByte(-13, 79);
						} catch (IllegalAccessException illegalaccessexception) {
							outputPacket.writeByte(-14, i ^ ~0x72);
						} catch (IllegalArgumentException illegalargumentexception) {
							outputPacket.writeByte(-15, i ^ ~0x76);
						} catch (java.lang.reflect.InvocationTargetException invocationtargetexception) {
							outputPacket.writeByte(-16, 125);
						} catch (SecurityException securityexception) {
							outputPacket.writeByte(-17, 43);
						} catch (IOException ioexception) {
							outputPacket.writeByte(-18, 48);
						} catch (NullPointerException nullpointerexception) {
							outputPacket.writeByte(-19, 87);
						} catch (Exception exception) {
							outputPacket.writeByte(-20, 70);
						} catch (Throwable throwable) {
							outputPacket.writeByte(-21, -120);
						}
					}
				}
				outputPacket.addCrc(outStart, (byte) -32);
				in.unlink(95);
			}
		}
	}
}

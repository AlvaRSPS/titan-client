/* Class111 - Decompiled by JODE
 */ package com.jagex.game.toolkit.matrix; /*
					*/

import com.Char;
import com.Class226;
import com.Class314;
import com.Class98_Sub10_Sub30;
import com.Class98_Sub25;

public abstract class Matrix {
	public static byte clanChatRank;

	public static final int indexOf(String name, int i) {
		if (name == null) {
			return -1;
		}
		for (int index = 0; (Class314.friendListSize ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
			if (name.equalsIgnoreCase(Class98_Sub25.friends[index])) {
				return index;
			}
		}
		return -1;
	}

	public static final boolean method2095(int i, int i_9_, byte i_10_) {
		return (i_9_ & 0x20 ^ 0xffffffff) != -1;
	}

	public static final void method2098(Char animable) {
		Class98_Sub10_Sub30.activeToolkit.H(animable.boundExtentsX, animable.anInt5089 + (animable.method2990(0) >> 1), animable.boundExtentsZ, Class226.anIntArray1699);
		animable.anInt5085 = Class226.anIntArray1699[0];
		animable.anInt5080 = Class226.anIntArray1699[1];
		animable.anInt5083 = Class226.anIntArray1699[2];
	}

	public Matrix() {
		/* empty */
	}

	public abstract void initIdentity();

	public abstract void initRotX(int i);

	public abstract void initRotY(int i);

	public abstract void initRotZ(int i);

	public abstract void method2090(int i);

	public abstract void method2092(Matrix class111_2_);

	public abstract void method2093(int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_);

	public abstract void method2096(int i, int i_11_, int i_12_, int[] is);

	public abstract void method2099(int i, int i_13_, int i_14_, int[] is);

	public abstract void method2100(int i, int i_15_, int i_16_);

	public abstract void method2101(int i);

	abstract Matrix method2102();

	public abstract void method2103(int i, int i_17_, int i_18_, int[] is);

	public abstract void method2107(int i);

	public abstract void method2108(int[] is);

	public abstract void translate(int i, int i_19_, int i_20_);
}

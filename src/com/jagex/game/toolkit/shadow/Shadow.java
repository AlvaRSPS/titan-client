/* r - Decompiled by JODE
 */ package com.jagex.game.toolkit.shadow; /*
											*/

import com.Class3;
import com.Class333;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub38;
import com.GZipDecompressor;
import com.SceneGraphNodeList;
import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.definition.AnimationDefinition;

public abstract class Shadow extends Cacheable {
	public static final void method1641(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		int i_7_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_5_);
		int i_8_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_0_);
		int i_9_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_1_);
		int i_10_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_3_);
		int i_11_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, i_5_ + i_2_);
		int i_12_ = GZipDecompressor.method3219(false, SceneGraphNodeList.anInt1635, Class98_Sub10_Sub38.anInt5753, -i_2_ + i_0_);
		for (int i_13_ = i_7_; (i_11_ ^ 0xffffffff) < (i_13_ ^ 0xffffffff); i_13_++) {
			Class333.method3761(i, AnimationDefinition.anIntArrayArray814[i_13_], i_9_, i_10_, (byte) 103);
		}
		if (i_4_ == -18907) {
			for (int i_14_ = i_8_; (i_14_ ^ 0xffffffff) < (i_12_ ^ 0xffffffff); i_14_--) {
				Class333.method3761(i, AnimationDefinition.anIntArrayArray814[i_14_], i_9_, i_10_, (byte) 126);
			}
			int i_15_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_1_ - -i_2_);
			int i_16_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_3_ - i_2_);
			for (int i_17_ = i_11_; i_12_ >= i_17_; i_17_++) {
				int[] is = AnimationDefinition.anIntArrayArray814[i_17_];
				Class333.method3761(i, is, i_9_, i_15_, (byte) 112);
				Class333.method3761(i_6_, is, i_15_, i_16_, (byte) -128);
				Class333.method3761(i, is, i_16_, i_10_, (byte) 3);
			}
		}
	}

	public Shadow() {
		/* empty */
	}
}

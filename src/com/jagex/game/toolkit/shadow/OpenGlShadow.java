/* r_Sub1 - Decompiled by JODE
 */ package com.jagex.game.toolkit.shadow; /*
											*/

import com.Class138;
import com.Class284_Sub1;
import com.Class98_Sub46_Sub19;
import com.Class98_Sub9;
import com.OpenGlToolkit;
import com.OutputStream_Sub2;
import com.SystemInformation;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.definition.RenderAnimDefinition;

public final class OpenGlShadow extends Shadow {
	public static AdvancedMemoryCache	aClass79_6321	= new AdvancedMemoryCache(10);
	public static long					aLong6322		= -1L;

	public static final void fillMap(byte i, byte[] map, int remaining, int end, int index, int start) {
		if (start < end) {
			index += start;
			remaining = -start + end >> 93881506;
			while (--remaining >= 0) {
				map[index++] = (byte) 1;
				map[index++] = (byte) 1;
				map[index++] = (byte) 1;
				map[index++] = (byte) 1;
			}
			remaining = -start + end & 0x3;
			while (--remaining >= 0) {
				map[index++] = (byte) 1;
			}
		}
	}

	public static final boolean method1642(byte i) {
		if (Class98_Sub46_Sub19.aClass98_Sub46_Sub8_6066 == null) {
			return false;
		}
		if (Class98_Sub46_Sub19.aClass98_Sub46_Sub8_6066.actionId >= 2000) {
			Class98_Sub46_Sub19.aClass98_Sub46_Sub8_6066.actionId -= 2000;
		}
		return (Class98_Sub46_Sub19.aClass98_Sub46_Sub8_6066.actionId ^ 0xffffffff) == -1007;
	}

	public static final void method1645(int i) {
		OutputStream_Sub2.anIntArray42 = null;
		SystemInformation.anIntArray1175 = null;
		Class98_Sub9.aBoolean3851 = false;
		Class284_Sub1.anIntArray5178 = null;
		Class138.hueBuffer = null;
		RenderAnimDefinition.anIntArray2406 = null;
	}

	public byte[]	map;

	public int		x;

	public int		length;

	public int		z;

	public int		width;

	public OpenGlShadow(OpenGlToolkit toolkit, int i, int i_10_) {
		map = new byte[i_10_ * i];
	}

	public final void clear(int i) {
		int index = -1;
		int length = map.length - 8;
		while (index < length) {
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
		}
		while ((map.length + -1 ^ 0xffffffff) < (index ^ 0xffffffff)) {
			map[++index] = (byte) 0;
		}
	}

	public final boolean containsRegion(int i, byte i_8_, int i_9_) {
		return (map.length ^ 0xffffffff) <= (i * i_9_ ^ 0xffffffff);
	}

	public final void fill(int i, int start, int _end, int _index, int index, int i_15_, int end) {
		int i_18_ = 0;
		if ((index ^ 0xffffffff) != (i ^ 0xffffffff)) {
			i_18_ = (start + -_end << 308809648) / (-i + index);
		}
		int i_19_ = 0;
		if (_index != index) {
			i_19_ = (-start + end << -1744910128) / (-index + _index);
		}
		int i_20_ = 0;
		if ((_index ^ 0xffffffff) != (i ^ 0xffffffff)) {
			i_20_ = (_end + -end << 478406896) / (-_index + i);
		}
		if ((i ^ 0xffffffff) >= (index ^ 0xffffffff) && i <= _index) {
			if (_index > index) {
				end = _end <<= 1202729712;
				if (i < 0) {
					_end -= i_18_ * i;
					end -= i * i_20_;
					i = 0;
				}
				start <<= 132460240;
				if ((index ^ 0xffffffff) > -1) {
					start -= i_19_ * index;
					index = 0;
				}
				if ((i ^ 0xffffffff) != (index ^ 0xffffffff) && i_18_ > i_20_ || (i ^ 0xffffffff) == (index ^ 0xffffffff) && (i_20_ ^ 0xffffffff) < (i_19_ ^ 0xffffffff)) {
					_index -= index;
					index -= i;
					i *= length;
					while (--index >= 0) {
						fillMap((byte) -104, map, 0, _end >> 2088693072, i, end >> -1493619120);
						i += length;
						end += i_20_;
						_end += i_18_;
					}
					while ((--_index ^ 0xffffffff) <= -1) {
						fillMap((byte) -104, map, 0, start >> 1016237104, i, end >> 43486800);
						end += i_20_;
						start += i_19_;
						i += length;
					}
				} else {
					_index -= index;
					index -= i;
					i *= length;
					while (--index >= 0) {
						fillMap((byte) -104, map, 0, end >> -70451696, i, _end >> -569292944);
						_end += i_18_;
						i += length;
						end += i_20_;
					}
					while ((--_index ^ 0xffffffff) <= -1) {
						fillMap((byte) -104, map, 0, end >> 1090756528, i, start >> -376719216);
						start += i_19_;
						end += i_20_;
						i += length;
					}
				}
			} else {
				start = _end <<= -2138871568;
				end <<= -1328825456;
				if ((i ^ 0xffffffff) > -1) {
					start -= i_20_ * i;
					_end -= i_18_ * i;
					i = 0;
				}
				if (_index < 0) {
					end -= _index * i_19_;
					_index = 0;
				}
				if (((i ^ 0xffffffff) == (_index ^ 0xffffffff) || i_20_ >= i_18_) && ((_index ^ 0xffffffff) != (i ^ 0xffffffff) || i_18_ >= i_19_)) {
					index -= _index;
					_index -= i;
					i *= length;
					while (--_index >= 0) {
						fillMap((byte) -104, map, 0, start >> 1658514640, i, _end >> -493710704);
						start += i_20_;
						i += length;
						_end += i_18_;
					}
					while (--index >= 0) {
						fillMap((byte) -104, map, 0, end >> 875406544, i, _end >> 968472208);
						end += i_19_;
						_end += i_18_;
						i += length;
					}
				} else {
					index -= _index;
					_index -= i;
					i *= length;
					while ((--_index ^ 0xffffffff) <= -1) {
						fillMap((byte) -104, map, 0, _end >> 1630435120, i, start >> 860712432);
						start += i_20_;
						i += length;
						_end += i_18_;
					}
					while (--index >= 0) {
						fillMap((byte) -104, map, 0, _end >> 215894032, i, end >> 2015104304);
						_end += i_18_;
						end += i_19_;
						i += length;
					}
				}
			}
		} else if (_index < index) {
			if (i >= index) {
				_end = end <<= 374585904;
				start <<= 1905005264;
				if ((_index ^ 0xffffffff) > -1) {
					_end -= i_19_ * _index;
					end -= i_20_ * _index;
					_index = 0;
				}
				if ((index ^ 0xffffffff) > -1) {
					start -= index * i_18_;
					index = 0;
				}
				if (i_19_ >= i_20_) {
					i -= index;
					index -= _index;
					_index *= length;
					while ((--index ^ 0xffffffff) <= -1) {
						fillMap((byte) -104, map, 0, _end >> -2063477360, _index, end >> 1547652080);
						end += i_20_;
						_end += i_19_;
						_index += length;
					}
					while ((--i ^ 0xffffffff) <= -1) {
						fillMap((byte) -104, map, 0, start >> -880271024, _index, end >> 1114847408);
						end += i_20_;
						start += i_18_;
						_index += length;
					}
				} else {
					i -= index;
					index -= _index;
					_index *= length;
					while (--index >= 0) {
						fillMap((byte) -104, map, 0, end >> -1610162128, _index, _end >> 844481776);
						end += i_20_;
						_index += length;
						_end += i_19_;
					}
					while ((--i ^ 0xffffffff) <= -1) {
						fillMap((byte) -104, map, 0, end >> -1338436880, _index, start >> -1716563952);
						end += i_20_;
						_index += length;
						start += i_18_;
					}
				}
			} else {
				start = end <<= 800608624;
				if ((_index ^ 0xffffffff) > -1) {
					end -= i_20_ * _index;
					start -= i_19_ * _index;
					_index = 0;
				}
				_end <<= 1504634480;
				if (i < 0) {
					_end -= i_18_ * i;
					i = 0;
				}
				if ((i_20_ ^ 0xffffffff) < (i_19_ ^ 0xffffffff)) {
					index -= i;
					i -= _index;
					_index = length * _index;
					while ((--i ^ 0xffffffff) <= -1) {
						fillMap((byte) -104, map, 0, end >> -1957735056, _index, start >> 1141100560);
						_index += length;
						start += i_19_;
						end += i_20_;
					}
					while (--index >= 0) {
						fillMap((byte) -104, map, 0, _end >> 1458762608, _index, start >> -491951696);
						start += i_19_;
						_index += length;
						_end += i_18_;
					}
				} else {
					index -= i;
					i -= _index;
					_index = length * _index;
					while (--i >= 0) {
						fillMap((byte) -104, map, 0, start >> -441923792, _index, end >> -1844532944);
						_index += length;
						end += i_20_;
						start += i_19_;
					}
					while (--index >= 0) {
						fillMap((byte) -104, map, 0, start >> -1096286768, _index, _end >> -2134156272);
						_index += length;
						start += i_19_;
						_end += i_18_;
					}
				}
			}
		} else if (i > _index) {
			_end = start <<= 1231068944;
			end <<= 1066666256;
			if (index < 0) {
				start -= i_19_ * index;
				_end -= index * i_18_;
				index = 0;
			}
			if (_index < 0) {
				end -= _index * i_20_;
				_index = 0;
			}
			if (_index != index && i_19_ > i_18_ || _index == index && i_20_ < i_18_) {
				i -= _index;
				_index -= index;
				index = length * index;
				while ((--_index ^ 0xffffffff) <= -1) {
					fillMap((byte) -104, map, 0, start >> 704910512, index, _end >> 114452112);
					_end += i_18_;
					index += length;
					start += i_19_;
				}
				while ((--i ^ 0xffffffff) <= -1) {
					fillMap((byte) -104, map, 0, end >> 876486832, index, _end >> 928878736);
					_end += i_18_;
					end += i_20_;
					index += length;
				}
			} else {
				i -= _index;
				_index -= index;
				index *= length;
				while (--_index >= 0) {
					fillMap((byte) -104, map, 0, _end >> -1290116336, index, start >> -1781790608);
					index += length;
					start += i_19_;
					_end += i_18_;
				}
				while (--i >= 0) {
					fillMap((byte) -104, map, 0, _end >> 192479824, index, end >> -1290160400);
					index += length;
					_end += i_18_;
					end += i_20_;
				}
			}
		} else {
			end = start <<= -1766173488;
			if (index < 0) {
				start -= index * i_19_;
				end -= index * i_18_;
				index = 0;
			}
			_end <<= -1121160880;
			if ((i ^ 0xffffffff) > -1) {
				_end -= i * i_20_;
				i = 0;
			}
			if (i_18_ >= i_19_) {
				_index -= i;
				i -= index;
				index *= length;
				while (--i >= 0) {
					fillMap((byte) -104, map, 0, end >> 1933867888, index, start >> -281985552);
					start += i_19_;
					index += length;
					end += i_18_;
				}
				while (--_index >= 0) {
					fillMap((byte) -104, map, 0, _end >> -1913628528, index, start >> -825231248);
					index += length;
					_end += i_20_;
					start += i_19_;
				}
			} else {
				_index -= i;
				i -= index;
				index *= length;
				while (--i >= 0) {
					fillMap((byte) -104, map, 0, start >> 1066305936, index, end >> -378739344);
					start += i_19_;
					index += length;
					end += i_18_;
				}
				while ((--_index ^ 0xffffffff) <= -1) {
					fillMap((byte) -104, map, 0, start >> -448370448, index, _end >> 842419952);
					start += i_19_;
					index += length;
					_end += i_20_;
				}
			}
		}
	}

	public final void setBounds(int z, int maxX, int maxZ, int x, int i_3_) {
		width = -x + maxX;
		this.x = x;
		length = -z + maxZ;
		this.z = z;
	}
}

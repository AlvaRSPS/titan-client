/* r_Sub2 - Decompiled by JODE
 */ package com.jagex.game.toolkit.shadow; /*
											*/

import com.Char;
import com.Class38;
import com.NativeToolkit;
import com.OutgoingOpcode;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;
import com.jagex.game.toolkit.ground.OpenGlGround;

public final class NativeShadow extends Shadow {
	public static OutgoingOpcode	aClass171_6330	= new OutgoingOpcode(77, -1);
	public static Class38			aClass38_6334	= new Class38();
	public static int				anInt6333;

	public static final int encodedStringLength(String string, byte i) {
		return string.length() - -1;
	}

	public static final void fillMap(byte i, int index, int start, int end, int remaining, byte[] map) {
		if (start < end) {
			index += start;
			remaining = end - start >> 2;

			while (--remaining >= 0) {
				map[index++] = (byte) 1;
				map[index++] = (byte) 1;
				map[index++] = (byte) 1;
				map[index++] = (byte) 1;
			}

			remaining = end - start & 0x3;
			while (--remaining >= 0) {
				map[index++] = (byte) 1;
			}
		}
	}

	public static final boolean method1655(int i, byte i_17_, int i_18_) {
		if (i_17_ > -120) {
			return true;
		}
		return OpenGlGround.method3432(i, (byte) 114, i_18_) & ScalingSpriteLoadingScreenElement.method176(24578, i, i_18_);
	}

	public static final void method1656(Char[] class246_sub3s, int i, int i_19_) {
		if (i < i_19_) {
			int i_20_ = (i + i_19_) / 2;
			int i_21_ = i;
			Char class246_sub3 = class246_sub3s[i_20_];
			class246_sub3s[i_20_] = class246_sub3s[i_19_];
			class246_sub3s[i_19_] = class246_sub3;
			int i_22_ = class246_sub3.anInt5083;
			for (int i_23_ = i; i_23_ < i_19_; i_23_++) {
				if (class246_sub3s[i_23_].anInt5083 > i_22_ + (i_23_ & 0x1)) {
					Char class246_sub3_24_ = class246_sub3s[i_23_];
					class246_sub3s[i_23_] = class246_sub3s[i_21_];
					class246_sub3s[i_21_++] = class246_sub3_24_;
				}
			}
			class246_sub3s[i_19_] = class246_sub3s[i_21_];
			class246_sub3s[i_21_] = class246_sub3;
			method1656(class246_sub3s, i, i_21_ - 1);
			method1656(class246_sub3s, i_21_ + 1, i_19_);
		}
	}

	public static final void method1657(int i, byte[] is, int i_25_, int i_26_, int i_27_, int i_28_, int i_29_, byte[] is_30_, int i_31_) {
		int i_32_ = -(i_28_ >> -110374878);
		i_28_ = -(0x3 & i_28_);
		for (int i_33_ = -i_25_; i_33_ < 0; i_33_++) {
			for (int i_34_ = i_32_; i_34_ < 0; i_34_++) {
				is[i_31_++] += is_30_[i++];
				is[i_31_++] += is_30_[i++];
				is[i_31_++] += is_30_[i++];
				is[i_31_++] += is_30_[i++];
			}
			for (int i_35_ = i_28_; (i_35_ ^ 0xffffffff) > -1; i_35_++) {
				is[i_31_++] += is_30_[i++];
			}
			i += i_29_;
			i_31_ += i_27_;
		}
	}

	public byte[]	map;

	public int		x;

	public int		length;

	public int		z;

	public int		width;

	public NativeShadow(NativeToolkit var_ha_Sub3, int i, int i_36_) {
		map = new byte[i_36_ * i];
	}

	public final void clear(int i) {
		int index = -1;
		int length = -8 + map.length;
		while ((index ^ 0xffffffff) > (length ^ 0xffffffff)) {
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
			map[++index] = (byte) 0;
		}
		while (map.length - 1 > index) {
			map[++index] = (byte) 0;
		}
	}

	public final boolean containsRegion(int i, int i_4_, int i_5_) {
		return (map.length ^ 0xffffffff) <= (i_4_ * i ^ 0xffffffff);
	}

	public final void fill(int index, int start, int i_7_, int end, int i_9_, byte i_10_, int i_11_) {
		if (i_10_ == -69) {
			int i_12_ = 0;
			if (i_11_ != index) {
				i_12_ = (-start + i_7_ << -443628592) / (-index + i_11_);
			}
			int i_13_ = 0;
			if (i_11_ != i_9_) {
				i_13_ = (-i_7_ + end << 785005552) / (-i_11_ + i_9_);
			}
			int i_14_ = 0;
			if ((index ^ 0xffffffff) != (i_9_ ^ 0xffffffff)) {
				i_14_ = (start + -end << -747179600) / (-i_9_ + index);
			}
			if ((i_11_ ^ 0xffffffff) > (index ^ 0xffffffff) || index > i_9_) {
				if ((i_9_ ^ 0xffffffff) <= (i_11_ ^ 0xffffffff)) {
					if ((index ^ 0xffffffff) >= (i_9_ ^ 0xffffffff)) {
						end = i_7_ <<= -1005720400;
						start <<= 1054242160;
						if (i_11_ < 0) {
							i_7_ -= i_13_ * i_11_;
							end -= i_11_ * i_12_;
							i_11_ = 0;
						}
						if ((index ^ 0xffffffff) > -1) {
							start -= i_14_ * index;
							index = 0;
						}
						if (i_12_ < i_13_) {
							i_9_ -= index;
							index -= i_11_;
							i_11_ *= length;
							while ((--index ^ 0xffffffff) <= -1) {
								fillMap((byte) 1, i_11_, end >> 1059858032, i_7_ >> 2069463248, 0, map);
								i_11_ += length;
								i_7_ += i_13_;
								end += i_12_;
							}
							while ((--i_9_ ^ 0xffffffff) <= -1) {
								fillMap((byte) 1, i_11_, start >> 120550704, i_7_ >> 1465970480, 0, map);
								i_11_ += length;
								start += i_14_;
								i_7_ += i_13_;
							}
						} else {
							i_9_ -= index;
							index -= i_11_;
							i_11_ = length * i_11_;
							while ((--index ^ 0xffffffff) <= -1) {
								fillMap((byte) 1, i_11_, i_7_ >> -670753872, end >> -481747760, 0, map);
								i_7_ += i_13_;
								i_11_ += length;
								end += i_12_;
							}
							while ((--i_9_ ^ 0xffffffff) <= -1) {
								fillMap((byte) 1, i_11_, i_7_ >> -2068840848, start >> 2101115120, 0, map);
								i_11_ += length;
								start += i_14_;
								i_7_ += i_13_;
							}
						}
					} else {
						start = i_7_ <<= 82412240;
						end <<= -2011332048;
						if (i_11_ < 0) {
							i_7_ -= i_11_ * i_13_;
							start -= i_11_ * i_12_;
							i_11_ = 0;
						}
						if ((i_9_ ^ 0xffffffff) > -1) {
							end -= i_14_ * i_9_;
							i_9_ = 0;
						}
						if (i_9_ != i_11_ && (i_12_ ^ 0xffffffff) > (i_13_ ^ 0xffffffff) || i_9_ == i_11_ && (i_14_ ^ 0xffffffff) > (i_12_ ^ 0xffffffff)) {
							index -= i_9_;
							i_9_ -= i_11_;
							i_11_ *= length;
							while (--i_9_ >= 0) {
								fillMap((byte) 1, i_11_, start >> 1993822576, i_7_ >> -51441968, 0, map);
								i_7_ += i_13_;
								i_11_ += length;
								start += i_12_;
							}
							while ((--index ^ 0xffffffff) <= -1) {
								fillMap((byte) 1, i_11_, start >> -1614605648, end >> -1609116880, 0, map);
								start += i_12_;
								end += i_14_;
								i_11_ += length;
							}
						} else {
							index -= i_9_;
							i_9_ -= i_11_;
							i_11_ *= length;
							while (--i_9_ >= 0) {
								fillMap((byte) 1, i_11_, i_7_ >> -24007280, start >> 797325680, 0, map);
								start += i_12_;
								i_11_ += length;
								i_7_ += i_13_;
							}
							while (--index >= 0) {
								fillMap((byte) 1, i_11_, end >> 1015000624, start >> 1871830288, 0, map);
								start += i_12_;
								i_11_ += length;
								end += i_14_;
							}
						}
					}
				} else if ((index ^ 0xffffffff) <= (i_11_ ^ 0xffffffff)) {
					start = end <<= 1675497904;
					i_7_ <<= -180783632;
					if (i_9_ < 0) {
						start -= i_13_ * i_9_;
						end -= i_9_ * i_14_;
						i_9_ = 0;
					}
					if ((i_11_ ^ 0xffffffff) > -1) {
						i_7_ -= i_11_ * i_12_;
						i_11_ = 0;
					}
					if (i_13_ >= i_14_) {
						index -= i_11_;
						i_11_ -= i_9_;
						i_9_ *= length;
						while (--i_11_ >= 0) {
							fillMap((byte) 1, i_9_, end >> 904296848, start >> 871105648, 0, map);
							i_9_ += length;
							start += i_13_;
							end += i_14_;
						}
						while (--index >= 0) {
							fillMap((byte) 1, i_9_, end >> 1374751856, i_7_ >> -493069808, 0, map);
							i_9_ += length;
							i_7_ += i_12_;
							end += i_14_;
						}
					} else {
						index -= i_11_;
						i_11_ -= i_9_;
						i_9_ *= length;
						while ((--i_11_ ^ 0xffffffff) <= -1) {
							fillMap((byte) 1, i_9_, start >> 588920496, end >> 1994599568, 0, map);
							end += i_14_;
							start += i_13_;
							i_9_ += length;
						}
						while ((--index ^ 0xffffffff) <= -1) {
							fillMap((byte) 1, i_9_, i_7_ >> 454894448, end >> 317092656, 0, map);
							end += i_14_;
							i_7_ += i_12_;
							i_9_ += length;
						}
					}
				} else {
					i_7_ = end <<= 1122623632;
					start <<= -88820752;
					if (i_9_ < 0) {
						i_7_ -= i_13_ * i_9_;
						end -= i_14_ * i_9_;
						i_9_ = 0;
					}
					if ((index ^ 0xffffffff) > -1) {
						start -= index * i_12_;
						index = 0;
					}
					if ((i_14_ ^ 0xffffffff) >= (i_13_ ^ 0xffffffff)) {
						i_11_ -= index;
						index -= i_9_;
						i_9_ = length * i_9_;
						while ((--index ^ 0xffffffff) <= -1) {
							fillMap((byte) 1, i_9_, end >> -1048394864, i_7_ >> -429300688, 0, map);
							i_9_ += length;
							end += i_14_;
							i_7_ += i_13_;
						}
						while (--i_11_ >= 0) {
							fillMap((byte) 1, i_9_, start >> -2103570032, i_7_ >> 1396136368, 0, map);
							i_9_ += length;
							i_7_ += i_13_;
							start += i_12_;
						}
					} else {
						i_11_ -= index;
						index -= i_9_;
						i_9_ = length * i_9_;
						while ((--index ^ 0xffffffff) <= -1) {
							fillMap((byte) 1, i_9_, i_7_ >> -322633904, end >> 955734320, 0, map);
							i_7_ += i_13_;
							end += i_14_;
							i_9_ += length;
						}
						while ((--i_11_ ^ 0xffffffff) <= -1) {
							fillMap((byte) 1, i_9_, i_7_ >> 504672272, start >> 46642864, 0, map);
							i_9_ += length;
							i_7_ += i_13_;
							start += i_12_;
						}
					}
				}
			} else if (i_9_ <= i_11_) {
				i_7_ = start <<= 825934864;
				end <<= 502840432;
				if ((index ^ 0xffffffff) > -1) {
					start -= i_12_ * index;
					i_7_ -= index * i_14_;
					index = 0;
				}
				if (i_9_ < 0) {
					end -= i_9_ * i_13_;
					i_9_ = 0;
				}
				if ((index ^ 0xffffffff) != (i_9_ ^ 0xffffffff) && i_14_ < i_12_ || (index ^ 0xffffffff) == (i_9_ ^ 0xffffffff) && (i_13_ ^ 0xffffffff) < (i_12_ ^ 0xffffffff)) {
					i_11_ -= i_9_;
					i_9_ -= index;
					index = length * index;
					while ((--i_9_ ^ 0xffffffff) <= -1) {
						fillMap((byte) 1, index, i_7_ >> -2091813776, start >> 146157264, 0, map);
						i_7_ += i_14_;
						start += i_12_;
						index += length;
					}
					while (--i_11_ >= 0) {
						fillMap((byte) 1, index, end >> 1605633872, start >> 118924656, 0, map);
						index += length;
						start += i_12_;
						end += i_13_;
					}
				} else {
					i_11_ -= i_9_;
					i_9_ -= index;
					index = length * index;
					while ((--i_9_ ^ 0xffffffff) <= -1) {
						fillMap((byte) 1, index, start >> 580937424, i_7_ >> -1796960496, 0, map);
						i_7_ += i_14_;
						index += length;
						start += i_12_;
					}
					while (--i_11_ >= 0) {
						fillMap((byte) 1, index, start >> 2001540368, end >> -1393449104, 0, map);
						index += length;
						start += i_12_;
						end += i_13_;
					}
				}
			} else {
				end = start <<= 1281760752;
				if (index < 0) {
					start -= i_12_ * index;
					end -= index * i_14_;
					index = 0;
				}
				i_7_ <<= -1708312816;
				if (i_11_ < 0) {
					i_7_ -= i_11_ * i_13_;
					i_11_ = 0;
				}
				if (((index ^ 0xffffffff) == (i_11_ ^ 0xffffffff) || i_14_ >= i_12_) && ((index ^ 0xffffffff) != (i_11_ ^ 0xffffffff) || i_14_ <= i_13_)) {
					i_9_ -= i_11_;
					i_11_ -= index;
					index *= length;
					while (--i_11_ >= 0) {
						fillMap((byte) 1, index, start >> 1230470320, end >> -1985593040, 0, map);
						index += length;
						end += i_14_;
						start += i_12_;
					}
					while ((--i_9_ ^ 0xffffffff) <= -1) {
						fillMap((byte) 1, index, i_7_ >> 52576112, end >> 411287696, 0, map);
						index += length;
						i_7_ += i_13_;
						end += i_14_;
					}
				} else {
					i_9_ -= i_11_;
					i_11_ -= index;
					index = length * index;
					while (--i_11_ >= 0) {
						fillMap((byte) 1, index, end >> 1095249744, start >> 265258096, 0, map);
						start += i_12_;
						index += length;
						end += i_14_;
					}
					while (--i_9_ >= 0) {
						fillMap((byte) 1, index, end >> 1255153136, i_7_ >> -1907768848, 0, map);
						i_7_ += i_13_;
						index += length;
						end += i_14_;
					}
				}
			}
		}
	}

	public final void setBounds(int i, int x, int i_1_, int z, int maxZ) {
		length = maxZ + -z;
		width = -x + i;
		this.x = x;
		this.z = z;
	}
}

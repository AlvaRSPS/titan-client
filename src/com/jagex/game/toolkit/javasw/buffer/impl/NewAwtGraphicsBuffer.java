
/* Class98_Sub32_Sub2 - Decompiled by JODE
 */ package com.jagex.game.toolkit.javasw.buffer.impl; /*
														*/

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.Hashtable;

import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;

@SuppressWarnings("UnusedDeclaration")
public final class NewAwtGraphicsBuffer extends GraphicsBuffer {
	private Canvas		canvas;
	private Rectangle	clipRectangle;
	private Image		image;
	private Shape		oldClip;

	@Override
	public final void drawImage(Graphics graphics, int distX, byte i_2_, int srcX, int width, int srcY, int height, int distY) {
		if (i_2_ == -125) {
			oldClip = graphics.getClip();
			clipRectangle.width = width;
			clipRectangle.x = distX;
			clipRectangle.y = distY;
			clipRectangle.height = height;
			graphics.setClip(clipRectangle);
			graphics.drawImage(image, distX + -srcX, -srcY + distY, canvas);
			graphics.setClip(oldClip);
		}
	}

	@Override
	public final void initialize(int height, int width, int i_1_, Canvas canvas) {
		this.canvas = canvas;
		clipRectangle = new Rectangle();
		this.height = height;
		this.width = width;
		pixels = new int[this.height * this.width];
		DataBufferInt buffer = new DataBufferInt(pixels, pixels.length);
		DirectColorModel model = new DirectColorModel(32, 0xff0000, 0x00ff00, 0x0000ff);
		WritableRaster raster = Raster.createWritableRaster(model.createCompatibleSampleModel(this.width, this.height), buffer, null);
		image = new BufferedImage(model, raster, false, new Hashtable());
	}
}


/* Class98_Sub32_Sub1 - Decompiled by JODE
 */ package com.jagex.game.toolkit.javasw.buffer.impl; /*
														*/

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Shape;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageProducer;

import com.Class155;
import com.IncomingOpcode;
import com.OutgoingOpcode;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;

public final class AwtGraphicsBuffer extends GraphicsBuffer implements ImageProducer {
	public static Class155[]		aClass155Array5889;
	public static OutgoingOpcode	aClass171_5887	= new OutgoingOpcode(84, 4);
	public static IncomingOpcode	aClass58_5883	= new IncomingOpcode(3, 5);
	public static float				aFloat5886;

	private Canvas					canvas;
	private Image					image;
	private ImageConsumer			imageConsumer;

	private ColorModel				rgb24ColourModel;

	@Override
	public final synchronized void addConsumer(ImageConsumer imageconsumer) {
		imageConsumer = imageconsumer;
		imageconsumer.setDimensions(this.width, this.height);
		imageconsumer.setProperties(null);
		imageconsumer.setColorModel(rgb24ColourModel);
		imageconsumer.setHints(14);
	}

	@Override
	public final void drawImage(Graphics graphics, int i, byte i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		this.updateConsumerPixels((byte) -75, i_3_, i_2_, i_1_, i_4_);
		Shape shape = graphics.getClip();
		graphics.clipRect(i, i_5_, i_2_, i_4_);
		graphics.drawImage(image, i - i_1_, -i_3_ + i_5_, canvas);
		graphics.setClip(shape);
	}

	@Override
	public final void initialize(int i, int i_6_, int i_7_, Canvas canvas) {
		this.width = i_6_;
		this.height = i;
		this.canvas = canvas;
		this.pixels = new int[this.height * this.width];
		rgb24ColourModel = new DirectColorModel(32, 16711680, 65280, 255);
		image = this.canvas.createImage(this);
		this.updateConsumerPixels((byte) 115);
		this.canvas.prepareImage(image, this.canvas);
		this.updateConsumerPixels((byte) 115);
		this.canvas.prepareImage(image, this.canvas);
		this.updateConsumerPixels((byte) 115);
		this.canvas.prepareImage(image, this.canvas);
	}

	@Override
	public final synchronized boolean isConsumer(ImageConsumer imageconsumer) {
		return imageConsumer == imageconsumer;
	}

	@Override
	public final synchronized void removeConsumer(ImageConsumer imageconsumer) {
		if (imageconsumer == imageConsumer) {
			imageConsumer = null;
		}
	}

	@Override
	public final void requestTopDownLeftRightResend(ImageConsumer imageconsumer) {
		/* empty */
	}

	@Override
	public final void startProduction(ImageConsumer imageconsumer) {
		addConsumer(imageconsumer);
	}

	private final synchronized void updateConsumerPixels(byte i) {
		if (imageConsumer != null) {
			imageConsumer.setPixels(0, 0, this.width, this.height, rgb24ColourModel, this.pixels, 0, this.width);
			imageConsumer.imageComplete(2);
		}
	}

	private final synchronized void updateConsumerPixels(byte i, int i_8_, int i_9_, int i_10_, int i_11_) {
		if (imageConsumer != null) {
			imageConsumer.setPixels(i_10_, i_8_, i_9_, i_11_, rgb24ColourModel, this.pixels, this.width * i_8_ - -i_10_, this.width);
			imageConsumer.imageComplete(2);
		}
	}
}

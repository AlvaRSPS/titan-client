
/* Class98_Sub32 - Decompiled by JODE
 */ package com.jagex.game.toolkit.javasw.buffer; /*
													*/

import java.awt.Canvas;
import java.awt.Graphics;

import com.Class151_Sub1;
import com.Class33;
import com.WhirlpoolGenerator;
import com.OutgoingOpcode;
import com.RtInterface;
import com.client;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5FileRequest;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;

public abstract class GraphicsBuffer extends Node {
	public static OutgoingOpcode	aClass171_4106;
	public static RtInterface		aClass293_4107	= null;
	public static int[]				anIntArray4109	= new int[5];
	public static int[][]			anIntArrayArray4111;

	static {
		aClass171_4106 = new OutgoingOpcode(26, -1);
		anIntArrayArray4111 = new int[][] { { 12, 12, 12, 12 }, { 12, 12, 12, 12, 12, 5 }, { 5, 5, 1, 1 }, { 5, 1, 1, 5 }, { 5, 5, 5 }, { 5, 5, 5 }, { 12, 12, 12, 12, 12, 12 }, { 1, 12, 12, 12, 12, 12 }, { 1, 1, 7, 1 }, { 8, 9, 9, 8, 8, 3, 1, 9 }, { 8, 8, 9, 8, 9, 9 }, { 10, 10, 11, 11, 11, 7, 3,
				7 }, { 12, 12, 12, 12 } };
	}

	public static final GraphicsBuffer create(int height, int width, Canvas canvas, byte i_1_) {
		try {
			Class<?> type = Class.forName("com.jagex.game.toolkit.javasw.buffer.impl.NewAwtGraphicsBuffer");
			GraphicsBuffer graphBuffer = (GraphicsBuffer) type.newInstance();
			graphBuffer.initialize(height, width, 4095, canvas);
			return graphBuffer;
		} catch (Throwable throwable) {
			AwtGraphicsBuffer graphicsBuffer = new AwtGraphicsBuffer();
			graphicsBuffer.initialize(height, width, 4095, canvas);
			return graphicsBuffer;
		}
	}

	public static final int method1433(int i, int i_0_, int i_1_, int i_2_) {
		i_0_ &= 0x3;
		if (i_0_ == 0) {
			return i_1_;
		}
		if (i_0_ == 1) {
			return i;
		}
		if (i_0_ == 2) {
			return -i_1_ + 4095;
		}
		return -i + 4095;
	}

	public static final String method1435(String string, String string_9_, byte i, char c) {
		int i_10_ = string.length();
		int i_11_ = string_9_.length();
		int i_12_ = i_10_;
		int i_13_ = -1 + i_11_;
		if (i_13_ != 0) {
			int i_14_ = 0;
			for (;;) {
				i_14_ = string.indexOf(c, i_14_);
				if (i_14_ < 0) {
					break;
				}
				i_14_++;
				i_12_ += i_13_;
			}
		}
		StringBuffer stringbuffer = new StringBuffer(i_12_);
		int i_15_ = 0;
		for (;;) {
			int i_16_ = string.indexOf(c, i_15_);
			if (i_16_ < 0) {
				break;
			}
			stringbuffer.append(string.substring(i_15_, i_16_));
			i_15_ = i_16_ - -1;
			stringbuffer.append(string_9_);
		}
		stringbuffer.append(string.substring(i_15_));
		return stringbuffer.toString();
	}

	public static final void method1436(int i, boolean bool) {
		client.preferences.setPreference((byte) -13, 0, client.preferences.aClass64_Sub3_4041);
		client.preferences.setPreference((byte) -13, 0, client.preferences.aClass64_Sub3_4076);
		client.preferences.setPreference((byte) -13, 1, client.preferences.removeRoofs);
		client.preferences.setPreference((byte) -13, 1, client.preferences.removeRoofsMode);
		client.preferences.setPreference((byte) -13, 0, client.preferences.groundDecoration);
		client.preferences.setPreference((byte) -13, 0, client.preferences.fog);
		client.preferences.setPreference((byte) -13, 0, client.preferences.groundBlending);
		client.preferences.setPreference((byte) -13, 0, client.preferences.idleAnimations);
		client.preferences.setPreference((byte) -13, 0, client.preferences.flickeringEffects);
		client.preferences.setPreference((byte) -13, 0, client.preferences.characterShadows);
		client.preferences.setPreference((byte) -13, 0, client.preferences.sceneryShadows);
		client.preferences.setPreference((byte) -13, 0, client.preferences.textures);
		client.preferences.setPreference((byte) -13, 0, client.preferences.lightningDetail);
		client.preferences.setPreference((byte) -13, 0, client.preferences.waterDetail);
		client.preferences.setPreference((byte) -13, 0, client.preferences.antiAliasing);
		client.preferences.setPreference((byte) -13, 0, client.preferences.multiSample);
		client.preferences.setPreference((byte) -13, 0, client.preferences.particles);
		client.preferences.setPreference((byte) -13, 0, client.preferences.buildArea);
		client.preferences.setPreference((byte) -13, 0, client.preferences.unknown3);
		Class151_Sub1.method2450((byte) 103);
		client.preferences.setPreference((byte) -13, 2, client.preferences.maxScreenSize);
		client.preferences.setPreference((byte) -13, 1, client.preferences.graphicsLevel);
		Js5FileRequest.method1593((byte) 76);
		if (i > -77) {
			method1435(null, null, (byte) 62, 'n');
		}
		WhirlpoolGenerator.method3980((byte) 121);
		Class33.aBoolean316 = true;
	}

	public static final int method1438(String string, int i) {
		int i_25_ = string.length();
		int i_26_ = 0;
		for (int i_27_ = 0; (i_25_ ^ 0xffffffff) < (i_27_ ^ 0xffffffff); i_27_++) {
			i_26_ = -i_26_ + (i_26_ << -672898683) + string.charAt(i_27_);
		}
		if (i != 6243) {
			method1436(-52, false);
		}
		return i_26_;
	}

	public int		height;

	public int[]	pixels;

	public int		width;

	public GraphicsBuffer() {
		/* empty */
	}

	public abstract void drawImage(Graphics graphics, int i, byte i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_);

	public abstract void initialize(int height, int width, int i_34_, Canvas canvas);
}

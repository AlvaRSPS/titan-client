/* za - Decompiled by JODE
 */ package com.jagex.game.toolkit.heap; /*
					*/

import com.Class3;
import com.Class48_Sub2_Sub1;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub38;
import com.SceneGraphNodeList;
import com.StringConcatenator;
import com.jagex.core.collections.Node;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;

public abstract class Heap extends Node {

	public static final int method1674(int i, int i_0_) {
		if (i != -1035933944) {
			return -95;
		}
		return i_0_ >>> -1035933944;
	}

	public static final void method1675(int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, byte i_7_) {
		if (i_2_ == i) {
			StringConcatenator.method3247(i_3_, i_6_, i_5_, i_4_, i, i_1_, 1333849452);
		} else {
			if (-i + i_5_ >= Class76_Sub8.anInt3778 && i_5_ + i <= Class3.anInt77 && (Class98_Sub10_Sub38.anInt5753 ^ 0xffffffff) >= (i_3_ + -i_2_ ^ 0xffffffff) && i_3_ + i_2_ <= SceneGraphNodeList.anInt1635) {
				OrthoZoomPreferenceField.method624(i_6_, -2211, i_5_, i_1_, i, i_3_, i_4_, i_2_);
			} else {
				Class48_Sub2_Sub1.method473(i_3_, i_1_, i_5_, i_2_, i_4_, i, -25682, i_6_);
			}
		}
	}
}

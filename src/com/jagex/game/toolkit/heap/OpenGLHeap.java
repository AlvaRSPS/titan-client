
/* za_Sub2 - Decompiled by JODE
 */ package com.jagex.game.toolkit.heap; /*
											*/

import com.Class176;
import com.Class185;
import com.Class3;
import com.Class76_Sub8;
import com.Class98_Sub10_Sub38;
import com.Class98_Sub46_Sub17;
import com.GameShell;
import com.ItemDeque;
import com.SceneGraphNodeList;
import com.jagex.game.client.archive.Archive;

import jaclib.memory.heap.NativeHeap;

public final class OpenGLHeap extends Heap {
	public static boolean	aBoolean6079		= false;
	public static double	aDouble6081;
	public static int		localPlayerIndex	= -1;

	public static final void addChatMessage(String string, int i, byte i_5_) {
		ItemDeque.addChatMessage((byte) 122, i, string, 0, "", "", "");
	}

	public static final void method1680(int i, int i_0_, int i_1_) {
		if (i_1_ == 9767) {
			Class98_Sub46_Sub17 dialogue = Class185.method2628(i_0_, -68, 6);
			dialogue.method1626((byte) -103);
			dialogue.anInt6054 = i;
		}
	}

	public static final boolean method1682(int i, int i_2_, int i_3_) {
		return (i & 0x800) != 0;
	}

	public static final boolean method1683(int i, int i_4_) {
		if (i != -11297) {
			return false;
		}
		return !(i_4_ != 10 && (i_4_ ^ 0xffffffff) != -12 && i_4_ != 12);
	}

	public static final void method1685(int i, int i_7_, int i_8_, boolean bool, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_) {
		do {
			if (i_7_ >= Class76_Sub8.anInt3778 && Class3.anInt77 >= i_7_ && (Class76_Sub8.anInt3778 ^ 0xffffffff) >= (i ^ 0xffffffff) && (i ^ 0xffffffff) >= (Class3.anInt77 ^ 0xffffffff) && (Class76_Sub8.anInt3778 ^ 0xffffffff) >= (i_8_ ^ 0xffffffff) && (Class3.anInt77 ^ 0xffffffff) <= (i_8_
					^ 0xffffffff) && i_14_ >= Class76_Sub8.anInt3778 && (Class3.anInt77 ^ 0xffffffff) <= (i_14_ ^ 0xffffffff) && i_13_ >= Class98_Sub10_Sub38.anInt5753 && (i_13_ ^ 0xffffffff) >= (SceneGraphNodeList.anInt1635 ^ 0xffffffff) && Class98_Sub10_Sub38.anInt5753 <= i_10_ && (i_10_
							^ 0xffffffff) >= (SceneGraphNodeList.anInt1635 ^ 0xffffffff) && i_9_ >= Class98_Sub10_Sub38.anInt5753 && SceneGraphNodeList.anInt1635 >= i_9_ && (Class98_Sub10_Sub38.anInt5753 ^ 0xffffffff) >= (i_11_ ^ 0xffffffff) && (SceneGraphNodeList.anInt1635 ^ 0xffffffff) <= (i_11_
									^ 0xffffffff)) {
				Class176.method2579(i_13_, i_9_, i_11_, i, i_7_, i_12_, i_8_, i_14_, i_10_, 22024);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			Archive.method3788(i_7_, i_12_, i_9_, i, (byte) -67, i_14_, i_8_, i_11_, i_13_, i_10_);
		} while (false);
	}

	public NativeHeap heap;

	public OpenGLHeap(int i) {
		heap = new NativeHeap(i);
	}

	public final void deallocate(byte i) {
		heap.deallocate();
	}
}

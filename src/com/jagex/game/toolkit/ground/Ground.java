/* s - Decompiled by JODE
 */ package com.jagex.game.toolkit.ground; /*
											*/

import com.PointLight;
import com.jagex.game.toolkit.shadow.Shadow;

public abstract class Ground {
	public static int[] anIntArray2205;

	public static final boolean method3419(int i, int i_14_) {
		return !((i_14_ ^ 0xffffffff) > -5 || (i_14_ ^ 0xffffffff) < -9);
	}

	public int		tileScale;
	public int		width;
	public int		length;

	public int		tileUnits;

	public int[][]	tileHeights;

	public Ground(int i, int i_57_, int i_58_, int[][] is) {
		width = i;
		length = i_57_;
		int i_59_ = 0;
		for (/**/; i_58_ > 1; i_58_ >>= 1) {
			i_59_++;
		}
		tileUnits = 1 << i_59_;
		tileHeights = is;
		tileScale = i_59_;
	}

	public abstract void addBlendedTile(int i, int i_18_, int[] is, int[] is_19_, int[] is_20_, int[] is_21_, int[] is_22_, int[] is_23_, int[] is_24_, int[] is_25_, int i_26_, int i_27_, int i_28_, boolean bool);

	public abstract void addLight(PointLight class98_sub5, int[] is);

	public final int averageHeight(int i, int i_4_, boolean bool) {
		int i_5_ = i >> tileScale;
		if (bool != true) {
			return -46;
		}
		int i_6_ = i_4_ >> tileScale;
		if ((i_5_ ^ 0xffffffff) > -1 || i_6_ < 0 || width + -1 < i_5_ || i_6_ > -1 + length) {
			return 0;
		}
		int i_7_ = i & -1 + tileUnits;
		int i_8_ = i_4_ & tileUnits - 1;
		int i_9_ = (-i_7_ + tileUnits) * tileHeights[i_5_][i_6_] + i_7_ * tileHeights[1 + i_5_][i_6_] >> tileScale;
		int i_10_ = (-i_7_ + tileUnits) * tileHeights[i_5_][1 + i_6_] - -(tileHeights[i_5_ - -1][1 + i_6_] * i_7_) >> tileScale;
		return i_8_ * i_10_ + (tileUnits - i_8_) * i_9_ >> tileScale;
	}

	public abstract void deleteShadow(Shadow var_r, int i, int i_38_, int i_39_, int i_40_, boolean bool);

	public abstract void finish();

	public final int getTileHeight(int i, int i_16_, int i_17_) {
		if (i_16_ != -12639) {
			method3425(110, 4);
		}
		return tileHeights[i_17_][i];
	}

	public abstract void method3416(int i, int i_0_, int i_1_, boolean[][] bools, boolean bool, int i_2_, int i_3_);

	public abstract boolean method3418(Shadow var_r, int i, int i_11_, int i_12_, int i_13_, boolean bool);

	public abstract void method3424(int i, int i_41_, int[] is, int[] is_42_, int[] is_43_, int[] is_44_, int[] is_45_, int[] is_46_, int[] is_47_, int[] is_48_, int[] is_49_, int[] is_50_, int[] is_51_, int i_52_, int i_53_, int i_54_, boolean bool);

	public abstract void method3425(int i, int i_55_);

	public abstract void method3426(int i, int i_60_, int i_61_, boolean[][] bools, boolean bool, int i_62_);

	public abstract void renderPlain(int i, int i_29_, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_, boolean[][] bools);

	public abstract Shadow replaceShadow(int i, int i_56_, Shadow var_r);

	public abstract void setShadowing(int i, int i_63_, int i_64_);

	public abstract void addShadow(Shadow var_r, int i, int i_35_, int i_36_, int i_37_, boolean bool);
}

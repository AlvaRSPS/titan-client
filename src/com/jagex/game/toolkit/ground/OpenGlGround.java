
/* s_Sub1 - Decompiled by JODE
 */ package com.jagex.game.toolkit.ground; /*
											*/

import com.ArrayBuffer;
import com.NativeOpenGlElementArrayBuffer;
import com.Class208;
import com.Class23;
import com.Class246_Sub3_Sub4;
import com.Class248;
import com.Class254;
import com.Class255;
import com.Class33;
import com.Class331;
import com.Class346;
import com.Class353;
import com.Class41;
import com.Class46;
import com.Class98_Sub10_Sub29;
import com.Class98_Sub10_Sub38;
import com.OpenGlPointLight;
import com.Class98_Sub48;
import com.FriendLoginUpdate;
import com.GameShell;
import com.IncomingOpcode;
import com.ItemDeque;
import com.JavaNetworkReader;
import com.NodeShort;
import com.NodeString;
import com.OpenGlArrayBufferPointer;
import com.OpenGlTileMaterial;
import com.OpenGlToolkit;
import com.OutgoingOpcode;
import com.OutgoingPacket;
import com.PointLight;
import com.RSByteBuffer;
import com.RSToolkit;
import com.RsFloatBuffer;
import com.TextureMetricsList;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.toolkit.shadow.NativeShadow;
import com.jagex.game.toolkit.shadow.OpenGlShadow;
import com.jagex.game.toolkit.shadow.Shadow;

import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeapBuffer;
import jaggl.OpenGL;

public final class OpenGlGround extends Ground {
	public static boolean			aBoolean5200;
	public static boolean			aBoolean5207	= true;
	public static OutgoingOpcode	aClass171_1424	= new OutgoingOpcode(20, -1);
	public static Class346			aClass346_5202;
	public static IncomingOpcode	aClass58_5205	= new IncomingOpcode(101, 6);

	public static final boolean method3427(int i, int i_35_, int i_36_, int i_37_, byte i_38_, int i_39_, int i_40_) {
		int i_41_ = i_35_ + i_40_;
		int i_42_ = i - -i_36_;
		int i_43_ = i_37_ + i_39_;
		if (!Class254.method3187(i_42_, i_40_, i_39_, (byte) 82, i_42_, i_43_, i_42_, i_43_, i_41_, i_40_)) {
			return false;
		}
		if (!Class254.method3187(i_42_, i_41_, i_39_, (byte) 82, i_42_, i_43_, i_42_, i_39_, i_41_, i_40_)) {
			return false;
		}
		if (i_40_ < JavaNetworkReader.anInt1018) {
			if (!Class254.method3187(i_42_, i_40_, i_43_, (byte) 82, i, i_43_, i_42_, i_39_, i_40_, i_40_)) {
				return false;
			}
			if (!Class254.method3187(i, i_40_, i_43_, (byte) 82, i, i_39_, i_42_, i_39_, i_40_, i_40_)) {
				return false;
			}
		} else {
			if (!Class254.method3187(i_42_, i_41_, i_43_, (byte) 82, i, i_43_, i_42_, i_39_, i_41_, i_41_)) {
				return false;
			}
			if (!Class254.method3187(i, i_41_, i_43_, (byte) 82, i, i_39_, i_42_, i_39_, i_41_, i_41_)) {
				return false;
			}
		}
		if (i_38_ != 16) {
			return true;
		}
		if (Class98_Sub48.anInt4280 <= i_39_) {
			if (!Class254.method3187(i_42_, i_40_, i_43_, (byte) 82, i, i_43_, i_42_, i_43_, i_41_, i_40_)) {
				return false;
			}
			if (!Class254.method3187(i, i_41_, i_43_, (byte) 82, i, i_43_, i_42_, i_43_, i_41_, i_40_)) {
				return false;
			}
		} else {
			if (!Class254.method3187(i_42_, i_40_, i_39_, (byte) 82, i, i_39_, i_42_, i_39_, i_41_, i_40_)) {
				return false;
			}
			if (!Class254.method3187(i, i_41_, i_39_, (byte) 82, i, i_39_, i_42_, i_39_, i_41_, i_40_)) {
				return false;
			}
		}
		return true;
	}

	public static final boolean method3432(int i, byte i_217_, int i_218_) {
		return false;
	}

	public static final boolean method3433(int i, int i_220_, int i_221_) {
		if (i_220_ != 15849) {
			return false;
		}
		return false;
	}

	public static final void method3434(Js5 class207, boolean bool, int i, int i_222_, int i_223_, int i_224_) {
		NodeString.method1144(i, i_224_ + 16527, bool, i_223_, class207, i_222_, 0L);
	}

	public static final void removeIgnore(String name, int i) {
		if (name != null) {
			if (name.startsWith("*")) {
				name = name.substring(1);
			}
			String filteredIgnoreName = Class353.filterName(-1, name);
			if (filteredIgnoreName != null) {
				for (int ignore = 0; ignore < Class248.ignoreListSize; ignore++) {
					String ignoreName = FriendLoginUpdate.ignores[ignore];
					if (ignoreName.startsWith("*")) {
						ignoreName = ignoreName.substring(1);
					}
					ignoreName = Class353.filterName(-1, ignoreName);
					if (ignoreName != null && ignoreName.equals(filteredIgnoreName)) {
						Class248.ignoreListSize--;
						for (int index = ignore; (Class248.ignoreListSize ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
							FriendLoginUpdate.ignores[index] = FriendLoginUpdate.ignores[1 + index];
							Class255.ignoreNames[index] = Class255.ignoreNames[1 + index];
							ItemDeque.ignoreDisplayNames[index] = ItemDeque.ignoreDisplayNames[index + 1];
							VerticalAlignment.ignoreDisplayNames[index] = VerticalAlignment.ignoreDisplayNames[1 + index];
							Class98_Sub10_Sub38.aBooleanArray5759[index] = Class98_Sub10_Sub38.aBooleanArray5759[index + 1];
						}
						NewsFetcher.anInt3099 = WorldMapInfoDefinition.logicTimer;
						OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(i + 24255, aClass171_1424, Class331.aClass117_2811);
						frame.packet.writeByte(NativeShadow.encodedStringLength(name, (byte) 106), i + 24104);
						frame.packet.writePJStr1(name, (byte) 113);
						Class98_Sub10_Sub29.sendPacket(false, frame);
						break;
					}
				}
			}
		}
	}

	private byte[][]					shadowFlags;
	private byte[][]					shadowing;
	public OpenGlArrayBufferPointer		normalPointer;
	private OpenGlArrayBufferPointer	colourPointer;
	public OpenGlArrayBufferPointer		vertexPointer;
	public OpenGlArrayBufferPointer		texturePointer;
	private LinkedList					lighters	= new LinkedList();
	private Class33						aClass33_5209;
	private HashTable					materialTable;
	private OpenGlTileMaterial[]		groundMaterials;
	private OpenGlTileMaterial[][][]	materials;
	private float[][]					lightningY;
	private float[][]					lightningZ;
	private float[][]					lightningX;
	public OpenGlToolkit				toolkit;
	private int							quarterTileScale;
	public int							flags;
	private int							anInt5203;
	private int							quarterTileUnits;
	private int							anInt5211;
	private int							maximumVertices;
	private int							vertices;
	public int[][][]					tileXOffsets;

	private int[][][]					anIntArrayArrayArray5193;

	public int[][][]					tileZOffsets;

	private int[][][]					tileBlendColours;

	private int[][][]					anIntArrayArrayArray5208;

	public int[][][]					tileColours;

	private ArrayBuffer					data;

	public short[][]					materialIndices;

	public OpenGlGround(OpenGlToolkit toolkit, int dummy, int flags, int width, int length, int[][] heights, int[][] lightning, int units) {
		super(width, length, units, heights);
		do {
			quarterTileScale = -2 + tileScale;
			this.toolkit = toolkit;
			tileZOffsets = new int[width][length][];
			tileColours = new int[width][length][];
			tileXOffsets = new int[width][length][];
			materialIndices = new short[length * width][];
			shadowing = new byte[1 + width][1 + length];
			anIntArrayArrayArray5193 = new int[width][length][];
			quarterTileUnits = 1 << quarterTileScale;
			tileBlendColours = new int[width][length][];
			lightningZ = new float[this.width - -1][this.length - -1];
			this.flags = flags;
			lightningY = new float[this.width + 1][this.length - -1];
			materials = new OpenGlTileMaterial[width][length][];
			shadowFlags = new byte[width][length];
			lightningX = new float[this.width - -1][1 + this.length];
			for (int z = 1; this.length > z; z++) {
				for (int x = 1; (x ^ 0xffffffff) > (this.width ^ 0xffffffff); x++) {
					int dz = -lightning[-1 + x][z] + lightning[1 + x][z];
					int dx = lightning[x][1 + z] + -lightning[x][-1 + z];
					float distance = (float) (1.0 / Math.sqrt(dx * dx + 4 * units * units + dz * dz));
					lightningZ[x][z] = distance * dz;
					lightningY[x][z] = 2 * -units * distance;
					lightningX[x][z] = dx * distance;
				}
			}
			materialTable = new HashTable(128);
			if ((flags & 0x10) == 0) {
				break;
			}
			aClass33_5209 = new Class33(toolkit, this);
			break;
		} while (false);
	}

	@Override
	public final void addBlendedTile(int x, int z, int[] offsetsX, int[] is_204_, int[] offsetsZ, int[] is_206_, int[] colours, int[] blendColours, int[] textures, int[] scales, int i_211_, int intensity, int colour, boolean bool) {
		TextureMetricsList metricsList = toolkit.metricsList;
		if (is_204_ != null && anIntArrayArrayArray5193 == null) {
			anIntArrayArrayArray5193 = new int[this.width][this.length][];
		}
		if (is_206_ != null && anIntArrayArrayArray5208 == null) {
			anIntArrayArrayArray5208 = new int[this.width][this.length][];
		}
		tileXOffsets[x][z] = offsetsX;
		tileZOffsets[x][z] = offsetsZ;
		tileColours[x][z] = colours;
		tileBlendColours[x][z] = blendColours;
		if (anIntArrayArrayArray5208 != null) {
			anIntArrayArrayArray5208[x][z] = is_206_;
		}
		if (anIntArrayArrayArray5193 != null) {
			anIntArrayArrayArray5193[x][z] = is_204_;
		}
		OpenGlTileMaterial[] tile = materials[x][z] = new OpenGlTileMaterial[colours.length];
		for (int index = 0; (colours.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
			int texture = textures[index];
			int scale = scales[index];
			if ((flags & 0x20) != 0 && (texture ^ 0xffffffff) != 0 && metricsList.getInfo(texture, -28755).aBoolean1825) {
				texture = -1;
				scale = 128;
			}
			long key = (long) intensity << 1267155370 | (long) colour << 1046159152 | (long) i_211_ << -194355108 | scale << 14 | texture;
			Node class98;
			for (class98 = materialTable.get(key, -1); class98 != null; class98 = materialTable.nextHashCollision(122)) {
				OpenGlTileMaterial class98_sub20 = (OpenGlTileMaterial) class98;
				if (texture == class98_sub20.texture && class98_sub20.scale == scale && class98_sub20.anInt3971 == i_211_ && (intensity ^ 0xffffffff) == (class98_sub20.anInt3974 ^ 0xffffffff) && (colour ^ 0xffffffff) == (class98_sub20.anInt3975 ^ 0xffffffff)) {
					break;
				}
			}
			if (class98 != null) {
				tile[index] = (OpenGlTileMaterial) class98;
			} else {
				tile[index] = new OpenGlTileMaterial(this, texture, scale, i_211_, intensity, colour);
				materialTable.put(tile[index], key, -1);
			}
		}
		if (bool) {
			shadowFlags[x][z] = (byte) Class41.or(shadowFlags[x][z], 1);
		}
		if ((colours.length ^ 0xffffffff) < (maximumVertices ^ 0xffffffff)) {
			maximumVertices = colours.length;
		}
		vertices += colours.length;
	}

	@Override
	public final void addLight(PointLight light, int[] data) {
		lighters.addLast(new OpenGlPointLight(toolkit, this, light, data), -20911);
	}

	@Override
	public final void deleteShadow(Shadow shadow, int x, int y, int z, int i_94_, boolean bool) {
		if (aClass33_5209 != null && shadow != null) {
			int projectionX = -(toolkit.sunProjectionX * y >> -1029072408) + x >> toolkit.shadowScale;
			int projectionZ = z - (y * toolkit.sunProjectionZ >> -1852845016) >> toolkit.shadowScale;
			aClass33_5209.delete((byte) 94, projectionZ, projectionX, shadow);
		}
	}

	@Override
	public final void finish() {
		if ((vertices ^ 0xffffffff) >= -1) {
			aClass33_5209 = null;
		} else {
			byte[][] shadows = new byte[1 + this.width][this.length - -1];
			for (int x = 1; (x ^ 0xffffffff) > (this.width ^ 0xffffffff); x++) {
				for (int z = 1; (z ^ 0xffffffff) > (this.length ^ 0xffffffff); z++) {
					shadows[x][z] = (byte) ((shadowing[x - 1][z] >> 2) + (shadowing[x + 1][z] >> 3) + (shadowing[x][z - 1] >> 2) + (shadowing[x][z + 1] >> 3) + (shadowing[x][z] >> 1));
				}
			}
			groundMaterials = new OpenGlTileMaterial[materialTable.size((byte) -6)];
			materialTable.getValues(groundMaterials, (byte) 74);
			for (int index = 0; (groundMaterials.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				groundMaterials[index].init(vertices, (byte) -64);
			}
			int stride = 24;
			if (anIntArrayArrayArray5208 != null) {
				stride += 4;
			}
			if ((flags & 0x7) != 0) {
				stride += 12;
			}
			NativeHeapBuffer buffer = toolkit.heap.allocate(stride * vertices, false);
			Stream stream = new Stream(buffer);
			OpenGlTileMaterial[] active = new OpenGlTileMaterial[vertices];
			int size = Class23.priorPowerOf2(47, vertices / 4);
			if (size < 1) {
				size = 1;
			}
			HashTable table = new HashTable(size);
			OpenGlTileMaterial[] unique = new OpenGlTileMaterial[maximumVertices];
			for (int x = 0; this.width > x; x++) {
				for (int z = 0; z < this.length; z++) {
					if (tileColours[x][z] != null) {
						OpenGlTileMaterial[] tile = materials[x][z];
						int[] xOffsets = tileXOffsets[x][z];
						int[] zOffsets = tileZOffsets[x][z];
						int[] blendColours = tileBlendColours[x][z];
						int[] colours = tileColours[x][z];
						int[] is_132_ = anIntArrayArrayArray5193 == null ? null : anIntArrayArrayArray5193[x][z];
						int[] is_133_ = anIntArrayArrayArray5208 == null ? null : anIntArrayArrayArray5208[x][z];
						if (blendColours == null) {
							blendColours = colours;
						}
						float lZ = lightningZ[x][z];
						float lY = lightningY[x][z];
						float lX = lightningX[x][z];
						float nlZ = lightningZ[x][1 + z];
						float nlY = lightningY[x][z - -1];
						float nlX = lightningX[x][1 + z];
						float nelZ = lightningZ[1 + x][z - -1];
						float nelY = lightningY[x + 1][1 + z];
						float nelX = lightningX[x - -1][z - -1];
						float elZ = lightningZ[1 + x][z];
						float elY = lightningY[1 + x][z];
						float elX = lightningX[x - -1][z];
						int shadow = 0xff & shadows[x][z];
						int newShadow = 0xff & shadows[x][z - -1];
						int newEShadow = shadows[1 + x][z + 1] & 0xff;
						int eShadow = shadows[x - -1][z] & 0xff;
						int count = 0;
						while_137_: for (int index = 0; (index ^ 0xffffffff) > (colours.length ^ 0xffffffff); index++) {
							OpenGlTileMaterial material = tile[index];
							for (int u = 0; count > u; u++) {
								if (material == unique[u]) {
									continue while_137_;
								}
							}
							unique[count++] = material;
						}
						short[] indices = materialIndices[((Ground) this).width * z - -x] = new short[colours.length];
						for (int index = 0; (colours.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
							int vertexX = (x << this.tileScale) + xOffsets[index];
							int vertexZ = (z << this.tileScale) - -zOffsets[index];
							int quarterX = vertexX >> quarterTileScale;
							int quarterZ = vertexZ >> quarterTileScale;
							int colour = colours[index];
							int blendColour = blendColours[index];
							int i_160_ = is_132_ != null ? is_132_[index] : 0;
							long key = quarterZ | (long) blendColour << 48 | (long) colour << 32 | quarterX << 16;
							int dx = xOffsets[index];
							int dz = zOffsets[index];
							int i_163_ = 74;
							int litColour = 0;
							float intensity = 1.0F;
							float f_166_;
							float f_167_;
							float f_168_;
							if ((dx ^ 0xffffffff) != -1 || (dz ^ 0xffffffff) != -1) {
								if ((dx ^ 0xffffffff) != -1 || (dz ^ 0xffffffff) != (this.tileUnits ^ 0xffffffff)) {
									if ((this.tileUnits ^ 0xffffffff) != (dx ^ 0xffffffff) || (dz ^ 0xffffffff) != (this.tileUnits ^ 0xffffffff)) {
										if (dx == this.tileUnits && dz == 0) {
											f_168_ = elZ;
											i_163_ -= eShadow;
											f_167_ = elX;
											f_166_ = elY;
										} else {
											float displacementX = (float) dx / (float) this.tileUnits;
											float displacementZ = (float) dz / (float) this.tileUnits;
											float f_171_ = lZ + (-lZ + elZ) * displacementX;
											float f_172_ = displacementX * (elY - lY) + lY;
											float f_173_ = (elX - lX) * displacementX + lX;
											float f_174_ = displacementX * (nelZ - nlZ) + nlZ;
											float f_175_ = nlY + (nelY - nlY) * displacementX;
											float f_176_ = (-nlX + nelX) * displacementX + nlX;
											f_166_ = (f_175_ - f_172_) * displacementZ + f_172_;
											f_168_ = displacementZ * (-f_171_ + f_174_) + f_171_;
											f_167_ = (f_176_ - f_173_) * displacementZ + f_173_;
											int i_177_ = shadow + (dx * (-shadow + eShadow) >> this.tileScale);
											int i_178_ = newShadow + (dx * (newEShadow - newShadow) >> this.tileScale);
											i_163_ -= i_177_ - -((-i_177_ + i_178_) * dz >> this.tileScale);
										}
									} else {
										f_166_ = nelY;
										f_167_ = nelX;
										i_163_ -= newEShadow;
										f_168_ = nelZ;
									}
								} else {
									f_166_ = nlY;
									f_168_ = nlZ;
									f_167_ = nlX;
									i_163_ -= newShadow;
								}
							} else {
								f_166_ = lY;
								i_163_ -= shadow;
								f_167_ = lX;
								f_168_ = lZ;
							}
							if (colour != -1) {
								int lightning = (0x7f & colour) * i_163_ >> 633955783;
								if (lightning < 2) {
									lightning = 2;
								} else if (lightning > 126) {
									lightning = 126;
								}
								if ((flags & 0x7 ^ 0xffffffff) == -1) {
									intensity = f_167_ * toolkit.sunDirection[2] + (f_168_ * toolkit.sunDirection[0] + f_166_ * toolkit.sunDirection[1]);
									intensity = (intensity > 0.0F ? toolkit.sunIntensity : toolkit.aFloat4407) * intensity + toolkit.ambient;
								}
								litColour = Class208.HSL_TABLE[lightning | 0xff80 & colour];
							}
							Node node = null;
							if ((vertexX & -1 + quarterTileUnits ^ 0xffffffff) == -1 && (vertexZ & -1 + quarterTileUnits ^ 0xffffffff) == -1) {
								node = table.get(key, -1);
							}
							int i_180_;
							if (node != null) {
								indices[index] = ((NodeShort) node).aShort4191;
								i_180_ = 0xffff & indices[index];
								if ((colour ^ 0xffffffff) != 0 && active[i_180_].hash > tile[index].hash) {
									active[i_180_] = tile[index];
								}
							} else {
								int i_181_;
								if (blendColour != colour) {
									int i_182_ = (0x7f & blendColour) * i_163_ >> 378317159;
									if (i_182_ < 2) {
										i_182_ = 2;
									} else if (i_182_ > 126) {
										i_182_ = 126;
									}
									i_181_ = Class208.HSL_TABLE[blendColour & 0xff80 | i_182_];
									if ((0x7 & flags) == 0) {
										float f_183_ = f_166_ * toolkit.sunDirection[1] + toolkit.sunDirection[0] * f_168_ + f_167_ * toolkit.sunDirection[2];
										f_183_ = intensity * (!(intensity > 0.0F) ? toolkit.aFloat4407 : toolkit.sunIntensity) + toolkit.ambient;
										int i_184_ = (0xff0cb1 & i_181_) >> 1379966064;
										int i_185_ = (i_181_ & 0xffc7) >> 704482856;
										int i_186_ = 0xff & i_181_;
										i_184_ *= f_183_;
										if (i_184_ >= 0) {
											if (i_184_ > 255) {
												i_184_ = 255;
											}
										} else {
											i_184_ = 0;
										}
										i_185_ *= f_183_;
										i_186_ *= f_183_;
										if ((i_185_ ^ 0xffffffff) > -1) {
											i_185_ = 0;
										} else if (i_185_ > 255) {
											i_185_ = 255;
										}
										if ((i_186_ ^ 0xffffffff) <= -1) {
											if ((i_186_ ^ 0xffffffff) < -256) {
												i_186_ = 255;
											}
										} else {
											i_186_ = 0;
										}
										i_181_ = i_184_ << -630144720 | i_185_ << 1834767400 | i_186_;
									}
								} else {
									i_181_ = litColour;
								}
								if (toolkit.bigEndian) {
									stream.writeBEFloat((float) vertexX);
									stream.writeBEFloat((float) (i_160_ + averageHeight(vertexX, vertexZ, true)));
									stream.writeBEFloat((float) vertexZ);
									stream.writeByte((byte) (i_181_ >> -274242992));
									stream.writeByte((byte) (i_181_ >> -438976888));
									stream.writeByte((byte) i_181_);
									stream.writeByte(-1);
									stream.writeBEFloat((float) vertexX);
									stream.writeBEFloat((float) vertexZ);
									if (anIntArrayArrayArray5208 != null) {
										stream.writeBEFloat((float) (is_133_ != null ? -1 + is_133_[index] : 0));
									}
									if ((0x7 & flags) != 0) {
										stream.writeBEFloat(f_168_);
										stream.writeBEFloat(f_166_);
										stream.writeBEFloat(f_167_);
									}
								} else {
									stream.a((float) vertexX);
									stream.a((float) (i_160_ + averageHeight(vertexX, vertexZ, true)));
									stream.a((float) vertexZ);
									stream.writeByte((byte) (i_181_ >> -177734704));
									stream.writeByte((byte) (i_181_ >> 1011400392));
									stream.writeByte((byte) i_181_);
									stream.writeByte(-1);
									stream.a((float) vertexX);
									stream.a((float) vertexZ);
									if (anIntArrayArrayArray5208 != null) {
										stream.a((float) (is_133_ == null ? 0 : -1 + is_133_[index]));
									}
									if ((flags & 0x7 ^ 0xffffffff) != -1) {
										stream.a(f_168_);
										stream.a(f_166_);
										stream.a(f_167_);
									}
								}
								i_180_ = anInt5203++;
								indices[index] = (short) i_180_;
								if ((colour ^ 0xffffffff) != 0) {
									active[i_180_] = tile[index];
								}
								table.put(new NodeShort(indices[index]), key, -1);
							}
							for (int i_187_ = 0; (count ^ 0xffffffff) < (i_187_ ^ 0xffffffff); i_187_++) {
								unique[i_187_].colour(i_163_, (byte) 77, intensity, litColour, i_180_);
							}
							anInt5211++;
						}
					}
				}
			}
			for (int i_188_ = 0; i_188_ < anInt5203; i_188_++) {
				OpenGlTileMaterial class98_sub20 = active[i_188_];
				if (class98_sub20 != null) {
					class98_sub20.activate(i_188_, true);
				}
			}
			for (int i_189_ = 0; (this.width ^ 0xffffffff) < (i_189_ ^ 0xffffffff); i_189_++) {
				for (int i_190_ = 0; this.length > i_190_; i_190_++) {
					short[] is_191_ = materialIndices[i_189_ + this.width * i_190_];
					if (is_191_ != null) {
						int i_192_ = 0;
						int i_193_ = 0;
						while (i_193_ < is_191_.length) {
							int i_194_ = is_191_[i_193_++] & 0xffff;
							int i_195_ = 0xffff & is_191_[i_193_++];
							int i_196_ = is_191_[i_193_++] & 0xffff;
							OpenGlTileMaterial class98_sub20 = active[i_194_];
							OpenGlTileMaterial class98_sub20_197_ = active[i_195_];
							OpenGlTileMaterial class98_sub20_198_ = active[i_196_];
							OpenGlTileMaterial class98_sub20_199_ = null;
							if (class98_sub20 != null) {
								class98_sub20.flag(i_192_, (byte) 118, i_190_, i_189_);
								class98_sub20_199_ = class98_sub20;
							}
							if (class98_sub20_197_ != null) {
								class98_sub20_197_.flag(i_192_, (byte) 123, i_190_, i_189_);
								if (class98_sub20_199_ == null || class98_sub20_197_.hash < class98_sub20_199_.hash) {
									class98_sub20_199_ = class98_sub20_197_;
								}
							}
							if (class98_sub20_198_ != null) {
								class98_sub20_198_.flag(i_192_, (byte) 125, i_190_, i_189_);
								if (class98_sub20_199_ == null || (class98_sub20_199_.hash ^ 0xffffffffffffffffL) < (class98_sub20_198_.hash ^ 0xffffffffffffffffL)) {
									class98_sub20_199_ = class98_sub20_198_;
								}
							}
							if (class98_sub20_199_ != null) {
								if (class98_sub20 != null) {
									class98_sub20_199_.activate(i_194_, true);
								}
								if (class98_sub20_197_ != null) {
									class98_sub20_199_.activate(i_195_, true);
								}
								if (class98_sub20_198_ != null) {
									class98_sub20_199_.activate(i_196_, true);
								}
								class98_sub20_199_.flag(i_192_, (byte) 126, i_190_, i_189_);
							}
							i_192_++;
						}
					}
				}
			}
			stream.flush();
			data = toolkit.createArrayBuffer(stride, (byte) 86, buffer, false, stream.written());
			vertexPointer = new OpenGlArrayBufferPointer(data, 5126, 3, 0);
			colourPointer = new OpenGlArrayBufferPointer(data, 5121, 4, 12);
			int i_200_;
			if (anIntArrayArrayArray5208 == null) {
				texturePointer = new OpenGlArrayBufferPointer(data, 5126, 2, 16);
				i_200_ = 24;
			} else {
				texturePointer = new OpenGlArrayBufferPointer(data, 5126, 3, 16);
				i_200_ = 28;
			}
			if ((flags & 0x7 ^ 0xffffffff) != -1) {
				normalPointer = new OpenGlArrayBufferPointer(data, 5126, 3, i_200_);
			}
			long[] ls = new long[groundMaterials.length];
			for (int index = 0; (groundMaterials.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				OpenGlTileMaterial class98_sub20 = groundMaterials[index];
				ls[index] = class98_sub20.hash;
				class98_sub20.finish(true, anInt5203);
			}
			Class46.sort(groundMaterials, false, ls);
			if (aClass33_5209 != null) {
				aClass33_5209.method321(119);
			}
		}
		tileColours = null;
		shadowing = null;
		anIntArrayArrayArray5208 = null;
		tileBlendColours = null;
		tileXOffsets = tileZOffsets = null;
		anIntArrayArrayArray5193 = null;
		materials = null;
		materialTable = null;
		lightningZ = lightningY = lightningX = null;
	}

	@Override
	public final void method3416(int i, int i_118_, int i_119_, boolean[][] bools, boolean bool, int i_120_, int i_121_) {
		method3428(i_121_, i_120_, bool, i_119_, i, bools, 11490, i_118_);
	}

	@Override
	public final boolean method3418(Shadow shadow, int i, int y, int i_88_, int i_89_, boolean bool) {
		if (aClass33_5209 == null || shadow == null) {
			return false;
		}
		int i_90_ = i - (y * toolkit.sunProjectionX >> 2105336840) >> toolkit.shadowScale;
		int i_91_ = i_88_ + -(y * toolkit.sunProjectionZ >> 1767505640) >> toolkit.shadowScale;
		return aClass33_5209.method325(shadow, -115, i_90_, i_91_);
	}

	@Override
	public final void method3424(int i, int i_7_, int[] is, int[] is_8_, int[] is_9_, int[] is_10_, int[] is_11_, int[] is_12_, int[] is_13_, int[] is_14_, int[] is_15_, int[] is_16_, int[] is_17_, int i_18_, int i_19_, int i_20_, boolean bool) {
		int i_21_ = is_14_.length;
		int[] is_22_ = new int[3 * i_21_];
		int[] is_23_ = new int[i_21_ * 3];
		int[] is_24_ = new int[3 * i_21_];
		int[] is_25_ = new int[i_21_ * 3];
		int[] is_26_ = new int[3 * i_21_];
		int[] is_27_ = new int[3 * i_21_];
		int[] is_28_ = is_8_ == null ? null : new int[3 * i_21_];
		int[] is_29_ = is_10_ == null ? null : new int[i_21_ * 3];
		int i_30_ = 0;
		for (int i_31_ = 0; i_31_ < i_21_; i_31_++) {
			int i_32_ = is_11_[i_31_];
			int i_33_ = is_12_[i_31_];
			int i_34_ = is_13_[i_31_];
			is_22_[i_30_] = is[i_32_];
			is_23_[i_30_] = is_9_[i_32_];
			is_24_[i_30_] = is_14_[i_31_];
			is_26_[i_30_] = is_16_[i_31_];
			is_27_[i_30_] = is_17_[i_31_];
			is_25_[i_30_] = is_15_ == null ? is_14_[i_31_] : is_15_[i_31_];
			if (is_8_ != null) {
				is_28_[i_30_] = is_8_[i_32_];
			}
			if (is_10_ != null) {
				is_29_[i_30_] = is_10_[i_32_];
			}
			i_30_++;
			is_22_[i_30_] = is[i_33_];
			is_23_[i_30_] = is_9_[i_33_];
			is_24_[i_30_] = is_14_[i_31_];
			is_26_[i_30_] = is_16_[i_31_];
			is_27_[i_30_] = is_17_[i_31_];
			is_25_[i_30_] = is_15_ != null ? is_15_[i_31_] : is_14_[i_31_];
			if (is_8_ != null) {
				is_28_[i_30_] = is_8_[i_33_];
			}
			if (is_10_ != null) {
				is_29_[i_30_] = is_10_[i_33_];
			}
			i_30_++;
			is_22_[i_30_] = is[i_34_];
			is_23_[i_30_] = is_9_[i_34_];
			is_24_[i_30_] = is_14_[i_31_];
			is_26_[i_30_] = is_16_[i_31_];
			is_27_[i_30_] = is_17_[i_31_];
			is_25_[i_30_] = is_15_ != null ? is_15_[i_31_] : is_14_[i_31_];
			if (is_8_ != null) {
				is_28_[i_30_] = is_8_[i_34_];
			}
			if (is_10_ != null) {
				is_29_[i_30_] = is_10_[i_34_];
			}
			i_30_++;
		}
		addBlendedTile(i, i_7_, is_22_, is_28_, is_23_, is_29_, is_24_, is_25_, is_26_, is_27_, i_18_, i_19_, i_20_, bool);
	}

	@Override
	public final void method3425(int i, int i_202_) {
		/* empty */
	}

	@Override
	public final void method3426(int i, int i_225_, int i_226_, boolean[][] bools, boolean bool, int i_227_) {
		method3428(i_227_, -1, bool, i_226_, i, bools, 11490, i_225_);
	}

	private final void method3428(int i, int i_44_, boolean bool, int i_45_, int i_46_, boolean[][] bools, int i_47_, int i_48_) {
		do {
			if (groundMaterials != null) {
				int i_49_ = 1 + i_45_ + i_45_;
				i_49_ *= i_49_;
				if ((i_49_ ^ 0xffffffff) < (toolkit.anIntArray4468.length ^ 0xffffffff)) {
					toolkit.anIntArray4468 = new int[i_49_];
				}
				if (anInt5211 * 2 > toolkit.dataBuffer.payload.length) {
					toolkit.dataBuffer = new RsFloatBuffer(2 * anInt5211);
				}
				int i_50_ = -i_45_ + i_46_;
				int i_51_ = i_50_;
				if (i_50_ < 0) {
					i_50_ = 0;
				}
				int i_52_ = i_48_ - i_45_;
				int i_53_ = i_52_;
				if (i_52_ < 0) {
					i_52_ = 0;
				}
				int i_54_ = i_46_ + i_45_;
				if (i_54_ > -1 + this.width) {
					i_54_ = -1 + this.width;
				}
				int i_55_ = i_45_ + i_48_;
				if (this.length - 1 < i_55_) {
					i_55_ = this.length + -1;
				}
				int i_56_ = 0;
				int[] is = toolkit.anIntArray4468;
				for (int i_57_ = i_50_; (i_54_ ^ 0xffffffff) <= (i_57_ ^ 0xffffffff); i_57_++) {
					boolean[] bools_58_ = bools[i_57_ - i_51_];
					for (int i_59_ = i_52_; (i_55_ ^ 0xffffffff) <= (i_59_ ^ 0xffffffff); i_59_++) {
						if (bools_58_[-i_53_ + i_59_]) {
							is[i_56_++] = i_59_ * this.width - -i_57_;
						}
					}
				}
				if (i_44_ == -1) {
					toolkit.method1861(19330);
				} else {
					toolkit.method1890(i_44_, true);
					toolkit.method1901((byte) -35);
				}
				toolkit.method1851((flags & 0x7 ^ 0xffffffff) != -1, false);
				for (int i_60_ = 0; (i_60_ ^ 0xffffffff) > (groundMaterials.length ^ 0xffffffff); i_60_++) {
					groundMaterials[i_60_].render(is, (byte) 98, i_56_);
				}
				if (!lighters.method2420(-126)) {
					int i_61_ = toolkit.anInt4455;
					int i_62_ = toolkit.fogDistance;
					toolkit.applyFog(0, i_62_, toolkit.anInt4427);
					toolkit.method1851(false, false);
					toolkit.method1875((byte) -123, false);
					toolkit.setBlendMode((byte) -58, 128);
					toolkit.method1834(-99, -2);
					toolkit.setActiveTexture(1, toolkit.aClass42_Sub1_4358);
					toolkit.method1899(7681, i_47_ ^ 0xfe2, 8448);
					toolkit.method1840(0, 770, 116, 34166);
					toolkit.method1886(770, 0, 34200, 34167);
					for (Node class98 = lighters.getFirst(32); class98 != null; class98 = lighters.getNext(91)) {
						OpenGlPointLight class98_sub37 = (OpenGlPointLight) class98;
						class98_sub37.method1461(i_48_, i_45_, i_46_, bools, (byte) 38);
					}
					toolkit.method1840(0, 768, i_47_ ^ 0x2c88, 5890);
					toolkit.method1886(770, 0, 34200, 5890);
					toolkit.setActiveTexture(1, null);
					toolkit.applyFog(i_61_, i_62_, toolkit.anInt4427);
				}
				if (aClass33_5209 != null) {
					OpenGL.glPushMatrix();
					OpenGL.glTranslatef(0.0F, -1.0F, 0.0F);
					toolkit.setPointers(null, null, texturePointer, vertexPointer, i_47_ + -11490);
					aClass33_5209.method320(bool, i_45_, i_46_, bools, (byte) 93, i_48_);
					OpenGL.glPopMatrix();
				}
			}
			if (i_47_ == 11490) {
				break;
			}
			aBoolean5200 = false;
			break;
		} while (false);
	}

	@Override
	public final void renderPlain(int startPositionX, int startPositionZ, int endZ, int startX, int startZ, int endX, int tileScale, boolean[][] plan) {
		do {
			if ((vertices ^ 0xffffffff) < -1) {
				toolkit.method1867(29458);
				toolkit.method1856(false, 6914);
				toolkit.method1851(false, false);
				toolkit.method1881(false, false);
				toolkit.method1875((byte) 27, false);
				toolkit.setBlendMode((byte) -35, 0);
				toolkit.method1834(-62, -2);
				toolkit.setActiveTexture(1, null);
				HitmarksDefinition.GROUND_MATRIX[4] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[13] = 1.0F - (startPositionZ * 2 + endZ * tileScale / 128.0F) / toolkit.height;
				HitmarksDefinition.GROUND_MATRIX[9] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[3] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[7] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[11] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[12] = -1.0F - (-(2 * startPositionX) + startX * endZ / 128.0F) / toolkit.width;
				HitmarksDefinition.GROUND_MATRIX[15] = 1.0F;
				HitmarksDefinition.GROUND_MATRIX[14] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[10] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[0] = endZ / (toolkit.width * (128.0F * this.tileUnits));
				HitmarksDefinition.GROUND_MATRIX[6] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[8] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[1] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[2] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[5] = endZ / (this.tileUnits * 128.0F * toolkit.height);
				OpenGL.glMatrixMode(5889);
				OpenGL.glLoadMatrixf(HitmarksDefinition.GROUND_MATRIX, 0);
				HitmarksDefinition.GROUND_MATRIX[10] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[7] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[12] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[15] = 1.0F;
				HitmarksDefinition.GROUND_MATRIX[4] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[5] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[14] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[11] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[2] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[8] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[3] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[0] = 1.0F;
				HitmarksDefinition.GROUND_MATRIX[6] = 1.0F;
				HitmarksDefinition.GROUND_MATRIX[1] = 0.0F;
				HitmarksDefinition.GROUND_MATRIX[9] = 1.0F;
				HitmarksDefinition.GROUND_MATRIX[13] = 0.0F;
				OpenGL.glMatrixMode(5888);
				OpenGL.glLoadMatrixf(HitmarksDefinition.GROUND_MATRIX, 0);
				if ((flags & 0x7 ^ 0xffffffff) != -1) {
					toolkit.method1851(true, false);
					toolkit.method1831(126);
				} else {
					toolkit.method1851(false, false);
				}
				toolkit.setPointers(colourPointer, normalPointer, texturePointer, vertexPointer, 0);
				if (toolkit.dataBuffer.payload.length < anInt5211 * 2) {
					toolkit.dataBuffer = new RsFloatBuffer(anInt5211 * 2);
				} else {
					toolkit.dataBuffer.position = 0;
				}
				int count = 0;
				RsFloatBuffer buffer = toolkit.dataBuffer;
				if (toolkit.bigEndian) {
					for (int z = startZ; z < tileScale; z++) {
						int offset = this.width * z - -startX;
						for (int x = startX; (x ^ 0xffffffff) > (endX ^ 0xffffffff); x++) {
							if (plan[-startX + x][-startZ + z]) {
								short[] indices = materialIndices[offset];
								if (indices != null) {
									for (int index = 0; (indices.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
										count++;
										buffer.writeShort(indices[index] & 0xffff, 1571862888);
									}
								}
							}
							offset++;
						}
					}
				} else {
					for (int z = startZ; tileScale > z; z++) {
						int offset = startX + z * this.width;
						for (int x = startX; endX > x; x++) {
							if (plan[x + -startX][-startZ + z]) {
								short[] indices = materialIndices[offset];
								if (indices != null) {
									for (short element : indices) {
										buffer.writeLEShortCorrectOne(element & 0xffff, 4);
										count++;
									}
								}
							}
							offset++;
						}
					}
				}
				if (count <= 0) {
					break;
				}
				NativeOpenGlElementArrayBuffer elements = new NativeOpenGlElementArrayBuffer(toolkit, 5123, buffer.payload, buffer.position);
				toolkit.drawElements(count, 4, elements, false, 0);
			}
			break;
		} while (false);
	}

	@Override
	public final Shadow replaceShadow(int x, int z, Shadow shadow) {
		if ((0x1 & shadowFlags[x][z] ^ 0xffffffff) == -1) {
			return null;
		}
		int units = this.tileUnits >> toolkit.shadowScale;
		OpenGlShadow replacement = (OpenGlShadow) shadow;
		OpenGlShadow result;
		do {
			if (replacement != null && replacement.containsRegion(units, (byte) -122, units)) {
				result = replacement;
				result.clear(93);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			result = new OpenGlShadow(toolkit, units, units);
		} while (false);
		result.setBounds(0, units, units, 0, -1);
		updateShadow(x, z, 0, result);
		return result;
	}

	@Override
	public final void setShadowing(int x, int z, int value) {
		do {
			if ((shadowing[x][z] & 0xff) >= value) {
				break;
			}
			shadowing[x][z] = (byte) value;
			break;
		} while (false);
	}

	private final void updateShadow(int i, int i_97_, int i_98_, OpenGlShadow var_r_Sub1) {
		int[] is = tileXOffsets[i][i_97_];
		int[] is_99_ = tileZOffsets[i][i_97_];
		int i_100_ = is.length;
		if ((i_100_ ^ 0xffffffff) < (toolkit.shadowX.length ^ 0xffffffff)) {
			toolkit.shadowX = new int[i_100_];
			toolkit.shadowY = new int[i_100_];
		}
		int[] is_101_ = toolkit.shadowX;
		int[] is_102_ = toolkit.shadowY;
		for (int i_103_ = 0; i_100_ > i_103_; i_103_++) {
			is_101_[i_103_] = is[i_103_] >> toolkit.shadowScale;
			is_102_[i_103_] = is_99_[i_103_] >> toolkit.shadowScale;
		}
		int i_104_ = i_98_;
		while (i_100_ > i_104_) {
			int i_105_ = is_101_[i_104_];
			int i_106_ = is_102_[i_104_++];
			int i_107_ = is_101_[i_104_];
			int i_108_ = is_102_[i_104_++];
			int i_109_ = is_101_[i_104_];
			int i_110_ = is_102_[i_104_++];
			if ((-i_107_ + i_105_) * (-i_110_ + i_108_) - (i_109_ - i_107_) * (-i_106_ + i_108_) > 0) {
				var_r_Sub1.fill(i_106_, i_107_, i_105_, i_110_, i_108_, i_98_ + -119, i_109_);
			}
		}
	}

	@Override
	public final void addShadow(Shadow shadow, int x, int y, int z, int i_4_, boolean bool) {
		if (aClass33_5209 != null && shadow != null) {
			int projectedX = x - (toolkit.sunProjectionX * y >> 404453736) >> toolkit.shadowScale;
			int projectedZ = z + -(y * toolkit.sunProjectionZ >> -391620984) >> toolkit.shadowScale;
			aClass33_5209.method322(projectedZ, projectedX, false, shadow);
		}
	}
}

/* Class147 - Decompiled by JODE
 */ package com.jagex.game.clanchat; /*
										*/

import com.Class116;
import com.Class151_Sub9;
import com.Class172;
import com.Class191;
import com.Class224_Sub1;
import com.Class248;
import com.Class268;
import com.Class284;
import com.Class38;
import com.Class49;
import com.Class65;
import com.Class76_Sub8;
import com.Class87;
import com.Class98_Sub10_Sub20;
import com.Class98_Sub10_Sub21;
import com.Class98_Sub10_Sub34;
import com.Class98_Sub10_Sub38;
import com.Class98_Sub10_Sub6;
import com.Class98_Sub30;
import com.Class98_Sub46_Sub20_Sub2;
import com.HashTableIterator;
import com.IncomingOpcode;
import com.Mob;
import com.NPC;
import com.OutgoingPacket;
import com.PacketParser;
import com.Player;
import com.PlayerUpdateMask;
import com.ProceduralTextureSource;
import com.RSByteBuffer;
import com.RsBitsBuffers;
import com.aa_Sub3;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class ClanChatMember {
	public static IncomingOpcode	aClass58_1192	= new IncomingOpcode(115, 8);
	public static int				anInt1194;
	public static String[]			currentScript;
	public static int				messageCount	= 0;

	public static final void method2411(int i) {
		Class76_Sub8.anInt3780 = 0;
		Class268.anInt2007 = i;
		for (int i_0_ = 0; HorizontalAlignment.anInt493 > i_0_; i_0_++) {
			int i_1_ = Class191.anInt1477 * i_0_;
			for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > (Class191.anInt1477 ^ 0xffffffff); i_2_++) {
				int i_3_ = i_2_ + i_1_;
				Class172.anInterface17Array1327[i_3_].copyToRenderer(FontSpecifications.anInt1513 * i_2_, Class98_Sub10_Sub38.anInt5761 * i_0_, FontSpecifications.anInt1513, Class98_Sub10_Sub38.anInt5761, 0, 0, true, true);
			}
		}
	}

	public static final void method2412(int i) {
		Class49.method477(-5788);
		Class98_Sub30.aClass58_4094 = null;
		Class98_Sub10_Sub6.anInt5569 = 0;
		Class65.currentPacketSize = 0;
		Class98_Sub10_Sub21.aClass58_5641 = null;
		PacketParser.buffer.position = 0;
		PacketParser.CURRENT_INCOMING_OPCODE = null;
		ProceduralTextureSource.aClass58_3262 = null;
		Class224_Sub1.anInt5031 = 0;
		OrthoZoomPreferenceField.method622((byte) -38);
		Class248.method3159(0);
		for (int i_4_ = 0; (i_4_ ^ 0xffffffff) > -2049; i_4_++) {
			Class151_Sub9.players[i_4_] = null;
		}
		Class87.localPlayer = null;
		for (int i_5_ = 0; i_5_ < Class98_Sub10_Sub20.anInt5640; i_5_++) {
			NPC npc = BackgroundColourLSEConfig.aClass98_Sub39Array3516[i_5_].npc;
			if (npc != null) {
				npc.anInt6364 = -1;
			}
		}
		Class98_Sub10_Sub34.method1106((byte) -61);
		Class116.anInt967 = CharacterShadowsPreferenceField.anInt3712 = -1;
		Class98_Sub46_Sub20_Sub2.cameraMode = 1;
		HashTableIterator.setClientState(10, false);
		for (int i_6_ = 0; i_6_ < 100; i_6_++) {
			aa_Sub3.isDirty[i_6_] = true;
		}
		OutgoingPacket.method1158(-2);
		Class284.aClass98_Sub4_2167 = null;
		CursorDefinitionParser.aLong121 = 0L;
	}

	public static final void method2413(RsBitsBuffers packet, int i) {
		for (int i_7_ = 0; (Class38.DECODE_MASKS_PLAYERS_COUNT ^ 0xffffffff) < (i_7_ ^ 0xffffffff); i_7_++) {
			int i_8_ = Class65.DECODE_MASKS_PLAYERS_INDEXES_LIST[i_7_];
			Player player = Class151_Sub9.players[i_8_];
			int i_9_ = packet.readUnsignedByte((byte) 35);
			if ((0x20 & i_9_) != 0) {
				i_9_ += packet.readUnsignedByte((byte) 36) << -475812536;
			}
			if ((0x800 & i_9_) != 0) {
				i_9_ += packet.readUnsignedByte((byte) 95) << 1299142480;
			}
			PlayerUpdateMask.method709(player, i_9_, packet, (byte) 82, i_8_);
		}
	}

	public String	accountName;
	public byte		chatRank;

	public String	displayName;

	public String	filteredName;

	public String	world;

	public int		worldOrLobbyColour;

	public ClanChatMember() {
		/* empty */
	}
}

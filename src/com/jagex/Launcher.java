
/* RunClient - Decompiled by JODE
 */ package com.jagex; /*
						*/

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import com.GameShell;
import com.client;
import com.jagex.game.client.tools.ScreenShot;
import com.jagex.game.client.tools.Theme;

public class Launcher extends Applet {

	public static boolean		DISABLE_LOADING_SCREEN_IMAGES	= false;
	public static final boolean	DISABLE_RSA						= true;
	public static boolean		DISABLE_CONSOLE_SNOW			= false;
	public static String		mainurl							= "127.0.0.1";		// blaze.maxgamer.org
	public static Properties	params							= new Properties();

	public static boolean		rs								= false;
	private static final long	serialVersionUID				= 1L;
	public static String		SCREENSHOT_PATH					= null;

	private static void createDirectory() throws IOException {
		File path = new File(System.getProperty("user.home") + File.separator + "TitanData");
		if (!path.exists()) {
			path.mkdir();
		}
		File cFile = new File(System.getProperty("user.home") + File.separator + "TitanData" + File.separator + "Constants.txt");
		if (!cFile.exists()) {
			cFile.createNewFile();
		}
		File ssFile = new File(System.getProperty("user.home") + File.separator + "TitanData" + File.separator + "ScreenShots");
		if (!ssFile.exists()) {
			ssFile.mkdir();
		}
		File lFile = new File(System.getProperty("user.home") + File.separator + "TitanData" + File.separator + "Links.txt");
		if (!lFile.exists()) {
			lFile.createNewFile();
		}
		save();
	}

	public static boolean hasDirectory() {
		File path = new File(System.getProperty("user.home") + File.separator + "TitanData"), file = new File(System.getProperty("user.home") + File.separator + "TitanData" + File.separator + "Constants.txt"), file2 = new File(System.getProperty("user.home") + File.separator + "TitanData"
				+ File.separator + "ScreenShots"), lFile = new File(System.getProperty("user.home") + File.separator + "TitanData" + File.separator + "Links.txt");
		return !(!path.exists() || !file.exists() || !file2.exists() || !lFile.exists());
	}

	public static void load() {
		if (!hasDirectory()) {
			try {
				createDirectory();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String line;
		try {
			BufferedReader in = new BufferedReader(new FileReader(System.getProperty("user.home") + File.separator + "TitanData" + File.separator + "Constants.txt"));
			while ((line = in.readLine()) != null) {
				if (line.startsWith("t")) {
					Theme.THEME = line.substring(6);
				} else if (line.startsWith("s")) {
					SCREENSHOT_PATH = line.substring(7);
				}
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] strings) {
		for (int i = 0; i < strings.length; i++) {
			if (strings[i].equalsIgnoreCase("servaddr")) {
				mainurl = strings[++i];
			}
		}
		Launcher runclient = new Launcher();
		runclient.doFrame();
	}

	public static void save() {
		String defaultPath = System.getProperty("user.home") + File.separator + "TitanData" + File.separator + "ScreenShots" + File.separator;
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(System.getProperty("user.home") + File.separator + "TitanData" + File.separator + "Constants.txt", false));
			if (Theme.THEME == null) {
				out.write("org.jvnet.substance.skin.SubstanceEmeraldDuskLookAndFeel");
			} else if (Theme.THEME != null) {
				out.write("theme:" + Theme.THEME);
			}
			out.newLine();
			if (SCREENSHOT_PATH == null) {
				out.write("sspath:" + defaultPath);
			} else if (SCREENSHOT_PATH != null) {
				out.write("sspath:" + SCREENSHOT_PATH);
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String	frameName	= "Titan - Offical Client (Build: Null)";
	public int		lang		= 0;

	public JFrame	jFrame;

	public JPanel	mainPane	= new JPanel();

	public JPanel	totalPanel;

	private Robot	robot;

	public Launcher() {
		try {
			robot = new Robot();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void doApplet() {
		try {
			readVars();
			startClient();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void doFrame() {
		try {
			readVars();
			openFrame();
			startClient();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public URL getCodeBase() {
		URL url;
		try {
			url = new URL(new StringBuilder().append("http://").append(mainurl).toString());
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
		return url;
	}

	@Override
	public URL getDocumentBase() {
		return getCodeBase();
	}

	/**
	 * Retrieves the actual gamescreen
	 * @return
	 */
	private Rectangle getLoc() {
		Rectangle rect = getBounds();
		rect.setLocation(getLocationOnScreen());
		return rect;
	}

	@Override
	public String getParameter(String string) {
		return (String) params.get(string);
	}

	@Override
	public void init() {
		doApplet();
	}

	public void openFrame() {
		load();
		Theme.setTheme();
		JPopupMenu.setDefaultLightWeightPopupEnabled(false);
		mainPane.setLayout(new BorderLayout());
		mainPane.add(this);
		mainPane.setPreferredSize(new Dimension(765, 503));
		totalPanel = new JPanel();
		totalPanel.setPreferredSize(new Dimension(765, 503));
		totalPanel.setLayout(new BorderLayout());
		totalPanel.add(mainPane, BorderLayout.CENTER);
		jFrame = new JFrame(frameName);
		jFrame.setLayout(new BorderLayout());
		//jFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/titan.png")));
		jFrame.getContentPane().add(totalPanel, BorderLayout.CENTER);
		jFrame.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				int confirm = JOptionPane.showOptionDialog(jFrame, "Are you really leaving?", "Exit Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});
		//Menu bar
		JMenuBar menu = new JMenuBar();
		jFrame.setJMenuBar(menu);
		//Screenshot button and listener
		JButton s = new JButton("Capture");
		/*try {
			Image icon = ImageIO.read(getClass().getClassLoader().getResource("./icons/camera.png"));//Icon source path
			s.setIcon(new ImageIcon(icon));//Set the button icon
			s.setMargin(new Insets(0, 0, 0, 0));//Remote spacing between borders and icon
		} catch (IOException e1) {
			e1.printStackTrace();
		}*/
		menu.add(s);//Add button to menu
		//s.setToolTipText("Capture the gamescreen");
		s.addActionListener(e -> new ScreenShot(robot.createScreenCapture(getLoc())));
		//Theme button and listener
		JButton t = new JButton("Theme");
		/*try {
			Image icon = ImageIO.read(getClass().getClassLoader().getResource("./icons/theme.png"));//Icon source path
			t.setIcon(new ImageIcon(icon));//Set the button icon
			t.setMargin(new Insets(0, 0, 0, 0));//Remote spacing between borders and icon
		} catch (IOException e1) {
			e1.printStackTrace();
		}*/
		menu.add(t);
		//t.setToolTipText("Change the Look & Feel of the Client UI");
		t.addActionListener(e -> new Theme());
		//Titan wiki
		JButton w = new JButton("Wiki");
		/*try {
			Image icon = ImageIO.read(getClass().getClassLoader().getResource("./icons/wiki.png"));//Icon source path
			w.setIcon(new ImageIcon(icon));//Set the button icon
			w.setMargin(new Insets(0, 0, 0, 0));//Remote spacing between borders and icon
		} catch (IOException e1) {
			e1.printStackTrace();
		}*/
		menu.add(w);
		//w.setToolTipText("Visit the Titan Wiki for useful and interesting information");
		w.addActionListener(e -> {
			try {
				String wiki = "http://titan.maxgamer.org/index.php/Main_Page";
				Desktop desktop = Desktop.getDesktop();
				URI uri = new URI(wiki);
				desktop.browse(uri.resolve(uri));
			} catch (URISyntaxException ex1) {
				Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex1);
			} catch (IOException ex2) {
				Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex2);
			}
		});
		//Notepad 
		JButton n = new JButton("Notepad");
		/*try {
			Image icon = ImageIO.read(getClass().getClassLoader().getResource("./icons/notepad.png"));//Icon source path
			n.setIcon(new ImageIcon(icon));//Set the button icon
			n.setMargin(new Insets(0, 0, 0, 0));//Remote spacing between borders and icon
		} catch (IOException e1) {
			e1.printStackTrace();
		}*/
		menu.add(n);
		//n.setToolTipText("The classic notepad");
		n.addActionListener(e -> {
			try {
				Runtime.getRuntime().exec("notepad");
			} catch (IOException ex) {
				Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
			}
		});
		//Forums
		JButton f = new JButton("Forums");
		/*try {
			Image icon = ImageIO.read(getClass().getClassLoader().getResource("./icons/forums.png"));//Icon source path
			f.setIcon(new ImageIcon(icon));//Set the button icon
			f.setMargin(new Insets(0, 0, 0, 0));//Remote spacing between borders and icon
		} catch (IOException e1) {
			e1.printStackTrace();
		}*/
		menu.add(f);
		//w.setToolTipText("Redirects you to the forums");
		f.addActionListener(e -> {
			try {
				String wiki = "http://titan.maxgamer.org";
				Desktop desktop = Desktop.getDesktop();
				URI uri = new URI(wiki);
				desktop.browse(uri.resolve(uri));
			} catch (URISyntaxException ex1) {
				Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex1);
			} catch (IOException ex2) {
				Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex2);
			}
		});

		jFrame.setDefaultCloseOperation(0);
		jFrame.pack();
		jFrame.setVisible(true);
	}

	public void readVars() throws IOException {
		params.put("cabbase", "g.cab");
		params.put("java_arguments", "-Xmx256m -Xss2m -Dsun.java2d.noddraw=true -XX:CompileThreshold=1500 -Xincgc -XX:+UseConcMarkSweepGC -XX:+UseParNewGC");
		params.put("colourid", "0");
		params.put("worldid", "16");
		params.put("lobbyid", "15");
		params.put("demoid", "0");
		params.put("demoaddress", "");
		params.put("modewhere", "0");
		params.put("modewhat", "0");
		params.put("lang", "0");
		params.put("objecttag", "0");
		params.put("js", "1");
		params.put("game", "0");
		params.put("affid", "0");
		params.put("advert", "1");
		params.put("settings", "wwGlrZHF5gJcZl7tf7KSRh0MZLhiU0gI0xDX6DwZ-Qk");
		params.put("country", "0");
		params.put("haveie6", "0");
		params.put("havefirefox", "1");
		params.put("cookieprefix", "");
		params.put("cookiehost", "titan.maxgamer.org");
		params.put("cachesubdirid", "0");
		params.put("crashurl", "");
		params.put("unsignedurl", "");
		params.put("sitesettings_member", "1");
		params.put("frombilling", "false");
		params.put("sskey", "");
		params.put("force64mb", "false");
		params.put("worldflags", "8");
		params.put("lobbyaddress", mainurl);
	}

	public void startClient() {
		try {
			GameShell.provideLoaderApplet(this);
			client var_client = new client();
			var_client.init();
			var_client.start();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

}

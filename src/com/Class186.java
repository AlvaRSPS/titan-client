/* Class186 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.anticheat.ReflectionAntiCheat;
import com.jagex.game.client.quickchat.QuickChatMessageType;

public final class Class186 implements RenderTarget {
	public static QuickChatMessageType	aClass348_3433	= new QuickChatMessageType(8, 0, 4, 1);
	public static int					anInt3431		= 0;
	public static int					cameraX;

	static {
		ReflectionAntiCheat.requestQueue = new LinkedList();
	}

	private Class333		aClass333_3425;
	float[]					aFloatArray3429;
	private PureJavaToolkit	aHa_Sub2_3432;
	int						anInt3426;

	int						anInt3430;

	int[]					anIntArray3427;

	Class186(PureJavaToolkit var_ha_Sub2, Sprite class332, Class333 class333) {
		do {
			aHa_Sub2_3432 = var_ha_Sub2;
			do {
				if (!(class332 instanceof Class332_Sub3_Sub2)) {
					if (!(class332 instanceof Class332_Sub3_Sub1)) {
						throw new RuntimeException();
					}
					Class332_Sub3_Sub1 class332_sub3_sub1 = (Class332_Sub3_Sub1) class332;
					anInt3426 = class332_sub3_sub1.anInt5433;
					anIntArray3427 = class332_sub3_sub1.anIntArray6212;
					anInt3430 = class332_sub3_sub1.anInt5454;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				Class332_Sub3_Sub2 class332_sub3_sub2 = (Class332_Sub3_Sub2) class332;
				anInt3430 = class332_sub3_sub2.anInt5454;
				anIntArray3427 = class332_sub3_sub2.anIntArray6213;
				anInt3426 = class332_sub3_sub2.anInt5433;
			} while (false);
			if (class333 == null) {
				break;
			}
			aClass333_3425 = class333;
			if ((anInt3426 ^ 0xffffffff) != (aClass333_3425.anInt3388 ^ 0xffffffff) || (anInt3430 ^ 0xffffffff) != (aClass333_3425.anInt3387 ^ 0xffffffff)) {
				throw new RuntimeException();
			}
			aFloatArray3429 = aClass333_3425.aFloatArray3389;
			break;
		} while (false);
	}

	@Override
	public final void copyFromRenderer(int i, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, boolean bool, boolean bool_11_) {
		Class246_Sub3_Sub4_Sub5.method3086(anInt3426, bool ? anIntArray3427 : null, i_6_, i, i_7_, bool_11_ ? aHa_Sub2_3432.aFloatArray4488 : null, i_10_, aHa_Sub2_3432.aClass98_Sub32_4478.pixels, i_8_, i_9_, !bool_11_ ? null : aFloatArray3429, aHa_Sub2_3432.aClass98_Sub32_4478.width, 0);
	}

	@Override
	public final void copyToRenderer(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, boolean bool, boolean bool_5_) {
		Class246_Sub3_Sub4_Sub5.method3086(aHa_Sub2_3432.aClass98_Sub32_4478.width, bool ? aHa_Sub2_3432.aClass98_Sub32_4478.pixels : null, i_0_, i, i_1_, bool_5_ ? aFloatArray3429 : null, i_4_, anIntArray3427, i_2_, i_3_, !bool_5_ ? null : aHa_Sub2_3432.aFloatArray4488, anInt3426, 0);
	}
}

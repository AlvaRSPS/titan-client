/* Class98_Sub49 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;

public final class Class98_Sub49 extends Node {
	public static Class100	aClass100_4283;
	public static int		anInt4286	= 0;

	static {
		aClass100_4283 = new Class100(64);
	}

	public static final int method1662(int i, int i_0_, int i_1_) {
		try {
			if (i_0_ != -1) {
				anInt4286 = -117;
			}
			int i_2_ = 1;
			while (i_1_ > 1) {
				if ((i_1_ & 0x1) != 0) {
					i_2_ *= i;
				}
				i_1_ >>= 1;
				i *= i;
			}
			if (i_1_ == 1) {
				return i * i_2_;
			}
			return i_2_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.B(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method1665(byte i) {
		try {
			if (i != 116) {
				method1665((byte) 30);
			}
			aClass100_4283 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.C(" + i + ')');
		}
	}

	int	anInt4284;

	int	anInt4285;

	Class98_Sub49(int i, int i_5_) {
		try {
			anInt4285 = i_5_;
			anInt4284 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.<init>(" + i + ',' + i_5_ + ')');
		}
	}

	public final int method1663(int i) {
		try {
			if (i != 1) {
				return 24;
			}
			return (0x1df9b4 & anInt4284) >> 682065522;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.F(" + i + ')');
		}
	}

	public final boolean method1664(int i) {
		try {
			if (i != -1) {
				method1664(-109);
			}
			return ((anInt4284 & 0x325ce0) >> 548331733 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.H(" + i + ')');
		}
	}

	public final boolean method1666(byte i, int i_3_) {
		try {
			if (i != -72) {
				return false;
			}
			return (0x1 & anInt4284 >> 1 + i_3_) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.G(" + i + ',' + i_3_ + ')');
		}
	}

	public final boolean method1667(byte i) {
		try {
			return (0x1 & anInt4284 >> -1566073674 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.A(" + i + ')');
		}
	}

	public final int method1668(int i) {
		try {
			if (i != -1) {
				method1669(-124);
			}
			return aa_Sub3.method157(anInt4284, (byte) 64);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.D(" + i + ')');
		}
	}

	public final boolean method1669(int i) {
		try {
			if (i != 1964468) {
				return false;
			}
			return (0x1 & anInt4284 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "waa.E(" + i + ')');
		}
	}
}


/* Class21_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.constants.BuildLocation;

import jaggl.OpenGL;

public final class Class21_Sub2 extends Class21 implements Interface4_Impl3 {
	public static int		cursorId			= -1;
	public static int		anInt5388;
	public static int[][]	anIntArrayArray5386	= { { 2, 4, 6, 0 }, { 0, 2, 4, 6 }, { 0, 2, 4 }, { 4, 0, 2 }, { 2, 4, 0 }, { 0, 2, 4 }, { 6, 0, 1, 2, 4, 5 }, { 0, 4, 7, 6 }, { 4, 7, 6, 0 }, { 0, 8, 6, 2, 9, 4 }, { 2, 9, 4, 0, 8, 6 }, { 2, 11, 4, 6, 10, 0 }, { 2, 4, 6, 0 } };

	public static final boolean method271(byte i, int i_0_, int i_1_) {
		try {
			if (i > -100) {
				method273((byte) -47);
			}
			return (0x10000 & i_0_ ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lq.E(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method272(byte i) {
		try {
			if (i == 86) {
				anIntArrayArray5386 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lq.A(" + i + ')');
		}
	}

	public static final void method273(byte i) {// npc masks
		try {
			for (int i_4_ = 0; i_4_ < Class65.anInt502; i_4_++) {
				int i_5_ = Class76_Sub11.anIntArray3796[i_4_];
				NPC class246_sub3_sub4_sub2_sub1 = ((NodeObject) ProceduralTextureSource.npc.get(i_5_, -1)).npc;
				int i_6_ = PacketParser.buffer.readUnsignedByte((byte) -106);
				if ((0x20 & i_6_) != 0) {
					i_6_ += PacketParser.buffer.readUnsignedByte((byte) 20) << 1706038888;
				}
				if ((0x2000 & i_6_) != 0) {
					class246_sub3_sub4_sub2_sub1.anInt6378 = PacketParser.buffer.method1187((byte) 0);
					class246_sub3_sub4_sub2_sub1.anInt6347 = PacketParser.buffer.method1187((byte) 0);
					class246_sub3_sub4_sub2_sub1.anInt6362 = PacketParser.buffer.method1187((byte) -112);
					class246_sub3_sub4_sub2_sub1.anInt6392 = PacketParser.buffer.method1187((byte) 0);
					class246_sub3_sub4_sub2_sub1.anInt6390 = PacketParser.buffer.readShort((byte) 127) - -Queue.timer;
					class246_sub3_sub4_sub2_sub1.anInt6424 = PacketParser.buffer.readShort1((byte) -123) + Queue.timer;
					class246_sub3_sub4_sub2_sub1.anInt6407 = PacketParser.buffer.readByteC((byte) -109);
					class246_sub3_sub4_sub2_sub1.anInt6362 += class246_sub3_sub4_sub2_sub1.pathX[0];
					class246_sub3_sub4_sub2_sub1.anInt6392 += class246_sub3_sub4_sub2_sub1.pathZ[0];
					class246_sub3_sub4_sub2_sub1.pathLength = 1;
					class246_sub3_sub4_sub2_sub1.anInt6378 += class246_sub3_sub4_sub2_sub1.pathX[0];
					class246_sub3_sub4_sub2_sub1.anInt6436 = 0;
					class246_sub3_sub4_sub2_sub1.anInt6347 += class246_sub3_sub4_sub2_sub1.pathZ[0];
				}
				if ((i_6_ & 0x4) != 0) {
					int i_7_ = PacketParser.buffer.readByteC((byte) -121);
					if (i_7_ > 0) {
						for (int i_8_ = 0; i_7_ > i_8_; i_8_++) {
							int i_9_ = -1;
							int i_10_ = -1;
							int i_11_ = -1;
							int i_12_ = PacketParser.buffer.readSmart(1689622712);
							if ((i_12_ ^ 0xffffffff) == -32768) {
								i_12_ = PacketParser.buffer.readSmart(1689622712);
								i_10_ = PacketParser.buffer.readSmart(1689622712);
								i_9_ = PacketParser.buffer.readSmart(1689622712);
								i_11_ = PacketParser.buffer.readSmart(1689622712);
							} else if (i_12_ != 32766) {
								i_10_ = PacketParser.buffer.readSmart(1689622712);
							} else {
								i_12_ = -1;
							}
							int i_13_ = PacketParser.buffer.readSmart(1689622712);
							int i_14_ = PacketParser.buffer.readUnsignedByte((byte) -128);
							class246_sub3_sub4_sub2_sub1.method3037(i_14_, false, i_10_, i_12_, Queue.timer, i_11_, i_13_, i_9_);
						}
					}
				}
				if ((0x4000 & i_6_ ^ 0xffffffff) != -1) {
					int i_15_ = class246_sub3_sub4_sub2_sub1.definition.anIntArray1117.length;
					int i_16_ = 0;
					if (class246_sub3_sub4_sub2_sub1.definition.aShortArray1105 != null) {
						i_16_ = class246_sub3_sub4_sub2_sub1.definition.aShortArray1105.length;
					}
					if (class246_sub3_sub4_sub2_sub1.definition.aShortArray1137 != null) {
						i_16_ = class246_sub3_sub4_sub2_sub1.definition.aShortArray1137.length;
					}
					int i_17_ = 0;
					int i_18_ = PacketParser.buffer.readByteS(106);
					if ((0x1 & i_18_ ^ 0xffffffff) != -2) {
						int[] is = null;
						if ((0x2 & i_18_) == 2) {
							is = new int[i_15_];
							for (int i_19_ = 0; (i_15_ ^ 0xffffffff) < (i_19_ ^ 0xffffffff); i_19_++) {
								is[i_19_] = PacketParser.buffer.readLEShortA((byte) -84);
							}
						}
						short[] is_20_ = null;
						if ((0x4 & i_18_) == 4) {
							is_20_ = new short[i_16_];
							for (int i_21_ = 0; i_21_ < i_16_; i_21_++) {
								is_20_[i_21_] = (short) PacketParser.buffer.readShortA(51);
							}
						}
						short[] is_22_ = null;
						if ((0x8 & i_18_ ^ 0xffffffff) == -9) {
							is_22_ = new short[i_17_];
							for (int i_23_ = 0; (i_17_ ^ 0xffffffff) < (i_23_ ^ 0xffffffff); i_23_++) {
								is_22_[i_23_] = (short) PacketParser.buffer.readLEShortA((byte) 108);
							}
						}
						long l = i_5_ | (long) class246_sub3_sub4_sub2_sub1.anInt6501++ << -1851073888;
						new Class40(l, is, is_20_, is_22_);
					}
				}
				if ((0x400 & i_6_ ^ 0xffffffff) != -1) {
					int i_24_ = PacketParser.buffer.readShortA(96);
					class246_sub3_sub4_sub2_sub1.anInt6394 = PacketParser.buffer.readByteC((byte) -114);
					class246_sub3_sub4_sub2_sub1.anInt6401 = PacketParser.buffer.readUnsignedByte((byte) -101);
					class246_sub3_sub4_sub2_sub1.aBoolean6348 = (i_24_ & 0x8000) != 0;
					class246_sub3_sub4_sub2_sub1.anInt6420 = i_24_ & 0x7fff;
					class246_sub3_sub4_sub2_sub1.anInt6412 = class246_sub3_sub4_sub2_sub1.anInt6420 + Queue.timer - -class246_sub3_sub4_sub2_sub1.anInt6394;
				}
				if ((0x2 & i_6_) != 0) {// Graphics
					int i_25_ = PacketParser.buffer.readShortA(65);
					if ((i_25_ ^ 0xffffffff) == -65536) {
						i_25_ = -1;
					}
					int i_26_ = PacketParser.buffer.readIntReverse(true);
					int i_27_ = PacketParser.buffer.readByteC((byte) 119);
					int i_28_ = 0x7 & i_27_;
					int i_29_ = (0x79 & i_27_) >> 2140717763;
					if ((i_29_ ^ 0xffffffff) == -16) {
						i_29_ = -1;
					}
					class246_sub3_sub4_sub2_sub1.method3032(i_29_, false, i_26_, i_28_, i_25_, -117);
				}
				if ((i_6_ & 0x8 ^ 0xffffffff) != -1) {
					class246_sub3_sub4_sub2_sub1.anInt6364 = PacketParser.buffer.readShort((byte) 127);
					if (class246_sub3_sub4_sub2_sub1.anInt6364 == 65535) {
						class246_sub3_sub4_sub2_sub1.anInt6364 = -1;
					}
				}
				if ((i_6_ & 0x8000 ^ 0xffffffff) != -1) {
					int i_30_ = class246_sub3_sub4_sub2_sub1.definition.models.length;
					int i_31_ = 0;
					if (class246_sub3_sub4_sub2_sub1.definition.aShortArray1105 != null) {
						i_31_ = class246_sub3_sub4_sub2_sub1.definition.aShortArray1105.length;
					}
					int i_32_ = 0;
					if (class246_sub3_sub4_sub2_sub1.definition.aShortArray1137 != null) {
						i_32_ = class246_sub3_sub4_sub2_sub1.definition.aShortArray1137.length;
					}
					int i_33_ = PacketParser.buffer.readUnsignedByte((byte) -98);
					if ((i_33_ & 0x1) == 1) {
						class246_sub3_sub4_sub2_sub1.aClass40_6502 = null;
					} else {
						int[] is = null;
						if ((i_33_ & 0x2) == 2) {
							is = new int[i_30_];
							for (int i_34_ = 0; (i_34_ ^ 0xffffffff) > (i_30_ ^ 0xffffffff); i_34_++) {
								is[i_34_] = PacketParser.buffer.readShortA(65);
							}
						}
						short[] is_35_ = null;
						if ((i_33_ & 0x4 ^ 0xffffffff) == -5) {
							is_35_ = new short[i_31_];
							for (int i_36_ = 0; i_31_ > i_36_; i_36_++) {
								is_35_[i_36_] = (short) PacketParser.buffer.readShortA(85);
							}
						}
						short[] is_37_ = null;
						if ((i_33_ & 0x8) == 8) {
							is_37_ = new short[i_32_];
							for (int i_38_ = 0; i_38_ < i_32_; i_38_++) {
								is_37_[i_38_] = (short) PacketParser.buffer.readShortA(106);
							}
						}
						long l = i_5_ | (long) class246_sub3_sub4_sub2_sub1.anInt6503++ << 270238944;
						class246_sub3_sub4_sub2_sub1.aClass40_6502 = new Class40(l, is, is_35_, is_37_);
					}
				}
				if ((0x800 & i_6_) != 0) {
					int i_39_ = PacketParser.buffer.readByteA(true);
					int[] is = new int[i_39_];
					int[] is_40_ = new int[i_39_];
					for (int i_41_ = 0; i_41_ < i_39_; i_41_++) {
						int i_42_ = PacketParser.buffer.readLEShortA((byte) 119);
						if ((i_42_ & 0xc000 ^ 0xffffffff) == -49153) {
							int i_43_ = PacketParser.buffer.readLEShortA((byte) -28);
							is[i_41_] = Class41.or(i_43_, i_42_ << 1873040720);
						} else {
							is[i_41_] = i_42_;
						}
						is_40_[i_41_] = PacketParser.buffer.readShort((byte) 127);
					}
					class246_sub3_sub4_sub2_sub1.method3038(is_40_, is, true);
				}
				if ((0x80 & i_6_) != 0) {
					if (class246_sub3_sub4_sub2_sub1.definition.method2302((byte) 97)) {
						Class98_Sub43_Sub4.method1504(class246_sub3_sub4_sub2_sub1, -16255);
					}
					class246_sub3_sub4_sub2_sub1.setDefinition(Class4.npcDefinitionList.get(5, PacketParser.buffer.readShort1((byte) -86)), 1);
					class246_sub3_sub4_sub2_sub1.setSize((byte) 88, class246_sub3_sub4_sub2_sub1.definition.boundSize);
					class246_sub3_sub4_sub2_sub1.anInt6414 = class246_sub3_sub4_sub2_sub1.definition.anInt1091 << -821006205;
					if (class246_sub3_sub4_sub2_sub1.definition.method2302((byte) 44)) {
						Class98_Sub31_Sub4.method1383(null, null, class246_sub3_sub4_sub2_sub1.pathX[0], 0, 3, class246_sub3_sub4_sub2_sub1.pathZ[0], class246_sub3_sub4_sub2_sub1.plane, class246_sub3_sub4_sub2_sub1);
					}
				}
				if ((0x10 & i_6_ ^ 0xffffffff) != -1) {
					int[] is = new int[4];
					for (int i_44_ = 0; i_44_ < 4; i_44_++) {
						is[i_44_] = PacketParser.buffer.readShortA(49);
						if ((is[i_44_] ^ 0xffffffff) == -65536) {
							is[i_44_] = -1;
						}
					}
					int i_45_ = PacketParser.buffer.readByteS(-85);
					Class98_Sub43.method1483(i_45_, class246_sub3_sub4_sub2_sub1, 1, is);
				}
				if ((0x1000 & i_6_) != 0) {
					int i_46_ = PacketParser.buffer.readByteA(true);
					int[] is = new int[i_46_];
					int[] is_47_ = new int[i_46_];
					int[] is_48_ = new int[i_46_];
					for (int i_49_ = 0; (i_49_ ^ 0xffffffff) > (i_46_ ^ 0xffffffff); i_49_++) {
						int i_50_ = PacketParser.buffer.readShort1((byte) 58);
						if (i_50_ == 65535) {
							i_50_ = -1;
						}
						is[i_49_] = i_50_;
						is_47_[i_49_] = PacketParser.buffer.readByteS(-26);
						is_48_[i_49_] = PacketParser.buffer.readShort((byte) 127);
					}
					Class262.method3215(26256, is_47_, is_48_, class246_sub3_sub4_sub2_sub1, is);
				}
				if ((i_6_ & 0x100 ^ 0xffffffff) != -1) {
					int i_51_ = PacketParser.buffer.readShortA(83);
					if (i_51_ == 65535) {
						i_51_ = -1;
					}
					int i_52_ = PacketParser.buffer.readInt(-2);
					int i_53_ = PacketParser.buffer.readByteC((byte) -106);
					int i_54_ = i_53_ & 0x7;
					int i_55_ = i_53_ >> -481470685 & 0xf;
					if ((i_55_ ^ 0xffffffff) == -16) {
						i_55_ = -1;
					}
					class246_sub3_sub4_sub2_sub1.method3032(i_55_, true, i_52_, i_54_, i_51_, -96);
				}
				if ((i_6_ & 0x40) != 0) {
					class246_sub3_sub4_sub2_sub1.anInt6510 = PacketParser.buffer.readLEShortA((byte) 115);
					class246_sub3_sub4_sub2_sub1.anInt6505 = PacketParser.buffer.readLEShortA((byte) 116);
				}
				if ((i_6_ & 0x200) != 0) {
					class246_sub3_sub4_sub2_sub1.aByte6404 = PacketParser.buffer.readSignedByte((byte) -19);
					class246_sub3_sub4_sub2_sub1.aByte6381 = PacketParser.buffer.method1187((byte) -112);
					class246_sub3_sub4_sub2_sub1.aByte6368 = PacketParser.buffer.method1234(128);
					class246_sub3_sub4_sub2_sub1.aByte6422 = (byte) PacketParser.buffer.readByteA(true);
					class246_sub3_sub4_sub2_sub1.anInt6403 = Queue.timer + PacketParser.buffer.readLEShortA((byte) 96);
					class246_sub3_sub4_sub2_sub1.anInt6349 = Queue.timer - -PacketParser.buffer.readShort1((byte) 100);
				}
				if ((0x1 & i_6_) != 0) {
					class246_sub3_sub4_sub2_sub1.message = PacketParser.buffer.readString((byte) 84);
					class246_sub3_sub4_sub2_sub1.anInt6384 = 100;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lq.C(" + i + ')');
		}
	}

	public static final void method274(byte i, int i_56_) {
		try {
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_56_, -23, 9);
			class98_sub46_sub17.method1621(i + -83);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lq.D(" + i + ',' + i_56_ + ')');
		}
	}

	Class21_Sub2(OpenGLXToolkit var_ha_Sub3_Sub2, int i, boolean bool, int[][] is) {
		super(var_ha_Sub3_Sub2, 34067, Class62.aClass164_486, Class162.aClass162_1266, 6 * i * i, bool);
		try {
			this.aHa_Sub3_Sub2_3233.method2005(this, -115);
			if (bool) {
				for (int i_2_ = 0; i_2_ < 6; i_2_++) {
					method264(i, is[i_2_], i, 526364520, i_2_ + 34069);
				}
			} else {
				for (int i_3_ = 0; (i_3_ ^ 0xffffffff) > -7; i_3_++) {
					OpenGL.glTexImage2Di(34069 - -i_3_, 0, method260(0), i, i, 0, BuildLocation.method2665(false, this.aClass164_3237), this.aHa_Sub3_Sub2_3233.anInt6135, is[i_3_], 0);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lq.<init>(" + (var_ha_Sub3_Sub2 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}
}

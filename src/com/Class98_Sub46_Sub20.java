/* Class98_Sub46_Sub20 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.input.impl.AwtMouseListener;

public abstract class Class98_Sub46_Sub20 extends Cacheable {
	public static int		anInt6074;

    /**
     * Bit masks. Each mask has the next bit set. Eg, [0] has the first bit set, [1] has the first and second bits set, [2]
     * has the first, second and third bits set. Naturally, the last value is -1, eg all bits set: 0xFFFF FFFF
     */
    public static int[] bitmasks = new int[32];
	public static String[]	aStringArray6073;

	static {
		int i = 2;
		for (int index = 0; ~index > -33; index++) {
			bitmasks[index] = -1 + i;
			i += i;
		}
	}

	public static void method1636(boolean bool) {
		try {
			if (bool == true) {
				bitmasks = null;
				aStringArray6073 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "naa.C(" + bool + ')');
		}
	}

	public static final int method1637(int i, int i_0_, int i_1_, int i_2_) {
		try {
			if (i_0_ >= -39) {
				return 26;
			}
			if ((i_1_ ^ 0xffffffff) == (i_2_ ^ 0xffffffff)) {
				return i_1_;
			}
			int i_3_ = -i + 128;
			int i_4_ = i * (0x7f & i_2_) + i_3_ * (0x7f & i_1_) >> 747196583;
			int i_5_ = i_3_ * (0x380 & i_1_) + (0x380 & i_2_) * i >> 1680000903;
			int i_6_ = (0xfc00 & i_2_) * i + (i_1_ & 0xfc00) * i_3_ >> -239610233;
			return i_5_ & 0x380 | 0xfc00 & i_6_ | i_4_ & 0x7f;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "naa.D(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public static final int method1639(int i, int i_7_) {
		if (AwtMouseListener.aByteArrayArray5291 != null) {
			return AwtMouseListener.aByteArrayArray5291[i][i_7_] & 0xff;
		}
		return 0;
	}

	int			anInt6072;

	CacheKey	anInterface20_6071;

	Class98_Sub46_Sub20(CacheKey interface20, int i) {
		try {
			anInt6072 = i;
			anInterface20_6071 = interface20;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "naa.<init>(" + (interface20 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	abstract Object method1635(int i);

	abstract boolean method1638(int i);
}

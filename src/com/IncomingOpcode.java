/* Class58 - Decompiled by JODE
 */ package com; /*
					*/

public final class IncomingOpcode {
	public static Class65	aClass65_459	= new Class65();
	public static int		anInt461		= -1;
	public static int		anInt463;

	public static final boolean method523(int i, int i_2_, int i_3_) {
		if (i_2_ != -1) {
			return true;
		}
		return (0x21 & i_3_) != 0;
	}

	int			anInt460;

	private int	anInt462;

	public IncomingOpcode(int i, int i_0_) {
		anInt460 = i_0_;
		anInt462 = i;
	}

	public final int method521(byte i) {
		return anInt462;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

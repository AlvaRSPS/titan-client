/* Class246_Sub7 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class246_Sub7 extends SceneGraphNode {
	public static OutgoingOpcode	aClass171_5115	= new OutgoingOpcode(30, 4);
	public static Class48			aClass48_5119;

	public static final int method3132(int i, byte i_0_, int i_1_) {
		try {
			if (i_0_ != 118) {
				return 45;
			}
			i = (i_1_ & 0x7f) * i >> -1157637401;
			do {
				if (i >= 2) {
					if ((i ^ 0xffffffff) >= -127) {
						break;
					}
					i = 126;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				i = 2;
			} while (false);
			return (i_1_ & 0xff80) + i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mda.A(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method3133(byte i) {
		do {
			try {
				aClass48_5119 = null;
				aClass171_5115 = null;
				if (i == 64) {
					break;
				}
				method3133((byte) -73);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mda.B(" + i + ')');
			}
			break;
		} while (false);
	}

	int		anInt5116;
	int		anInt5117;
	int		anInt5118;
	int		anInt5120;

	int		anInt5122;

	int		anInt5123;

	String	aString5121;

	public Class246_Sub7() {
		/* empty */
	}
}

/* Class364 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Exception;

public final class OpenGLTexture2DSource {
	public static SignLink	aClass88_3104;
	public static int		anInt3103;
	public static int[]		anIntArray3102;

	public static final int method3932(boolean bool, byte i) {
		try {
			if (i != -67) {
				aClass88_3104 = null;
			}
			int i_2_ = Cacheable.anInt4261;
			while_245_: do {
				while_244_: do {
					do {
						if (i_2_ != 0) {
							if (i_2_ == 1) {
								break;
							}
							if (i_2_ != 2) {
								break while_245_;
							}
							if (!GameShell.cleanedStatics) {
								break while_244_;
							}
						}
						if (bool) {
							return 0;
						}
						return Class272.anInt2037;
					} while (false);
					return Class272.anInt2037;
				} while (false);
				return 0;
			} while (false);
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vw.E(" + bool + ',' + i + ')');
		}
	}

	public static final void method3935() {
		Class314.method3644(1, anInt3103);
	}

	public static void method3936(byte i) {
		try {
			anIntArray3102 = null;
			aClass88_3104 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vw.D(" + i + ')');
		}
	}

	private AdvancedMemoryCache	aClass79_3106	= new AdvancedMemoryCache(256);

	private TextureMetricsList	aD3101;

	private OpenGlToolkit		aHa_Sub1_3105;

	OpenGLTexture2DSource(OpenGlToolkit var_ha_Sub1, TextureMetricsList var_d) {
		try {
			aD3101 = var_d;
			aHa_Sub1_3105 = var_ha_Sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vw.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + (var_d != null ? "{...}" : "null") + ')');
		}
	}

	public final Class42_Sub1 method3931(int i, int i_0_) {
		try {
			Object object = aClass79_3106.get(-121, i_0_);
			if (object != null) {
				return (Class42_Sub1) object;
			}
			if (!aD3101.isCached(-117, i_0_)) {
				return null;
			}
			if (i <= 122) {
				anIntArray3102 = null;
			}
			TextureMetrics class238 = aD3101.getInfo(i_0_, -28755);
			int i_1_ = !class238.aBoolean1822 ? aHa_Sub1_3105.anInt4309 : 64;
			Class42_Sub1 class42_sub1;
			if (class238.aBoolean1817 && aHa_Sub1_3105.canEnableBloom()) {
				float[] fs = aD3101.getPixelsFloat((byte) -126, false, i_0_, i_1_, 0.7F, i_1_);
				class42_sub1 = new Class42_Sub1(aHa_Sub1_3105, 3553, 34842, i_1_, i_1_, (class238.aByte1832 ^ 0xffffffff) != -1, fs, 6408);
			} else {
				int[] is;
				if ((class238.anInt1818 ^ 0xffffffff) == -3 || !Class98_Sub10_Sub7.method1023(1, class238.aByte1820)) {
					is = aD3101.getPixelsArgb(127, i_1_, i_0_, 0.7F, false, i_1_);
				} else {
					is = aD3101.getPixelsRgb(i_0_, (byte) -120, i_1_, 0.7F, true, i_1_);
				}
				class42_sub1 = new Class42_Sub1(aHa_Sub1_3105, 3553, 6408, i_1_, i_1_, class238.aByte1832 != 0, is, 0, 0, false);
			}
			class42_sub1.method383(class238.aBoolean1819, 10242, class238.aBoolean1826);
			aClass79_3106.put(i_0_, class42_sub1, (byte) -80);
			return class42_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vw.C(" + i + ',' + i_0_ + ')');
		}
	}

	public final void method3933(int i) {
		try {
			if (i != 0) {
				aClass88_3104 = null;
			}
			aClass79_3106.makeSoftReferences((byte) 62, 5);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vw.B(" + i + ')');
		}
	}

	public final void method3934(byte i) {
		try {
			if (i != 100) {
				aHa_Sub1_3105 = null;
			}
			aClass79_3106.clear(i + 13);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vw.F(" + i + ')');
		}
	}
}

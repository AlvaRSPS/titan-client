/* Class104 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.preferences.BrightnessPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;

public final class OpenGlArrayBufferPointer {
	public static Class36[]			aClass36Array903	= new Class36[8];
	public static int				anInt897;
	public static OutgoingOpcode	FIRST_OPTION		= new OutgoingOpcode(18, 3);

	public static final void renderCamera(boolean bool, int xCurve, int zoom, int yCam, int zCam, int xCam, int yCurve, int plane) {
		// System.out.println("xCurve: " + xCurve + " Zoom: " + zoom + " yCam; "
		// + yCam + " zCam: " + zCam + " xCam: " + xCam + " yCurve: " + yCurve +
		// " Plane: " + plane);
		int i_6_ = -334 + zCam;
		do {
			if (i_6_ >= 0) {
				if (i_6_ <= 100) {
					break;
				}
				i_6_ = 100;
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			i_6_ = 0;
		} while (false);
		int i_7_ = (Js5Client.clientZoom + -BrightnessPreferenceField.aShort3692) * i_6_ / 100 + BrightnessPreferenceField.aShort3692;
		Class16.anInt197 = i_7_ * Class16.orthoZoom >> 626243656;
		zoom = zoom * i_7_ >> -1212701304;
		int i_8_ = 16384 - yCurve & 0x3fff;
		int i_9_ = 16384 + -xCurve & 0x3fff;
		int i_10_ = 0;
		int i_11_ = 0;
		int cameraZoom = zoom;
		if ((i_8_ ^ 0xffffffff) != -1) {
			i_11_ = Class284_Sub2_Sub2.SINE[i_8_] * -cameraZoom >> -1123635698;
			cameraZoom = cameraZoom * Class284_Sub2_Sub2.COSINE[i_8_] >> 1849889102;
		}
		if ((i_9_ ^ 0xffffffff) != -1) {
			i_10_ = Class284_Sub2_Sub2.SINE[i_9_] * cameraZoom >> 1440327534;
			cameraZoom = cameraZoom * Class284_Sub2_Sub2.COSINE[i_9_] >> -1335926898;
		}
		SpriteLoadingScreenElement.yCamPosTile = -cameraZoom + yCam;
		Class308.anInt2584 = 0;
		Mob.anInt6357 = yCurve;
		Class98_Sub46_Sub10.xCamPosTile = -i_10_ + xCam;
		Class186.cameraX = xCurve;
		AdvancedMemoryCache.anInt601 = plane - i_11_;
		// System.out.println("SpriteLoadingScreenElement.anInt3461:" +
		// AdvancedMemoryCache.anInt601);
	}

	public byte			aByte898;
	public byte			aByte900;

	public ArrayBuffer	buffer;

	public short		type;

	public OpenGlArrayBufferPointer(ArrayBuffer buffer, int type, int i_13_, int i_14_) {
		aByte898 = (byte) i_14_;
		this.buffer = buffer;
		this.type = (short) type;
		aByte900 = (byte) i_13_;
	}
}

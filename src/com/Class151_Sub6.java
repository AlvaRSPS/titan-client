
/* Class151_Sub6 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

import jaggl.OpenGL;

public final class Class151_Sub6 extends Class151 {
	public static IncomingOpcode	aClass58_4997	= new IncomingOpcode(76, -1);
	public static IncomingOpcode	aClass58_4998	= new IncomingOpcode(12, 0);
	public static IncomingOpcode	aClass58_4999	= new IncomingOpcode(80, -2);

	public static final void method2463(RtInterface class293, RtInterface class293_15_, int i) {
		OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Huffman.aClass171_1604, Class331.aClass117_2811);
		frame.packet.writeInt(1571862888, class293_15_.idDword);
		if (i >= 18) {
			frame.packet.writeShortA(class293.unknown, (byte) 126);
			frame.packet.writeShort(class293_15_.unknown, 1571862888);
			frame.packet.writeLEInt(class293.idDword, 1046032984);
			frame.packet.writeLEShortA(class293_15_.componentIndex, 128);
			frame.packet.writeLEShort(class293.componentIndex, 17624);
			Class98_Sub10_Sub29.sendPacket(false, frame);
		}
	}

	private boolean				aBoolean4996	= false;
	private boolean				aBoolean5000;

	private Class42_Sub2[]		aClass42_Sub2Array4995;

	private OpenGLDisplayList	aClass91_4994;

	Class151_Sub6(OpenGlToolkit var_ha_Sub1) {
		super(var_ha_Sub1);
		do {
			try {
				if (!var_ha_Sub1.aBoolean4391) {
					break;
				}
				aBoolean5000 = (var_ha_Sub1.anInt4410 ^ 0xffffffff) > -4;
				int i = aBoolean5000 ? 48 : 127;
				byte[][] is = new byte[6][4096];
				byte[][] is_0_ = new byte[6][4096];
				byte[][] is_1_ = new byte[6][4096];
				int i_2_ = 0;
				for (int i_3_ = 0; i_3_ < 64; i_3_++) {
					for (int i_4_ = 0; i_4_ < 64; i_4_++) {
						float f = -1.0F + 2.0F * i_4_ / 64.0F;
						float f_5_ = -1.0F + i_3_ * 2.0F / 64.0F;
						float f_6_ = (float) (1.0 / Math.sqrt(f_5_ * f_5_ + (f * f + 1.0F)));
						f *= f_6_;
						f_5_ *= f_6_;
						for (int i_7_ = 0; i_7_ < 6; i_7_++) {
							float f_8_;
							if (i_7_ != 0) {
								if ((i_7_ ^ 0xffffffff) != -2) {
									if ((i_7_ ^ 0xffffffff) == -3) {
										f_8_ = f_5_;
									} else if ((i_7_ ^ 0xffffffff) != -4) {
										if (i_7_ == 4) {
											f_8_ = f_6_;
										} else {
											f_8_ = -f_6_;
										}
									} else {
										f_8_ = -f_5_;
									}
								} else {
									f_8_ = f;
								}
							} else {
								f_8_ = -f;
							}
							int i_9_;
							int i_10_;
							int i_11_;
							if (f_8_ > 0.0F) {
								i_9_ = (int) (Math.pow(f_8_, 96.0) * i);
								i_10_ = (int) (i * Math.pow(f_8_, 36.0));
								i_11_ = (int) (i * Math.pow(f_8_, 12.0));
							} else {
								i_9_ = i_10_ = i_11_ = 0;
							}
							is_0_[i_7_][i_2_] = (byte) i_9_;
							is_1_[i_7_][i_2_] = (byte) i_10_;
							is[i_7_][i_2_] = (byte) i_11_;
						}
						i_2_++;
					}
				}
				aClass42_Sub2Array4995 = new Class42_Sub2[3];
				aClass42_Sub2Array4995[0] = new Class42_Sub2(this.aHa_Sub1_1215, 6406, 64, false, is_0_, 6406);
				aClass42_Sub2Array4995[1] = new Class42_Sub2(this.aHa_Sub1_1215, 6406, 64, false, is_1_, 6406);
				aClass42_Sub2Array4995[2] = new Class42_Sub2(this.aHa_Sub1_1215, 6406, 64, false, is, 6406);
				method2464(0);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "nf.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final boolean method2439(int i) {
		try {
			if (i != 31565) {
				aBoolean5000 = true;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nf.A(" + i + ')');
		}
	}

	@Override
	public final void method2440(boolean bool, boolean bool_14_) {
		do {
			try {
				if (bool != false) {
					break;
				}
				if (aClass91_4994 != null && bool_14_) {
					if (!aBoolean5000) {
						this.aHa_Sub1_1215.method1845(2, 847872872);
						this.aHa_Sub1_1215.setActiveTexture(1, this.aHa_Sub1_1215.aClass42_Sub1_4358);
						this.aHa_Sub1_1215.method1845(0, 847872872);
					}
					aClass91_4994.callList('\0', false);
					aBoolean4996 = true;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 34168);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "nf.D(" + bool + ',' + bool_14_ + ')');
			}
		} while (false);
	}

	@Override
	public final void method2441(int i, int i_12_, int i_13_) {
		do {
			try {
				if (aBoolean4996) {
					this.aHa_Sub1_1215.method1845(1, 847872872);
					this.aHa_Sub1_1215.setActiveTexture(1, aClass42_Sub2Array4995[i - 1]);
					this.aHa_Sub1_1215.method1845(0, 847872872);
				}
				if (i_13_ < -2) {
					break;
				}
				aBoolean4996 = true;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "nf.G(" + i + ',' + i_12_ + ',' + i_13_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2442(Class42 class42, boolean bool, int i) {
		try {
			if (bool != false) {
				method2445((byte) -95);
			}
			this.aHa_Sub1_1215.setActiveTexture(1, class42);
			this.aHa_Sub1_1215.method1896(260, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nf.F(" + (class42 != null ? "{...}" : "null") + ',' + bool + ',' + i + ')');
		}
	}

	@Override
	public final void method2443(boolean bool, int i) {
		try {
			if (i != 255) {
				method2443(false, 106);
			}
			this.aHa_Sub1_1215.method1899(7681, 8960, 8448);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nf.C(" + bool + ',' + i + ')');
		}
	}

	@Override
	public final void method2445(byte i) {
		try {
			if (i <= 25) {
				aBoolean5000 = true;
			}
			do {
				if (aBoolean4996) {
					if (!aBoolean5000) {
						this.aHa_Sub1_1215.method1845(2, 847872872);
						this.aHa_Sub1_1215.setActiveTexture(1, null);
					}
					this.aHa_Sub1_1215.method1845(1, 847872872);
					this.aHa_Sub1_1215.setActiveTexture(1, null);
					this.aHa_Sub1_1215.method1845(0, 847872872);
					aClass91_4994.callList('\001', false);
					aBoolean4996 = false;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 5890);
			} while (false);
			this.aHa_Sub1_1215.method1899(8448, 8960, 8448);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nf.E(" + i + ')');
		}
	}

	private final void method2464(int i) {
		try {
			aClass91_4994 = new OpenGLDisplayList(this.aHa_Sub1_1215, 2);
			aClass91_4994.newList(0, -30389);
			this.aHa_Sub1_1215.method1845(1, 847872872);
			OpenGL.glTexGeni(8192, 9472, 34065);
			OpenGL.glTexGeni(8193, 9472, 34065);
			OpenGL.glTexGeni(8194, 9472, 34065);
			OpenGL.glEnable(3168);
			OpenGL.glEnable(3169);
			OpenGL.glEnable(3170);
			OpenGL.glMatrixMode(5890);
			OpenGL.glLoadIdentity();
			OpenGL.glRotatef(22.5F, 1.0F, 0.0F, 0.0F);
			OpenGL.glMatrixMode(5888);
			if (aBoolean5000) {
				this.aHa_Sub1_1215.method1899(7681, i ^ 0x2300, 260);
				this.aHa_Sub1_1215.method1840(0, 770, -121, 5890);
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 34167);
			} else {
				this.aHa_Sub1_1215.method1899(8448, i ^ 0x2300, 7681);
				this.aHa_Sub1_1215.method1840(0, 768, i ^ 0x54, 34168);
				this.aHa_Sub1_1215.method1845(2, 847872872);
				this.aHa_Sub1_1215.method1899(7681, i ^ 0x2300, 260);
				this.aHa_Sub1_1215.method1840(0, 768, 90, 34168);
				this.aHa_Sub1_1215.method1840(1, 770, 80, 34168);
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 34167);
			}
			this.aHa_Sub1_1215.method1845(0, 847872872);
			aClass91_4994.endList((byte) 58);
			aClass91_4994.newList(1, -30389);
			this.aHa_Sub1_1215.method1845(1, i ^ 0x32898368);
			OpenGL.glDisable(3168);
			OpenGL.glDisable(3169);
			OpenGL.glDisable(3170);
			OpenGL.glMatrixMode(5890);
			OpenGL.glLoadIdentity();
			OpenGL.glMatrixMode(5888);
			if (!aBoolean5000) {
				this.aHa_Sub1_1215.method1899(8448, 8960, 8448);
				this.aHa_Sub1_1215.method1840(0, 768, 81, 5890);
				this.aHa_Sub1_1215.method1845(2, 847872872);
				this.aHa_Sub1_1215.method1899(8448, 8960, 8448);
				this.aHa_Sub1_1215.method1840(0, 768, 110, 5890);
				this.aHa_Sub1_1215.method1840(1, 768, -124, 34168);
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 5890);
			} else {
				this.aHa_Sub1_1215.method1899(8448, 8960, 8448);
				this.aHa_Sub1_1215.method1840(0, 768, -82, 5890);
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 5890);
			}
			this.aHa_Sub1_1215.method1845(i, 847872872);
			aClass91_4994.endList((byte) -95);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nf.B(" + i + ')');
		}
	}
}

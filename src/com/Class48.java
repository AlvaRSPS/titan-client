/* Class48 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public abstract class Class48 {
	public static int	anInt409;
	public static int	anInt410	= -60;

	static {
		anInt409 = 0;
	}

	public static final int findNextGreaterPwr2(int i, int i_0_) {
		try {
			i_0_ = --i_0_ | i_0_ >>> 423660257;
			i_0_ |= i_0_ >>> -329257566;
			i_0_ |= i_0_ >>> -1109629244;
			i_0_ |= i_0_ >>> 1115354056;
			i_0_ |= i_0_ >>> -1953896048;
			if (i != 423660257) {
				anInt409 = -77;
			}
			return i_0_ - -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "di.K(" + i + ',' + i_0_ + ')');
		}
	}

	public static final String method454(int i, boolean bool) {
		try {
			if (bool != true) {
				findNextGreaterPwr2(94, -10);
			}
			Class98_Sub36 class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.get(i, -1);
			if (class98_sub36 != null) {
				Class98_Sub43_Sub4 class98_sub43_sub4 = class98_sub36.aClass237_Sub1_4157.method2913(1);
				if (class98_sub43_sub4 != null) {
					double d = class98_sub36.aClass237_Sub1_4157.method2901((byte) -100);
					if (class98_sub43_sub4.method1511(-48) <= d && d <= class98_sub43_sub4.method1509(-6085)) {
						return class98_sub43_sub4.method1503(22875);
					}
				}
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "di.L(" + i + ',' + bool + ')');
		}
	}

	public Class48() {
		/* empty */
	}
}

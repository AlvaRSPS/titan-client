/* Class98_Sub10_Sub13 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class98_Sub10_Sub13 extends Class98_Sub10 {
	public static int	anInt5600;
	public static int	anInt5602		= 0;
	public static int[]	anIntArray5603	= new int[5];

	public static final int method1041(Mob class246_sub3_sub4_sub2, int i) {
		try {
			if ((class246_sub3_sub4_sub2.anInt6414 ^ 0xffffffff) == -1) {
				return 0;
			}
			if ((class246_sub3_sub4_sub2.anInt6364 ^ 0xffffffff) != i) {
				Mob class246_sub3_sub4_sub2_0_ = null;
				if ((class246_sub3_sub4_sub2.anInt6364 ^ 0xffffffff) > -32769) {
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(class246_sub3_sub4_sub2.anInt6364, i + -1);
					if (class98_sub39 != null) {
						class246_sub3_sub4_sub2_0_ = class98_sub39.npc;
					}
				} else if ((class246_sub3_sub4_sub2.anInt6364 ^ 0xffffffff) <= -32769) {
					class246_sub3_sub4_sub2_0_ = Class151_Sub9.players[class246_sub3_sub4_sub2.anInt6364 + -32768];
				}
				if (class246_sub3_sub4_sub2_0_ != null) {
					int i_1_ = -class246_sub3_sub4_sub2_0_.boundExtentsX + class246_sub3_sub4_sub2.boundExtentsX;
					int i_2_ = -class246_sub3_sub4_sub2_0_.boundExtentsZ + class246_sub3_sub4_sub2.boundExtentsZ;
					if ((i_1_ ^ 0xffffffff) != -1 || i_2_ != 0) {
						class246_sub3_sub4_sub2.method3042(0x3fff & (int) (2607.5945876176133 * Math.atan2(i_1_, i_2_)), -8193);
					}
				}
			}
			do {
				if (!(class246_sub3_sub4_sub2 instanceof Player)) {
					if (!(class246_sub3_sub4_sub2 instanceof NPC)) {
						break;
					}
					NPC class246_sub3_sub4_sub2_sub1 = (NPC) class246_sub3_sub4_sub2;
					if (class246_sub3_sub4_sub2_sub1.anInt6510 == -1 || class246_sub3_sub4_sub2_sub1.pathLength != 0 && (class246_sub3_sub4_sub2_sub1.anInt6433 ^ 0xffffffff) >= -1) {
						break;
					}
					int i_3_ = class246_sub3_sub4_sub2_sub1.boundExtentsX - (-Class272.gameSceneBaseX + class246_sub3_sub4_sub2_sub1.anInt6510 + -Class272.gameSceneBaseX) * 256;
					int i_4_ = class246_sub3_sub4_sub2_sub1.boundExtentsZ - (-aa_Sub2.gameSceneBaseY + class246_sub3_sub4_sub2_sub1.anInt6505 - aa_Sub2.gameSceneBaseY) * 256;
					if ((i_3_ ^ 0xffffffff) != -1 || i_4_ != 0) {
						class246_sub3_sub4_sub2_sub1.method3042((int) (2607.5945876176133 * Math.atan2(i_3_, i_4_)) & 0x3fff, i ^ ~0x2000);
					}
					class246_sub3_sub4_sub2_sub1.anInt6510 = -1;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				Player player = (Player) class246_sub3_sub4_sub2;
				if (player.nextDirection != -1 && (player.pathLength == 0 || (player.anInt6433 ^ 0xffffffff) < -1)) {
					player.method3042(player.nextDirection, -8193);
					player.nextDirection = -1;
				}
			} while (false);
			return class246_sub3_sub4_sub2.method3033((byte) -39);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hb.E(" + (class246_sub3_sub4_sub2 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static void method1042(int i) {
		try {
			if (i > 126) {
				anIntArray5603 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hb.D(" + i + ')');
		}
	}

	public static final void method1043(byte i) {
		try {
			if ((Class151_Sub9.anInt5028 ^ 0xffffffff) == -2 || (Class151_Sub9.anInt5028 ^ 0xffffffff) == -4 || (OpenGLHeightMapToNormalMapConverter.anInt978 ^ 0xffffffff) != (Class151_Sub9.anInt5028 ^ 0xffffffff) && (Class151_Sub9.anInt5028 == 0
					|| OpenGLHeightMapToNormalMapConverter.anInt978 == 0)) {
				Class150.npcCount = 0;
				Class98_Sub10_Sub20.anInt5640 = 0;
				ProceduralTextureSource.npc.clear(-67);
			}
			OpenGLHeightMapToNormalMapConverter.anInt978 = Class151_Sub9.anInt5028;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hb.B(" + i + ')');
		}
	}

	private int anInt5601 = 4096;

	public Class98_Sub10_Sub13() {
		this(4096);
	}

	Class98_Sub10_Sub13(int i) {
		super(0, true);
		try {
			anInt5601 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hb.<init>(" + i + ')');
		}
	}

	@Override
	public final int[] method990(int i, int i_5_) {
		try {
			if (i != 255) {
				method1043((byte) 117);
			}
			int[] is = this.aClass16_3863.method237((byte) 98, i_5_);
			if (this.aClass16_3863.aBoolean198) {
				ArrayUtils.method2896(is, 0, Class25.anInt268, anInt5601);
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hb.G(" + i + ',' + i_5_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_6_) {
		try {
			if (i_6_ > -92) {
				anIntArray5603 = null;
			}
			int i_7_ = i;
			if ((i_7_ ^ 0xffffffff) == -1) {
				anInt5601 = (class98_sub22.readUnsignedByte((byte) -110) << -1329303188) / 255;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hb.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_6_ + ')');
		}
	}
}

/* Class108 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.clanchat.ClanChatMember;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;
import com.jagex.game.client.preferences.IdleAnimationsPreferenceField;

public final class Class108 {
	public static final String getColour(int playerCombatLevel, int npcCombatLevel, int i_11_) {
		int difference = -npcCombatLevel + playerCombatLevel;
		if (difference < -9) {
			return "<col=ff0000>";
		}
		if ((difference ^ 0xffffffff) > 5) {
			return "<col=ff3000>";
		}
		if (difference < -3) {
			return "<col=ff7000>";
		}
		if ((difference ^ 0xffffffff) > -1) {
			return "<col=ffb000>";
		}
		if (difference > 9) {
			return "<col=00ff00>";
		}
		if ((difference ^ 0xffffffff) < -7) {
			return "<col=40ff00>";
		}
		if ((difference ^ 0xffffffff) < -4) {
			return "<col=80ff00>";
		}
		if ((difference ^ 0xffffffff) < -1) {
			return "<col=c0ff00>";
		}
		return "<col=ffff00>";
	}

	public static final void method1729(int i, Mob mob) {
		do {
			if (i < -89 && (mob.anIntArray6383 != null || mob.anIntArray6370 != null)) {
				boolean bool = true;
				for (int i_0_ = 0; mob.anIntArray6383.length > i_0_; i_0_++) {
					int i_1_ = -1;
					if (mob.anIntArray6383 != null) {
						i_1_ = mob.anIntArray6383[i_0_];
					}
					if (i_1_ == -1) {
						if (!mob.method3043(12, i_0_, -1)) {
							bool = false;
						}
					} else {
						bool = false;
						int i_4_;
						int i_5_;
						if ((i_1_ & ~0x3fffffff ^ 0xffffffff) == 1073741823) {
							int i_6_ = 0xfffffff & i_1_;
							int i_7_ = i_6_ >> 9673934;
							i_5_ = mob.boundExtentsX + -256 + -((i_7_ + -Class272.gameSceneBaseX) * 512);
							int i_8_ = 0x3fff & i_6_;
							i_4_ = -256 - ((-aa_Sub2.gameSceneBaseY + i_8_) * 512 - mob.boundExtentsZ);
						} else if ((0x8000 & i_1_ ^ 0xffffffff) != -1) {
							int i_9_ = 0x7fff & i_1_;
							Player player = Class151_Sub9.players[i_9_];
							if (player != null) {
								i_4_ = mob.boundExtentsZ - player.boundExtentsZ;
								i_5_ = -player.boundExtentsX + mob.boundExtentsX;
							} else {
								mob.method3043(12, i_0_, -1);
								continue;
							}
						} else {
							NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(i_1_, -1);
							if (class98_sub39 != null) {
								NPC npc = class98_sub39.npc;
								i_4_ = mob.boundExtentsZ + -npc.boundExtentsZ;
								i_5_ = mob.boundExtentsX + -npc.boundExtentsX;
							} else {
								mob.method3043(12, i_0_, -1);
								continue;
							}
						}
						if ((i_5_ ^ 0xffffffff) != -1 || (i_4_ ^ 0xffffffff) != -1) {
							mob.method3043(12, i_0_, 0x3fff & (int) (2607.5945876176133 * Math.atan2(i_5_, i_4_)));// mystic
						}
					}
				}
				if (!bool) {
					break;
				}
				mob.anIntArray6370 = null;
				mob.anIntArray6383 = null;
			}
			break;
		} while (false);
	}

	public static final void sendPlayerUpdate(int i, RsBitsBuffers packet, int i_13_) {
		IdleAnimationsPreferenceField.aBoolean3710 = false;
		Class38.DECODE_MASKS_PLAYERS_COUNT = i_13_;
		StoreProgressMonitor.updateGlobalPlayers(packet, i_13_ ^ 0xffffffff);
		ClanChatMember.method2413(packet, 8429);
		if (IdleAnimationsPreferenceField.aBoolean3710) {
			System.out.println("---endgpp---");
		}
		if (i != packet.position) {
			throw new RuntimeException("gpi1 pos:" + packet.position + " psize:" + i);
		}
	}
}

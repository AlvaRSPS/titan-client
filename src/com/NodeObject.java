/* Class98_Sub39 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;

public final class NodeObject extends Node {
	public static double	aDouble4188;
	public static float[]	aFloatArray4189	= new float[4];

	public static final void method1468(int i) {
		PacketParser.buffer.startBitwiseAccess(i ^ i);
		int i_0_ = PacketParser.buffer.readBits((byte) -104, 8);
		if (i_0_ < Class150.npcCount) {
			for (int i_1_ = i_0_; i_1_ < Class150.npcCount; i_1_++) {
				Class246_Sub3_Sub4_Sub3.anIntArray6451[Class259.anInt1954++] = Orientation.npcIndices[i_1_];
			}
		}
		if (Class150.npcCount < i_0_) {
			throw new RuntimeException("gnpov1");
		}
		Class150.npcCount = 0;
		for (int i_2_ = 0; i_2_ < i_0_; i_2_++) {
			int i_3_ = Orientation.npcIndices[i_2_];
			NPC class246_sub3_sub4_sub2_sub1 = ((NodeObject) ProceduralTextureSource.npc.get(i_3_, -1)).npc;
			int i_4_ = PacketParser.buffer.readBits((byte) -74, 1);
			if (i_4_ == 0) {
				Orientation.npcIndices[Class150.npcCount++] = i_3_;
				class246_sub3_sub4_sub2_sub1.anInt6406 = Minimap.anInt1544;
			} else {
				int i_5_ = PacketParser.buffer.readBits((byte) -117, 2);
				if (i_5_ == 0) {
					Orientation.npcIndices[Class150.npcCount++] = i_3_;
					class246_sub3_sub4_sub2_sub1.anInt6406 = Minimap.anInt1544;
					Class76_Sub11.anIntArray3796[Class65.anInt502++] = i_3_;
				} else if (i_5_ == 1) {
					Orientation.npcIndices[Class150.npcCount++] = i_3_;
					class246_sub3_sub4_sub2_sub1.anInt6406 = Minimap.anInt1544;
					int i_6_ = PacketParser.buffer.readBits((byte) -16, 3);
					class246_sub3_sub4_sub2_sub1.method3050(0, 1, i_6_);
					int i_7_ = PacketParser.buffer.readBits((byte) -107, 1);
					if (i_7_ == 1) {
						Class76_Sub11.anIntArray3796[Class65.anInt502++] = i_3_;
					}
				} else if (i_5_ == 2) {
					Orientation.npcIndices[Class150.npcCount++] = i_3_;
					class246_sub3_sub4_sub2_sub1.anInt6406 = Minimap.anInt1544;
					if (PacketParser.buffer.readBits((byte) -42, 1) != 1) {
						int i_8_ = PacketParser.buffer.readBits((byte) -36, 3);
						class246_sub3_sub4_sub2_sub1.method3050(0, 0, i_8_);
					} else {
						int i_9_ = PacketParser.buffer.readBits((byte) -58, 3);
						class246_sub3_sub4_sub2_sub1.method3050(i ^ ~0x134d, 2, i_9_);
						int i_10_ = PacketParser.buffer.readBits((byte) -45, 3);
						class246_sub3_sub4_sub2_sub1.method3050(0, 2, i_10_);
					}
					int i_11_ = PacketParser.buffer.readBits((byte) -65, 1);
					if (i_11_ == 1) {
						Class76_Sub11.anIntArray3796[Class65.anInt502++] = i_3_;
					}
				} else if (i_5_ == 3) {
					Class246_Sub3_Sub4_Sub3.anIntArray6451[Class259.anInt1954++] = i_3_;
				}
			}
		}
	}

	public NPC npc;

	public NodeObject(NPC npc) {
		this.npc = npc;
	}
}

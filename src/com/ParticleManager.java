/* Class45 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5;

public final class ParticleManager {
	public static Sprite				aClass332_383;
	public static int					anInt384;
	public static int					emitterCount;
	public static String				inputBuffer				= "";
	public static int					particleCount;
	public static boolean				particleDebugEnabled	= false;
	public static int					runningSystemCount		= 0;
	public static SceneGraphNodeList	systems;
	public static Js5					vorbisJs5;

	public static final void method430(boolean bool, int i, int i_0_) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_0_, -126, 14);
		class98_sub46_sub17.method1626((byte) -103);
		class98_sub46_sub17.anInt6054 = i;
	}

	public static final void process(long time, boolean bool, RSToolkit toolkit) {
		do {
			emitterCount = 0;
			runningSystemCount = 0;
			Class237_Sub1.anInt5047 = particleCount;
			particleCount = 0;
			long currentTime = TimeTools.getCurrentTime(-47);
			for (Class246_Sub5 class246_sub5 = (Class246_Sub5) systems.getFirst((byte) 15); class246_sub5 != null; class246_sub5 = (Class246_Sub5) systems.getNext(false)) {
				if (class246_sub5.method3121(toolkit, time)) {
					runningSystemCount++;
				}
			}
			if (!particleDebugEnabled || time % 100L != 0L) {
				break;
			}
			System.out.println("Particle system count: " + systems.size(15) + ", running: " + runningSystemCount);
			System.out.println("Emitters: " + emitterCount + " Particles: " + particleCount + ". Time taken: " + (-currentTime + TimeTools.getCurrentTime(-47)) + "ms");
			break;
		} while (false);
	}
}

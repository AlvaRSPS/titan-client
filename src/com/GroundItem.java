/* Class98_Sub26 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.definition.AnimationDefinition;

public final class GroundItem extends Node {
	public static IncomingOpcode	aClass58_4029	= new IncomingOpcode(57, -2);
	public static int				anInt4028		= 0;
	public static int[]				friendsOnline	= new int[200];

	public static final int method1275(String string, boolean bool) {
		return 2 + string.length();
	}

	public static final int method1276(int i) {
		return Class284.titleFontsIndexCrc;
	}

	public static final void method1279(int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, boolean bool) {
		int i_6_ = i_2_ + i;
		int i_7_ = -i_2_ + i_4_;
		for (int i_8_ = i; i_6_ > i_8_; i_8_++) {
			Class333.method3761(i_3_, AnimationDefinition.anIntArrayArray814[i_8_], i_5_, i_1_, (byte) 27);
		}
		int i_9_ = i_1_ + -i_2_;
		for (int i_10_ = i_4_; i_10_ > i_7_; i_10_--) {
			Class333.method3761(i_3_, AnimationDefinition.anIntArrayArray814[i_10_], i_5_, i_1_, (byte) 81);
		}
		int i_11_ = i_2_ + i_5_;
		for (int i_12_ = i_6_; (i_12_ ^ 0xffffffff) >= (i_7_ ^ 0xffffffff); i_12_++) {
			int[] is = AnimationDefinition.anIntArrayArray814[i_12_];
			Class333.method3761(i_3_, is, i_5_, i_11_, (byte) -127);
			Class333.method3761(i_3_, is, i_9_, i_1_, (byte) -126);
		}
	}

	int	amount;

	int	itemId;

	GroundItem(int itemId, int amount) {
		this.itemId = itemId;
		this.amount = amount;
	}
}

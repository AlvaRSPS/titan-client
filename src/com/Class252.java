/* Class252 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.Queue;

public final class Class252 {
	public static Class172[][][]	aClass172ArrayArrayArray1927;
	public static int				anInt1923	= 0;
	public static int				anInt1924;

	private Queue					aClass215_1922;

	private Cacheable				aClass98_Sub46_1925;

	public Class252() {
		/* empty */
	}

	public Class252(Queue class215) {
		aClass215_1922 = class215;
	}

	public final Cacheable method3173(boolean bool) {
		Cacheable class98_sub46 = aClass215_1922.head.nextCacheable;
		if (class98_sub46 == aClass215_1922.head) {
			aClass98_Sub46_1925 = null;
			return null;
		}
		aClass98_Sub46_1925 = class98_sub46.nextCacheable;
		return class98_sub46;
	}

	public final Cacheable method3174(int i) {
		Cacheable class98_sub46 = aClass98_Sub46_1925;
		if (aClass215_1922.head == class98_sub46) {
			aClass98_Sub46_1925 = null;
			return null;
		}
		aClass98_Sub46_1925 = class98_sub46.nextCacheable;
		return class98_sub46;
	}
}

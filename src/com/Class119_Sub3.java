
/* Class119_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class119_Sub3 extends Class119 {
	public static final void method2185(int i, int i_16_) {
		try {
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_16_, -116, i);
			class98_sub46_sub17.method1621(i + -5);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pd.F(" + i + ',' + i_16_ + ')');
		}
	}

	public static final void method2186(int i, RSToolkit toolkit) {
		if (ProxyException.aClass148_30.method2424(0) != 0) {
			if (client.preferences.currentToolkit.getValue((byte) 121) != 0) {
				if (FloorUnderlayDefinitionParser.aHa1225 == null) {
					Canvas canvas = new Canvas();
					canvas.setSize(36, 32);
					FloorUnderlayDefinitionParser.aHa1225 = RSToolkit.createToolkit(0, canvas, 122, client.textureList, 0, client.shaderJs5);
					TextureMetrics.aClass43_1828 = FloorUnderlayDefinitionParser.aHa1225.createFont(FontSpecifications.load((byte) 111, 0, HitmarksDefinitionParser.p11FullIndex, client.fontJs5), Image.loadImages(client.spriteJs5, HitmarksDefinitionParser.p11FullIndex, 0), true);
				}
				for (Class98_Sub12 class98_sub12 = (Class98_Sub12) ProxyException.aClass148_30.getFirst(32); class98_sub12 != null; class98_sub12 = (Class98_Sub12) ProxyException.aClass148_30.getNext(106)) {
					Class98_Sub46_Sub19.itemDefinitionList.getSprite(TextureMetrics.aClass43_1828, false, class98_sub12.anInt3876, class98_sub12.anInt3873, class98_sub12.anInt3875, !class98_sub12.aBoolean3878 ? null : Class87.localPlayer.appearence, FloorUnderlayDefinitionParser.aHa1225,
							class98_sub12.anInt3871, false, class98_sub12.anInt3874, toolkit, false);
					class98_sub12.unlink(55);
				}
			} else {
				for (Class98_Sub12 class98_sub12 = (Class98_Sub12) ProxyException.aClass148_30.getFirst(32); class98_sub12 != null; class98_sub12 = (Class98_Sub12) ProxyException.aClass148_30.getNext(114)) {
					Class98_Sub46_Sub19.itemDefinitionList.getSprite(Class69_Sub2.p11Full, false, class98_sub12.anInt3876, class98_sub12.anInt3873, class98_sub12.anInt3875, class98_sub12.aBoolean3878 ? Class87.localPlayer.appearence : null, toolkit, class98_sub12.anInt3871, false,
							class98_sub12.anInt3874, toolkit, false);
					class98_sub12.unlink(84);
				}
				Class98_Sub43.setAllDirty(2);
			}
		}
	}

	public static final int method2187(byte i, int i_17_) {
		try {
			if (i >= -67) {
				return -87;
			}
			return i_17_ & 0xff;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pd.A(" + i + ',' + i_17_ + ')');
		}
	}

	private int	anInt4728;
	private int	anInt4729;
	private int	anInt4730;
	private int	anInt4731;
	private int	anInt4732;

	private int	anInt4733;

	private int	anInt4734;

	private int	anInt4735;

	Class119_Sub3(int i, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_) {
		super(-1, i_25_, i_26_);
		try {
			anInt4733 = i_20_;
			anInt4735 = i_23_;
			anInt4734 = i_21_;
			anInt4729 = i_18_;
			anInt4732 = i_22_;
			anInt4728 = i;
			anInt4731 = i_19_;
			anInt4730 = i_24_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pd.<init>(" + i + ',' + i_18_ + ',' + i_19_ + ',' + i_20_ + ',' + i_21_ + ',' + i_22_ + ',' + i_23_ + ',' + i_24_ + ',' + i_25_ + ',' + i_26_ + ')');
		}
	}

	@Override
	public final void method2174(int i, int i_14_, int i_15_) {
		do {
			try {
				if (i_15_ == -5515) {
					break;
				}
				method2179((byte) 87, -113, -49);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "pd.C(" + i + ',' + i_14_ + ',' + i_15_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2178(int i, int i_0_, int i_1_) {
		try {
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pd.B(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	@Override
	public final void method2179(byte i, int i_3_, int i_4_) {
		try {
			int i_6_ = i_4_ * anInt4728 >> 1090097196;
			int i_7_ = i_3_ * anInt4729 >> 1808074956;
			int i_8_ = i_4_ * anInt4731 >> -168057524;
			int i_9_ = i_3_ * anInt4733 >> 730639084;
			int i_10_ = anInt4734 * i_4_ >> -676575636;
			int i_11_ = i_3_ * anInt4732 >> -1065106292;
			int i_12_ = i_4_ * anInt4735 >> -1661896212;
			int i_13_ = i_3_ * anInt4730 >> -264501236;
			OpenGLHeap.method1685(i_8_, i_6_, i_10_, true, i_11_, i_9_, i_13_, this.anInt985, i_7_, i_12_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pd.E(" + i + ',' + i_3_ + ',' + i_4_ + ')');
		}
	}
}

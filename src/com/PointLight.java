/* Class98_Sub5 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;

public abstract class PointLight extends Node {
	public static Class157	aClass157_3835	= new Class157();
	public static String	username		= "";

	public static final boolean method960(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_) {
		if ((i_8_ ^ 0xffffffff) <= (i - -i_4_ ^ 0xffffffff) || (i ^ 0xffffffff) <= (i_11_ + i_8_ ^ 0xffffffff)) {
			return false;
		}
		if (i_7_ + i_9_ <= i_6_ || i_9_ >= i_6_ + i_10_) {
			return false;
		}
		if (i_5_ >= -100) {
			return true;
		}
		return true;
	}

	float		intensity;
	int			_y;
	private int	colour;
	int			_x;

	int			_z;

	private int	range;

	PointLight(int x, int y, int z, int _range, int col, float intensity) {
		colour = col;
		_x = x;
		range = _range;
		this.intensity = intensity;
		_z = z;
		_y = y;
	}

	public final int getColour(byte i) {
		return colour;
	}

	public final float getIntensity(boolean bool) {
		return intensity;
	}

	public final int getRange(int i) {
		return range;
	}

	public final int getX(int i) {
		return _x;
	}

	public final int getY(byte i) {
		return _y;
	}

	public final int getZ(int i) {
		return _z;
	}

	public abstract void reposition(int i, byte i_0_, int i_1_, int i_2_);

	public abstract void setIntensity(float intensity, int i);
}

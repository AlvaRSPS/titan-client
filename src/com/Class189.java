/* Class189 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.archive.Js5Exception;

public final class Class189 {
	public static RtInterface	aClass293_1457	= null;
	public static int			anInt1455		= 13156520;

	public static void method2641(byte i) {
		try {
			if (i > 11) {
				aClass293_1457 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mia.B(" + i + ')');
		}
	}

	public static final int method2642(byte i) {
		try {
			if (i != 42) {
				method2641((byte) -66);
			}
			if (Cacheable.anInt4261 == 1) {
				return CacheFileRequest.anInt6309;
			}
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mia.C(" + i + ')');
		}
	}

	public boolean			aBoolean1458;
	public boolean			aBoolean1459;

	public Interface2_Impl1	anInterface2_Impl1_1454;

	public Interface2_Impl1	anInterface2_Impl1_1456;

	public Class189(boolean bool) {
		try {
			aBoolean1459 = bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mia.<init>(" + bool + ')');
		}
	}

	public final void method2639(boolean bool) {
		try {
			if (anInterface2_Impl1_1454 != null) {
				anInterface2_Impl1_1454.method72(!bool);
			}
			if (bool != true) {
				method2642((byte) 27);
			}
			aBoolean1458 = false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mia.D(" + bool + ')');
		}
	}

	public final boolean method2640(int i) {
		try {
			if (i != 13156520) {
				aClass293_1457 = null;
			}
			return !(!aBoolean1458 || aBoolean1459);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mia.A(" + i + ')');
		}
	}
}

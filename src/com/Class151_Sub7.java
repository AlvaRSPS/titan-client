
/* Class151_Sub7 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.AnimationDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatMessageType;

import jaggl.OpenGL;

public final class Class151_Sub7 extends Class151 {
	public static boolean					aBoolean5007;
	public static RtAwtFontWrapper			aClass326_5009;
	public static QuickChatMessageType		aClass348_5008	= new QuickChatMessageType(11, 0, 1, 2);
	public static AdvancedMemoryCache		aClass79_5004	= new AdvancedMemoryCache(4);
	public static AnimationDefinitionParser	animationDefinitionList;
	public static int						anInt5005;
	public static int						cpsOccludedCount;

	public static final void method2466(int i) {
		do {
			try {
				Class175.method2578();
				if (i == -32346) {
					break;
				}
				aClass348_5008 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sl.B(" + i + ')');
			}
			break;
		} while (false);
	}

	public static void method2467(int i) {
		try {
			aClass348_5008 = null;
			aClass79_5004 = null;
			animationDefinitionList = null;
			if (i != 0) {
				method2466(-69);
			}
			aClass326_5009 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sl.H(" + i + ')');
		}
	}

	private Class51				aClass51_5002;

	private OpenGLDisplayList	aClass91_5003;

	Class151_Sub7(OpenGlToolkit var_ha_Sub1, Class51 class51) {
		super(var_ha_Sub1);
		try {
			aClass51_5002 = class51;
			aClass91_5003 = new OpenGLDisplayList(var_ha_Sub1, 2);
			aClass91_5003.newList(0, -30389);
			this.aHa_Sub1_1215.method1845(1, 847872872);
			if (aClass51_5002.aBoolean424) {
				OpenGL.glTexGeni(8194, 9472, 9217);
				OpenGL.glEnable(3170);
			}
			OpenGL.glTexGeni(8192, 9472, 9216);
			OpenGL.glTexGeni(8193, 9472, 9216);
			OpenGL.glEnable(3168);
			OpenGL.glEnable(3169);
			this.aHa_Sub1_1215.method1845(0, 847872872);
			aClass91_5003.endList((byte) -50);
			aClass91_5003.newList(1, -30389);
			this.aHa_Sub1_1215.method1845(1, 847872872);
			if (aClass51_5002.aBoolean424) {
				OpenGL.glDisable(3170);
			}
			OpenGL.glDisable(3168);
			OpenGL.glDisable(3169);
			this.aHa_Sub1_1215.method1845(0, 847872872);
			aClass91_5003.endList((byte) 53);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sl.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + (class51 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final boolean method2439(int i) {
		try {
			if (i != 31565) {
				cpsOccludedCount = -90;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sl.A(" + i + ')');
		}
	}

	@Override
	public final void method2440(boolean bool, boolean bool_5_) {
		do {
			try {
				aClass91_5003.callList('\0', bool);
				if (!aClass51_5002.aBoolean424) {
					break;
				}
				this.aHa_Sub1_1215.method1845(1, 847872872);
				this.aHa_Sub1_1215.setActiveTexture(1, aClass51_5002.aClass42_Sub4_425);
				this.aHa_Sub1_1215.method1845(0, 847872872);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sl.D(" + bool + ',' + bool_5_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2441(int i, int i_0_, int i_1_) {
		try {
			float f = ((0x3 & i) - -1) * -5.0E-4F;
			float f_2_ = (1 + ((0x1d & i) >> -1356659485)) * 5.0E-4F;
			float f_3_ = (0x40 & i ^ 0xffffffff) != -1 ? 9.765625E-4F : 4.8828125E-4F;
			if (i_1_ > -2) {
				aClass91_5003 = null;
			}
			this.aHa_Sub1_1215.method1845(1, 847872872);
			boolean bool = (i & 0x80 ^ 0xffffffff) != -1;
			if (bool) {
				Class98_Sub43_Sub4.aFloatArray5940[0] = f_3_;
				Class98_Sub43_Sub4.aFloatArray5940[2] = 0.0F;
				Class98_Sub43_Sub4.aFloatArray5940[3] = 0.0F;
				Class98_Sub43_Sub4.aFloatArray5940[1] = 0.0F;
			} else {
				Class98_Sub43_Sub4.aFloatArray5940[3] = 0.0F;
				Class98_Sub43_Sub4.aFloatArray5940[0] = 0.0F;
				Class98_Sub43_Sub4.aFloatArray5940[2] = f_3_;
				Class98_Sub43_Sub4.aFloatArray5940[1] = 0.0F;
			}
			OpenGL.glTexGenfv(8192, 9474, Class98_Sub43_Sub4.aFloatArray5940, 0);
			Class98_Sub43_Sub4.aFloatArray5940[0] = 0.0F;
			Class98_Sub43_Sub4.aFloatArray5940[1] = f_3_;
			Class98_Sub43_Sub4.aFloatArray5940[3] = this.aHa_Sub1_1215.anInt4321 * f % 1.0F;
			Class98_Sub43_Sub4.aFloatArray5940[2] = 0.0F;
			OpenGL.glTexGenfv(8193, 9474, Class98_Sub43_Sub4.aFloatArray5940, 0);
			if (aClass51_5002.aBoolean424) {
				Class98_Sub43_Sub4.aFloatArray5940[0] = 0.0F;
				Class98_Sub43_Sub4.aFloatArray5940[3] = f_2_ * this.aHa_Sub1_1215.anInt4321 % 1.0F;
				Class98_Sub43_Sub4.aFloatArray5940[2] = 0.0F;
				Class98_Sub43_Sub4.aFloatArray5940[1] = 0.0F;
				OpenGL.glTexGenfv(8194, 9473, Class98_Sub43_Sub4.aFloatArray5940, 0);
			} else {
				int i_4_ = (int) (16.0F * (f_2_ * this.aHa_Sub1_1215.anInt4321));
				this.aHa_Sub1_1215.setActiveTexture(1, aClass51_5002.aClass42_Sub1Array420[i_4_ % 16]);
			}
			this.aHa_Sub1_1215.method1845(0, 847872872);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sl.G(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	@Override
	public final void method2442(Class42 class42, boolean bool, int i) {
		do {
			try {
				this.aHa_Sub1_1215.setActiveTexture(1, class42);
				this.aHa_Sub1_1215.method1896(260, i);
				if (bool == false) {
					break;
				}
				method2466(-73);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sl.F(" + (class42 != null ? "{...}" : "null") + ',' + bool + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2443(boolean bool, int i) {
		do {
			try {
				if (i == 255) {
					break;
				}
				aClass51_5002 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sl.C(" + bool + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2445(byte i) {
		try {
			aClass91_5003.callList('\001', false);
			if (i <= 25) {
				aClass326_5009 = null;
			}
			this.aHa_Sub1_1215.method1845(1, 847872872);
			this.aHa_Sub1_1215.setActiveTexture(1, null);
			this.aHa_Sub1_1215.method1845(0, 847872872);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sl.E(" + i + ')');
		}
	}
}

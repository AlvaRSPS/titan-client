/* Class246_Sub3_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.ground.OpenGlGround;

public abstract class Animable extends Char {
	public static IncomingOpcode aClass58_6151 = new IncomingOpcode(46, 2);

	public static final void method3003(int i) {
		do {
			try {
				int i_0_ = 0;
				for (/**/; (i_0_ ^ 0xffffffff) > (EnumDefinition.anInt2566 ^ 0xffffffff); i_0_++) {
					Sound class338 = Class245.aClass338Array1865[i_0_];
					boolean bool = false;
					if (class338.aClass98_Sub31_Sub5_2836 == null) {
						class338.delay--;
						if (class338.delay < (class338.method3782(-4) ? -1500 : -10)) {
							bool = true;
						} else {
							if ((class338.aByte2840 ^ 0xffffffff) == -2 && class338.aClass37_2835 == null) {
								class338.aClass37_2835 = Class37.method342(Class76_Sub2.soundEffectsJs5, class338.soundId, 0);
								if (class338.aClass37_2835 == null) {
									continue;
								}
								class338.delay += class338.aClass37_2835.method343();
							} else if (class338.method3782(i ^ 0x548b) && (class338.aClass98_Sub13_2838 == null || class338.aClass98_Sub24_Sub1_2839 == null)) {
								if (class338.aClass98_Sub13_2838 == null) {
									class338.aClass98_Sub13_2838 = Class98_Sub13.method1137(BuildLocation.midiInstrumentsJs5, class338.soundId);
								}
								if (class338.aClass98_Sub13_2838 == null) {
									continue;
								}
								if (class338.aClass98_Sub24_Sub1_2839 == null) {
									class338.aClass98_Sub24_Sub1_2839 = class338.aClass98_Sub13_2838.method1132(new int[] { 22050 });
									if (class338.aClass98_Sub24_Sub1_2839 == null) {
										continue;
									}
								}
							}
							if (class338.delay < 0) {
								int i_1_ = 8192;
								int i_2_;
								if ((class338.anInt2837 ^ 0xffffffff) == -1) {
									i_2_ = class338.volume * (class338.aByte2840 == 3 ? client.preferences.voiceOverVolume.getValue((byte) 121) : client.preferences.soundEffectsVolume.getValue((byte) 125)) >> 1244172898;
								} else {
									int i_3_ = 0x3 & class338.anInt2837 >> 43771896;
									if ((i_3_ ^ 0xffffffff) != (Class87.localPlayer.plane ^ 0xffffffff)) {
										i_2_ = 0;
									} else {
										int i_4_ = (0xff & class338.anInt2837) << -898000343;
										int i_5_ = Class87.localPlayer.getSize(0) << -1121556408;
										int i_6_ = (class338.anInt2837 & 0xff3ad9) >> -375828144;
										int i_7_ = i_5_ + (i_6_ << -1239261591) - (-256 - -Class87.localPlayer.boundExtentsX);
										int i_8_ = class338.anInt2837 >> -936393368 & 0xff;
										int i_9_ = -Class87.localPlayer.boundExtentsZ + (i_8_ << -1280127351) - (-256 - i_5_);
										int i_10_ = -512 + Math.abs(i_7_) + Math.abs(i_9_);
										if ((i_4_ ^ 0xffffffff) > (i_10_ ^ 0xffffffff)) {
											class338.delay = -99999;
											continue;
										}
										if (i_10_ < 0) {
											i_10_ = 0;
										}
										i_2_ = (i_4_ + -i_10_) * client.preferences.areaSoundsVolume.getValue((byte) 127) * class338.volume / i_4_ >> 420110114;
										if (class338.aClass246_Sub3_2834 != null && class338.aClass246_Sub3_2834 instanceof Class246_Sub3_Sub4) {
										}
										if (i_7_ != 0 || (i_9_ ^ 0xffffffff) != -1) {
											int i_13_ = 0x3fff & -4096 + -Class186.cameraX - (int) (Math.atan2(i_7_, i_9_) * 2607.5945876176133);
											if (i_13_ > 8192) {
												i_13_ = -i_13_ + 16384;
											}
											int i_14_;
											if ((i_10_ ^ 0xffffffff) >= -1) {
												i_14_ = 8192;
											} else if (i_10_ >= 4096) {
												i_14_ = 16384;
											} else {
												i_14_ = (-i_10_ + 8192) / 4096 + 8192;
											}
											i_1_ = i_14_ * i_13_ / 8192 - -(-i_14_ + 16384 >> 1231343681);
										}
									}
								}
								if ((i_2_ ^ 0xffffffff) < -1) {
									Class98_Sub24_Sub1 class98_sub24_sub1 = null;
									if ((class338.aByte2840 ^ 0xffffffff) == -2) {
										class98_sub24_sub1 = class338.aClass37_2835.method344().method1269(LinkedList.aClass314_1195);
									} else if (class338.method3782(i + 21637)) {
										class98_sub24_sub1 = class338.aClass98_Sub24_Sub1_2839;
									}
									Class98_Sub31_Sub5 class98_sub31_sub5 = class338.aClass98_Sub31_Sub5_2836 = Class98_Sub31_Sub5.method1402(class98_sub24_sub1, class338.speed, i_2_, i_1_);
									class98_sub31_sub5.method1422(-1 + class338.timesPlayed);
									Class81.aClass98_Sub31_Sub3_619.method1371(class98_sub31_sub5);
								}
							}
						}
					} else if (!class338.aClass98_Sub31_Sub5_2836.isLinked((byte) 78)) {
						bool = true;
					}
					if (bool) {
						EnumDefinition.anInt2566--;
						for (int i_15_ = i_0_; (EnumDefinition.anInt2566 ^ 0xffffffff) < (i_15_ ^ 0xffffffff); i_15_++) {
							Class245.aClass338Array1865[i_15_] = Class245.aClass338Array1865[i_15_ + 1];
						}
						i_0_--;
					}
				}
				if (Class151_Sub5.aBoolean4991 && !InventoriesDefinitionParser.method188(false)) {
					if (client.preferences.musicVolume.getValue((byte) 120) != 0 && (Class144.anInt1169 ^ 0xffffffff) != 0) {
						if (Class151_Sub8.aClass98_Sub31_Sub2_5013 == null) {
							OpenGlGround.method3434(Class98_Sub10_Sub1.musicJs5, false, client.preferences.musicVolume.getValue((byte) 123), Class144.anInt1169, 0, -16523);
						} else {
							Class372.method3959(256, Class144.anInt1169, Class151_Sub8.aClass98_Sub31_Sub2_5013, Class98_Sub10_Sub1.musicJs5, client.preferences.musicVolume.getValue((byte) 122), 0, false);
						}
					}
					Class151_Sub5.aBoolean4991 = false;
					Class151_Sub8.aClass98_Sub31_Sub2_5013 = null;
				} else {
					if ((client.preferences.musicVolume.getValue((byte) 126) ^ 0xffffffff) == -1 || Class144.anInt1169 == -1 || InventoriesDefinitionParser.method188(false)) {
						break;
					}
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class140.aClass171_3248, Class331.aClass117_2811);
					class98_sub11.packet.writeInt(i ^ ~0x5db0ede0, Class144.anInt1169);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class144.anInt1169 = -1;
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ic.IA(" + i + ')');
			}
			break;
		} while (false);
	}

	public static void method3004(int i) {
		try {
			if (i < -117) {
				aClass58_6151 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.EA(" + i + ')');
		}
	}

	public static final void method3005(int i) {
		try {
			int i_20_ = Class105.regionNpcMapData.length;
			int i_21_ = 0;
			if (i != 21378) {
				method3005(36);
			}
			for (/**/; (i_21_ ^ 0xffffffff) > (i_20_ ^ 0xffffffff); i_21_++) {
				if (Class105.regionNpcMapData[i_21_] != null) {
					int i_22_ = -1;
					for (int i_23_ = 0; (i_23_ ^ 0xffffffff) > (Class254.anInt1944 ^ 0xffffffff); i_23_++) {
						if ((HitmarksDefinitionParser.regionPositionHash[i_21_] ^ 0xffffffff) == (Huffman.anIntArray1607[i_23_] ^ 0xffffffff)) {
							i_22_ = i_23_;
							break;
						}
					}
					if (i_22_ == -1) {
						Huffman.anIntArray1607[Class254.anInt1944] = HitmarksDefinitionParser.regionPositionHash[i_21_];
						i_22_ = Class254.anInt1944++;
					}
					RSByteBuffer class98_sub22 = new RSByteBuffer(Class105.regionNpcMapData[i_21_]);
					int i_24_ = 0;
					while ((Class105.regionNpcMapData[i_21_].length ^ 0xffffffff) < (class98_sub22.position ^ 0xffffffff)) {
						if (i_24_ >= 511 || Class150.npcCount >= 1023) {
							break;
						}
						int i_25_ = i_24_++ << 934686470 | i_22_;
						int i_26_ = class98_sub22.readShort((byte) 127);
						int i_27_ = i_26_ >> 1106851790;
						int i_28_ = (i_26_ & 0x1fd6) >> 62218407;
						int i_29_ = 0x3f & i_26_;
						int i_30_ = i_28_ + (HitmarksDefinitionParser.regionPositionHash[i_21_] >> -1827624344) * 64 - Class272.gameSceneBaseX;
						int i_31_ = -aa_Sub2.gameSceneBaseY + 64 * (0xff & HitmarksDefinitionParser.regionPositionHash[i_21_]) - -i_29_;
						NPCDefinition class141 = Class4.npcDefinitionList.get(i + -21373, class98_sub22.readShort((byte) 127));
						NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(i_25_, i ^ ~0x5382);
						if (class98_sub39 == null && (class141.aByte1103 & 0x1) > 0 && (i_27_ ^ 0xffffffff) == (SunDefinitionParser.anInt963 ^ 0xffffffff) && i_30_ >= 0 && Class165.mapWidth > i_30_ + class141.boundSize && i_31_ >= 0 && (Class98_Sub10_Sub7.mapLength
								^ 0xffffffff) < (class141.boundSize + i_31_ ^ 0xffffffff)) {
							NPC class246_sub3_sub4_sub2_sub1 = new NPC();
							class246_sub3_sub4_sub2_sub1.index = i_25_;
							NodeObject class98_sub39_32_ = new NodeObject(class246_sub3_sub4_sub2_sub1);
							ProceduralTextureSource.npc.put(class98_sub39_32_, i_25_, i + -21379);
							BackgroundColourLSEConfig.aClass98_Sub39Array3516[Class98_Sub10_Sub20.anInt5640++] = class98_sub39_32_;
							Orientation.npcIndices[Class150.npcCount++] = i_25_;
							class246_sub3_sub4_sub2_sub1.anInt6406 = Queue.timer;
							class246_sub3_sub4_sub2_sub1.setDefinition(class141, 1);
							class246_sub3_sub4_sub2_sub1.setSize((byte) 70, class246_sub3_sub4_sub2_sub1.definition.boundSize);
							class246_sub3_sub4_sub2_sub1.anInt6414 = class246_sub3_sub4_sub2_sub1.definition.anInt1091 << -1695734205;
							class246_sub3_sub4_sub2_sub1.method3047(class246_sub3_sub4_sub2_sub1.definition.aByte1141 + 4 << 907165931 & 0x3fda, true, 16);
							class246_sub3_sub4_sub2_sub1.method3049(class246_sub3_sub4_sub2_sub1.getSize(0), i_30_, true, (byte) -122, i_31_, i_27_);
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.BA(" + i + ')');
		}
	}

	Animable(int i, int i_33_, int i_34_, int i_35_, int i_36_) {
		try {
			this.anInt5089 = i_33_;
			this.boundExtentsZ = i_34_;
			this.collisionPlane = (byte) i_36_;
			this.boundExtentsX = i;
			this.plane = (byte) i_35_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.<init>(" + i + ',' + i_33_ + ',' + i_34_ + ',' + i_35_ + ',' + i_36_ + ')');
		}
	}

	@Override
	public final boolean method2977(RSToolkit var_ha, byte i) {
		try {
			if (i != 77) {
				method2981(null, (byte) 76, true, 85, null, -57, 85);
			}
			Class154 class154 = Class94.method914(this.plane, this.boundExtentsX >> Class151_Sub8.tileScale, this.boundExtentsZ >> Class151_Sub8.tileScale);
			if (class154 == null || !class154.aClass246_Sub3_Sub4_1232.aBoolean6162) {
				return Class76_Sub5.method758((byte) 103, this.plane, this.boundExtentsZ >> Class151_Sub8.tileScale, this.boundExtentsX >> Class151_Sub8.tileScale);
			}
			return Class195.method2661(this.plane, this.boundExtentsZ >> Class151_Sub8.tileScale, this.boundExtentsX >> Class151_Sub8.tileScale, class154.aClass246_Sub3_Sub4_1232.method2990(0) + method2990(i ^ 0x4d), (byte) -118);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.AA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2980(int i, PointLight[] class98_sub5s) {
		try {
			return method2989(this.boundExtentsX >> Class151_Sub8.tileScale, false, class98_sub5s, this.boundExtentsZ >> Class151_Sub8.tileScale);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.GA(" + i + ',' + (class98_sub5s != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_16_, RSToolkit var_ha, int i_17_, int i_18_) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_16_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_17_ + ',' + i_18_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i >= -70) {
				aClass58_6151 = null;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.HA(" + i + ')');
		}
	}

	@Override
	public final boolean method2991(boolean bool) {
		try {
			if (bool != false) {
				aClass58_6151 = null;
			}
			return Class74.aBooleanArrayArray551[Class259.anInt1959 + (this.boundExtentsX >> Class151_Sub8.tileScale) + -Class241.anInt1845][Class259.anInt1959 + -CharacterShadowsPreferenceField.anInt3714 + (this.boundExtentsZ >> Class151_Sub8.tileScale)];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.FA(" + bool + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		try {
			if (i != -73) {
				aClass58_6151 = null;
			}
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ic.DA(" + i + ')');
		}
	}
}

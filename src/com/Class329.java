/* Class329 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;

public final class Class329 {
	public static int	anInt2761;
	public static int[]	anIntArray2771;

	public static final void method3708(int i) {
		if (Class53_Sub1.method499(i ^ ~0x800, client.clientState) || Class246_Sub3_Sub3.method3011(-6410, client.clientState)) {
			Class185.method2630(SpriteLoadingScreenElement.yCamPosTile >> -802670004, -108, Class98_Sub46_Sub10.xCamPosTile >> 1156493548, 5000);
		} else {
			int xChunk = Class87.localPlayer.pathX[0] >> -1272391965;
			int yChunk = Class87.localPlayer.pathZ[0] >> -1164602557;

			if ((xChunk ^ 0xffffffff) > -1 || Class165.mapWidth >> -310895453 <= xChunk || (yChunk ^ 0xffffffff) > -1 || (yChunk ^ 0xffffffff) <= (Class98_Sub10_Sub7.mapLength >> 1384895075 ^ 0xffffffff)) {
				Class185.method2630(Class98_Sub10_Sub7.mapLength >> -980626748, i + -118, Class165.mapWidth >> -3353948, 0);
			} else {
				// System.out.println(" a: " + xChunk + " b: " + yChunk);
				// DumpUtils.dumpStackTrace();

				Class185.method2630(yChunk, i + -111, xChunk, 5000);
			}
		}
		Js5Exception.method4011(i + -113);
		Class230.renderAmbientAndSunSettings((byte) -74);
		StoreProgressMonitor.method2855(-19004);
		Class96.method928((byte) -42);
	}

	public static final int method3711(byte i, int i_4_) {
		return i_4_ >>> 367185160;
	}

	public static final void method3713(byte i, String string) {
		try {
			OutgoingPacket class98_sub11 = ActionGroup.method1556(false);
			class98_sub11.packet.writeByte(LoginOpcode.aClass222_2490.opcode, -110);
			class98_sub11.packet.writeShort(0, 1571862888);
			int i_5_ = class98_sub11.packet.position;
			class98_sub11.packet.writeShort(637, 1571862888);
			int[] is = Class42_Sub2.method389(12206, class98_sub11);
			int i_6_ = class98_sub11.packet.position;
			class98_sub11.packet.writePJStr1(string, (byte) 113);
			class98_sub11.packet.writeByte(client.gameLanguage, 84);
			class98_sub11.packet.position += 7;
			class98_sub11.packet.method1235(true, is, i_6_, class98_sub11.packet.position);
			class98_sub11.packet.method1207((byte) 90, class98_sub11.packet.position - i_5_);
			Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
			GroundItem.anInt4028 = 0;
			OpenGLRenderEffectManager.anInt442 = -3;
			Class21_Sub4.anInt5394 = 1;
			Class372.anInt3150 = 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uc.F(" + i + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	public int	anInt2760;
	public int	anInt2762;
	public int	anInt2763;
	public int	anInt2764;
	public int	anInt2766;
	public int	anInt2767;
	public int	anInt2768	= 128;

	public int	anInt2769;

	public int	anInt2770;

	public int	anInt2772;

	public int	anInt2773;

	public Class329(int i) {
		anInt2763 = 128;
		try {
			anInt2770 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uc.<init>(" + i + ')');
		}
	}

	private Class329(int i, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_) {
		anInt2763 = 128;
		try {
			anInt2768 = i_9_;
			anInt2770 = i;
			anInt2769 = i_12_;
			anInt2762 = i_10_;
			anInt2763 = i_8_;
			anInt2772 = i_11_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uc.<init>(" + i + ',' + i_8_ + ',' + i_9_ + ',' + i_10_ + ',' + i_11_ + ',' + i_12_ + ')');
		}
	}

	public final void method3709(Class329 class329_2_, int i) {
		try {
			anInt2770 = class329_2_.anInt2770;
			anInt2768 = class329_2_.anInt2768;
			anInt2769 = class329_2_.anInt2769;
			anInt2763 = class329_2_.anInt2763;
			anInt2762 = class329_2_.anInt2762;
			anInt2772 = class329_2_.anInt2772;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uc.C(" + (class329_2_ != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final Class329 method3710(int i) {
		try {
			if (i != 28889) {
				anInt2762 = -34;
			}
			return new Class329(anInt2770, anInt2763, anInt2768, anInt2762, anInt2772, anInt2769);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uc.B(" + i + ')');
		}
	}
}

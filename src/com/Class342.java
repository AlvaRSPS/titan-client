/* Class342 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.parser.VarPlayerDefinitionParser;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;

public final class Class342 {
	public static Class200	aClass200_2861	= new Class200();
	public static int[]		anIntArray2859	= new int[8];
	public static int[]		anIntArray2860	= new int[1];

	public static final void method3814(boolean bool, String username, int dummy, String password) {
		PointLight.username = username;
		Class360.password = password;
		Class246_Sub3_Sub4_Sub1.aBoolean6244 = bool;
		if (!Class246_Sub3_Sub4_Sub1.aBoolean6244 && (PointLight.username.equals("") || Class360.password.equals(""))) {
			Class369.setLoginResponse(3, (byte) -55);
		} else {
			Class76_Sub9.aBoolean3788 = false;
			if ((Class98_Sub46_Sub20_Sub2.anInt6317 ^ 0xffffffff) != -2) {
				Class98_Sub48.anInt4277 = 0;
				Class69_Sub1.anInt5330 = -1;
			}
			Class369.setLoginResponse(-3, (byte) -55);
			VarPlayerDefinitionParser.anInt1087 = 0;
			Class151_Sub9.anInt5020 = 0;
			MaxScreenSizePreferenceField.anInt3680 = 1;
		}
	}

	public static final void method3815(int i, int dummy) {
		GameShell.graphicsUpdateInterval = 1000000000L / i;
	}
}

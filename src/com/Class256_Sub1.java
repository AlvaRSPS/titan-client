/* Class256_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.WorldMapInfoDefinition;

public final class Class256_Sub1 extends Class256 {
	public static int[]		anIntArray5156	= new int[14];
	public static int[]		skillLevels		= new int[25];
	public static String	userName;

	public static final boolean isAdded(int i, String string) {
		if (string == null) {
			return false;
		}
		for (int count = i; count < Class314.friendListSize; count++) {
			if (string.equalsIgnoreCase(Class98_Sub25.friends[count])) {
				return true;
			}
		}
		return string.equalsIgnoreCase(Class87.localPlayer.accountName);
	}

	public static final Class98_Sub47 method3196(byte i) {
		try {
			if (WorldMap.aClass148_2065 == null || PointLight.aClass157_3835 == null) {
				return null;
			}
			PointLight.aClass157_3835.method2505((byte) 26, WorldMap.aClass148_2065);
			Class98_Sub47 class98_sub47 = (Class98_Sub47) PointLight.aClass157_3835.method2504((byte) -116);
			if (class98_sub47 == null) {
				return null;
			}
			WorldMapInfoDefinition class24 = WorldMap.aClass341_2057.get(-88, class98_sub47.anInt4268);
			if (i >= -68) {
				method3196((byte) -25);
			}
			if (class24 != null && class24.aBoolean241 && class24.method284(124, WorldMap.anInterface6_2060)) {
				return class98_sub47;
			}
			return Char.method2979(-76);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "su.C(" + i + ')');
		}
	}

	Class49[] aClass49Array5159;

	Class256_Sub1(Class49[] class49s) {
		try {
			aClass49Array5159 = class49s;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "su.<init>(" + (class49s != null ? "{...}" : "null") + ')');
		}
	}
}

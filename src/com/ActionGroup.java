/* Class98_Sub46_Sub9 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.Queue;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class ActionGroup extends Cacheable {
	public static OutgoingOpcode	aClass171_6000	= new OutgoingOpcode(0, 8);
	/* synthetic */ static Class	aClass6004;
	public static long				aLong5997		= 0L;
	public static int				anInt6003;
	public static boolean			waterDetail		= false;

	public static final void addGroup(ActionGroup group, byte dummy) {
		do {
			if (dummy == 87) {
				boolean bool = false;
				group.uncache((byte) -90);
				for (ActionGroup _group = (ActionGroup) RtInterfaceAttachment.actionGroups.getFirst(-1); _group != null; _group = (ActionGroup) RtInterfaceAttachment.actionGroups.getNext(0)) {
					if (Class378.method4004((byte) 127, group.getFirstActionId(78), _group.getFirstActionId(103))) {
						bool = true;
						Class51.insertNode(dummy + -38, _group, group);
						break;
					}
				}
				if (bool) {
					break;
				}
				RtInterfaceAttachment.actionGroups.insert(group, -107);
			}
			break;
		} while (false);
	}

	public static final void method1553(Char class246_sub3, int i, int i_0_, int i_1_) {
		if (i_0_ < BConfigDefinition.anInt3112) {
			Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_ + 1][i_1_];
			if (class172 != null && class172.aClass246_Sub3_Sub1_1332 != null && class172.aClass246_Sub3_Sub1_1332.method2982((byte) -84)) {
				class246_sub3.method2981(class172.aClass246_Sub3_Sub1_1332, (byte) -94, true, NativeShadow.anInt6333, Class98_Sub10_Sub30.activeToolkit, 0, 0);
			}
		}
		if (i_1_ < BConfigDefinition.anInt3112) {
			Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_][i_1_ + 1];
			if (class172 != null && class172.aClass246_Sub3_Sub1_1332 != null && class172.aClass246_Sub3_Sub1_1332.method2982((byte) -97)) {
				class246_sub3.method2981(class172.aClass246_Sub3_Sub1_1332, (byte) 115, true, 0, Class98_Sub10_Sub30.activeToolkit, 0, NativeShadow.anInt6333);
			}
		}
		if (i_0_ < BConfigDefinition.anInt3112 && i_1_ < Class64_Sub9.anInt3662) {
			Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_ + 1][i_1_ + 1];
			if (class172 != null && class172.aClass246_Sub3_Sub1_1332 != null && class172.aClass246_Sub3_Sub1_1332.method2982((byte) -88)) {
				class246_sub3.method2981(class172.aClass246_Sub3_Sub1_1332, (byte) 117, true, NativeShadow.anInt6333, Class98_Sub10_Sub30.activeToolkit, 0, NativeShadow.anInt6333);
			}
		}
		if (i_0_ < BConfigDefinition.anInt3112 && i_1_ > 0) {
			Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_ + 1][i_1_ - 1];
			if (class172 != null && class172.aClass246_Sub3_Sub1_1332 != null && class172.aClass246_Sub3_Sub1_1332.method2982((byte) -118)) {
				class246_sub3.method2981(class172.aClass246_Sub3_Sub1_1332, (byte) 88, true, NativeShadow.anInt6333, Class98_Sub10_Sub30.activeToolkit, 0, -NativeShadow.anInt6333);
			}
		}
	}

	public static final boolean method1554(int i, int i_2_, int i_3_) {
		return (i & 0x180 ^ 0xffffffff) != -1;
	}

	public static final OutgoingPacket method1556(boolean bool) {
		OutgoingPacket class98_sub11 = Class289.method3410(-1);
		class98_sub11.outgoingOpcode = null;
		class98_sub11.packetSize = 0;
		class98_sub11.packet = new RsBitsBuffers(5000);
		return class98_sub11;
	}

	public static final void method1558(byte i, Class98_Sub33 class98_sub33) {
		if (QuickChatCategory.aClass172ArrayArrayArray5948 != null) {
			Interface19 interface19 = null;
			if (class98_sub33.anInt4118 == 0) {
				interface19 = (Interface19) Class21_Sub1.method268(class98_sub33.anInt4116, class98_sub33.localX, class98_sub33.localZ);
			}
			if (i != 109) {
				waterDetail = false;
			}
			if ((class98_sub33.anInt4118 ^ 0xffffffff) == -2) {
				interface19 = (Interface19) GrandExchangeOffer.method1701(class98_sub33.anInt4116, class98_sub33.localX, class98_sub33.localZ);
			}
			if ((class98_sub33.anInt4118 ^ 0xffffffff) == -3) {
				interface19 = (Interface19) Class246_Sub3_Sub4.method931(class98_sub33.anInt4116, class98_sub33.localX, class98_sub33.localZ, aClass6004 != null ? aClass6004 : (aClass6004 = method1562("com.Interface19")));
			}
			if ((class98_sub33.anInt4118 ^ 0xffffffff) == -4) {
				interface19 = (Interface19) AsyncCache.method3177(class98_sub33.anInt4116, class98_sub33.localX, class98_sub33.localZ);
			}
			if (interface19 != null) {
				class98_sub33.anInt4119 = interface19.method64(30472);
				class98_sub33.anInt4115 = interface19.method63((byte) 20);
				class98_sub33.anInt4121 = interface19.method66(4657);
			} else {
				class98_sub33.anInt4121 = 0;
				class98_sub33.anInt4119 = -1;
				class98_sub33.anInt4115 = 0;
			}
		}
	}

	public static final void method1561(RSToolkit var_ha, int i) {
		int i_6_ = 0;
		int i_7_ = 0;
		if (OpenGLHeap.aBoolean6079) {
			i_6_ = Class189.method2642((byte) 42);
			i_7_ = MapScenesDefinitionParser.method3765(false);
		}
		int i_8_ = -10660793;
		Class42_Sub1.method381(Class15.anInt172, i_7_ + OpenGlArrayBufferPointer.anInt897, var_ha, -16777216, i_8_, Class246_Sub3_Sub4_Sub4.width, 8516, Class38.anInt355 + i_6_);
		Class98_Sub10_Sub34.p13Full.drawString((byte) -73, 14 + OpenGlArrayBufferPointer.anInt897 + i_7_, TextResources.CHOOSE_OPTION.getText(client.gameLanguage, (byte) 25), i_8_, -1, 3 + i_6_ + Class38.anInt355);
		int i_9_ = client.mouseListener.getMouseX(68) + i_6_;
		int i_10_ = client.mouseListener.getMouseY((byte) 90) + i_7_;
		if (Class248.aBoolean1896) {
			int i_11_ = 0;
			for (ActionGroup class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getFirst(-1); class98_sub46_sub9 != null; class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getNext(0)) {
				int i_12_ = i_11_ * 16 + 31 + OpenGlArrayBufferPointer.anInt897 + i_7_;
				i_11_++;
				if (class98_sub46_sub9.actionCount == 1) {
					Class246_Sub3_Sub2_Sub1.method3009(Class15.anInt172, (ActionQueueEntry) class98_sub46_sub9.actions.head.nextCacheable, -256, Class246_Sub3_Sub4_Sub4.width, i_12_, i_7_ + OpenGlArrayBufferPointer.anInt897, i_9_, var_ha, i_10_, 0, -1, Class38.anInt355 - -i_6_);
				} else {
					Class320.method3663(class98_sub46_sub9, -1, i_9_, Class15.anInt172, i_10_, i_6_ + Class38.anInt355, var_ha, i_12_, -256, 0, Class246_Sub3_Sub4_Sub4.width, i_7_ + OpenGlArrayBufferPointer.anInt897);
				}
			}
			if (Class308.aClass98_Sub46_Sub9_2583 != null) {
				Class42_Sub1.method381(Class98_Sub43_Sub4.anInt5938, BackgroundColourLSEConfig.anInt3518, var_ha, -16777216, i_8_, ScalingSpriteLoadingScreenElement.anInt3439, 8516, LoadingScreenSequence.anInt2128);
				Class98_Sub10_Sub34.p13Full.drawString((byte) -50, BackgroundColourLSEConfig.anInt3518 + 14, Class308.aClass98_Sub46_Sub9_2583.actionText, i_8_, -1, LoadingScreenSequence.anInt2128 - -3);
				i_11_ = 0;
				for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class308.aClass98_Sub46_Sub9_2583.actions.getFirst(i + 255); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class308.aClass98_Sub46_Sub9_2583.actions.getNext(0)) {
					int i_13_ = 16 * i_11_ + 31 + BackgroundColourLSEConfig.anInt3518;
					Class246_Sub3_Sub2_Sub1.method3009(Class98_Sub43_Sub4.anInt5938, class98_sub46_sub8, -256, ScalingSpriteLoadingScreenElement.anInt3439, i_13_, BackgroundColourLSEConfig.anInt3518, i_9_, var_ha, i_10_, 0, -1, LoadingScreenSequence.anInt2128);
					i_11_++;
				}
				Class351.method3849(Class98_Sub43_Sub4.anInt5938, -8, LoadingScreenSequence.anInt2128, ScalingSpriteLoadingScreenElement.anInt3439, BackgroundColourLSEConfig.anInt3518);
			}
		} else {
			int i_14_ = 0;
			for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getFirst(i ^ ~0xdf); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getNext(109)) {
				int i_15_ = 31 + i_7_ + OpenGlArrayBufferPointer.anInt897 - -((-i_14_ + -1 + Class359.actionCount) * 16);
				Class246_Sub3_Sub2_Sub1.method3009(Class15.anInt172, class98_sub46_sub8, -256, Class246_Sub3_Sub4_Sub4.width, i_15_, OpenGlArrayBufferPointer.anInt897 + i_7_, i_9_, var_ha, i_10_, 0, -1, Class38.anInt355 - -i_6_);
				i_14_++;
			}
		}
		Class351.method3849(Class15.anInt172, i ^ 0xf8, i_6_ + Class38.anInt355, Class246_Sub3_Sub4_Sub4.width, i_7_ + OpenGlArrayBufferPointer.anInt897);
	}

	/* synthetic */ static Class method1562(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public Queue	actions;

	public int		actionCount;

	public String	actionText;

	ActionGroup(String string) {
		actionText = string;
		actions = new Queue();
	}

	public final boolean addAction(int dummy, ActionQueueEntry action) {
		boolean bool = true;
		action.uncache((byte) -90);
		ActionQueueEntry _action = (ActionQueueEntry) actions.getFirst(-1);
		while (_action != null) {
			if (Class378.method4004((byte) 93, action.actionId, _action.actionId)) {
				Class51.insertNode(83, _action, action);
				actionCount++;
				return !bool;
			}
			_action = (ActionQueueEntry) actions.getNext(0);
			bool = false;
		}
		actions.insert(action, -30);
		actionCount++;
		return bool;
	}

	public final int getFirstActionId(int dummy) {
		if (actions.head.nextCacheable != actions.head) {
			return ((ActionQueueEntry) actions.head.nextCacheable).actionId;
		}
		return -1;
	}

	public final boolean method1557(byte i, ActionQueueEntry class98_sub46_sub8) {
		int i_5_ = getFirstActionId(75);
		class98_sub46_sub8.uncache((byte) -90);
		actionCount--;
		if (i >= -65) {
			actions = null;
		}
		if ((actionCount ^ 0xffffffff) != -1) {
			return i_5_ != getFirstActionId(123);
		}
		unlink(73);
		uncache((byte) -90);
		GraphicsLevelPreferenceField.groupCount--;
		AnimationSkeletonSet.aClass79_6046.put(class98_sub46_sub8.groupHash, this, (byte) -80);
		return false;
	}
}

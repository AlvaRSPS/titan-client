
/* Class88 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;

import com.jagex.core.timetools.general.TimeTools;

public final class SignLink implements Runnable {
	/* synthetic */ static Class	aClass703;
	/* synthetic */ static Class	aClass704;
	/* synthetic */ static Class	aClass705;
	/* synthetic */ static Class	aClass706;
	private static int				cache_id;
	private static String			game_name;
	public static String			java_vendor;
	public static String			javaVersion;
	public static String			osArch;
	private static String			osName;
	public static String			osNameLowerCase;
	public static String			osVersion;
	public static Method			setFocusCycleRoot;
	public static Method			setFocusTraversalKeysEnabled;
	private static volatile long	timeOutTime	= 0L;
	private static String			userHome;

	/* synthetic */ static Class method878(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public static final FileOnDisk openPreferences(int i, String string) {
		try {
			if (i > -64) {
				return null;
			}
			return openPreferences(game_name, string, cache_id, (byte) -106);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	private static final FileOnDisk openPreferences(String gameName, String prefName, int gameMode, byte i_14_) {
		String fileName;
		if (gameMode != 33) {
			if (gameMode != 34) {
				fileName = "titan_" + gameName + "_preferences" + prefName + ".dat";
			} else {
				fileName = "titan_" + gameName + "_preferences" + prefName + "_wip.dat";
			}
		} else {
			fileName = "titan_" + gameName + "_preferences" + prefName + "_rc.dat";
		}
		String[] directories = { "c:/rscache/", "/rscache/", userHome, "c:/windows/", "c:/winnt/", "c:/", "/tmp/", "" };
		for (int count = 0; (count ^ 0xffffffff) > (directories.length ^ 0xffffffff); count++) {
			String dirName = directories[count];
			if (dirName.length() <= 0 || new File(dirName).exists()) {
				try {
					FileOnDisk fileOnDisk = new FileOnDisk(new File(dirName, fileName), "rw", 10000L);
					return fileOnDisk;
				} catch (Exception exception) {
					/* empty */
				}
			}
		}
		return null;
	}

	FileOnDisk						cacheDataFile;
	FileOnDisk						cacheId225File;
	FileOnDisk[]					cacheIndexFiles;
	private Object					directSound;
	public boolean					is_signed;
	private MicrosoftCursor			microsoftCursorLine;
	private MicrosoftDisplayDevice	microsoftDisplayDevice;
	public boolean					msJava;
	FileOnDisk						randomFile;
	private SignLinkRequest			requestStackBottom	= null;
	private SignLinkRequest			requestStackTop;
	private boolean					shutDown;
	private Thread					signLinkThread;

	private Object					sunCursor;

	private Object					sunDisplayDevice;

	EventQueue						systemEventQueue;

	SignLink(int cacheId, String gameName, int indexCount, boolean isSigned) throws Exception {
		cacheId225File = null;
		randomFile = null;
		shutDown = false;
		requestStackTop = null;
		is_signed = false;
		msJava = false;
		cacheDataFile = null;
		try {
			javaVersion = "1.1";
			game_name = gameName;
			is_signed = isSigned;
			java_vendor = "Unknown";
			cache_id = cacheId;
			try {
				java_vendor = System.getProperty("java.vendor");
				javaVersion = System.getProperty("java.version");
			} catch (Exception exception) {
				/* empty */
			}
			if ((java_vendor.toLowerCase().indexOf("microsoft") ^ 0xffffffff) != 0) {
				msJava = true;
			}
			try {
				osName = System.getProperty("os.name");
			} catch (Exception exception) {
				osName = "Unknown";
			}
			osNameLowerCase = osName.toLowerCase();
			try {
				osArch = System.getProperty("os.arch").toLowerCase();
			} catch (Exception exception) {
				osArch = "";
			}
			try {
				osVersion = System.getProperty("os.version").toLowerCase();
			} catch (Exception exception) {
				osVersion = "";
			}
			try {
				userHome = System.getProperty("user.home");
				if (userHome != null) {
					userHome += "/";
				}
			} catch (Exception exception) {
				/* empty */
			}
			if (userHome == null) {
				userHome = "~/";
			}
			try {
				systemEventQueue = Toolkit.getDefaultToolkit().getSystemEventQueue();
			} catch (Throwable throwable) {
				/* empty */
			}
			if (!msJava) {
				try {
					setFocusTraversalKeysEnabled = Class.forName("java.awt.Component").getDeclaredMethod("setFocusTraversalKeysEnabled", Boolean.TYPE);
				} catch (Exception exception) {
					/* empty */
				}
				try {
					setFocusCycleRoot = Class.forName("java.awt.Container").getDeclaredMethod("setFocusCycleRoot", Boolean.TYPE);
				} catch (Exception exception) {
					/* empty */
				}
			}
			CacheLocator.initialize(false, cache_id, game_name);
			if (is_signed) {
				randomFile = new FileOnDisk(CacheLocator.openFile(cache_id, 0, "random.dat", null), "rw", 25L);
				cacheDataFile = new FileOnDisk(CacheLocator.openFile("main_file_cache.dat2", 13), "rw", 209715200L);
				cacheId225File = new FileOnDisk(CacheLocator.openFile("main_file_cache.idx255", 61), "rw", 1048576L);
				cacheIndexFiles = new FileOnDisk[indexCount];
				for (int index = 0; (indexCount ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
					cacheIndexFiles[index] = new FileOnDisk(CacheLocator.openFile("main_file_cache.idx" + index, -128), "rw", 1048576L);
				}
				if (msJava) {
					try {
						directSound = Class.forName("com.Class158").newInstance();
					} catch (Throwable throwable) {
						/* empty */
					}
				}
				try {
					if (!msJava) {
						sunDisplayDevice = Class.forName("com.SunDisplayDevice").newInstance();
					} else {
						microsoftDisplayDevice = new MicrosoftDisplayDevice();
					}
				} catch (Throwable throwable) {
					/* empty */
				}
				try {
					if (!msJava) {
						sunCursor = Class.forName("com.SunCursor").newInstance();// Class321
					} else {
						microsoftCursorLine = new MicrosoftCursor();
					}
				} catch (Throwable throwable) {
					/* empty */
				}
			}
			if (is_signed && !msJava) {
				ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
				for (ThreadGroup childThreadGroup = threadGroup.getParent(); childThreadGroup != null; childThreadGroup = threadGroup.getParent()) {
					threadGroup = childThreadGroup;
				}
				Thread[] threads = new Thread[1000];
				threadGroup.enumerate(threads);
				for (Thread thread : threads) {
					if (thread != null && thread.getName().startsWith("AWT")) {
						thread.setPriority(1);
					}
				}
			}
			shutDown = false;
			signLinkThread = new Thread(this);
			signLinkThread.setPriority(10);
			signLinkThread.setDaemon(true);
			signLinkThread.start();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final boolean canChangeResolution(byte i) {
		if (!is_signed) {
			return false;
		}
		if (!msJava) {
			return sunDisplayDevice != null;
		}
		return microsoftDisplayDevice != null;
	}

	public final SignLinkRequest enterFullScreen(int width, int height, int bitDepth, int refreshRate, int i_12_) {
		if (width != -21605) {
			return null;
		}
		return postRequest((height << 1082083888) - -refreshRate, width + 21511, i_12_ + (bitDepth << 2029886704), 6, null);
	}

	public final SignLinkRequest exitFullScreen(int i, Frame frame) {
		return postRequest(0, -66, 0, 7, frame);
	}

	public final SignLinkRequest getDeclaredField(String fieldName, Class var_class, int i) {
		return postRequest(0, -72, 0, 9, new Object[] { var_class, fieldName });
	}

	public final SignLinkRequest getDeclaredMethod(String methodNames, Class var_class, int i, Class[] parTypes) {
		return postRequest(0, -66, 0, 8, new Object[] { var_class, methodNames, parTypes });
	}

	public final SignLinkRequest listVideoModes(int i) {
		return postRequest(0, -114, 0, 5, null);
	}

	public final Object method867(boolean bool) {
		return directSound;
	}

	public final SignLinkRequest openPreferences(String fileName, boolean useGameName, int i) {
		if (!useGameName) {
			return postRequest(0, i + -21585, 0, 13, fileName);
		}
		return postRequest(0, -119, 0, 12, fileName);
	}

	public final SignLinkRequest openSocket(String hostName, boolean useProxy, int port, boolean bool_7_) {
		return postRequest(0, -93, port, bool_7_ ? 22 : 1, hostName);
	}

	public final SignLinkRequest openUrl(int i, String url) {
		return postRequest(0, -126, 0, 16, url);
	}

	public final SignLinkRequest openUrlDataInputStream(int i, URL url) {
		return postRequest(0, -102, 0, 4, url);
	}

	private final SignLinkRequest postRequest(int opCode, int i_22_, int i_23_, int i_24_, Object object) {
		SignLinkRequest request = new SignLinkRequest();
		request.objectParameter = object;
		request.opCode = i_24_;
		request.intParameter = i_23_;
		request.intParameter1 = opCode;
		synchronized (this) {
			if (requestStackBottom != null) {
				requestStackBottom.next = request;
				requestStackBottom = request;
			} else {
				requestStackBottom = requestStackTop = request;
			}
			notify();
		}
		return request;
	}

	public final SignLinkRequest resolveHostName(int ipAddress, int i_8_) {
		return postRequest(0, -77, ipAddress, 3, null);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public final void run() {
		for (;;) {
			SignLinkRequest currentRequest;
			synchronized (this) {
				for (;;) {
					if (shutDown) {
						return;
					}
					if (requestStackTop != null) {
						currentRequest = requestStackTop;
						requestStackTop = requestStackTop.next;
						if (requestStackTop == null) {
							requestStackBottom = null;
						}
						break;
					}
					try {
						this.wait();
					} catch (InterruptedException interruptedException) {
						/* empty */
					}
				}
			}
			try {
				int opCode = currentRequest.opCode;
				if ((opCode ^ 0xffffffff) != -2) {
					if (opCode == 22) {
						if ((timeOutTime ^ 0xffffffffffffffffL) < (TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL)) {
							throw new IOException();
						}
						try {
							currentRequest.result = SocketWrapper.create((String) currentRequest.objectParameter, 0, currentRequest.intParameter).open(-2);
						} catch (ProxyException proxyException) {
							currentRequest.result = proxyException.getMessage();
							throw proxyException;
						}
					} else if (opCode == 2) {
						Thread thread = new Thread((Runnable) currentRequest.objectParameter);
						thread.setDaemon(true);
						thread.start();
						thread.setPriority(currentRequest.intParameter);
						currentRequest.result = thread;
					} else if ((opCode ^ 0xffffffff) != -5) {
						if ((opCode ^ 0xffffffff) != -9) {
							if ((opCode ^ 0xffffffff) == -10) {
								Object[] objects = (Object[]) currentRequest.objectParameter;
								if (is_signed && ((Class) objects[0]).getClassLoader() == null) {
									throw new SecurityException();
								}
								currentRequest.result = ((Class) objects[0]).getDeclaredField((String) objects[1]);
							} else if (opCode == 18) {
								Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
								currentRequest.result = clipboard.getContents(null);
							} else if (opCode != 19) {
								if (is_signed) {
									if (opCode == 3) {
										if (timeOutTime > TimeTools.getCurrentTime(-47)) {
											throw new IOException();
										}
										String ipString = String.valueOf(0xff & currentRequest.intParameter >> 443133112) + "." + (0xff & currentRequest.intParameter >> -1933975568) + "." + ((currentRequest.intParameter & 0xfffc) >> -963801848) + "." + (0xff & currentRequest.intParameter);
										currentRequest.result = InetAddress.getByName(ipString).getHostName();
									} else if ((opCode ^ 0xffffffff) != -22) {
										if (opCode != 5) {
											if ((opCode ^ 0xffffffff) == -7) {
												Frame frame = new Frame("Jagex Full Screen");
												currentRequest.result = frame;
												frame.setResizable(false);
												if (!msJava) {
													Class.forName("com.SunDisplayDevice").getMethod("enter", new Class[] { aClass703 != null ? aClass703 : (aClass703 = method878("java.awt.Frame")), Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE }).invoke(sunDisplayDevice, frame, new Integer(currentRequest.intParameter >>> -367960272), new Integer(currentRequest.intParameter & 0xffff), new Integer(currentRequest.intParameter1 >> 753354768), new Integer(currentRequest.intParameter1 & 0xffff));
												} else {
													microsoftDisplayDevice.method255(currentRequest.intParameter1 >> 178448656, -2147483648, currentRequest.intParameter >>> 1019450064, currentRequest.intParameter1 & 0xffff, 0xffff & currentRequest.intParameter, frame);
												}
											} else if ((opCode ^ 0xffffffff) == -8) {
												if (!msJava) {
													Class.forName("com.SunDisplayDevice").getMethod("exit", new Class[0]).invoke(sunDisplayDevice);
												} else {
													microsoftDisplayDevice.method253(83, (Frame) currentRequest.objectParameter);
												}
											} else if (opCode == 12) {
												FileOnDisk class356 = openPreferences(game_name, (String) currentRequest.objectParameter, cache_id, (byte) -121);
												currentRequest.result = class356;
											} else if ((opCode ^ 0xffffffff) != -14) {
												if (is_signed && (opCode ^ 0xffffffff) == -15) {
													int x = currentRequest.intParameter;
													int y = currentRequest.intParameter1;
													if (msJava) {
														microsoftCursorLine.method356(1, y, x);
													} else {
														Class.forName("com.SunCursor").getDeclaredMethod("moveMouse", new Class[] { Integer.TYPE, Integer.TYPE }).invoke(sunCursor, new Integer(x), new Integer(y));
													}
												} else if (is_signed && (opCode ^ 0xffffffff) == -16) {
													boolean bool = currentRequest.intParameter != 0;
													Component component = (Component) currentRequest.objectParameter;
													if (msJava) {
														microsoftCursorLine.method358(bool, component, (byte) -104);
													} else {
														Class.forName("com.SunCursor").getDeclaredMethod("showCursor", new Class[] { aClass704 != null ? aClass704 : (aClass704 = method878("java.awt.Component")), Boolean.TYPE }).invoke(sunCursor, component, new Boolean(bool));
													}
												} else if (msJava || opCode != 17) {
													if (opCode != 16) {
														throw new Exception("");
													}
													try {
														if (!osNameLowerCase.startsWith("win")) {
															throw new Exception();
														}
														String string = (String) currentRequest.objectParameter;
														if (!string.startsWith("http://") && !string.startsWith("https://")) {
															throw new Exception();
														}
														String validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?&=,.%+-_#:/*";
														for (int i_6_ = 0; (i_6_ ^ 0xffffffff) > (string.length() ^ 0xffffffff); i_6_++) {
															if (validCharacters.indexOf(string.charAt(i_6_)) == -1) {
																throw new Exception();
															}
														}
														Runtime.getRuntime().exec("cmd /c start \"j\" \"" + string + "\"");
														currentRequest.result = null;
													} catch (Exception exception) {
														currentRequest.result = exception;
														throw exception;
													}
												} else {
													Object[] objects = (Object[]) currentRequest.objectParameter;
													Class.forName("com.SunCursor").getDeclaredMethod("setCustomCursor", new Class[] { aClass704 != null ? aClass704 : (aClass704 = method878("java.awt.Component")), aClass705 != null ? aClass705 : (aClass705 = method878("[I")), Integer.TYPE,
															Integer.TYPE, aClass706 != null ? aClass706 : (aClass706 = method878("java.awt.Point")) }).invoke(sunCursor, objects[0], objects[1], new Integer(currentRequest.intParameter), new Integer(
																	currentRequest.intParameter1), objects[2]);
												}
											} else {
												FileOnDisk class356 = openPreferences("", (String) currentRequest.objectParameter, cache_id, (byte) -110);
												currentRequest.result = class356;
											}
										} else if (msJava) {
											currentRequest.result = microsoftDisplayDevice.method252(false);
										} else {
											currentRequest.result = Class.forName("com.SunDisplayDevice").getMethod("listModes", new Class[0]).invoke(sunDisplayDevice);
										}
									} else {
										if ((timeOutTime ^ 0xffffffffffffffffL) < (TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL)) {
											throw new IOException();
										}
										currentRequest.result = InetAddress.getByName((String) currentRequest.objectParameter).getAddress();
									}
								} else {
									throw new Exception("");
								}
							} else {
								Transferable transferable = (Transferable) currentRequest.objectParameter;
								Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
								clipboard.setContents(transferable, null);
							}
						} else {
							Object[] objects = (Object[]) currentRequest.objectParameter;
							if (is_signed && ((Class) objects[0]).getClassLoader() == null) {
								throw new SecurityException();
							}
							currentRequest.result = ((Class) objects[0]).getDeclaredMethod((String) objects[1], (Class[]) objects[2]);
						}
					} else {
						if (timeOutTime > TimeTools.getCurrentTime(-47)) {
							throw new IOException();
						}
						currentRequest.result = new DataInputStream(((URL) currentRequest.objectParameter).openStream());
					}
				} else {
					if ((timeOutTime ^ 0xffffffffffffffffL) < (TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL)) {
						throw new IOException();
					}
					currentRequest.result = new Socket(InetAddress.getByName((String) currentRequest.objectParameter), currentRequest.intParameter);
				}
				currentRequest.status = 1;// Request completed
			} catch (ThreadDeath threadDeath) {
				throw threadDeath;
			} catch (Throwable throwable) {
				currentRequest.status = 2;// Request failed
			}
			synchronized (currentRequest) {
				currentRequest.notify();
			}
		}
	}

	public final SignLinkRequest setCustomcursor(int[] pixels, Point hotSpot, int width, byte i_19_, int height, Component component) {
		return postRequest(width, -101, height, 17, new Object[] { component, pixels, hotSpot });
	}

	public final void shutDown(int i) {
		synchronized (this) {
			shutDown = true;
			notifyAll();
		}
		try {
			signLinkThread.join();
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
		if (cacheDataFile != null) {
			try {
				cacheDataFile.close(true);
			} catch (IOException ioexception) {
				/* empty */
			}
		}
		if (i >= 54) {
			if (cacheId225File != null) {
				try {
					cacheId225File.close(true);
				} catch (IOException ioexception) {
					/* empty */
				}
			}
			if (cacheIndexFiles != null) {
				for (int i_21_ = 0; (i_21_ ^ 0xffffffff) > (cacheIndexFiles.length ^ 0xffffffff); i_21_++) {
					if (cacheIndexFiles[i_21_] != null) {
						try {
							cacheIndexFiles[i_21_].close(true);
						} catch (IOException ioexception) {
							/* empty */
						}
					}
				}
			}
			if (randomFile != null) {
				try {
					randomFile.close(true);
				} catch (IOException ioexception) {
					/* empty */
				}
			}
		}
	}

	public final SignLinkRequest startThread(int priority, Runnable runnable, int i_0_) {
		return postRequest(0, -115, priority, 2, runnable);
	}

	public final void startTimeOutTimer(int i) {
		timeOutTime = 5000L + TimeTools.getCurrentTime(-47);
	}

	public final boolean writeFile(byte[] is, boolean bool, File file) {
		try {
			FileOutputStream fileoutputstream = new FileOutputStream(file);
			if (file == null) {
				return false;
			}
			// System.out.println("BytesArray: " + Arrays.toString(is) + "
			// Length: " + is.length);
			// if (file.exists())
			// System.out.println("File exists mate");
			fileoutputstream.write(is, 0, is.length);
			fileoutputstream.close();
			return bool == true;
		} catch (IOException ioexception) {
			ioexception.printStackTrace();
			throw new RuntimeException();
		}
	}
}

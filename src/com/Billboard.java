
/* Class177 - Decompiled by JODE
 */ package com; /*
					*/

import jaggl.OpenGL;

public final class Billboard {
	public static Class103			aClass103_1375;
	public static Sprite[]			aClass332Array1382;
	public static float				aFloat1378		= 1.0F;
	public static IncomingOpcode	VARPBIT_SMALL	= new IncomingOpcode(5, 6);

	public static final Class336 method2584(OpenGlToolkit var_ha_Sub1, Class345[] class345s, boolean bool) {
		for (int i = 0; (class345s.length ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
			if (class345s[i] == null || (class345s[i].aLong2891 ^ 0xffffffffffffffffL) >= -1L) {
				return null;
			}
		}
		long l = OpenGL.glCreateProgramObjectARB();
		for (int i = 0; (i ^ 0xffffffff) > (class345s.length ^ 0xffffffff); i++) {
			OpenGL.glAttachObjectARB(l, class345s[i].aLong2891);
		}
		OpenGL.glLinkProgramARB(l);
		if (bool != true) {
			method2584(null, null, false);
		}
		OpenGL.glGetObjectParameterivARB(l, 35714, SystemInformation.anIntArray1177, 0);
		if ((SystemInformation.anIntArray1177[0] ^ 0xffffffff) == -1) {
			if ((SystemInformation.anIntArray1177[0] ^ 0xffffffff) == -1) {
				System.out.println("Shader linking failed:");
			}
			OpenGL.glGetObjectParameterivARB(l, 35716, SystemInformation.anIntArray1177, 1);
			if ((SystemInformation.anIntArray1177[1] ^ 0xffffffff) < -2) {
				byte[] is = new byte[SystemInformation.anIntArray1177[1]];
				OpenGL.glGetInfoLogARB(l, SystemInformation.anIntArray1177[1], SystemInformation.anIntArray1177, 0, is, 0);
				System.out.println(new String(is));
			}
			if (SystemInformation.anIntArray1177[0] == 0) {
				for (int i = 0; (class345s.length ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
					OpenGL.glDetachObjectARB(l, class345s[i].aLong2891);
				}
				OpenGL.glDeleteObjectARB(l);
				return null;
			}
		}
		return new Class336(var_ha_Sub1, l, class345s);
	}

	public boolean	aBoolean1377;
	public boolean	aBoolean1383;
	public int		texture	= -1;
	public int		anInt1374	= 64;
	public int		anInt1379;

	public int		anInt1380	= 64;

	public int		anInt1384;

	public Billboard() {
		aBoolean1377 = false;
		aBoolean1383 = false;
		anInt1384 = 1;
		anInt1379 = 2;
	}

	public final void decode(byte i, RSByteBuffer buffer, int i_2_) {
		for (;;) {
			int opcode = buffer.readUnsignedByte((byte) -125);
			if ((opcode ^ 0xffffffff) == -1) {
				break;
			}
			this.decode(buffer, opcode, 6, i_2_);
		}

	}

	private final void decode(RSByteBuffer buffer, int opcode, int i_0_, int i_1_) {
		do {
			if (i_0_ == 6) {
				if (opcode != 1) {
					if ((opcode ^ 0xffffffff) == -3) {
						anInt1374 = 1 + buffer.readShort((byte) 127);
						anInt1380 = buffer.readShort((byte) 127) + 1;
					} else if ((opcode ^ 0xffffffff) != -4) {
						if ((opcode ^ 0xffffffff) != -5) {
							if ((opcode ^ 0xffffffff) != -6) {
								if (opcode != 6) {
									if (opcode == 7) {
										aBoolean1377 = true;
									}
								} else {
									aBoolean1383 = true;
								}
							} else {
								anInt1384 = buffer.readUnsignedByte((byte) 22);
							}
						} else {
							anInt1379 = buffer.readUnsignedByte((byte) -100);
						}
					} else {
						buffer.readSignedByte((byte) -19);
					}
				} else {
					texture = buffer.readShort((byte) 127);
					if (texture != 65535) {
						break;
					}
					texture = -1;
				}
			}
			break;
		} while (false);
	}
}

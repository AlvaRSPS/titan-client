
/* IOException_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;

import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;

public final class ProxyException extends IOException {
	public static LinkedList	aClass148_30	= new LinkedList();
	public static float			aFloat31;

	public static void method126(int i) {
		try {
			if (i == 65535) {
				aClass148_30 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wh.A(" + i + ')');
		}
	}

	public static final int method127(int i, int i_0_) {
		if (GraphicsDefinitionParser.aShortArrayArray2534 != null) {
			return GraphicsDefinitionParser.aShortArrayArray2534[i][i_0_] & 0xffff;
		}
		return 0;
	}

	ProxyException(String string) {
		super(string);
	}
}

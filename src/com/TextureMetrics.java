/* Class238 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.elements.config.ProgressBarLSEConfig;
import com.jagex.game.toolkit.font.Font;

public final class TextureMetrics {
	public static QuickChatMessageType	aClass348_1834	= new QuickChatMessageType(15, 0, 1, 0);
	public static Font					aClass43_1828;
	public static int[]					anIntArray1836	= new int[2];

	public static final int method2917(int i, RsBitsBuffers buffer) {
		if (i != 0) {
			return 89;
		}
		int i_0_ = buffer.readBits((byte) -115, 2);
		int i_1_;
		if ((i_0_ ^ 0xffffffff) != -1) {
			if ((i_0_ ^ 0xffffffff) != -2) {
				if ((i_0_ ^ 0xffffffff) == -3) {
					i_1_ = buffer.readBits((byte) -58, 8);
				} else {
					i_1_ = buffer.readBits((byte) -110, 11);
				}
			} else {
				i_1_ = buffer.readBits((byte) -84, 5);
			}
		} else {
			i_1_ = 0;
		}
		return i_1_;
	}

	public static final boolean method2919(int i, int i_2_, int i_3_) {
		return (IncomingOpcode.method523(i_3_, -1, i_2_) | Class105.method1715(true, i_2_, i_3_) | Class76_Sub9.method766(-99, i_3_, i_2_)) & ProgressBarLSEConfig.method901(i_3_, i_2_, -122);

	}

	public static final void method2920(int i) {
		for (int i_4_ = 0; (i_4_ ^ 0xffffffff) > (Class281.flags.length ^ 0xffffffff); i_4_++) {
			for (int i_5_ = 0; i_5_ < Class281.flags[0].length; i_5_++) {
				for (int i_6_ = 0; (i_6_ ^ 0xffffffff) > (Class281.flags[0][0].length ^ 0xffffffff); i_6_++) {
					Class281.flags[i_4_][i_5_][i_6_] = (byte) 0;
				}
			}
		}
	}

	public boolean	aBoolean1817;
	public boolean	aBoolean1819;
	public boolean	aBoolean1822;
	public boolean	aBoolean1824;
	public boolean	aBoolean1825;
	public boolean	aBoolean1826;
	public boolean	aBoolean1827;
	public boolean	aBoolean1833;
	public byte		aByte1816;
	public byte		aByte1820;
	public byte		aByte1823;
	public byte		aByte1829;
	public byte		aByte1830;
	public byte		aByte1832;
	public byte		aByte1837;
	public int		anInt1818;
	public int		anInt1821;
	public int		anInt1835;
	public short	colour;

	public TextureMetrics() {
		/* empty */
	}
}

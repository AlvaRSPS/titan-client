/* Class171 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class OutgoingOpcode {
	public static int anInt1318;

	public static final void method2540(int i, int i_0_, FontSpecifications class197, int i_1_, RtInterface class293, int i_2_, byte i_3_, String string, RtInterfaceClip var_aa, int i_4_, int i_5_, Font class43) {
		try {
			int i_6_;
			do {
				if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) == -5) {
					i_6_ = (int) RsFloatBuffer.aFloat5794 & 0x3fff;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				i_6_ = (int) RsFloatBuffer.aFloat5794 - -Class204.anInt1553 & 0x3fff;
			} while (false);
			int i_7_ = Math.max(class293.renderWidth / 2, class293.renderHeight / 2) - -10;
			int i_8_ = i_5_ * i_5_ + i * i;
			if ((i_7_ * i_7_ ^ 0xffffffff) <= (i_8_ ^ 0xffffffff)) {
				if (i_3_ < 73) {
					method2540(52, -42, null, -60, null, 123, (byte) 1, null, null, -98, -39, null);
				}
				int i_9_ = Class284_Sub2_Sub2.SINE[i_6_];
				int i_10_ = Class284_Sub2_Sub2.COSINE[i_6_];
				if (Class98_Sub46_Sub20_Sub2.cameraMode != 4) {
					i_9_ = i_9_ * 256 / (Class151.anInt1213 + 256);
					i_10_ = i_10_ * 256 / (256 + Class151.anInt1213);
				}
				int i_11_ = i_5_ * i_9_ + i * i_10_ >> 2091290062;
				int i_12_ = i_10_ * i_5_ + -(i * i_9_) >> 1204621454;
				int i_13_ = class197.getStringWidth(100, string, null, (byte) 106);
				int i_14_ = class197.getStringHeight(null, 100, 0, string, true);
				i_11_ -= i_13_ / 2;
				if (-class293.renderWidth <= i_11_ && (class293.renderWidth ^ 0xffffffff) <= (i_11_ ^ 0xffffffff) && -class293.renderHeight <= i_12_ && (i_12_ ^ 0xffffffff) >= (class293.renderHeight ^ 0xffffffff)) {
					class43.drawString(class293.renderWidth / 2 + i_11_ - -i_0_, null, i_13_, string, i_0_, 0, var_aa, 1, (byte) -75, i_4_, null, 0, i_1_, 0, -i_14_ + -i_12_ + i_1_ - (-(class293.renderHeight / 2) - -i_2_), 50);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lga.B(" + i + ',' + i_0_ + ',' + (class197 != null ? "{...}" : "null") + ',' + i_1_ + ',' + (class293 != null ? "{...}" : "null") + ',' + i_2_ + ',' + i_3_ + ',' + (string != null ? "{...}" : "null") + ',' + (var_aa != null
					? "{...}" : "null") + ',' + i_4_ + ',' + i_5_ + ',' + (class43 != null ? "{...}" : "null") + ')');
		}
	}

	private int	opcode;

	int			size;

	public OutgoingOpcode(int i, int i_15_) {
		try {
			opcode = i;
			size = i_15_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lga.<init>(" + i + ',' + i_15_ + ')');
		}
	}

	public final int method2541(int i) {
		try {
			if (i != 2) {
				anInt1318 = -128;
			}
			return opcode;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lga.A(" + i + ')');
		}
	}

	@Override
	public final String toString() {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lga.toString(" + ')');
		}
	}
}

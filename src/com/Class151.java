/* Class151 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.ui.layout.HorizontalAlignment;

public abstract class Class151 {
	public static HorizontalAlignment	aClass63_1216;
	public static int					anInt1213	= 0;
	public static int					anInt1214;

	public static final int method2444(int i, int i_3_) {
		return i & 0xff;
	}

	OpenGlToolkit aHa_Sub1_1215;

	Class151(OpenGlToolkit var_ha_Sub1) {
		aHa_Sub1_1215 = var_ha_Sub1;
	}

	abstract boolean method2439(int i);

	public abstract void method2440(boolean bool, boolean bool_0_);

	public abstract void method2441(int i, int i_1_, int i_2_);

	public abstract void method2442(Class42 class42, boolean bool, int i);

	public abstract void method2443(boolean bool, int i);

	public abstract void method2445(byte i);
}

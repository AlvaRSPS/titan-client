
/* Class123 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;

public abstract class StreamHandler {
	public static AdvancedMemoryCache	aClass79_1010	= new AdvancedMemoryCache(8);
	public static long					aLong1011;

	public static final boolean method2205(int i, byte[] is, int i_3_, int i_4_, int i_5_, int i_6_) {
		boolean bool = true;
		RSByteBuffer class98_sub22 = new RSByteBuffer(is);
		int i_7_ = -1;
		for (;;) {
			int i_8_ = class98_sub22.method1208(3893);
			if ((i_8_ ^ 0xffffffff) == -1) {
				break;
			}
			i_7_ += i_8_;
			int i_9_ = 0;
			boolean bool_10_ = false;
			for (;;) {
				if (!bool_10_) {
					int i_11_ = class98_sub22.readSmart(1689622712);
					if ((i_11_ ^ 0xffffffff) == -1) {
						break;
					}
					i_9_ += i_11_ + -1;
					int i_12_ = 0x3f & i_9_;
					int i_13_ = 0x3f & i_9_ >> 145149062;
					int i_14_ = class98_sub22.readUnsignedByte((byte) -122) >> -2093245406;
					int i_15_ = i_13_ - -i_3_;
					int i_16_ = i_12_ - -i;
					if ((i_15_ ^ 0xffffffff) < -1 && i_16_ > 0 && (-1 + i_4_ ^ 0xffffffff) < (i_15_ ^ 0xffffffff) && i_16_ < -1 + i_5_) {
						GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(i_7_, (byte) 119);
						if ((i_14_ ^ 0xffffffff) != -23 || (client.preferences.groundDecoration.getValue((byte) 120) ^ 0xffffffff) != -1 || class352.anInt2998 != 0 || class352.actionCount == 1 || class352.aBoolean2969) {
							if (!class352.method3857(18182)) {
								VarClientDefinitionParser.anInt1043++;
								bool = false;
							}
							bool_10_ = true;
						}
					}
				} else {
					int i_17_ = class98_sub22.readSmart(1689622712);
					if (i_17_ == 0) {
						break;
					}
					class98_sub22.readUnsignedByte((byte) -124);
				}
			}
		}
		return bool;
	}

	public static final void method2206(RtInterface class293, byte i) {
		try {
			if (i == 19) {
				if (class293.contentType == Class375.anInt3168) {
					if (Class87.localPlayer.accountName == null) {
						class293.defaultMediaId = 0;
						class293.anInt2210 = 0;
					} else {
						class293.rotationX = 150;
						class293.rotationY = (int) (256.0 * Math.sin(Queue.timer / 40.0)) & 0x7ff;
						class293.defaultMediaId = OpenGLHeap.localPlayerIndex;
						class293.defaultMediaType = 5;
						class293.anInt2210 = GraphicsBuffer.method1438(Class87.localPlayer.accountName, i + 6224);
						class293.anInt2312 = 0;
						class293.anInt2303 = Class87.localPlayer.anInt6350;
						class293.anInt2287 = Class87.localPlayer.anInt6419;
						class293.defaultAnimation = Class87.localPlayer.anInt6385;
						AnimationDefinition class97 = (class293.defaultAnimation ^ 0xffffffff) == 0 ? null : Class151_Sub7.animationDefinitionList.method2623(class293.defaultAnimation, 16383);
						if (class97 != null) {
							QuickChatMessageParser.method3327(class293.anInt2303, class97, (byte) 93);
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ida.J(" + (class293 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static final short[] method2209(int i, int i_20_, short[] is) {
		short[] is_21_ = new short[i];
		ArrayUtils.arrayCopy(is, 0, is_21_, 0, i);
		return is_21_;
	}

	public StreamHandler() {
		/* empty */
	}

	public abstract void close(int i);

	public abstract boolean isBuffered(int i, int i_2_) throws IOException;

	public abstract int read(byte[] is, int i, int i_18_, int i_19_) throws IOException;

	public abstract void setDummyMode(int i);

	public abstract void write(int i, int i_0_, byte[] is, int i_1_) throws IOException;
}


/* Class98_Sub46_Sub20_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.lang.ref.SoftReference;

public final class Class98_Sub46_Sub20_Sub1 extends Class98_Sub46_Sub20 {
	private SoftReference aSoftReference6314;

	Class98_Sub46_Sub20_Sub1(CacheKey interface20, Object object, int i) {
		super(interface20, i);
		try {
			aSoftReference6314 = new SoftReference(object);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	public final Object method1635(int i) {
		Object object;
		try {
			object = aSoftReference6314.get();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return object;
	}

	@Override
	public final boolean method1638(int i) {
		boolean bool;
		try {
			if (i != 896) {
				return true;
			}
			bool = true;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return bool;
	}
}

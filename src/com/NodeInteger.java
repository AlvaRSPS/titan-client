/* Class98_Sub34 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementFactory;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class NodeInteger extends Node {
	public static Js5				aClass207_4127;
	public static IncomingOpcode	SEND_INTERFACE_ANIMATION	= new IncomingOpcode(86, 6);

	public static final void method1450(int i, int i_0_, int i_1_, int i_2_, byte i_3_, int i_4_, int i_5_) {
		int playerCount = Class2.playerCount;
		int[] playerIndices = Class319.playerIndices;
		Class48_Sub2_Sub1.anInt5532 = 0;
		for (int index = 0; (index ^ 0xffffffff) > (Class150.npcCount + playerCount ^ 0xffffffff); index++) {
			NPCDefinition definition = null;
			Mob mob;
			if ((index ^ 0xffffffff) <= (playerCount ^ 0xffffffff)) {
				mob = ((NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[-playerCount + index], -1)).npc;
				definition = ((NPC) mob).definition;
				if (definition.anIntArray1109 != null) {
					definition = definition.method2300(StartupStage.varValues, (byte) 127);
					if (definition == null) {
						continue;
					}
				}
			} else {
				mob = Class151_Sub9.players[playerIndices[index]];
			}
			if (mob.anInt6372 >= 0 && ((RemoveRoofsPreferenceField.anInt3676 ^ 0xffffffff) == (mob.anInt6417 ^ 0xffffffff) || mob.plane == Class87.localPlayer.plane)) {
				DataFs.method243(mob.method3044(false), i_0_ >> 620118273, -100, i_2_, mob, i_1_ >> -120706623, i);
				if ((Class259.anIntArray1957[0] ^ 0xffffffff) <= -1) {
					if (mob.message != null && ((playerCount ^ 0xffffffff) >= (index ^ 0xffffffff) || (Class265.friendsChatStatus ^ 0xffffffff) == -1 || Class265.friendsChatStatus == 3 || Class265.friendsChatStatus == 1 && Class256_Sub1.isAdded(0, ((Player) mob).accountName)) && (Class230.anInt1725
							^ 0xffffffff) < (Class48_Sub2_Sub1.anInt5532 ^ 0xffffffff)) {
						Class230.anIntArray1729[Class48_Sub2_Sub1.anInt5532] = Class42_Sub1.p13FullMetrics.method2674(mob.message, 124) / 2;
						Class230.anIntArray1724[Class48_Sub2_Sub1.anInt5532] = Class259.anIntArray1957[0];
						Class230.anIntArray1730[Class48_Sub2_Sub1.anInt5532] = Class259.anIntArray1957[1];
						Class230.anIntArray1726[Class48_Sub2_Sub1.anInt5532] = mob.anInt6398;
						Class230.anIntArray1728[Class48_Sub2_Sub1.anInt5532] = mob.anInt6402;
						Class230.anIntArray1722[Class48_Sub2_Sub1.anInt5532] = mob.anInt6384;
						Class230.aStringArray1721[Class48_Sub2_Sub1.anInt5532] = mob.message;
						Class48_Sub2_Sub1.anInt5532++;
					}
					int i_8_ = i_5_ + Class259.anIntArray1957[1];
					if (!mob.aBoolean6371 && (Queue.timer ^ 0xffffffff) > (mob.anInt6418 ^ 0xffffffff)) {
						int i_9_ = -1;
						int i_10_ = 1;
						if (playerCount <= index) {
							i_9_ = definition.anInt1127;
							if ((i_9_ ^ 0xffffffff) == 0) {
								i_9_ = mob.method3039(i_3_ ^ ~0x7a).anInt2381;
							}
						} else {
							Player player = Class151_Sub9.players[playerIndices[index]];
							i_9_ = mob.method3039(1).anInt2381;
							if (player.clanmate) {
								i_10_ = 2;
							}
						}
						Sprite[] class332s = Class254.aClass332Array1943;
						if (i_9_ != -1) {
							Sprite[] sprite = (Sprite[]) Class275.aClass79_2046.get(-128, i_9_);
							if (sprite == null) {
								Image[] class324s = Image.loadImages(client.spriteJs5, i_9_, 0);
								if (class324s != null) {
									sprite = new Sprite[class324s.length];
									for (int i_12_ = 0; (class324s.length ^ 0xffffffff) < (i_12_ ^ 0xffffffff); i_12_++) {
										sprite[i_12_] = client.graphicsToolkit.createSprite(class324s[i_12_], true);
									}
									Class275.aClass79_2046.put(i_9_, sprite, (byte) -80);
								}
							}
							if (sprite != null && sprite.length >= 2) {
								class332s = sprite;
							}
						}
						if (class332s.length <= i_10_) {
							i_10_ = 1;
						}
						Sprite class332 = class332s[0];
						Sprite class332_13_ = class332s[i_10_];
						i_8_ -= Math.max(Class42_Sub1.p13FullMetrics.anInt1517, class332.getHeight());
						int i_14_ = i_4_ + Class259.anIntArray1957[0] - (class332.getWidth() >> -1452536927);
						int i_15_ = class332.getWidth() * mob.anInt6429 / 255;
						int i_16_ = class332.getHeight();
						if (mob.anInt6429 > 0 && i_15_ < 2) {
							i_15_ = 2;
						}
						class332.draw(i_14_, i_8_);
						client.graphicsToolkit.constrainClip(i_14_, i_8_, i_14_ + i_15_, i_16_ + i_8_);
						class332_13_.draw(i_14_, i_8_);
						client.graphicsToolkit.setClip(i_4_, i_5_, i_0_ + i_4_, i_5_ - -i_1_);
						AnimatedProgressBarLSEConfig.method908(i_8_ + i_16_, i_8_, false, i_14_, class332.getRenderWidth() + i_14_);
					} else {
						i_8_ -= Math.max(Class42_Sub1.p13FullMetrics.anInt1517, Class254.aClass332Array1943[0].getHeight());
					}
					i_8_ -= 2;
					if (!mob.aBoolean6371) {
						if ((mob.anInt6412 ^ 0xffffffff) < (Queue.timer ^ 0xffffffff)) {
							Sprite class332 = Billboard.aClass332Array1382[!mob.aBoolean6348 ? 0 : 2];
							Sprite class332_17_ = Billboard.aClass332Array1382[!mob.aBoolean6348 ? 1 : 3];
							int i_18_ = -1;
							if (!(mob instanceof NPC)) {
								i_18_ = mob.method3039(1).anInt2374;
							} else {
								i_18_ = definition.anInt1100;
								if (i_18_ == -1) {
									i_18_ = mob.method3039(1).anInt2374;
								}
							}
							if (i_18_ != -1) {
								Sprite[] class332s = (Sprite[]) Class224_Sub3.aClass79_5039.get(-126, i_18_);
								if (class332s == null) {
									Image[] class324s = Image.loadImages(client.spriteJs5, i_18_, 0);
									if (class324s != null) {
										class332s = new Sprite[class324s.length];
										for (int i_19_ = 0; (i_19_ ^ 0xffffffff) > (class324s.length ^ 0xffffffff); i_19_++) {
											class332s[i_19_] = client.graphicsToolkit.createSprite(class324s[i_19_], true);
										}
										Class224_Sub3.aClass79_5039.put(i_18_, class332s, (byte) -80);
									}
								}
								if (class332s != null && class332s.length == 4) {
									class332 = class332s[!mob.aBoolean6348 ? 0 : 2];
									class332_17_ = class332s[!mob.aBoolean6348 ? 1 : 3];
								}
							}
							int i_20_ = -Queue.timer + mob.anInt6412;
							int i_21_;
							if ((mob.anInt6394 ^ 0xffffffff) <= (i_20_ ^ 0xffffffff)) {
								i_21_ = class332.getWidth();
							} else {
								i_20_ -= mob.anInt6394;
								int i_22_ = (mob.anInt6401 ^ 0xffffffff) != -1 ? (-i_20_ + mob.anInt6420) / mob.anInt6401 * mob.anInt6401 : 0;
								i_21_ = class332.getWidth() * i_22_ / mob.anInt6420;
							}
							int i_23_ = class332.getHeight();
							i_8_ -= i_23_;
							int i_24_ = Class259.anIntArray1957[0] + i_4_ + -(class332.getWidth() >> 1704497793);
							class332.draw(i_24_, i_8_);
							client.graphicsToolkit.constrainClip(i_24_, i_8_, i_21_ + i_24_, i_8_ - -i_23_);
							class332_17_.draw(i_24_, i_8_);
							client.graphicsToolkit.setClip(i_4_, i_5_, i_0_ + i_4_, i_5_ + i_1_);
							AnimatedProgressBarLSEConfig.method908(i_23_ + i_8_, i_8_, false, i_24_, class332.getRenderWidth() + i_24_);
							i_8_ -= 2;
						}
						if (index >= playerCount) {
							if ((definition.anInt1113 ^ 0xffffffff) <= -1 && (definition.anInt1113 ^ 0xffffffff) > (Class2.aClass332Array72.length ^ 0xffffffff)) {
								Sprite class332 = Class2.aClass332Array72[definition.anInt1113];
								i_8_ -= 25;
								class332.draw(i_4_ - -Class259.anIntArray1957[0] - (class332.getWidth() >> -1760821695), i_8_);
								AnimatedProgressBarLSEConfig.method908(class332.getRenderHeight() + i_8_, i_8_, false, i_4_ - (-Class259.anIntArray1957[0] + (class332.getWidth() >> 845665)), Class259.anIntArray1957[0] + i_4_ + -(class332.getWidth() >> 1759618049) + class332.getRenderWidth());
								i_8_ -= 2;
							}
						} else {
							Player player = (Player) mob;
							if (player.skullId != -1) {
								i_8_ -= 25;
								Sprite class332 = Class119_Sub4.aClass332Array4739[player.skullId];
								class332.draw(i_4_ + Class259.anIntArray1957[0] - 12, i_8_);
								AnimatedProgressBarLSEConfig.method908(class332.getRenderHeight() + i_8_, i_8_, false, i_4_ - (-Class259.anIntArray1957[0] - -12), Class259.anIntArray1957[0] + i_4_ + -12 + class332.getRenderWidth());
								i_8_ -= 2;
							}
							if ((player.headIconId ^ 0xffffffff) != 0) {
								i_8_ -= 25;
								Sprite class332 = Class2.aClass332Array72[player.headIconId];
								class332.draw(-12 + i_4_ - -Class259.anIntArray1957[0], i_8_);
								AnimatedProgressBarLSEConfig.method908(i_8_ - -class332.getRenderHeight(), i_8_, false, Class259.anIntArray1957[0] + i_4_ - 12, i_4_ + Class259.anIntArray1957[0] + -12 - -class332.getRenderWidth());
								i_8_ -= 2;
							}
						}
					}
					if (mob instanceof Player) {
						if ((index ^ 0xffffffff) <= -1) {
							int i_25_ = 0;
							Class36[] class36s = OpenGlArrayBufferPointer.aClass36Array903;
							for (Class36 class36 : class36s) {
								if (class36 != null && (class36.anInt346 ^ 0xffffffff) == -11 && class36.anInt345 == playerIndices[index]) {
									Sprite sprite = EnumDefinition.aClass332Array2557[class36.anInt341];
									if ((i_25_ ^ 0xffffffff) > (sprite.getHeight() ^ 0xffffffff)) {
										i_25_ = sprite.getHeight();
									}
									sprite.draw(Class259.anIntArray1957[0] + i_4_ + -12, -sprite.getHeight() + i_8_);
									AnimatedProgressBarLSEConfig.method908(-sprite.getHeight() + i_8_ - -sprite.getRenderHeight(), i_8_ - sprite.getHeight(), false, Class259.anIntArray1957[0] + i_4_ + -12, i_4_ - (-Class259.anIntArray1957[0] + 12) - -sprite.getRenderWidth());
								}
							}
							if (i_25_ > 0) {
								i_8_ -= 2 + i_25_;
							}
						}
					} else {
						int i_27_ = 0;
						Class36[] class36s = OpenGlArrayBufferPointer.aClass36Array903;
						for (Class36 class36 : class36s) {
							if (class36 != null && class36.anInt346 == 1 && (class36.anInt345 ^ 0xffffffff) == (Orientation.npcIndices[-playerCount + index] ^ 0xffffffff)) {
								Sprite class332 = EnumDefinition.aClass332Array2557[class36.anInt341];
								if (class332.getHeight() > i_27_) {
									i_27_ = class332.getHeight();
								}
								if ((Queue.timer % 20 ^ 0xffffffff) > -11) {
									class332.draw(-12 + Class259.anIntArray1957[0] + i_4_, i_8_ + -class332.getHeight());
									AnimatedProgressBarLSEConfig.method908(i_8_ - class332.getHeight() - -class332.getRenderHeight(), i_8_ + -class332.getHeight(), false, -12 + Class259.anIntArray1957[0] + i_4_, -12 + Class259.anIntArray1957[0] + i_4_ + class332.getRenderWidth());
								}
							}
						}
						if ((i_27_ ^ 0xffffffff) < -1) {
							i_8_ -= i_27_ - -2;
						}
					}
					int i_29_ = 0;
					for (/**/; i_29_ < LoadingScreenElementFactory.anInt3090; i_29_++) {
						int i_30_ = mob.anIntArray6375[i_29_];
						int i_31_ = mob.anIntArray6430[i_29_];
						HitmarksDefinition class86 = null;
						int i_32_ = 0;
						if ((i_31_ ^ 0xffffffff) <= -1) {
							if ((Queue.timer ^ 0xffffffff) <= (i_30_ ^ 0xffffffff)) {
								continue;
							}
							class86 = Class246_Sub3_Sub1.hitmarksDefinitionList.method2194(i_3_ + 87, mob.anIntArray6430[i_29_]);
							i_32_ = class86.duration;
						} else if (i_30_ < 0) {
							continue;
						}
						int i_33_ = mob.anIntArray6386[i_29_];
						HitmarksDefinition class86_34_ = null;
						if (i_33_ >= 0) {
							class86_34_ = Class246_Sub3_Sub1.hitmarksDefinitionList.method2194(94, i_33_);
						}
						if ((Queue.timer ^ 0xffffffff) <= (i_30_ - i_32_ ^ 0xffffffff)) {
							int i_35_ = mob.anIntArray6397[i_29_];
							if (i_35_ >= 0) {
								mob.anInt6418 = 300 + Queue.timer;
								mob.anInt6429 = i_35_;
								mob.anIntArray6397[i_29_] = -1;
							}
							if (class86 == null) {
								mob.anIntArray6375[i_29_] = -1;
							} else {
								int i_36_ = mob.method3044(false) / 2;
								DataFs.method243(i_36_, i_0_ >> 1162505793, 92, i_2_, mob, i_1_ >> 649783713, i);
								if ((Class259.anIntArray1957[0] ^ 0xffffffff) < 0) {
									Class259.anIntArray1957[0] += ItemSearch.anIntArray457[i_29_];
									Class259.anIntArray1957[1] += JavaThreadResource.anIntArray1764[i_29_];
									int i_40_ = 0;
									int i_41_ = 0;
									int i_42_ = 0;
									int i_43_ = 0;
									int i_44_ = 0;
									int i_45_ = 0;
									int i_46_ = 0;
									int i_47_ = 0;
									Sprite class332 = null;
									Sprite class332_48_ = null;
									Sprite class332_49_ = null;
									Sprite class332_50_ = null;
									int i_51_ = 0;
									int i_52_ = 0;
									int i_53_ = 0;
									int i_54_ = 0;
									int i_55_ = 0;
									int i_56_ = 0;
									int i_57_ = 0;
									int i_58_ = 0;
									Sprite class332_59_ = class86.getIconSprite(false, client.graphicsToolkit);
									int i_60_ = 0;
									if (class332_59_ != null) {
										i_40_ = class332_59_.getWidth();
										int i_61_ = class332_59_.getHeight();
										class332_59_.method3741(Class284_Sub1_Sub1.anIntArray6190);
										if ((i_61_ ^ 0xffffffff) < (i_60_ ^ 0xffffffff)) {
											i_60_ = i_61_;
										}
										i_44_ = Class284_Sub1_Sub1.anIntArray6190[0];
									}
									Sprite class332_62_ = class86.getMiddleSprite(40, client.graphicsToolkit);
									if (class332_62_ != null) {
										i_41_ = class332_62_.getWidth();
										int i_63_ = class332_62_.getHeight();
										class332_62_.method3741(Class284_Sub1_Sub1.anIntArray6190);
										if (i_63_ > i_60_) {
											i_60_ = i_63_;
										}
										i_45_ = Class284_Sub1_Sub1.anIntArray6190[0];
									}
									Sprite class332_64_ = class86.getLeftSprite(client.graphicsToolkit, i_3_ ^ 0x17);
									if (class332_64_ != null) {
										i_42_ = class332_64_.getWidth();
										int i_65_ = class332_64_.getHeight();
										class332_64_.method3741(Class284_Sub1_Sub1.anIntArray6190);
										if (i_60_ < i_65_) {
											i_60_ = i_65_;
										}
										i_46_ = Class284_Sub1_Sub1.anIntArray6190[0];
									}
									Sprite class332_66_ = class86.getRightSprite(-1, client.graphicsToolkit);
									if (class332_66_ != null) {
										i_43_ = class332_66_.getWidth();
										int i_67_ = class332_66_.getHeight();
										if ((i_67_ ^ 0xffffffff) < (i_60_ ^ 0xffffffff)) {
											i_60_ = i_67_;
										}
										class332_66_.method3741(Class284_Sub1_Sub1.anIntArray6190);
										i_47_ = Class284_Sub1_Sub1.anIntArray6190[0];
									}
									if (class86_34_ != null) {
										class332 = class86_34_.getIconSprite(false, client.graphicsToolkit);
										if (class332 != null) {
											i_51_ = class332.getWidth();
											int i_68_ = class332.getHeight();
											if ((i_60_ ^ 0xffffffff) > (i_68_ ^ 0xffffffff)) {
												i_60_ = i_68_;
											}
											class332.method3741(Class284_Sub1_Sub1.anIntArray6190);
											i_55_ = Class284_Sub1_Sub1.anIntArray6190[0];
										}
										class332_48_ = class86_34_.getMiddleSprite(63, client.graphicsToolkit);
										if (class332_48_ != null) {
											i_52_ = class332_48_.getWidth();
											int i_69_ = class332_48_.getHeight();
											class332_48_.method3741(Class284_Sub1_Sub1.anIntArray6190);
											if ((i_60_ ^ 0xffffffff) > (i_69_ ^ 0xffffffff)) {
												i_60_ = i_69_;
											}
											i_56_ = Class284_Sub1_Sub1.anIntArray6190[0];
										}
										class332_49_ = class86_34_.getLeftSprite(client.graphicsToolkit, i_3_ ^ 0x7d);
										if (class332_49_ != null) {
											i_53_ = class332_49_.getWidth();
											int i_70_ = class332_49_.getHeight();
											if (i_70_ > i_60_) {
												i_60_ = i_70_;
											}
											class332_49_.method3741(Class284_Sub1_Sub1.anIntArray6190);
											i_57_ = Class284_Sub1_Sub1.anIntArray6190[0];
										}
										class332_50_ = class86_34_.getRightSprite(i_3_ ^ 0x7b, client.graphicsToolkit);
										if (class332_50_ != null) {
											i_54_ = class332_50_.getWidth();
											int i_71_ = class332_50_.getHeight();
											class332_50_.method3741(Class284_Sub1_Sub1.anIntArray6190);
											if ((i_60_ ^ 0xffffffff) > (i_71_ ^ 0xffffffff)) {
												i_60_ = i_71_;
											}
											i_58_ = Class284_Sub1_Sub1.anIntArray6190[0];
										}
									}
									Font class43 = Class69_Sub2.p11Full;
									Font class43_72_ = Class69_Sub2.p11Full;
									FontSpecifications class197 = StrongReferenceMCNode.p11FullMetrics;
									int i_73_ = class86.font;
									FontSpecifications class197_74_ = StrongReferenceMCNode.p11FullMetrics;
									if (i_73_ >= 0) {
										Font class43_75_ = Class98_Sub1.method945(i_73_, client.graphicsToolkit, (byte) 124, true);
										FontSpecifications class197_76_ = Class98_Sub46_Sub6.loadFontMetrics(i_73_, 18361, client.graphicsToolkit);
										if (class43_75_ != null && class197_76_ != null) {
											class197 = class197_76_;
											class43 = class43_75_;
										}
									}
									if (class86_34_ != null) {
										i_73_ = class86_34_.font;
										if ((i_73_ ^ 0xffffffff) <= -1) {
											Font class43_77_ = Class98_Sub1.method945(i_73_, client.graphicsToolkit, (byte) 124, true);
											FontSpecifications class197_78_ = Class98_Sub46_Sub6.loadFontMetrics(i_73_, 18361, client.graphicsToolkit);
											if (class43_77_ != null && class197_78_ != null) {
												class197_74_ = class197_78_;
												class43_72_ = class43_77_;
											}
										}
									}
									String string = null;
									String string_80_ = class86.formatAmount(22414, mob.anIntArray6425[i_29_]);
									int i_81_ = 0;
									int i_82_ = class197.method2674(string_80_, i_3_ + 242);
									if (class86_34_ != null) {
										string = class86_34_.formatAmount(22414, mob.anIntArray6423[i_29_]);
										i_81_ = class197_74_.method2674(string, 107);
									}
									int i_83_ = 0;
									int i_84_ = 0;
									if (i_41_ > 0) {
										i_83_ = i_82_ / i_41_ + 1;
									}
									if (class86_34_ != null && i_52_ > 0) {
										i_84_ = 1 + i_81_ / i_52_;
									}
									int i_85_ = 0;
									int i_86_ = i_85_;
									if (i_40_ > 0) {
										i_85_ += i_40_;
									}
									i_85_ += 2;
									int i_87_ = i_85_;
									if ((i_42_ ^ 0xffffffff) < -1) {
										i_85_ += i_42_;
									}
									int i_88_ = i_85_;
									int i_89_ = i_85_;
									if ((i_41_ ^ 0xffffffff) < -1) {
										int i_90_ = i_83_ * i_41_;
										i_85_ += i_90_;
										i_89_ += (-i_82_ + i_90_) / 2;
									} else {
										i_85_ += i_82_;
									}
									int i_91_ = i_85_;
									if (i_43_ > 0) {
										i_85_ += i_43_;
									}
									int i_92_ = 0;
									int i_93_ = 0;
									int i_94_ = 0;
									int i_95_ = 0;
									int i_96_ = 0;
									if (class86_34_ != null) {
										i_85_ += 2;
										i_92_ = i_85_;
										if ((i_51_ ^ 0xffffffff) < -1) {
											i_85_ += i_51_;
										}
										i_85_ += 2;
										i_93_ = i_85_;
										if ((i_53_ ^ 0xffffffff) < -1) {
											i_85_ += i_53_;
										}
										i_94_ = i_85_;
										i_96_ = i_85_;
										if (i_52_ > 0) {
											int i_97_ = i_84_ * i_52_;
											i_96_ += (i_97_ - i_81_) / 2;
											i_85_ += i_97_;
										} else {
											i_85_ += i_81_;
										}
										i_95_ = i_85_;
										if ((i_54_ ^ 0xffffffff) < -1) {
											i_85_ += i_54_;
										}
									}
									int i_98_ = -Queue.timer + mob.anIntArray6375[i_29_];
									int i_99_ = -(class86.offsetX * i_98_ / class86.duration) + class86.offsetX;
									int i_100_ = class86.anInt650 * i_98_ / class86.duration + -class86.anInt650;
									int i_101_ = i_99_ + -(i_85_ >> -291615551) + Class259.anIntArray1957[0] + i_4_;
									int i_102_ = i_100_ + -12 + i_5_ + Class259.anIntArray1957[1];
									int i_103_ = i_102_;
									int i_104_ = i_60_ + i_102_;
									int i_105_ = class86.anInt646 + i_102_ - -15;
									int i_106_ = -class197.anInt1517 + i_105_;
									if (i_106_ < i_103_) {
										i_103_ = i_106_;
									}
									int i_107_ = class197.anInt1514 + i_105_;
									if ((i_107_ ^ 0xffffffff) < (i_104_ ^ 0xffffffff)) {
										i_104_ = i_107_;
									}
									int i_108_ = 0;
									if (class86_34_ != null) {
										i_108_ = 15 + i_102_ + class86_34_.anInt646;
										int i_109_ = i_108_ + -class197_74_.anInt1517;
										int i_110_ = class197_74_.anInt1514 + i_108_;
										if (i_109_ < i_103_) {
											i_103_ = i_109_;
										}
										if (i_104_ < i_110_) {
											i_104_ = i_110_;
										}
									}
									int i_111_ = 255;
									if ((class86.fade ^ 0xffffffff) <= -1) {
										i_111_ = (i_98_ << -847692632) / (class86.duration - class86.fade);
									}
									if ((i_111_ ^ 0xffffffff) <= -1 && (i_111_ ^ 0xffffffff) > -256) {
										int i_112_ = i_111_ << 877653624;
										int i_113_ = 0xffffff | i_112_;
										if (class332_59_ != null) {
											class332_59_.draw(-i_44_ + i_86_ + i_101_, i_102_, 0, i_113_, 1);
										}
										if (class332_64_ != null) {
											class332_64_.draw(-i_46_ + i_87_ + i_101_, i_102_, 0, i_113_, 1);
										}
										if (class332_62_ != null) {
											for (int i_114_ = 0; i_83_ > i_114_; i_114_++) {
												class332_62_.draw(i_101_ - -i_88_ - (i_45_ - i_114_ * i_41_), i_102_, 0, i_113_, 1);
											}
										}
										if (class332_66_ != null) {
											class332_66_.draw(-i_47_ + i_101_ - -i_91_, i_102_, 0, i_113_, 1);
										}
										class43.drawString((byte) -102, i_105_, string_80_, class86.textColour | i_112_, 0, i_89_ + i_101_);
										if (class86_34_ != null) {
											if (class332 != null) {
												class332.draw(i_101_ + i_92_ - i_55_, i_102_, 0, i_113_, 1);
											}
											if (class332_49_ != null) {
												class332_49_.draw(i_101_ - (-i_93_ - -i_57_), i_102_, 0, i_113_, 1);
											}
											if (class332_48_ != null) {
												for (int i_115_ = 0; i_115_ < i_84_; i_115_++) {
													class332_48_.draw(i_52_ * i_115_ + -i_56_ + i_101_ + i_94_, i_102_, 0, i_113_, 1);
												}
											}
											if (class332_50_ != null) {
												class332_50_.draw(i_95_ + i_101_ + -i_58_, i_102_, 0, i_113_, 1);
											}
											class43_72_.drawString((byte) -105, i_108_, string, i_112_ | class86_34_.textColour, 0, i_96_ + i_101_);
										}
									} else {
										if (class332_59_ != null) {
											class332_59_.draw(i_101_ - -i_86_ - i_44_, i_102_);
										}
										if (class332_64_ != null) {
											class332_64_.draw(-i_46_ + i_101_ + i_87_, i_102_);
										}
										if (class332_62_ != null) {
											for (int i_116_ = 0; i_83_ > i_116_; i_116_++) {
												class332_62_.draw(i_101_ - -i_88_ + -i_45_ - -(i_41_ * i_116_), i_102_);
											}
										}
										if (class332_66_ != null) {
											class332_66_.draw(-i_47_ + i_101_ + i_91_, i_102_);
										}
										class43.drawString((byte) 79, i_105_, string_80_, class86.textColour | ~0xffffff, 0, i_101_ - -i_89_);
										if (class86_34_ != null) {
											if (class332 != null) {
												class332.draw(i_101_ - (-i_92_ - -i_55_), i_102_);
											}
											if (class332_49_ != null) {
												class332_49_.draw(-i_57_ + i_93_ + i_101_, i_102_);
											}
											if (class332_48_ != null) {
												for (int i_117_ = 0; i_117_ < i_84_; i_117_++) {
													class332_48_.draw(i_101_ + i_94_ + -i_56_ - -(i_117_ * i_52_), i_102_);
												}
											}
											if (class332_50_ != null) {
												class332_50_.draw(-i_58_ + i_101_ + i_95_, i_102_);
											}
											class43_72_.drawString((byte) 81, i_108_, string, ~0xffffff | class86_34_.textColour, 0, i_101_ - -i_96_);
										}
									}
									AnimatedProgressBarLSEConfig.method908(1 + i_104_, i_103_, false, i_101_, i_85_ + i_101_);
								}
							}
						}
					}
				}
			}
		}
		for (int i_118_ = 0; (i_118_ ^ 0xffffffff) > (ReflectionRequest.anInt3955 ^ 0xffffffff); i_118_++) {
			int i_119_ = Class151_Sub1.anIntArray4969[i_118_];
			Mob mob;
			if (i_119_ < 2048) {
				mob = Class151_Sub9.players[i_119_];
			} else {
				mob = ((NodeObject) ProceduralTextureSource.npc.get(-2048 + i_119_, -1)).npc;
			}
			int i_120_ = Class119_Sub2.anIntArray4727[i_118_];
			Mob mob_;
			if (i_120_ >= 2048) {
				mob_ = ((NodeObject) ProceduralTextureSource.npc.get(-2048 + i_120_, i_3_ + 123)).npc;
			} else {
				mob_ = Class151_Sub9.players[i_120_];
			}
			VertexNormal.method3381(i, i_4_, i_2_, 123, mob, i_5_, mob_, --mob.anInt6358, i_0_, i_1_);
		}
		int i_122_ = 2 + Class42_Sub1.p13FullMetrics.anInt1514 + Class42_Sub1.p13FullMetrics.anInt1517;
		if (i_3_ == -124) {
			for (int i_123_ = 0; (Class48_Sub2_Sub1.anInt5532 ^ 0xffffffff) < (i_123_ ^ 0xffffffff); i_123_++) {
				int i_124_ = Class230.anIntArray1724[i_123_];
				int i_125_ = Class230.anIntArray1730[i_123_];
				int i_126_ = Class230.anIntArray1729[i_123_];
				boolean bool = true;
				while (bool) {
					bool = false;
					for (int i_127_ = 0; i_123_ > i_127_; i_127_++) {
						if (-i_122_ + Class230.anIntArray1730[i_127_] < i_125_ + 2 && (2 + Class230.anIntArray1730[i_127_] ^ 0xffffffff) < (-i_122_ + i_125_ ^ 0xffffffff) && -i_126_ + i_124_ < Class230.anIntArray1729[i_127_] + Class230.anIntArray1724[i_127_] && (-Class230.anIntArray1729[i_127_]
								+ Class230.anIntArray1724[i_127_] ^ 0xffffffff) > (i_124_ - -i_126_ ^ 0xffffffff) && (i_125_ ^ 0xffffffff) < (-i_122_ + Class230.anIntArray1730[i_127_] ^ 0xffffffff)) {
							i_125_ = Class230.anIntArray1730[i_127_] + -i_122_;
							bool = true;
						}
					}
				}
				Class230.anIntArray1730[i_123_] = i_125_;
				String string = Class230.aStringArray1721[i_123_];
				int i_128_ = Class42_Sub1.p13FullMetrics.method2674(string, 104);
				int i_129_ = i_4_ - -i_124_;
				int i_130_ = -Class42_Sub1.p13FullMetrics.anInt1517 + i_125_ + i_5_;
				int i_131_ = i_128_ + i_129_;
				int i_132_ = i_125_ + i_5_ + Class42_Sub1.p13FullMetrics.anInt1514;
				if (PacketBufferManager.anInt1026 == 0) {
					int i_133_ = 16776960;
					if (Class230.anIntArray1726[i_123_] < 6) {
						i_133_ = DummyOutputStream.anIntArray38[Class230.anIntArray1726[i_123_]];
					}
					if ((Class230.anIntArray1726[i_123_] ^ 0xffffffff) == -7) {
						i_133_ = (RemoveRoofsPreferenceField.anInt3676 % 20 ^ 0xffffffff) > -11 ? 16711680 : 16776960;
					}
					if ((Class230.anIntArray1726[i_123_] ^ 0xffffffff) == -8) {
						i_133_ = RemoveRoofsPreferenceField.anInt3676 % 20 >= 10 ? 65535 : 255;
					}
					if ((Class230.anIntArray1726[i_123_] ^ 0xffffffff) == -9) {
						i_133_ = RemoveRoofsPreferenceField.anInt3676 % 20 < 10 ? 45056 : 8454016;
					}
					if ((Class230.anIntArray1726[i_123_] ^ 0xffffffff) == -10) {
						int i_134_ = -Class230.anIntArray1722[i_123_] + 150;
						if ((i_134_ ^ 0xffffffff) <= -51) {
							if (i_134_ < 100) {
								i_133_ = 16776960 + -(i_134_ * 327680) + 16384000;
							} else if (i_134_ < 150) {
								i_133_ = (i_134_ - 100) * 5 + 65280;
							}
						} else {
							i_133_ = i_134_ * 1280 + 16711680;
						}
					}
					if ((Class230.anIntArray1726[i_123_] ^ 0xffffffff) == -11) {
						int i_135_ = 150 + -Class230.anIntArray1722[i_123_];
						if (i_135_ < 50) {
							i_133_ = i_135_ * 5 + 16711680;
						} else if ((i_135_ ^ 0xffffffff) <= -101) {
							if (i_135_ < 150) {
								i_133_ = 255 - -(327680 * i_135_) - (32768000 - -((i_135_ + -100) * 5));
							}
						} else {
							i_133_ = 16384000 - 327680 * i_135_ + 16711935;
						}
					}
					if ((Class230.anIntArray1726[i_123_] ^ 0xffffffff) == -12) {
						int i_136_ = 150 - Class230.anIntArray1722[i_123_];
						if ((i_136_ ^ 0xffffffff) <= -51) {
							if ((i_136_ ^ 0xffffffff) <= -101) {
								if (i_136_ < 150) {
									i_133_ = -(327680 * (-100 + i_136_)) + 16777215;
								}
							} else {
								i_133_ = -16384250 + 327685 * i_136_ + 65280;
							}
						} else {
							i_133_ = -(327685 * i_136_) + 16777215;
						}
					}
					int i_137_ = ~0xffffff | i_133_;
					if ((Class230.anIntArray1728[i_123_] ^ 0xffffffff) == -1) {
						i_131_ -= i_128_ >> 315503681;
						Class98_Sub10_Sub34.p13Full.drawStringCenterAligned(i_137_, string, i_4_ + i_124_, -16777216, (byte) 60, i_5_ + i_125_);
						i_129_ -= i_128_ >> 887140673;
					}
					if ((Class230.anIntArray1728[i_123_] ^ 0xffffffff) == -2) {
						i_130_ -= 5;
						i_129_ -= i_128_ >> -1595838975;
						Class98_Sub10_Sub34.p13Full.method403(-16777216, i_124_ + i_4_, i_5_ + i_125_, RemoveRoofsPreferenceField.anInt3676, 62, string, i_137_);
						i_132_ += 5;
						i_131_ -= i_128_ >> -50142815;
					}
					if ((Class230.anIntArray1728[i_123_] ^ 0xffffffff) == -3) {
						i_131_ -= (i_128_ >> 90209633) + -5;
						i_132_ += 5;
						i_130_ -= 5;
						Class98_Sub10_Sub34.p13Full.method412(-16777216, -127, i_137_, i_124_ + i_4_, i_125_ + i_5_, string, RemoveRoofsPreferenceField.anInt3676);
						i_129_ -= 5 + (i_128_ >> -574220287);
					}
					if ((Class230.anIntArray1728[i_123_] ^ 0xffffffff) == -4) {
						i_131_ -= i_128_ >> 988959905;
						i_132_ += 7;
						Class98_Sub10_Sub34.p13Full.method406(0, -16777216, i_5_ - -i_125_, i_137_, -Class230.anIntArray1722[i_123_] + 150, RemoveRoofsPreferenceField.anInt3676, i_124_ + i_4_, string);
						i_130_ -= 7;
						i_129_ -= i_128_ >> -798403551;
					}
					if (Class230.anIntArray1728[i_123_] == 4) {
						int i_138_ = (-Class230.anIntArray1722[i_123_] + 150) * (100 + Class42_Sub1.p13FullMetrics.method2674(string, 112)) / 150;
						client.graphicsToolkit.constrainClip(-50 + i_124_ + i_4_, i_5_, 50 + i_4_ + i_124_, i_5_ - -i_1_);
						i_131_ += 50 + -i_138_;
						i_129_ += -i_138_ + 50;
						Class98_Sub10_Sub34.p13Full.drawString((byte) 66, i_125_ + i_5_, string, i_137_, -16777216, -i_138_ + i_124_ + i_4_ - -50);
						client.graphicsToolkit.setClip(i_4_, i_5_, i_4_ + i_0_, i_1_ + i_5_);
					}
					if (Class230.anIntArray1728[i_123_] == 5) {
						int i_139_ = 150 + -Class230.anIntArray1722[i_123_];
						int i_140_ = 0;
						if (i_139_ < 25) {
							i_140_ = i_139_ + -25;
						} else if ((i_139_ ^ 0xffffffff) < -126) {
							i_140_ = -125 + i_139_;
						}
						int i_141_ = Class42_Sub1.p13FullMetrics.anInt1514 + Class42_Sub1.p13FullMetrics.anInt1517;
						client.graphicsToolkit.constrainClip(i_4_, -i_141_ + i_125_ + i_5_ - 1, i_0_ + i_4_, i_5_ - (-i_125_ + -5));
						i_130_ += i_140_;
						i_132_ += i_140_;
						i_131_ -= i_128_ >> -305213023;
						Class98_Sub10_Sub34.p13Full.drawStringCenterAligned(i_137_, string, i_4_ - -i_124_, -16777216, (byte) 124, i_140_ + i_5_ - -i_125_);
						i_129_ -= i_128_ >> 1565844833;
						client.graphicsToolkit.setClip(i_4_, i_5_, i_0_ + i_4_, i_1_ + i_5_);
					}
				} else {
					Class98_Sub10_Sub34.p13Full.drawStringCenterAligned(-256, string, i_124_ + i_4_, -16777216, (byte) -87, i_125_ + i_5_);
					i_131_ -= i_128_ >> 1383577281;
					i_129_ -= i_128_ >> -37573471;
				}
				AnimatedProgressBarLSEConfig.method908(1 + i_132_, i_130_, false, i_129_, i_131_ + 1);
			}
		}
	}

	public int value;

	public NodeInteger() {
		/* empty */
	}

	public NodeInteger(int value) {
		this.value = value;
	}
}

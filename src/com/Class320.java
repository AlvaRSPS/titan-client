/* Class320 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.Class64_Sub5;

public final class Class320 {
	public static IncomingOpcode aClass58_2708 = new IncomingOpcode(23, 0);

	public static final void method3663(ActionGroup class98_sub46_sub9, int i, int i_0_, int i_1_, int i_2_, int i_3_, RSToolkit var_ha, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		if ((i_3_ ^ 0xffffffff) > (i_0_ ^ 0xffffffff) && i_0_ < i_3_ + i_7_ && i_2_ > i_4_ - 13 && i_2_ < 3 + i_4_) {
			i = i_5_;
		}
		String string = Class21.method262(class98_sub46_sub9, (byte) 36);
		Class98_Sub10_Sub34.p13Full.method413(i_4_, Class64_Sub5.anIntArray3652, i_6_, string, i, 3 + i_3_, (byte) 18, GroupProgressMonitor.aClass332Array3408);
	}

}

/* Class224_Sub3_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class224_Sub3_Sub1 extends Class224_Sub3 {
	public static boolean			aBoolean6144			= false;
	public static Class298			aClass298_6145;
	public static RSByteBuffer[]	aClass98_Sub22Array6146	= new RSByteBuffer[2048];
	public static int				cameraYPosition;

	static {
		aClass298_6145 = new Class298();
	}

	public static void method2841(byte i) {
		try {
			if (i > -2) {
				method2841((byte) -40);
			}
			aClass98_Sub22Array6146 = null;
			aClass298_6145 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nv.A(" + i + ')');
		}
	}
}

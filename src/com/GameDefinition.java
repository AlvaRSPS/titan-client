
/* Class279 - Decompiled by JODE
 */ package com; /*
					*/

import java.util.Random;
import java.util.zip.CRC32;

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.constants.BuildLocation;

public final class GameDefinition {
	public static CRC32				aCRC32_2097		= new CRC32();
	public static double			aDouble2100;
	public static int				anInt2096		= 0;
	public static int				anInt2099		= 0;
	public static GameDefinition	GAME3			= new GameDefinition("game3", 2);
	public static GameDefinition	GAME4			= new GameDefinition("game4", 3);
	public static GameDefinition	RUNESCAPE		= new GameDefinition("titan", 0);
	public static GameDefinition	STELLAR_DAWN	= new GameDefinition("stellardawn", 1);

	public static final String method1153(byte i) {
		String string = "www";
		if (client.buildLocation == BuildLocation.WTRC) {
			string = "www-wtrc";
		} else if (BuildLocation.WTQA == client.buildLocation) {
			string = "www-wtqa";
		} else if (client.buildLocation == BuildLocation.WTWIP) {
			string = "www-wtwip";
		}
		String string_0_ = "";
		if (client.settingsString != null) {
			string_0_ = "/p=" + client.settingsString;
		}
		return "http://" + string + "." + client.game.code + ".com/l=" + client.gameLanguage + "/a=" + client.affiliateId + string_0_ + "/";
	}

	public static void method3321(boolean bool) {
		try {
			if (bool != true) {
				method3321(true);
			}
			aCRC32_2097 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rga.A(" + bool + ')');
		}
	}

	public static final void method3322(Node class98, Node class98_0_, byte i) {
		try {
			if (class98_0_.previous != null) {
				class98_0_.unlink(i ^ 0x71);
			}
			if (i != 24) {
				anInt2099 = 4;
			}
			class98_0_.previous = class98.previous;
			class98_0_.next = class98;
			class98_0_.previous.next = class98_0_;
			class98_0_.next.previous = class98_0_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rga.B(" + (class98 != null ? "{...}" : "null") + ',' + (class98_0_ != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static final byte[] method3323(int i, int i_1_) {
		try {
			Class98_Sub46_Sub7 class98_sub46_sub7 = (Class98_Sub46_Sub7) Class81.aClass100_617.method1694((byte) 117, i);
			if (class98_sub46_sub7 == null) {
				byte[] is = new byte[512];
				Random random = new Random(i);
				for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > -256; i_2_++) {
					is[i_2_] = (byte) i_2_;
				}
				for (int i_3_ = 0; i_3_ < 255; i_3_++) {
					int i_4_ = -i_3_ + 255;
					int i_5_ = HorizontalAlignment.randomNumber(-28737, i_4_, random);
					byte i_6_ = is[i_5_];
					is[i_5_] = is[i_4_];
					is[i_4_] = is[-i_3_ + 511] = i_6_;
				}
				class98_sub46_sub7 = new Class98_Sub46_Sub7(is);
				Class81.aClass100_617.method1695(26404, class98_sub46_sub7, i);
			}
			if (i_1_ != 512) {
				method3322(null, null, (byte) 75);
			}
			return class98_sub46_sub7.aByteArray5981;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rga.C(" + i + ',' + i_1_ + ')');
		}
	}

	String		code;

	public int	id;

	GameDefinition(String string, int i) {
		code = string;
		id = i;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}

}

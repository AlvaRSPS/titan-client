
/* Class98_Sub20 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.definition.parser.StructsDefinitionParser;
import com.jagex.game.input.impl.AwtMouseEvent;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.model.NativeModelRenderer;

import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeapBuffer;
import jaggl.OpenGL;

public final class OpenGlTileMaterial extends Node {
	public static Class53_Sub1[] aClass53_Sub1Array3967;

	public static final void method1171(int i) {
		Class246_Sub3_Sub3.aClass254_6152 = new Class254(8);
		Class340.anInt2849 = i;
		for (Class246_Sub5 class246_sub5 = (Class246_Sub5) ParticleManager.systems.getFirst((byte) 15); class246_sub5 != null; class246_sub5 = (Class246_Sub5) ParticleManager.systems.getNext(false)) {
			class246_sub5.method3129();
		}
	}

	public static final boolean method1172(byte i, int i_28_, int i_29_) {
		return !(!((i_28_ & 0x60000) != 0 | Class98_Sub10_Sub9.method1033(i_28_, i_29_, 16)) && !Class276.method3286((byte) 115, i_28_, i_29_));
	}

	public static final void method1173(int i) {
		if (NativeModelRenderer.method2408((byte) 57)) {
			if (Class98_Sub46_Sub20.aStringArray6073 == null) {
				StructsDefinitionParser.method3222((byte) -43);
			}
			AwtMouseEvent.consoleOpen = true;
			Class261.fadeInCounter = 0;
		}
	}

	public OpenGlArrayBufferPointer			colourPointer;
	public NativeOpenGlElementArrayBuffer	indices;
	public float							scale;
	public OpenGlToolkit					toolkit;
	public NativeHeapBuffer					colourBuffer;
	public int								texture;
	public int								anInt3971;
	public int								anInt3974;

	public int								anInt3975;

	public int[]							faceFlags;

	public OpenGlGround						ground;

	public Stream							stream;

	public OpenGlTileMaterial(OpenGlGround _ground, int i, int _scale, int i_35_, int i_36_, int i_37_) {
		ground = _ground;
		toolkit = ground.toolkit;
		anInt3974 = i_36_;
		faceFlags = new int[ground.width * ground.length];
		texture = i;
		anInt3975 = i_37_;
		anInt3971 = i_35_;
		scale = _scale;
		indices = new NativeOpenGlElementArrayBuffer(toolkit, 5123, null, 1);
	}

	public final void activate(int index, boolean dummy) {
		stream.setBackingOffset(3 + index * 4);
		stream.writeByte(-1);
	}

	public final void colour(int i, byte dummy, float intensity, int colour, int i_2_) {
		if (dummy > 64) {
			if ((texture ^ 0xffffffff) != 0) {
				TextureMetrics metrics = toolkit.metricsList.getInfo(texture, -28755);
				int alpha = 0xff & metrics.aByte1830;
				if ((alpha ^ 0xffffffff) != -1 && metrics.aByte1820 != 4) {
					int opaque;
					if (i >= 0) {
						if (i > 127) {
							opaque = 16777215;
						} else {
							opaque = 131586 * i;
						}
					} else {
						opaque = 0;
					}
					if ((alpha ^ 0xffffffff) == -257) {
						colour = opaque;
					} else {
						int i_5_ = alpha;
						int inverse = 256 - alpha;
						colour = (0xff0000 & inverse * (colour & 0xff00) + (opaque & 0xff00) * i_5_) + ((0xff00ff & colour) * inverse + i_5_ * (0xff00ff & opaque) & ~0xff00ff) >> -191668280;
					}
				}
				int offset = 0xff & metrics.aByte1829;
				if ((offset ^ 0xffffffff) != -1) {
					offset += 256;
					int red = (colour >> -1266724752 & 0xff) * offset;
					if ((red ^ 0xffffffff) < -65536) {
						red = 65535;
					}
					int green = offset * ((colour & 0xff00) >> 1126487880);
					if ((green ^ 0xffffffff) < -65536) {
						green = 65535;
					}
					int blue = offset * (colour & 0xff);
					if (blue > 65535) {
						blue = 65535;
					}
					colour = (blue >> 2022380040) + (green & 0xff00) + (0xff00ca & red << -1067840792);
				}
			}
			stream.setBackingOffset(4 * i_2_);
			if (intensity != 1.0F) {
				int red = colour >> -523059280 & 0xff;
				int green = (colour & 0xff54) >> -1129896280;
				red *= intensity;
				int blue = 0xff & colour;
				green *= intensity;
				if (red < 0) {
					red = 0;
				} else if (red > 255) {
					red = 255;
				}
				blue *= intensity;
				if ((green ^ 0xffffffff) <= -1) {
					if ((green ^ 0xffffffff) < -256) {
						green = 255;
					}
				} else {
					green = 0;
				}
				if ((blue ^ 0xffffffff) <= -1) {
					if (blue > 255) {
						blue = 255;
					}
				} else {
					blue = 0;
				}
				colour = green << 1129793576 | red << 1969555312 | blue;
			}
			stream.writeByte((byte) (colour >> 367830512));
			stream.writeByte((byte) (colour >> 1207926120));
			stream.writeByte((byte) colour);
		}
	}

	public final void render(int[] tiles, byte dummy, int length) {
		do {
			if (dummy == 98) {
				int count = 0;
				RsFloatBuffer buffer = toolkit.dataBuffer;
				buffer.position = 0;
				if (!toolkit.bigEndian) {
					for (int index = 0; length > index; index++) {
						int tile = tiles[index];
						int flags = faceFlags[tile];
						short[] materials = ground.materialIndices[tile];
						if (flags != 0 && materials != null) {
							int face = 0;
							int vertex = 0;
							while ((vertex ^ 0xffffffff) > (materials.length ^ 0xffffffff)) {
								if ((1 << face++ & flags ^ 0xffffffff) != -1) {
									buffer.writeLEShortCorrectOne(materials[vertex++] & 0xffff, 4);
									count++;
									count++;
									buffer.writeLEShortCorrectOne(materials[vertex++] & 0xffff, 4);
									buffer.writeLEShortCorrectOne(materials[vertex++] & 0xffff, 4);
									count++;
								} else {
									vertex += 3;
								}
							}
						}
					}
				} else {
					for (int index = 0; (length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
						int tile = tiles[index];
						short[] materials = ground.materialIndices[tile];
						int flags = faceFlags[tile];
						if ((flags ^ 0xffffffff) != -1 && materials != null) {
							int face = 0;
							int vertex = 0;
							while (materials.length > vertex) {
								if ((1 << face++ & flags ^ 0xffffffff) != -1) {
									count++;
									buffer.writeShort(0xffff & materials[vertex++], dummy + 1571862790);
									count++;
									buffer.writeShort(materials[vertex++] & 0xffff, dummy + 1571862790);
									count++;
									buffer.writeShort(0xffff & materials[vertex++], 1571862888);
								} else {
									vertex += 3;
								}
							}
						}
					}
				}
				if (count <= 0) {
					break;
				}
				indices.write((byte) -47, buffer.payload, buffer.position, 5123);
				toolkit.setPointers(colourPointer, ground.normalPointer, ground.texturePointer, ground.vertexPointer, dummy + -98);
				toolkit.method1897(texture, (0x8 & ground.flags ^ 0xffffffff) != -1, (ground.flags & 0x7 ^ 0xffffffff) != -1, (byte) -70);
				if (toolkit.underwater) {
					toolkit.EA(2147483647, anInt3971, anInt3974, anInt3975);
				}
				OpenGL.glMatrixMode(5890);
				OpenGL.glPushMatrix();
				OpenGL.glScalef(1.0F / scale, 1.0F / scale, 1.0F);
				OpenGL.glMatrixMode(5888);
				toolkit.setPointers(colourPointer, ground.normalPointer, ground.texturePointer, ground.vertexPointer, 0);
				toolkit.drawElements(count, 4, indices, false, 0);
				OpenGL.glMatrixMode(5890);
				OpenGL.glPopMatrix();
				OpenGL.glMatrixMode(5888);
			}
			break;
		} while (false);
	}

	public final void finish(boolean bool, int size) {
		stream.flush();
		ArrayBuffer interface16 = toolkit.createArrayBuffer(4, (byte) 78, colourBuffer, false, size * 4);
		colourPointer = new OpenGlArrayBufferPointer(interface16, 5121, 4, 0);
		colourBuffer = null;
		stream = null;
	}

	public final void flag(int flag, byte i_30_, int z, int x) {
		faceFlags[x + ((Ground) ground).width * z] = Class41.or(faceFlags[x + ground.width * z], 1 << flag);

	}

	public final void init(int size, byte i_33_) {
		colourBuffer = toolkit.heap.allocate(4 * size, true);
		stream = new Stream(colourBuffer);

	}
}

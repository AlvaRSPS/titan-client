/* Class284_Sub1_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.toolkit.model.NativeModelRenderer;

public final class Class284_Sub1_Sub1 extends Class284_Sub1 {
	public static Class200	aClass200_6187	= new Class200();
	public static int[]		anIntArray6190	= new int[4];

	public static void method3365(byte i) {
		anIntArray6190 = null;
		if (i == 89) {
			aClass200_6187 = null;
		}
	}

	public static long method3367(long l, long l_2_) {
		return l ^ l_2_;
	}

	public static final int method3368(int i, String string, RSByteBuffer packet) {
		int position = packet.position;
		byte[] is = RtInterfaceClip.method152(0, string);
		packet.method1237(is.length, -120);
		packet.position += NativeModelRenderer.aClass213_4949.method2780(is.length, packet.payload, 0, packet.position, is, 6350);
		return -position + packet.position;
	}

	private byte[] aByteArray6188;

	public Class284_Sub1_Sub1() {
		super(12, 5, 16, 2, 2, 0.45F);
	}

	@Override
	public final void method3363(byte i, int i_3_, byte i_4_) {
		if (i_4_ == 42) {
			int i_5_ = 2 * i_3_;
			i = (byte) (127 + ((i & 0xff) >> 768033697));
			aByteArray6188[i_5_++] = i;
			aByteArray6188[i_5_] = i;
		}
	}

	public final byte[] method3366(boolean bool, int i, int i_0_, int i_1_) {
		if (bool != true) {
			anIntArray6190 = null;
		}
		aByteArray6188 = new byte[2 * i_0_ * i_1_ * i];
		method3361((byte) -37, i_1_, i, i_0_);
		return aByteArray6188;
	}
}

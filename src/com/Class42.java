
/* Class42 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;
import com.jagex.game.toolkit.ground.OpenGlGround;

import jaggl.OpenGL;

public abstract class Class42 implements Interface3 {
	public static final boolean	aplSb		= true;
	public static short			aShort3231	= 1;

	public static final QuickChat decode(byte i, RSByteBuffer class98_sub22) {
		try {
			if (i != -12) {
				aShort3231 = (short) -15;
			}
			QuickChat class300 = new QuickChat();
			class300.messageId = class98_sub22.readShort((byte) 127);
			class300.message = NewsLSEConfig.quickChatMessageList.get(class300.messageId, 96);
			return class300;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.L(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	public static final void readRegionGroundData(Class305_Sub1 class305_sub1, int i, byte[][] groundData) {
		try {
			int[] is_4_ = { -1, 0, 0, 0, 0 };
			int chunkCount = groundData.length;
			for (int idx = i; chunkCount > idx; idx++) {
				byte[] chunkData = groundData[idx];
				if (chunkData != null) {
					RSByteBuffer groundBufferData = new RSByteBuffer(chunkData);
					int chunkX = HitmarksDefinitionParser.regionPositionHash[idx] >> -908871672;
					int chunkY = 0xff & HitmarksDefinitionParser.regionPositionHash[idx];
					int i_10_ = 64 * chunkX - Class272.gameSceneBaseX;
					int i_11_ = 64 * chunkY + -aa_Sub2.gameSceneBaseY;
					Class128.method2224(i ^ 0x58a8);
					class305_sub1.method3574((byte) 117, i_11_, aa_Sub2.gameSceneBaseY, Class272.gameSceneBaseX, groundBufferData, i_10_, VarPlayerDefinition.clipMaps);
					class305_sub1.method3582(i + 17685, i_11_, i_10_, client.graphicsToolkit, groundBufferData, is_4_);
					if (!class305_sub1.underwater && (chunkX ^ 0xffffffff) == (Class160.centreX / 8 ^ 0xffffffff) && chunkY == Class275.centreY / 8 && is_4_[0] != -1) {
						OpenGlGround.aClass346_5202 = SimpleProgressBarLoadingScreenElement.skyboxDefinitionList.method528(0, is_4_[0], is_4_[2], is_4_[1], GrandExchangeOffer.sunDefinitionList, is_4_[3]);
						NativeOpenGlElementArrayBuffer.anInt3278 = is_4_[4];
					}
				}
			}
			for (int i_12_ = 0; (i_12_ ^ 0xffffffff) > (chunkCount ^ 0xffffffff); i_12_++) {
				int i_13_ = (HitmarksDefinitionParser.regionPositionHash[i_12_] >> -1224911608) * 64 - Class272.gameSceneBaseX;
				int i_14_ = -aa_Sub2.gameSceneBaseY + (HitmarksDefinitionParser.regionPositionHash[i_12_] & 0xff) * 64;
				byte[] is_15_ = groundData[i_12_];
				if (is_15_ == null && Class275.centreY < 800) {
					Class128.method2224(22696);
					class305_sub1.method3569(64, true, i_14_, 64, i_13_);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.O(" + (class305_sub1 != null ? "{...}" : "null") + ',' + i + ',' + (groundData != null ? "{...}" : "null") + ')');
		}
	}

	private boolean	aBoolean3225;
	private boolean	aBoolean3228	= false;
	OpenGlToolkit	aHa_Sub1_3227;
	private int		anInt3224;
	int				anInt3226;

	int				anInt3229;

	int				anInt3230;

	Class42(OpenGlToolkit var_ha_Sub1, int i, int i_16_, int i_17_, boolean bool) {
		try {
			aHa_Sub1_3227 = var_ha_Sub1;
			anInt3226 = i;
			aBoolean3225 = bool;
			anInt3230 = i_16_;
			anInt3224 = i_17_;
			OpenGL.glGenTextures(1, Class165.anIntArray1277, 0);
			anInt3229 = Class165.anIntArray1277[0];
			method377(122, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_16_ + ',' + i_17_ + ',' + bool + ')');
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			method375(true);
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.finalize(" + ')');
		}
	}

	@Override
	public abstract void method3(byte i);

	private final void method368(int i) {
		try {
			aHa_Sub1_3227.setActiveTexture(1, this);
			do {
				if (aBoolean3228) {
					OpenGL.glTexParameteri(anInt3226, 10241, !aBoolean3225 ? 9729 : 9987);
					OpenGL.glTexParameteri(anInt3226, 10240, 9729);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				OpenGL.glTexParameteri(anInt3226, 10241, !aBoolean3225 ? 9728 : 9984);
				OpenGL.glTexParameteri(anInt3226, 10240, 9728);
			} while (false);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.Q(" + i + ')');
		}
	}

	public final int method369(boolean bool) {
		try {
			if (bool != true) {
				method377(57, 44);
			}
			return anInt3229;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.T(" + bool + ')');
		}
	}

	private final int method370(int i) {
		try {
			int i_1_ = aHa_Sub1_3227.method1866(i + 6368, anInt3230) * anInt3224;
			if (i != -6462) {
				decode((byte) 56, null);
			}
			if (!aBoolean3225) {
				return i_1_;
			}
			return i_1_ * 4 / 3;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.P(" + i + ')');
		}
	}

	public final boolean method371(int i) {
		try {
			if (i <= 31) {
				method370(108);
			}
			if (aHa_Sub1_3227.haveExtFrameBufferObject) {
				int i_2_ = method370(-6462);
				aHa_Sub1_3227.setActiveTexture(1, this);
				OpenGL.glGenerateMipmapEXT(anInt3226);
				aBoolean3225 = true;
				method368(-114);
				method377(113, i_2_);
				return true;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.S(" + i + ')');
		}
	}

	public final void method372(int i, boolean bool) {
		do {
			try {
				if (!bool == aBoolean3228) {
					aBoolean3228 = bool;
					method368(102);
				}
				if (i == -28003) {
					break;
				}
				aShort3231 = (short) 21;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cu.U(" + i + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	public final void method373(boolean bool, boolean bool_3_) {
		try {
			if (!aBoolean3225 == bool_3_) {
				int i = method370(-6462);
				aBoolean3225 = true;
				method368(-38);
				method377(100, i);
			}
			if (bool != true) {
				anInt3226 = 51;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.N(" + bool + ',' + bool_3_ + ')');
		}
	}

	public final void method375(boolean bool) {
		try {
			if (bool == true) {
				if (anInt3229 > 0) {
					aHa_Sub1_3227.method1873(method370(-6462), 4, anInt3229);
					anInt3229 = 0;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.M(" + bool + ')');
		}
	}

	private final void method377(int i, int i_18_) {
		try {
			aHa_Sub1_3227.anInt4337 -= i_18_;
			if (i <= 88) {
				anInt3226 = 38;
			}
			aHa_Sub1_3227.anInt4337 += method370(-6462);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cu.R(" + i + ',' + i_18_ + ')');
		}
	}
}


/* Class31 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;
import java.net.Socket;

import com.jagex.game.client.archive.Js5Exception;

public abstract class SocketWrapper {
	public static int			anInt300	= -2;
	public static RenderTarget	anInterface17_301;

	public static final SocketWrapper create(String string, int i, int i_1_) {
		try {
			SocketWrapper result;
			try {
				result = (SocketWrapper) Class.forName("com.ProxySocket").newInstance();
			} catch (Throwable throwable) {
				result = new DirectSocket();
			}
			result.port = i_1_;
			if (i != 0) {
				return null;
			}
			result.hostName = string;
			return result;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.B(" + (string != null ? "{...}" : "null") + ',' + i + ',' + i_1_ + ')');
		}
	}

	public static final void method306(int i, RSByteBuffer class98_sub22) {
		try {
			if ((-class98_sub22.position + class98_sub22.payload.length ^ 0xffffffff) <= -2) {
				int i_0_ = class98_sub22.readUnsignedByte((byte) 38);
				if ((i_0_ ^ 0xffffffff) <= -1 && (i_0_ ^ 0xffffffff) >= -2 && i == 10090 && (class98_sub22.payload.length - class98_sub22.position ^ 0xffffffff) <= -3) {
					int i_1_ = class98_sub22.readShort((byte) 127);
					if ((class98_sub22.payload.length - class98_sub22.position ^ 0xffffffff) <= (i_1_ * 6 ^ 0xffffffff)) {
						for (int i_2_ = 0; i_1_ > i_2_; i_2_++) {
							int i_3_ = class98_sub22.readShort((byte) 127);
							int i_4_ = class98_sub22.readInt(-2);
							if ((Class76_Sub5.intGlobalConfigs.length ^ 0xffffffff) < (i_3_ ^ 0xffffffff) && Class140.boolGlobalConfigs[i_3_] && ((Class345.varClientDefinitionList.method2237(i_3_, 101).aChar720 ^ 0xffffffff) != -50 || i_4_ >= -1 && i_4_ <= 1)) {
								Class76_Sub5.intGlobalConfigs[i_3_] = i_4_;
							}
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cda.B(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	public static void method307(boolean bool) {
		do {
			try {
				anInterface17_301 = null;
				if (bool == true) {
					break;
				}
				method307(false);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cda.E(" + bool + ')');
			}
			break;
		} while (false);
	}

	String	hostName;

	int		port;

	public SocketWrapper() {
		/* empty */
	}

	public final Socket createDirectSocket(byte i) throws IOException {
		try {
			if (i != -53) {
				anInterface17_301 = null;
			}
			return new Socket(hostName, port);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cda.D(" + i + ')');
		}
	}

	abstract Socket open(int i) throws IOException;
}


/* Class95 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.constants.BuildLocation;

public final class Class95 {
	public static boolean	aBoolean798		= false;
	public static int		anInt799;
	public static int[]		anIntArray800	= new int[13];

	public static final void method920(byte i) throws IOException {
		do {
			try {
				if (i < 77) {
					method921(true);
				}
				if (aa_Sub1.aClass123_3561 == null || (Class62.anInt490 ^ 0xffffffff) >= -1) {
					break;
				}
				Class160.aClass98_Sub22_1257.position = 0;
				for (;;) {
					OutgoingPacket class98_sub11 = (OutgoingPacket) Class336.aClass148_2827.getFirst(32);
					if (class98_sub11 == null || Class160.aClass98_Sub22_1257.payload.length + -Class160.aClass98_Sub22_1257.position < class98_sub11.anInt3869) {
						break;
					}
					Class160.aClass98_Sub22_1257.method1217(class98_sub11.packet.payload, class98_sub11.anInt3869, -1, 0);
					Class62.anInt490 -= class98_sub11.anInt3869;
					class98_sub11.unlink(90);
					class98_sub11.packet.method1201(0);
					class98_sub11.method1125((byte) 6);
				}
				aa_Sub1.aClass123_3561.write(-24305, Class160.aClass98_Sub22_1257.position, Class160.aClass98_Sub22_1257.payload, 0);
				Class98_Sub50.anInt4289 += Class160.aClass98_Sub22_1257.position;
				BuildLocation.anInt1511 = 0;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ft.B(" + i + ')');
			}
			break;
		} while (false);
	}

	public static void method921(boolean bool) {
		try {
			if (bool != false) {
				anInt799 = 59;
			}
			anIntArray800 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ft.A(" + bool + ')');
		}
	}
}

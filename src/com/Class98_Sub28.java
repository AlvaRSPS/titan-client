/* Class98_Sub28 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;

public abstract class Class98_Sub28 extends Node {
	public static int		anInt4080			= 0;
	public static char[]	charSet				= { '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	public static int		totalBoxVisibility	= 0;

	public static final String longToString(int i, long hash) {
		if (hash <= 0L || hash >= 6582952005840035281L) {
			return null;
		}
		if (hash % 37L == 0L) {
			return null;
		}
		int length = 0;
		long buf = hash;
		while (buf != 0L) {
			buf /= 37L;
			length++;
		}
		StringBuilder stringbuffer = new StringBuilder(length);
		while (hash != 0L) {
			long current = hash;
			hash /= 37L;
			stringbuffer.append(charSet[(int) (-(37L * hash) + current)]);
		}
		return stringbuffer.reverse().toString();
	}

	public static final void method1296(byte i) {
		try {
			Class142.aClass79_1158.clear(106);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lb.K(" + i + ')');
		}
	}

	public static final int method1306(byte i) {
		try {
			boolean bool = false;
			boolean bool_8_ = false;
			boolean bool_9_ = false;
			if (GameShell.signLink.is_signed && !GameShell.signLink.msJava) {
				bool = !(Exception_Sub1.platformInformation.totalPhysicMemory < 512 && Exception_Sub1.platformInformation.totalPhysicMemory != 0);
				if (!SignLink.osNameLowerCase.startsWith("win")) {
					bool_8_ = true;
				} else {
					bool_9_ = true;
					bool_8_ = true;
				}
			}
			if (Class98_Sub10_Sub38.aBoolean5756) {
				bool = false;
			}
			if (Class95.aBoolean798) {
				bool_8_ = false;
			}
			if (Class67.aBoolean520) {
				bool_9_ = false;
			}
			if (!bool && !bool_8_ && !bool_9_) {
				return Class246_Sub3_Sub4_Sub5.method3085(i ^ ~0x69);
			}
			int i_10_ = -1;
			int i_11_ = -1;
			if (bool) {
				try {
					i_10_ = Class66.method683((byte) -79, 1000, 2);
				} catch (Exception exception) {
					/* empty */
				}
			}
			int i_12_ = -1;
			do {
				if (bool_9_) {
					try {
						i_12_ = Class66.method683((byte) -113, 1000, 3);
						if (client.preferences.currentToolkit.getValue((byte) 127) == 3) {
							Class62 class62 = client.graphicsToolkit.method1799();
							long l = 0xffffffffffffL & class62.aLong485;
							int i_13_ = class62.anInt484;
							if ((i_13_ ^ 0xffffffff) != -4319) {
								if ((i_13_ ^ 0xffffffff) != -4099) {
									break;
								}
							} else {
								bool_8_ = bool_8_ & (l ^ 0xffffffffffffffffL) <= -64425238955L;
								break;
							}
							bool_8_ = bool_8_ & l >= 60129613779L;
						}
					} catch (Exception exception) {
						/* empty */
					}
				}
			} while (false);
			if (i != -106) {
				totalBoxVisibility = -48;
			}
			if (bool_8_) {
				try {
					i_11_ = Class66.method683((byte) -115, 1000, 1);
				} catch (Exception exception) {
					/* empty */
				}
			}
			if ((i_10_ ^ 0xffffffff) == 0 && (i_11_ ^ 0xffffffff) == 0 && i_12_ == -1) {
				return Class246_Sub3_Sub4_Sub5.method3085(0);
			}
			i_12_ *= 1.1F;
			i_11_ *= 1.1F;
			if (i_12_ >= i_10_ || i_10_ <= i_11_) {
				if (i_12_ <= i_11_) {
					return VarClientDefinitionParser.method2235(i_11_, 1, (byte) 98);
				}
				return VarClientDefinitionParser.method2235(i_12_, 3, (byte) 98);
			}
			return StrongReferenceMCNode.method1537(i_10_, 5000);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lb.O(" + i + ')');
		}
	}

	public static final int method1307(int i, int i_14_, int i_15_, int i_16_) {
		try {
			if ((QuickChatCategoryParser.loadingProgress ^ 0xffffffff) > -101) {
				return -2;
			}
			if (i_14_ != 1) {
				return 57;
			}
			int i_17_ = -2;
			int i_18_ = 2147483647;
			int i_19_ = -WorldMap.anInt2075 + i_15_;
			int i_20_ = -WorldMap.anInt2078 + i_16_;
			for (Class98_Sub47 class98_sub47 = (Class98_Sub47) WorldMap.aClass148_2065.getFirst(i_14_ ^ 0x21); class98_sub47 != null; class98_sub47 = (Class98_Sub47) WorldMap.aClass148_2065.getNext(88)) {
				if (i == class98_sub47.anInt4268) {
					int i_21_ = class98_sub47.anInt4272;
					int i_22_ = class98_sub47.anInt4267;
					int i_23_ = i_21_ + WorldMap.anInt2075 << -1106336498 | WorldMap.anInt2078 + i_22_;
					int i_24_ = (i_20_ + -i_22_) * (i_20_ + -i_22_) + (-i_21_ + i_19_) * (i_19_ - i_21_);
					if (i_17_ < 0 || i_24_ < i_18_) {
						i_17_ = i_23_;
						i_18_ = i_24_;
					}
				}
			}
			return i_17_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lb.L(" + i + ',' + i_14_ + ',' + i_15_ + ',' + i_16_ + ')');
		}
	}

	boolean			aBoolean4081;

	OpenGlToolkit	aHa_Sub1_4079;

	Class98_Sub28(OpenGlToolkit var_ha_Sub1) {
		try {
			aHa_Sub1_4079 = var_ha_Sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lb.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ')');
		}
	}

	abstract boolean method1294(byte i);

	public abstract void method1295(int i, int i_0_, boolean bool);

	public abstract void method1297(int i, byte i_2_);

	abstract boolean method1298(int i);

	int method1299(boolean bool) {
		try {
			if (bool != false) {
				method1302(null, -118, (byte) 5, null);
			}
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lb.B(" + bool + ')');
		}
	}

	public final boolean method1300(int i) {
		try {
			if (i != 0) {
				aBoolean4081 = false;
			}
			return aBoolean4081;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lb.N(" + i + ')');
		}
	}

	public final boolean method1301(int i) {
		try {
			if (i > -5) {
				return false;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lb.M(" + i + ')');
		}
	}

	public abstract void method1302(Class42_Sub1 class42_sub1, int i, byte i_3_, Class42_Sub1 class42_sub1_4_);

	public final int method1303(int i) {
		try {
			if (i != 0) {
				aBoolean4081 = true;
			}
			return 1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lb.Q(" + i + ')');
		}
	}

	public abstract void method1304(byte i);
}

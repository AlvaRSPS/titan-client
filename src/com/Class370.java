/* Class370 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;

public final class Class370 {
	public static IncomingOpcode				HINT_ICON;
	public static int							anInt3139;
	public static int							anInt3140;
	public static int[]							IDENTIKIT_EQUIPMENT_SLOTS	= { 8, 11, 4, 6, 9, 7, 10, 0 };
	public static RenderAnimDefinitionParser	renderAnimDefinitionList;

	static {
		HINT_ICON = new IncomingOpcode(0, 12);
	}

	public static void method3956(int i) {
		IDENTIKIT_EQUIPMENT_SLOTS = null;
		renderAnimDefinitionList = null;
		if (i <= -118) {
			HINT_ICON = null;
		}
	}

	public static final Class370 method491(int i, boolean bool, Js5 class207, String string) {
		int i_1_ = class207.getGroupId((byte) -102, string);
		if ((i_1_ ^ 0xffffffff) == 0) {
			return new Class370(0);
		}
		int[] is = class207.method2743(i_1_, 6341);
		Class370 class370 = new Class370(is.length);
		int i_2_ = 0;
		int i_3_ = 0;
		while (class370.anInt3137 > i_2_) {
			RSByteBuffer packet = new RSByteBuffer(class207.getFile(is[i_3_++], i_1_, false));
			int i_4_ = packet.readInt(-2);
			int i_5_ = packet.readShort((byte) 127);
			int i_6_ = packet.readUnsignedByte((byte) 96);
			if (!bool && (i_6_ ^ 0xffffffff) == -2) {
				class370.anInt3137--;
			} else {
				class370.anIntArray3133[i_2_] = i_4_;
				class370.anIntArray3138[i_2_] = i_5_;
				i_2_++;
			}
		}
		return class370;
	}

	public int	anInt3137;

	int[]		anIntArray3133;

	int[]		anIntArray3138;

	public Class370(int i) {
		anInt3137 = i;
		anIntArray3133 = new int[anInt3137];
		anIntArray3138 = new int[anInt3137];
	}
}


/* Class98_Sub43_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.constants.BuildLocation;

import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;

public final class Class98_Sub43_Sub2 extends Class98_Sub43 {
	public static OutgoingOpcode	aClass171_5906	= new OutgoingOpcode(49, 4);
	public static float				aFloat5909;
	public static int				anInt5910;

	public static void method1494(int i) {
		do {
			try {
				GameShell.signLink = null;
				aClass171_5906 = null;
				BuildLocation.LOCAL = null;
				if (i == 4) {
					break;
				}
				BuildLocation.LOCAL = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ria.A(" + i + ')');
			}
			break;
		} while (false);
	}

	Class98_Sub43_Sub2(OggStreamState oggstreamstate) {
		super(oggstreamstate);
	}

	@Override
	public final void method1482(OggPacket oggpacket, boolean bool) {
		do {
			try {
				if (bool == false) {
					break;
				}
				anInt5910 = 86;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ria.J(" + (oggpacket != null ? "{...}" : "null") + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method1487(int i) {
		do {
			try {
				if (i == -1128) {
					break;
				}
				aClass171_5906 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ria.C(" + i + ')');
			}
			break;
		} while (false);
	}
}


/* Class66 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Dimension;

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.preferences.LightningDetailPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;
import com.jagex.game.input.impl.AwtKeyEvent;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class Class66 {
	public static boolean aBoolean507 = false;

	public static final void addIgnore(int i, String name, boolean online) {
		if (name != null) {
			if (Class248.ignoreListSize >= 100) {
				OpenGLHeap.addChatMessage(TextResources.IGNORE_LIST_IS_FULL.getText(client.gameLanguage, (byte) 25), 4, (byte) -40);
			} else {
				String filteredName = Class353.filterName(-1, name);
				if (filteredName != null) {
					for (int count = 0; count < Class248.ignoreListSize; count++) {
						String ignore = Class353.filterName(-1, FriendLoginUpdate.ignores[count]);
						if (ignore != null && ignore.equals(filteredName)) {
							OpenGLHeap.addChatMessage(name + TextResources.ALREADY_ADDED_ON_IGNORE_LIST.getText(client.gameLanguage, (byte) 25), 4, (byte) -66);
							return;
						}
						if (ItemDeque.ignoreDisplayNames[count] != null) {
							String filteredDisplayName = Class353.filterName(-1, ItemDeque.ignoreDisplayNames[count]);
							if (filteredDisplayName != null && filteredDisplayName.equals(filteredName)) {
								OpenGLHeap.addChatMessage(name + TextResources.ALREADY_ADDED_ON_IGNORE_LIST.getText(client.gameLanguage, (byte) 25), 4, (byte) -84);
								return;
							}
						}
					}
					for (int count = 0; (count ^ 0xffffffff) > (Class314.friendListSize ^ 0xffffffff); count++) {
						String filteredFriendName = Class353.filterName(-1, Class98_Sub25.friends[count]);
						if (filteredFriendName != null && filteredFriendName.equals(filteredName)) {
							OpenGLHeap.addChatMessage(TextResources.PLEASE_REMOVE_.getText(client.gameLanguage, (byte) 25) + name + TextResources.FROM_FRIENDS_LIST_FIRST.getText(client.gameLanguage, (byte) 25), 4, (byte) 59);
							return;
						}
						if (TextLSEConfig.friendsDisplayNames[count] != null) {
							String filteredFriendDisplayName = Class353.filterName(-1, TextLSEConfig.friendsDisplayNames[count]);
							if (filteredFriendDisplayName != null && filteredFriendDisplayName.equals(filteredName)) {
								OpenGLHeap.addChatMessage(TextResources.PLEASE_REMOVE_.getText(client.gameLanguage, (byte) 25) + name + TextResources.FROM_FRIENDS_LIST_FIRST.getText(client.gameLanguage, (byte) 25), 4, (byte) -95);
								return;
							}
						}
					}
					if (Class353.filterName(-1, Class87.localPlayer.accountName).equals(filteredName)) {
						OpenGLHeap.addChatMessage(TextResources.CANT_ADD_YOURSELF_TO_IGNORE_LIST.getText(client.gameLanguage, (byte) 25), 4, (byte) -66);
					} else {
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, AwtKeyEvent.aClass171_3264, Class331.aClass117_2811);
						class98_sub11.packet.writeByte(1 + NativeShadow.encodedStringLength(name, (byte) 90), 91);
						class98_sub11.packet.writePJStr1(name, (byte) 113);
						class98_sub11.packet.writeByte(online ? 1 : 0, -89);
						if (i < -40) {
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
					}
				}
			}
		}
	}

	public static final int method683(byte i, int i_1_, int i_2_) {
		if ((LightningDetailPreferenceField.anInt3666 ^ 0xffffffff) == 0) {
			return 1;
		}
		if ((client.preferences.currentToolkit.getValue((byte) 122) ^ 0xffffffff) != (i_2_ ^ 0xffffffff)) {
			Class151_Sub9.method2472(true, TextResources.PROFILING.getText(client.gameLanguage, (byte) 25), i_2_, true);
			if ((i_2_ ^ 0xffffffff) != (client.preferences.currentToolkit.getValue((byte) 126) ^ 0xffffffff)) {
				return -1;
			}
		}
		try {
			Dimension dimension = GameShell.canvas.getSize();
			Class246_Sub2.draw(-117, Class98_Sub46_Sub10.p12FullMetrics, Class195.p12Full, true, client.graphicsToolkit, TextResources.PROFILING.getText(client.gameLanguage, (byte) 25));
			BaseModel class178 = Class98_Sub6.fromFile(0, -9252, Class76_Sub9.modelsJs5, LightningDetailPreferenceField.anInt3666);
			long l = TimeTools.getCurrentTime(-47);
			client.graphicsToolkit.clearClip();
			SunDefinition.aClass111_1986.method2100(0, Js5.anInt1577, 0);
			client.graphicsToolkit.a(SunDefinition.aClass111_1986);
			client.graphicsToolkit.DA(dimension.width / 2, dimension.height / 2, 512, 512);
			client.graphicsToolkit.setAmbientIntensity(1.0F);
			client.graphicsToolkit.setSun(16777215, 0.5F, 0.5F, 20.0F, -50.0F, 30.0F);
			ModelRenderer class146 = client.graphicsToolkit.createModelRenderer(class178, 2048, 64, 64, 768);
			int i_3_ = 0;
			while_77_: for (int i_4_ = 0; i_4_ < 500; i_4_++) {
				client.graphicsToolkit.clearImage(0);
				client.graphicsToolkit.ya();
				for (int i_5_ = 15; i_5_ >= 0; i_5_--) {
					for (int i_6_ = 0; i_5_ >= i_6_; i_6_++) {
						Class76_Sub5.aClass111_3745.method2100((int) ((i_6_ - i_5_ / 2.0F) * NativeShadow.anInt6333), 0, (1 + i_5_) * NativeShadow.anInt6333);
						class146.method2325(Class76_Sub5.aClass111_3745, null, 0);
						i_3_++;
						if ((i_1_ ^ 0xffffffffffffffffL) >= (-l + TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL)) {
							break while_77_;
						}
					}
				}
			}
			client.graphicsToolkit.method1825();
			long l_7_ = i_3_ * 1000 / (-l + TimeTools.getCurrentTime(-47));
			client.graphicsToolkit.clearImage(0);
			client.graphicsToolkit.ya();
			return (int) l_7_;
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			return -1;
		}
	}

	private boolean	aBoolean519	= false;
	long			aLong516;
	int				anInt505;
	int				anInt506;
	int				anInt508;
	int				anInt509;
	private int		anInt510;
	int				anInt511;
	int				anInt512;
	int				anInt513	= 0;
	int				anInt514;
	int				anInt515	= 0;

	int				anInt517;

	int				anInt518;

	public Class66() {
		/* empty */
	}

	public final void method682(RSByteBuffer class98_sub22, int i) {
		do {
			try {
				for (;;) {
					int i_0_ = class98_sub22.readUnsignedByte((byte) 96);
					if ((i_0_ ^ 0xffffffff) == -1) {
						break;
					}
					method686(i_0_, class98_sub22, (byte) -126);
				}
				if (i == 17127) {
					break;
				}
				method686(-122, null, (byte) -5);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ega.B(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final void method685(int i) {
		do {
			try {
				anInt514 = Class284_Sub2_Sub2.COSINE[anInt510 << 1969546147];
				long l = anInt506;
				if (i != 1) {
					anInt509 = 44;
				}
				long l_15_ = anInt511;
				long l_16_ = anInt505;
				anInt517 = (int) Math.sqrt(l * l + l_15_ * l_15_ + l_16_ * l_16_);
				if (anInt512 == 0) {
					anInt512 = 1;
				}
				if ((anInt518 ^ 0xffffffff) == -1) {
					aLong516 = 2147483647L;
				} else if ((anInt518 ^ 0xffffffff) == -2) {
					aLong516 = 8 * anInt517 / anInt512;
					aLong516 *= aLong516;
				} else if ((anInt518 ^ 0xffffffff) == -3) {
					aLong516 = 8 * anInt517 / anInt512;
				}
				if (!aBoolean519) {
					break;
				}
				anInt517 *= -1;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ega.A(" + i + ')');
			}
			break;
		} while (false);
	}

	private final void method686(int i, RSByteBuffer class98_sub22, byte i_17_) {
		do {
			try {
				if ((i ^ 0xffffffff) == -2) {
					anInt510 = class98_sub22.readShort((byte) 127);
				} else if (i == 2) {
					class98_sub22.readUnsignedByte((byte) 2);
				} else if (i != 3) {
					if ((i ^ 0xffffffff) == -5) {
						anInt518 = class98_sub22.readUnsignedByte((byte) 90);
						anInt512 = class98_sub22.readInt(-2);
					} else if (i == 6) {
						anInt508 = class98_sub22.readUnsignedByte((byte) 58);
					} else if ((i ^ 0xffffffff) != -9) {
						if (i == 9) {
							anInt515 = 1;
						} else if ((i ^ 0xffffffff) == -11) {
							aBoolean519 = true;
						}
					} else {
						anInt513 = 1;
					}
				} else {
					anInt506 = class98_sub22.readInt(-2);
					anInt511 = class98_sub22.readInt(-2);
					anInt505 = class98_sub22.readInt(-2);
				}
				if (i_17_ <= -120) {
					break;
				}
				anInt518 = 31;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ega.D(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_17_ + ')');
			}
			break;
		} while (false);
	}
}

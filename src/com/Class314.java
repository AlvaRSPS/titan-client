/* Class314 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.preferences.ToolkitPreferenceField;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.shadow.Shadow;

public final class Class314 {
	public static int	anInt2690		= -1;
	public static int[]	anIntArray2691	= new int[3];
	public static int	friendListSize	= 0;

	public static final int method3637(int i, int i_0_) {
		try {
			return 0xff & i_0_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tg.A(" + i + ',' + i_0_ + ')');
		}
	}

	public static final void method3639(int i, String string, boolean bool) {
		do {
			int playerCount = Class2.playerCount;
			int[] playerIndices = Class319.playerIndices;
			boolean bool_3_ = bool;
			for (int index = 0; playerCount > index; index++) {
				Player player = Class151_Sub9.players[playerIndices[index]];
				if (player != null && Class87.localPlayer != player && player.accountName != null && player.accountName.equalsIgnoreCase(string)) {
					bool_3_ = true;
					if (i == 1) {
						AnimationSkeletonSet.anInt6044++;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, ToolkitPreferenceField.aClass171_3661, Class331.aClass117_2811);
						class98_sub11.packet.writeLEShort(playerIndices[index], 17624);
						class98_sub11.packet.writeByteS(0, -65);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					} else if ((i ^ 0xffffffff) == -5) {
						Class65.anInt498++;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, GameObjectDefinitionParser.aClass171_2520, Class331.aClass117_2811);
						class98_sub11.packet.writeByteS(0, -24);
						class98_sub11.packet.writeShort(playerIndices[index], 1571862888);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					} else if ((i ^ 0xffffffff) != -6) {
						if (i != 6) {
							if ((i ^ 0xffffffff) == -8) {
								BConfigDefinition.anInt3111++;
								OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class277.aClass171_2051, Class331.aClass117_2811);
								class98_sub11.packet.writeByteS(0, -105);
								class98_sub11.packet.writeShort(playerIndices[index], 1571862888);
								Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
							}
						} else {
							Class98_Sub43.anInt4242++;
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, DummyOutputStream.aClass171_34, Class331.aClass117_2811);
							class98_sub11.packet.method1244(0, (byte) 112);
							class98_sub11.packet.writeShort(playerIndices[index], 1571862888);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
					} else {
						Class98_Sub23.anInt4001++;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, HitmarksDefinitionParser.aClass171_1001, Class331.aClass117_2811);
						class98_sub11.packet.writeLEShortA(playerIndices[index], 128);
						class98_sub11.packet.method1231(0, (byte) 110);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					}
					break;
				}
			}
			if (bool_3_) {
				break;
			}
			OpenGLHeap.addChatMessage(TextResources.UNABLE_TO_FIND.getText(client.gameLanguage, (byte) 25) + string, 4, (byte) 57);
			System.out.println("Reached line 72");
			break;
		} while (false);
	}

	public static final void method3644(int i, int i_27_) {
		Shadow var_r = null;
		for (int i_28_ = i; i_28_ < i_27_; i_28_++) {
			Ground var_s = StrongReferenceMCNode.aSArray6298[i_28_];
			if (var_s != null) {
				for (int i_29_ = 0; i_29_ < Class64_Sub9.anInt3662; i_29_++) {
					for (int i_30_ = 0; i_30_ < BConfigDefinition.anInt3112; i_30_++) {
						var_r = var_s.replaceShadow(i_30_, i_29_, var_r);
						if (var_r != null) {
							int i_31_ = i_30_ << Class151_Sub8.tileScale;
							int i_32_ = i_29_ << Class151_Sub8.tileScale;
							for (int i_33_ = i_28_ - 1; i_33_ >= 0; i_33_--) {
								Ground var_s_34_ = StrongReferenceMCNode.aSArray6298[i_33_];
								if (var_s_34_ != null) {
									int i_35_ = var_s.getTileHeight(i_29_, -12639, i_30_) - var_s_34_.getTileHeight(i_29_, -12639, i_30_);
									int i_36_ = var_s.getTileHeight(i_29_, -12639, i_30_ + 1) - var_s_34_.getTileHeight(i_29_, -12639, i_30_ + 1);
									int i_37_ = var_s.getTileHeight(i_29_ + 1, -12639, i_30_ + 1) - var_s_34_.getTileHeight(i_29_ + 1, -12639, i_30_ + 1);
									int i_38_ = var_s.getTileHeight(i_29_ + 1, -12639, i_30_) - var_s_34_.getTileHeight(i_29_ + 1, -12639, i_30_);
									var_s_34_.deleteShadow(var_r, i_31_, (i_35_ + i_36_ + i_37_ + i_38_) / 4, i_32_, 0, false);
								}
							}
						}
					}
				}
			}
		}
	}

	private int		anInt2686;

	private int		anInt2689;

	private int[][]	anIntArrayArray2687;

	public Class314(int i, int i_39_) {
		try {
			if ((i ^ 0xffffffff) != (i_39_ ^ 0xffffffff)) {
				int i_40_ = Class126.method2216(i, 111, i_39_);
				i_39_ /= i_40_;
				i /= i_40_;
				anInt2686 = i_39_;
				anIntArrayArray2687 = new int[i][14];
				anInt2689 = i;
				for (int i_41_ = 0; i_41_ < i; i_41_++) {
					int[] is = anIntArrayArray2687[i_41_];
					double d = 6.0 + (double) i_41_ / (double) i;
					int i_42_ = (int) Math.floor(d - 7.0 + 1.0);
					if (i_42_ < 0) {
						i_42_ = 0;
					}
					int i_43_ = (int) Math.ceil(7.0 + d);
					if ((i_43_ ^ 0xffffffff) < -15) {
						i_43_ = 14;
					}
					double d_44_ = (double) i_39_ / (double) i;
					for (/**/; (i_42_ ^ 0xffffffff) > (i_43_ ^ 0xffffffff); i_42_++) {
						double d_45_ = (-d + i_42_) * 3.141592653589793;
						double d_46_ = d_44_;
						if (d_45_ < -1.0E-4 || d_45_ > 1.0E-4) {
							d_46_ *= Math.sin(d_45_) / d_45_;
						}
						d_46_ *= Math.cos(0.2243994752564138 * (i_42_ - d)) * 0.46 + 0.54;
						is[i_42_] = (int) Math.floor(0.5 + d_46_ * 65536.0);
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tg.<init>(" + i + ',' + i_39_ + ')');
		}
	}

	public final int method3638(int i, int i_1_) {
		try {
			if (anIntArrayArray2687 != null) {
				i_1_ = 6 + (int) ((long) i_1_ * (long) anInt2686 / anInt2689);
			}
			if (i != 6) {
				method3642(false, null);
			}
			return i_1_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tg.E(" + i + ',' + i_1_ + ')');
		}
	}

	public final short[] method3641(int i, short[] is) {
		try {
			if (anIntArrayArray2687 != null) {
				int i_5_ = (int) ((long) anInt2686 * (long) is.length / anInt2689) - -14;
				int[] is_6_ = new int[i_5_];
				int i_7_ = 0;
				int i_8_ = 0;
				for (short i_10_ : is) {
					int[] is_11_ = anIntArrayArray2687[i_8_];
					for (int i_12_ = 0; (i_12_ ^ 0xffffffff) > -15; i_12_++) {
						is_6_[i_7_ - -i_12_] += is_11_[i_12_] * i_10_ >> 1476810210;
					}
					i_8_ += anInt2686;
					int i_13_ = i_8_ / anInt2689;
					i_7_ += i_13_;
					i_8_ -= i_13_ * anInt2689;
				}
				is = new short[i_5_];
				for (int i_14_ = 0; i_14_ < i_5_; i_14_++) {
					int i_15_ = 8192 + is_6_[i_14_] >> -2136199730;
					if (i_15_ >= -32768) {
						if (i_15_ <= 32767) {
							is[i_14_] = (short) i_15_;
						} else {
							is[i_14_] = (short) 32767;
						}
					} else {
						is[i_14_] = (short) -32768;
					}
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tg.G(" + i + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	public final byte[] method3642(boolean bool, byte[] is) {
		try {
			if (bool != true) {
				method3641(-23, null);
			}
			if (anIntArrayArray2687 != null) {
				int i = (int) ((long) is.length * (long) anInt2686 / anInt2689) - -14;
				int[] is_16_ = new int[i];
				int i_17_ = 0;
				int i_18_ = 0;
				for (int i_19_ = 0; (is.length ^ 0xffffffff) < (i_19_ ^ 0xffffffff); i_19_++) {
					int i_20_ = is[i_19_];
					int[] is_21_ = anIntArrayArray2687[i_18_];
					for (int i_22_ = 0; i_22_ < 14; i_22_++) {
						is_16_[i_17_ + i_22_] += is_21_[i_22_] * i_20_;
					}
					i_18_ += anInt2686;
					int i_23_ = i_18_ / anInt2689;
					i_17_ += i_23_;
					i_18_ -= anInt2689 * i_23_;
				}
				is = new byte[i];
				for (int i_24_ = 0; (i ^ 0xffffffff) < (i_24_ ^ 0xffffffff); i_24_++) {
					int i_25_ = 32768 + is_16_[i_24_] >> -107504688;
					if ((i_25_ ^ 0xffffffff) <= 127) {
						if (i_25_ <= 127) {
							is[i_24_] = (byte) i_25_;
						} else {
							is[i_24_] = (byte) 127;
						}
					} else {
						is[i_24_] = (byte) -128;
					}
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tg.C(" + bool + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	public final int method3643(int i, int i_26_) {
		try {
			if (i_26_ != 1) {
				anInt2686 = -77;
			}
			if (anIntArrayArray2687 != null) {
				i = (int) ((long) i * (long) anInt2686 / anInt2689);
			}
			return i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tg.H(" + i + ',' + i_26_ + ')');
		}
	}
}

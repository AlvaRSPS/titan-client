/* Class289 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;

public final class Class289 {
	public static Server server;

	public static final void method3407(Char class246_sub3, PointLight[] class98_sub5s) {
		if (QuickChatMessageType.aBoolean2914) {
			int i = class246_sub3.method2980(-51, class98_sub5s);
			Class98_Sub10_Sub30.activeToolkit.method1818(i, class98_sub5s);
		}
		if (Class81.aSArray618 == Class78.aSArray594) {
			int i;
			int i_1_;
			if (class246_sub3 instanceof Class246_Sub3_Sub4) {
				i = ((Class246_Sub3_Sub4) class246_sub3).aShort6158;
				i_1_ = ((Class246_Sub3_Sub4) class246_sub3).aShort6157;
			} else {
				i = class246_sub3.boundExtentsX >> Class151_Sub8.tileScale;
				i_1_ = class246_sub3.boundExtentsZ >> Class151_Sub8.tileScale;
			}
			Class98_Sub10_Sub30.activeToolkit.EA(StrongReferenceMCNode.aSArray6298[0].averageHeight(class246_sub3.boundExtentsX, class246_sub3.boundExtentsZ, true), PlayerAppearence.method3636(i, i_1_), ProxyException.method127(i, i_1_), Class98_Sub46_Sub20.method1639(i, i_1_));
		}
		Class246_Sub1 class246_sub1 = class246_sub3.method2975(Class98_Sub10_Sub30.activeToolkit, -30);
		if (class246_sub1 != null) {
			if (class246_sub3.aBoolean5082) {
				Class246_Sub6[] class246_sub6s = class246_sub1.aClass246_Sub6Array5067;
				for (Class246_Sub6 class246_sub6 : class246_sub6s) {
					if (class246_sub6.aBoolean5114) {
						AnimatedProgressBarLSEConfig.method908(class246_sub6.anInt5112 + class246_sub6.anInt5109, class246_sub6.anInt5113 - class246_sub6.anInt5109, false, class246_sub6.anInt5111 - class246_sub6.anInt5109, class246_sub6.anInt5110 + class246_sub6.anInt5109);
					}
				}
			}
			if (class246_sub1.aBoolean5070) {
				class246_sub1.aClass246_Sub3_5069 = class246_sub3;
				if (Class375.aBoolean3170) {
					synchronized (Class98_Sub10_Sub27.aClass84_5692) {
						Class98_Sub10_Sub27.aClass84_5692.method836(0, class246_sub1);
					}
				} else {
					Class98_Sub10_Sub27.aClass84_5692.method836(0, class246_sub1);
				}
			} else {
				Class35.method333(class246_sub1, 95);
			}
		}
	}

	public static final void method3408(byte i) {
		try {
			if (i < 79) {
				method3410(67);
			}
			for (Class98_Sub36 class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.startIteration(126); class98_sub36 != null; class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.iterateNext(-1)) {
				if (!class98_sub36.aBoolean4158) {
					OutgoingPacket.method1127((byte) 67, class98_sub36.anInt4160);
				} else {
					class98_sub36.aBoolean4158 = false;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rs.A(" + i + ')');
		}
	}

	public static void method3409(byte i) {
		do {
			try {
				server = null;
				if (i == -117) {
					break;
				}
				method3407(null, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rs.B(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final OutgoingPacket method3410(int i) {
		try {
			if (i == (Class98_Sub33.anInt4117 ^ 0xffffffff)) {
				return new OutgoingPacket();
			}
			return StrongReferenceMCNode.aClass98_Sub11Array6302[--Class98_Sub33.anInt4117];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rs.C(" + i + ')');
		}
	}
}

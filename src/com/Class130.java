/* Class130 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;

public final class Class130 {
	public static Char[]						aClass246_Sub3Array1029;
	public static int							anInt1031	= 0;
	public static Object						anObject1030;
	public static GameObjectDefinitionParser	gameObjectDefinitionList;

	public static final boolean method2230(int i, int i_0_, int i_1_, int i_2_, int i_3_, byte[] is, int i_4_) {
		int i_5_ = i_3_ % i;
		int i_6_;
		if ((i_5_ ^ 0xffffffff) != -1) {
			i_6_ = i - i_5_;
		} else {
			i_6_ = 0;
		}
		int i_7_ = -((-1 + i + i_4_) / i);
		int i_8_ = -((i + i_3_ - i_1_) / i);
		for (int i_9_ = i_7_; (i_9_ ^ 0xffffffff) > -1; i_9_++) {
			for (int i_10_ = i_8_; (i_10_ ^ 0xffffffff) > -1; i_10_++) {
				if ((is[i_0_] ^ 0xffffffff) == -1) {
					return true;
				}
				i_0_ += i;
			}
			i_0_ -= i_6_;
			if (is[-1 + i_0_] == 0) {
				return true;
			}
			i_0_ += i_2_;
		}
		return false;
	}

}

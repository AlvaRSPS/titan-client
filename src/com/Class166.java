/* Class166 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;

public final class Class166 {
	public static boolean	aBoolean1278	= false;
	public static Js5		imageJs5;
	public static int		anInt1279;

	public static final void method2525(int i) {
		try {
			Class259.anInt1954 = 0;
			Minimap.anInt1544++;
			Class65.anInt502 = 0;
			NodeObject.method1468(-4942);
			WorldMapInfoDefinitionParser.method3810((byte) 121);
			Class21_Sub2.method273((byte) -61);
			boolean bool = false;
			for (int i_0_ = 0; (Class259.anInt1954 ^ 0xffffffff) < (i_0_ ^ 0xffffffff); i_0_++) {
				int i_1_ = Class246_Sub3_Sub4_Sub3.anIntArray6451[i_0_];
				NodeObject node = (NodeObject) ProceduralTextureSource.npc.get(i_1_, -1);
				NPC npc = node.npc;
				if (Player.menuOpen && Class98_Sub44.method1514(-128, i_1_)) {
					Class317.method3651((byte) -53);
				}
				if ((Minimap.anInt1544 ^ 0xffffffff) != (npc.anInt6406 ^ 0xffffffff)) {
					if (npc.definition.method2302((byte) 117)) {
						Class98_Sub43_Sub4.method1504(npc, -16255);
					}
					npc.setDefinition(null, 1);
					node.unlink(i + 98);
					bool = true;
				}
			}
			if (bool) {
				Class98_Sub10_Sub20.anInt5640 = ProceduralTextureSource.npc.size((byte) -6);
				ProceduralTextureSource.npc.getValues(BackgroundColourLSEConfig.aClass98_Sub39Array3516, (byte) 74);
			}
			if (PacketParser.buffer.position != Class65.currentPacketSize) {
				throw new RuntimeException("gnp1 pos:" + PacketParser.buffer.position + " psize:" + Class65.currentPacketSize);
			}
			for (int i_2_ = 0; (Class150.npcCount ^ 0xffffffff) < (i_2_ ^ 0xffffffff); i_2_++) {
				if (ProceduralTextureSource.npc.get(Orientation.npcIndices[i_2_], -1) == null) {
					throw new RuntimeException("gnp2 pos:" + i_2_ + " size:" + Class150.npcCount);
				}
			}
			if (-Class150.npcCount + Class98_Sub10_Sub20.anInt5640 != i) {
				throw new RuntimeException("gnp3 mis:" + (Class98_Sub10_Sub20.anInt5640 + -Class150.npcCount));
			}
			for (int i_3_ = 0; i_3_ < Class98_Sub10_Sub20.anInt5640; i_3_++) {
				if ((Minimap.anInt1544 ^ 0xffffffff) != (BackgroundColourLSEConfig.aClass98_Sub39Array3516[i_3_].npc.anInt6406 ^ 0xffffffff)) {
					throw new RuntimeException("gnp4 uk:" + BackgroundColourLSEConfig.aClass98_Sub39Array3516[i_3_].npc.index);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Class166() {
		/* empty */
	}
}

/* Class76_Sub4 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.SkyboxDefinitionParser;
import com.jagex.game.toolkit.matrix.NativeMatrix;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class Class76_Sub4 extends Class76 {
	public static HashTable aClass377_3738 = new HashTable(16);

	public static final int getId(boolean bool, String languageCode) {
		int id = 0;
		for (/**/; (BConfigDefinition.languageCodes.length ^ 0xffffffff) < (id ^ 0xffffffff); id++) {
			if (BConfigDefinition.languageCodes[id].equalsIgnoreCase(languageCode)) {
				return id;
			}
		}
		return -1;
	}

	public static void method753(int i) {
		aClass377_3738 = null;
	}

	public static final void method754(int i, boolean bool, int i_1_) {
		Class151_Sub9.method2472(true, TextResources.LOADING.getText(client.gameLanguage, (byte) 25), i, bool);
	}

	public static final void method756(int i, String string) {
		if (WhirlpoolGenerator.clanChat != null) {
			OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, SkyboxDefinitionParser.aClass171_468, Class331.aClass117_2811);
			frame.packet.writeByte(NativeShadow.encodedStringLength(string, (byte) 98), -118);
			frame.packet.writePJStr1(string, (byte) 113);
			Class98_Sub10_Sub29.sendPacket(false, frame);
		}
	}

	private boolean				aBoolean3739;

	private boolean				aBoolean3741	= false;

	private Interface4_Impl3[]	anInterface4_Impl3Array3740;

	Class76_Sub4(NativeToolkit var_ha_Sub3) {
		super(var_ha_Sub3);
		do {
			try {
				if (!var_ha_Sub3.aBoolean4569) {
					break;
				}
				aBoolean3739 = (var_ha_Sub3.anInt4608 ^ 0xffffffff) > -4;
				int i = !aBoolean3739 ? 127 : 48;
				int[][] is = new int[6][4096];
				int[][] is_3_ = new int[6][4096];
				int[][] is_4_ = new int[6][4096];
				int i_5_ = 0;
				for (int i_6_ = 0; (i_6_ ^ 0xffffffff) > -65; i_6_++) {
					for (int i_7_ = 0; (i_7_ ^ 0xffffffff) > -65; i_7_++) {
						float f = -1.0F + 2.0F * i_7_ / 64.0F;
						float f_8_ = 2.0F * i_6_ / 64.0F - 1.0F;
						float f_9_ = (float) (1.0 / Math.sqrt(1.0F + f * f + f_8_ * f_8_));
						f *= f_9_;
						f_8_ *= f_9_;
						for (int i_10_ = 0; (i_10_ ^ 0xffffffff) > -7; i_10_++) {
							float f_11_;
							if ((i_10_ ^ 0xffffffff) == -1) {
								f_11_ = -f;
							} else if (i_10_ == 1) {
								f_11_ = f;
							} else if ((i_10_ ^ 0xffffffff) == -3) {
								f_11_ = f_8_;
							} else if ((i_10_ ^ 0xffffffff) == -4) {
								f_11_ = -f_8_;
							} else if (i_10_ == 4) {
								f_11_ = f_9_;
							} else {
								f_11_ = -f_9_;
							}
							int i_12_;
							int i_13_;
							int i_14_;
							if (f_11_ > 0.0F) {
								i_12_ = (int) (Math.pow(f_11_, 96.0) * i);
								i_13_ = (int) (Math.pow(f_11_, 36.0) * i);
								i_14_ = (int) (Math.pow(f_11_, 12.0) * i);
							} else {
								i_12_ = i_13_ = i_14_ = 0;
							}
							is_3_[i_10_][i_5_] = i_12_ << -1449782824;
							is_4_[i_10_][i_5_] = i_13_ << 1473485688;
							is[i_10_][i_5_] = i_14_ << -11137256;
						}
						i_5_++;
					}
				}
				anInterface4_Impl3Array3740 = new Interface4_Impl3[3];
				anInterface4_Impl3Array3740[0] = this.aHa_Sub3_585.method1934(8, false, is_3_, 64);
				anInterface4_Impl3Array3740[1] = this.aHa_Sub3_585.method1934(8, false, is_4_, 64);
				anInterface4_Impl3Array3740[2] = this.aHa_Sub3_585.method1934(8, false, is, 64);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "iea.<init>(" + (var_ha_Sub3 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method739(int i) {
		try {
			if (i != -2) {
				aBoolean3741 = true;
			}
			if (aBoolean3741) {
				this.aHa_Sub3_585.method1951((byte) 120, 1);
				this.aHa_Sub3_585.method2005(null, i ^ 0x7c);
				this.aHa_Sub3_585.method1964(NativeShadow.aClass38_6334, (byte) 26);
				this.aHa_Sub3_585.method1985(2);
				if (!aBoolean3739) {
					this.aHa_Sub3_585.method2019(Class249.aClass128_1903, Class249.aClass128_1903, 22831);
					this.aHa_Sub3_585.method2051(0, -55, QuickChat.aClass65_2499);
					this.aHa_Sub3_585.method1951((byte) 120, 2);
					this.aHa_Sub3_585.method2019(Class249.aClass128_1903, Class249.aClass128_1903, 22831);
					this.aHa_Sub3_585.method2051(0, -110, QuickChat.aClass65_2499);
					this.aHa_Sub3_585.method2051(1, -56, IncomingOpcode.aClass65_459);
					this.aHa_Sub3_585.method1953(i ^ 0x56, QuickChat.aClass65_2499, 0);
					this.aHa_Sub3_585.method2005(null, -120);
				} else {
					this.aHa_Sub3_585.method2019(Class249.aClass128_1903, Class249.aClass128_1903, 22831);
					this.aHa_Sub3_585.method2051(0, -114, QuickChat.aClass65_2499);
					this.aHa_Sub3_585.method1953(-120, QuickChat.aClass65_2499, 0);
				}
				this.aHa_Sub3_585.method1951((byte) 120, 0);
				aBoolean3741 = false;
			} else {
				this.aHa_Sub3_585.method1953(i + -83, QuickChat.aClass65_2499, 0);
			}
			this.aHa_Sub3_585.method2019(Class249.aClass128_1903, Class249.aClass128_1903, i + 22833);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iea.C(" + i + ')');
		}
	}

	@Override
	public final void method742(int i, int i_15_, Interface4 interface4) {
		try {
			this.aHa_Sub3_585.method2005(interface4, 89);
			if (i != 6) {
				method753(49);
			}
			this.aHa_Sub3_585.method2015(i_15_, (byte) 36);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iea.I(" + i + ',' + i_15_ + ',' + (interface4 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method743(int i, boolean bool) {
		try {
			if (i <= 93) {
				getId(false, null);
			}
			this.aHa_Sub3_585.method2019(Class288.aClass128_3381, Class249.aClass128_1903, 22831);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iea.D(" + i + ',' + bool + ')');
		}
	}

	@Override
	public final boolean method745(byte i) {
		try {
			if (i != 27) {
				method746(56, 65, -40);
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iea.H(" + i + ')');
		}
	}

	@Override
	public final void method746(int i, int i_16_, int i_17_) {
		try {
			if (i_17_ >= -75) {
				aBoolean3741 = false;
			}
			if (aBoolean3741) {
				this.aHa_Sub3_585.method1951((byte) 120, 1);
				this.aHa_Sub3_585.method2005(anInterface4_Impl3Array3740[i + -1], -125);
				this.aHa_Sub3_585.method1951((byte) 120, 0);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iea.E(" + i + ',' + i_16_ + ',' + i_17_ + ')');
		}
	}

	@Override
	public final void method748(int i, boolean bool) {
		try {
			if (i != 69) {
				aBoolean3741 = true;
			}
			if (anInterface4_Impl3Array3740 == null || !bool) {
				this.aHa_Sub3_585.method1953(-74, Class98_Sub43_Sub3.aClass65_5926, 0);
			} else {
				this.aHa_Sub3_585.method1951((byte) 120, 1);
				this.aHa_Sub3_585.method1964(Class357.aClass38_3026, (byte) 26);
				NativeMatrix class111_sub3 = this.aHa_Sub3_585.method1957((byte) -92);
				class111_sub3.method2107(1024);
				this.aHa_Sub3_585.method2008(Class144.aClass258_1168, (byte) 125);
				do {
					if (!aBoolean3739) {
						this.aHa_Sub3_585.method2019(Class249.aClass128_1903, Class288.aClass128_3381, 22831);
						this.aHa_Sub3_585.method2051(0, -60, IncomingOpcode.aClass65_459);
						this.aHa_Sub3_585.method1951((byte) 120, 2);
						this.aHa_Sub3_585.method2019(Class288.aClass128_3381, Class1.aClass128_64, 22831);
						this.aHa_Sub3_585.method2051(0, i + -183, IncomingOpcode.aClass65_459);
						this.aHa_Sub3_585.method2026(1, true, (byte) 27, IncomingOpcode.aClass65_459, false);
						this.aHa_Sub3_585.method1953(i + -177, Class98_Sub43_Sub3.aClass65_5926, 0);
						this.aHa_Sub3_585.method2005(this.aHa_Sub3_585.anInterface4_4586, -114);
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					this.aHa_Sub3_585.method2019(Class288.aClass128_3381, Class1.aClass128_64, 22831);
					this.aHa_Sub3_585.method2026(0, true, (byte) 27, QuickChat.aClass65_2499, false);
					this.aHa_Sub3_585.method1953(-123, Class98_Sub43_Sub3.aClass65_5926, 0);
				} while (false);
				this.aHa_Sub3_585.method1951((byte) 120, 0);
				aBoolean3741 = true;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iea.B(" + i + ',' + bool + ')');
		}
	}
}

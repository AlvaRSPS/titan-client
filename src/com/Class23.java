/* Class23 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.preferences.CpuUsagePreferenceField;
import com.jagex.game.toolkit.shadow.OpenGlShadow;

public final class Class23 {
	public static int	anInt221		= 0;
	public static int[]	anIntArray222	= new int[1];

	public static final int priorPowerOf2(int i, int i_1_) {
		int i_2_ = i_1_ >>> 1;
		i_2_ |= i_2_ >>> 1;
		i_2_ |= i_2_ >>> 2;
		i_2_ |= i_2_ >>> 4;
		i_2_ |= i_2_ >>> 8;
		i_2_ |= i_2_ >>> 16;
		return (i_2_ ^ 0xffffffff) & i_1_;
	}

	public static final void method283(byte i) {
		FileOnDisk class356 = null;
		try {
			class356 = SignLink.openPreferences(-106, "2");
			RSByteBuffer buffer = new RSByteBuffer(6 * CpuUsagePreferenceField.anInt3700 + 3);
			buffer.writeByte(1, -89);
			buffer.writeShort(CpuUsagePreferenceField.anInt3700, 1571862888);
			for (int config = 0; Class76_Sub5.intGlobalConfigs.length > config; config++) {
				if (Class140.boolGlobalConfigs[config]) {
					buffer.writeShort(config, 1571862888);
					buffer.writeInt(1571862888, Class76_Sub5.intGlobalConfigs[config]);
				}
			}
			class356.method3882(buffer.payload, 4657, 0, buffer.position);
		} catch (Exception exception) {
			/* empty */
		}
		try {
			if (class356 != null) {
				class356.close(true);
			}
		} catch (Exception exception) {
			/* empty */
		}
		OpenGlShadow.aLong6322 = TimeTools.getCurrentTime(-47);
		Class66.aBoolean507 = false;
	}
}

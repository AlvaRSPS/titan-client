/* Class150 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class150 {
	public static OutgoingOpcode	aClass171_1209	= new OutgoingOpcode(19, 2);
	public static IncomingOpcode	aClass58_1210;
	public static IncomingOpcode	aClass58_1212;
	public static int				npcCount		= 0;

	static {
		aClass58_1210 = new IncomingOpcode(49, 2);
		aClass58_1212 = new IncomingOpcode(31, -2);
	}

	public static void method2436(boolean bool) {
		try {
			aClass58_1210 = null;
			aClass171_1209 = null;
			aClass58_1212 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kca.A(" + bool + ')');
		}
	}

	public static final int method2437(byte i, int i_0_) {
		try {
			return i_0_ >>> -54059832;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kca.B(" + i + ',' + i_0_ + ')');
		}
	}
}

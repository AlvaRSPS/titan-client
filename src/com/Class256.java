/* Class256 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;

public abstract class Class256 {
	public static int anInt1945 = -1;

	public static final void method3193(int i) {
		try {
			Class246_Sub3_Sub1_Sub2.anInt6251 = -1;
			LightIntensityDefinitionParser.anInt2024 = i;
			Canvas_Sub1.anInt26 = -1;
			Class333.anInt3386 = 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qa.A(" + i + ')');
		}
	}

	public Class256() {
		/* empty */
	}
}

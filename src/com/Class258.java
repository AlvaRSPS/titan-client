/* Class258 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class258 {
	public static Class155[]	aClass155Array1951;
	public static int			anInt1952	= 0;

	public static final Class119_Sub2 method3203(byte i, RSByteBuffer class98_sub22) {
		try {
			if (i >= -91) {
				aClass155Array1951 = null;
			}
			return new Class119_Sub2(class98_sub22.readUShort(false), class98_sub22.readUShort(false), class98_sub22.readUShort(false), class98_sub22.readUShort(false), class98_sub22.readMediumInt(-126), class98_sub22.readMediumInt(-123), class98_sub22.readUnsignedByte((byte) -7));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qc.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	public static void method3204(byte i) {
		do {
			try {
				aClass155Array1951 = null;
				if (i == -18) {
					break;
				}
				aClass155Array1951 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qc.B(" + i + ')');
			}
			break;
		} while (false);
	}

	public Class258() {
		/* empty */
	}

	@Override
	public final String toString() {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qc.toString(" + ')');
		}
	}
}

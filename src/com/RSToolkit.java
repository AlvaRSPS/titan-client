
/* ha - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Rectangle;

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.SkyboxDefinition;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.heap.Heap;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public abstract class RSToolkit {
	public static boolean			aBoolean940		= false;
	public static OutgoingOpcode	aClass171_939	= new OutgoingOpcode(50, -1);
	public static int				anInt936;
	public static int				anInt943		= 0;
	public static int[]				anIntArray942	= new int[2];

	public static final synchronized RSToolkit create(byte i, int antialiasCount, Canvas canvas, TextureMetricsList textureSource, int i_3_, Js5 shaderJs5, int toolkit, int i_5_) {
		if ((toolkit ^ 0xffffffff) == -1) {
			return DummyOutputStream.create(textureSource, i_5_, i_3_, canvas, 500);
		}
		if ((toolkit ^ 0xffffffff) == -2) {
			return VerticalAlignment.create(canvas, 2, antialiasCount, textureSource);
		}
		if ((toolkit ^ 0xffffffff) == -6) {
			return SkyboxDefinition.create(antialiasCount, shaderJs5, textureSource, 0, canvas);
		}
		if (toolkit == 3) {
			return Class214.create(54, antialiasCount, canvas, textureSource, shaderJs5);
		}
		throw new IllegalArgumentException("Unknown Toolkit");
	}

	public static final RSToolkit createToolkit(int multisampleValue, Canvas canvas, int i_0_, TextureMetricsList textureList, int i_1_, Js5 shaderJs5) {
		int width = 0;
		int height = 0;
		if (canvas != null) {
			Dimension dimension = canvas.getSize();
			width = dimension.width;
			height = dimension.height;
		}
		return RSToolkit.create((byte) 64, multisampleValue, canvas, textureList, width, shaderJs5, i_1_, height);
	}

	public static void method1787(boolean bool) {
		anIntArray942 = null;
		if (bool != false) {
			aClass171_939 = null;
		}
		aClass171_939 = null;
	}

	public int					id;

	public TextureMetricsList	metricsList;

	RSToolkit(TextureMetricsList var_d) {
		metricsList = var_d;
		int i = -1;
		for (int i_104_ = 0; (i_104_ ^ 0xffffffff) > -9; i_104_++) {
			if (!Class98_Sub10_Sub8.aBooleanArray5579[i_104_]) {
				Class98_Sub10_Sub8.aBooleanArray5579[i_104_] = true;
				i = i_104_;
				break;
			}
		}
		if ((i ^ 0xffffffff) == 0) {
			throw new IllegalStateException("NFTI");
		}
		id = i;
	}

	public abstract void a(Matrix class111);

	public abstract void a(int i, int i_164_, int i_165_, int i_166_, int i_167_, int i_168_, RtInterfaceClip var_aa, int i_169_, int i_170_);

	public abstract void a(int i, int i_120_, int i_121_, int i_122_, int i_123_, int i_124_, RtInterfaceClip var_aa, int i_125_, int i_126_, int i_127_, int i_128_, int i_129_);

	abstract Ground a(int i, int i_7_, int[][] is, int[][] is_8_, int i_9_, int i_10_, int i_11_);

	public abstract void a(Rectangle[] rectangles, int i, int i_162_, int i_163_) throws Exception_Sub1;

	public abstract void a(Heap var_za);

	public abstract void fillImageClip(int i, RtInterfaceClip var_aa, int i_62_, int i_63_);

	public abstract void addCanvas(Canvas canvas, int i, int i_1_);

	public abstract void applyFog(int i, int i_183_, int i_184_);

	public abstract void attachContext(int i);

	public abstract void b(int i, int i_159_, int i_160_, int i_161_, double d);

	public abstract int c(int i, int i_103_);

	public abstract boolean canEnableBloom();

	public abstract void clearClip();

	public abstract void clearImage(int i);

	public abstract void constrainClip(int i, int i_85_, int i_86_, int i_87_);

	public abstract void createContexts(int i);

	public abstract Font createFont(FontSpecifications class197, Image[] class324s, boolean bool);

	public abstract Matrix createMatrix();

	public abstract ModelRenderer createModelRenderer(BaseModel class178, int i, int i_115_, int i_116_, int i_117_);

	abstract Heap createHeap(int i);

	abstract PointLight createPointLight(int i, int i_70_, int i_71_, int i_72_, int i_73_, float f);

	abstract RenderTarget createRenderTarget(Interface5 interface5, DepthBufferObject depthBufferObject);

	public abstract Sprite createSprite(Image class324, boolean bool);

	public abstract Sprite createSprite(int i, int i_0_, boolean bool);

	public final Sprite createSprite(int i, int i_23_, int i_24_, int i_25_, int[] is, int i_26_) {
		if (i != -7962) {
			return null;
		}
		return createSprite(is, i_23_, i_24_, i_26_, i_25_, true);
	}

	abstract Sprite createSprite(int[] is, int i, int i_76_, int i_77_, int i_78_, boolean bool);

	public abstract void da(int i, int i_146_, int i_147_, int[] is);

	public abstract void DA(int i, int i_27_, int i_28_, int i_29_);

	public final void destroy(int i) {
		do {
			Class98_Sub10_Sub8.aBooleanArray5579[id] = false;
			method1773();
			if (i == -1) {
				break;
			}
			H(24, 39, 35, null);
			break;
		} while (false);
	}

	public final void drawPlayerSquareDot(int i, int i_64_, int i_65_, int i_66_, byte i_67_, int i_68_) {
		fillRectangle(i_68_, i_65_, i, i_64_, i_66_, 1);
	}

	public abstract void drawRectangle(int i, int i_93_, int i_94_, int i_95_, int i_96_, int i_97_);

	abstract int E();

	public abstract void EA(int i, int i_156_, int i_157_, int i_158_);

	public abstract void F(int i, int i_42_);

	public abstract void fillRectangle(int i, int i_198_, int screenWidth, int screenHeight, int colour, int i_202_);

	@Override
	protected void finalize() {
		destroy(-1);
	}

	public abstract void getClip(int[] is);

	public abstract int getFarPlane();

	public abstract int getNearPlane();

	public abstract int[] getPixels(int i, int i_205_, int i_206_, int i_207_);

	public abstract void H(int i, int i_203_, int i_204_, int[] is);

	public abstract void HA(int i, int i_171_, int i_172_, int i_173_, int[] is);

	abstract int I();

	public abstract int JA(int i, int i_105_, int i_106_, int i_107_, int i_108_, int i_109_);

	abstract int M();

	public abstract int mergeFunctionMask(int i, int i_92_);

	abstract DepthBufferObject method1744(int i, int i_6_);

	public abstract void method1746(int i, int i_12_, int i_13_, int i_14_);

	abstract boolean method1747();

	public abstract void method1749(boolean bool);

	public abstract void method1751(int i, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_, int i_37_, int i_38_, int i_39_, int i_40_, int i_41_);

	abstract Matrix method1752();

	public final void method1753(int i, int i_43_, int i_44_, int i_45_, int i_46_) {
		if (i == 22294) {
			U(i_46_, i_45_, i_43_, i_44_, 1);
		}
	}

	public final void method1755(int i, int i_48_, int i_49_, int i_50_, int i_51_) {
		P(i_49_, i_48_, i_51_, i_50_, 1);
		if (i != 8479) {
			return;
		}
	}

	public abstract void method1756();

	public final void method1757(int i, int i_58_, int i_59_, int i_60_, int i_61_) {
		if (i_60_ <= 19) {
			setSun(-128, 0.4222986F, -0.8555362F, 0.64319474F, 0.16042754F, -0.00591929F);
		}
		za(i_61_, i_58_, i, i_59_, 1);
	}

	public abstract void method1761(boolean bool);

	public abstract void method1764(int i, int i_69_) throws Exception_Sub1;

	abstract boolean method1766();

	abstract boolean method1767();

	public abstract Class48 method1769(Class48 class48, Class48 class48_74_, float f, Class48 class48_75_);

	abstract boolean method1771();

	abstract RtInterfaceClip method1772(int i, int i_83_, int[] is, int[] is_84_);

	public abstract void method1773();

	public abstract void method1774(int i);

	public abstract void method1775(Class48 class48);

	public abstract void method1778(int i);

	abstract boolean method1780();

	public final void method1781(boolean bool, int i, int i_98_, int i_99_, int i_100_, int i_101_) {
		try {
			drawRectangle(i_100_, i_101_, i_98_, i, i_99_, 1);
			if (bool != true) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ha.SJ(" + bool + ',' + i + ',' + i_98_ + ',' + i_99_ + ',' + i_100_ + ',' + i_101_ + ')');
		}
	}

	public abstract void method1785(Class242 class242, int i);

	public abstract void method1786(Canvas canvas);

	abstract boolean method1788();

	public final void method1789(int i, int i_110_, int i_111_, int i_112_, int i_113_, int i_114_) {
		try {
			if (i_113_ != -10550) {
				method1825();
			}
			method1795(i_114_, i, i_112_, i_111_, i_110_, 1);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ha.GK(" + i + ',' + i_110_ + ',' + i_111_ + ',' + i_112_ + ',' + i_113_ + ',' + i_114_ + ')');
		}
	}

	public abstract void method1791(float f, float f_118_, float f_119_);

	public abstract Matrix method1793();

	public final void method1794(Rectangle[] rectangles, int i, int i_130_) throws Exception_Sub1 {
		try {

			this.a(rectangles, i, 0, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ha.AK(" + (rectangles != null ? "{...}" : "null") + ',' + i + ',' + i_130_ + ')');
		}
	}

	public abstract void method1795(int i, int i_131_, int i_132_, int i_133_, int i_134_, int i_135_);

	abstract Sprite method1797(int i, int i_139_, int i_140_, int i_141_, boolean bool);

	public abstract void method1798(int i);

	abstract Class62 method1799();

	abstract boolean method1800();

	public abstract void method1801(int[] is);

	abstract boolean method1802();

	public abstract Class48 method1803(int i, int i_148_, int i_149_, int i_150_, int i_151_, int i_152_);

	public abstract void method1806(int i);

	public abstract boolean method1810();

	public abstract void method1811(int i, int i_174_, int i_175_, int i_176_, int i_177_, int i_178_, int i_179_, int i_180_, int i_181_);

	public abstract void method1812();

	abstract Interface5 method1813(int i, int i_182_);

	public abstract void method1814();

	public abstract void method1816(int i, int i_192_, int i_193_, int i_194_, int i_195_, int i_196_, int i_197_);

	public abstract void method1817();

	public abstract void method1818(int i, PointLight[] class98_sub5s);

	public abstract boolean method1819();

	public abstract void method1820(Class242 class242);

	public abstract void method1820_cp(Class242 class69, ParticleDescriptor pDescriptor, int intensity, int ambient);

	public abstract int method1822();

	abstract boolean method1823();

	public abstract void method1825();

	abstract boolean needsNativeHeap();

	public abstract void P(int i, int i_88_, int i_89_, int i_90_, int i_91_);

	public abstract void pa();

	public abstract void Q(int i, int i_185_, int i_186_, int i_187_, int i_188_, int i_189_, byte[] is, int i_190_, int i_191_);

	public abstract int r(int i, int i_52_, int i_53_, int i_54_, int i_55_, int i_56_, int i_57_);

	public abstract void ra(int i, int i_19_, int i_20_, int i_21_);

	public final void renderFrame(int i) throws Exception_Sub1 {
		method1764(0, 0);
	}

	public abstract void resetRenderTarget();

	public abstract void resize(Canvas canvas, int width, int height);

	public abstract void setAmbientIntensity(float f);

	public abstract void setClip(int i, int i_153_, int i_154_, int i_155_);

	public abstract void setClipPlanes(int i, int i_22_);

	public abstract void setDepthWriteMask(boolean bool);

	public final void setRenderTarget(int i, Sprite class332) {
		try {
			if (i > -56) {
				getClip(null);
			}
			this.setRenderTarget(createRenderTarget(class332, method1744(class332.getWidth(), class332.getHeight())));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ha.UJ(" + i + ',' + (class332 != null ? "{...}" : "null") + ')');
		}
	}

	public abstract void setRenderTarget(RenderTarget renderTarget);

	public abstract void setSun(int i, float f, float f_79_, float f_80_, float f_81_, float f_82_);

	public abstract void switchCanvas(Canvas canvas);

	public abstract void U(int i, int i_15_, int i_16_, int i_17_, int i_18_);

	public abstract void X(int i);

	public abstract int[] Y();

	public abstract void ya();

	public abstract void za(int i, int i_142_, int i_143_, int i_144_, int i_145_);
}

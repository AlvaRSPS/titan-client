/* Class116 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;

public final class Class116 {
	public static HashTable				attachmentMap	= new HashTable(8);
	public static Class98_Sub31_Sub2	aClass98_Sub31_Sub2_965;
	public static int					anInt967;
	public static String[]				aStringArray966;

	public static final void method2159(byte i, RSToolkit var_ha) {
		do {
			if (!Class98_Sub5_Sub3.aBoolean5539) {
				ActionGroup.method1561(var_ha, -256);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			BackgroundColourLSEConfig.method2519(6, var_ha);
		} while (false);
	}

	public static final void setVarpBit(int value, int id, byte i_1_) {
		if (i_1_ == -120) {
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(id, -124, 1);
			class98_sub46_sub17.method1626((byte) -103);
			class98_sub46_sub17.anInt6054 = value;
		}
	}
}

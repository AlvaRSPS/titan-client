/* Class246_Sub3_Sub4_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.shadow.Shadow;

public final class Class246_Sub3_Sub4_Sub1 extends Class246_Sub3_Sub4 implements Interface19 {
	public static boolean	aBoolean6244	= false;
	public static int		anInt6241;

	public static final void method3025(byte i) {
		do {
			try {
				if ((client.preferences.removeRoofsMode.getValue((byte) 125) ^ 0xffffffff) == -3) {
					byte i_0_ = (byte) (RemoveRoofsPreferenceField.anInt3676 + -4 & 0xff);
					int i_1_ = RemoveRoofsPreferenceField.anInt3676 % Class165.mapWidth;
					for (int i_2_ = 0; i_2_ < 4; i_2_++) {
						for (int i_3_ = 0; Class98_Sub10_Sub7.mapLength > i_3_; i_3_++) {
							OutputStream_Sub2.aByteArrayArrayArray41[i_2_][i_1_][i_3_] = i_0_;
						}
					}
					if (Font.localPlane != 3) {
						int i_4_ = 0;
						if (i != 72) {
							method3029(-45);
						}
						for (/**/; (i_4_ ^ 0xffffffff) > -3; i_4_++) {
							Class204.anIntArray1551[i_4_] = -1000000;
							Class336.anIntArray2826[i_4_] = 1000000;
							Class287.anIntArray2195[i_4_] = 0;
							Class48_Sub1_Sub2.anIntArray5518[i_4_] = 1000000;
							Class295.anIntArray2409[i_4_] = 0;
						}
						int i_5_ = ((Char) Class87.localPlayer).boundExtentsX;
						int i_6_ = ((Char) Class87.localPlayer).boundExtentsZ;
						if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) == -2 || (Class116.anInt967 ^ 0xffffffff) != 0) {
							if (Class98_Sub46_Sub20_Sub2.cameraMode != 1) {
								i_6_ = CharacterShadowsPreferenceField.anInt3712;
								i_5_ = Class116.anInt967;
							}
							if ((Class281.flags[Font.localPlane][i_5_ >> 819933737][i_6_ >> -1734096311] & 0x4) != 0) {
								AsyncCache.method3175(i_5_ >> 302605065, 0, QuickChatCategory.aClass172ArrayArrayArray5948, i_6_ >> 68371017, (byte) -76, false);
							}
							if ((Mob.anInt6357 ^ 0xffffffff) > -2561) {
								int i_7_ = Class98_Sub46_Sub10.xCamPosTile >> 813546185;
								int i_8_ = SpriteLoadingScreenElement.yCamPosTile >> -794721367;
								int i_9_ = i_5_ >> 847752841;
								int i_10_ = i_6_ >> -43834903;
								int i_11_;
								if ((i_9_ ^ 0xffffffff) < (i_7_ ^ 0xffffffff)) {
									i_11_ = i_9_ + -i_7_;
								} else {
									i_11_ = -i_9_ + i_7_;
								}
								int i_12_;
								if ((i_8_ ^ 0xffffffff) <= (i_10_ ^ 0xffffffff)) {
									i_12_ = i_8_ - i_10_;
								} else {
									i_12_ = -i_8_ + i_10_;
								}
								if ((i_11_ ^ 0xffffffff) == -1 && i_12_ == 0 || (i_11_ ^ 0xffffffff) >= (-Class165.mapWidth ^ 0xffffffff) || (Class165.mapWidth ^ 0xffffffff) >= (i_11_ ^ 0xffffffff) || -Class98_Sub10_Sub7.mapLength >= i_12_ || i_12_ >= Class98_Sub10_Sub7.mapLength) {
									Class305_Sub1.reportError(null, -127, "RC: " + i_7_ + "," + i_8_ + " " + i_9_ + "," + i_10_ + " " + Class272.gameSceneBaseX + "," + aa_Sub2.gameSceneBaseY);
								} else if (i_11_ > i_12_) {
									int i_13_ = i_12_ * 65536 / i_11_;
									int i_14_ = 32768;
									while ((i_7_ ^ 0xffffffff) != (i_9_ ^ 0xffffffff)) {
										if ((i_9_ ^ 0xffffffff) >= (i_7_ ^ 0xffffffff)) {
											if (i_9_ < i_7_) {
												i_7_--;
											}
										} else {
											i_7_++;
										}
										if ((0x4 & Class281.flags[Font.localPlane][i_7_][i_8_]) != 0) {
											AsyncCache.method3175(i_7_, 1, QuickChatCategory.aClass172ArrayArrayArray5948, i_8_, (byte) -76, false);
											break;
										}
										i_14_ += i_13_;
										if ((i_14_ ^ 0xffffffff) <= -65537) {
											if ((i_10_ ^ 0xffffffff) < (i_8_ ^ 0xffffffff)) {
												i_8_++;
											} else if (i_8_ > i_10_) {
												i_8_--;
											}
											i_14_ -= 65536;
											if ((Class281.flags[Font.localPlane][i_7_][i_8_] & 0x4) != 0) {
												AsyncCache.method3175(i_7_, 1, QuickChatCategory.aClass172ArrayArrayArray5948, i_8_, (byte) -76, false);
												break;
											}
										}
									}
								} else {
									int i_15_ = 65536 * i_11_ / i_12_;
									int i_16_ = 32768;
									while (i_10_ != i_8_) {
										if ((i_10_ ^ 0xffffffff) >= (i_8_ ^ 0xffffffff)) {
											if ((i_8_ ^ 0xffffffff) < (i_10_ ^ 0xffffffff)) {
												i_8_--;
											}
										} else {
											i_8_++;
										}
										if ((Class281.flags[Font.localPlane][i_7_][i_8_] & 0x4) != 0) {
											AsyncCache.method3175(i_7_, 1, QuickChatCategory.aClass172ArrayArrayArray5948, i_8_, (byte) -76, false);
											break;
										}
										i_16_ += i_15_;
										if (i_16_ >= 65536) {
											if (i_9_ <= i_7_) {
												if ((i_7_ ^ 0xffffffff) < (i_9_ ^ 0xffffffff)) {
													i_7_--;
												}
											} else {
												i_7_++;
											}
											i_16_ -= 65536;
											if ((0x4 & Class281.flags[Font.localPlane][i_7_][i_8_]) != 0) {
												AsyncCache.method3175(i_7_, 1, QuickChatCategory.aClass172ArrayArrayArray5948, i_8_, (byte) -76, false);
												break;
											}
										}
									}
								}
							}
							break;
						}
						int i_17_ = StrongReferenceMCNode.getHeight(Font.localPlane, SpriteLoadingScreenElement.yCamPosTile, Class98_Sub46_Sub10.xCamPosTile, 24111);
						if (i_17_ + -AdvancedMemoryCache.anInt601 >= 3200 || (0x4 & Class281.flags[Font.localPlane][Class98_Sub46_Sub10.xCamPosTile >> 2064460777][SpriteLoadingScreenElement.yCamPosTile >> -1481382711]) == 0) {
							break;
						}
						AsyncCache.method3175(Class98_Sub46_Sub10.xCamPosTile >> 1855536521, 1, QuickChatCategory.aClass172ArrayArrayArray5948, SpriteLoadingScreenElement.yCamPosTile >> 510517769, (byte) -76, false);
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mb.M(" + i + ')');
			}
			break;
		} while (false);
	}

	public static void method3029(int i) {
		do {
			try {
				if (i == -1) {
					break;
				}
				method3025((byte) -65);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mb.L(" + i + ')');
			}
			break;
		} while (false);
	}

	private boolean		aBoolean6234;
	private boolean		aBoolean6235;
	private boolean		aBoolean6237;
	private boolean		aBoolean6239;
	private byte		aByte6232;
	private byte		aByte6233;
	ModelRenderer		aClass146_6243;
	private Class228	aClass228_6236;

	private Shadow		aR6242;

	private short		aShort6238;

	Class246_Sub3_Sub4_Sub1(RSToolkit var_ha, GameObjectDefinition class352, int i, int i_20_, int i_21_, int i_22_, int i_23_, boolean bool, int i_24_, int i_25_, int i_26_, int i_27_, int i_28_, int i_29_, boolean bool_30_) {
		super(i, i_20_, i_21_, i_22_, i_23_, i_24_, i_25_, i_26_, i_27_, class352.anInt2975 == 1, Class246_Sub3_Sub4_Sub5.method3088(i_28_, (byte) -41, i_29_));
		do {
			try {
				aBoolean6235 = bool_30_;
				aBoolean6237 = bool;
				aByte6233 = (byte) i_29_;
				aShort6238 = (short) class352.id;
				aBoolean6234 = (class352.anInt2998 ^ 0xffffffff) != -1 && !bool;
				((Char) this).collisionPlane = (byte) i_20_;
				aByte6232 = (byte) i_28_;
				aBoolean6239 = var_ha.method1771() && class352.aBoolean2935 && !aBoolean6237 && (client.preferences.sceneryShadows.getValue((byte) 127) ^ 0xffffffff) != -1;
				int i_31_ = 2048;
				if (aBoolean6235) {
					i_31_ |= 0x10000;
				}
				Class298 class298 = method3028(i_31_, (byte) 95, var_ha, aBoolean6239);
				if (class298 == null) {
					break;
				}
				aR6242 = class298.aR2479;
				aClass146_6243 = class298.aClass146_2477;
				if (!aBoolean6235) {
					break;
				}
				aClass146_6243 = aClass146_6243.method2341((byte) 0, i_31_, false);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mb.<init>(" + (var_ha != null ? "{...}" : "null") + ',' + (class352 != null ? "{...}" : "null") + ',' + i + ',' + i_20_ + ',' + i_21_ + ',' + i_22_ + ',' + i_23_ + ',' + bool + ',' + i_24_ + ',' + i_25_ + ',' + i_26_ + ','
						+ i_27_ + ',' + i_28_ + ',' + i_29_ + ',' + bool_30_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (i != -53) {
				return null;
			}
			if (aClass228_6236 == null) {
				aClass228_6236 = Class48_Sub2_Sub1.method472(((Char) this).anInt5089, ((Char) this).boundExtentsX, method3026(-128, 0, var_ha), ((Char) this).boundExtentsZ, i ^ ~0x30);
			}
			return aClass228_6236;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			if (aClass146_6243 == null) {
				return null;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
			if (i >= -12) {
				method2992((byte) -76);
			}
			Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, aBoolean6234);
			do {
				if (VarClientStringsDefinitionParser.aBoolean1839) {
					aClass146_6243.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				aClass146_6243.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
			} while (false);
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_32_, int i_33_) {
		try {
			if (i_32_ <= 59) {
				aBoolean6234 = false;
			}
			ModelRenderer class146 = method3026(-120, 131072, var_ha);
			if (class146 != null) {
				Matrix class111 = var_ha.method1793();
				class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
				if (!VarClientStringsDefinitionParser.aBoolean1839) {
					return class146.method2339(i, i_33_, class111, false, 0);
				}
				return class146.method2333(i, i_33_, class111, false, 0, Class16.anInt197);
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_32_ + ',' + i_33_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			if (aClass146_6243 != null) {
				if (aClass146_6243.r()) {
					return false;
				}
				return true;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.H(" + i + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_37_, RSToolkit var_ha, int i_38_, int i_39_) {
		try {
			do {
				if (class246_sub3 instanceof Class246_Sub3_Sub3_Sub2) {
					Class246_Sub3_Sub3_Sub2 class246_sub3_sub3_sub2 = (Class246_Sub3_Sub3_Sub2) class246_sub3;
					if (aClass146_6243 == null || class246_sub3_sub3_sub2.aClass146_6285 == null) {
						break;
					}
					aClass146_6243.method2332(class246_sub3_sub3_sub2.aClass146_6285, i_37_, i_38_, i_39_, bool);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if (class246_sub3 instanceof Class246_Sub3_Sub4_Sub1) {
					Class246_Sub3_Sub4_Sub1 class246_sub3_sub4_sub1_40_ = (Class246_Sub3_Sub4_Sub1) class246_sub3;
					if (aClass146_6243 != null && class246_sub3_sub4_sub1_40_.aClass146_6243 != null) {
						aClass146_6243.method2332(class246_sub3_sub4_sub1_40_.aClass146_6243, i_37_, i_38_, i_39_, bool);
					}
				}
			} while (false);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_37_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_38_ + ',' + i_39_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i >= -70) {
				anInt6241 = -94;
			}
			return aBoolean6235;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.HA(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				return -120;
			}
			if (aClass146_6243 != null) {
				return aClass146_6243.ma();
			}
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (aClass146_6243 != null) {
				return aClass146_6243.F();
			}
			if (i != 6540) {
				method2985(true);
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		try {

		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				aClass146_6243 = null;
			}
			if (aClass146_6243 == null) {
				return 0;
			}
			return aClass146_6243.fa();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.J(" + i + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		do {
			try {
				if (i != -73) {
					method2992((byte) -74);
				}
				aBoolean6235 = false;
				if (aClass146_6243 == null) {
					break;
				}
				aClass146_6243.updateFunctionMask(~0x10000 & aClass146_6243.functionMask());
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mb.DA(" + i + ')');
			}
			break;
		} while (false);
	}

	private final ModelRenderer method3026(int i, int i_19_, RSToolkit var_ha) {
		try {
			if (aClass146_6243 != null && (var_ha.c(aClass146_6243.functionMask(), i_19_) ^ 0xffffffff) == -1) {
				return aClass146_6243;
			}
			if (i >= -20) {
				return null;
			}
			Class298 class298 = method3028(i_19_, (byte) 123, var_ha, false);
			if (class298 != null) {
				return class298.aClass146_2477;
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.P(" + i + ',' + i_19_ + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	public final int method3027(int i) {
		try {
			if (i <= 20) {
				if (aClass146_6243 != null) {
					return aClass146_6243.na() / 4;
				}
			}
			return 15;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.K(" + i + ')');
		}
	}

	private final Class298 method3028(int i, byte i_35_, RSToolkit var_ha, boolean bool) {
		try {
			if (i_35_ < 69) {
				method61((byte) -69);
			}
			GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(aShort6238 & 0xffff, (byte) 119);
			Ground var_s;
			Ground var_s_36_;
			if (!aBoolean6237) {
				if ((((Char) this).collisionPlane ^ 0xffffffff) <= -4) {
					var_s = null;
				} else {
					var_s = StrongReferenceMCNode.aSArray6298[1 + ((Char) this).collisionPlane];
				}
				var_s_36_ = StrongReferenceMCNode.aSArray6298[((Char) this).collisionPlane];
			} else {
				var_s = StrongReferenceMCNode.aSArray6298[0];
				var_s_36_ = Class81.aSArray618[((Char) this).collisionPlane];
			}
			return class352.method3851(((Char) this).boundExtentsZ, false, var_s, (aByte6232 ^ 0xffffffff) == -12 ? (int) (aByte6233 + 4) : aByte6233, ((Char) this).anInt5089, bool, ((Char) this).boundExtentsX, i, null, var_s_36_, var_ha, aByte6232 != 11 ? aByte6232 : 10);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.D(" + i + ',' + i_35_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	@Override
	public final void method61(byte i) {
		do {
			try {
				if (aClass146_6243 != null) {
					aClass146_6243.method2326();
				}
				if (i == -96) {
					break;
				}
				aBoolean6234 = false;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mb.Q(" + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method62(RSToolkit var_ha, int i) {
		do {
			try {
				Shadow var_r;
				if (aR6242 != null || !aBoolean6239) {
					var_r = aR6242;
					aR6242 = null;
				} else {
					Class298 class298 = method3028(262144, (byte) 85, var_ha, true);
					var_r = class298 != null ? class298.aR2479 : null;
				}
				if (i != 24447) {
					method2990(94);
				}
				if (var_r == null) {
					break;
				}
				Class184.method2626(var_r, ((Char) this).collisionPlane, ((Char) this).boundExtentsX, ((Char) this).boundExtentsZ, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mb.G(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int method63(byte i) {
		try {
			if (i != 20) {
				method2992((byte) 9);
			}
			return aByte6232;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.B(" + i + ')');
		}
	}

	@Override
	public final int method64(int i) {
		try {
			if (i != 30472) {
				method2981(null, (byte) 46, false, -115, null, -54, -101);
			}
			return 0xffff & aShort6238;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.C(" + i + ')');
		}
	}

	@Override
	public final boolean method65(boolean bool) {
		try {
			if (bool != true) {
				aClass228_6236 = null;
			}
			return aBoolean6239;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.A(" + bool + ')');
		}
	}

	@Override
	public final int method66(int i) {
		try {
			if (i != 4657) {
				return 91;
			}
			return aByte6233;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mb.N(" + i + ')');
		}
	}

	@Override
	public final void method67(int i, RSToolkit var_ha) {
		do {
			try {
				Shadow var_r;
				if (aR6242 == null && aBoolean6239) {
					Class298 class298 = method3028(262144, (byte) 101, var_ha, true);
					var_r = class298 == null ? null : class298.aR2479;
				} else {
					var_r = aR6242;
					aR6242 = null;
				}
				if (var_r != null) {
					Class268.method3254(var_r, ((Char) this).collisionPlane, ((Char) this).boundExtentsX, ((Char) this).boundExtentsZ, null);
				}
				if (i == -25163) {
					break;
				}
				aBoolean6235 = true;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mb.E(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}
}

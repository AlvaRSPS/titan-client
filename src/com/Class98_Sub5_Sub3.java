
/* Class98_Sub5_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.toolkit.matrix.Matrix;

public final class Class98_Sub5_Sub3 extends PointLight {
	public static boolean	aBoolean5539	= false;
	public static Matrix	aClass111_5540;
	public static int		anInt5538;

	static {
		anInt5538 = 1;
	}

	public static final int method971(int i, int i_0_, int i_1_, int i_2_) {
		try {
			if (i_2_ > -39) {
				return -27;
			}
			int i_3_ = i / i_1_;
			int i_4_ = i & i_1_ - 1;
			int i_5_ = i_0_ / i_1_;
			int i_6_ = i_0_ & i_1_ - 1;
			int i_7_ = NativeOpenGlElementArrayBuffer.method2499(19, i_5_, i_3_);
			int i_8_ = NativeOpenGlElementArrayBuffer.method2499(-117, i_5_, i_3_ + 1);
			int i_9_ = NativeOpenGlElementArrayBuffer.method2499(84, i_5_ + 1, i_3_);
			int i_10_ = NativeOpenGlElementArrayBuffer.method2499(19, 1 + i_5_, i_3_ + 1);
			int i_11_ = ClientScript2Event.method1180(i_1_, (byte) -38, i_7_, i_4_, i_8_);
			int i_12_ = ClientScript2Event.method1180(i_1_, (byte) 107, i_9_, i_4_, i_10_);
			return ClientScript2Event.method1180(i_1_, (byte) 104, i_11_, i_6_, i_12_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lh.L(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public static void method973(int i) {
		do {
			try {
				aClass111_5540 = null;
				if (i == 1) {
					break;
				}
				aBoolean5539 = false;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "lh.N(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method974(int i, byte i_20_) {
		try {
			return !(i != 7 && (i ^ 0xffffffff) != -10);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lh.M(" + i + ',' + i_20_ + ')');
		}
	}

	Class98_Sub5_Sub3(int i, int i_16_, int i_17_, int i_18_, int i_19_, float f) {
		super(i, i_16_, i_17_, i_18_, i_19_, f);
	}

	@Override
	public final void reposition(int i, byte i_21_, int i_22_, int i_23_) {
		try {
			this._z = i;
			this._x = i_22_;
			this._y = i_23_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lh.A(" + i + ',' + i_21_ + ',' + i_22_ + ',' + i_23_ + ')');
		}
	}

	@Override
	public final void setIntensity(float f, int i) {
		do {
			try {
				this.intensity = f;
				if (i > 12) {
					break;
				}
				aClass111_5540 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "lh.D(" + f + ',' + i + ')');
			}
			break;
		} while (false);
	}
}

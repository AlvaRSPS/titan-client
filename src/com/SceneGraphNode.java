/* Class246 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.archive.Js5Manager;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public class SceneGraphNode {
	public static int		anInt1871;
	public static int		anInt1872	= 0;
	public static short[]	clientPalette;

	public static final void method2962(boolean bool) {
		Class154.aHa1231 = null;
		Class98_Sub5_Sub3.aClass111_5540 = null;
		Class224_Sub2_Sub1.anInt6141 = -1;
		if (bool == false) {
			aa_Sub1.anInt3558 = -1;
			Cacheable.anInt4261 = -1;
			SocketWrapper.anInterface17_301 = null;
			OpenGlModelRenderer.anIntArray4873 = null;
			Class42_Sub3.aClass111_5364 = null;
			Js5Manager.anInt926 = -1;
			Class200.aClass111_1543 = null;
			Class172.anInterface17Array1327 = null;
			SunDefinition.aClass84_1988.method833(0);
		}
	}

	public static void method2963(int i) {
		if (i != -1) {
			ParticleManager.particleDebugEnabled = true;
		}
		clientPalette = null;
	}

	SceneGraphNode	next;

	SceneGraphNode	previous;

	public SceneGraphNode() {
		/* empty */
	}

	public final void unlink(byte i) {
		if (previous != null) {
			previous.next = next;
			next.previous = previous;
			next = null;
			previous = null;
		}
	}
}

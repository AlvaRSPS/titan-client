/* Class98_Sub46_Sub15 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.preferences.BuildAreaPreferenceField;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;

public final class Class98_Sub46_Sub15 extends Cacheable {
	public static Class232			aClass232_6043		= new Class232();
	public static int				anInt6039			= -1;
	public static IncomingOpcode	SEND_WINDOW_PANE	= new IncomingOpcode(36, 3);

	public static final int getProgress(boolean bool, byte i) {
		if (Class2.fontIds == null) {
			return 0;
		}
		if (!bool && Class242.fonts != null) {
			return Class2.fontIds.length * 2;
		}
		int progress = 0;
		for (int fontId = 0; (Class2.fontIds.length ^ 0xffffffff) < (fontId ^ 0xffffffff); fontId++) {
			int fileId = Class2.fontIds[fontId];
			if (MaxScreenSizePreferenceField.fontmetricsStore.isFileCached(i + -161, fileId)) {
				progress++;
			}
			if (BuildAreaPreferenceField.glyphsStore.isFileCached(i ^ ~0x4d, fileId)) {
				progress++;
			}
		}
		return progress;
	}

	public static final void method1609(int i, int i_0_) {
		if (Class85.loadInterface(i, 11) && i_0_ == -12889) {
			RtInterface[] class293s = Class159.interfaceStore[i];
			for (int index = 0; (class293s.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				RtInterface class293 = class293s[index];
				if (class293 != null) {
					class293.anInt2303 = 0;
					class293.anInt2312 = 0;
					class293.anInt2287 = 1;
				}
			}
		}
	}

	public static final boolean method1611(byte i, char c) {
		return !(((c ^ 0xffffffff) > -66 || (c ^ 0xffffffff) < -91) && ((c ^ 0xffffffff) > -98 || (c ^ 0xffffffff) < -123));
	}

	double		aDouble6042;

	short[][]	aShortArrayArray6040;

	Class98_Sub46_Sub15(short[][] is, double d) {
		aShortArrayArray6040 = is;
		aDouble6042 = d;
	}

	public final long method1608(int i) {
		return aShortArrayArray6040[0].length | aShortArrayArray6040.length << -2104658688;
	}
}

/* Class284_Sub2_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class284_Sub2_Sub2 extends Class284_Sub2 {
	public static OutgoingOpcode	aClass171_6198	= new OutgoingOpcode(54, 7);
	public static Sprite			aClass332_6199;
	public static IncomingOpcode	aClass58_6197	= new IncomingOpcode(56, 10);
	public static short				aShort6201		= 1;
	public static int[]				COSINE;
	public static int[]				SINE;

	static {
		SINE = new int[16384];
		COSINE = new int[16384];
		double d = 3.834951969714103E-4;
		for (int i = 0; (i ^ 0xffffffff) > -16385; i++) {
			SINE[i] = (int) (16384.0 * Math.sin(i * d));
			COSINE[i] = (int) (Math.cos(d * i) * 16384.0);
		}
	}

	private byte[] aByteArray6196;

	public Class284_Sub2_Sub2() {
		super(8, 5, 8, 8, 2, 0.1F, 0.55F, 3.0F);
	}

	@Override
	public final void method3375(int i, int i_0_, byte i_1_) {
		try {
			int i_3_ = i * 2;
			aByteArray6196[i_3_++] = (byte) -1;
			int i_4_ = 0xff & i_1_;
			aByteArray6196[i_3_] = (byte) (3 * i_4_ >> 737825957);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kh.L(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public final byte[] method3379(int i, int i_5_, int i_6_, int i_7_) {
		try {
			if (i_5_ != 20283) {
				return null;
			}
			aByteArray6196 = new byte[2 * i_7_ * i_6_ * i];
			method3361((byte) -116, i_7_, i_6_, i);
			return aByteArray6196;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kh.M(" + i + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ')');
		}
	}
}

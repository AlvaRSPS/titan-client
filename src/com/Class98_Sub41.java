/* Class98_Sub41 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.preferences.GamePreferences;

public final class Class98_Sub41 extends Node {
	public static IncomingOpcode	aClass58_4199;
	public static float				aFloat4204;
	public static int				anInt4202;
	public static Object			anObject4203;

	static {
		aClass58_4199 = new IncomingOpcode(20, 7);
	}

	public static final void decodeMapArea(int dummy) {
		Class151_Sub9.anInt5028 = 0;
		int centreY = PacketParser.buffer.readLEShortA((byte) 104);
		if (dummy >= 4) {
			int centreX = PacketParser.buffer.readShortA(121);
			int regionSize = PacketParser.buffer.readUnsignedByte((byte) 80);
			boolean force = (PacketParser.buffer.readByteA(true) ^ 0xffffffff) == -2;
			Class98_Sub10_Sub13.method1043((byte) -65);
			GamePreferences.setSize((byte) 8, regionSize);
			int regionCount = (-PacketParser.buffer.position + Class65.currentPacketSize) / 16;
			// System.out.println(regionSize + "Count: " + regionCount + "
			// CentreX: " + centreX + " CentreY: " + centreY);
			Class98_Sub46_Sub17.regionXteaKey = new int[regionCount][4];
			for (int regPtr = 0; (regionCount ^ 0xffffffff) < (regPtr ^ 0xffffffff); regPtr++) {
				for (int keyPtr = 0; (keyPtr ^ 0xffffffff) > -5; keyPtr++) {
					Class98_Sub46_Sub17.regionXteaKey[regPtr][keyPtr] = PacketParser.buffer.readInt(-2);
				}
			}
			Class105.regionNpcMapData = null;
			HitmarksDefinitionParser.regionPositionHash = new int[regionCount];
			client.unknown = new byte[regionCount][];
			HashTable.regionUwObjectMapId = new int[regionCount];
			Class287.regionTerrainId = new int[regionCount];
			Player.aByteArrayArray6533 = new byte[regionCount][];
			Class255.aByteArrayArray3211 = new byte[regionCount][];
			HashTable.aByteArrayArray3182 = new byte[regionCount][];
			Class98_Sub36.regionObjectMapId = new int[regionCount];
			Class76_Sub7.anIntArray3765 = null;
			GameObjectDefinitionParser.regionUwTerrainId = new int[regionCount];
			regionCount = 0;
			for (int x = (-(Class165.mapWidth >> 4) + centreX) / 8; ((Class165.mapWidth >> 4) + centreX) / 8 >= x; x++) {
				for (int y = (centreY - (Class98_Sub10_Sub7.mapLength >> 4)) / 8; (centreY + (Class98_Sub10_Sub7.mapLength >> 4)) / 8 >= y; y++) {
					HitmarksDefinitionParser.regionPositionHash[regionCount] = (x << 8) + y;
					Class287.regionTerrainId[regionCount] = Class234.mapsJs5.getGroupId((byte) -90, "m" + x + "_" + y);
					Class98_Sub36.regionObjectMapId[regionCount] = Class234.mapsJs5.getGroupId((byte) -114, "l" + x + "_" + y);
					GameObjectDefinitionParser.regionUwTerrainId[regionCount] = Class234.mapsJs5.getGroupId((byte) -77, "um" + x + "_" + y);
					HashTable.regionUwObjectMapId[regionCount] = Class234.mapsJs5.getGroupId((byte) -61, "ul" + x + "_" + y);
					regionCount++;
				}
			}
			Class251.updateMapArea(-6547, centreY, force, centreX, 11);
		}
	}

	public static void method1474(boolean bool) {
		do {
			try {
				anObject4203 = null;
				aClass58_4199 = null;
				if (bool == false) {
					break;
				}
				aClass58_4199 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qda.B(" + bool + ')');
			}
			break;
		} while (false);
	}

	public String aString4201;

	public Class98_Sub41() {
		/* empty */
	}

	public Class98_Sub41(String string, int i) {
		try {
			aString4201 = string;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qda.<init>(" + (string != null ? "{...}" : "null") + ',' + i + ')');
		}
	}
}

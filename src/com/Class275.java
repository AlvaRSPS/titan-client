/* Class275 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.input.RtMouseEvent;

public final class Class275 {
	public static AdvancedMemoryCache	aClass79_2046	= new AdvancedMemoryCache(4);
	public static int[]					anIntArray2048	= new int[13];
	public static int					centreY;

	public static void method3282(byte i) {
		try {
			anIntArray2048 = null;
			if (i != -53) {
				method3282((byte) 8);
			}
			aClass79_2046 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rc.A(" + i + ')');
		}
	}

	public static final Class53_Sub1 method3283(byte i, int i_0_) {
		try {
			if (!RtMouseEvent.aBoolean3944 || (i_0_ ^ 0xffffffff) > (Class164.anInt1274 ^ 0xffffffff) || i_0_ > GrandExchangeOffer.anInt854) {
				return null;
			}
			if (i <= 112) {
				centreY = 95;
			}
			return OpenGlTileMaterial.aClass53_Sub1Array3967[-Class164.anInt1274 + i_0_];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rc.B(" + i + ',' + i_0_ + ')');
		}
	}
}

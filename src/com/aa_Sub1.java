/* aa_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

public final class aa_Sub1 extends RtInterfaceClip {
	public static boolean[][]		aBooleanArrayArray3560;
	public static StreamHandler		aClass123_3561;
	public static OutgoingOpcode	aClass171_3559	= new OutgoingOpcode(48, 2);
	public static IncomingOpcode	aClass58_3554	= new IncomingOpcode(94, -1);
	public static int				anInt3556;
	public static int				anInt3558		= -1;

	public static final Class42_Sub1_Sub1 method153(byte i, int i_0_, OpenGlToolkit var_ha_Sub1, int i_1_, int i_2_, int i_3_) {
		if (var_ha_Sub1.aBoolean4426 || Class81.method815(i_1_, 0) && Class81.method815(i_0_, 0)) {
			return new Class42_Sub1_Sub1(var_ha_Sub1, 3553, i_2_, i_3_, i_1_, i_0_, true);
		}
		if (!var_ha_Sub1.haveArbTextureRectangle) {
			return new Class42_Sub1_Sub1(var_ha_Sub1, i_2_, i_3_, i_1_, i_0_, Class48.findNextGreaterPwr2(423660257, i_1_), Class48.findNextGreaterPwr2(423660257, i_0_), true);
		}
		return new Class42_Sub1_Sub1(var_ha_Sub1, 34037, i_2_, i_3_, i_1_, i_0_, true);
	}

	public static final void method155(int i) {
		do {
			if (Class42_Sub4.anInt5371 < 0) {
				Class42_Sub4.anInt5371 = 0;
				GrandExchangeOffer.anInt849 = -1;
				Class169.anInt1307 = -1;
			}
			if (Class42_Sub4.anInt5371 > WorldMap.anInt2089) {
				Class169.anInt1307 = -1;
				GrandExchangeOffer.anInt849 = -1;
				Class42_Sub4.anInt5371 = WorldMap.anInt2089;
			}
			if ((NodeShort.anInt4197 ^ 0xffffffff) > i) {
				Class169.anInt1307 = -1;
				GrandExchangeOffer.anInt849 = -1;
				NodeShort.anInt4197 = 0;
			}
			if ((NodeShort.anInt4197 ^ 0xffffffff) >= (WorldMap.anInt2084 ^ 0xffffffff)) {
				break;
			}
			GrandExchangeOffer.anInt849 = -1;
			NodeShort.anInt4197 = WorldMap.anInt2084;
			Class169.anInt1307 = -1;
			break;
		} while (false);
	}

	public int[]	anIntArray3555;

	public int[]	anIntArray3557;

	aa_Sub1(int i, int i_5_, int[] is, int[] is_6_) {
		anIntArray3557 = is_6_;
		anIntArray3555 = is;
	}
}

/* Class98_Sub46_Sub20_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class98_Sub46_Sub20_Sub2 extends Class98_Sub46_Sub20 {
	public static SceneGraphNodeList[]	aClass218Array6316	= new SceneGraphNodeList[5];
	public static int					anInt6317;
	public static int[]					overlayFaces;
	public static int					cameraMode;

	static {
		for (int i = 0; i < aClass218Array6316.length; i++) {
			aClass218Array6316[i] = new SceneGraphNodeList();
		}
		overlayFaces = new int[] { 2, 1, 1, 1, 2, 2, 2, 1, 3, 3, 3, 2, 0, 4, 0 };
	}

	private Object anObject6315;

	Class98_Sub46_Sub20_Sub2(CacheKey interface20, Object object, int i) {
		super(interface20, i);
		try {
			anObject6315 = object;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "to.<init>(" + (interface20 != null ? "{...}" : "null") + ',' + (object != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final Object method1635(int i) {
		try {
			return anObject6315;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "to.A(" + i + ')');
		}
	}

	@Override
	public final boolean method1638(int i) {
		try {
			if (i != 896) {
				return false;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "to.B(" + i + ')');
		}
	}
}

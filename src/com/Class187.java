/* Class187 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;

public final class Class187 {
	public static boolean			aBoolean1451	= false;
	public static SignLinkRequest	aClass143_1449;
	public static int				anInt1450;

	public static final void method2634(int i, RtInterface[] interfaces, int i_0_) {
		int index = i;
		for (/**/; (interfaces.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
			RtInterface interf = interfaces[index];
			if (interf != null) {
				if ((interf.type ^ 0xffffffff) == -1) {
					if (interf.dynamicComponents != null) {
						method2634(i, interf.dynamicComponents, i_0_);
					}
					RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(interf.idDword, -1);
					if (class98_sub18 != null) {
						Js5.method2764(i_0_, class98_sub18.attachedInterfaceId, -105);
					}
				}
				if ((i_0_ ^ 0xffffffff) == -1 && interf.anObjectArray2330 != null) {
					ClientScript2Event class98_sub21 = new ClientScript2Event();
					class98_sub21.param = interf.anObjectArray2330;
					class98_sub21.component = interf;
					ClientScript2Runtime.handleEvent(class98_sub21);
				}
				if ((i_0_ ^ 0xffffffff) == -2 && interf.anObjectArray2319 != null) {
					if ((interf.componentIndex ^ 0xffffffff) <= -1) {
						RtInterface class293_2_ = RtInterface.getInterface(interf.idDword, -9820);
						if (class293_2_ == null || class293_2_.dynamicComponents == null || (class293_2_.dynamicComponents.length ^ 0xffffffff) >= (interf.componentIndex ^ 0xffffffff) || class293_2_.dynamicComponents[interf.componentIndex] != interf) {
							continue;
						}
					}
					ClientScript2Event class98_sub21 = new ClientScript2Event();
					class98_sub21.component = interf;
					class98_sub21.param = interf.anObjectArray2319;
					ClientScript2Runtime.handleEvent(class98_sub21);
				}
			}
		}
	}

	public static final boolean method2636(int i, int i_3_, int i_4_) {
		if (i_4_ != 3) {
			return true;
		}
		if (Class151_Sub2.method2451(i, 544, i_3_) | (i & 0x10000 ^ 0xffffffff) != -1 || WaterDetailPreferenceField.method670(i, -12294, i_3_)) {
			return true;
		}
		return !((0x37 & i_3_ ^ 0xffffffff) != -1 || !Class228.method2864(55, i, i_3_));
	}

	public static final void method2637(int i, int i_5_, int i_6_, byte[] is, byte[] is_7_, int i_8_, int i_9_, int i_10_, int i_11_) {
		int i_12_ = -(i_11_ >> 14184194);
		i_11_ = -(0x3 & i_11_);
		if (i_8_ != -18305) {
			method2634(8, null, 105);
		}
		for (int i_13_ = -i; (i_13_ ^ 0xffffffff) > -1; i_13_++) {
			for (int i_14_ = i_12_; (i_14_ ^ 0xffffffff) > -1; i_14_++) {
				is_7_[i_5_++] += -is[i_6_++];
				is_7_[i_5_++] += -is[i_6_++];
				is_7_[i_5_++] += -is[i_6_++];
				is_7_[i_5_++] += -is[i_6_++];
			}
			for (int i_15_ = i_11_; (i_15_ ^ 0xffffffff) > -1; i_15_++) {
				is_7_[i_5_++] += -is[i_6_++];
			}
			i_6_ += i_9_;
			i_5_ += i_10_;
		}
	}
}

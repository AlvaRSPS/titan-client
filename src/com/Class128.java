/* Class128 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class128 {
	public static int anInt1025 = -1;

	public static final boolean method2223(int i, byte i_0_, int i_1_) {
		try {
			if (i_0_ != -67) {
				method2223(102, (byte) -23, -84);
			}
			return (0x180 & i_1_) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "il.B(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static final void method2224(int i) {
		do {
			try {
				if (SystemInformation.aClass268_1173 != null) {
					SystemInformation.aClass268_1173.method3261((byte) -128);
				}
				if (Class27.aClass268_276 != null) {
					Class27.aClass268_276.method3261((byte) -78);
				}
				if (i == 22696) {
					break;
				}
				method2224(-42);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "il.A(" + i + ')');
			}
			break;
		} while (false);
	}

	public Class128() {
		/* empty */
	}
}

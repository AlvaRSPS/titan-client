/* Class98_Sub33 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.FlickeringEffectsPreferenceField;

public final class Class98_Sub33 extends Node {
	public static int anInt4117 = 0;

	public static final void method2428(Class98_Sub33 class98_sub33, boolean bool, int i) {
		do {
			if (class98_sub33.aBoolean4124) {
				if (class98_sub33.anInt4119 < 0 || Class358.method3889(class98_sub33.anInt4115, class98_sub33.anInt4119, false)) {
					if (!bool) {
						Canvas_Sub1.method119(class98_sub33.anInt4119, class98_sub33.localZ, class98_sub33.anInt4118, class98_sub33.anInt4121, class98_sub33.anInt4116, i ^ ~0x3d50, class98_sub33.localX, -1, class98_sub33.anInt4115);
					} else {
						FlickeringEffectsPreferenceField.method601(null, class98_sub33.localZ, class98_sub33.anInt4116, class98_sub33.localX, class98_sub33.anInt4118, 6093);
					}
					class98_sub33.unlink(109);
				}
			} else {
				if (!class98_sub33.aBoolean4123 || class98_sub33.localX < 1 || (class98_sub33.localZ ^ 0xffffffff) > -2 || Class165.mapWidth - 2 < class98_sub33.localX || (Class98_Sub10_Sub7.mapLength + -2 ^ 0xffffffff) > (class98_sub33.localZ ^ 0xffffffff)) {
					break;
				}
				if ((class98_sub33.anInt4114 ^ 0xffffffff) > -1 || Class358.method3889(class98_sub33.anInt4122, class98_sub33.anInt4114, false)) {
					if (!bool) {
						Canvas_Sub1.method119(class98_sub33.anInt4114, class98_sub33.localZ, class98_sub33.anInt4118, class98_sub33.anInt4120, class98_sub33.anInt4116, -2, class98_sub33.localX, -1, class98_sub33.anInt4122);
					} else {
						FlickeringEffectsPreferenceField.method601(class98_sub33.aClass185_4125, class98_sub33.localZ, class98_sub33.anInt4116, class98_sub33.localX, class98_sub33.anInt4118, 6093);
					}
					class98_sub33.aBoolean4123 = false;
					if (!bool && class98_sub33.anInt4119 == class98_sub33.anInt4114 && class98_sub33.anInt4119 == -1) {
						class98_sub33.unlink(45);
					} else {
						if (bool || (class98_sub33.anInt4114 ^ 0xffffffff) != (class98_sub33.anInt4119 ^ 0xffffffff) || class98_sub33.anInt4121 != class98_sub33.anInt4120 || (class98_sub33.anInt4115 ^ 0xffffffff) != (class98_sub33.anInt4122 ^ 0xffffffff)) {
							break;
						}
						class98_sub33.unlink(86);
					}
				}
			}
			break;
		} while (false);
	}

	public static final void method591(int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		Class98_Sub33 class98_sub33 = null;
		for (Class98_Sub33 class98_sub33_9_ = (Class98_Sub33) Class191.aClass148_1478.getFirst(32); class98_sub33_9_ != null; class98_sub33_9_ = (Class98_Sub33) Class191.aClass148_1478.getNext(122)) {
			if (class98_sub33_9_.anInt4116 == i_8_ && class98_sub33_9_.localX == i_2_ && class98_sub33_9_.localZ == i_4_ && i_5_ == class98_sub33_9_.anInt4118) {
				class98_sub33 = class98_sub33_9_;
				break;
			}
		}
		if (class98_sub33 == null) {
			class98_sub33 = new Class98_Sub33();
			class98_sub33.localZ = i_4_;
			class98_sub33.anInt4118 = i_5_;
			class98_sub33.anInt4116 = i_8_;
			class98_sub33.localX = i_2_;
			if (i_2_ >= 0 && i_4_ >= 0 && i_2_ < Class165.mapWidth && i_4_ < Class98_Sub10_Sub7.mapLength) {
				ActionGroup.method1558((byte) 109, class98_sub33);
			}
			Class191.aClass148_1478.addLast(class98_sub33, -20911);
		}
		class98_sub33.anInt4122 = i_3_;
		class98_sub33.aBoolean4124 = false;
		class98_sub33.anInt4114 = i_6_;
		class98_sub33.aBoolean4123 = true;
		class98_sub33.anInt4120 = i_7_;
	}

	public static final void method789(int i) {
		try {
			for (Class98_Sub33 class98_sub33 = (Class98_Sub33) Class191.aClass148_1478.getFirst(32); class98_sub33 != null; class98_sub33 = (Class98_Sub33) Class191.aClass148_1478.getNext(94)) {
				if (class98_sub33.aBoolean4124) {
					class98_sub33.unlink(79);
				} else {
					class98_sub33.aBoolean4123 = true;
					if (class98_sub33.localX >= 0 && class98_sub33.localZ >= 0 && (class98_sub33.localX ^ 0xffffffff) > (Class165.mapWidth ^ 0xffffffff) && (class98_sub33.localZ ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff)) {
						ActionGroup.method1558((byte) 109, class98_sub33);
					}
				}
			}
			for (Class98_Sub33 class98_sub33 = (Class98_Sub33) OutgoingPacket.aClass148_3866.getFirst(32); class98_sub33 != null; class98_sub33 = (Class98_Sub33) OutgoingPacket.aClass148_3866.getNext(98)) {
				if (!class98_sub33.aBoolean4124) {
					class98_sub33.aBoolean4123 = true;
				} else {
					class98_sub33.unlink(53);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oca.M(" + i + ')');
		}
	}

	public boolean	aBoolean4123;
	public boolean	aBoolean4124	= false;
	public Class185	aClass185_4125;
	public int		anInt4114;
	public int		anInt4115;
	public int		anInt4116;
	public int		anInt4118;
	public int		anInt4119;
	public int		anInt4120;
	public int		anInt4121;

	public int		anInt4122;

	public int		localX;

	public int		localZ;

	public Class98_Sub33() {
		aBoolean4123 = true;
	}
}

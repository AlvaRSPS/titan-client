/* Class16 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;

public final class Class16 {
	public static IncomingOpcode	aClass58_191	= new IncomingOpcode(45, 0);
	public static int				anInt190		= -1;
	public static int				anInt197;
	public static int				orthoZoom		= 7000;

	static {
		anInt197 = orthoZoom;
	}

	boolean					aBoolean198;
	private LinkedList		aClass148_189;
	private Class98_Sub48[]	aClass98_Sub48Array195;
	private int				anInt187	= -1;
	private int				anInt188;
	private int				anInt192;

	private int				anInt193	= 0;

	private int[][]			anIntArrayArray194;

	Class16(int i, int i_6_, int i_7_) {
		aClass148_189 = new LinkedList();
		aBoolean198 = false;
		anInt192 = i_6_;
		anInt188 = i;
		anIntArrayArray194 = new int[anInt188][i_7_];
		aClass98_Sub48Array195 = new Class98_Sub48[anInt192];
	}

	public final void method236(int i) {
		try {
			for (int i_0_ = 0; anInt188 > i_0_; i_0_++) {
				anIntArrayArray194[i_0_] = null;
			}
			anIntArrayArray194 = null;
			aClass98_Sub48Array195 = null;
			aClass148_189.clear((byte) 47);
			aClass148_189 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bca.B(" + i + ')');
		}
	}

	public final int[] method237(byte i, int i_2_) {
		try {
			if (i != 98) {
				return null;
			}
			if (anInt192 != anInt188) {
				if (anInt188 == 1) {
					aBoolean198 = (i_2_ ^ 0xffffffff) != (anInt187 ^ 0xffffffff);
					anInt187 = i_2_;
					return anIntArrayArray194[0];
				}
				Class98_Sub48 class98_sub48 = aClass98_Sub48Array195[i_2_];
				if (class98_sub48 != null) {
					aBoolean198 = false;
				} else {
					aBoolean198 = true;
					if (anInt193 < anInt188) {
						class98_sub48 = new Class98_Sub48(i_2_, anInt193);
						anInt193++;
					} else {
						Class98_Sub48 class98_sub48_3_ = (Class98_Sub48) aClass148_189.method2427(-111);
						class98_sub48 = new Class98_Sub48(i_2_, class98_sub48_3_.anInt4282);
						aClass98_Sub48Array195[class98_sub48_3_.anInt4278] = null;
						class98_sub48_3_.unlink(63);
					}
					aClass98_Sub48Array195[i_2_] = class98_sub48;
				}
				aClass148_189.method2423(-2, class98_sub48);
				return anIntArrayArray194[class98_sub48.anInt4282];
			}
			aBoolean198 = aClass98_Sub48Array195[i_2_] == null;
			aClass98_Sub48Array195[i_2_] = VarClientDefinitionParser.aClass98_Sub48_1048;
			return anIntArrayArray194[i_2_];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bca.C(" + i + ',' + i_2_ + ')');
		}
	}

	public final int[][] method238(int i) {
		try {
			if ((anInt188 ^ 0xffffffff) != (anInt192 ^ 0xffffffff)) {
				throw new RuntimeException("Can only retrieve a full image cache");
			}
			for (int i_5_ = 0; anInt188 > i_5_; i_5_++) {
				aClass98_Sub48Array195[i_5_] = VarClientDefinitionParser.aClass98_Sub48_1048;
			}
			return anIntArrayArray194;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bca.D(" + i + ')');
		}
	}
}

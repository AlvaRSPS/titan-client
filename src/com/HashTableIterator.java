/* Class61 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.TextLoadingScreenElement;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class HashTableIterator {
	public static short[][][]	colourToReplace;
	public static Js5			music2Js5;
	public static int			sceneryShadows	= 0;

	public static final void method537(byte i) {
		GamePreferences.setSize((byte) 8, client.preferences.buildArea.getValue((byte) 120));
		int centreX = (Class272.gameSceneBaseX >> 3) + (Class98_Sub46_Sub10.xCamPosTile >> 12);
		int centreY = (SpriteLoadingScreenElement.yCamPosTile >> 12) + (aa_Sub2.gameSceneBaseY >> 3);
		Font.localPlane = Class87.localPlayer.plane = (byte) 0;
		Class87.localPlayer.move(8, 8, 1470);
		int i_2_ = 18;
		Class105.regionNpcMapData = new byte[i_2_][];
		Class255.aByteArrayArray3211 = new byte[i_2_][];
		client.unknown = new byte[i_2_][];
		Class98_Sub46_Sub17.regionXteaKey = new int[i_2_][4];
		HitmarksDefinitionParser.regionPositionHash = new int[i_2_];
		Player.aByteArrayArray6533 = new byte[i_2_][];
		Class98_Sub36.regionObjectMapId = new int[i_2_];
		Class287.regionTerrainId = new int[i_2_];
		HashTable.aByteArrayArray3182 = new byte[i_2_][];
		HashTable.regionUwObjectMapId = new int[i_2_];
		Class76_Sub7.anIntArray3765 = new int[i_2_];
		GameObjectDefinitionParser.regionUwTerrainId = new int[i_2_];
		i_2_ = 0;
		for (int x = (centreX - (Class165.mapWidth >> 4)) / 8; x <= ((Class165.mapWidth >> 4) + centreX) / 8; x++) {
			for (int y = (-(Class98_Sub10_Sub7.mapLength >> 4) + centreY) / 8; y <= ((Class98_Sub10_Sub7.mapLength >> 4) + centreY) / 8; y++) {
				int i_5_ = y + (x << 8);
				HitmarksDefinitionParser.regionPositionHash[i_2_] = i_5_;
				Class287.regionTerrainId[i_2_] = Class234.mapsJs5.getGroupId((byte) -90, "m" + x + "_" + y);
				Class98_Sub36.regionObjectMapId[i_2_] = Class234.mapsJs5.getGroupId((byte) -69, "l" + x + "_" + y);
				Class76_Sub7.anIntArray3765[i_2_] = Class234.mapsJs5.getGroupId((byte) -100, "n" + x + "_" + y);
				GameObjectDefinitionParser.regionUwTerrainId[i_2_] = Class234.mapsJs5.getGroupId((byte) -109, "um" + x + "_" + y);
				HashTable.regionUwObjectMapId[i_2_] = Class234.mapsJs5.getGroupId((byte) -124, "ul" + x + "_" + y);
				if ((Class76_Sub7.anIntArray3765[i_2_] ^ 0xffffffff) == 0) {
					Class287.regionTerrainId[i_2_] = -1;
					Class98_Sub36.regionObjectMapId[i_2_] = -1;
					GameObjectDefinitionParser.regionUwTerrainId[i_2_] = -1;
					HashTable.regionUwObjectMapId[i_2_] = -1;
				}
				i_2_++;
			}
		}
		for (int i_6_ = i_2_; (i_6_ ^ 0xffffffff) > (Class76_Sub7.anIntArray3765.length ^ 0xffffffff); i_6_++) {
			Class76_Sub7.anIntArray3765[i_6_] = -1;
			Class287.regionTerrainId[i_6_] = -1;
			Class98_Sub36.regionObjectMapId[i_6_] = -1;
			GameObjectDefinitionParser.regionUwTerrainId[i_6_] = -1;
			HashTable.regionUwObjectMapId[i_6_] = -1;
		}
		int clientState;
		if (client.clientState != 3) {
			clientState = 8;
		} else {
			clientState = 4;
		}
		Class251.updateMapArea(-6547, centreY, false, centreX, clientState);
	}

	public static final void method540(byte i) {
		try {
			if (i != -51) {
				method537((byte) 114);
			}
			SceneGraphNode.method2962(false);
			OpenGLHeap.aBoolean6079 = false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ec.A(" + i + ')');
		}
	}

	public static final void setClientState(int state, boolean bool) {
		if ((state ^ 0xffffffff) != (client.clientState ^ 0xffffffff)) {
			if (state == 13) {
				if (client.ssKey == null) {
					Class251.handleLobbyToWorldStage(Class360.password, -17877, PointLight.username, OpenGlModelRenderer.anInt4855);
					System.out.println("Clientstatettetetetettetete");
				} else {
					Class98_Sub10_Sub14.method1045(OpenGlModelRenderer.anInt4855, -6182);
				}
			}
			if (state != 13 && Class318.aClass123_2698 != null) {
				Class318.aClass123_2698.close(-61);
				Class318.aClass123_2698 = null;
			}
			if (state == 3) {
				LightIntensityDefinitionParser.method3269(client.topLevelInterfaceId != ItemDefinition.loginInterfaceId, 0);
			}
			if ((state ^ 0xffffffff) == -8) {
				Class96.method922((client.topLevelInterfaceId ^ 0xffffffff) != (TextLoadingScreenElement.lobbyInterfaceId ^ 0xffffffff), -1);
			}
			if (state == 5) {
				if (client.ssKey != null) {
					Class27.method296((byte) 55);
				} else {
					Class276.handleLoginToLobbyStage(Class360.password, PointLight.username, (byte) -68);
				}
			} else if (state != 6) {
				if ((state ^ 0xffffffff) == -10) {
					if (client.ssKey == null) {
						Class251.handleLobbyToWorldStage(Class360.password, -17877, PointLight.username, OpenGlModelRenderer.anInt4855);
					} else {
						Class98_Sub10_Sub14.method1045(OpenGlModelRenderer.anInt4855, -6182);
					}
				} else if ((state ^ 0xffffffff) == -13) {
					if (client.ssKey == null) {
						System.out.println("Clientstatettetetetettetete3");
						Class276.handleLoginToLobbyStage(Class360.password, PointLight.username, (byte) -68);
					} else {
						Class27.method296((byte) 55);
					}
				}
			} else if (client.ssKey != null) {
				Class98_Sub10_Sub14.method1045(OpenGlModelRenderer.anInt4855, -6182);
			} else {
				System.out.println("Clientstatettetetetettetete4");
				Class251.handleLobbyToWorldStage(Class360.password, -17877, PointLight.username, OpenGlModelRenderer.anInt4855);
			}
			if (FloorOverlayDefinition.method2690(client.clientState, 8)) {
				client.miscellaneousJs5.discardUnpacked = 2;
				Class98_Sub10_Sub24.enumsJs5.discardUnpacked = 2;
				Class375.objectsJs5.discardUnpacked = 2;
				Class234.npcJs5.discardUnpacked = 2;
				Class208.itemsJs5.discardUnpacked = 2;
				Char.animationJs5.discardUnpacked = 2;
				PlayerUpdateMask.graphicJs5.discardUnpacked = 2;
			}
			if (FloorOverlayDefinition.method2690(state, 8)) {
				QuickChatMessageParser.anInt2105 = 1;
				Class98_Sub5_Sub3.anInt5538 = 1;
				Class130.anInt1031 = 0;
				Class142.anInt1160 = 0;
				VarClientDefinitionParser.anInt1043 = 0;
				OpenGLHeightMapToNormalMapConverter.method2173(true, 122);
				client.miscellaneousJs5.discardUnpacked = 1;
				Class98_Sub10_Sub24.enumsJs5.discardUnpacked = 1;
				Class375.objectsJs5.discardUnpacked = 1;
				Class234.npcJs5.discardUnpacked = 1;
				Class208.itemsJs5.discardUnpacked = 1;
				Char.animationJs5.discardUnpacked = 1;
				PlayerUpdateMask.graphicJs5.discardUnpacked = 1;
			}
			if ((state ^ 0xffffffff) == -12 || state == 3) {
				Class4.method174((byte) 99);
			}
			boolean bool_8_ = (state ^ 0xffffffff) == -3 || Class53_Sub1.method499(2048, state) || Class246_Sub3_Sub3.method3011(-6410, state);
			boolean bool_9_ = (client.clientState ^ 0xffffffff) == -3 || Class53_Sub1.method499(2048, client.clientState) || Class246_Sub3_Sub3.method3011(-6410, client.clientState);
			if (bool_8_ == !bool_9_) {
				if (bool_8_) {
					Class144.anInt1169 = Class94.anInt795;
					if ((client.preferences.generalMusicVolume.getValue((byte) 125) ^ 0xffffffff) == -1) {
						Class96.method923(103, 2);
					} else {
						Class226.method2854(false, false, client.preferences.generalMusicVolume.getValue((byte) 124), Class98_Sub10_Sub1.musicJs5, 0, 2, Class94.anInt795);
						Class233.method2883((byte) 111);
					}
					client.js5Client.notifyLogin(2, false);
				} else {
					Class96.method923(100, 2);
					client.js5Client.notifyLogin(2, true);
				}
			}
			if (FloorOverlayDefinition.method2690(state, 8) || (state ^ 0xffffffff) == -14) {
				client.graphicsToolkit.method1817();
			}
			if (bool == false) {
				client.clientState = state;
			}
		}
	}

	private HashTable	table;

	private Node		nodePos;

	private int			bucketPos	= 0;

	public HashTableIterator() {
		/* empty */
	}

	public HashTableIterator(HashTable hashTable) {
		table = hashTable;
	}

	public final Node next(int dummy) {
		if ((bucketPos ^ 0xffffffff) < -1 && table.buckets[bucketPos - 1] != nodePos) {
			Node node = nodePos;
			nodePos = node.next;
			return node;
		}
		while ((bucketPos ^ 0xffffffff) > (table.hashResolution ^ 0xffffffff)) {
			Node node = table.buckets[bucketPos++].next;
			if (node != table.buckets[-1 + bucketPos]) {
				nodePos = node.next;
				return node;
			}
		}
		return null;
	}

	public final Node start(int dummy) {
		bucketPos = 0;
		return next(2);
	}
}

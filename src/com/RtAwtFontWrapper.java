
/* Class326 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.PixelGrabber;

public final class RtAwtFontWrapper {
	private static int[]	charToGlyph	= new int[256];
	private static int		glyphCount	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7"
			.length();

	static {
		for (int charId = 0; charId < 256; charId++) {
			int glyphId = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7"
					.indexOf(charId);
			if (glyphId == -1) {
				glyphId = 74;
			}
			charToGlyph[charId] = glyphId;
		}
	}

	private boolean		aBoolean2737	= false;
	private int			anInt2736;
	private Sprite[]	charSprite;
	private int[]		charWidth;

	private int[]		clip			= new int[4];

	private int			fontHeight;

	public RtAwtFontWrapper(RSToolkit toolkit, int fontSize, boolean bold, Component component) {
		aBoolean2737 = false;
		charSprite = new Sprite[256];
		charWidth = new int[256];
		Font font = new Font("Helvetica", bold ? 1 : 0, fontSize);
		FontMetrics fontmetrics = component.getFontMetrics(font);
		for (int glyphId = 0; glyphId < glyphCount; glyphId++) {
			createCharSprite(toolkit, font, fontmetrics,
					"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7"
							.charAt(glyphId), glyphId, false);
		}
		if (bold && aBoolean2737) {
			aBoolean2737 = false;
			font = new Font("Helvetica", 0, fontSize);
			fontmetrics = component.getFontMetrics(font);
			for (int i_20_ = 0; i_20_ < glyphCount; i_20_++) {
				createCharSprite(toolkit, font, fontmetrics,
						"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7"
								.charAt(i_20_), i_20_, false);
			}
			if (!aBoolean2737) {
				aBoolean2737 = false;
				for (int glyphId = 0; glyphId < glyphCount; glyphId++) {
					createCharSprite(toolkit, font, fontmetrics,
							"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7"
									.charAt(glyphId), glyphId, true);
				}
			}
		}
	}

	private final void createCharSprite(RSToolkit toolkit, Font font, FontMetrics fontmetrics, char charId, int glyphId, boolean bool) {
		int imageWidth = fontmetrics.charWidth(charId);
		int characterWidth = imageWidth;
		if (bool) {
			try {
				if (charId == '/') {
					bool = false;
				}
				if (charId == 'f' || charId == 't' || charId == 'w' || charId == 'v' || charId == 'k' || charId == 'x' || charId == 'y' || charId == 'A' || charId == 'V' || charId == 'W') {
					imageWidth++;
				}
			} catch (Exception exception) {
				/* empty */
			}
		}
		int maxAscent = fontmetrics.getMaxAscent();
		int imageHeight = fontmetrics.getMaxAscent() + fontmetrics.getMaxDescent();
		int height = fontmetrics.getHeight();
		Image image = GameShell.canvas.createImage(imageWidth, imageHeight);
		Graphics graphics = image.getGraphics();
		graphics.setColor(Color.black);
		graphics.fillRect(0, 0, imageWidth, imageHeight);
		graphics.setColor(Color.white);
		graphics.setFont(font);
		graphics.drawString(String.valueOf(charId), 0, maxAscent);
		if (bool) {
			graphics.drawString(String.valueOf(charId), 1, maxAscent);
		}
		int[] pixels = new int[imageWidth * imageHeight];
		PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, imageWidth, imageHeight, pixels, 0, imageWidth);
		try {
			pixelgrabber.grabPixels();
		} catch (Exception exception) {
			/* empty */
		}
		image.flush();
		int firstLine = 0;
		while_223_: for (int y = 0; y < imageHeight; y++) {
			for (int x = 0; x < imageWidth; x++) {
				int val = pixels[x + y * imageWidth];
				if ((val & 0xffffff) != 0) {
					firstLine = y;
					break while_223_;
				}
			}
		}
		for (int pixelPtr = 0; pixelPtr < pixels.length; pixelPtr++) {
			if ((pixels[pixelPtr] & 0xffffff) == 0) {
				pixels[pixelPtr] = 0;
			}
		}
		anInt2736 = maxAscent - firstLine;
		fontHeight = height;
		charWidth[glyphId] = characterWidth;
		charSprite[glyphId] = toolkit.createSprite(-7962, 0, imageWidth, imageHeight, pixels, imageWidth);
	}

	private final void drawString(RSToolkit toolkit, String string, int[] clip, int x, int y, int colour, boolean shade) {
		if (colour == 0) {
			shade = false;
		}
		colour |= ~0xffffff;
		for (int stringPtr = 0; stringPtr < string.length(); stringPtr++) {
			int glyph = charToGlyph[string.charAt(stringPtr)];
			if (shade) {
				charSprite[glyph].draw(x + 1, y + 1, 0, -16777216, 1);
			}
			charSprite[glyph].draw(x, y, 0, colour, 1);
			x += charWidth[glyph];
		}
	}

	public final void drawStringLeftAnchor(RSToolkit toolkit, String string, int x, int y, int colour, boolean shade) {
		int textWidth = getStringWidth(string) / 2;
		toolkit.getClip(clip);
		if (x - textWidth <= clip[2] && x + textWidth >= clip[0] && y - anInt2736 <= clip[3] && y + fontHeight >= clip[1]) {
			drawString(toolkit, string, clip, x - textWidth, y, colour, shade);
		}
	}

	public final int getFontHeight() {
		return fontHeight - 1;
	}

	public final int getStringWidth(String string) {
		int i = 0;
		for (int index = 0; index < string.length(); index++) {
			int character = charToGlyph[string.charAt(index)];
			i += charWidth[character];
		}
		return i;
	}

	public final int method3704() {
		return anInt2736;
	}
}

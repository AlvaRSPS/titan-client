
/* Class98_Sub10_Sub32 - Decompiled by JODE
 */ package com; /*
					*/

import java.net.URL;

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;

public final class Class98_Sub10_Sub32 extends Class98_Sub10 {
	public static int	anInt5718	= 0;
	public static int	anInt5720	= 0;

	public static final int method1096(int i, int i_8_) {
		return i & 0x7f;
	}

	public static final SignLinkRequest method1097(int i, String string, SignLink class88, int i_9_) {
		try {
			if ((i_9_ ^ 0xffffffff) == -1) {
				return class88.openUrl(-14, string);
			}
			if (i_9_ == 1) {
				try {
					JavaScriptInterface.callJsMethod(26635, GameShell.applet, "openjs", new Object[] { new URL(GameShell.applet.getCodeBase(), string).toString() });
					SignLinkRequest class143 = new SignLinkRequest();
					class143.status = 1;
					return class143;
				} catch (Throwable throwable) {
					SignLinkRequest class143 = new SignLinkRequest();
					class143.status = 2;
					return class143;
				}
			}
			if ((i_9_ ^ 0xffffffff) == -3) {
				try {
					GameShell.applet.getAppletContext().showDocument(new URL(GameShell.applet.getCodeBase(), string), "_blank");
					SignLinkRequest class143 = new SignLinkRequest();
					class143.status = 1;
					return class143;
				} catch (Exception exception) {
					SignLinkRequest class143 = new SignLinkRequest();
					class143.status = 2;
					return class143;
				}
			}
			if (i != -18871) {
				method1096(103, -97);
			}
			if (i_9_ == 3) {
				try {
					JavaScriptInterface.callJsMethod("loggedout", GameShell.applet, -26978);
				} catch (Throwable throwable) {
					/* empty */
				}
				try {
					GameShell.applet.getAppletContext().showDocument(new URL(GameShell.applet.getCodeBase(), string), "_top");
					SignLinkRequest class143 = new SignLinkRequest();
					class143.status = 1;
					return class143;
				} catch (Exception exception) {
					SignLinkRequest class143 = new SignLinkRequest();
					class143.status = 2;
					return class143;
				}
			}
			throw new IllegalArgumentException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sd.E(" + i + ',' + (string != null ? "{...}" : "null") + ',' + (class88 != null ? "{...}" : "null") + ',' + i_9_ + ')');
		}
	}

	public static final void method1098(byte i) {
		do {
			try {
				if (Class98_Sub10_Sub9.aBoolean5585) {
					RtInterface class293 = Class246_Sub9.getDynamicComponent((byte) 72, Class187.anInt1450, Class310.anInt2652);
					if (class293 != null && class293.anObjectArray2324 != null) {
						ClientScript2Event class98_sub21 = new ClientScript2Event();
						class98_sub21.component = class293;
						class98_sub21.param = class293.anObjectArray2324;
						ClientScript2Runtime.handleEvent(class98_sub21);
					}
					Class98_Sub10_Sub9.aBoolean5585 = false;
					GlobalPlayer.anInt3173 = -1;
					Class21_Sub2.cursorId = -1;
					if (class293 == null) {
						break;
					}
					WorldMapInfoDefinitionParser.setDirty(1, class293);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sd.D(" + i + ')');
			}
			break;
		} while (false);
	}

	private int	anInt5719	= 1;

	private int	anInt5721	= 1;

	private int	anInt5722	= 204;

	public Class98_Sub10_Sub32() {
		super(0, true);
	}

	@Override
	public final int[] method990(int i, int i_0_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_0_);
			if (i != 255) {
				anInt5719 = 57;
			}
			if (this.aClass16_3863.aBoolean198) {
				int i_1_ = 0;
				for (/**/; (i_1_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_1_++) {
					int i_2_ = MonoOrStereoPreferenceField.anIntArray3640[i_1_];
					int i_3_ = GameObjectDefinition.anIntArray3001[i_0_];
					int i_4_ = anInt5721 * i_2_ >> 160759052;
					int i_5_ = i_3_ * anInt5719 >> 274375116;
					int i_6_ = i_2_ % (4096 / anInt5721) * anInt5721;
					int i_7_ = i_3_ % (4096 / anInt5719) * anInt5719;
					if (i_7_ < anInt5722) {
						for (i_4_ -= i_5_; (i_4_ ^ 0xffffffff) > -1; i_4_ += 4) {
							/* empty */
						}
						for (/**/; (i_4_ ^ 0xffffffff) < -4; i_4_ -= 4) {
							/* empty */
						}
						if ((i_4_ ^ 0xffffffff) != -2) {
							is[i_1_] = 0;
							continue;
						}
						if ((anInt5722 ^ 0xffffffff) < (i_6_ ^ 0xffffffff)) {
							is[i_1_] = 0;
							continue;
						}
					}
					if ((i_6_ ^ 0xffffffff) > (anInt5722 ^ 0xffffffff)) {
						for (i_4_ -= i_5_; (i_4_ ^ 0xffffffff) > -1; i_4_ += 4) {
							/* empty */
						}
						for (/**/; i_4_ > 3; i_4_ -= 4) {
							/* empty */
						}
						if ((i_4_ ^ 0xffffffff) < -1) {
							is[i_1_] = 0;
							continue;
						}
					}
					is[i_1_] = 4096;
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sd.G(" + i + ',' + i_0_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_10_) {
		while_167_: do {
			try {
				if (i_10_ >= -92) {
					anInt5719 = 113;
				}
				int i_11_ = i;
				do {
					if (i_11_ != 0) {
						if (i_11_ != 1) {
							if ((i_11_ ^ 0xffffffff) == -3) {
								break;
							}
							break while_167_;
						}
					} else {
						anInt5721 = class98_sub22.readUnsignedByte((byte) 126);
						break while_167_;
					}
					anInt5719 = class98_sub22.readUnsignedByte((byte) 8);
					break while_167_;
				} while (false);
				anInt5722 = class98_sub22.readShort((byte) 127);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sd.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_10_ + ')');
			}
		} while (false);
	}
}


/* Class214 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;
import java.lang.reflect.Method;

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;

public final class Class214 {
	public static final RSToolkit create(int i, int i_0_, Canvas canvas, TextureMetricsList metricsList, Js5 archive) {
		try {
			RSToolkit toolkit;
			try {
				if (!Class319.loadedNativeLibs(true)) {
					throw new RuntimeException("");
				}
				if (!AdvancedMemoryCache.loadNative("jagdx", (byte) -36)) {
					throw new RuntimeException("");
				}
				Class<?> directX = Class.forName("com.DirectXToolkit");
				Method method = directX.getDeclaredMethod("createToolkit", Class.forName("java.awt.Canvas"), Class.forName("com.TextureMetricsList"), Class.forName("com.jagex.game.client.archive.Js5"), Class.forName("java.lang.Integer"));
				toolkit = (RSToolkit) method.invoke(null, canvas, metricsList, archive, new Integer(i_0_));
			} catch (RuntimeException runtimeException) {
				throw runtimeException;
			} catch (Throwable throwable) {
				throw new RuntimeException("");
			}
			return toolkit;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}
}

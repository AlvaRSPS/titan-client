/* Class27 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

public final class Class27 {
	public static LinkedList		aClass148_275			= new LinkedList();
	public static Class268			aClass268_276;
	public static IncomingOpcode	aClass58_274			= new IncomingOpcode(70, -2);
	public static long[]			aLongArray280			= new long[11];
	public static long[][]			aLongArrayArray279		= new long[8][256];
	public static int[]				anIntArray278			= new int[8];
	public static IncomingOpcode	SEND_CLOSE_INTERFACE	= new IncomingOpcode(61, 4);

	static {
		for (int i = 0; i < 256; i++) {
			int i_0_ = "\u1823\uc6e8\u87b8\u014f\u36a6\ud2f5\u796f\u9152\u60bc\u9b8e\ua30c\u7b35\u1de0\ud7c2\u2e4b\ufe57\u1577\u37e5\u9ff0\u4ada\u58c9\u290a\ub1a0\u6b85\ubd5d\u10f4\ucb3e\u0567\ue427\u418b\ua77d\u95d8\ufbee\u7c66\udd17\u479e\uca2d\ubf07\uad5a\u8333\u6302\uaa71\uc819\u49d9\uf2e3\u5b88\u9a26\u32b0\ue90f\ud580\ubecd\u3448\uff7a\u905f\u2068\u1aae\ub454\u9322\u64f1\u7312\u4008\uc3ec\udba1\u8d3d\u9700\ucf2b\u7682\ud61b\ub5af\u6a50\u45f3\u30ef\u3f55\ua2ea\u65ba\u2fc0\ude1c\ufd4d\u9275\u068a\ub2e6\u0e1f\u62d4\ua896\uf9c5\u2559\u8472\u394c\u5e78\u388c\ud1a5\ue261\ub321\u9c1e\u43c7\ufc04\u5199\u6d0d\ufadf\u7e24\u3bab\uce11\u8f4e\ub7eb\u3c81\u94f7\ub913\u2cd3\ue76e\uc403\u5644\u7fa9\u2abb\uc153\udc0b\u9d6c\u3174\uf646\uac89\u14e1\u163a\u6909\u70b6\ud0ed\ucc42\u98a4\u285c\uf886"
					.charAt(i / 2);
			long l = (0x1 & i ^ 0xffffffff) == -1 ? i_0_ >>> 1856053640 : i_0_ & 0xff;
			long l_1_ = l << 909211713;
			if (l_1_ >= 256L) {
				l_1_ ^= 0x11dL;
			}
			long l_2_ = l_1_ << 1782822081;
			if ((l_2_ ^ 0xffffffffffffffffL) <= -257L) {
				l_2_ ^= 0x11dL;
			}
			long l_3_ = l_2_ ^ l;
			long l_4_ = l_2_ << 483539521;
			if ((l_4_ ^ 0xffffffffffffffffL) <= -257L) {
				l_4_ ^= 0x11dL;
			}
			long l_5_ = l ^ l_4_;
			aLongArrayArray279[0][i] = Class151_Sub1.method2448(l_5_, Class151_Sub1.method2448(Class151_Sub1.method2448(l_3_ << 390312336, Class151_Sub1.method2448(l_4_ << 1263478616, Class151_Sub1.method2448(Class151_Sub1.method2448(l_2_ << 1309969768, Class151_Sub1.method2448(l << -966627024,
					l << -650887624)), l << 996425056))), l_1_ << -175798776));
			for (int i_6_ = 1; (i_6_ ^ 0xffffffff) > -9; i_6_++) {
				aLongArrayArray279[i_6_][i] = Class151_Sub1.method2448(aLongArrayArray279[-1 + i_6_][i] << 2027155000, aLongArrayArray279[-1 + i_6_][i] >>> 1503710728);
			}
		}
		aLongArray280[0] = 0L;
		for (int i = 1; i <= 10; i++) {
			int i_7_ = -8 + i * 8;
			aLongArray280[i] = Class284_Sub1_Sub1.method3367(Class35.method335(aLongArrayArray279[7][7 + i_7_], 255L), Class284_Sub1_Sub1.method3367(Class284_Sub1_Sub1.method3367(Class284_Sub1_Sub1.method3367(Class35.method335(aLongArrayArray279[4][i_7_ + 4], 4278190080L), Class284_Sub1_Sub1
					.method3367(Class35.method335(aLongArrayArray279[3][i_7_ + 3], 1095216660480L), Class284_Sub1_Sub1.method3367(Class284_Sub1_Sub1.method3367(Class35.method335(71776119061217280L, aLongArrayArray279[1][i_7_ + 1]), Class35.method335(aLongArrayArray279[0][i_7_],
							-72057594037927936L)), Class35.method335(aLongArrayArray279[2][i_7_ - -2], 280375465082880L)))), Class35.method335(aLongArrayArray279[5][i_7_ + 5], 16711680L)), Class35.method335(aLongArrayArray279[6][i_7_ - -6], 65280L)));
		}
	}

	public static final void method294(boolean bool) {
		try {
			FloorOverlayDefinition.floorOverlayDefinitionList.cacheRemoveSoftReferences((byte) -46);
			Class82.floorUnderDefinitionList.freeSoftReferences((byte) 62);
			ParamDefinition.identikitDefinitionList.method828(-113);
			Class130.gameObjectDefinitionList.method3547((byte) 127);
			Class4.npcDefinitionList.method3535((byte) -19);
			Class98_Sub46_Sub19.itemDefinitionList.freeSoftReferences((byte) 71);
			Class151_Sub7.animationDefinitionList.method2619(-2118);
			BuildLocation.gfxDefinitionList.freeSoftReferences(4);
			DataFs.bConfigDefinitionList.method2683(0);
			SpriteLoadingScreenElement.varPlayerDefinitionList.method2283((byte) 97);
			Class370.renderAnimDefinitionList.method3202((byte) 96);
			Class98_Sub10_Sub23.mapScenesDefinitionList.method3768(10673);
			Class216.worldMapInfoDefinitionList.method3813(36);
			Class303.questDefinitionList.freeSoftReferences(bool);
			Class98_Sub43_Sub1.paramDefinitionList.method3944(-1);
			SimpleProgressBarLoadingScreenElement.skyboxDefinitionList.method525(-112);
			GrandExchangeOffer.sunDefinitionList.method2158(bool);
			Class21_Sub1.lightIntensityDefinitionList.method3271(bool);
			Class18.cursorDefinitionList.method200(1);
			Class62.structsDefinitionList.method3226(32);
			Class246_Sub3_Sub1.hitmarksDefinitionList.freeSoftReferences(0);
			ClipMap.method2942(1);
			NewsLSEConfig.method489(false);
			Class34.method328(0);
			TextLSEConfig.method3646(-106);
			Class246_Sub3_Sub2_Sub1.method3008((byte) 60);
			Class275.aClass79_2046.freeSoftReferences((byte) -106);
			Class224_Sub3.aClass79_5039.freeSoftReferences((byte) -82);
			Class378.aClass79_3189.freeSoftReferences((byte) -80);
			Class98_Sub6.aClass79_3847.freeSoftReferences((byte) 118);
			ClientScript2Runtime.aClass79_1890.freeSoftReferences((byte) 28);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "c.C(" + bool + ')');
		}
	}

	public static final void method296(byte i) {
		Class98_Sub46_Sub20_Sub2.anInt6317 = 1;
		OpenGlModelRenderer.anInt4855 = -1;
		long l = 0L;
		if (client.ssKey == null) {
			Class369.setLoginResponse(35, (byte) -55);
		} else {
			RSByteBuffer class98_sub22 = new RSByteBuffer(VarPlayerDefinition.method2531(Class378.method4006(client.ssKey, -1), 12705));
			l = class98_sub22.readLong(i + -171);
			Class98_Sub10_Sub19.aLong5631 = class98_sub22.readLong(-105);
			Class342.method3814(true, Class98_Sub28.longToString(-111, l), 80, "");
		}

	}
}

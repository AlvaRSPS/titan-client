/* Class98_Sub23 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;

public final class Class98_Sub23 extends Node {
	public static OutgoingOpcode	aClass171_3998	= new OutgoingOpcode(1, -1);
	public static Class85			aClass85_4007	= new Class85(3, 2);
	public static int				anInt4001;

	public static void method1266(int i) {
		try {
			aClass171_3998 = null;
			if (i != -6292) {
				aClass171_3998 = null;
			}
			aClass85_4007 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jia.C(" + i + ')');
		}
	}

	public static final byte[] method1268(int i, int i_2_, byte[] is, byte i_3_) {
		try {
			byte[] is_5_ = new byte[i];
			ArrayUtils.method2894(is, i_2_, is_5_, 0, i);
			return is_5_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jia.B(" + i + ',' + i_2_ + ',' + (is != null ? "{...}" : "null") + ',' + i_3_ + ')');
		}
	}

	Class98_Sub47	aClass98_Sub47_3997;
	int				anInt3996	= 2147483647;
	int				anInt3999	= 2147483647;
	int				anInt4000	= -2147483648;
	int				anInt4002;
	int				anInt4003;
	int				anInt4004;

	int				anInt4005	= 2147483647;

	int				anInt4006;

	Class98_Sub23(Class98_Sub47 class98_sub47) {
		anInt4003 = 2147483647;
		anInt4006 = -2147483648;
		anInt4002 = -2147483648;
		anInt4004 = -2147483648;
		try {
			aClass98_Sub47_3997 = class98_sub47;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jia.<init>(" + (class98_sub47 != null ? "{...}" : "null") + ')');
		}
	}

	public final boolean method1267(int i, int i_0_, int i_1_) {
		try {
			if (anInt3996 <= i_1_ && anInt4006 >= i_1_ && anInt3999 <= i_0_ && i_0_ <= anInt4000) {
				return true;
			}
			if (i != -2147483648) {
				method1267(85, -86, -89);
			}
			return (anInt4003 ^ 0xffffffff) >= (i_1_ ^ 0xffffffff) && (anInt4002 ^ 0xffffffff) <= (i_1_ ^ 0xffffffff) && anInt4005 <= i_0_ && anInt4004 >= i_0_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jia.A(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}
}

/* Class353 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteProgressBarLSEConfig;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class353 {
	public static int transOnscreenCount;

	public static final String filterName(int i, String string) {
		try {
			if (string == null) {
				return null;
			}
			int i_0_ = 0;
			int i_1_;
			for (i_1_ = string.length(); i_0_ < i_1_; i_0_++) {
				if (!SpriteProgressBarLSEConfig.method911(string.charAt(i_0_), 95)) {
					break;
				}
			}
			for (/**/; i_0_ < i_1_ && SpriteProgressBarLSEConfig.method911(string.charAt(-1 + i_1_), 95); i_1_--) {
				/* empty */
			}
			int i_2_ = -i_0_ + i_1_;
			if ((i_2_ ^ 0xffffffff) > -2 || i_2_ > 12) {
				return null;
			}
			if (i != -1) {
				transOnscreenCount = 84;
			}
			StringBuffer stringbuffer = new StringBuffer(i_2_);
			for (int i_3_ = i_0_; (i_3_ ^ 0xffffffff) > (i_1_ ^ 0xffffffff); i_3_++) {
				char c = string.charAt(i_3_);
				if (Class98_Sub43_Sub3.method1499((byte) 105, c)) {
					char c_4_ = Class346.method3829(c, i + 1);
					if ((c_4_ ^ 0xffffffff) != -1) {
						stringbuffer.append(c_4_);
					}
				}
			}
			if (stringbuffer.length() == 0) {
				return null;
			}
			return stringbuffer.toString();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vi.A(" + i + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method3868(int i, int i_5_, byte i_6_, int i_7_, int i_8_) {
		try {
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_5_, -114, 8);
			class98_sub46_sub17.method1626((byte) -103);
			class98_sub46_sub17.anInt6054 = i_7_;
			class98_sub46_sub17.anInt6053 = i;
			class98_sub46_sub17.headId = i_8_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vi.B(" + i + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	public static final void method3869(int i, int i_9_, int i_10_) {
		do {
			try {
				if (Class21_Sub4.anInt5396 == 1) {
					JavaNetworkWriter.method3604(i, (byte) 78, i_9_, Class347.aClass98_Sub46_Sub8_2908);
				} else if ((Class21_Sub4.anInt5396 ^ 0xffffffff) == -3) {
					if (OpenGLHeap.aBoolean6079) {
						Class98_Sub4.method953(i - -MapScenesDefinitionParser.method3765(false), i_9_ - -Class189.method2642((byte) 42), true);
					} else {
						Class98_Sub4.method953(i, i_9_, true);
					}
				}
				Class21_Sub4.anInt5396 = 0;
				Class347.aClass98_Sub46_Sub8_2908 = null;
				if (i_10_ == -2) {
					break;
				}
				filterName(-15, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vi.C(" + i + ',' + i_9_ + ',' + i_10_ + ')');
			}
			break;
		} while (false);
	}
}

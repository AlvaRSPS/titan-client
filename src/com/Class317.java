/* Class317 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Archive;
import com.jagex.game.client.archive.Js5Exception;

public abstract class Class317 {
	public static final void method3651(byte i) {
		try {
			if (i == -53) {
				Archive.method3789(70);
				Player.menuOpen = false;
				Class246_Sub3_Sub4_Sub3.method3071(Class15.anInt172, i ^ 0x34, Class246_Sub3_Sub4_Sub4.width, Class38.anInt355, OpenGlArrayBufferPointer.anInt897);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tj.D(" + i + ')');
		}
	}

	public abstract void method3652(byte i, byte[] is);

	abstract byte[] method3653(int i, int i_0_, boolean bool);

	abstract byte[] method3654(boolean bool);
}

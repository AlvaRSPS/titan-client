/* Class48_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;

public abstract class Class48_Sub2 extends Class48 {
	public static final void method470(byte[][] is, Class305_Sub1 class305_sub1, int i) {
		try {
			if (i != -4789) {
				method470(null, null, -69);
			}
			for (int i_0_ = 0; (i_0_ ^ 0xffffffff) > (class305_sub1.sizeHl ^ 0xffffffff); i_0_++) {
				Class128.method2224(i ^ ~0x4a1c);
				for (int i_1_ = 0; i_1_ < Class165.mapWidth >> -1696914461; i_1_++) {
					for (int i_2_ = 0; i_2_ < Class98_Sub10_Sub7.mapLength >> -516098653; i_2_++) {
						int i_3_ = Class170.regionData[i_0_][i_1_][i_2_];
						if ((i_3_ ^ 0xffffffff) != 0) {
							int i_4_ = 0x3 & i_3_ >> -1093162696;
							if (!class305_sub1.underwater || i_4_ == 0) {
								int i_5_ = 0x3 & i_3_ >> -610352415;
								int i_6_ = (i_3_ & 0xffc290) >> -1877248338;
								int i_7_ = (i_3_ & 0x3ffc) >> 255846019;
								int i_8_ = (i_6_ / 8 << -1553791576) - -(i_7_ / 8);
								for (int i_9_ = 0; i_9_ < HitmarksDefinitionParser.regionPositionHash.length; i_9_++) {
									if ((i_8_ ^ 0xffffffff) == (HitmarksDefinitionParser.regionPositionHash[i_9_] ^ 0xffffffff) && is[i_9_] != null) {
										class305_sub1.method3584(VarPlayerDefinition.clipMaps, i_0_, i_4_, is[i_9_], i_2_ * 8, 8 * i_1_, (i_6_ & 0x7) * 8, (0x7 & i_7_) * 8, i_5_, client.graphicsToolkit, i ^ ~0x12cc);
										break;
									}
								}
							}
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jv.H(" + (is != null ? "{...}" : "null") + ',' + (class305_sub1 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	abstract Interface4_Impl3 method469(int i);
}

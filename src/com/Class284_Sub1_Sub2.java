/* Class284_Sub1_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.preferences.VolumePreferenceField;

public final class Class284_Sub1_Sub2 extends Class284_Sub1 {
	public static OutgoingOpcode	aClass171_6191;
	public static int[]				anIntArray6193	= new int[4];
	public static int				layerColour;

	static {
		aClass171_6191 = new OutgoingOpcode(37, -1);
	}

	public static void method3369(byte i) {
		try {
			anIntArray6193 = null;
			if (i >= -28) {
				method3371(-40);
			}
			aClass171_6191 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tb.K(" + i + ')');
		}
	}

	public static final void method3370(int i, int i_0_, Mob mob, int i_1_, int i_2_) {
		try {
			RenderAnimDefinition definition = mob.method3039(1);
			if (i_0_ != 6144) {
				anIntArray6193 = null;
			}
			int i_3_ = mob.anInt6415 - mob.yaw.value & 0x3fff;
			if (i_1_ == -1) {
				if ((i_3_ ^ 0xffffffff) == -1 && (mob.anInt6408 ^ 0xffffffff) >= -26) {
					if (!mob.aBoolean6359 || !definition.method3480((byte) -53, mob.anInt6385)) {
						mob.anInt6385 = definition.method3478(i_0_ ^ 0x1820);
						mob.aBoolean6359 = mob.anInt6385 != -1;
					}
				} else {
					if ((i_2_ ^ 0xffffffff) <= -1 || definition.anInt2376 == -1) {
						if (i_2_ <= 0 || definition.anInt2388 == -1) {
							mob.anInt6385 = definition.anInt2399;
						} else {
							mob.anInt6385 = definition.anInt2388;
						}
					} else {
						mob.aBoolean6359 = false;
						mob.anInt6385 = definition.anInt2376;
					}
					mob.aBoolean6359 = false;
				}
			} else if ((mob.anInt6364 ^ 0xffffffff) == 0 || i_3_ < 10240 && i_3_ > 2048) {
				if ((i_3_ ^ 0xffffffff) != -1 || mob.anInt6408 > 25) {
					if (i_1_ != 2 || (definition.anInt2389 ^ 0xffffffff) == 0) {
						if (i_1_ == 0 && definition.anInt2368 != -1) {
							if (i_2_ < 0 && definition.anInt2405 != -1) {
								mob.anInt6385 = definition.anInt2405;
							} else if ((i_2_ ^ 0xffffffff) >= -1 || definition.anInt2404 == -1) {
								mob.anInt6385 = definition.anInt2368;
							} else {
								mob.anInt6385 = definition.anInt2404;
							}
						} else if ((i_2_ ^ 0xffffffff) <= -1 || definition.anInt2378 == -1) {
							if ((i_2_ ^ 0xffffffff) < -1 && definition.anInt2369 != -1) {
								mob.anInt6385 = definition.anInt2369;
							} else {
								mob.anInt6385 = definition.anInt2399;
							}
						} else {
							mob.anInt6385 = definition.anInt2378;
						}
					} else if (i_2_ < 0 && (definition.anInt2384 ^ 0xffffffff) != 0) {
						mob.anInt6385 = definition.anInt2384;
					} else if (i_2_ <= 0 || (definition.anInt2370 ^ 0xffffffff) == 0) {
						mob.anInt6385 = definition.anInt2389;
					} else {
						mob.anInt6385 = definition.anInt2370;
					}
					mob.aBoolean6359 = false;
				} else {
					if ((i_1_ ^ 0xffffffff) == -3 && (definition.anInt2389 ^ 0xffffffff) != 0) {
						mob.anInt6385 = definition.anInt2389;
					} else if (i_1_ == 0 && (definition.anInt2368 ^ 0xffffffff) != 0) {
						mob.anInt6385 = definition.anInt2368;
					} else {
						mob.anInt6385 = definition.anInt2399;
					}
					mob.aBoolean6359 = false;
				}
			} else {
				int i_4_ = 0x3fff & -mob.yaw.value + Class98_Sub43_Sub1.anIntArray5896[i];
				if ((i_1_ ^ 0xffffffff) == -3 && definition.anInt2389 != -1) {
					if (i_4_ > 2048 && i_4_ <= 6144 && (definition.anInt2402 ^ 0xffffffff) != 0) {
						mob.anInt6385 = definition.anInt2402;
					} else if (i_4_ < 10240 || (i_4_ ^ 0xffffffff) <= -14337 || definition.anInt2357 == -1) {
						if ((i_4_ ^ 0xffffffff) < -6145 && (i_4_ ^ 0xffffffff) > -10241 && (definition.anInt2361 ^ 0xffffffff) != 0) {
							mob.anInt6385 = definition.anInt2361;
						} else {
							mob.anInt6385 = definition.anInt2389;
						}
					} else {
						mob.anInt6385 = definition.anInt2357;
					}
				} else if ((i_1_ ^ 0xffffffff) == -1 && (definition.anInt2368 ^ 0xffffffff) != 0) {
					if ((i_4_ ^ 0xffffffff) < -2049 && i_4_ <= 6144 && (definition.anInt2403 ^ 0xffffffff) != 0) {
						mob.anInt6385 = definition.anInt2403;
					} else if (i_4_ < 10240 || (i_4_ ^ 0xffffffff) <= -14337 || (definition.anInt2377 ^ 0xffffffff) == 0) {
						if (i_4_ > 6144 && (i_4_ ^ 0xffffffff) > -10241 && (definition.anInt2394 ^ 0xffffffff) != 0) {
							mob.anInt6385 = definition.anInt2394;
						} else {
							mob.anInt6385 = definition.anInt2368;
						}
					} else {
						mob.anInt6385 = definition.anInt2377;
					}
				} else if (i_4_ > 2048 && i_4_ <= 6144 && (definition.anInt2372 ^ 0xffffffff) != 0) {
					mob.anInt6385 = definition.anInt2372;
				} else if (i_4_ < 10240 || (i_4_ ^ 0xffffffff) <= -14337 || (definition.anInt2359 ^ 0xffffffff) == 0) {
					if ((i_4_ ^ 0xffffffff) >= -6145 || i_4_ >= 10240 || (definition.anInt2365 ^ 0xffffffff) == 0) {
						mob.anInt6385 = definition.anInt2399;
					} else {
						mob.anInt6385 = definition.anInt2365;
					}
				} else {
					mob.anInt6385 = definition.anInt2359;
				}
				mob.aBoolean6359 = false;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tb.L(" + i + ',' + i_0_ + ',' + (mob != null ? "{...}" : "null") + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public static final void method3371(int i) {
		do {
			try {
				VolumePreferenceField.method644(-67);
				if (i == 31398) {
					break;
				}
				aClass171_6191 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "tb.N(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method3373(int i, int i_10_, byte i_11_) {
		try {
			if (i_11_ <= 10) {
				method3370(36, -41, null, 23, 85);
			}
			return (i_10_ & 0xc580 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tb.O(" + i + ',' + i_10_ + ',' + i_11_ + ')');
		}
	}

	private byte[] aByteArray6194;

	public Class284_Sub1_Sub2() {
		super(12, 5, 16, 2, 2, 0.45F);
	}

	@Override
	public final void method3363(byte i, int i_7_, byte i_8_) {
		do {
			try {
				i = (byte) (((i & 0xff) >> -428365951) + 127);
				int i_9_ = i_7_ * 2;
				aByteArray6194[i_9_++] = i;
				aByteArray6194[i_9_] = i;
				if (i_8_ == 42) {
					break;
				}
				aClass171_6191 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "tb.M(" + i + ',' + i_7_ + ',' + i_8_ + ')');
			}
			break;
		} while (false);
	}

	public final byte[] method3372(boolean bool, int i, int i_5_, int i_6_) {
		try {
			aByteArray6194 = new byte[2 * i_5_ * i * i_6_];
			if (bool != true) {
				method3363((byte) 104, -114, (byte) 118);
			}
			method3361((byte) -45, i_6_, i, i_5_);
			return aByteArray6194;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tb.J(" + bool + ',' + i + ',' + i_5_ + ',' + i_6_ + ')');
		}
	}
}

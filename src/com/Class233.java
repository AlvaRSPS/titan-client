/* Class233 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.input.impl.AwtMouseListener;
import com.jagex.game.toolkit.matrix.OpenGlMatrix;

public final class Class233 {

	public static final void method2883(byte i) {
		try {
			if (i != 111) {
				method2884(96);
			}
			AwtMouseListener.method3523(255, -1, i + -111);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "os.A(" + i + ')');
		}
	}

	public static final void method2884(int i) {
		try {
			Class246_Sub3_Sub4_Sub5.method3084(true);
			GroupProgressMonitor.method2799(2, 22050, (client.preferences.monoOrStereo.getValue((byte) 127) ^ 0xffffffff) == -2, (byte) 124);
			SystemInformation.aClass268_1173 = FloorUnderlayDefinitionParser.method2484(22050, 0, (byte) -126, GameShell.signLink, GameShell.canvas);
			Class246_Sub3_Sub5_Sub2.method3098(OpenGlMatrix.method2115(126, null), true, 28643);
			if (i >= 96) {
				Class27.aClass268_276 = FloorUnderlayDefinitionParser.method2484(2048, 1, (byte) -126, GameShell.signLink, GameShell.canvas);
				Class27.aClass268_276.method3252(0, Class81.aClass98_Sub31_Sub3_619);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "os.B(" + i + ')');
		}
	}
}

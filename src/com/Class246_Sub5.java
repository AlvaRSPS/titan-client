/* Class246_Sub5 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;

public final class Class246_Sub5 extends SceneGraphNode {
	private static boolean[]	aBooleanArray5094	= new boolean[8];
	private static boolean[]	aBooleanArray5098	= new boolean[32];

	public static final Class246_Sub5 method3117(int i, boolean bool) {
		if (Class273.anInt2039 != Class258.anInt1952) {
			Class246_Sub5 class246_sub5 = SimpleProgressBarLoadingScreenElement.aClass246_Sub5Array5469[Class258.anInt1952];
			Class258.anInt1952 = Class258.anInt1952 + 1 & Class224_Sub1.anIntArray5034[RotatingSpriteLSEConfig.anInt5497];
			class246_sub5.method3124(i, bool);
			return class246_sub5;
		}
		return new Class246_Sub5(i, bool);
	}

	public static void method3125() {
		aBooleanArray5098 = null;
		aBooleanArray5094 = null;
	}

	public boolean						aBoolean5096;
	public boolean						aBoolean5099	= false;
	public boolean						aBoolean5107;
	public boolean						aBoolean5108;
	public LinkedList					aClass148_5102;
	public SceneGraphNodeList			aClass218_5097;
	public Class242						aClass242_5104;
	public Class246_Sub4_Sub2_Sub1[]	aClass246_Sub4_Sub2_Sub1Array5095;
	public long							aLong5100;
	public long							aLong5101;
	public int							anInt5093		= 0;
	public int							anInt5103;

	public int							anInt5105;

	public int							anInt5106;

	private Class246_Sub5(int i, boolean bool) {
		aBoolean5096 = false;
		aClass218_5097 = new SceneGraphNodeList();
		anInt5103 = 0;
		aClass148_5102 = new LinkedList();
		anInt5105 = 0;
		aBoolean5108 = false;
		aBoolean5107 = false;
		aClass242_5104 = new Class242();
		aClass246_Sub4_Sub2_Sub1Array5095 = new Class246_Sub4_Sub2_Sub1[8192];
		method3124(i, bool);
	}

	public final void method3114() {
		aBoolean5096 = true;
	}

	public final Class242 method3115() {
		aClass242_5104.aClass358_1850.method3886((byte) -17);
		for (Class246_Sub4_Sub2_Sub1 element : aClass246_Sub4_Sub2_Sub1Array5095) {
			if (element != null && element.aClass246_Sub9_6492 != null) {
				aClass242_5104.aClass358_1850.method3891(element, 8);
			}
		}
		return aClass242_5104;
	}

	public final Class242 method3116() {
		return aClass242_5104;
	}

	public final void method3118(RSToolkit var_ha) {
		aClass242_5104.aClass358_1850.method3886((byte) 99);
		for (Class246_Sub9 class246_sub9 = (Class246_Sub9) aClass218_5097.getFirst((byte) 15); class246_sub9 != null; class246_sub9 = (Class246_Sub9) aClass218_5097.getNext(false)) {
			class246_sub9.method3134(aLong5100, var_ha, (byte) 38);
		}
	}

	private final void method3119(RSToolkit var_ha, Class87[] class87s, boolean bool) {
		for (int i = 0; i < 32; i++) {
			aBooleanArray5098[i] = false;
		}
		while_102_: for (Class246_Sub9 class246_sub9 = (Class246_Sub9) aClass218_5097.getFirst((byte) 15); class246_sub9 != null; class246_sub9 = (Class246_Sub9) aClass218_5097.getNext(false)) {
			if (class87s != null) {
				for (int i = 0; i < class87s.length; i++) {
					if (class246_sub9.aClass87_5131 == class87s[i] || class246_sub9.aClass87_5131 == class87s[i].aClass87_657) {
						aBooleanArray5098[i] = true;
						class246_sub9.method3138(-1);
						class246_sub9.aBoolean5139 = false;
						continue while_102_;
					}
				}
			}
			if (!bool) {
				if (class246_sub9.anInt5135 == 0) {
					class246_sub9.unlink((byte) 122);
					anInt5103--;
				} else {
					class246_sub9.aBoolean5139 = true;
				}
			}
		}
		if (class87s != null) {
			for (int i = 0; i < class87s.length; i++) {
				if (i == 32 || anInt5103 == 32) {
					break;
				}
				if (!aBooleanArray5098[i]) {
					Class246_Sub9 class246_sub9 = new Class246_Sub9(var_ha, class87s[i], this, aLong5101);
					aClass218_5097.addLast(true, class246_sub9);
					anInt5103++;
					aBooleanArray5098[i] = true;
				}
			}
		}
	}

	public final void method3120(RSToolkit var_ha, long l, Class87[] class87s, Class35[] class35s, boolean bool) {
		if (!aBoolean5099) {
			method3119(var_ha, class87s, bool);
			method3122(class35s, bool);
			aLong5101 = l;
		}
	}

	public final boolean method3121(RSToolkit var_ha, long l) {
		if (aLong5101 != aLong5100) {
			method3114();
		} else {
			method3128();
		}
		if (l - aLong5101 > 750L) {
			method3129();
			return false;
		}
		int i = (int) (l - aLong5100);
		if (aBoolean5107) {
			for (Class246_Sub9 class246_sub9 = (Class246_Sub9) aClass218_5097.getFirst((byte) 15); class246_sub9 != null; class246_sub9 = (Class246_Sub9) aClass218_5097.getNext(false)) {
				for (int i_0_ = 0; i_0_ < class246_sub9.aClass92_5132.anInt784; i_0_++) {
					class246_sub9.method3135(var_ha, !aBoolean5096, l, -64, 1);
				}
			}
			aBoolean5107 = false;
		}
		for (Class246_Sub9 class246_sub9 = (Class246_Sub9) aClass218_5097.getFirst((byte) 15); class246_sub9 != null; class246_sub9 = (Class246_Sub9) aClass218_5097.getNext(false)) {
			class246_sub9.method3135(var_ha, !aBoolean5096, l, -64, i);
		}
		aLong5100 = l;
		return true;
	}

	private final void method3122(Class35[] class35s, boolean bool) {
		for (int i = 0; i < 8; i++) {
			aBooleanArray5094[i] = false;
		}
		while_104_: for (Class98_Sub46_Sub6 class98_sub46_sub6 = (Class98_Sub46_Sub6) aClass148_5102.getFirst(32); class98_sub46_sub6 != null; class98_sub46_sub6 = (Class98_Sub46_Sub6) aClass148_5102.getNext(91)) {
			if (class35s != null) {
				for (int i = 0; i < class35s.length; i++) {
					if (class98_sub46_sub6.aClass35_5971 == class35s[i] || class98_sub46_sub6.aClass35_5971 == class35s[i].aClass35_328) {
						aBooleanArray5094[i] = true;
						class98_sub46_sub6.method1547(-121);
						continue while_104_;
					}
				}
			}
			if (!bool) {
				class98_sub46_sub6.unlink(111);
				anInt5105--;
				if (class98_sub46_sub6.isCached((byte) 92)) {
					class98_sub46_sub6.uncache((byte) -90);
					Class340.anInt2849--;
				}
			}
		}
		if (class35s != null) {
			for (int i = 0; i < class35s.length; i++) {
				if (i == 8 || anInt5105 == 8) {
					break;
				}
				if (!aBooleanArray5094[i]) {
					Class98_Sub46_Sub6 class98_sub46_sub6 = null;
					if (class35s[i].method331((byte) 110).anInt508 == 1 && Class340.anInt2849 < 32) {
						class98_sub46_sub6 = new Class98_Sub46_Sub6(class35s[i], this);
						Class246_Sub3_Sub3.aClass254_6152.method3185((byte) -14, class98_sub46_sub6, class35s[i].anInt329);
						Class340.anInt2849++;
					}
					if (class98_sub46_sub6 == null) {
						class98_sub46_sub6 = new Class98_Sub46_Sub6(class35s[i], this);
					}
					aClass148_5102.addLast(class98_sub46_sub6, -20911);
					anInt5105++;
					aBooleanArray5094[i] = true;
				}
			}
		}
	}

	public final void method3123(int i, int i_1_, int i_2_, int i_3_, int i_4_) {
		anInt5106 = i;
	}

	private final void method3124(int i, boolean bool) {
		ParticleManager.systems.addLast(true, this);
		aLong5101 = i;
		aLong5100 = i;
		aBoolean5107 = true;
		aBoolean5108 = bool;
	}

	public final void method3126(long l) {
		aLong5101 = l;
	}

	public final void method3127() {
		aBoolean5107 = true;
	}

	private final void method3128() {
		aBoolean5096 = false;
	}

	public final void method3129() {
		aBoolean5099 = true;
		for (Class98_Sub46_Sub6 class98_sub46_sub6 = (Class98_Sub46_Sub6) aClass148_5102.getFirst(32); class98_sub46_sub6 != null; class98_sub46_sub6 = (Class98_Sub46_Sub6) aClass148_5102.getNext(111)) {
			if (class98_sub46_sub6.aClass66_5973.anInt508 == 1) {
				class98_sub46_sub6.uncache((byte) -90);
			}
		}
		for (int i = 0; i < aClass246_Sub4_Sub2_Sub1Array5095.length; i++) {
			if (aClass246_Sub4_Sub2_Sub1Array5095[i] != null) {
				aClass246_Sub4_Sub2_Sub1Array5095[i].method3113();
				aClass246_Sub4_Sub2_Sub1Array5095[i] = null;
			}
		}
		anInt5093 = 0;
		aClass218_5097 = new SceneGraphNodeList();
		anInt5103 = 0;
		aClass148_5102 = new LinkedList();
		anInt5105 = 0;
		unlink((byte) 127);
		SimpleProgressBarLoadingScreenElement.aClass246_Sub5Array5469[Class273.anInt2039] = this;
		Class273.anInt2039 = Class273.anInt2039 + 1 & Class224_Sub1.anIntArray5034[RotatingSpriteLSEConfig.anInt5497];
	}
}

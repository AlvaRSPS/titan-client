/* Class142 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.timetools.general.TimeTools;

public abstract class Class142 {
	public static Class105				aClass105_1159	= new Class105("", 10);
	public static HashTable				aClass377_1157	= new HashTable(32);
	public static AdvancedMemoryCache	aClass79_1158	= new AdvancedMemoryCache(64);
	public static int					anInt1160		= 0;

	public static final int method2307(int i, int i_0_, int i_1_) {
		double d = Math.log(i_0_) / Math.log(2.0);
		double d_2_ = Math.log(i) / Math.log(2.0);
		double d_4_ = d_2_ + Math.random() * (-d_2_ + d);
		return (int) (0.5 + Math.pow(2.0, d_4_));
	}

	public static final void method2309(int i, String string) {
		client.settingsString = string;
		if (GameShell.applet != null) {
			try {
				String string_5_ = GameShell.applet.getParameter("cookieprefix");
				String string_6_ = GameShell.applet.getParameter("cookiehost");
				String string_7_ = string_5_ + "settings=" + string + "; version=1; path=/; domain=" + string_6_;
				if (i != 19208) {
					aClass105_1159 = null;
				}
				do {
					if ((string.length() ^ 0xffffffff) == -1) {
						string_7_ += "; Expires=Thu, 01-Jan-2149 00:00:00 GMT; Max-Age=0";
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					string_7_ += "; Expires=" + NodeShort.method1471(5090, 94608000000L + TimeTools.getCurrentTime(i + -19255)) + "; Max-Age=" + 94608000L;
				} while (false);
				JavaScriptInterface.evalJs(GameShell.applet, "document.cookie=\"" + string_7_ + "\"", 9202);
			} catch (Throwable throwable) {
				/* empty */
			}
		}
	}

	public static final void method2310(byte i) {
		Class232.aClass79_1740.clear(18);
	}

	public Class142() {
		/* empty */
	}

	abstract long method2308(byte i);
}

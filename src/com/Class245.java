/* Class245 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;

public final class Class245 {
	public static Sound[]	aClass338Array1865	= new Sound[50];
	public static Js5		nativesJs5;
	public static Js5		particlesJs5;

	public static void method2955(byte i) {
		try {
			if (i != 67) {
				nativesJs5 = null;
			}
			nativesJs5 = null;
			aClass338Array1865 = null;
			particlesJs5 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ph.F(" + i + ')');
		}
	}

	private Class174			aClass174_1868;
	private SceneGraphNodeList	aClass218_1863	= new SceneGraphNodeList();
	private volatile int		anInt1867;

	String						aString1866;

	public Class245(String string) {
		try {
			aString1866 = string;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ph.<init>(" + (string != null ? "{...}" : "null") + ')');
		}
	}

	public final SceneGraphNode method2956(int i) {
		try {
			SceneGraphNode class246;
			synchronized (aClass218_1863) {
				class246 = aClass218_1863.getFirst((byte) 15);
				class246.unlink((byte) 123);
				anInt1867--;
				if (i != 0) {
					method2958((byte) 124, null);
				}
			}
			return class246;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ph.A(" + i + ')');
		}
	}

	public final void method2957(Class174 class174, boolean bool) {
		try {
			aClass174_1868 = class174;
			if (bool != false) {
				anInt1867 = -25;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ph.D(" + (class174 != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	public final void method2958(byte i, Char class246_sub3) {
		try {
			class246_sub3.aBoolean5078 = true;
			synchronized (aClass218_1863) {
				aClass218_1863.addLast(true, class246_sub3);
				anInt1867++;
			}
			if (aClass174_1868 != null) {
				synchronized (aClass174_1868) {
					aClass174_1868.notify();
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ph.B(" + i + ',' + (class246_sub3 != null ? "{...}" : "null") + ')');
		}
	}

	public final boolean method2959(int i) {
		try {
			if (i < 113) {
				aClass338Array1865 = null;
			}
			return anInt1867 == 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ph.G(" + i + ')');
		}
	}

	public final void method2960(Char class246_sub3, int i) {
		try {
			class246_sub3.aBoolean5078 = false;
			synchronized (aClass218_1863) {
				aClass218_1863.addLast(true, class246_sub3);
				anInt1867++;
			}
			if (i != 0) {
				method2959(22);
			}
			if (aClass174_1868 != null) {
				synchronized (aClass174_1868) {
					aClass174_1868.notify();
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ph.E(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final void method2961(boolean bool, Class246_Sub10 class246_sub10) {
		try {
			synchronized (aClass218_1863) {
				aClass218_1863.addLast(bool, class246_sub10);
				anInt1867++;
			}
			if (aClass174_1868 != null) {
				synchronized (aClass174_1868) {
					aClass174_1868.notify();
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ph.C(" + bool + ',' + (class246_sub10 != null ? "{...}" : "null") + ')');
		}
	}
}

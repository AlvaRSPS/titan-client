/* Class98_Sub19 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class ReflectionRequest extends Node {
	public static int	anInt3955	= 0;
	public static int	anInt3956;

	public static final void method1164(int headIndex, int i_0_, int i_1_, int i_2_, int i_3_) {
		Class98_Sub46_Sub17 dialogue = Class185.method2628(i_3_, -105, i_2_);
		dialogue.method1626((byte) -103);
		dialogue.headId = headIndex;
		dialogue.anInt6054 = i_1_;
		dialogue.anInt6053 = i_0_;
		System.out.println("i: " + i_2_);
	}

	public static final void method1165(byte i, String string) {
		do {
			if (Class98_Sub10_Sub9.aBoolean5585 && (0x18 & Class98_Sub4.anInt3826 ^ 0xffffffff) != -1 && i == 36) {
				boolean bool = false;
				int i_4_ = Class2.playerCount;
				int[] is = Class319.playerIndices;
				for (int i_5_ = 0; (i_5_ ^ 0xffffffff) > (i_4_ ^ 0xffffffff); i_5_++) {
					Player player = Class151_Sub9.players[is[i_5_]];
					if (player.accountName != null && player.accountName.equalsIgnoreCase(string) && (Class87.localPlayer == player && (0x10 & Class98_Sub4.anInt3826) != 0 || player != null && (Class98_Sub4.anInt3826 & 0x8) != 0)) {
						client.anInt3548++;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class160.aClass171_1259, Class331.aClass117_2811);
						class98_sub11.packet.writeLEShortA(is[i_5_], 128);
						class98_sub11.packet.writeShortA(Class310.anInt2652, (byte) 126);
						class98_sub11.packet.writeLEInt(Class187.anInt1450, 1046032984);
						class98_sub11.packet.method1231(0, (byte) -95);
						class98_sub11.packet.writeShortA(GlobalPlayer.anInt3173, (byte) 126);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, 0, player.pathZ[0], player.pathX[0], true, player.getSize(0));
						bool = true;
						break;
					}
				}
				if (!bool) {
					OpenGLHeap.addChatMessage(TextResources.UNABLE_TO_FIND.getText(client.gameLanguage, (byte) 25) + string, 4, (byte) 71);
				}
				if (!Class98_Sub10_Sub9.aBoolean5585) {
					break;
				}
				Class98_Sub10_Sub32.method1098((byte) 119);
			}
			break;
		} while (false);
	}

	public SignLinkRequest[]	fieldRequests;
	public int[]				integerValues;
	public int					link;
	public SignLinkRequest[]	methodRequests;
	public int[]				opcode;
	public int					opCount;

	public byte[][][]			parameterData;

	public int[]				status;

	public ReflectionRequest() {
		/* empty */
	}

}

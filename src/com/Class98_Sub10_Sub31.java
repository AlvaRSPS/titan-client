/* Class98_Sub10_Sub31 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class98_Sub10_Sub31 extends Class98_Sub10 {
	public static Class1[]		aClass1Array5717;
	public static ChatStatus	ON	= new ChatStatus(0);

	public static void method1095(int i) {
		do {
			try {
				aClass1Array5717 = null;
				ON = null;
				if (i == 10640) {
					break;
				}
				aClass1Array5717 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rf.B(" + i + ')');
			}
			break;
		} while (false);
	}

	private int	anInt5714;

	private int	anInt5715	= 4096;

	public Class98_Sub10_Sub31() {
		super(1, true);
		anInt5714 = 0;
	}

	@Override
	public final int[] method990(int i, int i_2_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_2_);
			if (this.aClass16_3863.aBoolean198) {
				int[] is_3_ = method1000(i_2_, 0, 0);
				for (int i_4_ = 0; i_4_ < Class25.anInt268; i_4_++) {
					int i_5_ = is_3_[i_4_];
					is[i_4_] = (i_5_ ^ 0xffffffff) <= (anInt5714 ^ 0xffffffff) && i_5_ <= anInt5715 ? 4096 : 0;
				}
			}
			if (i != 255) {
				ON = null;
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rf.G(" + i + ',' + i_2_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_0_) {
		do {
			try {
				int i_1_ = i;
				do {
					if ((i_1_ ^ 0xffffffff) != -1) {
						if (i_1_ != 1) {
							break;
						}
					} else {
						anInt5714 = class98_sub22.readShort((byte) 127);
						break;
					}
					anInt5715 = class98_sub22.readShort((byte) 127);
				} while (false);
				if (i_0_ < -92) {
					break;
				}
				method990(-115, -128);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rf.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_0_ + ')');
			}
			break;
		} while (false);
	}
}

/* Class372 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.toolkit.ground.OpenGlGround;

public final class Class372 {
	public static boolean			aBoolean3152	= false;
	public static float				aFloat3151;
	public static int				anInt3150;
	public static int[][]			anIntArrayArray3149;
	public static short[]			clientPalette	= new short[256];
	public static IncomingOpcode	VARPBIT_LARGE	= new IncomingOpcode(51, 3);

	static {
		anInt3150 = 0;
	}

	public static final void method3957(RSToolkit var_ha, boolean bool, RtInterface class293) {
		do {
			try {
				if (bool == true) {
					boolean bool_0_ = Class98_Sub46_Sub19.itemDefinitionList.getCachedSprite(class293.rotation, class293.unknown, var_ha, class293.objectUsePlayerAppearence ? Class87.localPlayer.appearence : null, class293.itemStackSize, class293.anInt2305, ~0xffffff | class293.backgroundColour,
							24056) == null;
					if (!bool_0_) {
						break;
					}
					ProxyException.aClass148_30.addLast(new Class98_Sub12(class293.unknown, class293.itemStackSize, class293.rotation, class293.backgroundColour | ~0xffffff, class293.anInt2305, class293.objectUsePlayerAppearence), -20911);
					WorldMapInfoDefinitionParser.setDirty(1, class293);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wj.C(" + (var_ha != null ? "{...}" : "null") + ',' + bool + ',' + (class293 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public static final void method3959(int i, int i_1_, Class98_Sub31_Sub2 class98_sub31_sub2, Js5 class207, int i_2_, int i_3_, boolean bool) {
		do {
			try {
				OpenGlGround.method3434(class207, bool, i_2_, i_1_, i_3_, -16523);
				Class116.aClass98_Sub31_Sub2_965 = class98_sub31_sub2;
				if (i == 256) {
					break;
				}
				anInt3150 = 3;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wj.A(" + i + ',' + i_1_ + ',' + (class98_sub31_sub2 != null ? "{...}" : "null") + ',' + (class207 != null ? "{...}" : "null") + ',' + i_2_ + ',' + i_3_ + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	public int	anInt3145;

	public int	anInt3146;

	public int	anInt3148;

	public Class372() {
		/* empty */
	}
}

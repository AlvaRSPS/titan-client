package com;

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.MemoryCacheNodeFactory;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.anticheat.ReflectionAntiCheat;
import com.jagex.game.clanchat.ClanChatMember;
import com.jagex.game.client.archive.*;
import com.jagex.game.client.definition.*;
import com.jagex.game.client.definition.parser.*;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.*;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.config.*;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsItem;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.constants.BuildType;
import com.jagex.game.input.RtKeyListener;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

import java.io.IOException;

public class PacketParser {

	public static IncomingOpcode	aClass58_5466			= new IncomingOpcode(18, 3);
	public static RsBitsBuffers		buffer					= new RsBitsBuffers(5000);
	public static IncomingOpcode	CURRENT_INCOMING_OPCODE	= null;

	public static final boolean method3967(int i) throws IOException {
		try {
			if (aa_Sub1.aClass123_3561 == null) {
				return false;
			}
			if (CURRENT_INCOMING_OPCODE == null) {
				if (Class98_Sub43.aBoolean4243) {
					if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
						return false;
					}
					aa_Sub1.aClass123_3561.read(buffer.payload, 0, 2047, 1);
					Class224_Sub1.anInt5031 = 0;
					Class103.anInt892++;
					Class98_Sub43.aBoolean4243 = false;
				}
				buffer.position = 0;
				if (buffer.xzSmart((byte) 54)) {
					if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
						return false;
					}
					aa_Sub1.aClass123_3561.read(buffer.payload, 1, 2047, 1);
					Class103.anInt892++;
					Class224_Sub1.anInt5031 = 0;
				}
				Class98_Sub43.aBoolean4243 = true;
				IncomingOpcode[] opcodes = PlayerAppearence.getAll(125);
				int i_14_ = buffer.xgSmart(0);
				if (i_14_ < 0 || i_14_ >= opcodes.length) {
					throw new IOException("invo:" + i_14_ + " ip:" + buffer.position);
				}
				CURRENT_INCOMING_OPCODE = opcodes[i_14_];
				Class65.currentPacketSize = CURRENT_INCOMING_OPCODE.anInt460;
			}
			if ((Class65.currentPacketSize ^ 0xffffffff) == 0) {
				if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 1)) {
					return false;
				}
				aa_Sub1.aClass123_3561.read(buffer.payload, 0, 2047, 1);
				Class224_Sub1.anInt5031 = 0;
				Class65.currentPacketSize = buffer.payload[0] & 0xff;
				Class103.anInt892++;
			}
			if (Class65.currentPacketSize == -2) {
				if (!aa_Sub1.aClass123_3561.isBuffered(-1949, 2)) {
					return false;
				}
				aa_Sub1.aClass123_3561.read(buffer.payload, 0, 2047, 2);
				buffer.position = 0;
				Class65.currentPacketSize = buffer.readShort((byte) 127);
				Class103.anInt892 += 2;
				Class224_Sub1.anInt5031 = 0;
			}
			if ((Class65.currentPacketSize ^ 0xffffffff) < -1) {
				if (!aa_Sub1.aClass123_3561.isBuffered(-1949, Class65.currentPacketSize)) {
					return false;
				}
				buffer.position = 0;
				aa_Sub1.aClass123_3561.read(buffer.payload, 0, i + -525198532, Class65.currentPacketSize);
				Class224_Sub1.anInt5031 = 0;
				Class103.anInt892 += Class65.currentPacketSize;
			}
			Class98_Sub10_Sub21.aClass58_5641 = ProceduralTextureSource.aClass58_3262;
			ProceduralTextureSource.aClass58_3262 = Class98_Sub30.aClass58_4094;
			Class98_Sub30.aClass58_4094 = CURRENT_INCOMING_OPCODE;
			if (CURRENT_INCOMING_OPCODE == RtKeyListener.aClass58_591) {
				CURRENT_INCOMING_OPCODE = null;
				return false;
			}
			if (SkyboxDefinition.VARP_LARGE == CURRENT_INCOMING_OPCODE) {
				int value = buffer.readInt1(false);
				int id = buffer.readShort((byte) 127);
				StartupStage.varValues.setConfig(-7469, value, id);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (aa_Sub3.aClass58_3566 == CURRENT_INCOMING_OPCODE) {
				boolean bool = buffer.readUnsignedByte((byte) 64) == 1;
				String string = buffer.readString((byte) 84);
				String string_17_ = string;
				if (bool) {
					string_17_ = buffer.readString((byte) 84);
				}
				int i_18_ = buffer.readUnsignedByte((byte) 81);
				boolean bool_19_ = false;
				if (i_18_ <= 1) {
					if (Js5Manager.aBoolean933 && !Class98_Sub10_Sub35.aBoolean5732 || BaseModel.isQuickChatWorld) {
						bool_19_ = true;
					} else if (i_18_ <= 1 && Class14.isIgnored(string_17_, (byte) 113)) {
						bool_19_ = true;
					}
				}
				if (!bool_19_ && (Class22.anInt216 ^ 0xffffffff) == -1) {
					String string_20_ = Class249.escapeHtml(OpenGlElementBufferPointer.decodeString(buffer, (byte) 72), 62);
					if ((i_18_ ^ 0xffffffff) == -3) {
						Class137.addChatMessage(string, 24, "<img=1>" + string, string_20_, -1, null, (byte) -67, 0, "<img=1>" + string_17_);
					} else if ((i_18_ ^ 0xffffffff) == -2) {
						Class137.addChatMessage(string, 24, "<img=0>" + string, string_20_, -1, null, (byte) -82, 0, "<img=0>" + string_17_);
					} else {
						Class137.addChatMessage(string, 24, string, string_20_, -1, null, (byte) -63, 0, string_17_);
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				System.out.println("Reached line 138");
				return true;
			}
			if (Class284_Sub1.aClass58_5176 == CURRENT_INCOMING_OPCODE) {
				int i_21_ = buffer.readInt(-2);
				int i_22_ = buffer.readShort((byte) 127);
				Class98_Sub25.method1274(i + -525200580);
				OpenGLHeap.method1680(i_22_, i_21_, 9767);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub46_Sub19.aClass58_6057) {
				int i_23_ = buffer.readLEShortA((byte) 99);
				if (i_23_ == 65535) {
					i_23_ = -1;
				}
				int i_24_ = buffer.readInt(-2);
				int i_25_ = buffer.readInt2(-43);
				Class98_Sub25.method1274(i ^ ~0x1f4decc3);
				CharacterShadowsPreferenceField.method660(i_25_, i_24_, 113, i_23_);
				ItemDefinition class297 = Class98_Sub46_Sub19.itemDefinitionList.get(i_23_, (byte) -125);
				Class353.method3868(class297.zoom2D, i_25_, (byte) -104, class297.xAngle2D, class297.yAngle2D);
				Class323.method3675(i_25_, class297.xOffset2D, true, class297.yOffset2D, class297.zAngle2D);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class39.aClass58_364) {
				Class98_Sub25.method1274(-1);
				GameObjectDefinition.method3856((byte) 1);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (aClass58_5466 == CURRENT_INCOMING_OPCODE) {
				int i_26_ = buffer.readShortA(i ^ 0x1f4decad);
				int i_27_ = buffer.readByteA(true);
				Class98_Sub25.method1274(-1);
				Class98_Sub10_Sub30.method1093(-29680, i_27_, true, i_26_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Js5.aClass58_1576) {
				Class98_Sub10_Sub22.method1070((byte) -45, Class242.aClass85_1849);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == OpenGlElementBufferPointer.ATTACH_INTERFACE) {
				int i_28_ = buffer.readInt2(-84);
				int interfaceId = buffer.readLEShortA((byte) -106);
				int slotId = buffer.readByteC((byte) -126);
				Class98_Sub25.method1274(-1);
				RtInterfaceAttachment interfAttachment = (RtInterfaceAttachment) Class116.attachmentMap.get(i_28_, i ^ ~0x1f4decc3);
				if (interfAttachment != null) {
					RtInterfaceAttachment.detachInterface(16398, false, interfAttachment, interfaceId != interfAttachment.attachedInterfaceId);
				}
				RtInterfaceAttachment.attachInterface(false, -126, interfaceId, i_28_, slotId);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == OpenGlGround.aClass58_5205) {
				int i_31_ = buffer.readShort((byte) 127);
				int i_32_ = buffer.readInt2(-89);
				Class98_Sub25.method1274(-1);
				SeekableFile.method2848(i_32_, 17, i_31_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == GameObjectDefinition.aClass58_2943) {
				Node.tradeStatus = buffer.readByteA(true);
				Class265.friendsChatStatus = buffer.readByteS(-126);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (RtMouseListener.aClass58_2495 == CURRENT_INCOMING_OPCODE) {
				String string = buffer.readString((byte) 84);
				int i_33_ = buffer.readShort((byte) 127);
				String string_34_ = NewsLSEConfig.quickChatMessageList.get(i_33_, 67).decodeMessage(15281, buffer);
				Class137.addChatMessage(string, 19, string, string_34_, i_33_, null, (byte) -93, 0, string);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (GameObjectDefinitionParser.SEND_GROUND_ITEM == CURRENT_INCOMING_OPCODE) {
				Class98_Sub10_Sub22.method1070((byte) -45, BuildType.SEND_GROUND_ITEM);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class98_Sub10_Sub14.aClass58_5606 == CURRENT_INCOMING_OPCODE) {
				Class98_Sub10_Sub22.method1070((byte) -45, RectangleLoadingScreenElement.aClass85_3454);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class76_Sub2.aClass58_3731 == CURRENT_INCOMING_OPCODE) {
				ReflectionAntiCheat.decodeRequest(i + -525200579, GameShell.signLink, buffer, Class65.currentPacketSize);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class151_Sub8.VARCSTR_LARGE == CURRENT_INCOMING_OPCODE) {
				int i_35_ = buffer.readShort((byte) 127);
				String string = buffer.readString((byte) 84);
				Class98_Sub25.method1274(-1);
				Class94.setVarcstr(string, i_35_, 52);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Huffman.aClass58_1609) {
				Class142.method2309(19208, buffer.readString((byte) 84));
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (RsFloatBuffer.aClass58_5793 == CURRENT_INCOMING_OPCODE) {
				int interfaceId = buffer.readInt2(-124);
				int i_37_ = buffer.readInt(i + -525200581);
				Class98_Sub25.method1274(-1);
				RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(i_37_, -1);
				RtInterfaceAttachment class98_sub18_38_ = (RtInterfaceAttachment) Class116.attachmentMap.get(interfaceId, -1);
				if (class98_sub18_38_ != null) {
					RtInterfaceAttachment.detachInterface(16398, false, class98_sub18_38_, class98_sub18 == null || class98_sub18.attachedInterfaceId != class98_sub18_38_.attachedInterfaceId);
				}
				if (class98_sub18 != null) {
					class98_sub18.unlink(i ^ 0x1f4dec9a);
					Class116.attachmentMap.put(class98_sub18, interfaceId, i ^ ~0x1f4decc3);
				}
				RtInterface interf = RtInterface.getInterface(i_37_, -9820);
				if (interf != null) {
					WorldMapInfoDefinitionParser.setDirty(1, interf);
				}
				interf = RtInterface.getInterface(interfaceId, -9820);
				if (interf != null) {
					WorldMapInfoDefinitionParser.setDirty(i + -525200578, interf);
					RtInterface.calculateLayout(interf, true, (byte) -117);
				}
				if (client.topLevelInterfaceId != -1) {
					Js5.method2764(1, client.topLevelInterfaceId, -43);
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (FriendLoginUpdate.aClass58_6166 == CURRENT_INCOMING_OPCODE) {
				boolean bool = buffer.readUnsignedByte((byte) 15) == 1;
				byte[] is = new byte[Class65.currentPacketSize - 1];
				buffer.getData(is, true, -1 + Class65.currentPacketSize, 0);
				Node.method943(is, bool, false);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class232.SEND_FRIEND == CURRENT_INCOMING_OPCODE) {
				while (buffer.position < Class65.currentPacketSize) {
					boolean unknown = buffer.readUnsignedByte((byte) 102) == 1;
					String friendName = buffer.readString((byte) 84);
					String friendDisplayName = buffer.readString((byte) 84);
					int friendOnline = buffer.readShort((byte) 127);
					int i_41_ = buffer.readUnsignedByte((byte) 87);
					String world = "";
					boolean bool_43_ = false;
					if (friendOnline > 0) {
						world = buffer.readString((byte) 84);
						bool_43_ = buffer.readUnsignedByte((byte) -100) == 1;
					}

					for (int count = 0; (count ^ 0xffffffff) > (Class314.friendListSize ^ 0xffffffff); count++) {
						if (!unknown) {
							if (friendName.equals(Class98_Sub25.friends[count])) {
								if (friendOnline != GroundItem.friendsOnline[count]) {
									boolean existing = true;
									for (FriendLoginUpdate update = (FriendLoginUpdate) Class119.friendLogins.getFirst((byte) 15); update != null; update = (FriendLoginUpdate) Class119.friendLogins.getNext(false)) {
										if (update.friendName.equals(friendName)) {
											if ((friendOnline ^ 0xffffffff) != -1 && (update.onlineStatus ^ 0xffffffff) == -1) {
												existing = false;
												update.unlink((byte) -72);
											} else if (friendOnline == 0 && (update.onlineStatus ^ 0xffffffff) != -1) {
												existing = false;
												update.unlink((byte) 126);
											}
										}
									}
									if (existing) {
										Class119.friendLogins.addLast(true, new FriendLoginUpdate(friendName, friendOnline));
									}
									GroundItem.friendsOnline[count] = friendOnline;
								}
								TextLSEConfig.friendsDisplayNames[count] = friendDisplayName;
								Class98_Sub10_Sub17.worlds[count] = world;
								Class69.anIntArray3222[count] = i_41_;
								friendName = null;
								aa_Sub3.aBooleanArray3575[count] = bool_43_;
								System.out.println(world + "" + friendDisplayName);
								break;
							}
						} else if (friendDisplayName.equals(Class98_Sub25.friends[count])) {
							Class98_Sub25.friends[count] = friendName;
							friendName = null;
							TextLSEConfig.friendsDisplayNames[count] = friendDisplayName;
							break;
						}
					}
					if (friendName != null && (Class314.friendListSize ^ 0xffffffff) > -201) {
						Class98_Sub25.friends[Class314.friendListSize] = friendName;
						TextLSEConfig.friendsDisplayNames[Class314.friendListSize] = friendDisplayName;
						GroundItem.friendsOnline[Class314.friendListSize] = friendOnline;
						Class98_Sub10_Sub17.worlds[Class314.friendListSize] = world;
						Class69.anIntArray3222[Class314.friendListSize] = i_41_;
						aa_Sub3.aBooleanArray3575[Class314.friendListSize] = bool_43_;
						Class314.friendListSize++;
					}
				}
				NewsFetcher.anInt3099 = WorldMapInfoDefinition.logicTimer;
				Class98_Sub28.totalBoxVisibility = 2;
				boolean bool = false;
				int friends = Class314.friendListSize;
				while ((friends ^ 0xffffffff) < -1) {
					bool = true;
					friends--;
					for (int count = 0; (friends ^ 0xffffffff) < (count ^ 0xffffffff); count++) {
						if (GroundItem.friendsOnline[count] != client.server.index && (GroundItem.friendsOnline[1 + count] ^ 0xffffffff) == (client.server.index ^ 0xffffffff) || GroundItem.friendsOnline[count] == 0 && GroundItem.friendsOnline[1 + count] != 0) {
							int friend = GroundItem.friendsOnline[count];
							GroundItem.friendsOnline[count] = GroundItem.friendsOnline[count + 1];
							GroundItem.friendsOnline[1 + count] = friend;
							String string = Class98_Sub10_Sub17.worlds[count];
							Class98_Sub10_Sub17.worlds[count] = Class98_Sub10_Sub17.worlds[1 + count];
							Class98_Sub10_Sub17.worlds[1 + count] = string;
							String string_49_ = Class98_Sub25.friends[count];
							Class98_Sub25.friends[count] = Class98_Sub25.friends[count - -1];
							Class98_Sub25.friends[count + 1] = string_49_;
							String string_50_ = TextLSEConfig.friendsDisplayNames[count];
							TextLSEConfig.friendsDisplayNames[count] = TextLSEConfig.friendsDisplayNames[1 + count];
							TextLSEConfig.friendsDisplayNames[count + 1] = string_50_;
							int i_51_ = Class69.anIntArray3222[count];
							Class69.anIntArray3222[count] = Class69.anIntArray3222[1 + count];
							Class69.anIntArray3222[count + 1] = i_51_;
							boolean bool_52_ = aa_Sub3.aBooleanArray3575[count];
							aa_Sub3.aBooleanArray3575[count] = aa_Sub3.aBooleanArray3575[1 + count];
							bool = false;
							aa_Sub3.aBooleanArray3575[1 + count] = bool_52_;
							System.out.println(i_51_ + "" + string_49_);
						}
					}
					if (bool) {
						break;
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class188.aClass58_1452 == CURRENT_INCOMING_OPCODE) { // llll
				int i_53_ = buffer.readLEShortA((byte) -114);
				int i_54_ = buffer.readByteA(true);
				int i_55_ = buffer.readByteS(108);
				int i_56_ = buffer.readByteS(-110);
				int i_57_ = buffer.readByteC((byte) 122);
				Class98_Sub25.method1274(-1);
				GroupProgressMonitor.aBooleanArray3410[i_57_] = true;
				aa_Sub3.anIntArray3571[i_57_] = i_54_;
				Class98_Sub10_Sub13.anIntArray5603[i_57_] = i_56_;
				GraphicsBuffer.anIntArray4109[i_57_] = i_55_;
				QuickChatCategoryParser.anIntArray1597[i_57_] = i_53_;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class246_Sub4_Sub2.SEND_RUN_ENERGY == CURRENT_INCOMING_OPCODE) {
				NewsItem.runEnergyLevel = buffer.readUnsignedByte((byte) 48);
				CURRENT_INCOMING_OPCODE = null;
				Class98_Sub36.anInt4161 = WorldMapInfoDefinition.logicTimer;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class151_Sub6.aClass58_4999) {
				Class98_Sub41.decodeMapArea(119);
				CURRENT_INCOMING_OPCODE = null;
				return false;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub41.aClass58_4199) {
				Class98_Sub10_Sub22.method1070((byte) -45, SpriteProgressBarLoadingScreenElement.aClass85_5474);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class40.aClass58_369) {
				buffer.position += 28;
				if (buffer.method1210(-114)) {
					Class76_Sub7.method762(buffer.position + -28, buffer, true);
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == RtKeyListener.aClass58_592) {
				int i_58_ = buffer.readInt(-2);
				Class98_Sub25.method1274(i + -525200580);
				if (i_58_ != -1) {
					int i_59_ = (0xfffeabc & i_58_) >> 884258670;
					int i_60_ = i_58_ & 0x3fff;
					i_59_ -= Class272.gameSceneBaseX;
					i_60_ -= aa_Sub2.gameSceneBaseY;
					if ((i_59_ ^ 0xffffffff) > -1) {
						i_59_ = 0;
					} else if (Class165.mapWidth <= i_59_) {
						i_59_ = Class165.mapWidth;
					}
					Class116.anInt967 = 256 + (i_59_ << -1786298711);
					if ((i_60_ ^ 0xffffffff) <= -1) {
						if (Class98_Sub10_Sub7.mapLength <= i_60_) {
							i_60_ = Class98_Sub10_Sub7.mapLength;
						}
					} else {
						i_60_ = 0;
					}
					CharacterShadowsPreferenceField.anInt3712 = 256 + (i_60_ << 1215708553);
				} else {
					CharacterShadowsPreferenceField.anInt3712 = -1;
					Class116.anInt967 = -1;
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == TextLSEConfig.aClass58_3533) {
				int i_61_ = buffer.readByteA(true);
				int i_62_ = buffer.readByteS(i + -525200499);
				int i_63_ = buffer.readUnsignedByte((byte) 127);
				int i_64_ = buffer.readByteC((byte) -120);
				int i_65_ = buffer.readShort1((byte) 107) << -1408209406;
				Class98_Sub25.method1274(i + -525200580);
				FileRequest.method1592(-25686, i_64_, i_62_, i_65_, i_61_, i_63_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class358.aClass58_3029 == CURRENT_INCOMING_OPCODE) {
				Class98_Sub10_Sub22.method1070((byte) -45, LightningDetailPreferenceField.aClass85_3667);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (FileProgressMonitor.aClass58_3398 == CURRENT_INCOMING_OPCODE) {
				int i_66_ = buffer.readInt1(false);
				int i_67_ = buffer.readLEShortA((byte) 91);
				int i_68_ = buffer.readLEShortA((byte) -81);
				int i_69_ = buffer.readShort((byte) 127);
				int i_70_ = buffer.readByteC((byte) -124);
				boolean bool = (0x80 & i_70_ ^ 0xffffffff) != -1;
				int i_71_ = i_70_ & 0x7;
				int i_72_ = 0xf & i_70_ >> 525200579;
				if (i_72_ == 15) {
					i_72_ = -1;
				}
				if (i_66_ >> -1165516802 != 0) {
					int i_73_ = i_66_ >> 1762702268 & 0x3;
					int i_74_ = ((i_66_ & 0xfffdb84) >> -1260772882) - Class272.gameSceneBaseX;
					int i_75_ = -aa_Sub2.gameSceneBaseY + (0x3fff & i_66_);
					if (i_74_ >= 0 && i_75_ >= 0 && Class165.mapWidth > i_74_ && (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) < (i_75_ ^ 0xffffffff)) {
						int i_76_ = 512 * i_74_ + 256;
						int i_77_ = 256 + i_75_ * 512;
						int i_78_ = i_73_;
						if ((i_78_ ^ 0xffffffff) > -4 && Class1.isBridge(i_75_, (byte) -118, i_74_)) {
							i_78_++;
						}
						Class246_Sub3_Sub4_Sub3 class246_sub3_sub4_sub3 = new Class246_Sub3_Sub4_Sub3(i_69_, i_68_, Queue.timer, i_73_, i_78_, i_76_, -i_67_ + StrongReferenceMCNode.getHeight(i_73_, i_77_, i_76_, 24111), i_77_, i_74_, i_74_, i_75_, i_75_, i_71_);
						Class98_Sub10_Sub11.animatedObjects.addLast(new Class98_Sub46_Sub3(class246_sub3_sub4_sub3), -20911);
					}
				} else if ((i_66_ >> -602350499 ^ 0xffffffff) == -1) {
					if ((i_66_ >> 1424175516 ^ 0xffffffff) != -1) {
						int i_79_ = i_66_ & 0xffff;
						Player player;
						if (i_79_ == OpenGLHeap.localPlayerIndex) {
							player = Class87.localPlayer;
						} else {
							player = Class151_Sub9.players[i_79_];
						}
						if (player != null) {
							if (i_69_ == 65535) {
								i_69_ = -1;
							}
							boolean bool_80_ = true;
							int i_81_ = bool ? player.anInt6365 : player.anInt6379;
							if (i_69_ != -1 && (i_81_ ^ 0xffffffff) != 0) {
								if ((i_69_ ^ 0xffffffff) != (i_81_ ^ 0xffffffff)) {
									GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, i_69_);
									GraphicsDefinition class107_82_ = BuildLocation.gfxDefinitionList.method3564(2, i_81_);
									if ((class107.animation ^ 0xffffffff) != 0 && class107_82_.animation != -1) {
										AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383);
										AnimationDefinition class97_83_ = Class151_Sub7.animationDefinitionList.method2623(class107_82_.animation, 16383);
										if ((class97.anInt829 ^ 0xffffffff) > (class97_83_.anInt829 ^ 0xffffffff)) {
											bool_80_ = false;
										}
									}
								} else {
									GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, i_69_);
									if (class107.aBoolean909 && (class107.animation ^ 0xffffffff) != 0) {
										AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class107.animation, i + -525184196);
										int i_84_ = class97.anInt819;
										if ((i_84_ ^ 0xffffffff) == -1 || (i_84_ ^ 0xffffffff) == -3) {
											bool_80_ = false;
										} else if ((i_84_ ^ 0xffffffff) == -2) {
											bool_80_ = true;
										}
									}
								}
							}
							if (bool_80_) {
								if (!bool) {
									player.anInt6376 = 0;
									player.anInt6396 = 0;
									player.anInt6367 = 1;
									player.anInt6410 = i_72_;
									player.anInt6379 = i_69_;
									player.anInt6382 = i_67_;
									player.anInt6389 = i_71_;
									player.anInt6391 = i_68_ + Queue.timer;
									if ((player.anInt6391 ^ 0xffffffff) < (Queue.timer ^ 0xffffffff)) {
										player.anInt6376 = -1;
									}
									if ((player.anInt6379 ^ 0xffffffff) == -65536) {
										player.anInt6379 = -1;
									}
									if (player.anInt6379 != -1 && (player.anInt6391 ^ 0xffffffff) == (Queue.timer ^ 0xffffffff)) {
										int i_85_ = BuildLocation.gfxDefinitionList.method3564(i + -525200577, player.anInt6379).animation;
										if (i_85_ != -1) {
											AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_85_, 16383);
											if (class97 != null && class97.frameIds != null && !player.aBoolean6371) {
												Class349.method3840((byte) -128, player, 0, class97);
											}
										}
									}
								} else {
									player.anInt6427 = 0;
									player.anInt6426 = i_68_ + Queue.timer;
									player.anInt6353 = i_72_;
									player.anInt6365 = i_69_;
									player.anInt6360 = i_71_;
									player.anInt6428 = 0;
									player.anInt6363 = i_67_;
									player.anInt6421 = 1;
									if ((Queue.timer ^ 0xffffffff) > (player.anInt6426 ^ 0xffffffff)) {
										player.anInt6428 = -1;
									}
									if ((player.anInt6365 ^ 0xffffffff) == -65536) {
										player.anInt6365 = -1;
									}
									if (player.anInt6365 != -1 && player.anInt6426 == Queue.timer) {
										int i_86_ = BuildLocation.gfxDefinitionList.method3564(2, player.anInt6365).animation;
										if (i_86_ != -1) {
											AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_86_, i ^ 0x1f4dd33c);
											if (class97 != null && class97.frameIds != null && !player.aBoolean6371) {
												Class349.method3840((byte) -128, player, 0, class97);
											}
										}
									}
								}
							}
						}
					}
				} else {
					int i_87_ = i_66_ & 0xffff;
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(i_87_, -1);
					if (class98_sub39 != null) {
						NPC npc = class98_sub39.npc;
						if (i_69_ == 65535) {
							i_69_ = -1;
						}
						boolean bool_88_ = true;
						int i_89_ = bool ? npc.anInt6365 : npc.anInt6379;
						if ((i_69_ ^ 0xffffffff) != 0 && (i_89_ ^ 0xffffffff) != 0) {
							if ((i_69_ ^ 0xffffffff) == (i_89_ ^ 0xffffffff)) {
								GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(i ^ 0x1f4decc1, i_69_);
								if (class107.aBoolean909 && (class107.animation ^ 0xffffffff) != 0) {
									AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383);
									int i_90_ = class97.anInt819;
									if (i_90_ != 0 && i_90_ != 2) {
										if (i_90_ == 1) {
											bool_88_ = true;
										}
									} else {
										bool_88_ = false;
									}
								}
							} else {
								GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, i_69_);
								GraphicsDefinition class107_91_ = BuildLocation.gfxDefinitionList.method3564(2, i_89_);
								if ((class107.animation ^ 0xffffffff) != 0 && (class107_91_.animation ^ 0xffffffff) != 0) {
									AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383);
									AnimationDefinition class97_92_ = Class151_Sub7.animationDefinitionList.method2623(class107_91_.animation, 16383);
									if ((class97_92_.anInt829 ^ 0xffffffff) < (class97.anInt829 ^ 0xffffffff)) {
										bool_88_ = false;
									}
								}
							}
						}
						if (bool_88_) {
							if (bool) {
								npc.anInt6426 = i_68_ + Queue.timer;
								npc.anInt6353 = i_72_;
								npc.anInt6427 = 0;
								npc.anInt6360 = i_71_;
								npc.anInt6365 = i_69_;
								npc.anInt6428 = 0;
								npc.anInt6421 = 1;
								npc.anInt6363 = i_67_;
								if (Queue.timer < npc.anInt6426) {
									npc.anInt6428 = -1;
								}
								if (npc.anInt6365 != -1 && Queue.timer == npc.anInt6426) {
									int i_93_ = BuildLocation.gfxDefinitionList.method3564(2, npc.anInt6365).animation;
									if (i_93_ != -1) {
										AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_93_, 16383);
										if (class97 != null && class97.frameIds != null && !npc.aBoolean6371) {
											Class349.method3840((byte) -128, npc, 0, class97);
										}
									}
								}
							} else {
								npc.anInt6367 = 1;
								npc.anInt6389 = i_71_;
								npc.anInt6376 = 0;
								npc.anInt6396 = 0;
								npc.anInt6379 = i_69_;
								npc.anInt6410 = i_72_;
								npc.anInt6391 = Queue.timer - -i_68_;
								npc.anInt6382 = i_67_;
								if (npc.anInt6391 > Queue.timer) {
									npc.anInt6376 = -1;
								}
								if (npc.anInt6379 != -1 && (npc.anInt6391 ^ 0xffffffff) == (Queue.timer ^ 0xffffffff)) {
									int i_94_ = BuildLocation.gfxDefinitionList.method3564(2, npc.anInt6379).animation;
									if ((i_94_ ^ 0xffffffff) != 0) {
										AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_94_, 16383);
										if (class97 != null && class97.frameIds != null && !npc.aBoolean6371) {
											Class349.method3840((byte) -128, npc, 0, class97);
										}
									}
								}
							}
						}
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub12.aClass58_3877) {
				HitmarksDefinition.chatStatus = Class98_Sub10_Sub8.setChatStatus((byte) -107, buffer.readUnsignedByte((byte) -108));
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (OpenGLRenderEffectManager.aClass58_433 == CURRENT_INCOMING_OPCODE) {
				Class98_Sub10_Sub22.method1070((byte) -45, Class35.aClass85_332);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == ParamDefinition.SEND_MESSAGE) {
				int type = buffer.readSmart(1689622712);
				int flags = buffer.readInt(-2);
				int i_97_ = buffer.readUnsignedByte((byte) 122);
				String displayName = "";
				String usernameFrom = displayName;
				if ((i_97_ & 0x1 ^ 0xffffffff) != -1) {
					displayName = buffer.readString((byte) 84);
					if ((i_97_ & 0x2 ^ 0xffffffff) != -1) {
						usernameFrom = buffer.readString((byte) 84);
					} else {
						usernameFrom = displayName;
					}
				}
				String message = buffer.readString((byte) 84);
				if (type != 99) {
					if (!usernameFrom.equals("") && Class14.isIgnored(usernameFrom, (byte) 125)) {
						CURRENT_INCOMING_OPCODE = null;
						return true;
					}
					ItemDeque.addChatMessage((byte) -113, type, message, flags, usernameFrom, displayName, displayName);
				} else {
					Cacheable.write(message, -92);
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (QuickChatMessageType.aClass58_2912 == CURRENT_INCOMING_OPCODE) {
				Class98_Sub10_Sub22.method1070((byte) -45, Class98_Sub23.aClass85_4007);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub10_Sub15.aClass58_5615) {
				String displayName = buffer.readString((byte) 84);
				boolean hasNoDisplayName = (buffer.readUnsignedByte((byte) -103) ^ 0xffffffff) == -2;
				String accountName;
				if (hasNoDisplayName) {
					accountName = buffer.readString((byte) 84);
				} else {
					accountName = displayName;
				}
				int colour = buffer.readShort((byte) 127);
				byte rank = buffer.readSignedByte((byte) -19);
				boolean isJMod = false;
				if (rank == -128) {
					isJMod = true;
				}
				if (!isJMod) {
					String world = buffer.readString((byte) 84);
					ClanChatMember member = new ClanChatMember();
					member.displayName = displayName;
					member.accountName = accountName;
					member.filteredName = Class353.filterName(-1, member.accountName);
					member.chatRank = rank;
					member.worldOrLobbyColour = colour;
					member.world = world;
					int memb;
					for (memb = -1 + FloorOverlayDefinitionParser.clanChatSize; (memb ^ 0xffffffff) <= -1; memb--) {
						int value = WhirlpoolGenerator.clanChat[memb].filteredName.compareTo(member.filteredName);
						if ((value ^ 0xffffffff) == -1) {
							WhirlpoolGenerator.clanChat[memb].worldOrLobbyColour = colour;
							WhirlpoolGenerator.clanChat[memb].chatRank = rank;
							WhirlpoolGenerator.clanChat[memb].world = world;
							if (accountName.equals(Class87.localPlayer.accountName)) {
								Matrix.clanChatRank = rank;
							}
							VolumePreferenceField.anInt3705 = WorldMapInfoDefinition.logicTimer;
							CURRENT_INCOMING_OPCODE = null;
							return true;
						}
						if (value < 0) {
							break;
						}
					}
					if ((FloorOverlayDefinitionParser.clanChatSize ^ 0xffffffff) <= (WhirlpoolGenerator.clanChat.length ^ 0xffffffff)) {
						CURRENT_INCOMING_OPCODE = null;
						return true;
					}
					for (int chatMember = FloorOverlayDefinitionParser.clanChatSize + -1; (memb ^ 0xffffffff) > (chatMember ^ 0xffffffff); chatMember--) {
						WhirlpoolGenerator.clanChat[chatMember - -1] = WhirlpoolGenerator.clanChat[chatMember];
					}
					if (FloorOverlayDefinitionParser.clanChatSize == 0) {
						WhirlpoolGenerator.clanChat = new ClanChatMember[100];
					}
					WhirlpoolGenerator.clanChat[memb - -1] = member;
					FloorOverlayDefinitionParser.clanChatSize++;
					if (accountName.equals(Class87.localPlayer.accountName)) {
						Matrix.clanChatRank = rank;
					}

				} else {
					if ((FloorOverlayDefinitionParser.clanChatSize ^ 0xffffffff) == -1) {
						CURRENT_INCOMING_OPCODE = null;
						return true;
					}
					int chatMember;
					for (chatMember = 0; (chatMember ^ 0xffffffff) > (FloorOverlayDefinitionParser.clanChatSize ^ 0xffffffff); chatMember++) {
						if (WhirlpoolGenerator.clanChat[chatMember].accountName.equals(accountName) && WhirlpoolGenerator.clanChat[chatMember].worldOrLobbyColour == colour) {
							break;
						}
					}
					if (FloorOverlayDefinitionParser.clanChatSize > chatMember) {
						for (/**/; (chatMember ^ 0xffffffff) > (FloorOverlayDefinitionParser.clanChatSize - 1 ^ 0xffffffff); chatMember++) {
							WhirlpoolGenerator.clanChat[chatMember] = WhirlpoolGenerator.clanChat[chatMember - -1];
						}
						FloorOverlayDefinitionParser.clanChatSize--;
						WhirlpoolGenerator.clanChat[FloorOverlayDefinitionParser.clanChatSize] = null;
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				VolumePreferenceField.anInt3705 = WorldMapInfoDefinition.logicTimer;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == AwtGraphicsBuffer.aClass58_5883) {
				int i_110_ = buffer.readInt2(-91);
				int i_111_ = buffer.readByteC((byte) 59);
				Class98_Sub25.method1274(-1);
				Class305_Sub1.method3587(i_111_, -39, i_110_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == RtInterfaceAttachment.VARCSTR_SMALL) {
				String name = buffer.readString((byte) 84);
				int id = buffer.readLEShortA((byte) 101);
				Class98_Sub25.method1274(i + -525200580);
				Class94.setVarcstr(name, id, 68);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (DummyInputStream.aClass58_28 == CURRENT_INCOMING_OPCODE) {
				int i_113_ = buffer.readShort((byte) 127);
				if ((i_113_ ^ 0xffffffff) == -65536) {
					i_113_ = -1;
				}
				int i_114_ = buffer.readUnsignedByte((byte) -123);
				int i_115_ = buffer.readShort((byte) 127);
				int i_116_ = buffer.readUnsignedByte((byte) -118);
				Class98_Sub10_Sub9.method1036(i ^ ~0x6bb6e430, i_116_, i_114_, i_113_, true, i_115_, 256);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class69_Sub2.aClass58_5333) {
				int i_117_ = buffer.readLEShortA((byte) -31);
				int i_118_ = buffer.readIntReverse(true);
				Class98_Sub25.method1274(-1);
				ToolkitPreferenceField.method585(i_117_, (byte) -85, i_118_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == DecoratedProgressBarLSEConfig.SEND_PUBLIC_CHAT_MESSAGE) {
				int spawnIndex = buffer.readShort((byte) 127);
				Player player;
				if (spawnIndex != OpenGLHeap.localPlayerIndex) {
					player = Class151_Sub9.players[spawnIndex];
				} else {
					player = Class87.localPlayer;
				}
				if (player == null) {
					CURRENT_INCOMING_OPCODE = null;
					return true;
				}
				int effects = buffer.readShort((byte) 127);
				int rights = buffer.readUnsignedByte((byte) -112);
				boolean onlQuickchatAvailable = (0x8000 & effects ^ 0xffffffff) != -1;
				if (player.accountName != null && player.appearence != null) {
					boolean hideMessage = false;
					if ((rights ^ 0xffffffff) >= -2) {
						if (!onlQuickchatAvailable && (Js5Manager.aBoolean933 && !Class98_Sub10_Sub35.aBoolean5732 || BaseModel.isQuickChatWorld)) {
							hideMessage = true;
						} else if (Class14.isIgnored(player.accountName, (byte) 117)) {
							hideMessage = true;
						}
					}
					if (!hideMessage && Class22.anInt216 == 0) {
						int index = -1;
						String text;
						if (!onlQuickchatAvailable) {
							text = Class249.escapeHtml(OpenGlElementBufferPointer.decodeString(buffer, (byte) 72), 62);
						} else {
							effects &= 0x7fff;
							QuickChat quickChat = Class42.decode((byte) -12, buffer);
							index = quickChat.messageId;
							text = quickChat.message.decodeMessage(15281, buffer);
						}
						player.message = text.trim();
						player.anInt6398 = effects >> -1332042328;
						player.anInt6402 = 0xff & effects;
						player.anInt6384 = 150;
						int count;
						if ((rights ^ 0xffffffff) == -2 || (rights ^ 0xffffffff) == -3) {
							count = onlQuickchatAvailable ? 17 : 1;
						} else {
							count = !onlQuickchatAvailable ? 2 : 17;
						}
						if ((rights ^ 0xffffffff) == -3) {
							Class137.addChatMessage(player.displayName, count, "<img=1>" + player.formattedName(0, true), text, index, null, (byte) -107, 0, "<img=1>" + player.getName(-1, false));
						} else if (rights == 1) {
							Class137.addChatMessage(player.displayName, count, "<img=0>" + player.formattedName(0, true), text, index, null, (byte) -84, 0, "<img=0>" + player.getName(-1, false));
						} else {
							Class137.addChatMessage(player.displayName, count, player.formattedName(i ^ 0x1f4decc3, true), text, index, null, (byte) -81, 0, player.getName(-1, false));
						}
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class246_Sub3_Sub4_Sub5.aClass58_6264 == CURRENT_INCOMING_OPCODE) {
				int i_125_ = buffer.readShort((byte) 127);
				if ((i_125_ ^ 0xffffffff) == -65536) {
					i_125_ = -1;
				}
				int i_126_ = buffer.readIntReverse(true);
				Class98_Sub25.method1274(i + -525200580);
				Class98_Sub46_Sub10.method1572(i_126_, 30585, i_125_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub10_Sub20.aClass58_5638) {
				int i_127_ = buffer.readByteA(true);
				byte i_128_ = buffer.method1187((byte) -112);
				Class98_Sub25.method1274(-1);
				DummyOutputStream.method130(i_128_, 65280, i_127_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class151_Sub5.aClass58_4992) {
				Class98_Sub10_Sub22.method1070((byte) -45, OutgoingPacket.aClass85_3868);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Animable.aClass58_6151) {
				int i_129_ = buffer.readShort1((byte) 112);
				Class98_Sub25.method1274(-1);
				Class98_Sub42.method1476(256, i_129_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub10_Sub20.aClass58_5635) {
				int i_130_ = buffer.readShort1((byte) -85);
				int i_131_ = buffer.readLEShortA((byte) 92);
				Class98_Sub25.method1274(-1);
				Class308.handleCameraClick(i_131_, 0, i + -525200495, i_130_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Archive.aClass58_2844 == CURRENT_INCOMING_OPCODE) {
				int[] is = new int[4];
				for (int i_132_ = 0; (i_132_ ^ 0xffffffff) > -5; i_132_++) {
					is[i_132_] = buffer.readShort1((byte) -104);
				}
				int i_133_ = buffer.readShort((byte) 127);
				int i_134_ = buffer.readByteS(i ^ ~0x1f4decb5);
				NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(i_133_, -1);
				if (class98_sub39 != null) {
					Class98_Sub43.method1483(i_134_, class98_sub39.npc, i ^ 0x1f4decc2, is);
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Char.SEND_GRAND_EXCHANGE_OFFER == CURRENT_INCOMING_OPCODE) {
				int slot = buffer.readUnsignedByte((byte) 37);
				if ((buffer.readUnsignedByte((byte) 75) ^ 0xffffffff) == -1) {
					Class98_Sub10_Sub24.offers[slot] = new GrandExchangeOffer();
				} else {
					buffer.position--;
					Class98_Sub10_Sub24.offers[slot] = new GrandExchangeOffer(buffer);
				}
				CURRENT_INCOMING_OPCODE = null;
				GroundDecorationPreferenceField.anInt3668 = WorldMapInfoDefinition.logicTimer;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class15.aClass58_184) {
				int i_136_ = buffer.readByteC((byte) -112);
				int i_137_ = i_136_ >> 41032322;
				int i_138_ = i_136_ & 0x3;
				int i_139_ = BuildAreaPreferenceField.anIntArray3685[i_137_];
				int i_140_ = buffer.readLEShortA((byte) -40);
				if (i_140_ == 65535) {
					i_140_ = -1;
				}
				int i_141_ = buffer.readInt2(i + -525200657);
				int i_142_ = 0x3 & i_141_ >> 87030492;
				int i_143_ = (0xffff619 & i_141_) >> 1305310190;
				i_143_ -= Class272.gameSceneBaseX;
				int i_144_ = i_141_ & 0x3fff;
				i_144_ -= aa_Sub2.gameSceneBaseY;
				Class283.method3351(i_137_, i_138_, true, i_143_, i_144_, i_142_, i_140_, i_139_);
				CURRENT_INCOMING_OPCODE = null;
				System.out.println("Reeeeee");

				return true;
			}
			if (Class65.aClass58_499 == CURRENT_INCOMING_OPCODE) {
				Class98_Sub10_Sub22.method1070((byte) -45, SkyboxDefinition.aClass85_471);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class48_Sub1_Sub2.VARP_SMALL == CURRENT_INCOMING_OPCODE) {
				Class116.setVarpBit(client.zoom, 184, (byte) -120);// TODO: This
																	// is not a
																	// good way
				int id = buffer.readShortA(50);
				byte value = buffer.method1234(128);// readByteS
				StartupStage.varValues.setConfig(-7469, value, id);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class246_Sub3_Sub4_Sub3.aClass58_6457) {
				if (GameShell.fullScreenFrame != null) {
					Class98_Sub16.setWindowMode(client.preferences.screenSize.getValue((byte) 120), -1, 3, -1, false);
				}
				byte[] is = new byte[Class65.currentPacketSize];
				buffer.xgData(0, Class65.currentPacketSize, is, true);
				String string = Class98_Sub46_Sub6.method1546(Class65.currentPacketSize, 0, (byte) -84, is);
				TextLSEConfig.method3647(true, (client.preferences.currentToolkit.getValue((byte) 127) ^ 0xffffffff) == -2, string, true, GameShell.signLink);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (ActionQueueEntry.RECIEVE_PRIVATE_MESSAGE == CURRENT_INCOMING_OPCODE) {
				boolean bool = (buffer.readUnsignedByte((byte) 65) ^ 0xffffffff) == -2;
				String string = buffer.readString((byte) 84);
				String string_147_ = string;
				if (bool) {
					string_147_ = buffer.readString((byte) 84);
				}
				long l = buffer.readShort((byte) 127);
				long l_148_ = buffer.readMediumInt(i ^ ~0x1f4decbf);
				int i_149_ = buffer.readUnsignedByte((byte) 106);
				long l_150_ = (l << -1427120736) + l_148_;
				boolean bool_151_ = false;
				while_29_: do {
					for (int i_152_ = 0; (i_152_ ^ 0xffffffff) > -101; i_152_++) {
						if (l_150_ == Class94.aLongArray794[i_152_]) {
							bool_151_ = true;
							break while_29_;
						}
					}
					if (i_149_ <= 1) {
						if (Js5Manager.aBoolean933 && !Class98_Sub10_Sub35.aBoolean5732 || BaseModel.isQuickChatWorld) {
							bool_151_ = true;
						} else if (Class14.isIgnored(string_147_, (byte) 126)) {
							bool_151_ = true;
						}
					}
				} while (false);
				if (!bool_151_ && Class22.anInt216 == 0) {
					Class94.aLongArray794[ClanChatMember.messageCount] = l_150_;
					ClanChatMember.messageCount = (1 + ClanChatMember.messageCount) % 100;
					String string_153_ = Class249.escapeHtml(OpenGlElementBufferPointer.decodeString(buffer, (byte) 72), 62);
					if ((i_149_ ^ 0xffffffff) != -3) {
						if ((i_149_ ^ 0xffffffff) != -2) {
							Class137.addChatMessage(string, 3, string, string_153_, -1, null, (byte) -108, 0, string_147_);
						} else {
							Class137.addChatMessage(string, 7, "<img=0>" + string, string_153_, -1, null, (byte) -120, 0, "<img=0>" + string_147_);
						}
					} else {
						Class137.addChatMessage(string, 7, "<img=1>" + string, string_153_, -1, null, (byte) -93, 0, "<img=1>" + string_147_);
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Exception_Sub1.aClass58_43) {
				Class273.method3280((byte) -101);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class372.VARPBIT_LARGE == CURRENT_INCOMING_OPCODE) {
				int id = buffer.readShort1((byte) 50);
				byte value = buffer.readSignedByte((byte) -19);
				Class98_Sub25.method1274(-1);
				Class116.setVarpBit(value, id, (byte) -120);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class224_Sub1.aClass58_5032) {
				String string = buffer.readString((byte) 84);
				int i_156_ = buffer.readIntReverse(true);
				Class98_Sub25.method1274(i ^ ~0x1f4decc3);
				OpenGlToolkit.method1895(i_156_, (byte) -52, string);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class16.aClass58_191) {
				Class98_Sub10_Sub1.handleLogout(false, aa_Sub3.aBoolean3569);
				CURRENT_INCOMING_OPCODE = null;
				return false;
			}
			if (CURRENT_INCOMING_OPCODE == Class277.aClass58_2052) {
				int i_157_ = buffer.readShort1((byte) -79);
				int i_158_ = buffer.readLEShortA((byte) -72);
				int i_159_ = buffer.readInt1(false);
				Class98_Sub25.method1274(-1);
				Class98_Sub10_Sub33.method1099(i_159_, i_158_ + (i_157_ << -528695792), (byte) 111);
				CURRENT_INCOMING_OPCODE = null;

				return true;
			}
			if (Class283.aClass58_2143 == CURRENT_INCOMING_OPCODE) { // save
				int i_160_ = buffer.readIntReverse(true);
				int i_161_ = buffer.readLEShortA((byte) -48);
				int i_162_ = buffer.readIntReverse(true);
				Class98_Sub25.method1274(-1);
				ReflectionRequest.method1164(i_161_, i_160_, 5, 4, i_162_);
				// Class98_Sub19.method1164(id, -1, 2, i ^ 0x1f4decc7,
				// interfaceId);
				CURRENT_INCOMING_OPCODE = null;

				return true;
			}
			if (Class369.IGNORE_LIST == CURRENT_INCOMING_OPCODE) {
				Class248.ignoreListSize = buffer.readUnsignedByte((byte) 15);
				for (int index = 0; (Class248.ignoreListSize ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
					FriendLoginUpdate.ignores[index] = buffer.readString((byte) 84);
					Class255.ignoreNames[index] = buffer.readString((byte) 84);
					if (Class255.ignoreNames[index].equals("")) {
						Class255.ignoreNames[index] = FriendLoginUpdate.ignores[index];
					}
					ItemDeque.ignoreDisplayNames[index] = buffer.readString((byte) 84);
					VerticalAlignment.ignoreDisplayNames[index] = buffer.readString((byte) 84);
					if (VerticalAlignment.ignoreDisplayNames[index].equals("")) {
						VerticalAlignment.ignoreDisplayNames[index] = ItemDeque.ignoreDisplayNames[index];
					}
					Class98_Sub10_Sub38.aBooleanArray5759[index] = false;
				}
				NewsFetcher.anInt3099 = WorldMapInfoDefinition.logicTimer;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class150.aClass58_1212) {
				Class98_Sub36.method1459(-1048016408);
				CURRENT_INCOMING_OPCODE = null;
				return false;
			}
			if (TextResources.aClass58_2651 == CURRENT_INCOMING_OPCODE) {// steven
				int i_164_ = buffer.readByteC((byte) -107);
				int i_165_ = buffer.readShortA(75);
				StartupStage.varValues.method2292(i_165_, i_164_, -32727);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (ItemSpriteCacheKey.SET_PLAYER_OPTION == CURRENT_INCOMING_OPCODE) {
				int i_166_ = buffer.readShortA(i ^ 0x1f4dec82);
				if (i_166_ == 65535) {
					i_166_ = -1;
				}
				int i_167_ = buffer.readByteS(-83);
				String option = buffer.readString((byte) 84);
				int slot = buffer.readByteS(126);
				if ((slot ^ 0xffffffff) <= -2 && (slot ^ 0xffffffff) >= -9) {
					if (option.equalsIgnoreCase("null")) {
						option = null;
					}
					LightIntensityDefinitionParser.playerOptions[-1 + slot] = option;
					Class151_Sub9.playerOptionCursors[slot + -1] = i_166_;
					OpenGlModelRenderer.playerOptionReducedPriority[-1 + slot] = (i_167_ ^ 0xffffffff) == -1;
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class370.HINT_ICON == CURRENT_INCOMING_OPCODE) {
				int i_169_ = buffer.readUnsignedByte((byte) 51);
				int i_170_ = i_169_ >> -2017319579;
				int i_171_ = i_169_ & 0x1f;
				if (i_171_ == 0) {
					OpenGlArrayBufferPointer.aClass36Array903[i_170_] = null;
					CURRENT_INCOMING_OPCODE = null;
					return true;
				}
				Class36 class36 = new Class36();
				class36.anInt346 = i_171_;
				class36.anInt341 = buffer.readUnsignedByte((byte) 90);
				if (class36.anInt341 >= 0 && class36.anInt341 < EnumDefinition.aClass332Array2557.length) {
					if ((class36.anInt346 ^ 0xffffffff) != -2 && class36.anInt346 != 10) {
						if ((class36.anInt346 ^ 0xffffffff) <= -3 && class36.anInt346 <= 6) {
							if (class36.anInt346 == 2) {
								class36.anInt338 = 256;
								class36.anInt347 = 256;
							}
							if ((class36.anInt346 ^ 0xffffffff) == -4) {
								class36.anInt338 = 0;
								class36.anInt347 = 256;
							}
							if ((class36.anInt346 ^ 0xffffffff) == -5) {
								class36.anInt338 = 512;
								class36.anInt347 = 256;
							}
							if ((class36.anInt346 ^ 0xffffffff) == -6) {
								class36.anInt338 = 256;
								class36.anInt347 = 0;
							}
							if ((class36.anInt346 ^ 0xffffffff) == -7) {
								class36.anInt347 = 512;
								class36.anInt338 = 256;
							}
							class36.anInt346 = 2;
							class36.anInt342 = buffer.readUnsignedByte((byte) -112);
							class36.anInt338 += buffer.readShort((byte) 127) - Class272.gameSceneBaseX << -1727288087;
							class36.anInt347 += buffer.readShort((byte) 127) + -aa_Sub2.gameSceneBaseY << 357961001;
							class36.anInt343 = buffer.readUnsignedByte((byte) 2) << -1401715486;
							class36.anInt340 = buffer.readShort((byte) 127);
						}
					} else {
						class36.anInt345 = buffer.readShort((byte) 127);
						buffer.position += 6;
					}
					class36.anInt339 = buffer.readShort((byte) 127);
					if (class36.anInt339 == 65535) {
						class36.anInt339 = -1;
					}
					OpenGlArrayBufferPointer.aClass36Array903[i_170_] = class36;
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class211.SEND_VOICE) {
				int voiceId = buffer.readShort((byte) 127);
				if (voiceId == 65535) {
					voiceId = -1;
				}
				int timesPlayed = buffer.readUnsignedByte((byte) 91);
				int delay = buffer.readShort((byte) 127);
				int volume = buffer.readUnsignedByte((byte) 5);
				int speed = buffer.readShort((byte) 127);
				Class98_Sub10_Sub9.method1036(-1962608884, volume, timesPlayed, voiceId, false, delay, speed);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == NPCDefinitionParser.aClass58_2507) {
				int i_177_ = buffer.readByteS(i ^ 0x1f4dec88);
				int i_178_ = buffer.readByteA(true);
				int i_179_ = buffer.readShort1((byte) 57) << 12040226;
				int i_180_ = buffer.readByteA(true);
				int i_181_ = buffer.readByteA(true);
				Class98_Sub25.method1274(-1);
				OpenGlToolkit.method1871(i_177_, i_180_, true, i_179_, i_178_, i_181_, i + -525200676);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (SkyboxDefinitionParser.aClass58_469 == CURRENT_INCOMING_OPCODE) {
				int i_182_ = buffer.readShort((byte) 127);
				int i_183_ = buffer.readUnsignedByte((byte) 101);
				boolean bool = (0x1 & i_183_) == 1;
				Class232.method2882(i + -525200605, i_182_, bool);
				int i_184_ = buffer.readShort((byte) 127);
				for (int i_185_ = 0; (i_184_ ^ 0xffffffff) < (i_185_ ^ 0xffffffff); i_185_++) {
					int i_186_ = buffer.readShortA(84);
					int i_187_ = buffer.readByteC((byte) -122);
					if (i_187_ == 255) {
						i_187_ = buffer.readInt1(false);
					}
					Class349.method3841(bool, i_187_, 3113, i_182_, -1 + i_186_, i_185_);
				}
				Class78.anIntArray597[Class202.and(SystemInformation.anInt1172++, 31)] = i_182_;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class151_Sub6.aClass58_4997) {
				String string = buffer.readString((byte) 84);
				String string_188_ = Class249.escapeHtml(OpenGlElementBufferPointer.decodeString(buffer, (byte) 72), 62);
				ItemDeque.addChatMessage((byte) 44, 6, string_188_, 0, string, string, string);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class150.aClass58_1210) {
				int i_189_ = buffer.readByteS(74);
				int i_190_ = buffer.readByteS(i ^ 0x1f4dec88);
				if (i_189_ == 255) {
					i_190_ = -1;
					i_189_ = -1;
				}
				Class244.method2953((byte) -103, i_189_, i_190_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub31_Sub2.aClass58_5838) {
				Class333.anInt3386 = buffer.readUnsignedByte((byte) -118);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub42.aClass58_4222) {
				Class98_Sub10_Sub22.method1070((byte) -45, Class246_Sub4_Sub2.aClass85_6186);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class25.aClass58_266 == CURRENT_INCOMING_OPCODE) {
				int i_191_ = buffer.readIntReverse(true);
				int i_192_ = buffer.readLEShortA((byte) 121);
				Class98_Sub25.method1274(-1);
				ParticleManager.method430(true, i_192_, i_191_);
				CURRENT_INCOMING_OPCODE = null;

				return true;
			}
			if (CURRENT_INCOMING_OPCODE == aa_Sub1.aClass58_3554) {
				System.out.println("Packet: 94, -1");
				int i_193_ = buffer.readShort((byte) 127);
				int i_194_ = buffer.readShort((byte) 127);
				int i_195_ = buffer.readShort((byte) 127);
				Class98_Sub25.method1274(-1);
				if (Class159.interfaceStore[i_193_] != null) {
					for (int i_196_ = i_194_; i_196_ < i_195_; i_196_++) {
						int i_197_ = buffer.readMediumInt(i + -525200702);
						if ((Class159.interfaceStore[i_193_].length ^ 0xffffffff) < (i_196_ ^ 0xffffffff) && Class159.interfaceStore[i_193_][i_196_] != null) {
							Class159.interfaceStore[i_193_][i_196_].anInt2259 = i_197_;
						}
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class3.aClass58_75) {
				Class98_Sub10_Sub22.method1070((byte) -45, AdvancedMemoryCache.aClass85_600);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class44.aClass58_379 == CURRENT_INCOMING_OPCODE) {
				int i_198_ = buffer.readByteS(i + -525200497);
				int i_199_ = buffer.readLEShortA((byte) -81);
				boolean bool = (0x1 & i_198_ ^ 0xffffffff) == -2;
				Class181.method2610(true, bool, i_199_);
				Class78.anIntArray597[Class202.and(SystemInformation.anInt1172++, 31)] = i_199_;
				CURRENT_INCOMING_OPCODE = null;
				System.out.println("Reeeeee");

				return true;
			}
			if (Class98_Sub46_Sub15.SEND_WINDOW_PANE == CURRENT_INCOMING_OPCODE) {
				int i_200_ = buffer.readByteA(true);
				int i_201_ = buffer.readShortA(102);
				Class98_Sub25.method1274(-1);
				if (i_200_ == 2) {
					CursorDefinition.method2878(i ^ 0x1f4decc1);
				}
				client.topLevelInterfaceId = i_201_;
				Class98_Sub46_Sub15.method1609(i_201_, i + -525213468);
				Class40.calculateLayout(-124, false);
				ClientScript2Runtime.sendWindowPane(client.topLevelInterfaceId);
				for (int i_202_ = 0; (i_202_ ^ 0xffffffff) > -101; i_202_++) {
					aa_Sub3.isDirty[i_202_] = true;
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (SafeModePreferenceField.aClass58_3645 == CURRENT_INCOMING_OPCODE) {
				int i_203_ = buffer.readByteC((byte) -128);
				int i_204_ = buffer.readByteC((byte) -128);
				int i_205_ = buffer.readShort1((byte) 90);
				if ((i_205_ ^ 0xffffffff) == -65536) {
					i_205_ = -1;
				}
				Class246_Sub3_Sub1.method2994(i_205_, i_203_, (byte) -83, i_204_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (LightningDetailPreferenceField.aClass58_3665 == CURRENT_INCOMING_OPCODE) {
				int i_206_ = buffer.method1192((byte) -108);
				int i_207_ = buffer.readByteS(88);
				int i_208_ = buffer.readShort1((byte) 42);
				if ((i_208_ ^ 0xffffffff) == -65536) {
					i_208_ = -1;
				}
				Class228.method2861(i_206_, i_207_, i_208_, 18596);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == QuestDefinitionParser.aClass58_161) { // Changed
				// their
				// streams?
				MemoryCacheNodeFactory.tileZ = buffer.readUnsignedByte((byte) 100);
				MapScenesDefinitionParser.chunkY = buffer.method1187((byte) -112) << -1201567837;
				Class53.chunkX = buffer.readByteA(i + -2023493939) << -1233392637;
				while (buffer.position < Class65.currentPacketSize) {
					Class85 class85 = LoadingScreenElementType.method2143(-1)[buffer.readUnsignedByte((byte) 111)];
					Class98_Sub10_Sub22.method1070((byte) -45, class85);
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class76.SEND_SOUND == CURRENT_INCOMING_OPCODE) {
				int soundId = buffer.readShort((byte) 127);
				if (soundId == 65535) {
					soundId = -1;
				}
				int timesPlayed = buffer.readUnsignedByte((byte) 56);
				int delay = buffer.readShort((byte) 127);
				int volume = buffer.readUnsignedByte((byte) 25);
				int speed = buffer.readShort((byte) 127);
				NPCDefinitionParser.method3537(speed, (byte) 1, soundId, timesPlayed, delay, volume);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class211.aClass58_1596 == CURRENT_INCOMING_OPCODE) {
				Class98_Sub25.method1274(i + -525200580);
				Class284.method3359(9268);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class246_Sub3_Sub4_Sub4.aClass58_6487) {
				int i_214_ = buffer.readLEShortA((byte) -39);
				int i_215_ = buffer.readInt(-2);
				StartupStage.varValues.method2292(i_214_, i_215_, i ^ ~0x1f4d9315);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == GroupProgressMonitor.aClass58_3406) {
				Class98_Sub10_Sub22.method1070((byte) -45, Class98_Sub10_Sub16.REMOVE_GROUND_ITEM);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == RSByteBuffer.SEND_CHUNK_UPDATE) {
				Class53.chunkX = buffer.readByteA(-1498293360) << -182592509;
				MemoryCacheNodeFactory.tileZ = buffer.readByteC((byte) -106);
				MapScenesDefinitionParser.chunkY = buffer.readByteA(i + -2023493939) << 1849187075;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (GroundItem.aClass58_4029 == CURRENT_INCOMING_OPCODE) {
				int i_216_ = buffer.readShort((byte) 127);
				int i_217_ = buffer.readUnsignedByte((byte) -100);
				boolean bool = (0x1 & i_217_ ^ 0xffffffff) == -2;
				while ((buffer.position ^ 0xffffffff) > (Class65.currentPacketSize ^ 0xffffffff)) {
					int i_218_ = buffer.readSmart(1689622712);
					int i_219_ = buffer.readShort((byte) 127);
					int i_220_ = 0;
					if ((i_219_ ^ 0xffffffff) != -1) {
						i_220_ = buffer.readUnsignedByte((byte) -124);
						if (i_220_ == 255) {
							i_220_ = buffer.readInt(i + -525200581);
						}
					}
					Class349.method3841(bool, i_220_, i ^ 0x1f4de0ea, i_216_, -1 + i_219_, i_218_);
				}
				Class78.anIntArray597[Class202.and(SystemInformation.anInt1172++, 31)] = i_216_;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class98_Sub47.aClass58_4270 == CURRENT_INCOMING_OPCODE) {
				boolean bool = buffer.readByteS(-31) == 1;
				Class98_Sub25.method1274(-1);
				QuickChatCategory.aBoolean5943 = bool;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class27.aClass58_274) {
				Class108.sendPlayerUpdate(Class65.currentPacketSize, buffer, i + -525200579);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class159.SEND_IGNORE_LIST == CURRENT_INCOMING_OPCODE) {
				int value = buffer.readUnsignedByte((byte) -114);
				boolean bool = (value & 0x1 ^ 0xffffffff) == -2;
				String accountName = buffer.readString((byte) 84);
				String displayName = buffer.readString((byte) 84);
				if (displayName.equals("")) {
					displayName = accountName;
				}
				String lastKnownAccountName = buffer.readString((byte) 84);
				String lastKnownDisplayName = buffer.readString((byte) 84);
				if (lastKnownDisplayName.equals("")) {
					lastKnownDisplayName = lastKnownAccountName;
				}
				if (bool) {
					for (int index = 0; (index ^ 0xffffffff) > (Class248.ignoreListSize ^ 0xffffffff); index++) {
						if (Class255.ignoreNames[index].equals(lastKnownDisplayName)) {
							FriendLoginUpdate.ignores[index] = accountName;
							Class255.ignoreNames[index] = displayName;
							ItemDeque.ignoreDisplayNames[index] = lastKnownAccountName;
							VerticalAlignment.ignoreDisplayNames[index] = lastKnownDisplayName;
							break;
						}
					}
				} else {
					FriendLoginUpdate.ignores[Class248.ignoreListSize] = accountName;
					Class255.ignoreNames[Class248.ignoreListSize] = displayName;
					ItemDeque.ignoreDisplayNames[Class248.ignoreListSize] = lastKnownAccountName;
					VerticalAlignment.ignoreDisplayNames[Class248.ignoreListSize] = lastKnownDisplayName;
					Class98_Sub10_Sub38.aBooleanArray5759[Class248.ignoreListSize] = Class202.and(2, value) == 2;
					Class248.ignoreListSize++;
				}
				CURRENT_INCOMING_OPCODE = null;
				NewsFetcher.anInt3099 = WorldMapInfoDefinition.logicTimer;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class246_Sub3_Sub2_Sub1.aClass58_6335) {
				boolean bool = buffer.readUnsignedByte((byte) -125) == 1;
				String string = buffer.readString((byte) 84);
				String string_226_ = string;
				if (bool) {
					string_226_ = buffer.readString((byte) 84);
				}
				int i_227_ = buffer.readUnsignedByte((byte) -6);
				int i_228_ = buffer.readShort((byte) 127);
				boolean bool_229_ = false;
				if (i_227_ <= 1 && Class14.isIgnored(string_226_, (byte) 108)) {
					bool_229_ = true;
				}
				if (!bool_229_ && (Class22.anInt216 ^ 0xffffffff) == -1) {
					String string_230_ = NewsLSEConfig.quickChatMessageList.get(i_228_, 44).decodeMessage(i + -525185298, buffer);
					if (i_227_ != 2) {
						if (i_227_ != 1) {
							Class137.addChatMessage(string, 25, string, string_230_, i_228_, null, (byte) -100, 0, string_226_);
						} else {
							Class137.addChatMessage(string, 25, "<img=0>" + string, string_230_, i_228_, null, (byte) -127, 0, "<img=0>" + string_226_);
						}
					} else {
						Class137.addChatMessage(string, 25, "<img=1>" + string, string_230_, i_228_, null, (byte) -123, 0, "<img=1>" + string_226_);
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class320.aClass58_2708 == CURRENT_INCOMING_OPCODE) {
				Class98_Sub10_Sub1.handleLogout(false, false);
				CURRENT_INCOMING_OPCODE = null;
				return false;
			}
			if (CURRENT_INCOMING_OPCODE == Class53.SEND_EXPERIENCE_UPDATE) {
				int experience = buffer.readInt1(false);
				int skillId = buffer.readByteS(i ^ 0x1f4dec8f);
				int level = buffer.readByteS(-103);
				NewsLSEConfig.skillsExperience[skillId] = experience;
				CpuUsagePreferenceField.levels[skillId] = level;
				Class256_Sub1.skillLevels[skillId] = 1;
				int i_234_ = -1 + Class98_Sub46_Sub4.anIntArray5955[skillId];
				for (int i_235_ = 0; i_234_ > i_235_; i_235_++) {
					if (experience >= Class48_Sub1.anIntArray3629[i_235_]) {
						Class256_Sub1.skillLevels[skillId] = 2 + i_235_;
					}
				}
				Class98_Sub16.anIntArray3928[Class202.and(31, DecoratedProgressBarLSEConfig.anInt5477++)] = skillId;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == MonoOrStereoPreferenceField.aClass58_3637) {
				Class98_Sub10_Sub22.method1070((byte) -45, Class39.aClass85_362);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub50.CHANGE_WALK_HERE_OPTION_TEXT) {
				SceneGraphNodeList.optionText = (Class65.currentPacketSize ^ 0xffffffff) < -3 ? buffer.readString((byte) 84) : TextResources.WALK_HERE.getText(client.gameLanguage, (byte) 25);
				FloorOverlayDefinition.anInt1541 = Class65.currentPacketSize > 0 ? buffer.readShort((byte) 127) : -1;
				CURRENT_INCOMING_OPCODE = null;
				if ((FloorOverlayDefinition.anInt1541 ^ 0xffffffff) == -65536) {
					FloorOverlayDefinition.anInt1541 = -1;
				}
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class151_Sub6.aClass58_4998) {
				Class98_Sub28.totalBoxVisibility = 1;
				CURRENT_INCOMING_OPCODE = null;
				NewsFetcher.anInt3099 = WorldMapInfoDefinition.logicTimer;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == ModelRenderer.aClass58_1179) {
				int i_236_ = buffer.readInt2(i ^ ~0x1f4deccd);
				Class98_Sub25.method1274(-1);
				ReflectionRequest.method1164(OpenGLHeap.localPlayerIndex, 0, 5, i ^ 0x1f4decc7, i_236_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == ClanChatMember.aClass58_1192) {
				int i_237_ = buffer.method1242(-1420625632);
				int i_238_ = buffer.readInt(i + -525200581);
				int i_239_ = buffer.method1183(65536);
				Class98_Sub25.method1274(-1);
				Char.method2984(i_239_, (byte) -105, i_237_, i_238_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class27.SEND_CLOSE_INTERFACE) {
				int i_240_ = buffer.readIntReverse(true);
				Class98_Sub25.method1274(-1);
				RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(i_240_, -1);
				if (class98_sub18 != null) {
					RtInterfaceAttachment.detachInterface(16398, false, class98_sub18, true);
				}
				if (DummyOutputStream.rtInterface != null) {
					WorldMapInfoDefinitionParser.setDirty(1, DummyOutputStream.rtInterface);
					DummyOutputStream.rtInterface = null;
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == NodeInteger.SEND_INTERFACE_ANIMATION) {
				int i_241_ = buffer.readInt1(false);
				int emoteId = buffer.method1198(i + -525200697);
				Class98_Sub25.method1274(i ^ ~0x1f4decc3);
				SceneGraphNodeList.method2806(i_241_, emoteId, true);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class308.aClass58_2581 == CURRENT_INCOMING_OPCODE) {
				int i_243_ = buffer.readShort((byte) 127);
				int i_244_ = buffer.readIntReverse(true);
				int i_245_ = buffer.readLEShortA((byte) -115);
				int i_246_ = buffer.readShortA(96);
				Class98_Sub25.method1274(-1);
				ReflectionRequest.method1164(i_245_ << 725805072 | i_243_, i_246_, 7, 4, i_244_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class283.aClass58_2139) {
				Class98_Sub10_Sub22.method1070((byte) -45, Class351.aClass85_2921);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == GameObjectDefinition.aClass58_2993) {
				ActionGroup.anInt6003 = buffer.method1227((byte) -1);
				Js5Manager.aBoolean933 = buffer.readUnsignedByte((byte) 112) == 1;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (SunDefinition.aClass58_1992 == CURRENT_INCOMING_OPCODE) {
				if ((client.topLevelInterfaceId ^ 0xffffffff) != 0) {
					Js5.method2764(0, client.topLevelInterfaceId, -46);
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class98_Sub10_Sub14.aClass58_5608 == CURRENT_INCOMING_OPCODE) {
				if (!Class246_Sub3_Sub3.method3011(-6410, client.clientState)) {
					Class98_Sub10_Sub6.anInt5569 = 30 * buffer.readShort((byte) 127);
				} else {
					Class98_Sub10_Sub6.anInt5569 = (int) (buffer.readShort((byte) 127) * 2.5F);
				}
				Class98_Sub36.anInt4161 = WorldMapInfoDefinition.logicTimer;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (SimpleProgressBarLSEConfig.aClass58_5493 == CURRENT_INCOMING_OPCODE) {
				Class98_Sub10_Sub22.method1070((byte) -45, OpenGlToolkit.aClass85_4299);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class98_Sub10_Sub28.aClass58_5697 == CURRENT_INCOMING_OPCODE) {
				int i_247_ = buffer.readShort1((byte) 75);
				Class98_Sub25.method1274(i + -525200580);
				OutgoingPacket.method1127((byte) 67, i_247_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Js5Index.aClass58_2661 == CURRENT_INCOMING_OPCODE) {
				for (int i_248_ = 0; i_248_ < Class151_Sub9.players.length; i_248_++) {
					if (Class151_Sub9.players[i_248_] != null) {
						Class151_Sub9.players[i_248_].anIntArray6373 = null;
						Class151_Sub9.players[i_248_].anInt6413 = -1;
					}
				}
				for (int i_249_ = 0; (i_249_ ^ 0xffffffff) > (Class98_Sub10_Sub20.anInt5640 ^ 0xffffffff); i_249_++) {
					BackgroundColourLSEConfig.aClass98_Sub39Array3516[i_249_].npc.anIntArray6373 = null;
					BackgroundColourLSEConfig.aClass98_Sub39Array3516[i_249_].npc.anInt6413 = -1;
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class336.PLAYER_ON_INTERFACE == CURRENT_INCOMING_OPCODE) {
				int i_250_ = buffer.readInt1(false);
				Class98_Sub25.method1274(-1);
				ReflectionRequest.method1164(-1, -1, 3, i ^ 0x1f4decc7, i_250_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == InventoriesDefinitionParser.SEND_CLAN_CHAT_MESSAGE) {
				boolean hasDisplayName = buffer.readUnsignedByte((byte) 20) == 1;
				String accountName = buffer.readString((byte) 84);
				String name = accountName;
				if (hasDisplayName) {
					name = buffer.readString((byte) 84);
				}
				long chatName = buffer.readLong(-105);
				long id1 = buffer.readShort((byte) 127);
				long id2 = buffer.readMediumInt(-124);
				int rights = buffer.readUnsignedByte((byte) 126);
				long hash = (id1 << -172131936) + id2;
				boolean hideMessage = false;
				while_30_: do {
					for (int index = 0; index < 100; index++) {
						if (hash == Class94.aLongArray794[index]) {
							hideMessage = true;
							break while_30_;
						}
					}
					if (rights <= 1) {
						if (Js5Manager.aBoolean933 && !Class98_Sub10_Sub35.aBoolean5732 || BaseModel.isQuickChatWorld) {
							hideMessage = true;
						} else if (Class14.isIgnored(name, (byte) 121)) {
							hideMessage = true;
						}
					}
				} while (false);
				if (!hideMessage && (Class22.anInt216 ^ 0xffffffff) == -1) {
					Class94.aLongArray794[ClanChatMember.messageCount] = hash;
					ClanChatMember.messageCount = (ClanChatMember.messageCount - -1) % 100;
					String message = Class249.escapeHtml(OpenGlElementBufferPointer.decodeString(buffer, (byte) 72), 62);
					System.out.println("String 258: " + hash);
					if (rights != 2 && rights != 3) {
						if ((rights ^ 0xffffffff) == -2) {
							Class137.addChatMessage(accountName, 9, "<img=0>" + accountName, message, -1, Class18.longToString(chatName, i + -525200681), (byte) -65, 0, "<img=0>" + name);
						} else {
							Class137.addChatMessage(accountName, 9, accountName, message, -1, Class18.longToString(chatName, -120), (byte) -84, 0, name);
						}
					} else {
						Class137.addChatMessage(accountName, 9, "<img=1>" + accountName, message, -1, Class18.longToString(chatName, i + -525200644), (byte) -113, 0, "<img=1>" + name);
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == RemoveRoofsPreferenceField.SEND_AMASK) {
				int i_259_ = buffer.readInt2(-108);
				int length = buffer.readShortA(i + -525200459);
				if (length == 65535) {
					length = -1;
				}
				int offset = buffer.readShortA(74);
				if ((offset ^ 0xffffffff) == -65536) {
					offset = -1;
				}
				int i_262_ = buffer.readIntReverse(true);
				Class98_Sub25.method1274(-1);
				for (int i_263_ = offset; (length ^ 0xffffffff) <= (i_263_ ^ 0xffffffff); i_263_++) {
					long l = ((long) i_262_ << -1557162784) - -(long) i_263_;
					Class98_Sub49 class98_sub49 = (Class98_Sub49) Class168.aClass377_1287.get(l, -1);
					Class98_Sub49 class98_sub49_264_;
					if (class98_sub49 != null) {
						class98_sub49_264_ = new Class98_Sub49(i_259_, class98_sub49.anInt4285);
						class98_sub49.unlink(103);
					} else if ((i_263_ ^ 0xffffffff) == 0) {
						class98_sub49_264_ = new Class98_Sub49(i_259_, RtInterface.getInterface(i_262_, -9820).aClass98_Sub49_2348.anInt4285);
					} else {
						class98_sub49_264_ = new Class98_Sub49(i_259_, -1);
					}
					Class168.aClass377_1287.put(class98_sub49_264_, l, -1);
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class36.aClass58_344) {
				int i_265_ = buffer.readInt1(false);
				int i_266_ = buffer.readShort((byte) 127);
				if ((i_266_ ^ 0xffffffff) == -65536) {
					i_266_ = -1;
				}
				int i_267_ = buffer.readShortA(66);
				int i_268_ = buffer.readShort((byte) 127);
				if ((i_268_ ^ 0xffffffff) == -65536) {
					i_268_ = -1;
				}
				Class98_Sub25.method1274(-1);
				for (int i_269_ = i_266_; (i_269_ ^ 0xffffffff) >= (i_268_ ^ 0xffffffff); i_269_++) {
					long l = ((long) i_265_ << -1828767200) - -(long) i_269_;
					Class98_Sub49 class98_sub49 = (Class98_Sub49) Class168.aClass377_1287.get(l, -1);
					Class98_Sub49 class98_sub49_270_;
					if (class98_sub49 != null) {
						class98_sub49_270_ = new Class98_Sub49(class98_sub49.anInt4284, i_267_);
						class98_sub49.unlink(110);
					} else if ((i_269_ ^ 0xffffffff) == 0) {
						class98_sub49_270_ = new Class98_Sub49(RtInterface.getInterface(i_265_, i ^ ~0x1f4dca98).aClass98_Sub49_2348.anInt4284, i_267_);
					} else {
						class98_sub49_270_ = new Class98_Sub49(0, i_267_);
					}
					Class168.aClass377_1287.put(class98_sub49_270_, l, i ^ ~0x1f4decc3);
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class284_Sub2_Sub2.aClass58_6197 == CURRENT_INCOMING_OPCODE) {
				int i_271_ = buffer.readIntReverse(true);
				int i_272_ = buffer.readShortA(111);
				int i_273_ = buffer.readShort((byte) 127);
				int i_274_ = buffer.readShort((byte) 127);
				Class98_Sub25.method1274(i + -525200580);
				Class353.method3868(i_274_, i_271_, (byte) -121, i_273_, i_272_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Player.CLAN_CHAT_DATA) {
				VolumePreferenceField.anInt3705 = WorldMapInfoDefinition.logicTimer;
				if (Class65.currentPacketSize == 0) {
					WhirlpoolGenerator.clanChat = null;
					SimpleProgressBarLSEConfig.clanChatOwnerName = null;
					FloorOverlayDefinitionParser.clanChatSize = 0;
					FloorUnderlayDefinitionParser.clanChatName = null;
					CURRENT_INCOMING_OPCODE = null;
					return true;
				}
				SimpleProgressBarLSEConfig.clanChatOwnerName = buffer.readString((byte) 84);
				boolean hasDisplayName = buffer.readUnsignedByte((byte) 85) == 1;
				if (hasDisplayName) {
					buffer.readString((byte) 84);
				}
				long name = buffer.readLong(-126);
				System.out.println("Chat name: " + name);
				FloorUnderlayDefinitionParser.clanChatName = Class98_Sub28.longToString(-89, name);
				Class232.kickRequirement = buffer.readSignedByte((byte) -19);
				int size = buffer.readUnsignedByte((byte) 120);
				if ((size ^ 0xffffffff) == -256) {
					CURRENT_INCOMING_OPCODE = null;
					return true;
				}
				FloorOverlayDefinitionParser.clanChatSize = size;
				System.out.println("ClanChat name: " + Class65.currentPacketSize + " Long: " + name);
				ClanChatMember[] chatMembers = new ClanChatMember[100];
				for (int member = 0; (member ^ 0xffffffff) > (FloorOverlayDefinitionParser.clanChatSize ^ 0xffffffff); member++) {
					chatMembers[member] = new ClanChatMember();
					chatMembers[member].displayName = buffer.readString((byte) 84);
					hasDisplayName = buffer.readUnsignedByte((byte) -106) == 1;
					if (hasDisplayName) {
						chatMembers[member].accountName = buffer.readString((byte) 84);
					} else {
						chatMembers[member].accountName = chatMembers[member].displayName;
					}
					chatMembers[member].filteredName = Class353.filterName(i ^ ~0x1f4decc3, chatMembers[member].accountName);
					chatMembers[member].worldOrLobbyColour = buffer.readShort((byte) 127);
					chatMembers[member].chatRank = buffer.readSignedByte((byte) -19);
					chatMembers[member].world = buffer.readString((byte) 84);
					if (chatMembers[member].accountName.equals(Class87.localPlayer.accountName)) {
						Matrix.clanChatRank = chatMembers[member].chatRank;
					}
					System.out.println("Display name: " + chatMembers[member].displayName + " AccountName: " + chatMembers[member].accountName + " Filtered name: " + chatMembers[member].filteredName + " Rank: " + chatMembers[member].chatRank + " Colour: " + chatMembers[member].worldOrLobbyColour
							+ " World: " + chatMembers[member].world);
				}
				boolean ordered = false;
				int clanSize = FloorOverlayDefinitionParser.clanChatSize;
				while (clanSize > 0) {
					ordered = true;
					clanSize--;
					for (int member = 0; clanSize > member; member++) {
						if ((chatMembers[member].filteredName.compareTo(chatMembers[1 + member].filteredName) ^ 0xffffffff) < -1) {
							ClanChatMember memb = chatMembers[member];
							chatMembers[member] = chatMembers[member + 1];
							chatMembers[1 + member] = memb;
							ordered = false;
						}
					}
					if (ordered) {
						break;
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				WhirlpoolGenerator.clanChat = chatMembers;
				return true;
			}
			if (Class18.aClass58_215 == CURRENT_INCOMING_OPCODE) {
				boolean bool = buffer.readUnsignedByte((byte) -117) == 1;
				String string = buffer.readString((byte) 84);
				String string_280_ = string;
				if (bool) {
					string_280_ = buffer.readString((byte) 84);
				}
				long l = buffer.readLong(-122);
				long l_281_ = buffer.readShort((byte) 127);
				long l_282_ = buffer.readMediumInt(i ^ ~0x1f4decbe);
				int i_283_ = buffer.readUnsignedByte((byte) -119);
				int i_284_ = buffer.readShort((byte) 127);
				long l_285_ = l_282_ + (l_281_ << -741032544);
				boolean bool_286_ = false;
				while_31_: do {
					for (int i_287_ = 0; (i_287_ ^ 0xffffffff) > -101; i_287_++) {
						if (l_285_ == Class94.aLongArray794[i_287_]) {
							bool_286_ = true;
							break while_31_;
						}
					}
					if (i_283_ <= 1 && Class14.isIgnored(string_280_, (byte) 103)) {
						bool_286_ = true;
					}
				} while (false);
				if (!bool_286_ && Class22.anInt216 == 0) {
					Class94.aLongArray794[ClanChatMember.messageCount] = l_285_;
					ClanChatMember.messageCount = (ClanChatMember.messageCount - -1) % 100;
					String string_288_ = NewsLSEConfig.quickChatMessageList.get(i_284_, 98).decodeMessage(15281, buffer);
					if (i_283_ == 2) {
						Class137.addChatMessage(string, 20, "<img=1>" + string, string_288_, i_284_, Class18.longToString(l, -76), (byte) -117, 0, "<img=1>" + string_280_);
					} else if (i_283_ == 1) {
						Class137.addChatMessage(string, 20, "<img=0>" + string, string_288_, i_284_, Class18.longToString(l, -113), (byte) -121, 0, "<img=0>" + string_280_);
					} else {
						Class137.addChatMessage(string, 20, string, string_288_, i_284_, Class18.longToString(l, -81), (byte) -53, 0, string_280_);
					}
				}
				System.out.println("Reached this;");
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Billboard.VARPBIT_SMALL == CURRENT_INCOMING_OPCODE) {
				int id = buffer.readLEShortA((byte) -102);
				int value = buffer.readIntReverse(true);
				Class98_Sub25.method1274(i ^ ~0x1f4decc3);
				Class116.setVarpBit(value, id, (byte) -120);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class98_Sub6.aClass58_3844) {
				boolean bool = buffer.readUnsignedByte((byte) 117) == 1;
				String string = buffer.readString((byte) 84);
				String string_291_ = string;
				if (bool) {
					string_291_ = buffer.readString((byte) 84);
				}
				long l = buffer.readShort((byte) 127);
				long l_292_ = buffer.readMediumInt(-123);
				int i_293_ = buffer.readUnsignedByte((byte) 79);
				int i_294_ = buffer.readShort((byte) 127);
				long l_295_ = l_292_ + (l << 812765856);
				boolean bool_296_ = false;
				while_32_: do {
					for (int i_297_ = 0; i_297_ < 100; i_297_++) {
						if (Class94.aLongArray794[i_297_] == l_295_) {
							bool_296_ = true;
							break while_32_;
						}
					}
					if (i_293_ <= 1 && Class14.isIgnored(string_291_, (byte) 104)) {
						bool_296_ = true;
					}
				} while (false);
				if (!bool_296_ && (Class22.anInt216 ^ 0xffffffff) == -1) {
					Class94.aLongArray794[ClanChatMember.messageCount] = l_295_;
					ClanChatMember.messageCount = (ClanChatMember.messageCount - -1) % 100;
					String string_298_ = NewsLSEConfig.quickChatMessageList.get(i_294_, 98).decodeMessage(15281, buffer);
					if ((i_293_ ^ 0xffffffff) == -3) {
						Class137.addChatMessage(string, 18, "<img=1>" + string, string_298_, i_294_, null, (byte) -101, 0, "<img=1>" + string_291_);
					} else if ((i_293_ ^ 0xffffffff) != -2) {
						Class137.addChatMessage(string, 18, string, string_298_, i_294_, null, (byte) -97, 0, string_291_);
					} else {
						Class137.addChatMessage(string, 18, "<img=0>" + string, string_298_, i_294_, null, (byte) -126, 0, "<img=0>" + string_291_);
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class76.aClass58_587 == CURRENT_INCOMING_OPCODE) {
				Class166.method2525(0);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == ScreenSizePreferenceField.aClass58_3715) {
				String string = buffer.readString((byte) 84);
				Object[] objects = new Object[string.length() + 1];
				for (int i_299_ = string.length() + -1; i_299_ >= 0; i_299_--) {
					if (string.charAt(i_299_) != 's') {
						objects[1 + i_299_] = new Integer(buffer.readInt(-2));
					} else {
						objects[i_299_ + 1] = buffer.readString((byte) 84);
					}
				}
				objects[0] = new Integer(buffer.readInt(-2));
				Class98_Sub25.method1274(-1);
				ClientScript2Event class98_sub21 = new ClientScript2Event();
				class98_sub21.param = objects;
				ClientScript2Runtime.handleEvent(class98_sub21);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == MapScenesDefinitionParser.NPC_ON_INTERFACE) {
				int i_300_ = buffer.readInt2(-112);
				int npcId = buffer.readShort1((byte) -77);
				if ((npcId ^ 0xffffffff) == -65536) {
					npcId = -1;
				}
				Class98_Sub25.method1274(-1);
				System.out.println("String: " + i_300_);
				ReflectionRequest.method1164(npcId, -1, 2, i ^ 0x1f4decc7, i_300_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (CURRENT_INCOMING_OPCODE == Class251.aClass58_1921) {
				int i_302_ = buffer.readShort1((byte) 78);
				if (i_302_ == 65535) {
					i_302_ = -1;
				}
				int i_303_ = buffer.readInt(-2);
				Class98_Sub25.method1274(i ^ ~0x1f4decc3);
				ReflectionRequest.method1164(i_302_, -1, 1, 4, i_303_);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class287.aClass58_2187 == CURRENT_INCOMING_OPCODE) {
				int i_304_ = buffer.readUnsignedByte((byte) 8);
				Class98_Sub25.method1274(-1);
				Class103.anInt896 = i_304_;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class287.aClass58_2194 == CURRENT_INCOMING_OPCODE) {
				WorldMapInfoDefinition.anInt255 = buffer.readUShort(false);
				Class98_Sub36.anInt4161 = WorldMapInfoDefinition.logicTimer;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (Class98_Sub46_Sub6.aClass58_5975 == CURRENT_INCOMING_OPCODE) {
				int i_305_ = buffer.readInt(-2);
				Class187.aClass143_1449 = GameShell.signLink.resolveHostName(i_305_, 113);
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (BuildLocation.aClass58_1507 == CURRENT_INCOMING_OPCODE) {
				StartupStage.varValues.method2288((byte) -79);
				VarClientStringsDefinitionParser.anInt1844 += 32;
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			if (SafeModePreferenceField.aClass58_3642 == CURRENT_INCOMING_OPCODE) { // rawr
				MemoryCacheNodeFactory.tileZ = buffer.readUnsignedByte((byte) -115);
				MapScenesDefinitionParser.chunkY = buffer.method1234(128) << 212170051;
				Class53.chunkX = buffer.readSignedByte((byte) -19) << 773724419;
				for (ItemDeque class98_sub45 = (ItemDeque) ModelRenderer.groundItems.startIteration(105); class98_sub45 != null; class98_sub45 = (ItemDeque) ModelRenderer.groundItems.iterateNext(-1)) {
					int i_306_ = (int) (0x3L & class98_sub45.hash >> 1200025692);
					int i_307_ = (int) (0x3fffL & class98_sub45.hash);
					int i_308_ = i_307_ + -Class272.gameSceneBaseX;
					int i_309_ = (int) (0x3fffL & class98_sub45.hash >> 1428694926);
					int i_310_ = -aa_Sub2.gameSceneBaseY + i_309_;
					if (MemoryCacheNodeFactory.tileZ == i_306_ && i_308_ >= Class53.chunkX && (Class53.chunkX - -8 ^ 0xffffffff) < (i_308_ ^ 0xffffffff) && MapScenesDefinitionParser.chunkY <= i_310_ && (i_310_ ^ 0xffffffff) > (MapScenesDefinitionParser.chunkY - -8 ^ 0xffffffff)) {
						class98_sub45.unlink(i ^ 0x1f4dec80);
						if ((i_308_ ^ 0xffffffff) <= -1 && (i_310_ ^ 0xffffffff) <= -1 && i_308_ < Class165.mapWidth && (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) < (i_310_ ^ 0xffffffff)) {
							Class246_Sub3_Sub2_Sub1.method1437(i_308_, MemoryCacheNodeFactory.tileZ, (byte) 122, i_310_);
						}
					}
				}
				for (Class98_Sub33 class98_sub33 = (Class98_Sub33) Class191.aClass148_1478.getFirst(32); class98_sub33 != null; class98_sub33 = (Class98_Sub33) Class191.aClass148_1478.getNext(96)) {
					if ((class98_sub33.localX ^ 0xffffffff) <= (Class53.chunkX ^ 0xffffffff) && class98_sub33.localX < 8 + Class53.chunkX && (class98_sub33.localZ ^ 0xffffffff) <= (MapScenesDefinitionParser.chunkY ^ 0xffffffff) && (8 + MapScenesDefinitionParser.chunkY
							^ 0xffffffff) < (class98_sub33.localZ ^ 0xffffffff) && MemoryCacheNodeFactory.tileZ == class98_sub33.anInt4116) {
						class98_sub33.aBoolean4124 = true;
					}
				}
				for (Class98_Sub33 class98_sub33 = (Class98_Sub33) OutgoingPacket.aClass148_3866.getFirst(32); class98_sub33 != null; class98_sub33 = (Class98_Sub33) OutgoingPacket.aClass148_3866.getNext(i + -525200461)) {
					if ((Class53.chunkX ^ 0xffffffff) >= (class98_sub33.localX ^ 0xffffffff) && class98_sub33.localX < 8 + Class53.chunkX && MapScenesDefinitionParser.chunkY <= class98_sub33.localZ && (8 + MapScenesDefinitionParser.chunkY ^ 0xffffffff) < (class98_sub33.localZ ^ 0xffffffff)
							&& (MemoryCacheNodeFactory.tileZ ^ 0xffffffff) == (class98_sub33.anInt4116 ^ 0xffffffff)) {
						class98_sub33.aBoolean4124 = true;
					}
				}
				CURRENT_INCOMING_OPCODE = null;
				return true;
			}
			Class305_Sub1.reportError(null, i ^ ~0x1f4decb8, "T1 - " + (CURRENT_INCOMING_OPCODE != null ? CURRENT_INCOMING_OPCODE.method521((byte) 67) : -1) + "," + (ProceduralTextureSource.aClass58_3262 == null ? -1 : ProceduralTextureSource.aClass58_3262.method521((byte) 76)) + ","
					+ (Class98_Sub10_Sub21.aClass58_5641 == null ? -1 : Class98_Sub10_Sub21.aClass58_5641.method521((byte) 71)) + " - " + Class65.currentPacketSize);
			Class98_Sub10_Sub1.handleLogout(false, false);
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bia.I(" + i + ')');
		}
	}

}

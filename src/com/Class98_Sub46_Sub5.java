/* Class98_Sub46_Sub5 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.archive.Js5Exception;

public final class Class98_Sub46_Sub5 extends Cacheable {
	public static Class174[] aClass174Array5970;

	public static final int light(int i, byte i_5_, int i_6_) {
		try {
			if (i_5_ <= 103) {
				return 86;
			}
			int i_7_ = i >>> -280491016;
			i = (i_7_ * (0xff00 & i) & 0xff0000 | ~0xff00ff & i_7_ * (0xff00ff & i)) >>> 591549448;
			int i_8_ = 255 + -i_7_;
			return ((i_8_ * (i_6_ & 0xff00) & 0xff0000 | ~0xff00ff & (0xff00ff & i_6_) * i_8_) >>> -1712218008) + i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "de.B(" + i + ',' + i_5_ + ',' + i_6_ + ')');
		}
	}

	public static final boolean method1543(int i, int i_0_, byte i_1_) {
		try {
			if (!Player.menuOpen) {
				return false;
			}
			if (i_1_ != 6) {
				method1543(-33, 90, (byte) -77);
			}
			int i_2_ = i >> -831084784;
			int i_3_ = i & 0xffff;
			if (Class159.interfaceStore[i_2_] == null || Class159.interfaceStore[i_2_][i_3_] == null) {
				return false;
			}
			RtInterface class293 = Class159.interfaceStore[i_2_][i_3_];
			if ((i_0_ ^ 0xffffffff) == 0 && (class293.type ^ 0xffffffff) == -1) {
				for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getFirst(32); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getNext(i_1_ + 99)) {
					if (class98_sub46_sub8.actionId == 59 || (class98_sub46_sub8.actionId ^ 0xffffffff) == -1007 || class98_sub46_sub8.actionId == 21 || class98_sub46_sub8.actionId == 49 || class98_sub46_sub8.actionId == 9) {
						for (RtInterface class293_4_ = RtInterface.getInterface(class98_sub46_sub8.anInt5993, i_1_ ^ ~0x265d); class293_4_ != null; class293_4_ = RtInterface.getParent(true, class293_4_)) {
							if ((class293_4_.idDword ^ 0xffffffff) == (class293.idDword ^ 0xffffffff)) {
								return true;
							}
						}
					}
				}
			} else {
				for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getFirst(32); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getNext(112)) {
					if (i_0_ == class98_sub46_sub8.anInt5995 && class98_sub46_sub8.anInt5993 == class293.idDword && ((class98_sub46_sub8.actionId ^ 0xffffffff) == -60 || class98_sub46_sub8.actionId == 1006 || class98_sub46_sub8.actionId == 21 || class98_sub46_sub8.actionId == 49
							|| class98_sub46_sub8.actionId == 9)) {
						return true;
					}
				}
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "de.A(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method1545(byte i) {
		do {
			try {
				aClass174Array5970 = null;
				if (i == 78) {
					break;
				}
				aClass174Array5970 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "de.C(" + i + ')');
			}
			break;
		} while (false);
	}

	public Class246_Sub3_Sub4_Sub4 aClass246_Sub3_Sub4_Sub4_5969;

	Class98_Sub46_Sub5(Class246_Sub3_Sub4_Sub4 class246_sub3_sub4_sub4) {
		try {
			aClass246_Sub3_Sub4_Sub4_5969 = class246_sub3_sub4_sub4;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "de.<init>(" + (class246_sub3_sub4_sub4 != null ? "{...}" : "null") + ')');
		}
	}
}

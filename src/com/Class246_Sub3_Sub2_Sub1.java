/* Class246_Sub3_Sub2_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;
import com.jagex.game.client.preferences.LightningDetailPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.input.impl.AwtMouseListener;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class246_Sub3_Sub2_Sub1 extends Animable {
	public static OutgoingOpcode	aClass171_6342	= new OutgoingOpcode(73, 8);
	public static IncomingOpcode	aClass58_6335	= new IncomingOpcode(65, -1);
	public static int				anInt6345		= 0;

	public static final void method1437(int tileX, int tileZ, byte dummy, int tileY) {
		int x = Class272.gameSceneBaseX + tileX;
		int y = aa_Sub2.gameSceneBaseY + tileY;
		if (QuickChatCategory.aClass172ArrayArrayArray5948 != null && tileX >= 0 && tileY >= 0 && (Class165.mapWidth ^ 0xffffffff) < (tileX ^ 0xffffffff) && (tileY ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff) && ((client.preferences.aClass64_Sub3_4076.getValue((byte) 122)
				^ 0xffffffff) != -1 || (tileZ ^ 0xffffffff) == (Class87.localPlayer.plane ^ 0xffffffff))) {
			long hash = tileZ << 1137066972 | y << -790678898 | x;
			ItemDeque deque = (ItemDeque) ModelRenderer.groundItems.get(hash, -1);
			if (deque == null) {
				Class281.method3333(tileZ, tileX, tileY);
			} else {
				GroundItem groundItem = (GroundItem) deque.aClass148_4254.getFirst(32);
				if (groundItem == null) {
					Class281.method3333(tileZ, tileX, tileY);
				} else {
					Class246_Sub3_Sub2_Sub1 class246_sub3_sub2_sub1 = (Class246_Sub3_Sub2_Sub1) Class281.method3333(tileZ, tileX, tileY);
					if (class246_sub3_sub2_sub1 == null) {
						class246_sub3_sub2_sub1 = new Class246_Sub3_Sub2_Sub1(tileX << -489573239, Class78.aSArray594[tileZ].getTileHeight(tileY, -12639, tileX), tileY << 1477649545, tileZ, tileZ);
					} else {
						class246_sub3_sub2_sub1.anInt6341 = class246_sub3_sub2_sub1.anInt6343 = -1;
					}
					class246_sub3_sub2_sub1.anInt6338 = groundItem.amount;
					class246_sub3_sub2_sub1.anInt6340 = groundItem.itemId;
					for (;;) {
						GroundItem class98_sub26_22_ = (GroundItem) deque.aClass148_4254.getNext(88);
						if (class98_sub26_22_ == null) {
							break;
						}
						if (class246_sub3_sub2_sub1.anInt6340 != class98_sub26_22_.itemId) {
							class246_sub3_sub2_sub1.anInt6346 = class98_sub26_22_.amount;
							class246_sub3_sub2_sub1.anInt6341 = class98_sub26_22_.itemId;
							for (;;) {
								GroundItem class98_sub26_23_ = (GroundItem) deque.aClass148_4254.getNext(101);
								if (class98_sub26_23_ == null) {
									break;
								}
								if (class98_sub26_23_.itemId != class246_sub3_sub2_sub1.anInt6340 && (class246_sub3_sub2_sub1.anInt6341 ^ 0xffffffff) != (class98_sub26_23_.itemId ^ 0xffffffff)) {
									class246_sub3_sub2_sub1.anInt6343 = class98_sub26_23_.itemId;
									class246_sub3_sub2_sub1.anInt6337 = class98_sub26_23_.amount;
								}
							}
							break;
						}
					}
					int i_24_ = StrongReferenceMCNode.getHeight(tileZ, (tileY << -1106673015) + 256, 256 + (tileX << 1297062281), 24111);
					class246_sub3_sub2_sub1.anInt6339 = 0;
					class246_sub3_sub2_sub1.plane = (byte) tileZ;
					class246_sub3_sub2_sub1.boundExtentsZ = tileY << 657245129;
					class246_sub3_sub2_sub1.anInt5089 = i_24_;
					class246_sub3_sub2_sub1.boundExtentsX = tileX << -121249015;
					class246_sub3_sub2_sub1.collisionPlane = (byte) tileZ;
					if (Class1.isBridge(tileY, (byte) -102, tileX)) {
						class246_sub3_sub2_sub1.collisionPlane++;
					}
					SunDefinition.method3239(tileZ, tileX, tileY, i_24_, class246_sub3_sub2_sub1);
				}
			}
		}
	}

	public static final boolean method3007(int i, int i_2_, byte i_3_) {
		try {
			if (i_3_ != -15) {
				return false;
			}
			return !(!((0x40000 & i ^ 0xffffffff) != -1 | LightningDetailPreferenceField.method594(i, 6, i_2_)) && !Class228.method2864(55, i, i_2_));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.C(" + i + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public static final void method3008(byte i) {
		try {
			synchronized (RtMouseListener.aClass79_2493) {
				if (i != 60) {
					/* empty */
				} else {
					RtMouseListener.aClass79_2493.freeSoftReferences((byte) -114);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.E(" + i + ')');
		}
	}

	public static final void method3009(int i, ActionQueueEntry class98_sub46_sub8, int i_35_, int i_36_, int i_37_, int i_38_, int i_39_, RSToolkit var_ha, int i_40_, int i_41_, int i_42_, int i_43_) {
		do {
			try {
				if ((i_39_ ^ 0xffffffff) < (i_43_ ^ 0xffffffff) && i_39_ < i_36_ + i_43_ && i_40_ > -13 + i_37_ && i_40_ < 3 + i_37_ && class98_sub46_sub8.aBoolean5984) {
					i_42_ = i_35_;
				}
				int[] is = null;
				if (!AwtMouseListener.method3526(119, class98_sub46_sub8.actionId)) {
					if (class98_sub46_sub8.anInt5988 == -1) {
						if (!Class36.method340(class98_sub46_sub8.actionId, (byte) -87)) {
							if (Class98_Sub10_Sub21.method1064(class98_sub46_sub8.actionId, false)) {
								GameObjectDefinition class352;
								if ((class98_sub46_sub8.actionId ^ 0xffffffff) == -1010) {
									class352 = Class130.gameObjectDefinitionList.get((int) class98_sub46_sub8.aLong5987, (byte) 119);
								} else {
									class352 = Class130.gameObjectDefinitionList.get((int) (0x7fffffffL & class98_sub46_sub8.aLong5987 >>> 615469152), (byte) 119);
								}
								if (class352.transformIDs != null) {
									class352 = class352.get(StartupStage.varValues, (byte) -66);
								}
								if (class352 != null) {
									is = class352.anIntArray2934;
								}
							}
						} else {
							NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get((int) class98_sub46_sub8.aLong5987, -1);
							if (class98_sub39 != null) {
								NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
								NPCDefinition class141 = class246_sub3_sub4_sub2_sub1.definition;
								if (class141.anIntArray1109 != null) {
									class141 = class141.method2300(StartupStage.varValues, (byte) 32);
								}
								if (class141 != null) {
									is = class141.anIntArray1152;
								}
							}
						}
					} else {
						is = Class98_Sub46_Sub19.itemDefinitionList.get(class98_sub46_sub8.anInt5988, (byte) -117).campaigns;
					}
				} else {
					is = Class98_Sub46_Sub19.itemDefinitionList.get((int) class98_sub46_sub8.aLong5987, (byte) -127).campaigns;
				}
				String string = HitmarksDefinition.getMenuText((byte) -124, class98_sub46_sub8);
				if (is != null) {
					string += GroundBlendingPreferenceField.method653(0, is);
				}
				Class98_Sub10_Sub34.p13Full.method413(i_37_, Class64_Sub5.anIntArray3652, i_41_, string, i_42_, 3 + i_43_, (byte) 18, GroupProgressMonitor.aClass332Array3408);
				if (!class98_sub46_sub8.aBoolean5983) {
					break;
				}
				Class284_Sub2_Sub2.aClass332_6199.draw(Class42_Sub1.p13FullMetrics.method2674(string, 118) + i_43_ - -5, -12 + i_37_);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "iha.D(" + i + ',' + (class98_sub46_sub8 != null ? "{...}" : "null") + ',' + i_35_ + ',' + i_36_ + ',' + i_37_ + ',' + i_38_ + ',' + i_39_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_40_ + ',' + i_41_ + ',' + i_42_
						+ ',' + i_43_ + ')');
			}
			break;
		} while (false);
	}

	public static void method3010(byte i) {
		try {
			if (i == -37) {
				aClass58_6335 = null;
				aClass171_6342 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.A(" + i + ')');
		}
	}

	private boolean	aBoolean6336	= false;
	int				anInt6337;
	int				anInt6338;

	int				anInt6339		= 0;

	int				anInt6340;

	public int		anInt6341;

	int				anInt6343		= -1;

	private int		anInt6344		= 0;

	int				anInt6346;

	Class246_Sub3_Sub2_Sub1(int i, int i_44_, int i_45_, int i_46_, int i_47_) {
		super(i, i_44_, i_45_, i_46_, i_47_);
		anInt6341 = -1;
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (i != -53) {
				method2974((byte) -91, null);
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			Class154 class154 = Class94.method914(this.plane, this.boundExtentsX >> Class151_Sub8.tileScale, this.boundExtentsZ >> Class151_Sub8.tileScale);
			if (class154 != null && class154.aClass246_Sub3_Sub4_1232.aBoolean6162) {
				int i_4_ = class154.aClass246_Sub3_Sub4_1232.method2990(0);
				if (anInt6339 != i_4_) {
					this.anInt5089 -= anInt6339;
					this.anInt5089 += i_4_;
					anInt6339 = i_4_;
				}
			}
			Matrix class111 = var_ha.method1793();
			class111.initIdentity();
			if (i > -12) {
				return null;
			}
			if (class154 == null || !class154.aClass246_Sub3_Sub4_1232.aBoolean6162) {
				Ground var_s = Class78.aSArray594[this.collisionPlane];
				int i_7_ = anInt6344 << -1155039327;
				int i_8_ = i_7_;
				int i_9_ = -i_7_ / 2;
				int i_10_ = -i_8_ / 2;
				int i_11_ = var_s.averageHeight(this.boundExtentsX - -i_9_, this.boundExtentsZ - -i_10_, true);
				int i_12_ = i_7_ / 2;
				int i_13_ = -i_8_ / 2;
				int i_14_ = var_s.averageHeight(this.boundExtentsX + i_12_, this.boundExtentsZ - -i_13_, true);
				int i_15_ = -i_7_ / 2;
				int i_16_ = i_8_ / 2;
				int i_17_ = var_s.averageHeight(this.boundExtentsX - -i_15_, this.boundExtentsZ - -i_16_, true);
				int i_18_ = i_7_ / 2;
				int i_19_ = i_8_ / 2;
				int i_20_ = var_s.averageHeight(i_18_ + this.boundExtentsX, i_19_ + this.boundExtentsZ, true);
				int i_21_ = i_14_ <= i_11_ ? i_14_ : i_11_;
				int i_22_ = (i_20_ ^ 0xffffffff) >= (i_17_ ^ 0xffffffff) ? i_20_ : i_17_;
				int i_23_ = (i_14_ ^ 0xffffffff) > (i_20_ ^ 0xffffffff) ? i_14_ : i_20_;
				int i_24_ = (i_11_ ^ 0xffffffff) > (i_17_ ^ 0xffffffff) ? i_11_ : i_17_;
				if (i_8_ != 0) {
					int i_25_ = 0x3fff & (int) (2607.5945876176133 * Math.atan2(i_21_ - i_22_, i_8_));
					if ((i_25_ ^ 0xffffffff) != -1) {
						class111.initRotX(i_25_);
					}
				}
				if ((i_7_ ^ 0xffffffff) != -1) {
					int i_26_ = 0x3fff & (int) (2607.5945876176133 * Math.atan2(-i_23_ + i_24_, i_7_));
					if (i_26_ != 0) {
						class111.method2090(-i_26_);
					}
				}
				int i_27_ = i_20_ + i_11_;
				if ((i_27_ ^ 0xffffffff) < (i_14_ + i_17_ ^ 0xffffffff)) {
					i_27_ = i_17_ + i_14_;
				}
				i_27_ = (i_27_ >> 147921889) - this.anInt5089;
				if ((i_27_ ^ 0xffffffff) != -1) {
					class111.translate(0, i_27_, 0);
				}
			}
			class111.translate(this.boundExtentsX, -10 + this.anInt5089, this.boundExtentsZ);
			Class246_Sub1 class246_sub1 = Class94.method915(3, (byte) -47, true);
			aBoolean6336 = false;
			anInt6344 = 0;
			if ((anInt6343 ^ 0xffffffff) != 0) {
				ModelRenderer class146 = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6343, (byte) -118).method3501(0, 2048, 0, null, -1, var_ha, anInt6337, 128, null);
				if (class146 != null) {
					if (!VarClientStringsDefinitionParser.aBoolean1839) {
						class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[2], 0);
					} else {
						class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[2], Class16.anInt197, 0);
					}
					aBoolean6336 |= class146.F();
					anInt6344 = class146.ma();
				}
			}
			if (anInt6341 != -1) {
				ModelRenderer class146 = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6341, (byte) -122).method3501(0, 2048, 0, null, -1, var_ha, anInt6346, 128, null);
				if (class146 != null) {
					if (!VarClientStringsDefinitionParser.aBoolean1839) {
						class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[1], 0);
					} else {
						class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[1], Class16.anInt197, 0);
					}
					aBoolean6336 |= class146.F();
					if (class146.ma() > anInt6344) {
						anInt6344 = class146.ma();
					}
				}
			}
			ModelRenderer class146 = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6340, (byte) -121).method3501(0, 2048, 0, null, -1, var_ha, anInt6338, 128, null);
			if (class146 != null) {
				if (VarClientStringsDefinitionParser.aBoolean1839) {
					class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
				} else {
					class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
				}
				aBoolean6336 |= class146.F();
				if ((class146.ma() ^ 0xffffffff) < (anInt6344 ^ 0xffffffff)) {
					anInt6344 = class146.ma();
				}
			}
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_31_, int i_32_) {
		try {
			if (i_31_ < 59) {
				method2990(-84);
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(this.boundExtentsX, this.anInt5089 + -10, this.boundExtentsZ);
			ItemDefinition class297 = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6340, (byte) -118);
			ModelRenderer class146 = class297.method3501(0, 131072, 0, null, -1, var_ha, anInt6338, 128, null);
			if (class146 != null && (VarClientStringsDefinitionParser.aBoolean1839 ? class146.method2333(i, i_32_, class111, true, class297.pickSizeShift, Class16.anInt197) : class146.method2339(i, i_32_, class111, true, class297.pickSizeShift))) {
				return true;
			}
			if ((anInt6341 ^ 0xffffffff) != 0) {
				ItemDefinition class297_33_ = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6341, (byte) -121);
				class146 = class297_33_.method3501(0, 131072, 0, null, -1, var_ha, anInt6346, 128, null);
				if (class146 != null && (VarClientStringsDefinitionParser.aBoolean1839 ? class146.method2333(i, i_32_, class111, true, class297_33_.pickSizeShift, Class16.anInt197) : class146.method2339(i, i_32_, class111, true, class297_33_.pickSizeShift))) {
					return true;
				}
			}
			if (anInt6343 != -1) {
				ItemDefinition class297_34_ = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6343, (byte) -127);
				class146 = class297_34_.method3501(0, 131072, 0, null, -1, var_ha, anInt6337, 128, null);
				if (class146 != null && (VarClientStringsDefinitionParser.aBoolean1839 ? class146.method2333(i, i_32_, class111, true, class297_34_.pickSizeShift, Class16.anInt197) : class146.method2339(i, i_32_, class111, true, class297_34_.pickSizeShift))) {
					return true;
				}
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_31_ + ',' + i_32_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.H(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				aClass171_6342 = null;
			}
			return anInt6344;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.O(" + bool + ')');
		}
	}

	@Override
	public final int method2986(int i) {
		try {
			if (i != -14240) {
				return 114;
			}
			ItemDefinition class297 = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6340, (byte) -116);
			int i_28_ = class297.pickSizeShift;
			if (anInt6341 != -1) {
				ItemDefinition class297_29_ = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6341, (byte) -122);
				if ((class297_29_.pickSizeShift ^ 0xffffffff) < (i_28_ ^ 0xffffffff)) {
					i_28_ = class297_29_.pickSizeShift;
				}
			}
			if ((anInt6343 ^ 0xffffffff) != 0) {
				ItemDefinition class297_30_ = Class98_Sub46_Sub19.itemDefinitionList.get(anInt6343, (byte) -118);
				if ((i_28_ ^ 0xffffffff) > (class297_30_.pickSizeShift ^ 0xffffffff)) {
					i_28_ = class297_30_.pickSizeShift;
				}
			}
			return i_28_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.FB(" + i + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (i != 6540) {
				return true;
			}
			return aBoolean6336;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		try {
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				anInt6343 = 51;
			}
			return -10;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "iha.J(" + i + ')');
		}
	}
}

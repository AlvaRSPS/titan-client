
/* Class3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class3 {
	public static IncomingOpcode		aClass58_75	= new IncomingOpcode(81, 4);
	public static float					aFloat78;
	public static int					anInt77		= 100;
	public static LoadingScreenSequence	loadingScreenSequence;

	public static final void method172(int i) {
		do {
			if ((client.topLevelInterfaceId ^ 0xffffffff) != 0) {
				int i_0_ = client.mouseListener.getMouseX(61);
				int i_1_ = client.mouseListener.getMouseY((byte) 80);
				RtMouseEvent class98_sub17 = (RtMouseEvent) VarPlayerDefinition.aClass148_1284.getFirst(32);
				if (class98_sub17 != null) {
					i_0_ = class98_sub17.getMouseX(123);
					i_1_ = class98_sub17.getMouseY(48);
				}
				int i_2_ = 0;
				int i_3_ = 0;
				if (OpenGLHeap.aBoolean6079) {
					i_2_ = Class189.method2642((byte) 42);
					i_3_ = MapScenesDefinitionParser.method3765(false);
				}
				Class62.method544(i_2_ + i_0_, i_3_, client.topLevelInterfaceId, i_2_ - -GameShell.frameWidth, 0, GameShell.frameHeight + i_3_, i_2_, i_0_, i_1_, i_3_ + i_1_, i_3_, i_2_);
				if (CursorDefinitionParser.aClass293_120 == null) {
					break;
				}
				Entity.method3099(i_0_ - -i_2_, i_3_ + i_1_, (byte) 2);
			}
			break;
		} while (false);
	}
}

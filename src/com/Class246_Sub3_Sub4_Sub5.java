/* Class246_Sub3_Sub4_Sub5 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class246_Sub3_Sub4_Sub5 extends Class246_Sub3_Sub4 implements Interface19 {
	public static Class258			aClass258_6260	= new Class258();
	public static IncomingOpcode	aClass58_6264	= new IncomingOpcode(24, 6);
	public static int[]				mapSizes		= { 104, 120, 136, 168 };

	public static final void method3084(boolean bool) {
		do {
			try {
				if (bool != true) {
					aClass58_6264 = null;
				}
				if (SystemInformation.aClass268_1173 != null) {
					SystemInformation.aClass268_1173.method3256((byte) 112);
				}
				if (Class27.aClass268_276 == null) {
					break;
				}
				Class27.aClass268_276.method3256((byte) 79);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ts.D(" + bool + ')');
			}
			break;
		} while (false);
	}

	public static final int method3085(int i) {
		try {
			int i_4_;
			if (GameShell.maxHeap >= 96) {
				int i_5_ = Class278_Sub1.method3320(12);
				if ((i_5_ ^ 0xffffffff) >= -101) {
					GamePreferences.method1284(i + 1);
					i_4_ = 4;
				} else if ((i_5_ ^ 0xffffffff) >= -501) {
					i_4_ = 3;
					Class98_Sub50.method1672((byte) 19);
				} else if ((i_5_ ^ 0xffffffff) < -1001) {
					GraphicsBuffer.method1436(i + -101, true);
					i_4_ = 1;
				} else {
					i_4_ = 2;
					Class287.setClientPreferences((byte) 60);
				}
			} else {
				i_4_ = 1;
				GraphicsBuffer.method1436(-102, true);
			}
			if (client.preferences.currentToolkit.getValue((byte) 124) != i) {
				client.preferences.setPreference((byte) -13, 0, client.preferences.desiredToolkit);
				Class76_Sub4.method754(0, false, -116);
			}
			Class310.method3618(i + -5964);
			return i_4_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.M(" + i + ')');
		}
	}

	public static final void method3086(int i, int[] is, int i_6_, int i_7_, int i_8_, float[] fs, int i_9_, int[] is_10_, int i_11_, int i_12_, float[] fs_13_, int i_14_, int i_15_) {
		try {
			if (i_15_ != 0) {
				mapSizes = null;
			}
			int i_16_ = i_6_ * i + i_7_;
			int i_17_ = i_12_ + i_14_ * i_9_;
			int i_18_ = -i_8_ + i;
			int i_19_ = -i_8_ + i_14_;
			if (is == null) {
				for (int i_20_ = 0; (i_20_ ^ 0xffffffff) > (i_11_ ^ 0xffffffff); i_20_++) {
					int i_21_ = i_16_ + i_8_;
					while ((i_21_ ^ 0xffffffff) < (i_16_ ^ 0xffffffff)) {
						fs[i_17_++] = fs_13_[i_16_++];
					}
					i_16_ += i_18_;
					i_17_ += i_19_;
				}
			} else if (fs_13_ == null) {
				for (int i_22_ = 0; (i_22_ ^ 0xffffffff) > (i_11_ ^ 0xffffffff); i_22_++) {
					int i_23_ = i_16_ - -i_8_;
					while ((i_16_ ^ 0xffffffff) > (i_23_ ^ 0xffffffff)) {
						is_10_[i_17_++] = is[i_16_++];
					}
					i_17_ += i_19_;
					i_16_ += i_18_;
				}
			} else {
				for (int i_24_ = 0; i_11_ > i_24_; i_24_++) {
					int i_25_ = i_16_ - -i_8_;
					while (i_16_ < i_25_) {
						is_10_[i_17_] = is[i_16_];
						fs[i_17_++] = fs_13_[i_16_++];
					}
					i_17_ += i_19_;
					i_16_ += i_18_;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.P(" + i + ',' + (is != null ? "{...}" : "null") + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + (fs != null ? "{...}" : "null") + ',' + i_9_ + ',' + (is_10_ != null ? "{...}" : "null") + ',' + i_11_ + ',' + i_12_ + ','
					+ (fs_13_ != null ? "{...}" : "null") + ',' + i_14_ + ',' + i_15_ + ')');
		}
	}

	public static void method3087(int i) {
		try {
			aClass58_6264 = null;
			aClass258_6260 = null;
			if (i == -1001) {
				mapSizes = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.L(" + i + ')');
		}
	}

	public static final byte method3088(int i, byte i_28_, int i_29_) {
		try {
			if (i_28_ != -41) {
				method3085(48);
			}
			if ((i ^ 0xffffffff) != -10) {
				return (byte) 0;
			}
			if ((0x1 & i_29_) != 0) {
				return (byte) 2;
			}
			return (byte) 1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.R(" + i + ',' + i_28_ + ',' + i_29_ + ')');
		}
	}

	private boolean		aBoolean6259	= false;

	private boolean		aBoolean6262;

	private Class228	aClass228_6263;

	Class359			aClass359_6261;

	Class246_Sub3_Sub4_Sub5(RSToolkit var_ha, GameObjectDefinition class352, int i, int i_30_, int i_31_, int i_32_, int i_33_, boolean bool, int i_34_, int i_35_, int i_36_, int i_37_, int i_38_, int i_39_, int i_40_) {
		super(i, i_30_, i_31_, i_32_, i_33_, i_34_, i_35_, i_36_, i_37_, class352.anInt2975 == 1, Class190.method2644(i_38_, 74, i_39_));
		try {
			aClass359_6261 = new Class359(var_ha, class352, i_38_, i_39_, ((Char) this).plane, i_30_, this, bool, i_40_);
			aBoolean6262 = class352.anInt2998 != 0 && !bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.<init>(" + (var_ha != null ? "{...}" : "null") + ',' + (class352 != null ? "{...}" : "null") + ',' + i + ',' + i_30_ + ',' + i_31_ + ',' + i_32_ + ',' + i_33_ + ',' + bool + ',' + i_34_ + ',' + i_35_ + ',' + i_36_ + ',' + i_37_
					+ ',' + i_38_ + ',' + i_39_ + ',' + i_40_ + ')');
		}
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (i != -53) {
				method64(119);
			}
			return aClass228_6263;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = aClass359_6261.method3897(-1, false, 2048, var_ha, true);
			if (class146 == null) {
				return null;
			}
			if (i >= -12) {
				mapSizes = null;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
			Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, aBoolean6262);
			aClass359_6261.method3895(class146, ((Class246_Sub3_Sub4) this).aShort6160, ((Class246_Sub3_Sub4) this).aShort6157, class111, true, ((Class246_Sub3_Sub4) this).aShort6158, var_ha, false, ((Class246_Sub3_Sub4) this).aShort6159);
			if (!VarClientStringsDefinitionParser.aBoolean1839) {
				class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
			} else {
				class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
			}
			if (aClass359_6261.aClass246_Sub5_3062 != null) {
				Class242 class242 = aClass359_6261.aClass246_Sub5_3062.method3116();
				if (!VarClientStringsDefinitionParser.aBoolean1839) {
					var_ha.method1820(class242);
				} else {
					var_ha.method1785(class242, Class16.anInt197);
				}
			}
			aBoolean6259 = class146.F() || aClass359_6261.aClass246_Sub5_3062 != null;
			if (aClass228_6263 != null) {
				Class283.method3350(((Char) this).anInt5089, ((Char) this).boundExtentsX, 18, ((Char) this).boundExtentsZ, class146, aClass228_6263);
			} else {
				aClass228_6263 = Class48_Sub2_Sub1.method472(((Char) this).anInt5089, ((Char) this).boundExtentsX, class146, ((Char) this).boundExtentsZ, 4);
			}
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_41_, int i_42_) {
		try {
			if (i_41_ <= 59) {
				return false;
			}
			ModelRenderer class146 = aClass359_6261.method3897(-1, false, 131072, var_ha, false);
			if (class146 == null) {
				return false;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
			if (VarClientStringsDefinitionParser.aBoolean1839) {
				return class146.method2333(i, i_42_, class111, false, 0, Class16.anInt197);
			}
			return class146.method2339(i, i_42_, class111, false, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_41_ + ',' + i_42_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.H(" + i + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_0_, RSToolkit var_ha, int i_1_, int i_2_) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_0_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i > -70) {
				method3085(81);
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.HA(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				method3085(-102);
			}
			return aClass359_6261.method3903((byte) -117);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (i != 6540) {
				method2976(59, null, (byte) -80, -69);
			}
			return aBoolean6259;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = aClass359_6261.method3897(-1, true, 262144, var_ha, true);
			if (class146 != null) {
				Matrix class111 = var_ha.method1793();
				class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
				aClass359_6261.method3895(class146, ((Class246_Sub3_Sub4) this).aShort6160, ((Class246_Sub3_Sub4) this).aShort6157, class111, false, ((Class246_Sub3_Sub4) this).aShort6158, var_ha, false, ((Class246_Sub3_Sub4) this).aShort6159);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				aBoolean6262 = false;
			}
			return aClass359_6261.method3899((byte) 126);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.J(" + i + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		try {
			if (i != -73) {
				aClass258_6260 = null;
			}
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.DA(" + i + ')');
		}
	}

	public final void method3083(Class185 class185, byte i) {
		try {
			if (i != 32) {
				method2978(-52);
			}
			aClass359_6261.method3901(class185, -126);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.K(" + (class185 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final void method61(byte i) {
		do {
			try {
				if (i == -96) {
					break;
				}
				method3088(-9, (byte) 6, -89);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ts.Q(" + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method62(RSToolkit var_ha, int i) {
		do {
			try {
				aClass359_6261.method3892(var_ha, 107);
				if (i == 24447) {
					break;
				}
				method3083(null, (byte) 57);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ts.G(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int method63(byte i) {
		try {
			if (i != 20) {
				aClass58_6264 = null;
			}
			return aClass359_6261.anInt3038;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.B(" + i + ')');
		}
	}

	@Override
	public final int method64(int i) {
		try {
			if (i != 30472) {
				return 57;
			}
			return aClass359_6261.anInt3052;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.C(" + i + ')');
		}
	}

	@Override
	public final boolean method65(boolean bool) {
		try {
			if (bool != true) {
				method66(-55);
			}
			return aClass359_6261.method3898(93);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.A(" + bool + ')');
		}
	}

	@Override
	public final int method66(int i) {
		try {
			if (i != 4657) {
				method61((byte) 109);
			}
			return aClass359_6261.anInt3059;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.N(" + i + ')');
		}
	}

	@Override
	public final void method67(int i, RSToolkit var_ha) {
		try {
			if (i != -25163) {
				aClass58_6264 = null;
			}
			aClass359_6261.method3894((byte) 119, var_ha);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ts.E(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}
}

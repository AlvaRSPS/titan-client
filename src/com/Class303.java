/* Class303 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.QuestDefinitionParser;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.Class64_Sub5;

public final class Class303 {
	public static int					anInt2530;
	public static int[]					anIntArray2531	= { 28, 35, 40, 44 };
	public static QuestDefinitionParser	questDefinitionList;

	public static final void handleKeysPressed(int keyCode, int dynPtr, String opBase, int dummy, int idDword) {
		do {
			try {
				RtInterface comp = Class246_Sub9.getDynamicComponent((byte) 72, idDword, dynPtr);
				if (comp != null) {
					if (comp.param != null) {
						ClientScript2Event event = new ClientScript2Event();
						event.param = comp.param;
						event.component = comp;
						event.code = keyCode;
						event.opbase = opBase;
						ClientScript2Runtime.handleEvent(event);
					}
					if (client.clientState == 10 && client.method116(comp).method1666((byte) -72, -1 + keyCode)) {
						if ((keyCode ^ 0xffffffff) == -2) {
							OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class246_Sub3_Sub5_Sub1.aClass171_6221, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, frame);
							Class98_Sub10_Sub29.sendPacket(false, frame);
						}
						if (keyCode == 2) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, aa_Sub2.aClass171_3564, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if (keyCode == 3) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, ActionGroup.aClass171_6000, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if ((keyCode ^ 0xffffffff) == -5) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class234.aClass171_1747, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if ((keyCode ^ 0xffffffff) == -6) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, RtInterfaceClip.aClass171_49, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if (dummy > -114) {
							handleKeysPressed(-106, 15, null, 96, 60);
						}
						if ((keyCode ^ 0xffffffff) == -7) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class98_Sub1.aClass171_3811, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if ((keyCode ^ 0xffffffff) == -8) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, DataFs.aClass171_202, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if ((keyCode ^ 0xffffffff) == -9) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class15.aClass171_177, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if ((keyCode ^ 0xffffffff) == -10) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class246_Sub3_Sub2_Sub1.aClass171_6342, Class331.aClass117_2811);
							FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						}
						if ((keyCode ^ 0xffffffff) != -11) {
							break;
						}
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, QuestDefinitionParser.aClass171_164, Class331.aClass117_2811);
						FileProgressMonitor.method2604(dynPtr, -21568, idDword, comp.unknown, class98_sub11);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sj.B(" + keyCode + ',' + dynPtr + ',' + (opBase != null ? "{...}" : "null") + ',' + dummy + ',' + idDword + ')');
			}
			break;
		} while (false);
	}

	public static void method3558(int i) {
		do {
			try {
				questDefinitionList = null;
				anIntArray2531 = null;
				if (i == 10) {
					break;
				}
				anIntArray2531 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sj.C(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void setImageTagSprites(Sprite[] class332s, int i) {
		try {
			Js5Exception.anInt3201 = class332s.length;
			GroupProgressMonitor.aClass332Array3408 = new Sprite[10 + Js5Exception.anInt3201];
			Class64_Sub5.anIntArray3652 = new int[Js5Exception.anInt3201 - -10];
			ArrayUtils.method2892(class332s, 0, GroupProgressMonitor.aClass332Array3408, i, Js5Exception.anInt3201);
			for (int i_0_ = 0; i_0_ < Js5Exception.anInt3201; i_0_++) {
				Class64_Sub5.anIntArray3652[i_0_] = GroupProgressMonitor.aClass332Array3408[i_0_].getRenderHeight();
			}
			for (int i_1_ = Js5Exception.anInt3201; i_1_ < GroupProgressMonitor.aClass332Array3408.length; i_1_++) {
				Class64_Sub5.anIntArray3652[i_1_] = 12;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sj.A(" + (class332s != null ? "{...}" : "null") + ',' + i + ')');
		}
	}
}

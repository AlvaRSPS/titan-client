/* Class170 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class170 {
	public static int[][][]	regionData;
	public static boolean	showProfiling	= false;

	public static final String method2538(int i, RtInterface class293) {
		try {
			if ((client.method116(class293).method1668(i) ^ 0xffffffff) == -1) {
				return null;
			}
			if (class293.aString2214 == null || (class293.aString2214.trim().length() ^ 0xffffffff) == -1) {
				if (Class15.qaOpTest) {
					return "Hidden-use";
				}
				return null;
			}
			return class293.aString2214;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lfa.A(" + i + ',' + (class293 != null ? "{...}" : "null") + ')');
		}
	}

	public static void method2539(byte i) {
		try {
			if (i != 13) {
				showProfiling = true;
			}
			regionData = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lfa.B(" + i + ')');
		}
	}

	public boolean	aBoolean1314;
	public byte		aByte1308;
	public byte		aByte1312;
	public int		anInt1315;
	public int		anInt1316;
	public short	aShort1309;

	public short	aShort1310;

	public short	aShort1317;

	public Class170(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, boolean bool, int i_8_) {
		try {
			aByte1312 = (byte) i_7_;
			aShort1317 = (short) i_3_;
			aShort1310 = (short) i_5_;
			aByte1308 = (byte) i_6_;
			anInt1316 = i_8_;
			aShort1309 = (short) i_4_;
			anInt1315 = i;
			aBoolean1314 = bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lfa.<init>(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + bool + ',' + i_8_ + ')');
		}
	}
}

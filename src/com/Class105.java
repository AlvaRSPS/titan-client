/* Class105 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Archive;
import com.jagex.game.input.impl.AwtKeyListener;

public final class Class105 implements Interface15 {
	public static int		anInt3415;
	public static int		anInt3417;
	public static byte[][]	regionNpcMapData;

	public static final boolean method1715(boolean bool, int i, int i_0_) {
		if (bool != true) {
			return true;
		}
		return (i & 0x22 ^ 0xffffffff) != -1;
	}

	public static final int method1717(int i, int i_8_, int i_9_, int i_10_, int i_11_) {
		int i_12_ = 0xf & i_10_;
		int i_13_ = (i_12_ ^ 0xffffffff) > -9 ? i_9_ : i_8_;
		int i_14_ = i_11_ >= (i_12_ ^ 0xffffffff) ? i_12_ != 12 && i_12_ != 14 ? i : i_9_ : i_8_;
		return ((i_12_ & 0x2) == 0 ? i_14_ : -i_14_) + ((i_12_ & 0x1 ^ 0xffffffff) == -1 ? i_13_ : -i_13_);
	}

	public static final RtAwtFontWrapper method1718(int i, int i_15_) {
		if ((i ^ 0xffffffff) != -1) {
			if (i != 1) {
				if ((i ^ 0xffffffff) == -3) {
					if (WorldMap.aFloat2064 == 3.0) {
						return Class151_Sub7.aClass326_5009;
					}
					if (WorldMap.aFloat2064 == 4.0) {
						return Class271.aClass326_2033;
					}
					if (WorldMap.aFloat2064 == 6.0) {
						return Class224.aClass326_1686;
					}
					if (WorldMap.aFloat2064 >= 8.0) {
						return ProceduralTextureSource.aClass326_3263;
					}
				}
			} else {
				if (WorldMap.aFloat2064 == 3.0) {
					return Js5Archive.aClass326_5308;
				}
				if (WorldMap.aFloat2064 == 4.0) {
					return Class137.aClass326_1080;
				}
				if (WorldMap.aFloat2064 == 6.0) {
					return Class151_Sub7.aClass326_5009;
				}
				if (WorldMap.aFloat2064 >= 8.0) {
					return Class271.aClass326_2033;
				}
			}
		} else {
			if (WorldMap.aFloat2064 == 3.0) {
				return Js5Archive.aClass326_5315;
			}
			if (WorldMap.aFloat2064 == 4.0) {
				return AwtKeyListener.aClass326_3805;
			}
			if (WorldMap.aFloat2064 == 6.0) {
				return Js5Archive.aClass326_5308;
			}
			if (WorldMap.aFloat2064 >= 8.0) {
				return Class137.aClass326_1080;
			}
		}
		return null;
	}

	int anInt3416;

	public Class105(String string, int i) {
		anInt3416 = i;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

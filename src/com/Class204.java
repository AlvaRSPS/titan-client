/* Class204 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Archive;
import com.jagex.game.client.archive.Js5Manager;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.preferences.Class64_Sub3;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.shadow.OpenGlShadow;

public final class Class204 {
	public static Class38	aClass38_1552;
	public static int		anInt1553		= 0;
	public static int[]		anIntArray1551	= new int[2];

	static {
		aClass38_1552 = new Class38();
	}

	public static final void method2709(byte i) {
		do {
			if (!Player.menuOpen) {
				Class248.aBoolean1896 = (Class64_Sub3.anInt3647 ^ 0xffffffff) != 0 && (Class64_Sub3.anInt3647 ^ 0xffffffff) >= (Class359.actionCount ^ 0xffffffff) || (GameShell.frameHeight ^ 0xffffffff) > (16 * Class359.actionCount - -(Class98_Sub5_Sub3.aBoolean5539 ? 26 : 22) ^ 0xffffffff);
			}
			Class27.aClass148_275.clear((byte) 47);
			SeekableFile.aClass148_1695.clear((byte) 47);
			for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getFirst(i ^ 0x11); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getNext(i + 60)) {
				int i_0_ = class98_sub46_sub8.actionId;
				if (i_0_ < 1000) {
					class98_sub46_sub8.unlink(52);
					if (i_0_ != 46 && i_0_ != 50 && (i_0_ ^ 0xffffffff) != -49 && (i_0_ ^ 0xffffffff) != -59 && i_0_ != 5 && i_0_ != 57 && i_0_ != 59) {
						Class27.aClass148_275.addLast(class98_sub46_sub8, i ^ ~0x519f);
					} else {
						SeekableFile.aClass148_1695.addLast(class98_sub46_sub8, -20911);
					}
				}
			}
			Class27.aClass148_275.method2425(Class33.actionList, 106);
			SeekableFile.aClass148_1695.method2425(Class33.actionList, -90);
			if ((Class359.actionCount ^ 0xffffffff) >= -2) {
				SunDefinition.aClass98_Sub46_Sub8_1994 = null;
				Class98_Sub46_Sub19.aClass98_Sub46_Sub8_6066 = null;
			} else {
				if (!Js5Manager.shiftClickEnabled || !client.keyListener.isKeyDown(81, i ^ 0x154e) || (Class359.actionCount ^ 0xffffffff) >= -3) {
					SunDefinition.aClass98_Sub46_Sub8_1994 = (ActionQueueEntry) Class33.actionList.top.previous;
				} else {
					SunDefinition.aClass98_Sub46_Sub8_1994 = (ActionQueueEntry) Class33.actionList.top.previous.previous;
				}
				Class98_Sub46_Sub19.aClass98_Sub46_Sub8_6066 = (ActionQueueEntry) Class33.actionList.top.previous;
			}
			if (i != 49) {
				method2709((byte) 76);
			}
			int i_1_ = -1;
			RtMouseEvent class98_sub17 = (RtMouseEvent) VarPlayerDefinition.aClass148_1284.getFirst(32);
			if (class98_sub17 != null) {
				i_1_ = class98_sub17.getEventType(-5);
			}
			if (!Player.menuOpen) {
				if ((i_1_ ^ 0xffffffff) == -1 && ((Class305_Sub1.anInt5303 ^ 0xffffffff) == -2 && Class359.actionCount > 2 || OpenGlShadow.method1642((byte) 127))) {
					i_1_ = 2;
				}
				if ((i_1_ ^ 0xffffffff) == -3 && Class359.actionCount > 0 && class98_sub17 != null) {
					if (Class255.aClass293_3208 != null || Class156_Sub2.anInt3423 != 0) {
						Class21_Sub4.anInt5396 = 2;
					} else {
						Class98_Sub4.method953(class98_sub17.getMouseY(94), class98_sub17.getMouseX(i + 73), true);
					}
				}
				if ((i_1_ ^ 0xffffffff) == -1) {
					if (SunDefinition.aClass98_Sub46_Sub8_1994 == null) {
						if (Class98_Sub10_Sub9.aBoolean5585) {
							Class98_Sub10_Sub32.method1098((byte) 97);
						}
					} else {
						Class246_Sub3_Sub4_Sub3.method3070(-100);
					}
				}
				if (Class255.aClass293_3208 == null && (Class156_Sub2.anInt3423 ^ 0xffffffff) == -1) {
					Class347.aClass98_Sub46_Sub8_2908 = null;
					Class21_Sub4.anInt5396 = 0;
				}
			} else {
				if (i_1_ == -1) {
					int i_2_ = client.mouseListener.getMouseX(25);
					int i_3_ = client.mouseListener.getMouseY((byte) 28);
					boolean bool = false;
					if (Class308.aClass98_Sub46_Sub9_2583 != null) {
						if (LoadingScreenSequence.anInt2128 + -10 <= i_2_ && (LoadingScreenSequence.anInt2128 - (-ScalingSpriteLoadingScreenElement.anInt3439 + -10) ^ 0xffffffff) <= (i_2_ ^ 0xffffffff) && BackgroundColourLSEConfig.anInt3518 - 10 <= i_3_ && (i_3_
								^ 0xffffffff) >= (BackgroundColourLSEConfig.anInt3518 + Class98_Sub43_Sub4.anInt5938 + 10 ^ 0xffffffff)) {
							bool = true;
						} else {
							Archive.method3789(i + 21);
						}
					}
					if (!bool) {
						if (-10 + Class38.anInt355 > i_2_ || i_2_ > Class38.anInt355 + Class246_Sub3_Sub4_Sub4.width + 10 || -10 + OpenGlArrayBufferPointer.anInt897 > i_3_ || (i_3_ ^ 0xffffffff) < (Class15.anInt172 + OpenGlArrayBufferPointer.anInt897 - -10 ^ 0xffffffff)) {
							Class317.method3651((byte) -53);
						} else if (Class248.aBoolean1896) {
							int i_4_ = -1;
							int i_5_ = -1;
							for (int i_6_ = 0; GraphicsLevelPreferenceField.groupCount > i_6_; i_6_++) {
								if (!Class98_Sub5_Sub3.aBoolean5539) {
									int i_7_ = OpenGlArrayBufferPointer.anInt897 - -31 - -(16 * i_6_);
									if (i_3_ > -13 + i_7_ && (i_3_ ^ 0xffffffff) > (3 + i_7_ ^ 0xffffffff)) {
										i_4_ = i_6_;
										i_5_ = -13 + i_7_;
										break;
									}
								} else {
									int i_8_ = 33 + OpenGlArrayBufferPointer.anInt897 + i_6_ * 16;
									if ((i_3_ ^ 0xffffffff) < (i_8_ - 13 ^ 0xffffffff) && (i_3_ ^ 0xffffffff) > (i_8_ + 4 ^ 0xffffffff)) {
										i_5_ = i_8_ + -13;
										i_4_ = i_6_;
										break;
									}
								}
							}
							if ((i_4_ ^ 0xffffffff) != 0) {
								int i_9_ = 0;
								Class252 class252 = new Class252(RtInterfaceAttachment.actionGroups);
								for (ActionGroup class98_sub46_sub9 = (ActionGroup) class252.method3173(true); class98_sub46_sub9 != null; class98_sub46_sub9 = (ActionGroup) class252.method3174(0)) {
									if (i_4_ == i_9_) {
										if (class98_sub46_sub9.actionCount > 1) {
											Class98_Sub10_Sub25.method1081(i_5_, class98_sub46_sub9, true, i_3_);
										}
										break;
									}
									i_9_++;
								}
							}
						}
					}
				}
				if (i_1_ != 0) {
					break;
				}
				int i_10_ = class98_sub17.getMouseX(124);
				int i_11_ = class98_sub17.getMouseY(40);
				if (Class308.aClass98_Sub46_Sub9_2583 != null && (LoadingScreenSequence.anInt2128 ^ 0xffffffff) >= (i_10_ ^ 0xffffffff) && i_10_ <= ScalingSpriteLoadingScreenElement.anInt3439 + LoadingScreenSequence.anInt2128 && (BackgroundColourLSEConfig.anInt3518 ^ 0xffffffff) >= (i_11_
						^ 0xffffffff) && BackgroundColourLSEConfig.anInt3518 + Class98_Sub43_Sub4.anInt5938 >= i_11_) {
					int i_12_ = -1;
					for (int i_13_ = 0; (Class308.aClass98_Sub46_Sub9_2583.actionCount ^ 0xffffffff) < (i_13_ ^ 0xffffffff); i_13_++) {
						if (Class98_Sub5_Sub3.aBoolean5539) {
							int i_14_ = 16 * i_13_ + 33 + BackgroundColourLSEConfig.anInt3518;
							if (i_14_ - 13 < i_11_ && (4 + i_14_ ^ 0xffffffff) < (i_11_ ^ 0xffffffff)) {
								i_12_ = i_13_;
							}
						} else {
							int i_15_ = 31 + BackgroundColourLSEConfig.anInt3518 - -(i_13_ * 16);
							if ((i_11_ ^ 0xffffffff) < (i_15_ - 13 ^ 0xffffffff) && i_15_ - -3 > i_11_) {
								i_12_ = i_13_;
							}
						}
					}
					if ((i_12_ ^ 0xffffffff) != 0) {
						int i_16_ = 0;
						Class252 class252 = new Class252(Class308.aClass98_Sub46_Sub9_2583.actions);
						for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) class252.method3173(true); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) class252.method3174(0)) {
							if (i_12_ == i_16_) {
								JavaNetworkWriter.method3604(i_11_, (byte) 78, i_10_, class98_sub46_sub8);
								break;
							}
							i_16_++;
						}
					}
					Class317.method3651((byte) -53);
				} else {
					if ((i_10_ ^ 0xffffffff) > (Class38.anInt355 ^ 0xffffffff) || i_10_ > Class246_Sub3_Sub4_Sub4.width + Class38.anInt355 || i_11_ < OpenGlArrayBufferPointer.anInt897 || i_11_ > OpenGlArrayBufferPointer.anInt897 + Class15.anInt172) {
						break;
					}
					if (!Class248.aBoolean1896) {
						int i_17_ = -1;
						for (int i_18_ = 0; (Class359.actionCount ^ 0xffffffff) < (i_18_ ^ 0xffffffff); i_18_++) {
							if (Class98_Sub5_Sub3.aBoolean5539) {
								int i_19_ = (-i_18_ + -1 + Class359.actionCount) * 16 + OpenGlArrayBufferPointer.anInt897 - -33;
								if ((i_11_ ^ 0xffffffff) < (-13 + i_19_ ^ 0xffffffff) && i_11_ < 4 + i_19_) {
									i_17_ = i_18_;
								}
							} else {
								int i_20_ = (-1 + Class359.actionCount + -i_18_) * 16 + OpenGlArrayBufferPointer.anInt897 + 31;
								if (i_11_ > -13 + i_20_ && (i_11_ ^ 0xffffffff) > (i_20_ - -3 ^ 0xffffffff)) {
									i_17_ = i_18_;
								}
							}
						}
						if ((i_17_ ^ 0xffffffff) != 0) {
							int i_21_ = 0;
							Class157 class157 = new Class157(Class33.actionList);
							for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) class157.method2504((byte) -116); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) class157.method2503(1000)) {
								if (i_17_ == i_21_) {
									JavaNetworkWriter.method3604(i_11_, (byte) 78, i_10_, class98_sub46_sub8);
									break;
								}
								i_21_++;
							}
						}
						Class317.method3651((byte) -53);
					} else {
						int i_22_ = -1;
						for (int i_23_ = 0; i_23_ < GraphicsLevelPreferenceField.groupCount; i_23_++) {
							if (!Class98_Sub5_Sub3.aBoolean5539) {
								int i_24_ = 31 + OpenGlArrayBufferPointer.anInt897 + i_23_ * 16;
								if ((i_24_ - 13 ^ 0xffffffff) > (i_11_ ^ 0xffffffff) && (i_11_ ^ 0xffffffff) > (i_24_ - -3 ^ 0xffffffff)) {
									i_22_ = i_23_;
									break;
								}
							} else {
								int i_25_ = 33 + OpenGlArrayBufferPointer.anInt897 + 16 * i_23_;
								if (i_11_ > i_25_ + -13 && i_11_ < i_25_ + 4) {
									i_22_ = i_23_;
									break;
								}
							}
						}
						if ((i_22_ ^ 0xffffffff) == 0) {
							break;
						}
						int i_26_ = 0;
						Class252 class252 = new Class252(RtInterfaceAttachment.actionGroups);
						for (ActionGroup class98_sub46_sub9 = (ActionGroup) class252.method3173(true); class98_sub46_sub9 != null; class98_sub46_sub9 = (ActionGroup) class252.method3174(0)) {
							if (i_22_ == i_26_) {
								JavaNetworkWriter.method3604(i_11_, (byte) 78, i_10_, (ActionQueueEntry) class98_sub46_sub9.actions.head.nextCacheable);
								Class317.method3651((byte) -53);
								break;
							}
							i_26_++;
						}
					}
				}
			}
			break;
		} while (false);
	}

	public Class204() {
		/* empty */
	}

	public final boolean method2708(int i) {
		return this == ParamDefinition.aClass204_1206 | this == Class176.aClass204_1372;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

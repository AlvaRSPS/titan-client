/* Class50 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.toolkit.font.Font;

public final class Class50 {
	public static float	aFloat419;
	public static int	anInt418	= 0;
	public static int[]	anIntArray417;

	public static void method483(int i) {
		try {
			if (i != -11543) {
				method483(-8);
			}
			anIntArray417 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dj.D(" + i + ')');
		}
	}

	public static final void method484(int i, int i_0_) {
		try {
			int i_2_ = Queue.timer + -Class98_Sub10_Sub14.anInt5613;
			if (i_2_ >= 100) {
				Class116.anInt967 = CharacterShadowsPreferenceField.anInt3712 = -1;
				Class98_Sub46_Sub20_Sub2.cameraMode = 1;
			} else {
				int i_3_ = (int) Class119_Sub4.aFloat4740;
				if ((i_3_ ^ 0xffffffff) > (Font.anInt372 >> 237711528 ^ 0xffffffff)) {
					i_3_ = Font.anInt372 >> 238117992;
				}
				if (GroupProgressMonitor.aBooleanArray3410[4] && 128 + Class98_Sub10_Sub13.anIntArray5603[4] > i_3_) {
					i_3_ = 128 + Class98_Sub10_Sub13.anIntArray5603[4];
				}
				int i_4_ = Class98_Sub10_Sub9.anInt5581 + (int) RsFloatBuffer.aFloat5794 & 0x3fff;
				OpenGlArrayBufferPointer.renderCamera(false, i_4_, (i_3_ >> 756311203) * 3 + 600 << 995508098, Class224_Sub3_Sub1.cameraYPosition, i, Minimap.cameraXPosition, i_3_, StrongReferenceMCNode.getHeight(Font.localPlane, Class87.localPlayer.boundExtentsZ,
						Class87.localPlayer.boundExtentsX, 24111) - 200);
				float f = 1.0F - (100 + -i_2_) * (-i_2_ + 100) * (100 - i_2_) / 1000000.0F;
				Mob.anInt6357 = (int) ((-RtMouseListener.anInt2494 + Mob.anInt6357) * f + RtMouseListener.anInt2494);
				Class98_Sub46_Sub10.xCamPosTile = (int) (NPC.anInt6511 + (-NPC.anInt6511 + Class98_Sub46_Sub10.xCamPosTile) * f);
				AdvancedMemoryCache.anInt601 = (int) (NewsFetcher.anInt3095 + f * (-NewsFetcher.anInt3095 + AdvancedMemoryCache.anInt601));
				SpriteLoadingScreenElement.yCamPosTile = (int) (Class98_Sub50.anInt4292 + (SpriteLoadingScreenElement.yCamPosTile + -Class98_Sub50.anInt4292) * f);
				int i_5_ = -Class96.anInt801 + Class186.cameraX;
				do {
					if ((i_5_ ^ 0xffffffff) >= -8193) {
						if (i_5_ >= -8192) {
							break;
						}
						i_5_ += 16384;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					i_5_ -= 16384;
				} while (false);
				Class186.cameraX = (int) (Class96.anInt801 + i_5_ * f);
				Class186.cameraX &= 0x3fff;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dj.B(" + i + ',' + i_0_ + ')');
		}
	}

	public static final void method485(int i) {
		try {
			int i_6_ = Class2.playerCount;
			int[] is = Class319.playerIndices;
			for (int i_8_ = 0; i_8_ < i_6_; i_8_++) {
				Player player = Class151_Sub9.players[is[i_8_]];
				if (player != null) {
					Class98_Sub10_Sub10.method1038(player, player.getSize(0), -12212);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dj.A(" + i + ')');
		}
	}

	public static final OutgoingPacket method486(int i, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, boolean bool, int i_21_) {
		try {
			OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class254.aClass171_1940, Class331.aClass117_2811);
			// class98_sub11 = Class246_Sub3_Sub4.method3023(260,
			// ha.aClass171_939, Class331.aClass117_2811);//steven
			// ((Class98_Sub11)
			// class98_sub11).aClass98_Sub22_Sub1_3865.writeShort(Class272.anInt2038
			// + toLocalX, 128);
			// ((Class98_Sub11)
			// class98_sub11).aClass98_Sub22_Sub1_3865.writeShort(aa_Sub2.anInt3562
			// + toLocalY, 128);
			class98_sub11.packet.writeShort(Class272.gameSceneBaseX + i_20_, 128);
			class98_sub11.packet.writeShort(aa_Sub2.gameSceneBaseY + i_19_, 128);
			class98_sub11.packet.writeByte(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, 72);
			return class98_sub11;
		} catch (RuntimeException runtimeException) {
			runtimeException.printStackTrace();
			return null;
		}
	}
}

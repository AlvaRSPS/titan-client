
/* Class69_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

import jaclib.memory.Buffer;

public final class Class69_Sub1 extends Class69 implements Interface2_Impl2 {
	public static int	anInt5330	= -1;
	private Class162	aClass162_5331;

	Class69_Sub1(OpenGLXToolkit var_ha_Sub3_Sub2, Class162 class162, boolean bool) {
		super(var_ha_Sub3_Sub2, 34963, bool);
		try {
			aClass162_5331 = class162;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ud.<init>(" + (var_ha_Sub3_Sub2 != null ? "{...}" : "null") + ',' + (class162 != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	@Override
	public final int method2(int i) {
		try {
			if (i != 200) {
				aClass162_5331 = null;
			}
			return super.method2(200);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ud.E(" + i + ')');
		}
	}

	@Override
	public final void method72(boolean bool) {
		try {
			super.method72(bool);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ud.L(" + bool + ')');
		}
	}

	@Override
	public final void method76(int i, int i_1_) {
		try {
			super.method76(aClass162_5331.anInt1263 * i, i_1_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ud.G(" + i + ',' + i_1_ + ')');
		}
	}

	@Override
	public final Class162 method77(int i) {
		try {
			if (i != -15448) {
				method2(120);
			}
			return aClass162_5331;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ud.P(" + i + ')');
		}
	}

	@Override
	public final Buffer method78(boolean bool, int i) {
		try {
			if (i > -79) {
				anInt5330 = 20;
			}
			return super.method694(bool, this.aHa_Sub3_Sub2_3217.aMapBuffer6125, -15793);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ud.O(" + bool + ',' + i + ')');
		}
	}

	@Override
	public final boolean method79(byte i) {
		try {
			return super.method703((byte) -68, this.aHa_Sub3_Sub2_3217.aMapBuffer6125);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ud.N(" + i + ')');
		}
	}
}

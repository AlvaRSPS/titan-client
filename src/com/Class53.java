/* Class53 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public abstract class Class53 {
	public static int				chunkX;
	public static IncomingOpcode	SEND_EXPERIENCE_UPDATE	= new IncomingOpcode(8, 6);

	int								anInt426;
	int								anInt427;

	int								anInt429;

	public Class53() {
		/* empty */
	}

	public final boolean method493(int i) {
		try {
			if (i != 9811) {
				method496((byte) -66);
			}
			return (anInt427 & 0x2 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dl.A(" + i + ')');
		}
	}

	public final boolean method495(byte i) {
		try {
			return (anInt427 & 0x8) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dl.D(" + i + ')');
		}
	}

	public final boolean method496(byte i) {
		try {
			if (i <= 42) {
				method496((byte) 70);
			}
			return (anInt427 & 0x4) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dl.C(" + i + ')');
		}
	}

	public final boolean method497(boolean bool) {
		try {
			if (bool != false) {
				return true;
			}
			return (0x1 & anInt427) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dl.E(" + bool + ')');
		}
	}
}

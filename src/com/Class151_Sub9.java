
/* Class151_Sub9 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5FileRequest;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteProgressBarLSEConfig;
import com.jagex.game.toolkit.matrix.OpenGlMatrix;

import jaggl.OpenGL;

public final class Class151_Sub9 extends Class151 {
	public static QuickChatMessageType	aClass348_5023;
	public static int					anInt5020	= 0;
	public static int					anInt5028;
	public static int[]					playerOptionCursors;
	public static Player[]				players;

	static {
		playerOptionCursors = new int[8];
		aClass348_5023 = new QuickChatMessageType(0, 2, 2, 1);
		anInt5028 = 0;
		players = new Player[2048];
	}

	public static void method2469(int i) {
		try {
			playerOptionCursors = null;
			players = null;
			if (i == 2147483647) {
				aClass348_5023 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ws.B(" + i + ')');
		}
	}

	public static final void method2472(boolean bool, String string, int i, boolean bool_6_) {
		try {
			Class98_Sub43_Sub1.method1493(57);
			Class98_Sub36.method1458(-24580);
			Class98_Sub43_Sub4.method1510(28837);
			client.initializeToolkit((byte) 69, string, i, bool_6_);
			Class378.method4001((byte) -34);
			Class98_Sub10_Sub19.loadFonts((byte) -85, client.graphicsToolkit);
			SpriteProgressBarLSEConfig.loadFonts(client.graphicsToolkit, (byte) -48);
			NPCDefinition.loadSprites(client.graphicsToolkit, (byte) -124, client.spriteJs5);
			Class48_Sub1_Sub2.method466(bool);
			Class303.setImageTagSprites(OrthoZoomPreferenceField.nameIcons, 0);
			Class98_Sub43.setAllDirty(2);
			Js5FileRequest.method1593((byte) 115);
			if ((client.clientState ^ 0xffffffff) != -4) {
				if ((client.clientState ^ 0xffffffff) != -8) {
					if (client.clientState != 10) {
						if (client.clientState == 1 || (client.clientState ^ 0xffffffff) == -3) {
							SunDefinition.method3235((byte) -121);
						}
					} else {
						HashTableIterator.setClientState(11, false);
					}
				} else {
					HashTableIterator.setClientState(8, false);
				}
			} else {
				HashTableIterator.setClientState(4, false);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ws.H(" + bool + ',' + (string != null ? "{...}" : "null") + ',' + i + ',' + bool_6_ + ')');
		}
	}

	private boolean			aBoolean5018;
	private boolean			aBoolean5021;
	private boolean			aBoolean5024	= false;
	private boolean			aBoolean5026;
	private Class202		aClass202_5017;
	private Class202		aClass202_5022;

	private Class202		aClass202_5025;

	private Class202		aClass202_5029;

	private Class42_Sub1	aClass42_Sub1_5027;

	Class151_Sub9(OpenGlToolkit var_ha_Sub1) {
		super(var_ha_Sub1);
		try {
			if (this.aHa_Sub1_1215.aBoolean4431) {
				aClass202_5022 = Class347.method3835(0, this.aHa_Sub1_1215, 34336,
						"!!ARBvp1.0\nATTRIB  iPos         = vertex.position;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oPos         = result.position;\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   fogParams    = program.local[0];\nPARAM   waterPlane   = program.local[1];\nPARAM   tMatrix[4]   = { state.matrix.texture[0] };\nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nTEMP    viewPos, fogFactor;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nSUB   fogFactor.x, -viewPos.z, fogParams.x;\nMUL   fogFactor.x, fogFactor.x, 0.001953125;\nMAD   fogFactor.y, iTexCoord.z, fogParams.z, fogParams.w;\nSUB   fogFactor.z, -viewPos.z, fogParams.y;\nMUL   fogFactor.z, fogFactor.z, 0.00390625;\nMUL   fogFactor.x, fogFactor.x, fogFactor.y;\nMIN   fogFactor, fogFactor, 1;\nMAX   fogFactor, fogFactor, 0;\nMUL   fogFactor.z, fogFactor.z, iTexCoord.z;\nMAD   viewPos.xyz, waterPlane.xyzw, fogFactor.zzzz, viewPos.xyzw;\nMAX   oTexCoord1.xyz, fogFactor.xxxx, fogFactor.yyyy;\nMOV   oTexCoord1.w, 1;\nMOV   oColour, iColour;\nDP4   oPos.x, pMatrix[0], viewPos;\nDP4   oPos.y, pMatrix[1], viewPos;\nDP4   oPos.z, pMatrix[2], viewPos;\nDP4   oPos.w, pMatrix[3], viewPos;\nMOV   oFogCoord.x, viewPos.z;\nDP3   oTexCoord0.x, tMatrix[0], iTexCoord;\nDP3   oTexCoord0.y, tMatrix[1], iTexCoord;\nMOV   oTexCoord0.zw, iTexCoord;\nEND\n");
				aClass202_5029 = Class347.method3835(0, this.aHa_Sub1_1215, 34336,
						"!!ARBvp1.0\nATTRIB  iPos         = vertex.position;\nATTRIB  iNormal      = vertex.normal;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oPos         = result.position;\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   fogParams    = program.local[0];\nPARAM   waterPlane   = program.local[1];\nPARAM   tMatrix[4]   = { state.matrix.texture[0] };\nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nTEMP    viewPos, viewNormal, fogFactor, colour, ndotl;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nSUB   fogFactor.x, -viewPos.z, fogParams.x;\nMUL   fogFactor.x, fogFactor.x, 0.001953125;\nMAD   fogFactor.y, iTexCoord.z, fogParams.z, fogParams.w;\nSUB   fogFactor.z, -viewPos.z, fogParams.y;\nMUL   fogFactor.z, fogFactor.z, 0.00390625;\nMUL   fogFactor.x, fogFactor.x, fogFactor.y;\nMIN   fogFactor, fogFactor, 1;\nMAX   fogFactor, fogFactor, 0;\nMUL   fogFactor.z, fogFactor.z, iTexCoord.z;\nMAD   viewPos.xyz, waterPlane.xyzw, fogFactor.zzzz, viewPos.xyzw;\nMAX   oTexCoord1.xyz, fogFactor.xxxx, fogFactor.yyyy;\nMOV   oTexCoord1.w, 1;\nDP3   viewNormal.x, mvMatrix[0], iNormal;\nDP3   viewNormal.y, mvMatrix[1], iNormal;\nDP3   viewNormal.z, mvMatrix[2], iNormal;\nDP3   ndotl.x, viewNormal, state.light[0].position;\nDP3   ndotl.y, viewNormal, state.light[1].position;\nMAX   ndotl, ndotl, 0;\nMOV   colour, state.lightmodel.ambient;\nMAD   colour, state.light[0].diffuse, ndotl.xxxx, colour;\nMAD   colour, state.light[1].diffuse, ndotl.yyyy, colour;\nMUL   oColour, iColour, colour;\nDP4   oPos.x, pMatrix[0], viewPos;\nDP4   oPos.y, pMatrix[1], viewPos;\nDP4   oPos.z, pMatrix[2], viewPos;\nDP4   oPos.w, pMatrix[3], viewPos;\nMOV   oFogCoord.x, viewPos.z;\nDP3   oTexCoord0.x, tMatrix[0], iTexCoord;\nDP3   oTexCoord0.y, tMatrix[1], iTexCoord;\nMOV   oTexCoord0.zw, iTexCoord;\nEND\n");
				aClass202_5025 = Class347.method3835(0, this.aHa_Sub1_1215, 34336,
						"!!ARBvp1.0\nATTRIB  iPos         = vertex.position;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oPos         = result.position;\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   fogParams    = program.local[0];\nPARAM   waterPlane   = program.local[1];\nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nPARAM   texMatrix[4] = { state.matrix.texture[0] };\nTEMP    viewPos, fogFactor, depth;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nSUB   fogFactor.x, -viewPos.z, fogParams.x;\nMUL   fogFactor.x, fogFactor.x, 0.001953125;\nDP4   depth, waterPlane, viewPos;\nMAD   fogFactor.y, -depth, fogParams.z, fogParams.w;\nSUB   fogFactor.z, -viewPos.z, fogParams.y;\nMUL   fogFactor.z, fogFactor.z, 0.00390625;\nMIN   fogFactor, fogFactor, 1;\nMAX   fogFactor, fogFactor, 0;\nMUL   fogFactor.z, fogFactor.z, -depth;\nMAD   viewPos.xyz, waterPlane.xyzw, fogFactor.zzzz, viewPos.xyzw;\nMAX   oTexCoord1.xyz, fogFactor.xxxx, fogFactor.yyyy;\nMOV   oTexCoord1.w, 1;\nMOV   oColour, iColour;\nDP4   oPos.x, pMatrix[0], viewPos;\nDP4   oPos.y, pMatrix[1], viewPos;\nDP4   oPos.z, pMatrix[2], viewPos;\nDP4   oPos.w, pMatrix[3], viewPos;\nMOV   oFogCoord.x, viewPos.z;\nDP4   oTexCoord0.x, texMatrix[0], iTexCoord;\nDP4   oTexCoord0.y, texMatrix[1], iTexCoord;\nDP4   oTexCoord0.z, texMatrix[2], iTexCoord;\nMOV   oTexCoord0.w, 1;\nEND\n");
				aClass202_5017 = Class347.method3835(0, this.aHa_Sub1_1215, 34336,
						"!!ARBvp1.0\nATTRIB  iPos         = vertex.position;\nATTRIB  iNormal      = vertex.normal;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oPos         = result.position;\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   fogParams    = program.local[0];\nPARAM   waterPlane   = program.local[1];\nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nPARAM   texMatrix[4] = { state.matrix.texture[0] };\nTEMP    viewPos, viewNormal, fogFactor, depth, colour, ndotl;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nSUB   fogFactor.x, -viewPos.z, fogParams.x;\nMUL   fogFactor.x, fogFactor.x, 0.001953125;\nDP4   depth, waterPlane, viewPos;\nMAD   fogFactor.y, -depth, fogParams.z, fogParams.w;\nSUB   fogFactor.z, -viewPos.z, fogParams.y;\nMUL   fogFactor.z, fogFactor.z, 0.00390625;\nMIN   fogFactor, fogFactor, 1;\nMAX   fogFactor, fogFactor, 0;\nMUL   fogFactor.z, fogFactor.z, -depth;\nMAD   viewPos.xyz, waterPlane.xyzw, fogFactor.zzzz, viewPos.xyzw;\nMAX   oTexCoord1.xyz, fogFactor.xxxx, fogFactor.yyyy;\nMOV   oTexCoord1.w, 1;\nDP3   viewNormal.x, mvMatrix[0], iNormal;\nDP3   viewNormal.y, mvMatrix[1], iNormal;\nDP3   viewNormal.z, mvMatrix[2], iNormal;\nDP3   ndotl.x, viewNormal, state.light[0].position;\nDP3   ndotl.y, viewNormal, state.light[1].position;\nMAX   ndotl, ndotl, 0;\nMOV   colour, state.lightmodel.ambient;\nMAD   colour, state.light[0].diffuse, ndotl.xxxx, colour;\nMAD   colour, state.light[1].diffuse, ndotl.yyyy, colour;\nMUL   oColour, iColour, colour;\nDP4   oPos.x, pMatrix[0], viewPos;\nDP4   oPos.y, pMatrix[1], viewPos;\nDP4   oPos.z, pMatrix[2], viewPos;\nDP4   oPos.w, pMatrix[3], viewPos;\nMOV   oFogCoord.x, viewPos.z;\nDP4   oTexCoord0.x, texMatrix[0], iTexCoord;\nDP4   oTexCoord0.y, texMatrix[1], iTexCoord;\nDP4   oTexCoord0.z, texMatrix[2], iTexCoord;\nMOV   oTexCoord0.w, 1;\nEND\n");
				if (!(aClass202_5017 != null & aClass202_5025 != null & aClass202_5029 != null & aClass202_5022 != null)) {
					aBoolean5018 = false;
				} else {
					aClass42_Sub1_5027 = new Class42_Sub1(var_ha_Sub1, 3553, 6406, 2, 1, false, new byte[] { 0, -1 }, 6406, false);
					aClass42_Sub1_5027.method383(false, 10242, false);
					aBoolean5018 = true;
				}
			} else {
				aBoolean5018 = false;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ws.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final boolean method2439(int i) {
		try {
			if (i != 31565) {
				aBoolean5021 = true;
			}
			return aBoolean5018;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ws.A(" + i + ')');
		}
	}

	@Override
	public final void method2440(boolean bool, boolean bool_5_) {
		try {
			aBoolean5021 = bool_5_;
			this.aHa_Sub1_1215.method1845(1, 847872872);
			this.aHa_Sub1_1215.setActiveTexture(1, aClass42_Sub1_5027);
			if (bool != false) {
				aBoolean5018 = false;
			}
			this.aHa_Sub1_1215.method1899(7681, 8960, 34165);
			this.aHa_Sub1_1215.method1840(0, 768, 108, 34166);
			this.aHa_Sub1_1215.method1840(2, 770, 113, 5890);
			this.aHa_Sub1_1215.method1886(770, 0, 34200, 34168);
			this.aHa_Sub1_1215.method1845(0, 847872872);
			method2471((byte) 34);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ws.D(" + bool + ',' + bool_5_ + ')');
		}
	}

	@Override
	public final void method2441(int i, int i_0_, int i_1_) {
		do {
			try {
				if (i_1_ <= -2) {
					break;
				}
				method2445((byte) -6);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ws.G(" + i + ',' + i_0_ + ',' + i_1_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2442(Class42 class42, boolean bool, int i) {
		do {
			try {
				if (bool == false) {
					if (class42 != null) {
						if (aBoolean5024) {
							this.aHa_Sub1_1215.method1840(0, 768, 74, 5890);
							this.aHa_Sub1_1215.method1886(770, 0, 34200, 5890);
							aBoolean5024 = false;
						}
						this.aHa_Sub1_1215.setActiveTexture(1, class42);
						this.aHa_Sub1_1215.method1896(260, i);
					} else {
						if (aBoolean5024) {
							break;
						}
						this.aHa_Sub1_1215.setActiveTexture(1, this.aHa_Sub1_1215.aClass42_Sub1_4358);
						this.aHa_Sub1_1215.method1896(260, 1);
						this.aHa_Sub1_1215.method1840(0, 768, -50, 34168);
						this.aHa_Sub1_1215.method1886(770, 0, 34200, 34168);
						aBoolean5024 = true;
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ws.F(" + (class42 != null ? "{...}" : "null") + ',' + bool + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2443(boolean bool, int i) {
		do {
			try {
				if (i == 255) {
					break;
				}
				method2442(null, false, -47);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ws.C(" + bool + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2445(byte i) {
		do {
			try {
				if (aBoolean5026) {
					OpenGL.glBindProgramARB(34336, 0);
					OpenGL.glDisable(34820);
					OpenGL.glDisable(34336);
					aBoolean5026 = false;
				}
				this.aHa_Sub1_1215.method1845(1, 847872872);
				this.aHa_Sub1_1215.setActiveTexture(1, null);
				this.aHa_Sub1_1215.method1899(8448, 8960, 8448);
				this.aHa_Sub1_1215.method1840(0, 768, -107, 5890);
				this.aHa_Sub1_1215.method1840(2, 770, 104, 34166);
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 5890);
				if (i <= 25) {
					anInt5020 = -23;
				}
				this.aHa_Sub1_1215.method1845(0, 847872872);
				if (!aBoolean5024) {
					break;
				}
				this.aHa_Sub1_1215.method1840(0, 768, 117, 5890);
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 5890);
				aBoolean5024 = false;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ws.E(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void method2470(int i) {
		try {
			if (i != -16661) {
				anInt5020 = 117;
			}
			if (aBoolean5026) {
				int i_2_ = this.aHa_Sub1_1215.getFarPlane();
				int i_3_ = this.aHa_Sub1_1215.getNearPlane();
				float f = i_2_ - 0.125F * (-i_3_ + i_2_);
				float f_4_ = -((i_2_ + -i_3_) * 0.25F) + i_2_;
				OpenGL.glProgramLocalParameter4fARB(34336, 0, f_4_, f, 1.0F / this.aHa_Sub1_1215.anInt4454, this.aHa_Sub1_1215.anInt4453 / 255.0F);
				this.aHa_Sub1_1215.method1845(1, 847872872);
				this.aHa_Sub1_1215.method1882(this.aHa_Sub1_1215.anInt4423, -104);
				this.aHa_Sub1_1215.method1845(0, i ^ ~0x3289c27c);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ws.I(" + i + ')');
		}
	}

	public final void method2471(byte i) {
		try {
			OpenGlMatrix class111_sub1 = this.aHa_Sub1_1215.aClass111_Sub1_4353;
			do {
				if (!aBoolean5021) {
					OpenGL.glBindProgramARB(34336, this.aHa_Sub1_1215.anInt4362 == 2147483647 ? aClass202_5022.anInt1549 : aClass202_5025.anInt1549);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				OpenGL.glBindProgramARB(34336, this.aHa_Sub1_1215.anInt4362 != 2147483647 ? aClass202_5017.anInt1549 : aClass202_5029.anInt1549);
			} while (false);
			class111_sub1.method2109(-29834, 0.0F, this.aHa_Sub1_1215.anInt4362, 0.0F, -1.0F, LoginOpcode.aFloatArray1671);
			if (i == 34) {
				OpenGL.glProgramLocalParameter4fARB(34336, 1, LoginOpcode.aFloatArray1671[0], LoginOpcode.aFloatArray1671[1], LoginOpcode.aFloatArray1671[2], LoginOpcode.aFloatArray1671[3]);
				OpenGL.glEnable(34336);
				aBoolean5026 = true;
				method2470(-16661);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ws.J(" + i + ')');
		}
	}
}

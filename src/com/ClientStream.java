
/* Class361 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.jagex.core.timetools.general.TimeTools;

public final class ClientStream implements Runnable {
	public static int anInt3089 = 0;

	public static final int getProgress(int i) {
		return Class98_Sub46_Sub15.getProgress(false, (byte) 90);
	}

	public static final int nextStartupStage(int i) {
		int stage = client.LOAD_JS5_INDICES.getId((byte) -10);
		if ((stage ^ 0xffffffff) > (client.startupStages.length + -1 ^ 0xffffffff)) {
			client.LOAD_JS5_INDICES = client.startupStages[1 + stage];
		}
		return 100;
	}

	private InputStream		inputStream;
	private OutputStream	outputStream;
	private SignLinkRequest	request;
	private boolean			shutDown;
	private SignLink		sign_link;
	private Socket			socket_;
	private byte[]			writeBuffer;
	private int				writeBufferInPtr	= 0;

	private int				writeBufferOutPtr;

	private int				writeBufferSize;

	private boolean			writeErrorOccurred;

	ClientStream(Socket socket, SignLink signLink, int size) throws IOException {
		writeErrorOccurred = false;
		shutDown = false;
		writeBufferOutPtr = 0;
		socket_ = socket;
		sign_link = signLink;
		socket_.setSoTimeout(30000);
		socket_.setTcpNoDelay(true);
		inputStream = socket_.getInputStream();
		outputStream = socket_.getOutputStream();
		writeBufferSize = size;
	}

	public final int available(int i) throws IOException {
		if (shutDown) {
			return 0;
		}
		return inputStream.available();
	}

	@Override
	protected final void finalize() {
		shutDown(-29789);
	}

	public final void handleError(boolean bool) throws IOException {
		if (bool != true) {
			writeBufferSize = 96;
		}
		if (!shutDown) {
			if (writeErrorOccurred) {
				writeErrorOccurred = false;
				throw new IOException();
			}
		}
	}

	public final int read(int i) throws IOException {
		if (shutDown) {
			return 0;
		}
		return inputStream.read();
	}

	public final void read(int length, boolean bool, int offset, byte[] buffer) throws IOException {
		if (bool != true) {
			finalize();
		}
		if (!shutDown) {
			int readLength;
			for (/**/; offset > 0; offset -= readLength) {
				readLength = inputStream.read(buffer, length, offset);
				if ((readLength ^ 0xffffffff) >= -1) {
					throw new EOFException();
				}
				length += readLength;
			}
		}
	}

	@Override
	public final void run() {
		try {
			for (;;) {
				int writeLength;
				int writeOffset;
				synchronized (this) {
					if ((writeBufferInPtr ^ 0xffffffff) == (writeBufferOutPtr ^ 0xffffffff)) {
						if (shutDown) {
							break;
						}
						try {
							this.wait();
						} catch (InterruptedException interruptedexception) {
							/* empty */
						}
					}
					writeLength = writeBufferOutPtr;
					if ((writeBufferInPtr ^ 0xffffffff) > (writeBufferOutPtr ^ 0xffffffff)) {
						writeOffset = -writeBufferOutPtr + writeBufferSize;
					} else {
						writeOffset = writeBufferInPtr - writeBufferOutPtr;
					}
				}
				if ((writeOffset ^ 0xffffffff) < -1) {
					try {
						outputStream.write(writeBuffer, writeLength, writeOffset);
					} catch (IOException ioexception) {
						writeErrorOccurred = true;
					}
					writeBufferOutPtr = (writeOffset + writeBufferOutPtr) % writeBufferSize;
					try {
						if ((writeBufferInPtr ^ 0xffffffff) == (writeBufferOutPtr ^ 0xffffffff)) {
							outputStream.flush();
						}
					} catch (IOException ioexception) {
						writeErrorOccurred = true;
					}
				}
			}
			try {
				if (inputStream != null) {
					inputStream.close();
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (socket_ != null) {
					socket_.close();
				}
			} catch (IOException ioexception) {
				/* empty */
			}
			writeBuffer = null;
		} catch (Exception exception) {
			Class305_Sub1.reportError(exception, -128, null);
		}
	}

	public final void setDummyMode(int i) {
		if (!shutDown) {
			inputStream = new DummyInputStream();
			outputStream = new DummyOutputStream();
		}
	}

	public final void shutDown(int i) {
		if (!shutDown) {
			synchronized (this) {
				shutDown = true;
				if (i != -29789) {
					setDummyMode(-126);
				}
				notifyAll();
			}
			if (request != null) {
				while ((request.status ^ 0xffffffff) == -1) {
					TimeTools.sleep(0, 1L);
				}
				if ((request.status ^ 0xffffffff) == -2) {
					try {
						((Thread) request.result).join();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
			}
			request = null;
		}
	}

	public final void write(byte i, int offset, int i_10_, byte[] buffer) throws IOException {
		if (!shutDown) {
			if (writeErrorOccurred) {
				writeErrorOccurred = false;
				throw new IOException();
			}
			if (writeBuffer == null) {
				writeBuffer = new byte[writeBufferSize];
			}
			synchronized (this) {
				for (int wPos = 0; (i_10_ ^ 0xffffffff) < (wPos ^ 0xffffffff); wPos++) {
					writeBuffer[writeBufferInPtr] = buffer[wPos + offset];
					writeBufferInPtr = (1 + writeBufferInPtr) % writeBufferSize;
					if ((writeBufferInPtr ^ 0xffffffff) == ((writeBufferOutPtr - (-writeBufferSize - -100)) % writeBufferSize ^ 0xffffffff)) {
						throw new IOException();
					}
				}
				if (i != 77) {
					writeBufferOutPtr = 85;
				}
				if (request == null) {
					request = sign_link.startThread(3, this, 1);
				}
				notifyAll();
			}
		}
	}
}

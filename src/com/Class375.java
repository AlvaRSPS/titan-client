
/* Class375 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.File;

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;

public final class Class375 {
	public static boolean	aBoolean3170	= false;
	public static int		anInt3168		= 328;
	public static int		planeTele;
	public static Js5		objectsJs5;

	public static final boolean method3986(int i, byte i_0_) {
		try {
			if (i_0_ != -108) {
				method3988(null, (byte) -13, -123);
			}
			return !((i ^ 0xffffffff) != -3 && (i ^ 0xffffffff) != -4);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wp.B(" + i + ',' + i_0_ + ')');
		}
	}

	public static void method3987(byte i) {
		try {
			if (i != -73) {
				anInt3168 = -23;
			}
			objectsJs5 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wp.C(" + i + ')');
		}
	}

	public static final byte[] method3988(File file, byte i, int i_1_) {
		try {
			if (i != 78) {
				aBoolean3170 = true;
			}
			try {
				byte[] is = new byte[i_1_];
				Class261.method3211(is, 124, i_1_, file);
				return is;
			} catch (java.io.IOException ioexception) {
				return null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wp.A(" + (file != null ? "{...}" : "null") + ',' + i + ',' + i_1_ + ')');
		}
	}
}

/* Class98_Sub6 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;

public final class Class98_Sub6 extends Node {
	public static IncomingOpcode		aClass58_3844	= new IncomingOpcode(121, -1);
	public static AdvancedMemoryCache	aClass79_3847	= new AdvancedMemoryCache(8);

	public static final BaseModel fromFile(int i, int i_15_, Js5 class207, int i_16_) {
		try {
			// System.out.println(i_16_); //model id
			byte[] is = class207.getFile(i, i_16_, false);
			if (i_15_ != -9252) {
				aClass79_3847 = null;
			}
			if (is == null) {
				return null;
			}
			return new BaseModel(is);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cd.F(" + i + ',' + i_15_ + ',' + (class207 != null ? "{...}" : "null") + ',' + i_16_ + ')');
		}
	}

	public static void method975(int i) {
		do {
			try {
				aClass58_3844 = null;
				aClass79_3847 = null;
				if (i == 1) {
					break;
				}
				method978(true, true, 19, true, 86);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cd.B(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final int method978(boolean bool, boolean bool_5_, int i, boolean bool_6_, int i_7_) {
		try {
			ClientInventory class98_sub3 = WaterDetailPreferenceField.get(i, bool, 6);
			if (class98_sub3 == null) {
				return 0;
			}
			int i_8_ = 0;
			for (int i_9_ = 0; (i_9_ ^ 0xffffffff) > (class98_sub3.anIntArray3824.length ^ 0xffffffff); i_9_++) {
				if ((class98_sub3.anIntArray3824[i_9_] ^ 0xffffffff) <= -1 && (Class98_Sub46_Sub19.itemDefinitionList.count ^ 0xffffffff) < (class98_sub3.anIntArray3824[i_9_] ^ 0xffffffff)) {
					ItemDefinition class297 = Class98_Sub46_Sub19.itemDefinitionList.get(class98_sub3.anIntArray3824[i_9_], (byte) -121);
					int i_10_ = class297.method3494(i_7_, (byte) -128, Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, i_7_).defaultInteger);
					if (bool_5_) {
						i_8_ += class98_sub3.anIntArray3823[i_9_] * i_10_;
					} else {
						i_8_ += i_10_;
					}
				}
			}
			if (bool_6_ != true) {
				method975(-10);
			}
			return i_8_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cd.G(" + bool + ',' + bool_5_ + ',' + i + ',' + bool_6_ + ',' + i_7_ + ')');
		}
	}

	int			anInt3838;
	int			anInt3839;
	private int	anInt3840;
	private int	anInt3841;
	private int	anInt3842;
	int			anInt3843;

	int			anInt3845;

	private int	anInt3846;

	private int	anInt3848;

	Class98_Sub6(int i, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_, int i_23_, int i_24_) {
		try {
			anInt3842 = i;
			anInt3840 = i_17_;
			anInt3845 = i_24_;
			anInt3846 = i_18_;
			anInt3841 = i_20_;
			anInt3839 = i_21_;
			anInt3848 = i_19_;
			anInt3838 = i_23_;
			anInt3843 = i_22_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cd.<init>(" + i + ',' + i_17_ + ',' + i_18_ + ',' + i_19_ + ',' + i_20_ + ',' + i_21_ + ',' + i_22_ + ',' + i_23_ + ',' + i_24_ + ')');
		}
	}

	public final boolean method976(int i, int i_0_, int i_1_, int i_2_) {
		try {
			if (i_0_ < 104) {
				anInt3848 = 122;
			}
			return (anInt3842 ^ 0xffffffff) == (i_2_ ^ 0xffffffff) && i_1_ >= anInt3840 && i_1_ <= anInt3848 && i >= anInt3846 && i <= anInt3841;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cd.E(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	public final boolean method977(int i, byte i_3_, int i_4_) {
		try {
			if (i_3_ <= 32) {
				anInt3848 = 89;
			}
			return (anInt3839 ^ 0xffffffff) >= (i ^ 0xffffffff) && i <= anInt3838 && anInt3843 <= i_4_ && anInt3845 >= i_4_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cd.D(" + i + ',' + i_3_ + ',' + i_4_ + ')');
		}
	}

	public final void method979(int i, int i_11_, int i_12_, int[] is) {
		try {
			is[1] = i_12_ - anInt3839 - -anInt3840;
			is[i_11_] = anInt3842;
			is[2] = i - -anInt3846 + -anInt3843;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cd.H(" + i + ',' + i_11_ + ',' + i_12_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	public final boolean method980(int i, int i_13_, int i_14_) {
		try {
			if (i > -94) {
				method980(104, -50, -22);
			}
			return !((i_13_ ^ 0xffffffff) > (anInt3840 ^ 0xffffffff) || (i_13_ ^ 0xffffffff) < (anInt3848 ^ 0xffffffff) || (anInt3846 ^ 0xffffffff) < (i_14_ ^ 0xffffffff) || anInt3841 < i_14_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cd.C(" + i + ',' + i_13_ + ',' + i_14_ + ')');
		}
	}

	public final void method982(int i, int i_25_, int i_26_, int[] is) {
		do {
			try {
				is[0] = 0;
				is[2] = anInt3843 - anInt3846 + i;
				is[1] = -anInt3840 - (-anInt3839 - i_25_);
				if (i_26_ > 43) {
					break;
				}
				anInt3842 = 27;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cd.A(" + i + ',' + i_25_ + ',' + i_26_ + ',' + (is != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}
}

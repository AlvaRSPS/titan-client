
/* Class261 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.cache.MemoryCacheNodeFactory;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.clanchat.ClanChatMember;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.BConfigDefinitionParser;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.input.RtKeyEvent;

public final class Class261 {
	public static int[]	anIntArray3879;

	public static int	fadeInCounter	= 0;

	public static final void executeInput(boolean bool, boolean bool_0_) {
		if (ParticleManager.inputBuffer.length() != 0) {
			Cacheable.write("--> " + ParticleManager.inputBuffer, -117);
			Class295.handleClientCommand(ParticleManager.inputBuffer, bool, bool_0_, (byte) 117);
			ParticleManager.inputBuffer = "";
			Class98_Sub31_Sub2.anInt5822 = 0;
			BConfigDefinitionParser.anInt1524 = 0;
		}
	}

	public static final void method1044(byte i, String[] strings) {
		try {
			if ((strings.length ^ 0xffffffff) >= -2) {
				ParticleManager.inputBuffer += strings[0];
				BConfigDefinitionParser.anInt1524 += strings[0].length();
			} else {
				for (int i_9_ = 0; i_9_ < strings.length; i_9_++) {
					if (!strings[i_9_].startsWith("pause")) {
						ParticleManager.inputBuffer = strings[i_9_];
						executeInput(false, false);
					} else {
						int i_10_ = 5;
						try {
							i_10_ = Integer.parseInt(strings[i_9_].substring(6));
						} catch (Exception exception) {
							/* empty */
						}
						Cacheable.write("Pausing for " + i_10_ + " seconds...", 101);
						Class169.scriptPointer = i_9_ - -1;
						ClanChatMember.currentScript = strings;
						BConfigDefinitionParser.scriptResumeTimeStamp = TimeTools.getCurrentTime(-47) - -(long) (i_10_ * 1000);
						return;
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hb.F(" + i + ',' + (strings != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method3211(byte[] is, int i, int i_4_, File file) throws IOException {
		try {
			if (i < 122) {
				process(72);
			}
			DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
			try {
				datainputstream.readFully(is, 0, i_4_);
			} catch (java.io.EOFException eofexception) {
				/* empty */
			}
			datainputstream.close();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qga.C(" + (is != null ? "{...}" : "null") + ',' + i + ',' + i_4_ + ',' + (file != null ? "{...}" : "null") + ')');
		}
	}

	public static final void process(int i) {
		if (fadeInCounter < 102) {
			fadeInCounter += 6;
		}
		if ((Class169.scriptPointer ^ 0xffffffff) != 0 && (TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL) < (BConfigDefinitionParser.scriptResumeTimeStamp ^ 0xffffffffffffffffL)) {
			for (int slptr = Class169.scriptPointer; (slptr ^ 0xffffffff) > (ClanChatMember.currentScript.length ^ 0xffffffff); slptr++) {
				if (!ClanChatMember.currentScript[slptr].startsWith("pause")) {
					ParticleManager.inputBuffer = ClanChatMember.currentScript[slptr];
					executeInput(false, false);
				} else {
					int pause = 5;
					try {
						pause = Integer.parseInt(ClanChatMember.currentScript[slptr].substring(6));
					} catch (Exception exception) {
						/* empty */
					}
					Cacheable.write("Pausing for " + pause + " seconds...", -109);
					Class169.scriptPointer = 1 + slptr;
					BConfigDefinitionParser.scriptResumeTimeStamp = TimeTools.getCurrentTime(i + -151) + pause * 1000;
					return;
				}
			}
			Class169.scriptPointer = -1;
		}
		if (Class319.mouseScrollDelta != 0) {
			NativeProgressMonitor.anInt3395 -= 5 * Class319.mouseScrollDelta;
			if (NativeProgressMonitor.anInt3395 >= Class98_Sub28.anInt4080) {
				NativeProgressMonitor.anInt3395 = -1 + Class98_Sub28.anInt4080;
			}
			if ((NativeProgressMonitor.anInt3395 ^ 0xffffffff) > -1) {
				NativeProgressMonitor.anInt3395 = 0;
			}
			Class319.mouseScrollDelta = 0;
		}
		int i_7_ = 0;
		if (i == 104) {
			for (/**/; i_7_ < client.keyPressCount; i_7_++) {
				RtKeyEvent interface7 = client.keyPressEvents[i_7_];
				int i_8_ = interface7.getKeyCode(true);
				char c = interface7.getKeyChar(13313);
				int i_9_ = interface7.getModifiers((byte) 82);
				if ((i_8_ ^ 0xffffffff) == -85) {
					executeInput(false, false);
				}
				if ((i_8_ ^ 0xffffffff) == -81) {
					executeInput(true, false);
				} else if ((i_8_ ^ 0xffffffff) != -67 || (0x4 & i_9_) == 0) {
					if (i_8_ != 67 || (i_9_ & 0x4 ^ 0xffffffff) == -1) {
						if (i_8_ == 85 && BConfigDefinitionParser.anInt1524 > 0) {
							ParticleManager.inputBuffer = ParticleManager.inputBuffer.substring(0, -1 + BConfigDefinitionParser.anInt1524) + ParticleManager.inputBuffer.substring(BConfigDefinitionParser.anInt1524);
							BConfigDefinitionParser.anInt1524--;
						} else if (i_8_ == 101 && BConfigDefinitionParser.anInt1524 < ParticleManager.inputBuffer.length()) {
							ParticleManager.inputBuffer = ParticleManager.inputBuffer.substring(0, BConfigDefinitionParser.anInt1524) + ParticleManager.inputBuffer.substring(1 + BConfigDefinitionParser.anInt1524);
						} else if (i_8_ == 96 && BConfigDefinitionParser.anInt1524 > 0) {
							BConfigDefinitionParser.anInt1524--;
						} else if ((i_8_ ^ 0xffffffff) != -98 || BConfigDefinitionParser.anInt1524 >= ParticleManager.inputBuffer.length()) {
							if (i_8_ == 102) {
								BConfigDefinitionParser.anInt1524 = 0;
							} else if (i_8_ == 103) {
								BConfigDefinitionParser.anInt1524 = ParticleManager.inputBuffer.length();
							} else if (i_8_ != 104 || Class98_Sub31_Sub2.anInt5822 >= Class98_Sub46_Sub20.aStringArray6073.length) {
								if ((i_8_ ^ 0xffffffff) == -106 && (Class98_Sub31_Sub2.anInt5822 ^ 0xffffffff) < -1) {
									Class98_Sub31_Sub2.anInt5822--;
									MemoryCacheNodeFactory.method2724((byte) 123);
									BConfigDefinitionParser.anInt1524 = ParticleManager.inputBuffer.length();
								} else if (Class114.method2147(c, i ^ 0x4) || (c ^ 0xffffffff) == -93 || c == 47 || c == 46 || (c ^ 0xffffffff) == -59 || c == 44 || c == 32 || (c ^ 0xffffffff) == -96 || (c ^ 0xffffffff) == -46 || c == 43 || (c ^ 0xffffffff) == -92 || c == 93) {
									ParticleManager.inputBuffer = ParticleManager.inputBuffer.substring(0, BConfigDefinitionParser.anInt1524) + client.keyPressEvents[i_7_].getKeyChar(13313) + ParticleManager.inputBuffer.substring(BConfigDefinitionParser.anInt1524);
									BConfigDefinitionParser.anInt1524++;
								}
							} else {
								Class98_Sub31_Sub2.anInt5822++;
								MemoryCacheNodeFactory.method2724((byte) 127);
								BConfigDefinitionParser.anInt1524 = ParticleManager.inputBuffer.length();
							}
						} else {
							BConfigDefinitionParser.anInt1524++;
						}
					} else if (client.systemClipboard != null) {
						Transferable transferable = client.systemClipboard.getContents(null);
						if (transferable != null) {
							try {
								String string = (String) transferable.getTransferData(DataFlavor.stringFlavor);
								if (string != null) {
									String[] strings = Class112.splitString(string, '\n', false);
									method1044((byte) 120, strings);
								}
							} catch (Exception exception) {
								/* empty */
							}
						}
					}
				} else if (client.systemClipboard != null) {
					String string = "";
					for (int i_10_ = -1 + Class98_Sub46_Sub20.aStringArray6073.length; i_10_ >= 0; i_10_--) {
						if (Class98_Sub46_Sub20.aStringArray6073[i_10_] != null && (Class98_Sub46_Sub20.aStringArray6073[i_10_].length() ^ 0xffffffff) < -1) {
							string += Class98_Sub46_Sub20.aStringArray6073[i_10_] + '\n';
						}
					}
					client.systemClipboard.setContents(new StringSelection(string), null);
				}
			}
			QuickChatCategory.keyReleaseCount = 0;
			client.keyPressCount = 0;
			if (GameShell.frameWidth >> 1 != Class98_Sub10_Sub3.anIntArray10356.length)
				Class98_Sub10_Sub3.anIntArray10356 = new int[GameShell.frameWidth >> 1];
			for (int snowFlake = 0; snowFlake < 100; snowFlake++) {
				Class98_Sub10_Sub3.anIntArray10273[snowFlake] += anIntArray3879[snowFlake];
				if (Class98_Sub10_Sub3.anIntArray10273[snowFlake] >> 4 >= 350) {
					int i_10_ = Class98_Sub10_Sub3.anIntArray1283[snowFlake] >> 4;
					int i_11_ = Class98_Sub10_Sub3.anIntArray1283[snowFlake] & 0xf;
					i_10_ += Class284_Sub2_Sub2.COSINE[64 * (350 + (i_10_ & 0x40)) + 1023 * i_11_ & 0x3fff] >> 10;
					i_10_ >>= 1;
					for (int i_12_ = -3; i_12_ <= 3; i_12_++) {
						if (i_12_ + i_10_ >= 0 && i_10_ + i_12_ < Class98_Sub10_Sub3.anIntArray10356.length)
							Class98_Sub10_Sub3.anIntArray10356[i_10_ + i_12_] += 4 - Math.abs(i_12_);
					}
					Class98_Sub10_Sub3.anIntArray1283[snowFlake] = ((int) (Math.random() * GameShell.frameWidth) << 4) + (int) (Math.random() * 15.0);
					Class98_Sub10_Sub3.anIntArray10273[snowFlake] = 0;
					Class98_Sub10_Sub3.anIntArray1289[snowFlake] = (int) (Math.random() * 102.0) + 51;
					anIntArray3879[snowFlake] = 8 + (int) (Math.random() * 48.0);
				}
			}
			Class98_Sub43.setAllDirty(2);
		}
	}
}

/* Class103 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Archive;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;

public final class Class103 implements Runnable {
	public static int	anInt890;
	public static int	anInt892;
	public static int	anInt896	= 0;

	public static final void method1711(RSToolkit toolkit, byte i, Class98_Sub47 class98_sub47, WorldMapInfoDefinition class24) {
		Sprite sprite = class24.method286(toolkit, (byte) 92);
		if (sprite != null) {
			int i_0_ = sprite.getWidth();
			if ((i_0_ ^ 0xffffffff) > (sprite.getHeight() ^ 0xffffffff)) {
				i_0_ = sprite.getHeight();
			}
			int i_1_ = 10;
			int i_2_ = class98_sub47.anInt4266;
			int i_3_ = class98_sub47.anInt4271;
			int i_4_ = 0;
			int i_5_ = 0;
			int i_6_ = 0;
			if (class24.locationName != null) {
				i_4_ = StrongReferenceMCNode.p11FullMetrics.performWordWrap(class24.locationName, Class35.aStringArray335, null, null, -1);
				for (int i_7_ = 0; i_4_ > i_7_; i_7_++) {
					String string = Class35.aStringArray335[i_7_];
					if (i_7_ < i_4_ - 1) {
						string = string.substring(0, -4 + string.length());
					}
					int stringWidth = Js5Archive.aClass326_5308.getStringWidth(string);
					if (i_5_ < stringWidth) {
						i_5_ = stringWidth;
					}
				}
				i_6_ = Js5Archive.aClass326_5308.getFontHeight() * i_4_ - -(Js5Archive.aClass326_5308.method3704() / 2);
			}
			int i_9_ = i_0_ / 2 + class98_sub47.anInt4266;
			if (WorldMap.anInt2086 - -i_0_ <= i_2_) {
				if ((i_2_ ^ 0xffffffff) < (-i_0_ + WorldMap.anInt2093 ^ 0xffffffff)) {
					i_9_ = WorldMap.anInt2093 - i_0_ / 2 - (i_1_ + i_5_ / 2) - 5;
					i_2_ = -i_0_ + WorldMap.anInt2093;
				}
			} else {
				i_9_ = i_5_ / 2 + i_1_ + i_0_ / 2 + WorldMap.anInt2086 + 5;
				i_2_ = WorldMap.anInt2086;
			}
			int i_10_ = class98_sub47.anInt4271;
			if ((WorldMap.anInt2077 - -i_0_ ^ 0xffffffff) < (i_3_ ^ 0xffffffff)) {
				i_10_ = i_0_ / 2 + i_1_ + WorldMap.anInt2077;
				i_3_ = WorldMap.anInt2077;
			} else if ((WorldMap.anInt2094 - i_0_ ^ 0xffffffff) > (i_3_ ^ 0xffffffff)) {
				i_10_ = -i_6_ + -i_1_ + WorldMap.anInt2094 + -(i_0_ / 2);
				i_3_ = -i_0_ + WorldMap.anInt2094;
			}
			int i_11_ = 0xffff & (int) (32767.0 * (Math.atan2(i_2_ - class98_sub47.anInt4266, -class98_sub47.anInt4271 + i_3_) / 3.141592653589793));
			sprite.method3730(i_2_ + i_0_ / 2.0F, i_0_ / 2.0F + i_3_, 4096, i_11_);
			int i_12_ = -2;
			int i_13_ = -2;
			int i_14_ = -2;
			int i_15_ = -2;
			if (class24.locationName != null) {
				i_13_ = i_10_;
				i_12_ = i_9_ + -(i_5_ / 2) + -5;
				i_15_ = 3 + i_4_ * Js5Archive.aClass326_5308.getFontHeight() + i_13_;
				i_14_ = 10 + i_12_ + i_5_;
				if ((class24.anInt226 ^ 0xffffffff) != -1) {
					toolkit.drawPlayerSquareDot(i_14_ + -i_12_, i_15_ + -i_13_, i_13_, class24.anInt226, (byte) -66, i_12_);
				}
				if ((class24.anInt239 ^ 0xffffffff) != -1) {
					toolkit.method1781(true, i_15_ + -i_13_, i_14_ - i_12_, class24.anInt239, i_12_, i_13_);
				}
				for (int i_16_ = 0; (i_16_ ^ 0xffffffff) > (i_4_ ^ 0xffffffff); i_16_++) {
					String string = Class35.aStringArray335[i_16_];
					if (i_16_ < i_4_ - 1) {
						string = string.substring(0, -4 + string.length());
					}
					Js5Archive.aClass326_5308.drawStringLeftAnchor(toolkit, string, i_9_, i_10_, class24.anInt257, true);
					i_10_ += Js5Archive.aClass326_5308.getFontHeight();
				}
			}
			if (class24.anInt245 != -1 || class24.locationName != null) {
				Class98_Sub23 class98_sub23 = new Class98_Sub23(class98_sub47);
				i_0_ >>= 1;
				class98_sub23.anInt4004 = i_15_;
				class98_sub23.anInt4005 = i_13_;
				class98_sub23.anInt4002 = i_14_;
				class98_sub23.anInt4003 = i_12_;
				class98_sub23.anInt4000 = i_3_ - -i_0_;
				class98_sub23.anInt4006 = i_2_ + i_0_;
				class98_sub23.anInt3996 = i_2_ + -i_0_;
				class98_sub23.anInt3999 = -i_0_ + i_3_;
				InventoriesDefinitionParser.aClass148_110.addLast(class98_sub23, -20911);
			}
		}
	}

	volatile boolean			aBoolean893;
	volatile boolean			aBoolean895;
	public volatile Class268[]	aClass268Array894	= new Class268[2];

	public SignLink				aClass88_891;

	public Class103() {
		aBoolean893 = false;
		aBoolean895 = false;
	}

	@Override
	public final void run() {
		aBoolean895 = true;
		try {
			while (!aBoolean893) {
				for (int i = 0; (i ^ 0xffffffff) > -3; i++) {
					Class268 class268 = aClass268Array894[i];
					if (class268 != null) {
						class268.method3261((byte) -122);
					}
				}
				TimeTools.sleep(0, 10L);
				GameShell.createDummyActionEvent(aClass88_891, 31668, null);
			}
		} catch (Exception exception) {
			Class305_Sub1.reportError(exception, -123, null);
		} finally {
			aBoolean895 = false;
		}
	}
}

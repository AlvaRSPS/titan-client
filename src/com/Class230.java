/* Class230 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementFactory;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class230 {
	public static OutgoingOpcode	aClass171_1727;
	public static int				anInt1725	= 50;
	public static int				anInt1732;
	public static int[]				anIntArray1722;
	public static int[]				anIntArray1724;
	public static int[]				anIntArray1726;
	public static int[]				anIntArray1728;
	public static int[]				anIntArray1729;
	public static int[]				anIntArray1730;
	public static String[]			aStringArray1721;

	static {
		anIntArray1724 = new int[anInt1725];
		anIntArray1730 = new int[anInt1725];
		anIntArray1729 = new int[anInt1725];
		anIntArray1728 = new int[anInt1725];
		aStringArray1721 = new String[anInt1725];
		anIntArray1726 = new int[anInt1725];
		anIntArray1722 = new int[anInt1725];
		aClass171_1727 = new OutgoingOpcode(62, 1);
		anInt1732 = 1;
	}

	public static final void method2869(int i) {
		try {
			for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getFirst(32); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getNext(101)) {
				if (ClientScript2Event.method1179(class98_sub46_sub8.actionId, 255)) {
					QuickChatMessageParser.method3324(class98_sub46_sub8, -32612);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "on.C(" + i + ')');
		}
	}

	public static final void method2871(int i) {
		try {
			SceneGraphNode.method2962(false);
			int i_1_ = client.preferences.orthoZoom.getValue((byte) 120);
			do {
				if ((i_1_ ^ 0xffffffff) != -3) {
					if (i_1_ != 3) {
						break;
					}
				} else {
					Class98_Sub10_Sub39.method1121((byte) 17, client.graphicsToolkit, GameShell.frameHeight, GameShell.frameWidth, 100, 100);
					break;
				}
				LoadingScreenElementFactory.method3926(-120, 2, GameShell.frameWidth, WorldMapInfoDefinitionParser.anInt2856, client.graphicsToolkit, FileOnDisk.anInt3018, 2, GameShell.frameHeight);
			} while (false);
			if (i >= -30) {
				method2869(-126);
			}
			if (client.preferences.orthoZoom.supported((byte) -77)) {
				RtInterface.method3471(GameShell.canvas, 0);
			}
			if (client.graphicsToolkit != null) {
				Class98_Sub10_Sub34.method1104(99);
			}
			VarClientStringsDefinitionParser.aBoolean1839 = client.preferences.orthoZoom.getValue((byte) 122) != 0;
			OpenGLHeap.aBoolean6079 = client.preferences.orthoZoom.supported((byte) -58);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "on.B(" + i + ')');
		}
	}

	public static final void renderAmbientAndSunSettings(byte i) {

		OpenGlPointLight.aHa4185.setAmbientIntensity((client.preferences.brightnessLevel.getValue((byte) 125) * 0.1F + 0.7F) * Class159.aFloat1254);
		OpenGlPointLight.aHa4185.setSun(Class98_Sub46_Sub6.anInt5979, NPCDefinition.aFloat1150, Queue.aFloat1613, InputStream_Sub2.anInt29 << -1868059326, Class48.anInt410 << -1893384734, OutputStream_Sub2.anInt40 << 1516482466);
		OpenGlPointLight.aHa4185.method1775(Class98_Sub46_Sub4.aClass48_5962);
		// System.out.println("A: " + Class98_Sub46_Sub6.anInt5979 + " B: " +
		// NPCDefinition.aFloat1150 + " C: " + Queue.aFloat1613);
	}

	private OpenGLXToolkit	aHa_Sub3_Sub2_1731;

	public long				aLong1723;

	public Class230(OpenGLXToolkit var_ha_Sub3_Sub2, long l, int i) {
		aLong1723 = l;
		aHa_Sub3_Sub2_1731 = var_ha_Sub3_Sub2;
	}

	@Override
	protected final void finalize() throws Throwable {
		aHa_Sub3_Sub2_1731.method2082(0, aLong1723);
		super.finalize();
	}
}

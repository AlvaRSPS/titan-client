/* Class98_Sub10_Sub25 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;

public final class Class98_Sub10_Sub25 extends Class98_Sub10 {
	public static double	aDouble5675;
	public static long		aLong5677	= 0L;

	public static final int method1079(ActionGroup class98_sub46_sub9, int i) {
		try {
			if (i != 21) {
				return -125;
			}
			String string = Class21.method262(class98_sub46_sub9, (byte) 103);
			return Class42_Sub1.p13FullMetrics.method2676((byte) 108, GroupProgressMonitor.aClass332Array3408, string);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "om.B(" + (class98_sub46_sub9 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static final void method1080(byte i) {
		try {
			if (GameShell.signLink.is_signed && (Class289.server.index ^ 0xffffffff) != 0) {
				Class98_Sub12.method1131(-8804, Class289.server.index, Class289.server.host);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "om.E(" + i + ')');
		}
	}

	public static final void method1081(int i, ActionGroup class98_sub46_sub9, boolean bool, int i_6_) {
		try {
			if (Player.menuOpen) {
				int i_7_ = 0;
				for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) class98_sub46_sub9.actions.getFirst(-1); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) class98_sub46_sub9.actions.getNext(0)) {
					int i_8_ = LoginOpcode.method2824((byte) 85, class98_sub46_sub8);
					if (i_8_ > i_7_) {
						i_7_ = i_8_;
					}
				}
				i_7_ += 8;
				int i_9_ = 21 + class98_sub46_sub9.actionCount * 16;
				Class98_Sub43_Sub4.anInt5938 = (Class98_Sub5_Sub3.aBoolean5539 ? 26 : 22) + 16 * class98_sub46_sub9.actionCount;
				int i_10_ = Class246_Sub3_Sub4_Sub4.width + Class38.anInt355;
				if (i_10_ + i_7_ > GameShell.frameWidth) {
					i_10_ = Class38.anInt355 - i_7_;
				}
				if (bool == true) {
					if ((i_10_ ^ 0xffffffff) > -1) {
						i_10_ = 0;
					}
					int i_11_ = !Class98_Sub5_Sub3.aBoolean5539 ? 31 : 33;
					int i_12_ = i + -i_11_ + 13;
					if (GameShell.frameHeight < i_9_ + i_12_) {
						i_12_ = GameShell.frameHeight - i_9_;
					}
					LoadingScreenSequence.anInt2128 = i_10_;
					if (i_12_ < 0) {
						i_12_ = 0;
					}
					BackgroundColourLSEConfig.anInt3518 = i_12_;
					Class308.aClass98_Sub46_Sub9_2583 = class98_sub46_sub9;
					ScalingSpriteLoadingScreenElement.anInt3439 = i_7_;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "om.D(" + i + ',' + (class98_sub46_sub9 != null ? "{...}" : "null") + ',' + bool + ',' + i_6_ + ')');
		}
	}

	private int		anInt5676	= -1;

	private int		anInt5678;

	private int		anInt5679;

	private int[]	anIntArray5680;

	public Class98_Sub10_Sub25() {
		super(0, false);
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_2_) {
		try {
			if ((i ^ 0xffffffff) == -1) {
				anInt5676 = class98_sub22.readShort((byte) 127);
			}
			if (i_2_ >= -92) {
				anIntArray5680 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "om.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_2_ + ')');
		}
	}

	@Override
	public final int method992(int i) {
		try {
			return anInt5676;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "om.S(" + i + ')');
		}
	}

	@Override
	public final void method993(int i) {
		try {
			super.method993((short) i);
			anIntArray5680 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "om.P(" + i + ')');
		}
	}

	@Override
	public final int[][] method997(int i, int i_13_) {
		try {
			if (i >= -76) {
				anIntArray5680 = null;
			}
			int[][] is = this.aClass223_3859.method2828(i_13_, 0);
			if (this.aClass223_3859.aBoolean1683) {
				int i_14_ = ((HorizontalAlignment.anInt492 ^ 0xffffffff) == (anInt5678 ^ 0xffffffff) ? i_13_ : anInt5678 * i_13_ / HorizontalAlignment.anInt492) * anInt5679;
				int[] is_15_ = is[0];
				int[] is_16_ = is[1];
				int[] is_17_ = is[2];
				if (Class25.anInt268 != anInt5679) {
					for (int i_18_ = 0; Class25.anInt268 > i_18_; i_18_++) {
						int i_19_ = i_18_ * anInt5679 / Class25.anInt268;
						int i_20_ = anIntArray5680[i_19_ + i_14_];
						is_17_[i_18_] = Class202.and(i_20_, 255) << 1263100004;
						is_16_[i_18_] = Class202.and(4080, i_20_ >> 136339012);
						is_15_[i_18_] = Class202.and(4080, i_20_ >> 1712612364);
					}
				} else {
					for (int i_21_ = 0; Class25.anInt268 > i_21_; i_21_++) {
						int i_22_ = anIntArray5680[i_14_++];
						is_17_[i_21_] = Class202.and(255, i_22_) << 1450547076;
						is_16_[i_21_] = Class202.and(4080, i_22_ >> 365050084);
						is_15_[i_21_] = Class202.and(4080, i_22_ >> 1685547052);
					}
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "om.C(" + i + ',' + i_13_ + ')');
		}
	}

	@Override
	public final void method998(int i, int i_3_, int i_4_) {
		do {
			try {
				super.method998(i, i_3_, i_4_);
				if ((anInt5676 ^ 0xffffffff) > -1 || Class38.aD356 == null) {
					break;
				}
				int i_5_ = Class38.aD356.getInfo(anInt5676, -28755).aBoolean1822 ? 64 : 128;
				anIntArray5680 = Class38.aD356.getPixelsRgb(anInt5676, (byte) -121, i_5_, 1.0F, false, i_5_);
				anInt5679 = i_5_;
				anInt5678 = i_5_;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "om.O(" + i + ',' + i_3_ + ',' + i_4_ + ')');
			}
			break;
		} while (false);
	}
}

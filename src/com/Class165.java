/* Class165 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;

public final class Class165 {
	public static int[]	anIntArray1277	= new int[1];
	public static int	mapWidth		= 104;

	public static final void method2523(int i, Mob class246_sub3_sub4_sub2) {
		do {
			try {
				int i_0_ = -Queue.timer + class246_sub3_sub4_sub2.anInt6390;
				int i_1_ = class246_sub3_sub4_sub2.anInt6378 * 512 + 256 * class246_sub3_sub4_sub2.getSize(i ^ i);
				int i_2_ = class246_sub3_sub4_sub2.anInt6347 * 512 + 256 * class246_sub3_sub4_sub2.getSize(0);
				class246_sub3_sub4_sub2.boundExtentsZ += (-class246_sub3_sub4_sub2.boundExtentsZ + i_2_) / i_0_;
				class246_sub3_sub4_sub2.boundExtentsX += (-class246_sub3_sub4_sub2.boundExtentsX + i_1_) / i_0_;
				class246_sub3_sub4_sub2.anInt6433 = 0;
				if ((class246_sub3_sub4_sub2.anInt6407 ^ 0xffffffff) == -1) {
					class246_sub3_sub4_sub2.method3042(8192, i + -20481);
				}
				if (class246_sub3_sub4_sub2.anInt6407 == 1) {
					class246_sub3_sub4_sub2.method3042(12288, -8193);
				}
				if (class246_sub3_sub4_sub2.anInt6407 == 2) {
					class246_sub3_sub4_sub2.method3042(0, -8193);
				}
				if (class246_sub3_sub4_sub2.anInt6407 != 3) {
					break;
				}
				class246_sub3_sub4_sub2.method3042(4096, -8193);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "kv.A(" + i + ',' + (class246_sub3_sub4_sub2 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public static void method2524(byte i) {
		try {
			if (i > -103) {
				method2524((byte) 15);
			}
			anIntArray1277 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "kv.B(" + i + ')');
		}
	}
}

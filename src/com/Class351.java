
/* Class351 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Rectangle;

import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;

public final class Class351 {
	public static Class85					aClass85_2921;
	public static AnimationSkeletonSet[]	aClass98_Sub46_Sub16Array2924;
	public static int						anInt2922		= 0;
	public static int[]						anIntArray2923	= { 1, 0, -1, 0 };

	static {
		aClass85_2921 = new Class85(9, 7);
		aClass98_Sub46_Sub16Array2924 = new AnimationSkeletonSet[14];
	}

	public static final void handleClickTeleport(int i, int i_27_, int i_28_, int i_29_) {
		String string = "tele " + i_28_ + "," + (i_27_ >> -351282842) + "," + (i >> -1268340314) + "," + (i_27_ & 0x3f) + "," + (i & 0x3f);
		System.out.println("String: " + string);
		Class295.handleClientCommand(string, false, true, (byte) 117);
	}

	public static final void method3849(int i, int i_39_, int i_40_, int i_41_, int i_42_) {
		for (int i_43_ = 0; (i_43_ ^ 0xffffffff) > (Class69_Sub2.anInt5335 ^ 0xffffffff); i_43_++) {
			Rectangle rectangle = PlatformInformation.aRectangleArray4144[i_43_];
			if ((i_40_ ^ 0xffffffff) > (rectangle.width + rectangle.x ^ 0xffffffff) && i_40_ + i_41_ > rectangle.x && (i_42_ ^ 0xffffffff) > (rectangle.y + rectangle.height ^ 0xffffffff) && i_42_ + i > rectangle.y) {
				Class98_Sub10_Sub20.aBooleanArray5639[i_43_] = true;
			}
		}
		AnimatedProgressBarLSEConfig.method908(i_42_ + i, i_42_, false, i_40_, i_41_ + i_40_);
	}

	public static final boolean processOutsideUpdate(int i, int index, RsBitsBuffers buffer) {
		int i_1_ = buffer.readBits((byte) -86, 2);
		if ((i_1_ ^ 0xffffffff) == -1) {
			if ((buffer.readBits((byte) -110, 1) ^ 0xffffffff) != -1) {
				processOutsideUpdate(-2, index, buffer);
			}
			int i_2_ = buffer.readBits((byte) -119, 6);
			int i_3_ = buffer.readBits((byte) -53, 6);
			boolean bool = (buffer.readBits((byte) -15, 1) ^ 0xffffffff) == -2;
			if (bool) {
				Class65.DECODE_MASKS_PLAYERS_INDEXES_LIST[Class38.DECODE_MASKS_PLAYERS_COUNT++] = index;
			}
			if (Class151_Sub9.players[index] != null) {
				throw new RuntimeException("hr:lr");
			}
			GlobalPlayer globalPlayer = EnumDefinition.aClass376Array2562[index];
			Player player = Class151_Sub9.players[index] = new Player();
			player.index = index;
			if (Class224_Sub3_Sub1.aClass98_Sub22Array6146[index] != null) {
				player.decodeAppearance(Class224_Sub3_Sub1.aClass98_Sub22Array6146[index], (byte) 73);
			}
			player.method3047(globalPlayer.nextDirection, true, 61);
			player.anInt6364 = globalPlayer.target;
			int i_4_ = globalPlayer.positionHash;
			int i_5_ = i_4_ >> 55960220;
			int i_6_ = (0x3ffe75 & i_4_) >> -1901701522;
			int i_7_ = i_4_ & 0xff;
			int x = -Class272.gameSceneBaseX + i_2_ + (i_6_ << 847426022);
			player.clanmate = globalPlayer.clanmate;
			int z = i_3_ + (i_7_ << 1601007846) + -aa_Sub2.gameSceneBaseY;
			((Mob) player).pathSpeed[0] = Class98_Sub10_Sub21.playerMovementSpeeds[index];
			player.plane = player.collisionPlane = (byte) i_5_;
			if (Class1.isBridge(z, (byte) -104, x)) {
				player.collisionPlane++;
			}
			player.move(z, x, 1470);
			player.aBoolean6532 = false;
			EnumDefinition.aClass376Array2562[index] = null;
			return true;
		}
		if (i_1_ == 1) {
			int i_10_ = buffer.readBits((byte) -17, 2);
			int i_11_ = EnumDefinition.aClass376Array2562[index].positionHash;
			EnumDefinition.aClass376Array2562[index].positionHash = ((0x3 & i_10_ + (i_11_ >> -1513072196)) << 1924544668) + (i_11_ & 0xfffffff);
			return false;
		}
		if (i_1_ == 2) {
			int i_12_ = buffer.readBits((byte) -75, 5);
			int i_13_ = i_12_ >> 1739733571;
			int i_14_ = i_12_ & 0x7;
			int i_15_ = EnumDefinition.aClass376Array2562[index].positionHash;
			int i_16_ = 0x3 & i_13_ + (i_15_ >> 1832736924);
			int i_17_ = i_15_ >> 1823883438 & 0xff;
			int i_18_ = i_15_ & 0xff;
			if ((i_14_ ^ 0xffffffff) == -1) {
				i_18_--;
				i_17_--;
			}
			if (i_14_ == 1) {
				i_18_--;
			}
			if ((i_14_ ^ 0xffffffff) == -3) {
				i_17_++;
				i_18_--;
			}
			if ((i_14_ ^ 0xffffffff) == -4) {
				i_17_--;
			}
			if (i_14_ == 4) {
				i_17_++;
			}
			if (i_14_ == 5) {
				i_17_--;
				i_18_++;
			}
			if (i_14_ == 6) {
				i_18_++;
			}
			if ((i_14_ ^ 0xffffffff) == -8) {
				i_18_++;
				i_17_++;
			}
			EnumDefinition.aClass376Array2562[index].positionHash = (i_17_ << -893106514) + (i_16_ << 1563645276) - -i_18_;
			return false;
		}
		int i_19_ = buffer.readBits((byte) -63, 18);
		int i_20_ = i_19_ >> -720323312;
		int i_21_ = (0xffe5 & i_19_) >> 1140001512;
		int i_22_ = 0xff & i_19_;
		int i_24_ = i_20_;
		int i_25_ = i_21_;
		int i_26_ = i_22_;
		EnumDefinition.aClass376Array2562[index].positionHash = (i_25_ << 40340942) + (i_24_ << -1914335332) + i_26_;
		return false;
	}
}

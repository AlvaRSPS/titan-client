
/* Class31_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;
import java.net.Socket;

public final class DirectSocket extends SocketWrapper {
	public DirectSocket() {
		/* empty */
	}

	@Override
	public final Socket open(int i) throws IOException {
		try {
			if (i != -2) {
				return null;
			}
			return createDirectSocket((byte) -53);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}
}

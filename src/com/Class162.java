/* Class162 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.MemoryCacheNode;
import com.jagex.game.client.definition.SkyboxDefinition;

public final class Class162 {
	public static Class162		aClass162_1262	= new Class162(1);
	public static Class162		aClass162_1264	= new Class162(2);
	public static Class162		aClass162_1265	= new Class162(4);
	public static Class162		aClass162_1266	= new Class162(1);
	public static Class162		aClass162_1267	= new Class162(2);
	public static Class162		aClass162_1268	= new Class162(4);
	public static Class162		aClass162_1269	= new Class162(2);
	public static Class162		aClass162_1270	= new Class162(4);
	public static RtInterface	aClass293_1272	= null;
	public static int			anInt1271;

	public static final void method2516(int i) {
		Class331.aClass98_Sub46_Sub8_2803 = new ActionQueueEntry(TextResources.CANCEL.getText(client.gameLanguage, (byte) 25), "", Class21_Sub2.cursorId, 1012, -1, 0L, 0, 0, true, false, 0L, true);
	}

	public static final void method2518(int i, int i_1_, int i_2_, int i_3_) {
		if ((i_1_ ^ 0xffffffff) != -1012) {
			if ((i_1_ ^ 0xffffffff) == -1004) {
				ClientScript2Runtime.method3152(Class336.aClass105_2829, i_3_, i_2_);
			} else if (i_1_ == 1001) {
				ClientScript2Runtime.method3152(MemoryCacheNode.aClass105_5951, i_3_, i_2_);
			} else if (i_1_ == 1010) {
				ClientScript2Runtime.method3152(SkyboxDefinition.aClass105_474, i_3_, i_2_);
			} else if (i_1_ == 1004) {
				ClientScript2Runtime.method3152(Class308.aClass105_2576, i_3_, i_2_);
			}
		} else {
			ClientScript2Runtime.method3152(Class142.aClass105_1159, i_3_, i_2_);
		}
	}

	int anInt1263;

	private Class162(int i) {
		anInt1263 = i;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

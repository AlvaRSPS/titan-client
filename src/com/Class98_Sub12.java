
/* Class98_Sub12 - Decompiled by JODE
 */ package com; /*
					*/

import java.net.URL;

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.constants.BuildLocation;

public final class Class98_Sub12 extends Node {
	public static IncomingOpcode	aClass58_3877	= new IncomingOpcode(66, 1);
	public static int				anInt3872;
	public static int				bytesOut;

	public static final int method1128(Class65 class65, int i) {
		if (QuickChat.aClass65_2499 != class65) {
			if (Class98_Sub43_Sub3.aClass65_5926 != class65) {
				if (IncomingOpcode.aClass65_459 == class65) {
					return 34168;
				}
				if (class65 == MaxScreenSizePreferenceField.aClass65_3681) {
					return 34166;
				}
			} else {
				return 34167;
			}
		} else {
			return 5890;
		}
		throw new IllegalArgumentException();
	}

	public static void method1129(boolean bool) {
		try {
			if (bool != false) {
				method1129(false);
			}
			aClass58_3877 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "er.B(" + bool + ')');
		}
	}

	public static final void method1130(int i) {
		do {
			try {
				if (client.clientState != 9) {
					if (client.clientState == 5 || client.clientState == 6) {
						HashTableIterator.setClientState(3, false);
					} else if (client.clientState == 12) {
						HashTableIterator.setClientState(3, false);
					}
				} else {
					HashTableIterator.setClientState(5, false);
				}
				if (i == 27089) {
					break;
				}
				method1130(-66);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "er.D(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method1131(int i, int i_1_, String string) {
		try {
			if (GameShell.signLink.is_signed) {
				client.server = new Server();
				client.server.host = string;
				client.server.index = i_1_;
				if (BuildLocation.LIVE != client.buildLocation) {
					client.server.secondPort = client.server.index + 50000;
					client.server.initialPort = client.server.index + 40000;
				}
				if (Class98_Sub28_Sub1.aClass53_Sub1Array5805.length > i_1_ && Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_1_] != null) {
					client.worldFlags = Class98_Sub28_Sub1.aClass53_Sub1Array5805[i_1_].anInt427;
				}
				return true;
			}
			String string_2_ = "";
			if (client.buildLocation != BuildLocation.LIVE) {
				string_2_ = ":" + (i_1_ - -7000);
			}
			String string_3_ = "";
			if (client.settingsString != null) {
				string_3_ = "/p=" + client.settingsString;
			}
			String string_4_ = "http://" + string + string_2_ + "/l=" + client.gameLanguage + "/a=" + client.affiliateId + string_3_ + "/j" + (!client.useObjectTag ? "0" : "1") + ",o" + (!client.jsEnabled ? "0" : "1") + ",a2";
			if (i != -8804) {
				method1129(false);
			}
			try {
				client.activeClient.getAppletContext().showDocument(new URL(string_4_), "_self");
			} catch (Exception exception) {
				return false;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "er.C(" + i + ',' + i_1_ + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	boolean	aBoolean3878;
	int		anInt3871;

	int		anInt3873;

	int		anInt3874;

	int		anInt3875;

	int		anInt3876;

	Class98_Sub12(int i, int i_5_, int i_6_, int i_7_, int i_8_, boolean bool) {
		try {
			aBoolean3878 = bool;
			anInt3876 = i;
			anInt3875 = i_7_;
			anInt3873 = i_6_;
			anInt3871 = i_8_;
			anInt3874 = i_5_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "er.<init>(" + i + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + bool + ')');
		}
	}
}

/* Class246_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.CursorDefinition;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.constants.BuildType;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class246_Sub2 extends SceneGraphNode {
	public static Class172[][][] aClass172ArrayArrayArray5077;

	public static final void draw(int i, FontSpecifications fontMetrics, Font font, boolean bool, RSToolkit toolkit, String infoString) {
		boolean bool_2_ = !Class98_Sub10.aBoolean3858 || NativeToolkit.method2062(-4264);
		if (bool_2_) {
			do {
				if (!Class98_Sub10.aBoolean3858 || !bool_2_) {
					int stringWidth = fontMetrics.getStringWidth(250, infoString, null, (byte) -115);
					int i_4_ = 13 * fontMetrics.method2669(250, 0, infoString, null);
					int i_5_ = 4;
					int i_6_ = 6 + i_5_;
					int i_7_ = 6 + i_5_;
					toolkit.fillRectangle(-i_5_ + i_6_, -i_5_ + i_7_, i_5_ + stringWidth + i_5_, i_5_ + i_5_ + i_4_, -16777216, 0);
					toolkit.drawRectangle(-i_5_ + i_6_, i_7_ + -i_5_, i_5_ + stringWidth - -i_5_, i_5_ + i_4_ - -i_5_, -1, 0);
					font.drawString(i_6_, null, stringWidth, infoString, 0, -1, null, 1, (byte) -84, -1, null, 0, 0, 1, i_7_, i_4_);
					Class246_Sub3_Sub4_Sub3.method3071(i_5_ + i_4_ - -i_5_, -1, stringWidth - (-i_5_ - i_5_), -i_5_ + i_6_, i_7_ - i_5_);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				fontMetrics = HitmarksDefinitionParser.aClass197_1004;
				font = toolkit.createFont(fontMetrics, Class98_Sub10_Sub7.aClass324Array5575, true);
				int stringWidth = fontMetrics.getStringWidth(250, infoString, null, (byte) 124);
				int stringHeight = fontMetrics.getStringHeight(null, 250, fontMetrics.anInt1518, infoString, true);
				int i_10_ = CursorDefinition.aClass324_1733.anInt2722;
				int i_11_ = i_10_ - -4;
				stringHeight += i_11_ * 2;
				stringWidth += i_11_ * 2;
				if ((stringHeight ^ 0xffffffff) > (GamePreferences.anInt4060 ^ 0xffffffff)) {
					stringHeight = GamePreferences.anInt4060;
				}
				if ((stringWidth ^ 0xffffffff) > (Class98_Sub10_Sub18.anInt5626 ^ 0xffffffff)) {
					stringWidth = Class98_Sub10_Sub18.anInt5626;
				}
				int xPos = Class151.aClass63_1216.getPosition(client.clientWidth, stringWidth, (byte) 61) + Class15.anInt170;
				int yPos = Class202.aClass110_1547.getPosition(stringHeight, client.clientHeight, (byte) -56) + Class76_Sub10.anInt3793;
				if (OpenGLHeap.aBoolean6079) {
					xPos += Class189.method2642((byte) 42);
					yPos += MapScenesDefinitionParser.method3765(false);
				}
				toolkit.createSprite(CharacterShadowsPreferenceField.aClass324_3713, false).method3728(Class42_Sub2.aClass324_5359.anInt2722 + xPos, yPos + Class42_Sub2.aClass324_5359.anInt2720, stringWidth - 2 * Class42_Sub2.aClass324_5359.anInt2722, -(2 * Class42_Sub2.aClass324_5359.anInt2720)
						+ stringHeight, 1, 0, 0);
				toolkit.createSprite(Class42_Sub2.aClass324_5359, true).draw(xPos, yPos);
				Class42_Sub2.aClass324_5359.flipHorizontal();
				toolkit.createSprite(Class42_Sub2.aClass324_5359, true).draw(-i_10_ + stringWidth + xPos, yPos);
				Class42_Sub2.aClass324_5359.flipVertical();
				toolkit.createSprite(Class42_Sub2.aClass324_5359, true).draw(-i_10_ + xPos + stringWidth, stringHeight + yPos - i_10_);
				Class42_Sub2.aClass324_5359.flipHorizontal();
				toolkit.createSprite(Class42_Sub2.aClass324_5359, true).draw(xPos, -i_10_ + yPos - -stringHeight);
				Class42_Sub2.aClass324_5359.flipVertical();
				toolkit.createSprite(CursorDefinition.aClass324_1733, true).drawRepeat(xPos, yPos - -Class42_Sub2.aClass324_5359.anInt2720, i_10_, stringHeight - 2 * Class42_Sub2.aClass324_5359.anInt2720);
				CursorDefinition.aClass324_1733.method3687();
				toolkit.createSprite(CursorDefinition.aClass324_1733, true).drawRepeat(xPos + Class42_Sub2.aClass324_5359.anInt2722, yPos, stringWidth + -(2 * Class42_Sub2.aClass324_5359.anInt2722), i_10_);
				CursorDefinition.aClass324_1733.method3687();
				toolkit.createSprite(CursorDefinition.aClass324_1733, true).drawRepeat(-i_10_ + stringWidth + xPos, Class42_Sub2.aClass324_5359.anInt2720 + yPos, i_10_, -(2 * Class42_Sub2.aClass324_5359.anInt2720) + stringHeight);
				CursorDefinition.aClass324_1733.method3687();
				toolkit.createSprite(CursorDefinition.aClass324_1733, true).drawRepeat(Class42_Sub2.aClass324_5359.anInt2722 + xPos, stringHeight + yPos - i_10_, -(Class42_Sub2.aClass324_5359.anInt2722 * 2) + stringWidth, i_10_);
				CursorDefinition.aClass324_1733.method3687();
				font.drawString(i_11_ + xPos, null, stringWidth - i_11_ * 2, infoString, 0, -1, null, 1, (byte) -90, ~0xffffff | ParticleManager.anInt384, null, 0, 0, 1, yPos - -i_11_, -(i_11_ * 2) + stringHeight);
				Class246_Sub3_Sub4_Sub3.method3071(stringHeight, -1, stringWidth, xPos, yPos);
			} while (false);
			if (bool) {
				try {
					if (!OpenGLHeap.aBoolean6079) {
						toolkit.renderFrame(-127);
					} else {
						BuildType.method181((byte) 10);
					}
				} catch (Exception_Sub1 exception_sub1) {
					/* empty */
				}
			}
		}
	}

	public static final void method2971(int i, byte i_0_, int i_1_) {
		GrandExchangeOffer.anInt849 = -WorldMap.anInt2075 + i_1_;
		Class169.anInt1307 = i + -WorldMap.anInt2078;
	}

	public static final boolean method2973(int i, int i_14_, byte i_15_) {
		return (i & 0x800) != 0;
	}

	Mob	aClass246_Sub3_Sub4_Sub2_5076;

	int	anInt5071;

	int	anInt5073;

	int	anInt5074;

	int	anInt5075;
}

/* Class119_Sub4 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;
import com.jagex.game.toolkit.heap.Heap;

public final class Class119_Sub4 extends Class119 {
	public static Sprite[]	aClass332Array4739;
	public static float		aFloat4740	= 1024.0F;

	public static final float method2188(float f, int i) {
		try {
			if (i != 1024) {
				method2190(-15, -6);
			}
			return f * f * f * (10.0F + f * (6.0F * f - 15.0F));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sr.F(" + f + ',' + i + ')');
		}
	}

	public static void method2189(byte i) {
		do {
			try {
				aClass332Array4739 = null;
				if (i == -1) {
					break;
				}
				method2190(-110, 15);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sr.D(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method2190(int i, int i_22_) {
		try {
			Class256.anInt1945 = i_22_;
			Class98_Sub5_Sub2.anInt5536 = -1;
			GroundBlendingPreferenceField.anInt3711 = 3;
			Class287.anInt2186 = 100;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sr.A(" + i + ',' + i_22_ + ')');
		}
	}

	private int	anInt4736;

	private int	anInt4737;

	private int	anInt4738;

	private int	anInt4741;

	Class119_Sub4(int i, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_) {
		super(i_16_, i_17_, i_18_);
		try {
			anInt4738 = i;
			anInt4741 = i_15_;
			anInt4736 = i_14_;
			anInt4737 = i_13_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sr.<init>(" + i + ',' + i_13_ + ',' + i_14_ + ',' + i_15_ + ',' + i_16_ + ',' + i_17_ + ',' + i_18_ + ')');
		}
	}

	@Override
	public final void method2174(int i, int i_7_, int i_8_) {
		try {
			int i_9_ = i * anInt4738 >> 263929676;
			int i_10_ = i * anInt4736 >> -496298516;
			if (i_8_ == -5515) {
				int i_11_ = anInt4737 * i_7_ >> -1264175348;
				int i_12_ = anInt4741 * i_7_ >> 226072012;
				Heap.method1675(i_10_, this.anInt987, i_12_, i_11_, this.anInt985, i_9_, this.anInt988, (byte) -89);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sr.C(" + i + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	@Override
	public final void method2178(int i, int i_0_, int i_1_) {
		try {
			int i_3_ = i_0_ * anInt4738 >> -282666356;
			int i_4_ = i_0_ * anInt4736 >> 151121132;
			int i_5_ = anInt4737 * i_1_ >> -444889972;
			int i_6_ = i_1_ * anInt4741 >> 150376812;
			Class98_Sub47.method1658(i_5_, i_3_, i_4_, 16977, i_6_, this.anInt988);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sr.B(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	@Override
	public final void method2179(byte i, int i_19_, int i_20_) {
		try {
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sr.E(" + i + ',' + i_19_ + ',' + i_20_ + ')');
		}
	}
}

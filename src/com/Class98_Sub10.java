/* Class98_Sub10 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.quickchat.QuickChatCategory;

public abstract class Class98_Sub10 extends Node {
	public static boolean	aBoolean3858;
	public static boolean	aBoolean3862;

	public static final void method995(NPC npc, byte i, boolean bool) {
		if (Class359.actionCount < 400) {
			NPCDefinition definition = npc.definition;
			if (definition.anIntArray1109 != null) {
				definition = definition.method2300(StartupStage.varValues, (byte) 120);
				if (definition == null) {
					return;
				}
			}
			if (definition.visibleOnMinimap) {
				String npcName = definition.name;

				if (definition.combatLevel != 0) {
					String levelString = GameDefinition.STELLAR_DAWN != client.game ? TextResources.LEVEL.getText(client.gameLanguage, (byte) 25) : TextResources.RATING.getText(client.gameLanguage, (byte) 25);
					npcName += Class108.getColour(Class87.localPlayer.combatLevel, definition.combatLevel, i + 16328) + " (" + levelString + definition.combatLevel + ")";
				}

				if (Class98_Sub10_Sub9.aBoolean5585 && !bool) {
					ParamDefinition class149 = (QuickChatCategory.anInt5945 ^ 0xffffffff) != 0 ? Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, QuickChatCategory.anInt5945) : null;
					if ((0x2 & Class98_Sub4.anInt3826 ^ 0xffffffff) != -1 && (class149 == null || (definition.method2305(QuickChatCategory.anInt5945, class149.defaultInteger, (byte) 125) ^ 0xffffffff) != (class149.defaultInteger ^ 0xffffffff))) {
						ActionQueueEntry.addAction(false, true, npc.index, Class336.anInt2823, 0, Class246_Sub3_Sub3.applyMenuText + " -> <col=ffff00>" + npcName, false, 0, 48, npc.index, -1, false, Class287_Sub2.aString3272);
					}
				}

				if (!bool) {
					String[] options = definition.options;
					if (Class34.aBoolean324) {
						options = Class98_Sub10_Sub6.getUsedOptions(i ^ ~0x37b, options);
					}
					if (options != null) {
						for (int option = 4; (option ^ 0xffffffff) <= -1; option--) {
							if (options[option] != null && (definition.aByte1129 == 0 || !options[option].equalsIgnoreCase(TextResources.ATTACK.getText(client.gameLanguage, (byte) 25)))) {
								int i_8_ = 0;
								if (option == 0) {
									i_8_ = 10;
								}
								int i_9_ = Class284_Sub2.anInt5186;
								if (option == 1) {
									i_8_ = 25;
								}
								if (option == 2) {
									i_8_ = 11;
								}
								if ((option ^ 0xffffffff) == -4) {
									i_8_ = 12;
								}
								if (definition.anInt1143 == option) {
									i_9_ = definition.anInt1154;
								}
								if (option == 4) {
									i_8_ = 17;
								}
								if (definition.anInt1114 == option) {
									i_9_ = definition.anInt1110;
								}
								ActionQueueEntry.addAction(false, true, npc.index, options[option].equalsIgnoreCase(TextResources.ATTACK.getText(client.gameLanguage, (byte) 25)) ? definition.anInt1099 : i_9_, 0, "<col=ffff00>" + npcName, false, 0, i_8_, npc.index, -1, false,
										options[option]);
								// System.out.println("option:" + option +
								// "string: " + npcName + "bool: " + bool +
								// "definition.aByte1129:" +
								// definition.aByte1129);
							}
						}
					}
					if (definition.aByte1129 == 1 && options != null) {
						for (int option = 4; option >= 0; option--) {
							if (options[option] != null && options[option].equalsIgnoreCase(TextResources.ATTACK.getText(client.gameLanguage, (byte) 25))) {
								short i_11_ = 0;
								if ((Class87.localPlayer.combatLevel ^ 0xffffffff) > (definition.combatLevel ^ 0xffffffff)) {
									i_11_ = (short) 2000;
								}
								short i_12_ = 0;
								if (option == 0) {
									i_12_ = (short) 10;
								}
								if ((option ^ 0xffffffff) == -2) {
									i_12_ = (short) 25;
								}
								if ((option ^ 0xffffffff) == -3) {
									i_12_ = (short) 11;
								}
								if ((option ^ 0xffffffff) == -4) {
									i_12_ = (short) 12;
								}
								if ((option ^ 0xffffffff) == -5) {
									i_12_ = (short) 17;
								}
								if ((i_12_ ^ 0xffffffff) != -1) {
									i_12_ += i_11_;
								}
								ActionQueueEntry.addAction(false, true, npc.index, definition.anInt1099, 0, "<col=ffff00>" + npcName, false, 0, i_12_, npc.index, -1, false, options[option]);
							}
						}
					}
				}
				ActionQueueEntry.addAction(false, true, npc.index, Class16.anInt190, 0, "<col=ffff00>" + npcName, false, 0, 1002, npc.index, -1, bool, TextResources.EXAMINE.getText(client.gameLanguage, (byte) 25));
			}
		}
	}

	public static final void method999(byte i) {
		try {
			if ((client.clientState ^ 0xffffffff) != -4) {
				if ((client.clientState ^ 0xffffffff) == -8) {
					HashTableIterator.setClientState(8, false);
				} else if (client.clientState == 10) {
					HashTableIterator.setClientState(11, false);
				}
			} else {
				HashTableIterator.setClientState(4, false);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.R(" + i + ')');
		}
	}

	boolean			aBoolean3861;
	Class16			aClass16_3863;
	Class223		aClass223_3859;

	Class98_Sub10[]	aClass98_Sub10Array3857;

	public int		anInt3860;

	Class98_Sub10(int i, boolean bool) {
		try {
			aBoolean3861 = bool;
			aClass98_Sub10Array3857 = new Class98_Sub10[i];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.<init>(" + i + ',' + bool + ')');
		}
	}

	public final int[] method1000(int i, int i_18_, int i_19_) {
		try {
			if (i_19_ != 0) {
				method993(-48);
			}
			if (aClass98_Sub10Array3857[i_18_].aBoolean3861) {
				return aClass98_Sub10Array3857[i_18_].method990(255, i);
			}
			return aClass98_Sub10Array3857[i_18_].method997(-94, i)[0];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.Q(" + i + ',' + i_18_ + ',' + i_19_ + ')');
		}
	}

	public void method1001(byte i) {
		try {
			if (i != 66) {
				anInt3860 = 121;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.I(" + i + ')');
		}
	}

	int[] method990(int i, int i_0_) {
		try {
			if (i != 255) {
				aClass98_Sub10Array3857 = null;
			}
			throw new IllegalStateException("This operation does not have a monochrome output");
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.G(" + i + ',' + i_0_ + ')');
		}
	}

	public void method991(int i, RSByteBuffer class98_sub22, byte i_1_) {
		try {
			if (i_1_ > -92) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_1_ + ')');
		}
	}

	int method992(int i) {
		try {
			return -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.S(" + i + ')');
		}
	}

	void method993(int i) {
		try {
			if (i != 1002) {
				aBoolean3862 = true;
			}
			if (aBoolean3861) {
				aClass16_3863.method236(34);
				aClass16_3863 = null;
			} else {
				aClass223_3859.method2831(0);
				aClass223_3859 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.P(" + i + ')');
		}
	}

	public final int[][] method994(int i, int i_3_, int i_4_) {
		try {
			if (i_3_ != 24431) {
				aClass16_3863 = null;
			}
			if (!aClass98_Sub10Array3857[i_4_].aBoolean3861) {
				return aClass98_Sub10Array3857[i_4_].method997(-119, i);
			}
			int[] is = aClass98_Sub10Array3857[i_4_].method990(i_3_ + -24176, i);
			int[][] is_5_ = new int[3][];
			is_5_[1] = is;
			is_5_[2] = is;
			is_5_[0] = is;
			return is_5_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.L(" + i + ',' + i_3_ + ',' + i_4_ + ')');
		}
	}

	int method996(byte i) {
		try {
			if (i > -119) {
				return 85;
			}
			return -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.M(" + i + ')');
		}
	}

	int[][] method997(int i, int i_13_) {
		try {
			if (i > -76) {
				method999((byte) 1);
			}
			throw new IllegalStateException("This operation does not have a colour output");
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.C(" + i + ',' + i_13_ + ')');
		}
	}

	void method998(int i, int i_14_, int i_15_) {
		try {
			int i_16_ = i_15_ == (anInt3860 ^ 0xffffffff) ? i : anInt3860;
			if (aBoolean3861) {
				aClass16_3863 = new Class16(i_16_, i, i_14_);
			} else {
				aClass223_3859 = new Class223(i_16_, i, i_14_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dt.O(" + i + ',' + i_14_ + ',' + i_15_ + ')');
		}
	}
}

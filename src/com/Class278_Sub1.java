
/* Class278_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;

public final class Class278_Sub1 extends WorldMap {
	public static boolean[][]	aBooleanArrayArray5171	= { new boolean[13], { false, false, true, true, true, true, true, false, false, false, false, false, true }, { true, true, true, true, true, true, false, false, false, false, false, false, false }, { true, true, true, false, false, true, true,
			true, false, false, false, false, false }, { true, false, false, false, false, true, true, true, false, false, false, false, false }, { false, false, true, true, true, true, false, false, false, false, false, false, false }, { false, true, true, true, true, true, false, false, false,
					false, false, false, true }, { false, true, true, true, true, true, true, true, false, false, false, false, true }, { true, true, false, false, false, false, false, true, false, false, false, false, false }, { true, true, true, true, true, false, false, false, true, true, false,
							false, false }, { true, false, false, false, true, true, true, true, true, true, false, false, false }, { true, false, true, true, true, true, true, true, false, false, true, true, false }, { true, true, true, true, true, true, true, true, true, true, true, true, true },
			new boolean[13], { true, true, true, true, true, true, true, true, true, true, true, true, true } };
	public static int			anInt5170;
	public static int[]			COSINE;
	public static int[][]		anIntArrayArray5169		= { { 12, 12, 12, 12 }, { 12, 12, 12, 12 }, { 5, 5, 5 }, { 5, 5, 5 }, { 5, 5, 5 }, { 5, 5, 5 }, { 12, 12, 12, 12, 12, 12 }, { 1, 1, 1, 7 }, { 1, 1, 7, 1 }, { 8, 9, 9, 8, 8, 9 }, { 8, 8, 9, 8, 9, 9 }, { 10, 10, 11, 11, 11, 10 }, { 12, 12, 12,
			12 } };

	static {
		anInt5170 = 0;
	}

	public static void method3319(int i) {
		try {
			anIntArrayArray5169 = null;
			aBooleanArrayArray5171 = null;
			if (i == 1204) {
				COSINE = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "paa.A(" + i + ')');
		}
	}

	public static final int method3320(int i) {
		RSToolkit toolkit = client.graphicsToolkit;
		boolean bool = false;
		if ((client.preferences.currentToolkit.getValue((byte) 123) ^ 0xffffffff) != -1) {
			Canvas canvas = new Canvas();
			canvas.setSize(100, 100);
			bool = true;

			toolkit = RSToolkit.createToolkit(0, canvas, 127, null, 0, null);
		}
		long l = TimeTools.getCurrentTime(-47);
		for (int i_0_ = 0; i_0_ < 10000; i_0_++) {
			toolkit.method1751(5, 10, 100, 75, 50, 100, 15, 90, 100, -65536, -65536, -65536, 1);
		}
		int i_1_ = (int) (TimeTools.getCurrentTime(-47) - l);
		toolkit.drawPlayerSquareDot(100, 100, 0, -16777216, (byte) -66, 0);
		if (bool) {
			toolkit.destroy(-1);
		}
		if (i != 12) {
			method3319(-59);
		}
		return i_1_;
	}
}

/* Class98_Sub10_Sub20 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;

public final class Class98_Sub10_Sub20 extends Class98_Sub10 {
	public static boolean[]			aBooleanArray5639	= new boolean[100];
	public static IncomingOpcode	aClass58_5635		= new IncomingOpcode(52, 4);
	public static IncomingOpcode	aClass58_5638		= new IncomingOpcode(2, 2);
	public static int				anInt5640			= 0;

	public static final void decodeMovementBlock(int index, int dummy, RsBitsBuffers packet) {
		boolean needMasksUpdate = packet.readBits((byte) -110, 1) == 1;
		if (needMasksUpdate) {
			Class65.DECODE_MASKS_PLAYERS_INDEXES_LIST[Class38.DECODE_MASKS_PLAYERS_COUNT++] = index;
		}
		int updateType = packet.readBits((byte) -48, 2);
		Player player = Class151_Sub9.players[index];
		if (updateType == 0) {// REMOVE/ADD
			if (needMasksUpdate) {
				player.aBoolean6532 = false;
			} else {
				if ((index ^ 0xffffffff) == (OpenGLHeap.localPlayerIndex ^ 0xffffffff)) {
					throw new RuntimeException("Local player index mismatch with player index!");
				}
				GlobalPlayer globalPlayer = EnumDefinition.aClass376Array2562[index] = new GlobalPlayer();
				globalPlayer.positionHash = aa_Sub2.gameSceneBaseY + player.pathZ[0] >> 6 + (player.pathX[0] + Class272.gameSceneBaseX >> 6 << 14) + (player.plane << 28);
				if (player.nextDirection != -1) {
					globalPlayer.nextDirection = player.nextDirection;
				} else {
					globalPlayer.nextDirection = player.yaw.value((byte) 116);
				}
				globalPlayer.clanmate = player.clanmate;
				globalPlayer.target = player.anInt6364;
				if (player.anInt6525 > 0) {
					Huffman.method2778(true, player);
				}
				Class151_Sub9.players[index] = null;
				if (packet.readBits((byte) -105, 1) != 0) {
					Class351.processOutsideUpdate(dummy + -14, index, packet);
				}
			}
		} else if (updateType == 1) {
			int dir = packet.readBits((byte) -46, 3);
			int p_x = player.pathX[0];
			int p_z = player.pathZ[0];
			if (dir == 0) {
				p_z--;
				p_x--;
			} else if (dir == 1) {
				p_z--;
			} else if (dir == 2) {
				p_z--;
				p_x++;
			} else if (dir == 3) {
				p_x--;
			} else if (dir == 4) {
				p_x++;
			} else if (dir == 5) {
				p_z++;
				p_x--;
			} else if (dir == 6) {
				p_z++;
			} else if (dir == 7) {
				p_x++;
				p_z++;
			}
			if (!needMasksUpdate) {
				player.move(p_x, p_z, Class98_Sub10_Sub21.playerMovementSpeeds[index], dummy ^ ~0xc);
			} else {
				player.path_z = p_z;
				player.aBoolean6532 = true;
				player.path_x = p_x;
			}
		} else if (updateType == 2) {
			int dir = packet.readBits((byte) -85, 4);
			int p_x = player.pathX[0];
			int p_z = player.pathZ[0];
			if (dir == 0) {
				p_x -= 2;
				p_z -= 2;
			} else if (dir == 1) {
				p_z -= 2;
				p_x--;
			} else if (dir == 2) {
				p_z -= 2;
			} else if (dir == 3) {
				p_z -= 2;
				p_x++;
			} else if (dir == 4) {
				p_x += 2;
				p_z -= 2;
			} else if (dir == 5) {
				p_z--;
				p_x -= 2;
			} else if (dir == 6) {
				p_z--;
				p_x += 2;
			} else if (dir == 7) {
				p_x -= 2;
			} else if (dir == 8) {
				p_x += 2;
			} else if (dir == 9) {
				p_x -= 2;
				p_z++;
			} else if (dir == 10) {
				p_x += 2;
				p_z++;
			} else if (dir == 11) {
				p_x -= 2;
				p_z += 2;
			} else if (dir == 12) {
				p_z += 2;
				p_x--;
			} else if (dir == 13) {
				p_z += 2;
			} else if (dir == 14) {
				p_z += 2;
				p_x++;
			} else if (dir == 15) {
				p_z += 2;
				p_x += 2;
			}
			if (!needMasksUpdate) {
				player.move(p_x, p_z, Class98_Sub10_Sub21.playerMovementSpeeds[index], dummy + -13);
			} else {
				player.path_z = p_z;
				player.aBoolean6532 = true;
				player.path_x = p_x;
			}
		} else {
			int local = packet.readBits((byte) -53, 1);
			if (local == 0) {
				int packed = packet.readBits((byte) -102, 12);
				int speed = packed >> 10;
				int d_x = (packed & 0x3e0) >> 5;
				if (d_x > 15) {
					d_x -= 32;
				}
				int d_z = packed & 0x1f;
				if (d_z > 15) {
					d_z -= 32;
				}
				int x = d_x + player.pathX[0];
				int z = player.pathZ[0] - -d_z;
				if (needMasksUpdate) {
					player.path_x = x;
					player.aBoolean6532 = true;
					player.path_z = z;
				} else {
					player.move(x, z, Class98_Sub10_Sub21.playerMovementSpeeds[index], -1);
				}
				player.plane = player.collisionPlane = (byte) (0x3 & speed + player.plane);
				if (Class1.isBridge(z, (byte) -102, x)) {
					player.collisionPlane++;
				}
				if (OpenGLHeap.localPlayerIndex == index) {
					if (player.plane != Font.localPlane) {
						Class358.aBoolean3033 = true;
					}
					Font.localPlane = player.plane;
				}
			} else {
				int packed_ = packet.readBits((byte) -103, 30);
				int dy = packed_ >> 28;
				int dx = 0x3fff & packed_ >> 14;
				int dz = packed_ & 0x3fff;
				int x = dx - Class272.gameSceneBaseX;
				int z = dz - aa_Sub2.gameSceneBaseY;
				if (needMasksUpdate) {
					player.path_z = z;
					player.path_x = x;
					player.aBoolean6532 = true;
				} else {
					player.move(x, z, Class98_Sub10_Sub21.playerMovementSpeeds[index], -1);
				}
				player.plane = player.collisionPlane = (byte) (0x3 & dy);
				if (Class1.isBridge(z, (byte) -95, x)) {
					player.collisionPlane++;
				}
				if (index == OpenGLHeap.localPlayerIndex) {
					Font.localPlane = player.plane;
				}
			}
		}
	}

	public static final byte[] method1061(int i, int i_23_, int i_24_, Object object) {
		if (object == null) {
			return null;
		}
		if (object instanceof byte[]) {
			byte[] is = (byte[]) object;
			return Class98_Sub23.method1268(i_23_, i_24_, is, (byte) 50);
		}
		if (object instanceof Class317) {
			Class317 class317 = (Class317) object;
			return class317.method3653(i_24_, i_23_, false);
		}
		throw new IllegalArgumentException();
	}

	private boolean	aBoolean5636;

	private int		anInt5637	= 4096;

	public Class98_Sub10_Sub20() {
		super(1, false);
		aBoolean5636 = true;
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_21_) {
		int i_22_ = i;
		do {
			if (i_22_ != 0) {
				if (i_22_ != 1) {
					break;
				}
			} else {
				anInt5637 = class98_sub22.readShort((byte) 127);
				break;
			}
			aBoolean5636 = (class98_sub22.readUnsignedByte((byte) 81) ^ 0xffffffff) == -2;
		} while (false);

	}

	@Override
	public final int[][] method997(int i, int i_25_) {
		int[][] is = this.aClass223_3859.method2828(i_25_, 0);
		if (this.aClass223_3859.aBoolean1683) {
			int[] is_26_ = method1000(i_25_ + -1 & SoftwareNativeHeap.anInt6075, 0, 0);
			int[] is_27_ = method1000(i_25_, 0, 0);
			int[] is_28_ = method1000(SoftwareNativeHeap.anInt6075 & i_25_ + 1, 0, 0);
			int[] is_29_ = is[0];
			int[] is_30_ = is[1];
			int[] is_31_ = is[2];
			for (int i_32_ = 0; i_32_ < Class25.anInt268; i_32_++) {
				int i_33_ = anInt5637 * (is_28_[i_32_] - is_26_[i_32_]);
				int i_34_ = (-is_27_[i_32_ - 1 & Class329.anInt2761] + is_27_[1 + i_32_ & Class329.anInt2761]) * anInt5637;
				int i_35_ = i_34_ >> -1929830068;
				int i_36_ = i_33_ >> -741780532;
				int i_37_ = i_35_ * i_35_ >> 2073615084;
				int i_38_ = i_36_ * i_36_ >> 908367244;
				int i_39_ = (int) (Math.sqrt((4096 + i_38_ + i_37_) / 4096.0F) * 4096.0);
				int i_40_;
				int i_41_;
				int i_42_;
				if (i_39_ != 0) {
					i_41_ = i_34_ / i_39_;
					i_40_ = 16777216 / i_39_;
					i_42_ = i_33_ / i_39_;
				} else {
					i_40_ = 0;
					i_41_ = 0;
					i_42_ = 0;
				}
				if (aBoolean5636) {
					i_41_ = 2048 - -(i_41_ >> 2066599329);
					i_42_ = (i_42_ >> 122551201) + 2048;
					i_40_ = (i_40_ >> 323698081) + 2048;
				}
				is_29_[i_32_] = i_41_;
				is_30_[i_32_] = i_42_;
				is_31_[i_32_] = i_40_;
			}
		}
		return is;
	}
}

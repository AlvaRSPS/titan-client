
/* Class281 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Frame;

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.quickchat.QuickChatCategory;

public final class Class281 {
	public static byte[][][] flags;

	public static final void method3331(boolean bool, Frame frame, SignLink class88) {
		try {
			for (;;) {
				SignLinkRequest class143 = class88.exitFullScreen(-3470, frame);
				while ((class143.status ^ 0xffffffff) == -1) {
					TimeTools.sleep(0, 10L);
				}
				if (class143.status == 1) {
					break;
				}
				TimeTools.sleep(0, 100L);
			}
			frame.setVisible(bool);
			frame.dispose();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ri.C(" + bool + ',' + (frame != null ? "{...}" : "null") + ',' + (class88 != null ? "{...}" : "null") + ')');
		}
	}

	public static void method3332(int i) {
		try {
			if (i != 22011) {
				flags = null;
			}
			flags = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ri.B(" + i + ')');
		}
	}

	public static final Animable method3333(int i, int i_10_, int i_11_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_10_][i_11_];
		if (class172 == null) {
			return null;
		}
		Animable class246_sub3_sub2 = class172.aClass246_Sub3_Sub2_1331;
		class172.aClass246_Sub3_Sub2_1331 = null;
		PacketBufferManager.method2227(class246_sub3_sub2);
		return class246_sub3_sub2;
	}

	public boolean	aBoolean2118;
	public byte		aByte2113;
	public int		anInt2115;
	public int		anInt2116;
	public int		anInt2120;
	public int		anInt2121;
	public int		anInt2122;

	public short	aShort2114;

	public short	aShort2119;

	public short	aShort2123;

	public Class281(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, boolean bool, boolean bool_8_, int i_9_) {
		try {
			aShort2119 = (short) i_4_;
			anInt2115 = i_2_;
			anInt2122 = i_0_;
			aByte2113 = (byte) i_7_;
			aShort2114 = (short) i_5_;
			anInt2121 = i;
			aBoolean2118 = bool_8_;
			aShort2123 = (short) i_3_;
			anInt2120 = i_1_;
			anInt2116 = i_9_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ri.<init>(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + bool + ',' + bool_8_ + ',' + i_9_ + ')');
		}
	}
}

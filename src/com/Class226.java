/* Class226 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatMessageType;

public final class Class226 {
	public static int	anInt1705;
	public static int[]	anIntArray1699	= new int[3];
	public static int	mapflagId;

	public static final void method2854(boolean bool, boolean bool_0_, int i, Js5 class207, int i_1_, int i_2_, int i_3_) {
		Class76_Sub8.anInt3770 = i_1_;
		RenderAnimDefinitionParser.anInt1948 = 1;
		Class224_Sub3.anInt5037 = i;
		Class1.aBoolean66 = bool_0_;
		RtInterfaceAttachment.anInt3951 = i_3_;
		LightIntensityDefinitionParser.aClass207_2025 = class207;
		QuickChatMessageType.anInt2911 = i_2_;
		Class116.aClass98_Sub31_Sub2_965 = null;
	}

	public int	anInt1700;
	public int	anInt1701;
	public int	anInt1702;
	public int	anInt1703;

	public int	anInt1704;

	public int	anInt1707;

	public Class226() {
		/* empty */
	}
}

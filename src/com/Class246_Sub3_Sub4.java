/* Class246_Sub3_Sub4 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.crypto.ISAACPseudoRNG;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;

public abstract class Class246_Sub3_Sub4 extends Char {
	public static final Class246_Sub3_Sub4 method931(int i, int i_9_, int i_10_, Class var_class) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_9_][i_10_];
		if (class172 == null) {
			return null;
		}
		for (Class154 class154 = class172.aClass154_1325; class154 != null; class154 = class154.aClass154_1233) {
			Class246_Sub3_Sub4 class246_sub3_sub4 = class154.aClass246_Sub3_Sub4_1232;
			if (var_class.isAssignableFrom(class246_sub3_sub4.getClass()) && class246_sub3_sub4.aShort6158 == i_9_ && class246_sub3_sub4.aShort6157 == i_10_) {
				return class246_sub3_sub4;
			}
		}
		return null;
	}

	public static final OutgoingPacket prepareOutgoingPacket(int i, OutgoingOpcode opcode, ISAACPseudoRNG class117) {
		OutgoingPacket packet = Class289.method3410(i + -261);
		packet.outgoingOpcode = opcode;
		packet.packetSize = opcode.size;
		if ((packet.packetSize ^ 0xffffffff) != 0) {
			if ((packet.packetSize ^ 0xffffffff) != 1) {
				if (packet.packetSize <= 18) {
					packet.packet = new RsBitsBuffers(20);
				} else if (packet.packetSize <= 98) {
					packet.packet = new RsBitsBuffers(100);
				} else {
					packet.packet = new RsBitsBuffers(260);
				}
			} else {
				packet.packet = new RsBitsBuffers(10000);
			}
		} else {
			packet.packet = new RsBitsBuffers(260);
		}
		packet.packet.initializeCypher((byte) -125, class117);
		packet.packet.method1261(false, packet.outgoingOpcode.method2541(2));
		packet.anInt3869 = 0;
		return packet;
	}

	public boolean	aBoolean6162;
	public byte		aByte6161;
	public short	aShort6157;

	public short	aShort6158;

	public short	aShort6159;

	public short	aShort6160;

	Class246_Sub3_Sub4(int i, int i_28_, int i_29_, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_, boolean bool, byte i_36_) {
		aShort6158 = (short) i_32_;
		aBoolean6162 = bool;
		this.anInt5089 = i_30_;
		this.collisionPlane = (byte) i_28_;
		aShort6157 = (short) i_34_;
		aByte6161 = i_36_;
		aShort6160 = (short) i_33_;
		this.boundExtentsX = i_29_;
		this.boundExtentsZ = i_31_;
		aShort6159 = (short) i_35_;
		this.plane = (byte) i;
	}

	@Override
	public final boolean method2977(RSToolkit var_ha, byte i) {
		return HorizontalAlignment.method547(aShort6158, aShort6157, method2974((byte) -53, var_ha), 0, aShort6160, aShort6159, this.collisionPlane);
	}

	@Override
	public final int method2980(int i, PointLight[] class98_sub5s) {
		int i_9_ = 0;
		while_119_: for (int i_10_ = aShort6158; i_10_ <= aShort6160; i_10_++) {
			for (int i_11_ = aShort6157; i_11_ <= aShort6159; i_11_++) {
				long l = SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[this.plane][i_10_][i_11_];
				long l_12_ = 0L;
				while_118_: while (l_12_ <= 48L) {
					int i_13_ = (int) (0xffffL & l >>> (int) l_12_);
					if (i_13_ <= 0) {
						break;
					}
					Class1 class1 = Class98_Sub10_Sub31.aClass1Array5717[i_13_ + -1];
					for (int i_14_ = 0; i_9_ > i_14_; i_14_++) {
						if (class1.light == class98_sub5s[i_14_]) {
							l_12_ += 16L;
							continue while_118_;
						}
					}
					class98_sub5s[i_9_++] = class1.light;
					if (i_9_ == 4) {
						break while_119_;
					}
					l_12_ += 16L;
				}
			}
		}
		for (int i_16_ = i_9_; i_16_ < 4; i_16_++) {
			class98_sub5s[i_16_] = null;
		}
		if (aByte6161 != 0) {
			int i_17_ = aShort6158 - Class241.anInt1845;
			int i_18_ = -CharacterShadowsPreferenceField.anInt3714 + aShort6157;
			int i_19_;
			int i_20_;
			short i_21_;
			short i_22_;
			if ((aByte6161 ^ 0xffffffff) != -2) {
				if (i_18_ <= -i_17_) {
					i_21_ = aShort6158;
					i_22_ = aShort6157;
					i_20_ = 1 + aShort6157;
					i_19_ = aShort6158 + 1;
				} else {
					i_19_ = aShort6158 + -1;
					i_20_ = aShort6157 + -1;
					i_22_ = aShort6157;
					i_21_ = aShort6158;
				}
			} else if (i_17_ < i_18_) {
				i_19_ = aShort6158 + 1;
				i_20_ = -1 + aShort6157;
				i_21_ = aShort6158;
				i_22_ = aShort6157;
			} else {
				i_22_ = aShort6157;
				i_20_ = aShort6157 + 1;
				i_19_ = -1 + aShort6158;
				i_21_ = aShort6158;
			}
			int i_23_ = 0;
			while_121_: for (/**/; i_23_ < i_9_; i_23_++) {
				long l = SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[this.plane][i_21_][i_20_];
				while ((l ^ 0xffffffffffffffffL) != -1L) {
					Class1 class1 = Class98_Sub10_Sub31.aClass1Array5717[(int) ((0xffffL & l) - 1L)];
					l >>>= 16;
					if (class1.light == class98_sub5s[i_23_]) {
						continue while_121_;
					}
				}
				l = SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[this.plane][i_19_][i_22_];
				while (l != 0L) {
					Class1 class1 = Class98_Sub10_Sub31.aClass1Array5717[(int) (-1L + (l & 0xffffL))];
					l >>>= 16;
					if (class98_sub5s[i_23_] == class1.light) {
						continue while_121_;
					}
				}
				for (int i_24_ = i_23_; (i_9_ + -1 ^ 0xffffffff) < (i_24_ ^ 0xffffffff); i_24_++) {
					class98_sub5s[i_24_] = class98_sub5s[i_24_ + 1];
				}
				i_9_--;
			}
		}
		return i_9_;
	}

	@Override
	public final boolean method2991(boolean bool) {
		for (int i = aShort6158; aShort6160 >= i; i++) {
			for (int i_25_ = aShort6157; aShort6159 >= i_25_; i_25_++) {
				int i_26_ = Class259.anInt1959 + i + -Class241.anInt1845;
				if (i_26_ >= 0 && (i_26_ ^ 0xffffffff) > (Class74.aBooleanArrayArray551.length ^ 0xffffffff)) {
					int i_27_ = -CharacterShadowsPreferenceField.anInt3714 + i_25_ - -Class259.anInt1959;
					if ((i_27_ ^ 0xffffffff) <= -1 && Class74.aBooleanArrayArray551.length > i_27_ && Class74.aBooleanArrayArray551[i_26_][i_27_]) {
						return true;
					}
				}
			}
		}
		return false;
	}

	void method3022(int i) {
	}
}

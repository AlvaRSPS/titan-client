/* Class98_Sub48 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;

public final class Class98_Sub48 extends Node {
	public static int	anInt4277	= 0;
	public static int	anInt4279;
	public static int	anInt4280;
	public static int	anInt4281	= 0;

	public static final int method1660(int i) {
		try {
			if (i <= 21) {
				return 2;
			}
			return 16;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vfa.B(" + i + ')');
		}
	}

	public static final void method1661(int i, int i_0_, Class39 class39, byte i_1_, float f, float f_2_, int i_3_, float f_4_, int i_5_, int i_6_, float f_7_, byte[] is, float f_8_) {
		try {
			for (int i_9_ = 0; i_9_ < i_0_; i_9_++) {
				Class98_Sub10_Sub5_Sub1.method1018(f_4_, false, i, f_2_, i_0_, f, class39, i_6_, f_7_, i_3_, is, f_8_, i_5_, i_9_);
				i += i_5_ * i_3_;
			}
			if (i_1_ > -14) {
				anInt4281 = 28;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vfa.A(" + i + ',' + i_0_ + ',' + (class39 != null ? "{...}" : "null") + ',' + i_1_ + ',' + f + ',' + f_2_ + ',' + i_3_ + ',' + f_4_ + ',' + i_5_ + ',' + i_6_ + ',' + f_7_ + ',' + (is != null ? "{...}" : "null") + ',' + f_8_ + ')');
		}
	}

	int	anInt4278;

	int	anInt4282;

	public Class98_Sub48(int i, int i_10_) {
		try {
			anInt4278 = i;
			anInt4282 = i_10_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vfa.<init>(" + i + ',' + i_10_ + ')');
		}
	}
}

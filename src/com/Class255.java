/* Class255 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.quickchat.QuickChatInterpolantProvider;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.input.impl.AwtKeyListener;

public final class Class255 implements QuickChatInterpolantProvider {
	public static byte[][]			aByteArrayArray3211;
	public static OutgoingOpcode	aClass171_3206;
	public static RtInterface		aClass293_3208	= null;
	public static int[]				anIntArray3207;
	public static int[]				anIntArray3210	= new int[1000];
	public static String[]			ignoreNames		= new String[100];

	static {
		aClass171_3206 = new OutgoingOpcode(25, 7);
	}

	public static void method3191(byte i) {
		try {
			anIntArray3210 = null;
			ignoreNames = null;
			if (i != 49) {
				aByteArrayArray3211 = null;
			}
			aByteArrayArray3211 = null;
			anIntArray3207 = null;
			aClass171_3206 = null;
			aClass293_3208 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pv.A(" + i + ')');
		}
	}

	public static final Class119_Sub1 method3192(int i, RSByteBuffer class98_sub22) {
		try {
			if (i >= -115) {
				return null;
			}
			return new Class119_Sub1(class98_sub22.readUShort(false), class98_sub22.readUShort(false), class98_sub22.readUShort(false), class98_sub22.readUShort(false), class98_sub22.readMediumInt(-125), class98_sub22.readUnsignedByte((byte) -100));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pv.C(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	public Class255() {
		/* empty */
	}

	@Override
	public final String provide(int i, long l, QuickChatMessageType class348, int[] is) {
		try {
			if (class348 == Class151_Sub9.aClass348_5023) {
				EnumDefinition class306 = Class98_Sub10_Sub16.enumsDefinitionList.read(is[0], 1028602529);
				return class306.get((int) l, (byte) 43);
			}
			if (AwtKeyListener.aClass348_3801 == class348 || class348 == Class359.aClass348_3046) {
				ItemDefinition class297 = Class98_Sub46_Sub19.itemDefinitionList.get((int) l, (byte) -120);
				return class297.name;
			}
			if (class348 == Class42_Sub3.aClass348_5363 || class348 == Class98_Sub36.aClass348_4156 || Class151_Sub7.aClass348_5008 == class348) {
				return Class98_Sub10_Sub16.enumsDefinitionList.read(is[0], 1028602529).get((int) l, (byte) 127);
			}
			if (i != 17438) {
				method3191((byte) 34);
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pv.B(" + i + ',' + l + ',' + (class348 != null ? "{...}" : "null") + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}
}

/* Class18 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.parser.CursorDefinitionParser;

public final class Class18 {
	public static IncomingOpcode			aClass58_215	= new IncomingOpcode(1, -1);
	public static int						anInt212		= 1;
	public static CursorDefinitionParser	cursorDefinitionList;
	public static int						resultBufferSize;

	public static final String longToString(long l, int i) {
		if ((l ^ 0xffffffffffffffffL) >= -1L || l >= 6582952005840035281L) {
			return null;
		}
		if ((l % 37L ^ 0xffffffffffffffffL) == -1L) {
			return null;
		}
		int i_0_ = 0;
		for (long l_1_ = l; l_1_ != 0L; l_1_ /= 37L) {
			i_0_++;
		}
		StringBuffer stringbuffer = new StringBuffer(i_0_);
		while (l != 0L) {
			long l_2_ = l;
			l /= 37L;
			char c = Class98_Sub28.charSet[(int) (l_2_ + -(l * 37L))];
			if ((c ^ 0xffffffff) == -96) {
				int i_3_ = stringbuffer.length() - 1;
				stringbuffer.setCharAt(i_3_, Character.toUpperCase(stringbuffer.charAt(i_3_)));
				c = '\u00a0';
			}
			stringbuffer.append(c);
		}
		stringbuffer.reverse();
		stringbuffer.setCharAt(0, Character.toUpperCase(stringbuffer.charAt(0)));
		return stringbuffer.toString();
	}

	public static final void method248(boolean bool, int i, boolean bool_4_) {
		if (bool) {
			Class98_Sub10_Sub9.anInt5580--;
			if ((Class98_Sub10_Sub9.anInt5580 ^ 0xffffffff) == -1) {
				Class208.HSL_TABLE = null;
			}
		}
		if (bool_4_) {
			RtInterfaceAttachment.anInt3952--;
			if (RtInterfaceAttachment.anInt3952 == 0) {
				Class221.HSV_TABLE = null;
			}
		}
	}

	public boolean			aBoolean207;

	public boolean			aBoolean209;

	public Interface2_Impl2	anInterface2_Impl2_208;

	public Interface2_Impl2	anInterface2_Impl2_211;

	public Class18(boolean bool) {
		aBoolean209 = bool;
	}

	public final void method249(int i) {
		if (anInterface2_Impl2_208 != null) {
			anInterface2_Impl2_208.method72(false);
		}
		aBoolean207 = false;
		anInterface2_Impl2_208 = null;
	}

	public final boolean method250(byte i) {
		return !(!aBoolean207 || aBoolean209);
	}
}

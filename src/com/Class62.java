/* Class62 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.StructsDefinitionParser;
import com.jagex.game.client.preferences.FlickeringEffectsPreferenceField;

public final class Class62 {
	public static Class164					aClass164_486	= new Class164(4);
	public static int						anInt490		= 0;
	public static StructsDefinitionParser	structsDefinitionList;

	public static void method543(int i) {
		do {
			try {
				aClass164_486 = null;
				structsDefinitionList = null;
				if (i == 0) {
					break;
				}
				structsDefinitionList = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "eca.A(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method544(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_) {
		do {
			try {
				if (Class85.loadInterface(i_1_, 48)) {
					do {
						if (FlickeringEffectsPreferenceField.interfaceOverride[i_1_] == null) {
							client.method104(Class159.interfaceStore[i_1_], -1, i_10_, i_0_, i_2_, i_4_, i_5_, i_9_, i_6_, i_7_, i, i_8_);
							if (!GameShell.cleanedStatics) {
								break;
							}
						}
						client.method104(FlickeringEffectsPreferenceField.interfaceOverride[i_1_], -1, i_10_, i_0_, i_2_, i_4_, i_5_, i_9_, i_6_, i_7_, i, i_8_);
					} while (false);
					if (i_3_ == 0) {
						break;
					}
					method543(-60);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "eca.B(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + i_9_ + ',' + i_10_ + ')');
			}
			break;
		} while (false);
	}

	long	aLong485;
	int		anInt483;
	int		anInt484;

	String	aString488;

	String	aString489;

	Class62(int i, String string, int i_11_, String string_12_, long l) {
		try {
			aString488 = string_12_;
			aString489 = string;
			anInt484 = i;
			aLong485 = l;
			anInt483 = i_11_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "eca.<init>(" + i + ',' + (string != null ? "{...}" : "null") + ',' + i_11_ + ',' + (string_12_ != null ? "{...}" : "null") + ',' + l + ')');
		}
	}
}

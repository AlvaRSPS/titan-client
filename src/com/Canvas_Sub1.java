
/* Canvas_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;
import java.awt.Component;
import java.awt.Graphics;

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.toolkit.model.NativeModelRenderer;

public final class Canvas_Sub1 extends Canvas {
	public static float	aFloat25;
	public static int	anInt23;
	public static int	anInt26	= -1;

	public static final void method118(byte i) {
		try {
			if ((client.clientState ^ 0xffffffff) == -8) {
				Class98_Sub10_Sub1.handleLogout(false, false);
			} else {
				Class318.aClass123_2698 = aa_Sub1.aClass123_3561;
				aa_Sub1.aClass123_3561 = null;
				if (i == 104) {
					HashTableIterator.setClientState(13, false);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bn.A(" + i + ')');
		}
	}

	public static final void method119(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		do {
			try {
				if (i_4_ < (i_5_ ^ 0xffffffff) || i_0_ < 1 || (i_5_ ^ 0xffffffff) < (Class165.mapWidth + -2 ^ 0xffffffff) || -2 + Class98_Sub10_Sub7.mapLength < i_0_) {
					break;
				}
				int i_8_ = i_3_;
				if ((i_8_ ^ 0xffffffff) > -4 && Class1.isBridge(i_0_, (byte) -88, i_5_)) {
					i_8_++;
				}
				if ((client.preferences.aClass64_Sub3_4076.getValue((byte) 122) != 0 || RenderAnimDefinition.method3477(i_0_, i_8_, i_5_, SunDefinitionParser.anInt963, 55)) && QuickChatCategory.aClass172ArrayArrayArray5948 != null) {
					NativeModelRenderer.aClass305_Sub1_4952.method3590(i_0_, i_3_, client.graphicsToolkit, VarPlayerDefinition.clipMaps[i_3_], 1, i_1_, i_5_);
					if (i < 0) {
						break;
					}
					int i_9_ = client.preferences.groundDecoration.getValue((byte) 122);
					client.preferences.setPreference((byte) -13, 1, client.preferences.groundDecoration);
					NativeModelRenderer.aClass305_Sub1_4952.method3588(i, i_0_, i_6_, i_8_, false, i_5_, client.graphicsToolkit, i_2_, i_7_, i_3_, VarPlayerDefinition.clipMaps[i_3_]);
					client.preferences.setPreference((byte) -13, i_9_, client.preferences.groundDecoration);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "bn.C(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ')');
			}
			break;
		} while (false);
	}

	public static final aa_Sub2 method120(int i, NativeToolkit var_ha_Sub3, int i_10_, int[] is, int i_11_, int[] is_12_) {
		try {
			if (i_11_ <= 70) {
				method119(108, -86, 41, 17, 85, 52, -105, 11, 108);
			}
			if (!var_ha_Sub3.method1942(0, Class53_Sub1.aClass164_3633, Class162.aClass162_1266)) {
				int[] is_13_ = new int[i_10_ * i];
				for (int i_14_ = 0; i > i_14_; i_14_++) {
					int i_15_ = is[i_14_] + i_14_ * i_10_;
					for (int i_16_ = 0; (is_12_[i_14_] ^ 0xffffffff) < (i_16_ ^ 0xffffffff); i_16_++) {
						is_13_[i_15_++] = -16777216;
					}
				}
				return new aa_Sub2(var_ha_Sub3, i_10_, i, is_13_);
			}
			byte[] is_17_ = new byte[i_10_ * i];
			for (int i_18_ = 0; i > i_18_; i_18_++) {
				int i_19_ = i_10_ * i_18_ + is[i_18_];
				for (int i_20_ = 0; is_12_[i_18_] > i_20_; i_20_++) {
					is_17_[i_19_++] = (byte) -1;
				}
			}
			return new aa_Sub2(var_ha_Sub3, i_10_, i, is_17_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bn.B(" + i + ',' + (var_ha_Sub3 != null ? "{...}" : "null") + ',' + i_10_ + ',' + (is != null ? "{...}" : "null") + ',' + i_11_ + ',' + (is_12_ != null ? "{...}" : "null") + ')');
		}
	}

	private Component aComponent24;

	Canvas_Sub1(Component component) {
		try {
			aComponent24 = component;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bn.<init>(" + (component != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void paint(Graphics graphics) {
		try {
			aComponent24.paint(graphics);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bn.paint(" + (graphics != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void update(Graphics graphics) {
		try {
			aComponent24.update(graphics);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bn.update(" + (graphics != null ? "{...}" : "null") + ')');
		}
	}
}

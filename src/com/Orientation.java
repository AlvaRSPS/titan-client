/* Class325 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.preferences.TexturesPreferenceField;

public final class Orientation {
	public static int	anInt2729				= -1;
	public static int	CONTENT_TYPE_MINIMAP	= 1338;
	public static int[]	npcIndices				= new int[1024];

	public static final void method3696(byte i, int i_0_, int i_1_, int i_2_, String string, int i_3_, int i_4_, int i_5_) {
		Class246_Sub7 class246_sub7 = new Class246_Sub7();
		class246_sub7.anInt5118 = Queue.timer - -i_2_;
		class246_sub7.anInt5116 = i_5_;
		class246_sub7.anInt5123 = i_1_;
		class246_sub7.aString5121 = string;
		class246_sub7.anInt5117 = i_0_;
		class246_sub7.anInt5120 = i_4_;
		class246_sub7.anInt5122 = i_3_;
		TexturesPreferenceField.aClass218_3694.addLast(true, class246_sub7);
	}

	private int	step;

	int			value;

	public Orientation() {
		/* empty */
	}

	public final void normalize(int i) {
		value &= 0x3fff;
	}

	public final boolean pulse(int i, int maximumChange, int target, int minimumChange) {
		int initialStep = step;
		if (target == value && step == 0) {
			return false;
		}
		boolean reached;
		if ((step ^ 0xffffffff) == -1) {
			if (value < target && value - -minimumChange >= target || target < value && target >= value - minimumChange) {
				value = target;
				return false;
			}
			reached = true;
		} else if (step <= 0 || (value ^ 0xffffffff) <= (target ^ 0xffffffff)) {
			if ((step ^ 0xffffffff) > -1 && value > target) {
				int change = step * step / (minimumChange * 2);
				int adjusted = value + -change;
				reached = target < adjusted && adjusted <= value;
			} else {
				reached = false;
			}
		} else {
			int change = step * step / (2 * minimumChange);
			int adjusted = value + change;
			reached = (target ^ 0xffffffff) < (adjusted ^ 0xffffffff) && (value ^ 0xffffffff) >= (adjusted ^ 0xffffffff);
		}
		if (!reached) {
			if ((step ^ 0xffffffff) < -1) {
				step -= minimumChange;
				if ((step ^ 0xffffffff) > -1) {
					step = 0;
				}
			} else {
				step += minimumChange;
				if (step > 0) {
					step = 0;
				}
			}
		} else {
			if ((target ^ 0xffffffff) < (value ^ 0xffffffff)) {
				step += minimumChange;
				if (maximumChange != 0 && (maximumChange ^ 0xffffffff) > (step ^ 0xffffffff)) {
					step = maximumChange;
				}
			} else {
				step -= minimumChange;
				if (maximumChange != 0 && -maximumChange > step) {
					step = -maximumChange;
				}
			}
			if ((step ^ 0xffffffff) != (initialStep ^ 0xffffffff)) {
				int change = step * step / (minimumChange * 2);
				if (value >= target) {
					if (target < value && target > -change + value) {
						step = initialStep;
					}
				} else if ((target ^ 0xffffffff) > (change + value ^ 0xffffffff)) {
					step = initialStep;
				}
			}
		}
		value += initialStep + step >> -1761865567;
		return reached;
	}

	public final void update(boolean bool, int val) {
		value = val;
		step = 0;
	}

	public final int value(byte i) {
		return value & 0x3fff;
	}
}

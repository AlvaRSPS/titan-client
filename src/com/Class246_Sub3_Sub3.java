/* Class246_Sub3_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.Class64_Sub9;

public abstract class Class246_Sub3_Sub3 extends Char {
	public static SignLinkRequest	aClass143_6155;
	public static Class254			aClass254_6152	= new Class254(8);
	public static String			applyMenuText	= null;

	public static final boolean method3011(int i, int i_5_) {
		return !((i_5_ ^ 0xffffffff) != -8 && i_5_ != 8 && i_5_ != 9);
	}

	public static final void method3012(Mob mob, byte i) {
		do {
			if (i >= -105) {
				method3012(null, (byte) 25);
			}
			if (!(mob instanceof NPC)) {
				if (mob instanceof Player) {
					Player player = (Player) mob;
					Class98_Sub30.method1311((player.plane ^ 0xffffffff) != (Class87.localPlayer.plane ^ 0xffffffff), true, player);
				}
			} else {
				NPC npc = (NPC) mob;
				if (npc.definition == null) {
					break;
				}
				Class98_Sub10.method995(npc, (byte) 55, npc.plane != Class87.localPlayer.plane);
			}
			break;
		} while (false);
	}

	public static final int method3014(int i, int i_6_) {
		if (i_6_ != 6406) {
			if ((i_6_ ^ 0xffffffff) != -6410) {
				if (i_6_ != 32841) {
					if (i_6_ != 6410) {
						if (i_6_ == 6407) {
							return 3;
						}
						if ((i_6_ ^ 0xffffffff) == -6409) {
							return 4;
						}
					} else {
						return 2;
					}
				} else {
					return 1;
				}
			} else {
				return 1;
			}
		} else {
			return 1;
		}
		throw new IllegalArgumentException("");
	}

	public static final void method3015(int yChunk, int xChunk, byte i_8_, Class28 class28) {

		Class76.aClass28ArrayArray586[xChunk][yChunk] = class28;
		// for (Class28[] classes : Class76.aClass28ArrayArray586) {
		// System.out.println(Arrays.toString(classes));
		// }
	}

	short aShort6153;

	Class246_Sub3_Sub3(int i, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_) {
		try {
			aShort6153 = (short) i_13_;
			this.boundExtentsX = i;
			this.plane = (byte) i_11_;
			this.collisionPlane = (byte) i_12_;
			this.anInt5089 = i_9_;
			this.boundExtentsZ = i_10_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jn.<init>(" + i + ',' + i_9_ + ',' + i_10_ + ',' + i_11_ + ',' + i_12_ + ',' + i_13_ + ')');
		}
	}

	@Override
	public final boolean method2977(RSToolkit var_ha, byte i) {
		try {
			if (i != 77) {
				return false;
			}
			return Class100.method1688(this.collisionPlane, this.boundExtentsX >> Class151_Sub8.tileScale, this.boundExtentsZ >> Class151_Sub8.tileScale, this, (byte) 112);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jn.AA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2980(int i, PointLight[] class98_sub5s) {
		try {
			int i_0_ = this.boundExtentsX >> Class151_Sub8.tileScale;
			int i_1_ = this.boundExtentsZ >> Class151_Sub8.tileScale;
			int i_2_ = 0;
			if (i_0_ != Class241.anInt1845) {
				if (Class241.anInt1845 < i_0_) {
					i_2_ += 2;
				}
			} else {
				i_2_++;
			}
			if ((CharacterShadowsPreferenceField.anInt3714 ^ 0xffffffff) != (i_1_ ^ 0xffffffff)) {
				if ((i_1_ ^ 0xffffffff) > (CharacterShadowsPreferenceField.anInt3714 ^ 0xffffffff)) {
					i_2_ += 6;
				}
			} else {
				i_2_ += 3;
			}
			int i_3_ = Class241.anIntArray1846[i_2_];
			if ((aShort6153 & i_3_ ^ 0xffffffff) != -1) {
				return method2989(i_0_, false, class98_sub5s, i_1_);
			}
			if ((aShort6153 ^ 0xffffffff) == -2 && i_0_ > 0) {
				return method2989(-1 + i_0_, false, class98_sub5s, i_1_);
			}
			if ((aShort6153 ^ 0xffffffff) == -5 && i_0_ <= BConfigDefinition.anInt3112) {
				return method2989(1 + i_0_, false, class98_sub5s, i_1_);
			}
			if (aShort6153 == 8 && i_1_ > 0) {
				return method2989(i_0_, false, class98_sub5s, -1 + i_1_);
			}
			if (aShort6153 == 2 && (Class64_Sub9.anInt3662 ^ 0xffffffff) <= (i_1_ ^ 0xffffffff)) {
				return method2989(i_0_, false, class98_sub5s, 1 + i_1_);
			}
			if (aShort6153 == 16 && i_0_ > 0 && (i_1_ ^ 0xffffffff) >= (Class64_Sub9.anInt3662 ^ 0xffffffff)) {
				return method2989(-1 + i_0_, false, class98_sub5s, 1 + i_1_);
			}
			if ((aShort6153 ^ 0xffffffff) == -33 && (BConfigDefinition.anInt3112 ^ 0xffffffff) <= (i_0_ ^ 0xffffffff) && Class64_Sub9.anInt3662 >= i_1_) {
				return method2989(i_0_ + 1, false, class98_sub5s, 1 + i_1_);
			}
			if (aShort6153 == 128 && (i_0_ ^ 0xffffffff) < -1 && (i_1_ ^ 0xffffffff) < -1) {
				return method2989(i_0_ + -1, false, class98_sub5s, i_1_ - 1);
			}
			if ((aShort6153 ^ 0xffffffff) == -65 && i_0_ <= BConfigDefinition.anInt3112 && (i_1_ ^ 0xffffffff) < -1) {
				return method2989(i_0_ + 1, false, class98_sub5s, i_1_ - 1);
			}
			throw new RuntimeException("");
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jn.GA(" + i + ',' + (class98_sub5s != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final boolean method2991(boolean bool) {
		try {
			if (bool != false) {
				applyMenuText = null;
			}
			return Class74.aBooleanArrayArray551[(this.boundExtentsX >> Class151_Sub8.tileScale) + -Class241.anInt1845 - -Class259.anInt1959][-CharacterShadowsPreferenceField.anInt3714 + (this.boundExtentsZ >> Class151_Sub8.tileScale) + Class259.anInt1959];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jn.FA(" + bool + ')');
		}
	}
}


/* Class98_Sub43_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GraphicsDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.parser.ParamDefinitionParser;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;

import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;
import jagtheora.vorbis.DSPState;
import jagtheora.vorbis.VorbisBlock;
import jagtheora.vorbis.VorbisComment;
import jagtheora.vorbis.VorbisInfo;

public final class Class98_Sub43_Sub1 extends Class98_Sub43 {
	public static boolean				aBoolean5895;
	public static int[]					anIntArray5896	= { -1, 8192, 0, -1, 12288, 10240, 14336, -1, 4096, 6144, 2048 };
	public static ParamDefinitionParser	paramDefinitionList;

	static {
		aBoolean5895 = false;
	}

	public static final void method1490(int i) {
		do {
			try {
				if (client.startupStages == null) {
					client.startupStages = StartupStage.getStartupStages(-127);
					client.LOAD_JS5_INDICES = client.startupStages[0];
					JavaThreadResource.aLong1753 = TimeTools.getCurrentTime(i + 16751);
				}
				if (Class140.loadingScreenRenderer == null) {
					SunDefinition.method3238(i + 16798);
				}
				StartupStage class75 = client.LOAD_JS5_INDICES;
				if (i == -16798) {
					int i_0_ = client.processStartup(125);
					if (class75 == client.LOAD_JS5_INDICES) {
						GraphicsDefinition.aString912 = client.LOAD_JS5_INDICES.fetchingMessages.getText(client.gameLanguage, (byte) 25);
						if (client.LOAD_JS5_INDICES.gradualProgress) {
							Class195.anInt1506 = (-client.LOAD_JS5_INDICES.percentageStart + client.LOAD_JS5_INDICES.percentageStop) * i_0_ / 100 + client.LOAD_JS5_INDICES.percentageStart;
						}
						if (client.LOAD_JS5_INDICES.aBoolean554) {
							GraphicsDefinition.aString912 += Class195.anInt1506 + "%";
						}
					} else if (StartupStage.aClass75_579 == client.LOAD_JS5_INDICES) {
						Class140.loadingScreenRenderer = null;
						HashTableIterator.setClientState(3, false);
					} else {
						GraphicsDefinition.aString912 = class75.checkingMessages.getText(client.gameLanguage, (byte) 25);
						Class195.anInt1506 = class75.percentageStop;
						if (client.LOAD_JS5_INDICES.aBoolean554) {
							GraphicsDefinition.aString912 += class75.percentageStop + "%";
						}
						if (client.LOAD_JS5_INDICES.gradualProgress || class75.gradualProgress) {
							JavaThreadResource.aLong1753 = TimeTools.getCurrentTime(-47);
						}
					}
					if (Class140.loadingScreenRenderer == null) {
						break;
					}
					Class140.loadingScreenRenderer.setProgress(JavaThreadResource.aLong1753, (byte) -119, Class195.anInt1506, client.LOAD_JS5_INDICES, GraphicsDefinition.aString912);
					if (Class39_Sub1.loadingScreens == null) {
						break;
					}
					for (int i_1_ = 1 + Class21_Sub3.anInt5390; (Class39_Sub1.loadingScreens.length ^ 0xffffffff) < (i_1_ ^ 0xffffffff); i_1_++) {
						if ((Class39_Sub1.loadingScreens[i_1_].getCacheProgress(i + 16004) ^ 0xffffffff) <= -101 && -1 + i_1_ == Class21_Sub3.anInt5390 && (client.clientState ^ 0xffffffff) <= -2 && Class140.loadingScreenRenderer.isCurrentScreenActive((byte) 124)) {
							try {
								Class39_Sub1.loadingScreens[i_1_].loadMedia(-31295);
							} catch (Exception exception) {
								Class39_Sub1.loadingScreens = null;
								break;
							}
							Class140.loadingScreenRenderer.setLoadingScreen((byte) -74, Class39_Sub1.loadingScreens[i_1_]);
							Class21_Sub3.anInt5390++;
							if ((Class21_Sub3.anInt5390 ^ 0xffffffff) <= (Class39_Sub1.loadingScreens.length + -1 ^ 0xffffffff) && (Class39_Sub1.loadingScreens.length ^ 0xffffffff) < -2) {
								Class21_Sub3.anInt5390 = Class3.loadingScreenSequence.isLoaded(1) ? 0 : -1;
							}
						}
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cga.G(" + i + ')');
			}
			break;
		} while (false);
	}

	public static void method1491(int i) {
		try {
			if (i == -3) {
				anIntArray5896 = null;
				paramDefinitionList = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cga.A(" + i + ')');
		}
	}

	public static final void method1493(int i) {
		try {
			if (Class140.loadingScreenRenderer != null) {
				Class140.loadingScreenRenderer.shutDown(false);
			}
			if (Class76_Sub9.aThread3783 != null) {
				for (;;) {
					try {
						Class76_Sub9.aThread3783.join();
						break;
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cga.E(" + i + ')');
		}
	}

	private Class314			aClass314_5900;
	private Class98_Sub31_Sub4	aClass98_Sub31_Sub4_5902;
	private double				aDouble5899;
	private DSPState			aDSPState5901;

	private int					anInt5903;

	private VorbisBlock			aVorbisBlock5898;

	private VorbisComment		aVorbisComment5904;

	private VorbisInfo			aVorbisInfo5905;

	Class98_Sub43_Sub1(OggStreamState oggstreamstate) {
		super(oggstreamstate);
		try {
			aVorbisInfo5905 = new VorbisInfo();
			aVorbisComment5904 = new VorbisComment();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cga.<init>(" + (oggstreamstate != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method1482(OggPacket oggpacket, boolean bool) {
		do {
			try {
				if (bool == false) {
					if ((this.anInt4240 ^ 0xffffffff) <= -4) {
						if ((aVorbisBlock5898.synthesis(oggpacket) ^ 0xffffffff) == -1) {
							aDSPState5901.blockIn(aVorbisBlock5898);
						}
						float[][] fs = aDSPState5901.pcmOut(aVorbisInfo5905.channels);
						aDouble5899 = aDSPState5901.granuleTime();
						if (aDouble5899 == -1.0) {
							aDouble5899 = (float) anInt5903 / (float) aVorbisInfo5905.rate;
						}
						aDSPState5901.read(fs[0].length);
						anInt5903 += fs[0].length;
						Class98_Sub46_Sub15 class98_sub46_sub15 = aClass98_Sub31_Sub4_5902.method1385(aDouble5899, fs[0].length, -99);
						Class151_Sub3.method2455(fs, (byte) -78, class98_sub46_sub15.aShortArrayArray6040);
						for (int i = 0; i < aVorbisInfo5905.channels; i++) {
							class98_sub46_sub15.aShortArrayArray6040[i] = aClass314_5900.method3641(6, class98_sub46_sub15.aShortArrayArray6040[i]);
						}
						aClass98_Sub31_Sub4_5902.method1380((byte) 111, class98_sub46_sub15);
					} else {
						int i = aVorbisInfo5905.headerIn(aVorbisComment5904, oggpacket);
						if ((i ^ 0xffffffff) > -1) {
							throw new IllegalStateException(String.valueOf(i));
						}
						if ((this.anInt4240 ^ 0xffffffff) != -3) {
							break;
						}
						if ((aVorbisInfo5905.channels ^ 0xffffffff) < -3 || aVorbisInfo5905.channels < 1) {
							throw new RuntimeException(String.valueOf(aVorbisInfo5905.channels));
						}
						aDSPState5901 = new DSPState(aVorbisInfo5905);
						aVorbisBlock5898 = new VorbisBlock(aDSPState5901);
						aClass314_5900 = new Class314(aVorbisInfo5905.rate, RemoveRoofsPreferenceField.anInt3678);
						aClass98_Sub31_Sub4_5902 = new Class98_Sub31_Sub4(aVorbisInfo5905.channels);
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cga.J(" + (oggpacket != null ? "{...}" : "null") + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method1487(int i) {
		do {
			try {
				if (aVorbisBlock5898 != null) {
					aVorbisBlock5898.a();
				}
				if (i != -1128) {
					method1489(114);
				}
				if (aDSPState5901 != null) {
					aDSPState5901.a();
				}
				aVorbisComment5904.a();
				aVorbisInfo5905.a();
				if (aClass98_Sub31_Sub4_5902 == null) {
					break;
				}
				aClass98_Sub31_Sub4_5902.method1379(i ^ ~0x467);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cga.C(" + i + ')');
			}
			break;
		} while (false);
	}

	public final Class98_Sub31_Sub4 method1488(byte i) {
		try {
			if (i <= 70) {
				return null;
			}
			return aClass98_Sub31_Sub4_5902;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cga.D(" + i + ')');
		}
	}

	public final double method1489(int i) {
		try {
			if (i != 0) {
				method1492((byte) 79);
			}
			double d = aDouble5899;
			if (aClass98_Sub31_Sub4_5902 != null) {
				d = aClass98_Sub31_Sub4_5902.method1382(false);
				if (d < 0.0) {
					d = aDouble5899;
				}
			}
			return -(256.0F / RemoveRoofsPreferenceField.anInt3678) + d;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cga.F(" + i + ')');
		}
	}

	public final int method1492(byte i) {
		try {
			if (i > -14) {
				return -48;
			}
			if (aClass98_Sub31_Sub4_5902 == null) {
				return 0;
			}
			return aClass98_Sub31_Sub4_5902.method1387(true);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cga.B(" + i + ')');
		}
	}
}

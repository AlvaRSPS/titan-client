/* Class172 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class Class172 {
	public static boolean			aBoolean1321	= false;
	public static RenderTarget[]	anInterface17Array1327;

	public static final void method2542(boolean bool, RtInterface class293) {
		try {
			if (Class98_Sub10_Sub9.aBoolean5585) {
				if (class293.anObjectArray2253 != null) {
					RtInterface class293_0_ = Class246_Sub9.getDynamicComponent((byte) 72, Class187.anInt1450, Class310.anInt2652);
					if (class293_0_ != null) {
						ClientScript2Event class98_sub21 = new ClientScript2Event();
						class98_sub21.aClass293_3982 = class293_0_;
						class98_sub21.component = class293;
						class98_sub21.param = class293.anObjectArray2253;
						ClientScript2Runtime.handleEvent(class98_sub21);
					}
				}
				if (bool == false) {
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class44.aClass171_380, Class331.aClass117_2811);
					class98_sub11.packet.writeInt(1571862888, class293.idDword);
					class98_sub11.packet.writeShort(Class310.anInt2652, 1571862888);
					class98_sub11.packet.writeLEShort(class293.unknown, 17624);
					class98_sub11.packet.writeShort(class293.componentIndex, 1571862888);
					class98_sub11.packet.writeInt(1571862888, Class187.anInt1450);
					class98_sub11.packet.writeLEShortA(GlobalPlayer.anInt3173, 128);
					Class98_Sub10_Sub29.sendPacket(bool, class98_sub11);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lha.A(" + bool + ',' + (class293 != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method562(Char class246_sub3, int i, int i_6_, int i_7_, int i_8_, int i_9_) {
		boolean bool = true;
		int i_10_ = i_6_;
		int i_11_ = i_6_ + i_8_;
		int i_12_ = i_7_ - 1;
		int i_13_ = i_7_ + i_9_;
		for (int i_14_ = i; i_14_ <= i + 1; i_14_++) {
			if (i_14_ != OpenGLTexture2DSource.anInt3103) {
				for (int i_15_ = i_10_; i_15_ <= i_11_; i_15_++) {
					if (i_15_ >= 0 && i_15_ < BConfigDefinition.anInt3112) {
						for (int i_16_ = i_12_; i_16_ <= i_13_; i_16_++) {
							if (i_16_ >= 0 && i_16_ < Class64_Sub9.anInt3662 && (!bool || i_15_ >= i_11_ || i_16_ >= i_13_ || i_16_ < i_7_ && i_15_ != i_6_)) {
								Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i_14_][i_15_][i_16_];
								if (class172 != null) {
									int i_17_ = (Class78.aSArray594[i_14_].getTileHeight(i_16_, -12639, i_15_) + Class78.aSArray594[i_14_].getTileHeight(i_16_, -12639, i_15_ + 1) + Class78.aSArray594[i_14_].getTileHeight(i_16_ + 1, -12639, i_15_) + Class78.aSArray594[i_14_].getTileHeight(i_16_ + 1,
											-12639, i_15_ + 1)) / 4 - (Class78.aSArray594[i].getTileHeight(i_7_, -12639, i_6_) + Class78.aSArray594[i].getTileHeight(i_7_, -12639, i_6_ + 1) + Class78.aSArray594[i].getTileHeight(i_7_ + 1, -12639, i_6_) + Class78.aSArray594[i].getTileHeight(i_7_ + 1,
													-12639, i_6_ + 1)) / 4;
									Class246_Sub3_Sub3 class246_sub3_sub3 = class172.aClass246_Sub3_Sub3_1324;
									Class246_Sub3_Sub3 class246_sub3_sub3_18_ = class172.aClass246_Sub3_Sub3_1333;
									if (class246_sub3_sub3 != null && class246_sub3_sub3.method2982((byte) -84)) {
										class246_sub3.method2981(class246_sub3_sub3, (byte) -124, bool, (i_15_ - i_6_) * NativeShadow.anInt6333 + (1 - i_8_) * Js5.anInt1577, Class98_Sub10_Sub30.activeToolkit, i_17_, (i_16_ - i_7_) * NativeShadow.anInt6333 + (1 - i_9_) * Js5.anInt1577);
									}
									if (class246_sub3_sub3_18_ != null && class246_sub3_sub3_18_.method2982((byte) -71)) {
										class246_sub3.method2981(class246_sub3_sub3_18_, (byte) 88, bool, (i_15_ - i_6_) * NativeShadow.anInt6333 + (1 - i_8_) * Js5.anInt1577, Class98_Sub10_Sub30.activeToolkit, i_17_, (i_16_ - i_7_) * NativeShadow.anInt6333 + (1 - i_9_) * Js5.anInt1577);
									}
									for (Class154 class154 = class172.aClass154_1325; class154 != null; class154 = class154.aClass154_1233) {
										Class246_Sub3_Sub4 class246_sub3_sub4 = class154.aClass246_Sub3_Sub4_1232;
										if (class246_sub3_sub4 != null && class246_sub3_sub4.method2982((byte) -125) && (i_15_ == class246_sub3_sub4.aShort6158 || i_15_ == i_10_) && (i_16_ == class246_sub3_sub4.aShort6157 || i_16_ == i_12_)) {
											int i_19_ = class246_sub3_sub4.aShort6160 - class246_sub3_sub4.aShort6158 + 1;
											int i_20_ = class246_sub3_sub4.aShort6159 - class246_sub3_sub4.aShort6157 + 1;
											class246_sub3.method2981(class246_sub3_sub4, (byte) -126, bool, (class246_sub3_sub4.aShort6158 - i_6_) * NativeShadow.anInt6333 + (i_19_ - i_8_) * Js5.anInt1577, Class98_Sub10_Sub30.activeToolkit, i_17_, (class246_sub3_sub4.aShort6157 - i_7_)
													* NativeShadow.anInt6333 + (i_20_ - i_9_) * Js5.anInt1577);
										}
									}
								}
							}
						}
					}
				}
				i_10_--;
				bool = false;
			}
		}
	}

	byte						aByte1322;
	public Class154				aClass154_1325;
	public Class172				aClass172_1330;
	Class246_Sub3_Sub1			aClass246_Sub3_Sub1_1332;
	public Animable				aClass246_Sub3_Sub2_1331;
	public Class246_Sub3_Sub3	aClass246_Sub3_Sub3_1324;
	public Class246_Sub3_Sub3	aClass246_Sub3_Sub3_1333;
	Class246_Sub3_Sub5			aClass246_Sub3_Sub5_1326;
	Class246_Sub3_Sub5			aClass246_Sub3_Sub5_1334;
	public short				aShort1323;

	public short				aShort1328;

	public short				aShort1329;

	public short				aShort1335;

	public Class172(int i) {
		aByte1322 = (byte) i;
	}

	public final void method2544(int i) {
		try {
			Class154 class154;
			for (/**/; aClass154_1325 != null; aClass154_1325 = class154) {
				class154 = aClass154_1325.aClass154_1233;
				aClass154_1325.method2491(2);
			}
			if (i != 6730) {
				method2542(false, null);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lha.B(" + i + ')');
		}
	}
}


/* Class247 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.Calendar;
import java.util.Date;

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.MemoryCacheNode;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.clanchat.ClanChatMember;
import com.jagex.game.client.archive.FileRequest;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Archive;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5FileRequest;
import com.jagex.game.client.archive.Js5Manager;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.CursorDefinition;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.IdentikitDefinition;
import com.jagex.game.client.definition.InventoriesDefinition;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.VarClientDefinition;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.definition.parser.AnimationDefinitionParser;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.definition.parser.NPCDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.definition.parser.StructsDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientDefinitionParser;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;
import com.jagex.game.client.preferences.BrightnessPreferenceField;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.Class64_Sub3;
import com.jagex.game.client.preferences.CpuUsagePreferenceField;
import com.jagex.game.client.preferences.FlickeringEffectsPreferenceField;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;
import com.jagex.game.client.preferences.IntegerPreferenceField;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.preferences.SceneryShadowsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.quickchat.QuickChatMessage;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.layout.VerticalAlignment;
import com.jagex.game.client.ui.loading.LoadingScreenRenderer;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SimpleProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.SpriteProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.TextLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.DecoratedProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.RotatingSpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.ScalingSpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsItem;
import com.jagex.game.constants.BuildType;
import com.jagex.game.input.RtKeyListener;
import com.jagex.game.input.impl.AwtKeyListener;
import com.jagex.game.input.impl.AwtMouseEvent;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.model.NativeModelRenderer;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class ClientScript2Runtime {
	private static Calendar				aCalendar1882;
	private static RtInterface			aClass293_1877;
	private static RtInterface			aClass293_1879;
	private static QuickChat			quickChat;
	private static Class349[]			aClass349Array1889;
	public static AdvancedMemoryCache	aClass79_1890;
	public static int					anInt1880;
	private static int					anInt1888		= 0;
	private static int					anInt1893;
	private static int[]				anIntArray1875;
	private static int[]				anIntArray1887;
	private static int[]				anIntArray1891;
	private static String[]				aStringArray1886;
	private static String[]				aStringArray1892;
	private static int[][]				integerArrays	= new int[5][5000];
	public static int[]					integerStack	= new int[1000];
	private static int					integerStackPtr;
	private static String[]				stringStack		= new String[1000];
	private static int					stringStackPtr	= 0;

	static {
		anIntArray1887 = new int[5];
		integerStackPtr = 0;
		aClass349Array1889 = new Class349[50];
		aCalendar1882 = Calendar.getInstance();
		aStringArray1892 = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
		anIntArray1891 = new int[3];
		aClass79_1890 = new AdvancedMemoryCache(4);
		anInt1893 = 0;
	}

	private static final void handleChatEffects(String string, int i) {
		if (LoadingScreenSequence.rights != 0 || (!Js5Manager.aBoolean933 || Class98_Sub10_Sub35.aBoolean5732) && !BaseModel.isQuickChatWorld) {
			String message = string.toLowerCase();
			int colourEffect = 0;
			if (message.startsWith(TextResources.YELLOW.getText(0, (byte) 25))) {
				colourEffect = 0;
				string = string.substring(TextResources.YELLOW.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.RED.getText(0, (byte) 25))) {
				colourEffect = 1;
				string = string.substring(TextResources.RED.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.GREEN.getText(0, (byte) 25))) {
				colourEffect = 2;
				string = string.substring(TextResources.GREEN.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.CYAN.getText(0, (byte) 25))) {
				colourEffect = 3;
				string = string.substring(TextResources.CYAN.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.PURPLE.getText(0, (byte) 25))) {
				colourEffect = 4;
				string = string.substring(TextResources.PURPLE.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.WHITE.getText(0, (byte) 25))) {
				colourEffect = 5;
				string = string.substring(TextResources.WHITE.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.FLASH1.getText(0, (byte) 25))) {
				colourEffect = 6;
				string = string.substring(TextResources.FLASH1.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.FLASH2.getText(0, (byte) 25))) {
				colourEffect = 7;
				string = string.substring(TextResources.FLASH2.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.FLASH3.getText(0, (byte) 25))) {
				colourEffect = 8;
				string = string.substring(TextResources.FLASH3.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.GLOW1.getText(0, (byte) 25))) {
				colourEffect = 9;
				string = string.substring(TextResources.GLOW1.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.GLOW2.getText(0, (byte) 25))) {
				colourEffect = 10;
				string = string.substring(TextResources.GLOW2.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.GLOW3.getText(0, (byte) 25))) {
				colourEffect = 11;
				string = string.substring(TextResources.GLOW3.getText(0, (byte) 25).length());
			} else if (client.gameLanguage != 0) {
				if (message.startsWith(TextResources.YELLOW.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 0;
					string = string.substring(TextResources.YELLOW.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.RED.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 1;
					string = string.substring(TextResources.RED.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.GREEN.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 2;
					string = string.substring(TextResources.GREEN.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.CYAN.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 3;
					string = string.substring(TextResources.CYAN.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.PURPLE.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 4;
					string = string.substring(TextResources.PURPLE.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.WHITE.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 5;
					string = string.substring(TextResources.WHITE.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.FLASH1.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 6;
					string = string.substring(TextResources.FLASH1.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.FLASH2.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 7;
					string = string.substring(TextResources.FLASH2.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.FLASH3.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 8;
					string = string.substring(TextResources.FLASH3.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.GLOW1.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 9;
					string = string.substring(TextResources.GLOW1.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.GLOW2.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 10;
					string = string.substring(TextResources.GLOW2.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.GLOW3.getText(client.gameLanguage, (byte) 25))) {
					colourEffect = 11;
					string = string.substring(TextResources.GLOW3.getText(client.gameLanguage, (byte) 25).length());
				}
			}
			message = string.toLowerCase();
			int textAnimation = 0;
			if (message.startsWith(TextResources.WAVE.getText(0, (byte) 25))) {
				textAnimation = 1;
				string = string.substring(TextResources.WAVE.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.WAVE2.getText(0, (byte) 25))) {
				textAnimation = 2;
				string = string.substring(TextResources.WAVE2.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.SHAKE.getText(0, (byte) 25))) {
				textAnimation = 3;
				string = string.substring(TextResources.SHAKE.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.SCROLL.getText(0, (byte) 25))) {
				textAnimation = 4;
				string = string.substring(TextResources.SCROLL.getText(0, (byte) 25).length());
			} else if (message.startsWith(TextResources.SLIDE.getText(0, (byte) 25))) {
				textAnimation = 5;
				string = string.substring(TextResources.SLIDE.getText(0, (byte) 25).length());
			} else if (client.gameLanguage != 0) {
				if (message.startsWith(TextResources.WAVE.getText(client.gameLanguage, (byte) 25))) {
					textAnimation = 1;
					string = string.substring(TextResources.WAVE.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.WAVE2.getText(client.gameLanguage, (byte) 25))) {
					textAnimation = 2;
					string = string.substring(TextResources.WAVE2.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.SHAKE.getText(client.gameLanguage, (byte) 25))) {
					textAnimation = 3;
					string = string.substring(TextResources.SHAKE.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.SCROLL.getText(client.gameLanguage, (byte) 25))) {
					textAnimation = 4;
					string = string.substring(TextResources.SCROLL.getText(client.gameLanguage, (byte) 25).length());
				} else if (message.startsWith(TextResources.SLIDE.getText(client.gameLanguage, (byte) 25))) {
					textAnimation = 5;
					string = string.substring(TextResources.SLIDE.getText(client.gameLanguage, (byte) 25).length());
				}
			}
			OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class87.CHAT_OUTGOING_PACKET, Class331.aClass117_2811);
			frame.packet.writeByte(0, 70);
			int packetPosition = frame.packet.position;
			frame.packet.writeByte(colourEffect, 122);
			frame.packet.writeByte(textAnimation, -54);
			Class284_Sub1_Sub1.method3368(127, string, frame.packet);
			frame.packet.method1211((byte) 84, frame.packet.position - packetPosition);
			Class98_Sub10_Sub29.sendPacket(false, frame);
		}
	}

	private static final void handleClientButtons(int buttonId, boolean bool) {
		if (buttonId < 5100) {
			if (buttonId == 5000) {
				integerStack[integerStackPtr++] = Class265.friendsChatStatus;
				return;
			}
			if (buttonId == 5001) {
				integerStackPtr -= 3;
				Class265.friendsChatStatus = integerStack[integerStackPtr];
				HitmarksDefinition.chatStatus = Class98_Sub10_Sub8.setChatStatus((byte) -107, integerStack[integerStackPtr + 1]);
				if (HitmarksDefinition.chatStatus == null) {
					HitmarksDefinition.chatStatus = GamePreferences.FRIENDS;
				}
				Node.tradeStatus = integerStack[integerStackPtr + 2];
				OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, AwtMouseEvent.CHANGE_CHAT_STATUS, Class331.aClass117_2811);
				frame.packet.writeByte(Class265.friendsChatStatus, -77);
				frame.packet.writeByte(HitmarksDefinition.chatStatus.privateChatStatus, -57);
				frame.packet.writeByte(Node.tradeStatus, 120);
				Class98_Sub10_Sub29.sendPacket(false, frame);
				return;
			}
			if (buttonId == 5002) {
				stringStackPtr -= 2;
				String username = stringStack[stringStackPtr];
				String reportMessage = stringStack[stringStackPtr + 1];
				integerStackPtr -= 2;
				int i_293_ = integerStack[integerStackPtr];
				int i_294_ = integerStack[integerStackPtr + 1];
				if (reportMessage == null) {
					reportMessage = "";
				}
				if (reportMessage.length() > 80) {
					reportMessage = reportMessage.substring(0, 80);
				}
				OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, SceneryShadowsPreferenceField.SEND_REPORT_PLAYER, Class331.aClass117_2811);
				frame.packet.writeByte(NativeShadow.encodedStringLength(username, (byte) 109) + 2 + NativeShadow.encodedStringLength(reportMessage, (byte) 110), 52);
				frame.packet.writePJStr1(username, (byte) 113);
				frame.packet.writeByte(i_293_ - 1, -91);
				frame.packet.writeByte(i_294_, 108);
				frame.packet.writePJStr1(reportMessage, (byte) 113);
				Class98_Sub10_Sub29.sendPacket(false, frame);
				return;
			}
			if (buttonId == 5003) {
				int messageCount = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageCount);
				String chatMessage = "";
				if (message != null && message.chatboxMessage != null) {
					chatMessage = message.chatboxMessage;
				}
				stringStack[stringStackPtr++] = chatMessage;
				return;
			}
			if (buttonId == 5004) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				int type = -1;
				if (message != null) {
					type = message.messageType;
				}
				integerStack[integerStackPtr++] = type;
				return;
			}
			if (buttonId == 5005) {
				if (HitmarksDefinition.chatStatus == null) {
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = HitmarksDefinition.chatStatus.privateChatStatus;
					return;
				}
				return;
			}
			if (buttonId == 5006) {
				int chatType = integerStack[--integerStackPtr];
				OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, NativeModelRenderer.CHAT_TYPE, Class331.aClass117_2811);
				frame.packet.writeByte(chatType, 55);
				Class98_Sub10_Sub29.sendPacket(false, frame);
				return;
			}
			if (buttonId == 5008) {
				String chatMessage = stringStack[--stringStackPtr];
				handleChatEffects(chatMessage, buttonId);
				return;
			}
			if (buttonId == 5009) {
				stringStackPtr -= 2;
				String target = stringStack[stringStackPtr];
				String message = stringStack[stringStackPtr + 1];
				if (LoadingScreenSequence.rights != 0 || (!Js5Manager.aBoolean933 || Class98_Sub10_Sub35.aBoolean5732) && !BaseModel.isQuickChatWorld) {
					OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class246_Sub4_Sub2.SEND_PRIVATE_MESSAGE, Class331.aClass117_2811);
					frame.packet.writeByte(0, -83);
					int position = frame.packet.position;
					frame.packet.writePJStr1(target, (byte) 113);
					Class284_Sub1_Sub1.method3368(127, message, frame.packet);
					frame.packet.method1211((byte) 98, frame.packet.position - position);
					Class98_Sub10_Sub29.sendPacket(false, frame);
					return;
				}
				return;
			}
			if (buttonId == 5010) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				String fullName = "";
				if (message != null && message.displayName != null) {
					fullName = message.displayName;
				}
				stringStack[stringStackPtr++] = fullName;
				return;
			}
			if (buttonId == 5011) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				String chatName = "";
				if (message != null && message.clanChatName != null) {
					chatName = message.clanChatName;
				}
				stringStack[stringStackPtr++] = chatName;
				return;
			}
			if (buttonId == 5012) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				int fileId = -1;
				if (message != null) {
					fileId = message.fileIndex;
				}
				integerStack[integerStackPtr++] = fileId;
				return;
			}
			if (buttonId == 5015) {
				String name;
				if (Class87.localPlayer != null && Class87.localPlayer.displayName != null) {
					name = Class87.localPlayer.formattedName(0, true);
				} else {
					name = "";
				}
				stringStack[stringStackPtr++] = name;
				return;
			}
			if (buttonId == 5016) {
				integerStack[integerStackPtr++] = Node.tradeStatus;
				return;
			}
			if (buttonId == 5017) {
				integerStack[integerStackPtr++] = Class98_Sub10_Sub27.getTotalChatLines((byte) -4);
				return;
			}
			if (buttonId == 5018) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				int hash = 0;
				if (message != null) {
					hash = message.flags;
				}
				integerStack[integerStackPtr++] = hash;
				return;
			}
			if (buttonId == 5019) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				String nameAndRights = "";
				if (message != null && message.nameUnfiltered != null) {
					nameAndRights = message.nameUnfiltered;
				}
				stringStack[stringStackPtr++] = nameAndRights;
				return;
			}
			if (buttonId == 5020) {
				String displayName;
				if (Class87.localPlayer != null && Class87.localPlayer.displayName != null) {
					displayName = Class87.localPlayer.getName(-1, false);
				} else {
					displayName = "";
				}
				stringStack[stringStackPtr++] = displayName;
				return;
			}
			if (buttonId == 5023) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				int unknown = -1;
				if (message != null) {
					unknown = message.somethingClanChatRelated;
				}
				integerStack[integerStackPtr++] = unknown;
				return;
			}
			if (buttonId == 5024) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				int unknown = -1;
				if (message != null) {
					unknown = message.anInt1039;
				}
				integerStack[integerStackPtr++] = unknown;
				return;
			}
			if (buttonId == 5025) {
				int messageIndex = integerStack[--integerStackPtr];
				ChatLine message = Class138.getChatMessageByIndex((byte) 49, messageIndex);
				String playerName = "";
				if (message != null && message.nameSimple != null) {
					playerName = message.nameSimple;
				}
				stringStack[stringStackPtr++] = playerName;
				return;
			}
			if (buttonId == 5050) {
				int quickChatCategory = integerStack[--integerStackPtr];
				stringStack[stringStackPtr++] = SceneGraphNodeList.quickChatCategoryList.get(28559, quickChatCategory).name;
				return;
			}
			if (buttonId == 5051) {
				int quickChatCategory = integerStack[--integerStackPtr];
				QuickChatCategory qcCategory = SceneGraphNodeList.quickChatCategoryList.get(28559, quickChatCategory);
				if (qcCategory.subcategories == null) {
					integerStack[integerStackPtr++] = 0;
				} else {
					integerStack[integerStackPtr++] = qcCategory.subcategories.length;
					return;
				}
				return;
			}
			if (buttonId == 5052) {
				integerStackPtr -= 2;
				int cat = integerStack[integerStackPtr];
				int sub = integerStack[integerStackPtr + 1];
				QuickChatCategory qcCategory = SceneGraphNodeList.quickChatCategoryList.get(28559, cat);
				int qcMessageIndex = qcCategory.subcategories[sub];
				integerStack[integerStackPtr++] = qcMessageIndex;
				return;
			}
			if (buttonId == 5053) {
				int cat = integerStack[--integerStackPtr];
				QuickChatCategory qcCategory = SceneGraphNodeList.quickChatCategoryList.get(28559, cat);
				if (qcCategory.messages == null) {
					integerStack[integerStackPtr++] = 0;
				} else {
					integerStack[integerStackPtr++] = qcCategory.messages.length;
					return;
				}
				return;
			}
			if (buttonId == 5054) {
				integerStackPtr -= 2;
				int cat = integerStack[integerStackPtr];
				int messageIndex = integerStack[integerStackPtr + 1];
				integerStack[integerStackPtr++] = SceneGraphNodeList.quickChatCategoryList.get(28559, cat).messages[messageIndex];
				return;
			}
			if (buttonId == 5055) {
				int cat = integerStack[--integerStackPtr];
				stringStack[stringStackPtr++] = NewsLSEConfig.quickChatMessageList.get(cat, 51).getMessage(false);
				return;
			}
			if (buttonId == 5056) {
				int cat = integerStack[--integerStackPtr];
				QuickChatMessage qcMessage = NewsLSEConfig.quickChatMessageList.get(cat, 53);
				if (qcMessage.responses == null) {
					integerStack[integerStackPtr++] = 0;
				} else {
					integerStack[integerStackPtr++] = qcMessage.responses.length;
					return;
				}
				return;
			}
			if (buttonId == 5057) {
				integerStackPtr -= 2;
				int cat = integerStack[integerStackPtr];
				int responseIndex = integerStack[integerStackPtr + 1];
				integerStack[integerStackPtr++] = NewsLSEConfig.quickChatMessageList.get(cat, 74).responses[responseIndex];
				return;
			}
			if (buttonId == 5058) {
				quickChat = new QuickChat();
				quickChat.messageId = integerStack[--integerStackPtr];
				quickChat.message = NewsLSEConfig.quickChatMessageList.get(quickChat.messageId, 98);
				quickChat.messages = new int[quickChat.message.fillerCount((byte) -111)];
				return;
			}
			if (buttonId == 5059) {
				anInt1880++;
				OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class246_Sub3_Sub1.SEND_QUICK_CHAT_MESSAGE, Class331.aClass117_2811);
				frame.packet.writeByte(0, 59);
				int position = frame.packet.position;
				frame.packet.writeByte(0, 72);
				frame.packet.writeShort(quickChat.messageId, 1571862888);
				quickChat.message.encode(frame.packet, quickChat.messages, -3);
				frame.packet.method1211((byte) 98, frame.packet.position - position);
				Class98_Sub10_Sub29.sendPacket(false, frame);
				return;
			}
			if (buttonId == 5060) {
				String string = stringStack[--stringStackPtr];
				OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, AwtMouseEvent.SEND_PRIVATE_QUICK_CHAT_MESSAGE, Class331.aClass117_2811);
				frame.packet.writeByte(0, -109);
				int position = frame.packet.position;
				frame.packet.writePJStr1(string, (byte) 113);
				frame.packet.writeShort(quickChat.messageId, 1571862888);
				quickChat.message.encode(frame.packet, quickChat.messages, -3);
				frame.packet.method1211((byte) 115, frame.packet.position - position);
				Class98_Sub10_Sub29.sendPacket(false, frame);
				return;
			}
			if (buttonId == 5061) {
				anInt1880++;
				OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class246_Sub3_Sub1.SEND_QUICK_CHAT_MESSAGE, Class331.aClass117_2811);
				frame.packet.writeByte(0, 91);
				int position = frame.packet.position;
				frame.packet.writeByte(1, -118);
				frame.packet.writeShort(quickChat.messageId, 1571862888);
				quickChat.message.encode(frame.packet, quickChat.messages, -3);
				frame.packet.method1211((byte) 118, frame.packet.position - position);
				Class98_Sub10_Sub29.sendPacket(false, frame);
				return;
			}
			if (buttonId == 5062) {
				integerStackPtr -= 2;
				int cat = integerStack[integerStackPtr];
				int hotkey = integerStack[integerStackPtr + 1];
				integerStack[integerStackPtr++] = SceneGraphNodeList.quickChatCategoryList.get(28559, cat).subcategoryHotkeys[hotkey];
				return;
			}
			if (buttonId == 5063) {
				integerStackPtr -= 2;
				int messageId = integerStack[integerStackPtr];
				int hotkey = integerStack[integerStackPtr + 1];
				integerStack[integerStackPtr++] = SceneGraphNodeList.quickChatCategoryList.get(28559, messageId).messageHotkeys[hotkey];
				return;
			}
			if (buttonId == 5064) {
				integerStackPtr -= 2;
				int cat = integerStack[integerStackPtr];
				int subCat = integerStack[integerStackPtr + 1];
				if (subCat == -1) {
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = SceneGraphNodeList.quickChatCategoryList.get(28559, cat).getSubcategory(-1, (char) subCat);
					return;
				}
				return;
			}
			if (buttonId == 5065) {
				integerStackPtr -= 2;
				int cat = integerStack[integerStackPtr];
				int messageId = integerStack[integerStackPtr + 1];
				if (messageId == -1) {
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = SceneGraphNodeList.quickChatCategoryList.get(28559, cat).getMessage((char) messageId, (byte) -126);
					return;
				}
				return;
			}
			if (buttonId == 5066) {
				int messageIndex = integerStack[--integerStackPtr];
				integerStack[integerStackPtr++] = NewsLSEConfig.quickChatMessageList.get(messageIndex, 109).fillerCount((byte) -109);
				return;
			}
			if (buttonId == 5067) {
				integerStackPtr -= 2;
				int messageId = integerStack[integerStackPtr];
				int type = integerStack[integerStackPtr + 1];
				int qcMessageIndex = NewsLSEConfig.quickChatMessageList.get(messageId, 67).getType(type, 78).id;
				integerStack[integerStackPtr++] = qcMessageIndex;
				return;
			}
			if (buttonId == 5068) {
				integerStackPtr -= 2;
				int message = integerStack[integerStackPtr];
				int i_341_ = integerStack[integerStackPtr + 1];
				quickChat.messages[message] = i_341_;
				return;
			}
			if (buttonId == 5069) {
				integerStackPtr -= 2;
				int message = integerStack[integerStackPtr];
				int messageIndex = integerStack[integerStackPtr + 1];
				quickChat.messages[message] = messageIndex;
				return;
			}
			if (buttonId == 5070) {
				integerStackPtr -= 3;
				int cat = integerStack[integerStackPtr];
				int type = integerStack[integerStackPtr + 1];
				int i_346_ = integerStack[integerStackPtr + 2];
				QuickChatMessage qcMessage = NewsLSEConfig.quickChatMessageList.get(cat, 65);
				if (qcMessage.getType(type, -126).id != 0) {
					throw new RuntimeException("bad command");
				}
				integerStack[integerStackPtr++] = qcMessage.getConfig(121, i_346_, type);
				return;
			}
			if (buttonId == 5071) {
				String searchMessage = stringStack[--stringStackPtr];
				boolean restricted = integerStack[--integerStackPtr] == 1;
				Js5.searchQuickChatMessage(restricted, searchMessage, (byte) 69);
				integerStack[integerStackPtr++] = Class18.resultBufferSize;
				return;
			}
			if (buttonId == 5072) {
				if (MaxScreenSizePreferenceField.resultIndexBuffer == null || Class85.resultBufferPtr >= Class18.resultBufferSize) {
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = MaxScreenSizePreferenceField.resultIndexBuffer[Class85.resultBufferPtr++] & 0xffff;
					return;
				}
				return;
			}
			if (buttonId == 5073) {
				Class85.resultBufferPtr = 0;
				return;
			}
		} else if (buttonId < 5200) {
			if (buttonId == 5100) {
				if (client.keyListener.isKeyDown(86, 5503)) {
					System.out.println("Key down!!!!");
					integerStack[integerStackPtr++] = 1;
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
			if (buttonId == 5101) {
				if (client.keyListener.isKeyDown(82, 5503)) {
					System.out.println("Key down!!!!");
					integerStack[integerStackPtr++] = 1;
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
			if (buttonId == 5102) {
				if (client.keyListener.isKeyDown(81, 5503)) {
					System.out.println("Key down!!!!");
					integerStack[integerStackPtr++] = 1;
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
		} else if (buttonId < 5300) {// TODO: from here refactor buttons
			if (buttonId == 5200) {
				MemoryCacheNode.setWorldMapZoom(integerStack[--integerStackPtr], false);
				return;
			}
			if (buttonId == 5201) {
				integerStack[integerStackPtr++] = IntegerPreferenceField.getWorldMapZoom(1024);
				return;
			}
			if (buttonId == 5205) {
				Class119.method2176(-1, false, integerStack[--integerStackPtr], (byte) 89, -1);
				return;
			}
			if (buttonId == 5206) {
				int i_348_ = integerStack[--integerStackPtr];
				Class98_Sub46_Sub10 class98_sub46_sub10 = WorldMap.method3303(i_348_ >> 14 & 0x3fff, i_348_ & 0x3fff);
				if (class98_sub46_sub10 == null) {
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = class98_sub46_sub10.anInt6014;
					return;
				}
				return;
			}
			if (buttonId == 5207) {
				Class98_Sub46_Sub10 class98_sub46_sub10 = WorldMap.method3306(integerStack[--integerStackPtr]);
				if (class98_sub46_sub10 == null || class98_sub46_sub10.optionBoxMapName == null) {
					stringStack[stringStackPtr++] = "";
				} else {
					stringStack[stringStackPtr++] = class98_sub46_sub10.optionBoxMapName;
					return;
				}
				return;
			}
			if (buttonId == 5208) {
				integerStack[integerStackPtr++] = RtInterfaceClip.anInt48;
				integerStack[integerStackPtr++] = Class246_Sub3_Sub5_Sub2.anInt6268;
				return;
			}
			if (buttonId == 5209) {
				integerStack[integerStackPtr++] = Class42_Sub4.anInt5371 + WorldMap.anInt2075;
				integerStack[integerStackPtr++] = NodeShort.anInt4197 + WorldMap.anInt2078;
				return;
			}
			if (buttonId == 5210) {
				int i_349_ = integerStack[--integerStackPtr];
				Class98_Sub46_Sub10 class98_sub46_sub10 = WorldMap.method3306(i_349_);
				if (class98_sub46_sub10 == null) {
					integerStack[integerStackPtr++] = 0;
					integerStack[integerStackPtr++] = 0;
				} else {
					System.out.println("LALALALALALA: " + class98_sub46_sub10.anInt6006);

					integerStack[integerStackPtr++] = class98_sub46_sub10.anInt6006 >> 14 & 0x3fff;
					integerStack[integerStackPtr++] = class98_sub46_sub10.anInt6006 & 0x3fff;
					return;
				}
				return;
			}
			if (buttonId == 5211) {
				int i_350_ = integerStack[--integerStackPtr];
				Class98_Sub46_Sub10 class98_sub46_sub10 = WorldMap.method3306(i_350_);
				if (class98_sub46_sub10 == null) {
					integerStack[integerStackPtr++] = 0;
					integerStack[integerStackPtr++] = 0;
				} else {
					integerStack[integerStackPtr++] = class98_sub46_sub10.anInt6016 - class98_sub46_sub10.anInt6008;
					integerStack[integerStackPtr++] = class98_sub46_sub10.anInt6023 - class98_sub46_sub10.anInt6009;
					return;
				}
				return;
			}
			if (buttonId == 5212) {
				Class98_Sub47 class98_sub47 = Class256_Sub1.method3196((byte) -99);
				if (class98_sub47 == null) {
					integerStack[integerStackPtr++] = -1;
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = class98_sub47.anInt4268;
					int i_351_ = class98_sub47.anInt4269 << 28 | class98_sub47.anInt4272 + WorldMap.anInt2075 << 14 | class98_sub47.anInt4267 + WorldMap.anInt2078;
					integerStack[integerStackPtr++] = i_351_;
					return;
				}
				return;
			}
			if (buttonId == 5213) {
				Class98_Sub47 class98_sub47 = Char.method2979(-105);
				if (class98_sub47 == null) {
					integerStack[integerStackPtr++] = -1;
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = class98_sub47.anInt4268;
					int i_352_ = class98_sub47.anInt4269 << 28 | class98_sub47.anInt4272 + WorldMap.anInt2075 << 14 | class98_sub47.anInt4267 + WorldMap.anInt2078;
					integerStack[integerStackPtr++] = i_352_;
					return;
				}
				return;
			}
			if (buttonId == 5214) {
				int i_353_ = integerStack[--integerStackPtr];
				Class98_Sub46_Sub10 class98_sub46_sub10 = Class98_Sub10_Sub8.method1026(-3);
				if (class98_sub46_sub10 != null) {
					boolean bool_354_ = class98_sub46_sub10.method1573(i_353_ >> 28 & 0x3, anIntArray1891, -90, i_353_ & 0x3fff, i_353_ >> 14 & 0x3fff);
					if (bool_354_) {
						Class246_Sub2.method2971(anIntArray1891[2], (byte) 39, anIntArray1891[1]);
					}
				}
				return;
			}
			if (buttonId == 5215) {
				integerStackPtr -= 2;
				int i_355_ = integerStack[integerStackPtr];
				int i_356_ = integerStack[integerStackPtr + 1];
				Queue queue = WorldMap.method3296(i_355_ >> 14 & 0x3fff, i_355_ & 0x3fff);
				boolean bool_357_ = false;
				for (Class98_Sub46_Sub10 class98_sub46_sub10 = (Class98_Sub46_Sub10) queue.getFirst(-1); class98_sub46_sub10 != null; class98_sub46_sub10 = (Class98_Sub46_Sub10) queue.getNext(0)) {
					if (class98_sub46_sub10.anInt6014 == i_356_) {
						bool_357_ = true;
						break;
					}
				}
				if (bool_357_) {
					integerStack[integerStackPtr++] = 1;
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
			if (buttonId == 5218) {
				int i_358_ = integerStack[--integerStackPtr];
				Class98_Sub46_Sub10 class98_sub46_sub10 = WorldMap.method3306(i_358_);
				if (class98_sub46_sub10 == null) {
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = class98_sub46_sub10.anInt6007;
					return;
				}
				return;
			}
			if (buttonId == 5220) {
				integerStack[integerStackPtr++] = QuickChatCategoryParser.loadingProgress == 100 ? 1 : 0;
				return;
			}
			if (buttonId == 5221) {
				int i_359_ = integerStack[--integerStackPtr];
				Class246_Sub2.method2971(i_359_ & 0x3fff, (byte) 100, i_359_ >> 14 & 0x3fff);
				return;
			}
			if (buttonId == 5222) {
				Class98_Sub46_Sub10 class98_sub46_sub10 = Class98_Sub10_Sub8.method1026(-3);
				if (class98_sub46_sub10 != null) {
					boolean bool_360_ = class98_sub46_sub10.method1563(31960, Class42_Sub4.anInt5371 + WorldMap.anInt2075, anIntArray1891, NodeShort.anInt4197 + WorldMap.anInt2078);
					if (bool_360_) {
						integerStack[integerStackPtr++] = anIntArray1891[1];
						integerStack[integerStackPtr++] = anIntArray1891[2];
					} else {
						integerStack[integerStackPtr++] = -1;
						integerStack[integerStackPtr++] = -1;
					}
				} else {
					integerStack[integerStackPtr++] = -1;
					integerStack[integerStackPtr++] = -1;
					return;
				}
				return;
			}
			if (buttonId == 5223) {
				integerStackPtr -= 2;
				int i_361_ = integerStack[integerStackPtr];
				int i_362_ = integerStack[integerStackPtr + 1];
				Class119.method2176(i_362_ >> 14 & 0x3fff, false, i_361_, (byte) 89, i_362_ & 0x3fff);
				return;
			}
			if (buttonId == 5224) {
				int i_363_ = integerStack[--integerStackPtr];
				Class98_Sub46_Sub10 class98_sub46_sub10 = Class98_Sub10_Sub8.method1026(-3);
				if (class98_sub46_sub10 != null) {
					boolean bool_364_ = class98_sub46_sub10.method1573(i_363_ >> 28 & 0x3, anIntArray1891, -105, i_363_ & 0x3fff, i_363_ >> 14 & 0x3fff);
					if (bool_364_) {
						integerStack[integerStackPtr++] = anIntArray1891[1];
						integerStack[integerStackPtr++] = anIntArray1891[2];
					} else {
						integerStack[integerStackPtr++] = -1;
						integerStack[integerStackPtr++] = -1;
					}
				} else {
					integerStack[integerStackPtr++] = -1;
					integerStack[integerStackPtr++] = -1;
					return;
				}
				return;
			}
			if (buttonId == 5225) {
				int i_365_ = integerStack[--integerStackPtr];
				Class98_Sub46_Sub10 class98_sub46_sub10 = Class98_Sub10_Sub8.method1026(-3);
				if (class98_sub46_sub10 != null) {
					boolean bool_366_ = class98_sub46_sub10.method1563(31960, i_365_ >> 14 & 0x3fff, anIntArray1891, i_365_ & 0x3fff);
					if (bool_366_) {
						integerStack[integerStackPtr++] = anIntArray1891[1];
						integerStack[integerStackPtr++] = anIntArray1891[2];
					} else {
						integerStack[integerStackPtr++] = -1;
						integerStack[integerStackPtr++] = -1;
					}
				} else {
					integerStack[integerStackPtr++] = -1;
					integerStack[integerStackPtr++] = -1;
					return;
				}
				return;
			}
			if (buttonId == 5226) {
				Class40.method362((byte) 103, integerStack[--integerStackPtr]);
				return;
			}
			if (buttonId == 5227) {
				integerStackPtr -= 2;
				int i_367_ = integerStack[integerStackPtr];
				int i_368_ = integerStack[integerStackPtr + 1];
				Class119.method2176(i_368_ >> 14 & 0x3fff, true, i_367_, (byte) 89, i_368_ & 0x3fff);
				return;
			}
			if (buttonId == 5228) {
				Class98_Sub43_Sub1.aBoolean5895 = integerStack[--integerStackPtr] == 1;
				return;
			}
			if (buttonId == 5229) {
				integerStack[integerStackPtr++] = Class98_Sub43_Sub1.aBoolean5895 ? 1 : 0;
				return;
			}
			if (buttonId == 5230) {
				int i_369_ = integerStack[--integerStackPtr];
				Class119_Sub4.method2190(125, i_369_);
				return;
			}
			if (buttonId == 5231) {
				integerStackPtr -= 2;
				int i_370_ = integerStack[integerStackPtr];
				boolean bool_371_ = integerStack[integerStackPtr + 1] == 1;
				if (BConfigDefinition.aClass377_3114 != null) {
					Node node = BConfigDefinition.aClass377_3114.get(i_370_, -1);
					if (node != null && !bool_371_) {
						node.unlink(120);
					} else if (node == null && bool_371_) {
						node = new Node();
						BConfigDefinition.aClass377_3114.put(node, i_370_, -1);
					}
				}
				return;
			}
			if (buttonId == 5232) {
				int i_372_ = integerStack[--integerStackPtr];
				if (BConfigDefinition.aClass377_3114 != null) {
					Node class98 = BConfigDefinition.aClass377_3114.get(i_372_, -1);
					integerStack[integerStackPtr++] = class98 != null ? 1 : 0;
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
			if (buttonId == 5233) {
				integerStackPtr -= 2;
				int i_373_ = integerStack[integerStackPtr];
				boolean bool_374_ = integerStack[integerStackPtr + 1] == 1;
				if (Class248.aClass377_1894 != null) {
					Node class98 = Class248.aClass377_1894.get(i_373_, -1);
					if (class98 != null && !bool_374_) {
						class98.unlink(97);
					} else if (class98 == null && bool_374_) {
						class98 = new Node();
						Class248.aClass377_1894.put(class98, i_373_, -1);
					}
				}
				return;
			}
			if (buttonId == 5234) {
				int i_375_ = integerStack[--integerStackPtr];
				if (Class248.aClass377_1894 != null) {
					Node class98 = Class248.aClass377_1894.get(i_375_, -1);
					integerStack[integerStackPtr++] = class98 != null ? 1 : 0;
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
			if (buttonId == 5235) {
				integerStack[integerStackPtr++] = WorldMap.aClass98_Sub46_Sub10_2056 != null ? WorldMap.aClass98_Sub46_Sub10_2056.anInt6014 : -1;
				return;
			}
			if (buttonId == 5236) {
				integerStackPtr -= 2;
				int i_376_ = integerStack[integerStackPtr];
				int i_377_ = integerStack[integerStackPtr + 1];
				int i_378_ = i_377_ >> 14 & 0x3fff;
				int i_379_ = i_377_ & 0x3fff;
				int i_380_ = Class98_Sub28.method1307(i_376_, 1, i_378_, i_379_);
				if (i_380_ < 0) {
					integerStack[integerStackPtr++] = -1;
				} else {
					integerStack[integerStackPtr++] = i_380_;
					return;
				}
				return;
			}
			if (buttonId == 5237) {
				CursorDefinition.method2878(2);
				return;
			}
		} else if (buttonId < 5400) {
			if (buttonId == 5300) {
				integerStackPtr -= 2;
				int i_381_ = integerStack[integerStackPtr];
				int i_382_ = integerStack[integerStackPtr + 1];
				Class98_Sub16.setWindowMode(3, i_381_, 3, i_382_, false);
				integerStack[integerStackPtr++] = GameShell.fullScreenFrame != null ? 1 : 0;
				return;
			}
			if (buttonId == 5301) {
				if (GameShell.fullScreenFrame != null) {
					Class98_Sub16.setWindowMode(client.preferences.screenSize.getValue((byte) 123), -1, 3, -1, false);
				}
				return;
			}
			if (buttonId == 5302) {
				Class259[] class259s = Class259.method451(124);
				integerStack[integerStackPtr++] = class259s.length;
				return;
			}
			if (buttonId == 5303) {
				int i_383_ = integerStack[--integerStackPtr];
				Class259[] class259s = Class259.method451(121);
				integerStack[integerStackPtr++] = class259s[i_383_].anInt1953;
				integerStack[integerStackPtr++] = class259s[i_383_].anInt1956;
				return;
			}
			if (buttonId == 5305) {
				int i_384_ = VerticalAlignment.anInt946;
				int i_385_ = Class112.anInt949;
				int i_386_ = -1;
				Class259[] class259s = Class259.method451(122);
				for (int i_387_ = 0; i_387_ < class259s.length; i_387_++) {
					Class259 class259 = class259s[i_387_];
					if (class259.anInt1953 == i_384_ && class259.anInt1956 == i_385_) {
						i_386_ = i_387_;
						break;
					}
				}
				integerStack[integerStackPtr++] = i_386_;
				return;
			}
			if (buttonId == 5306) {
				integerStack[integerStackPtr++] = OpenGlModelRenderer.getWindowMode((byte) 102);
				return;
			}
			if (buttonId == 5307) {
				int windowMode = integerStack[--integerStackPtr];
				if (windowMode >= 1 && windowMode <= 2) {
					Class98_Sub16.setWindowMode(windowMode, -1, 3, -1, false);
					return;
				}
				return;
			}
			if (buttonId == 5308) {
				integerStack[integerStackPtr++] = client.preferences.screenSize.getValue((byte) 122);
				return;
			}
			if (buttonId == 5309) {
				int toolkit = integerStack[--integerStackPtr];
				if (toolkit >= 1 && toolkit <= 2) {
					client.preferences.setPreference((byte) -13, toolkit, client.preferences.screenSize);
					client.preferences.setPreference((byte) -13, toolkit, client.preferences.currentScreenSize);
					Class310.method3618(-5964);
					return;
				}
				return;
			}
		} else if (buttonId < 5500) {
			if (buttonId == 5400) {
				stringStackPtr -= 2;
				String string = stringStack[stringStackPtr];
				String string_390_ = stringStack[stringStackPtr + 1];
				int i_391_ = integerStack[--integerStackPtr];
				System.out.println("String: " + string);
				OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class224.aClass171_1684, Class331.aClass117_2811);
				frame.packet.writeByte(NativeShadow.encodedStringLength(string, (byte) 79) + NativeShadow.encodedStringLength(string_390_, (byte) 124) + 1, -41);
				frame.packet.writePJStr1(string, (byte) 113);
				frame.packet.writePJStr1(string_390_, (byte) 113);
				frame.packet.writeByte(i_391_, -78);
				Class98_Sub10_Sub29.sendPacket(false, frame);
				return;
			}
			if (buttonId == 5401) {
				integerStackPtr -= 2;
				SceneGraphNode.clientPalette[integerStack[integerStackPtr]] = (short) HslUtils.rgbToHsl(integerStack[integerStackPtr + 1], -13);
				Class98_Sub46_Sub19.itemDefinitionList.clear(60);
				Class98_Sub46_Sub19.itemDefinitionList.method2717(64);
				Class4.npcDefinitionList.method3534(-123);
				Class98_Sub43.setAllDirty(2);
				return;
			}
			if (buttonId == 5405) {
				integerStackPtr -= 2;
				int i_392_ = integerStack[integerStackPtr];
				int i_393_ = integerStack[integerStackPtr + 1];
				if (i_392_ >= 0 && i_392_ < 2) {
					DummyInputStream.animatedBackgroundCoords[i_392_] = new int[i_393_ << 1][4];
				}
				return;
			}
			if (buttonId == 5406) {
				integerStackPtr -= 7;
				int i_394_ = integerStack[integerStackPtr];
				int i_395_ = integerStack[integerStackPtr + 1] << 1;
				int i_396_ = integerStack[integerStackPtr + 2];
				int i_397_ = integerStack[integerStackPtr + 3];
				int i_398_ = integerStack[integerStackPtr + 4];
				int i_399_ = integerStack[integerStackPtr + 5];
				int i_400_ = integerStack[integerStackPtr + 6];
				if (i_394_ >= 0 && i_394_ < 2 && DummyInputStream.animatedBackgroundCoords[i_394_] != null && i_395_ >= 0 && i_395_ < DummyInputStream.animatedBackgroundCoords[i_394_].length) {
					DummyInputStream.animatedBackgroundCoords[i_394_][i_395_] = new int[] { (i_396_ >> 14 & 0x3fff) << 9, i_397_ << 2, (i_396_ & 0x3fff) << 9, i_400_ };
					DummyInputStream.animatedBackgroundCoords[i_394_][i_395_ + 1] = new int[] { (i_398_ >> 14 & 0x3fff) << 9, i_399_ << 2, (i_398_ & 0x3fff) << 9 };
				}
				return;
			}
			if (buttonId == 5407) {
				int i_401_ = DummyInputStream.animatedBackgroundCoords[integerStack[--integerStackPtr]].length >> 1;
				integerStack[integerStackPtr++] = i_401_;
				return;
			}
			if (buttonId == 5411) {
				if (GameShell.fullScreenFrame != null) {
					Class98_Sub16.setWindowMode(client.preferences.screenSize.getValue((byte) 121), -1, 3, -1, false);
				}
				if (GameShell.frame != null) {
					Class23.method283((byte) 100);
					System.exit(0);
				} else {
					String string = ScalingSpriteLoadingScreenElement.aString3440 != null ? ScalingSpriteLoadingScreenElement.aString3440 : GameDefinition.method1153((byte) -100);
					TextLSEConfig.method3647(false, client.preferences.currentToolkit.getValue((byte) 123) == 1, string, true, GameShell.signLink);
					return;
				}
				return;
			}
			if (buttonId == 5419) {
				String string = "";
				if (Class187.aClass143_1449 != null) {
					if (Class187.aClass143_1449.result != null) {
						string = (String) Class187.aClass143_1449.result;
					} else {
						string = Class98_Sub10_Sub39.method1122(Class187.aClass143_1449.intParameter, (byte) -36);
					}
				}
				stringStack[stringStackPtr++] = string;
				return;
			}
			if (buttonId == 5420) {
				integerStack[integerStackPtr++] = GameShell.signLink.is_signed ? 0 : 1;
				return;
			}
			if (buttonId == 5421) {
				if (GameShell.fullScreenFrame != null) {
					Class98_Sub16.setWindowMode(client.preferences.screenSize.getValue((byte) 125), -1, 3, -1, false);
				}
				String string = stringStack[--stringStackPtr];
				boolean bool_402_ = integerStack[--integerStackPtr] == 1;
				String string_403_ = GameDefinition.method1153((byte) -64) + string;
				TextLSEConfig.method3647(bool_402_, client.preferences.currentToolkit.getValue((byte) 123) == 1, string_403_, true, GameShell.signLink);
				return;
			}
			if (buttonId == 5422) {
				stringStackPtr -= 2;
				String string = stringStack[stringStackPtr];
				String string_404_ = stringStack[stringStackPtr + 1];
				int i_405_ = integerStack[--integerStackPtr];
				if (string.length() > 0) {
					if (Class116.aStringArray966 == null) {
						Class116.aStringArray966 = new String[LoadingScreenSequence.anIntArray2130[client.game.id]];
					}
					Class116.aStringArray966[i_405_] = string;
				}
				if (string_404_.length() > 0) {
					if (Class84.aStringArray636 == null) {
						Class84.aStringArray636 = new String[LoadingScreenSequence.anIntArray2130[client.game.id]];
					}
					Class84.aStringArray636[i_405_] = string_404_;
				}
				return;
			}
			if (buttonId == 5423) {
				System.out.println(stringStack[--stringStackPtr]);
				return;
			}
			if (buttonId == 5424) {
				integerStackPtr -= 11;
				ProceduralTextureSource.anInt3261 = integerStack[integerStackPtr];
				Class355.anInt3017 = integerStack[integerStackPtr + 1];
				AnimatedProgressBarLSEConfig.anInt6289 = integerStack[integerStackPtr + 2];
				StructsDefinitionParser.anInt1971 = integerStack[integerStackPtr + 3];
				Class38.anInt360 = integerStack[integerStackPtr + 4];
				LoginOpcode.anInt1672 = integerStack[integerStackPtr + 5];
				Class25.anInt267 = integerStack[integerStackPtr + 6];
				Class95.anInt799 = integerStack[integerStackPtr + 7];
				ModelRenderer.anInt1183 = integerStack[integerStackPtr + 8];
				ClanChatMember.anInt1194 = integerStack[integerStackPtr + 9];
				SceneGraphNode.anInt1871 = integerStack[integerStackPtr + 10];
				client.spriteJs5.isFileCached(-28, Class38.anInt360);
				client.spriteJs5.isFileCached(-124, LoginOpcode.anInt1672);
				client.spriteJs5.isFileCached(-125, Class25.anInt267);
				client.spriteJs5.isFileCached(-34, Class95.anInt799);
				client.spriteJs5.isFileCached(-116, ModelRenderer.anInt1183);
				ParticleManager.aClass332_383 = Class98_Sub50.aClass332_4287 = Class98_Sub47.aClass332_4273 = null;
				NPC.aClass332_6508 = QuickChat.aClass332_2500 = Class76_Sub11.aClass332_3795 = null;
				Class98_Sub10_Sub28.aClass332_5704 = Class221.aClass332_1666 = null;
				Class98_Sub5_Sub3.aBoolean5539 = true;
				return;
			}
			if (buttonId == 5425) {
				Class48_Sub1_Sub2.method466(true);
				Class98_Sub5_Sub3.aBoolean5539 = false;
				return;
			}
			if (buttonId == 5426) {
				integerStackPtr -= 2;
				OutputStream_Sub2.anInt39 = integerStack[integerStackPtr];
				Class284_Sub2.anInt5186 = integerStack[integerStackPtr + 1];
				return;
			}
			if (buttonId == 5427) {
				integerStackPtr -= 2;
				Class16.anInt190 = integerStack[integerStackPtr + 1];
				return;
			}
			if (buttonId == 5428) {
				integerStackPtr -= 2;
				int i_406_ = integerStack[integerStackPtr];
				int i_407_ = integerStack[integerStackPtr + 1];
				integerStack[integerStackPtr++] = Class98_Sub46_Sub5.method1543(i_406_, i_407_, (byte) 6) ? 1 : 0;
				return;
			}
			if (buttonId == 5429) {
				Class295.handleClientCommand(stringStack[--stringStackPtr], false, false, (byte) 117);
				return;
			}
			if (buttonId == 5430) {
				try {
					// JavaScriptInterface.callJsMethod("accountcreated",
					// GameShell.applet, -26978);
				} catch (Throwable throwable) {
					/* empty */
				}
				return;
			}
			if (buttonId == 5431) {
				try {
					// JavaScriptInterface.callJsMethod("accountcreatestarted",
					// GameShell.applet, -26978);
				} catch (Throwable throwable) {
					/* empty */
				}
				return;
			}
			if (buttonId == 5432) {
				String string = "";
				if (client.systemClipboard != null) {
					Transferable transferable = client.systemClipboard.getContents(null);
					if (transferable != null) {
						try {
							string = (String) transferable.getTransferData(DataFlavor.stringFlavor);
							if (string == null) {
								string = "";
							}
						} catch (Exception exception) {
							/* empty */
						}
					}
				}
				stringStack[stringStackPtr++] = string;
				return;
			}
			if (buttonId == 5433) {
				Class64_Sub3.anInt3647 = integerStack[--integerStackPtr];
				return;
			}
		} else if (buttonId < 5600) {
			if (buttonId == 5500) {
				integerStackPtr -= 4;
				int i_408_ = integerStack[integerStackPtr];
				int i_409_ = integerStack[integerStackPtr + 1];
				int i_410_ = integerStack[integerStackPtr + 2];
				int i_411_ = integerStack[integerStackPtr + 3];
				OpenGlToolkit.method1871(i_410_, (i_408_ >> 14 & 0x3fff) - Class272.gameSceneBaseX, false, i_409_ << 2, (i_408_ & 0x3fff) - aa_Sub2.gameSceneBaseY, i_411_, -116);
				return;
			}
			if (buttonId == 5501) {
				integerStackPtr -= 4;
				int i_412_ = integerStack[integerStackPtr];
				int i_413_ = integerStack[integerStackPtr + 1];
				int i_414_ = integerStack[integerStackPtr + 2];
				int i_415_ = integerStack[integerStackPtr + 3];
				FileRequest.method1592(-25686, i_414_, (i_412_ >> 14 & 0x3fff) - Class272.gameSceneBaseX, i_413_ << 2, i_415_, (i_412_ & 0x3fff) - aa_Sub2.gameSceneBaseY);
				return;
			}
			if (buttonId == 5502) {
				integerStackPtr -= 6;
				int i_416_ = integerStack[integerStackPtr];
				if (i_416_ >= 2) {
					throw new RuntimeException();
				}
				NewsItem.anInt3128 = i_416_;
				int i_417_ = integerStack[integerStackPtr + 1];
				if (i_417_ + 1 >= DummyInputStream.animatedBackgroundCoords[NewsItem.anInt3128].length >> 1) {
					throw new RuntimeException();
				}
				Class50.anInt418 = i_417_;
				NativeProgressMonitor.anInt3394 = 0;
				SceneGraphNode.anInt1872 = integerStack[integerStackPtr + 2];
				Class98_Sub10_Sub32.anInt5718 = integerStack[integerStackPtr + 3];
				int i_418_ = integerStack[integerStackPtr + 4];
				if (i_418_ >= 2) {
					throw new RuntimeException();
				}
				Class53_Sub1.anInt3636 = i_418_;
				int i_419_ = integerStack[integerStackPtr + 5];
				if (i_419_ + 1 >= DummyInputStream.animatedBackgroundCoords[Class53_Sub1.anInt3636].length >> 1) {
					throw new RuntimeException();
				}
				RSToolkit.anInt943 = i_419_;
				Class98_Sub46_Sub20_Sub2.cameraMode = 3;
				Class116.anInt967 = CharacterShadowsPreferenceField.anInt3712 = -1;
				return;
			}
			if (buttonId == 5503) {
				Class284.method3359(9268);
				return;
			}
			if (buttonId == 5504) {
				integerStackPtr -= 2;
				Class308.handleCameraClick(integerStack[integerStackPtr + 1], 0, 87, integerStack[integerStackPtr]);
				return;
			}
			if (buttonId == 5505) {
				integerStack[integerStackPtr++] = (int) Class119_Sub4.aFloat4740 >> 3;
				return;
			}
			if (buttonId == 5506) {
				integerStack[integerStackPtr++] = (int) RsFloatBuffer.aFloat5794 >> 3;
				return;
			}
			if (buttonId == 5507) {
				Exception_Sub1.method134((byte) -87);
				return;
			}
			if (buttonId == 5508) {
				Class98_Sub43.method1485(-1);
				return;
			}
			if (buttonId == 5509) {
				FlickeringEffectsPreferenceField.method603((byte) -107);
				return;
			}
			if (buttonId == 5510) {
				Class98_Sub31_Sub2.method1367((byte) 83);
				return;
			}
			if (buttonId == 5511) {
				int i_420_ = integerStack[--integerStackPtr];
				int i_421_ = i_420_ >> 14 & 0x3fff;
				int i_422_ = i_420_ & 0x3fff;
				i_421_ -= Class272.gameSceneBaseX;
				if (i_421_ < 0) {
					i_421_ = 0;
				} else if (i_421_ >= Class165.mapWidth) {
					i_421_ = Class165.mapWidth;
				}
				i_422_ -= aa_Sub2.gameSceneBaseY;
				if (i_422_ < 0) {
					i_422_ = 0;
				} else if (i_422_ >= Class98_Sub10_Sub7.mapLength) {
					i_422_ = Class98_Sub10_Sub7.mapLength;
				}
				StrongReferenceMCNode.anInt6295 = (i_421_ << 9) + 256;
				Js5Client.anInt1051 = (i_422_ << 9) + 256;
				Class98_Sub46_Sub20_Sub2.cameraMode = 4;
				Class116.anInt967 = CharacterShadowsPreferenceField.anInt3712 = -1;
				return;
			}
			if (buttonId == 5512) {
				GameObjectDefinition.method3856((byte) 1);
				return;
			}
			if (buttonId == 5514) {
				Class16.orthoZoom = integerStack[--integerStackPtr];
				return;
			}
			if (buttonId == 5516) {
				integerStack[integerStackPtr++] = Class16.orthoZoom;
				return;
			}
			if (buttonId == 5517) {
				int i_423_ = integerStack[--integerStackPtr];
				if (i_423_ == -1) {
					int i_424_ = i_423_ >> 14 & 0x3fff;
					int i_425_ = i_423_ & 0x3fff;
					i_424_ -= Class272.gameSceneBaseX;
					if (i_424_ < 0) {
						i_424_ = 0;
					} else if (i_424_ >= Class165.mapWidth) {
						i_424_ = Class165.mapWidth;
					}
					i_425_ -= aa_Sub2.gameSceneBaseY;
					if (i_425_ < 0) {
						i_425_ = 0;
					} else if (i_425_ >= Class98_Sub10_Sub7.mapLength) {
						i_425_ = Class98_Sub10_Sub7.mapLength;
					}
					Class116.anInt967 = (i_424_ << 9) + 256;
					CharacterShadowsPreferenceField.anInt3712 = (i_425_ << 9) + 256;
				} else {
					Class116.anInt967 = -1;
					CharacterShadowsPreferenceField.anInt3712 = -1;
					return;
				}
				return;
			}
		} else if (buttonId < 5700) {
			if (buttonId == 5600) {
				stringStackPtr -= 2;
				String username = stringStack[stringStackPtr];
				String password = stringStack[stringStackPtr + 1];
				int i_427_ = integerStack[--integerStackPtr];
				System.out.println("String :" + username);
				if (username.length() <= 320 && client.clientState == 3 && MaxScreenSizePreferenceField.anInt3680 == 0 && Class21_Sub4.anInt5394 == 0) {
					PointLight.username = username;
					Class360.password = password;
					OpenGlModelRenderer.anInt4855 = i_427_;
					HashTableIterator.setClientState(6, false);
					return;
				}
				return;
			}
			if (buttonId == 5601) {
				FriendLoginUpdate.method3103((byte) -38);
				return;
			}
			if (buttonId == 5602) {
				if (MaxScreenSizePreferenceField.anInt3680 == 0) {
					RenderAnimDefinitionParser.anInt1946 = -2;
					SocketWrapper.anInt300 = -2;
				}
				return;
			}
			if (buttonId == 5604) {
				System.out.println("String :");
				stringStackPtr--;
				if (client.clientState == 3 && MaxScreenSizePreferenceField.anInt3680 == 0 && Class21_Sub4.anInt5394 == 0) {
					Class329.method3713((byte) 34, stringStack[stringStackPtr]);
					return;
				}
				return;
			}
			if (buttonId == 5605) {
				stringStackPtr -= 2;
				integerStackPtr -= 2;
				if (client.clientState == 3 && MaxScreenSizePreferenceField.anInt3680 == 0 && Class21_Sub4.anInt5394 == 0) {
					VarClientDefinitionParser.method2236(integerStack[integerStackPtr + 1] == 1, 0, stringStack[stringStackPtr + 1], stringStack[stringStackPtr], integerStack[integerStackPtr]);
					return;
				}
				return;
			}
			if (buttonId == 5606) {
				if (Class21_Sub4.anInt5394 == 0) {
					OpenGLRenderEffectManager.anInt442 = -2;
				}
				return;
			}
			if (buttonId == 5607) {
				integerStack[integerStackPtr++] = SocketWrapper.anInt300;
				return;
			}
			if (buttonId == 5608) {
				integerStack[integerStackPtr++] = Class98_Sub48.anInt4277;
				return;
			}
			if (buttonId == 5609) {
				integerStack[integerStackPtr++] = OpenGLRenderEffectManager.anInt442;
				return;
			}
			if (buttonId == 5611) {
				integerStack[integerStackPtr++] = Class69_Sub1.anInt5330;
				return;
			}
			if (buttonId == 5612) {
				int i_428_ = integerStack[--integerStackPtr];
				System.out.println(i_428_);
				if (client.clientState == 7 && MaxScreenSizePreferenceField.anInt3680 == 0 && Class21_Sub4.anInt5394 == 0) {
					if (aa_Sub1.aClass123_3561 != null) {
						aa_Sub1.aClass123_3561.close(-18);
						aa_Sub1.aClass123_3561 = null;
					}
					OpenGlModelRenderer.anInt4855 = i_428_;
					HashTableIterator.setClientState(9, false);
					return;
				}
				return;
			}
			if (buttonId == 5613) {
				integerStack[integerStackPtr++] = SocketWrapper.anInt300;
				return;
			}
			if (buttonId == 5615) {
				stringStackPtr -= 2;
				String username = stringStack[stringStackPtr];
				String password = stringStack[stringStackPtr + 1];
				if (username.length() <= 320 && client.clientState == 3 && MaxScreenSizePreferenceField.anInt3680 == 0 && Class21_Sub4.anInt5394 == 0) {
					if (aa_Sub1.aClass123_3561 != null) {
						aa_Sub1.aClass123_3561.close(-64);
						aa_Sub1.aClass123_3561 = null;
					}
					PointLight.username = username;
					Class360.password = password;
					HashTableIterator.setClientState(5, false);
					return;
				}
				return;
			}
			if (buttonId == 5616) {
				Class98_Sub10_Sub1.handleLogout(false, false);
				return;
			}
			if (buttonId == 5617) {
				integerStack[integerStackPtr++] = RenderAnimDefinitionParser.anInt1946;
				return;
			}
			if (buttonId == 5618) {
				integerStackPtr--;
				return;
			}
			if (buttonId == 5619) {
				integerStackPtr--;
				return;
			}
			if (buttonId == 5620) {
				integerStack[integerStackPtr++] = 0;
				return;
			}
			if (buttonId == 5621) {
				stringStackPtr -= 2;
				integerStackPtr -= 2;
				return;
			}
			if (buttonId == 5622) {
				return;
			}
			if (buttonId == 5623) {
				if (client.ssKey != null) {
					integerStack[integerStackPtr++] = 1;
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
			if (buttonId == 5624) {
				integerStack[integerStackPtr++] = (int) (client.userFlow >> 32);
				integerStack[integerStackPtr++] = (int) (client.userFlow & 0xffffL);
				return;
			}
			if (buttonId == 5625) {
				integerStack[integerStackPtr++] = RSToolkit.aBoolean940 ? 1 : 0;
				return;
			}
			if (buttonId == 5626) {
				RSToolkit.aBoolean940 = true;
				MapRegion.method3571(-68);
				return;
			}
		} else if (buttonId < 6100) {
			if (buttonId == 6001) {
				int i_430_ = integerStack[--integerStackPtr];
				client.preferences.setPreference((byte) -13, i_430_, client.preferences.brightnessLevel);
				Class98_Sub10.method999((byte) -74);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6002) {
				boolean bool_431_ = integerStack[--integerStackPtr] == 1;
				client.preferences.setPreference((byte) -13, bool_431_ ? 1 : 0, client.preferences.aClass64_Sub3_4041);
				client.preferences.setPreference((byte) -13, bool_431_ ? 1 : 0, client.preferences.aClass64_Sub3_4076);
				Class98_Sub10.method999((byte) 121);
				Js5Client.method2264((byte) -118);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6003) {
				boolean bool_432_ = integerStack[--integerStackPtr] == 1;
				client.preferences.setPreference((byte) -13, bool_432_ ? 2 : 1, client.preferences.removeRoofs);
				client.preferences.setPreference((byte) -13, bool_432_ ? 2 : 1, client.preferences.removeRoofsMode);
				Js5Client.method2264((byte) -117);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6005) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 1 : 0, client.preferences.groundDecoration);
				Class98_Sub10.method999((byte) 124);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6007) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.idleAnimations);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6008) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 1 : 0, client.preferences.flickeringEffects);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6010) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 1 : 0, client.preferences.characterShadows);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6011) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.sceneryShadows);
				Class98_Sub10.method999((byte) 123);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6012) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 1 : 0, client.preferences.lightningDetail);
				Js5FileRequest.method1593((byte) 67);
				WhirlpoolGenerator.method3980((byte) 127);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6014) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 2 : 0, client.preferences.waterDetail);
				Class98_Sub10.method999((byte) 119);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6015) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 1 : 0, client.preferences.fog);
				Class98_Sub10.method999((byte) 123);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6016) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.multiSample);
				Class76_Sub4.method754(client.preferences.currentToolkit.getValue((byte) 123), false, 80);
				Class310.method3618(-5964);
				return;
			}
			if (buttonId == 6017) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 1 : 0, client.preferences.monoOrStereo);
				Class233.method2884(124);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6018) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.soundEffectsVolume);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6019) {
				int i_433_ = integerStack[--integerStackPtr];
				int i_434_ = client.preferences.musicVolume.getValue((byte) 121);
				if (i_433_ != i_434_) {
					if (OpenGLHeap.method1683(-11297, client.clientState)) {
						if (i_434_ == 0 && Class144.anInt1169 != -1) {
							OpenGlGround.method3434(Class98_Sub10_Sub1.musicJs5, false, i_433_, Class144.anInt1169, 0, -16523);
							Class233.method2883((byte) 111);
							Class151_Sub5.aBoolean4991 = false;
						} else if (i_433_ == 0) {
							RotatingSpriteLSEConfig.method3777(31585);
							Class151_Sub5.aBoolean4991 = false;
						} else {
							Class98_Sub10_Sub19.method1057(i_433_, 1024);
						}
					}
					client.preferences.setPreference((byte) -13, i_433_, client.preferences.musicVolume);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
				}
				return;
			}
			if (buttonId == 6020) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.areaSoundsVolume);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6021) {
				int i_435_ = client.preferences.removeRoofs.getValue((byte) 127);
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 0 : i_435_, client.preferences.removeRoofsMode);
				Js5Client.method2264((byte) -113);
				return;
			}
			if (buttonId == 6023) {
				int i_436_ = integerStack[--integerStackPtr];
				client.preferences.setPreference((byte) -13, i_436_, client.preferences.particles);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6024) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.antiAliasing);
				Class310.method3618(-5964);
				return;
			}
			if (buttonId == 6025) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.buildArea);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6027) {
				int i_437_ = integerStack[--integerStackPtr];
				if (i_437_ < 0 || i_437_ > 1) {
					i_437_ = 0;
				}
				Class98_Sub5_Sub1.method966(29089, i_437_ == 1);
				return;
			}
			if (buttonId == 6028) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] != 0 ? 1 : 0, client.preferences.useCustomCursor);
				Class310.method3618(-5964);
				return;
			}
			if (buttonId == 6029) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.idleAnimations);
				Class310.method3618(-5964);
				return;
			}
			if (buttonId == 6030) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] != 0 ? 1 : 0, client.preferences.groundBlending);
				Class310.method3618(-5964);
				Class98_Sub10.method999((byte) -71);
				return;
			}
			if (buttonId == 6031) {
				int i_438_ = integerStack[--integerStackPtr];
				if (i_438_ < 0 || i_438_ > 5) {
					i_438_ = 2;
				}
				Class76_Sub4.method754(i_438_, false, 3);
				return;
			}
			if (buttonId == 6032) {
				integerStackPtr -= 2;
				int i_439_ = integerStack[integerStackPtr];
				boolean bool_440_ = integerStack[integerStackPtr + 1] == 1;
				client.preferences.setPreference((byte) -13, i_439_, client.preferences.desiredToolkit);
				if (!bool_440_) {
					client.preferences.setPreference((byte) -13, 0, client.preferences.graphicsLevel);
				}
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6033) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.cpuUsage);
				Class310.method3618(-5964);
				return;
			}
			if (buttonId == 6034) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 1 : 0, client.preferences.textures);
				Class310.method3618(-5964);
				Js5FileRequest.method1593((byte) 49);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6035) {
				int i_441_ = client.preferences.aClass64_Sub3_4041.getValue((byte) 125);
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr] == 1 ? 1 : i_441_, client.preferences.aClass64_Sub3_4076);
				Class98_Sub10.method999((byte) 112);
				Js5Client.method2264((byte) -118);
				return;
			}
			if (buttonId == 6036) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.maxScreenSize);
				Class310.method3618(-5964);
				Class33.aBoolean316 = true;
				return;
			}
			if (buttonId == 6037) {
				client.preferences.setPreference((byte) -13, integerStack[--integerStackPtr], client.preferences.voiceOverVolume);
				Class310.method3618(-5964);
				OpenGlGround.aBoolean5207 = false;
				return;
			}
			if (buttonId == 6038) {
				int i_442_ = integerStack[--integerStackPtr];
				int i_443_ = client.preferences.generalMusicVolume.getValue((byte) 123);
				if (i_442_ != i_443_ && Class144.anInt1169 == Class94.anInt795) {
					if (!OpenGLHeap.method1683(-11297, client.clientState)) {
						if (i_443_ == 0) {
							OpenGlGround.method3434(Class98_Sub10_Sub1.musicJs5, false, i_442_, Class144.anInt1169, 0, -16523);
							Class233.method2883((byte) 111);
							Class151_Sub5.aBoolean4991 = false;
						} else if (i_442_ == 0) {
							RotatingSpriteLSEConfig.method3777(31585);
							Class151_Sub5.aBoolean4991 = false;
						} else {
							Class98_Sub10_Sub19.method1057(i_442_, 1024);
						}
					}
					client.preferences.setPreference((byte) -13, i_442_, client.preferences.generalMusicVolume);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
				}
				return;
			}
			if (buttonId == 6039) {
				int i_444_ = integerStack[--integerStackPtr];
				if (i_444_ > 255 || i_444_ < 0) {
					i_444_ = 0;
				}
				if (i_444_ != client.preferences.loadingScreenSequence.getValue((byte) 125)) {
					client.preferences.setPreference((byte) -13, i_444_, client.preferences.loadingScreenSequence);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
				}
				return;
			}
			if (buttonId == 6040) {
				int i_445_ = integerStack[--integerStackPtr];
				if (i_445_ != client.preferences.orthoZoom.getValue((byte) 122)) {
					client.preferences.setPreference((byte) -13, i_445_, client.preferences.orthoZoom);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
					Class230.method2871(-58);
				}
				return;
			}
		} else if (buttonId < 6200) {
			if (buttonId == 6101) {
				integerStack[integerStackPtr++] = client.preferences.brightnessLevel.getValue((byte) 125);
				return;
			}
			if (buttonId == 6102) {
				integerStack[integerStackPtr++] = client.preferences.aClass64_Sub3_4041.getValue((byte) 127) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6103) {
				integerStack[integerStackPtr++] = client.preferences.removeRoofs.getValue((byte) 125) == 2 ? 1 : 0;
				return;
			}
			if (buttonId == 6105) {
				integerStack[integerStackPtr++] = client.preferences.groundDecoration.getValue((byte) 125) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6107) {
				integerStack[integerStackPtr++] = client.preferences.idleAnimations.getValue((byte) 126);
				return;
			}
			if (buttonId == 6108) {
				integerStack[integerStackPtr++] = client.preferences.flickeringEffects.getValue((byte) 127) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6110) {
				integerStack[integerStackPtr++] = client.preferences.characterShadows.getValue((byte) 120) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6111) {
				integerStack[integerStackPtr++] = client.preferences.sceneryShadows.getValue((byte) 126);
				return;
			}
			if (buttonId == 6112) {
				integerStack[integerStackPtr++] = client.preferences.lightningDetail.getValue((byte) 123) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6114) {
				integerStack[integerStackPtr++] = client.preferences.waterDetail.getValue((byte) 125) == 2 ? 1 : 0;
				return;
			}
			if (buttonId == 6115) {
				integerStack[integerStackPtr++] = client.preferences.fog.getValue((byte) 125) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6116) {
				integerStack[integerStackPtr++] = client.preferences.multiSample.getValue((byte) 125);
				return;
			}
			if (buttonId == 6117) {
				integerStack[integerStackPtr++] = client.preferences.monoOrStereo.getValue((byte) 120) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6118) {
				integerStack[integerStackPtr++] = client.preferences.soundEffectsVolume.getValue((byte) 126);
				return;
			}
			if (buttonId == 6119) {
				integerStack[integerStackPtr++] = client.preferences.musicVolume.getValue((byte) 121);
				return;
			}
			if (buttonId == 6120) {
				integerStack[integerStackPtr++] = client.preferences.areaSoundsVolume.getValue((byte) 120);
				return;
			}
			if (buttonId == 6123) {
				integerStack[integerStackPtr++] = CacheFileRequest.method1600((byte) -47);
				return;
			}
			if (buttonId == 6124) {
				integerStack[integerStackPtr++] = client.preferences.antiAliasing.getValue((byte) 121);
				return;
			}
			if (buttonId == 6125) {
				integerStack[integerStackPtr++] = client.preferences.buildArea.getValue((byte) 123);
				return;
			}
			if (buttonId == 6127) {
				integerStack[integerStackPtr++] = client.preferences.unknown3.getValue((byte) 121) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6128) {
				integerStack[integerStackPtr++] = client.preferences.useCustomCursor.getValue((byte) 122) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6129) {
				integerStack[integerStackPtr++] = client.preferences.idleAnimations.getValue((byte) 125);
				return;
			}
			if (buttonId == 6130) {
				integerStack[integerStackPtr++] = client.preferences.groundBlending.getValue((byte) 127) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6131) {
				integerStack[integerStackPtr++] = client.preferences.currentToolkit.getValue((byte) 122);
				return;
			}
			if (buttonId == 6132) {
				integerStack[integerStackPtr++] = client.preferences.desiredToolkit.getValue((byte) 120);
				return;
			}
			if (buttonId == 6133) {
				integerStack[integerStackPtr++] = GameShell.signLink.is_signed && !GameShell.signLink.msJava ? 1 : 0;
				return;
			}
			if (buttonId == 6135) {
				integerStack[integerStackPtr++] = client.preferences.cpuUsage.getValue((byte) 121);
				return;
			}
			if (buttonId == 6136) {
				integerStack[integerStackPtr++] = client.preferences.textures.getValue((byte) 122) == 1 ? 1 : 0;
				return;
			}
			if (buttonId == 6138) {
				integerStack[integerStackPtr++] = Class66.method683((byte) -80, 200, client.preferences.currentToolkit.getValue((byte) 122));
				return;
			}
			if (buttonId == 6139) {
				integerStack[integerStackPtr++] = client.preferences.maxScreenSize.getValue((byte) 124);
				return;
			}
			if (buttonId == 6142) {
				integerStack[integerStackPtr++] = client.preferences.voiceOverVolume.getValue((byte) 127);
				return;
			}
			if (buttonId == 6143) {
				integerStack[integerStackPtr++] = client.preferences.generalMusicVolume.getValue((byte) 120);
				return;
			}
			if (buttonId == 6144) {
				integerStack[integerStackPtr++] = InventoriesDefinition.aBoolean6056 ? 1 : 0;
				return;
			}
			if (buttonId == 6145) {
				integerStack[integerStackPtr++] = client.preferences.loadingScreenSequence.getValue((byte) 121);
				return;
			}
			if (buttonId == 6146) {
				integerStack[integerStackPtr++] = client.preferences.orthoZoom.getValue((byte) 127);
				return;
			}
			if (buttonId == 6147) {
				integerStack[integerStackPtr++] = Exception_Sub1.platformInformation.totalPhysicMemory < 512 || InventoriesDefinition.aBoolean6056 || Class223.aBoolean1679 ? 1 : 0;
				return;
			}
			if (buttonId == 6148) {
				integerStack[integerStackPtr++] = client.safeMode ? 1 : 0;
				return;
			}
		} else if (buttonId < 6300) {
			if (buttonId == 6200) {
				integerStackPtr -= 2;
				Class265.aShort1973 = (short) integerStack[integerStackPtr];
				if (Class265.aShort1973 <= 0) {
					Class265.aShort1973 = (short) 256;
				}
				Class98_Sub43_Sub4.aShort5934 = (short) integerStack[integerStackPtr + 1];
				if (Class98_Sub43_Sub4.aShort5934 <= 0) {
					Class98_Sub43_Sub4.aShort5934 = (short) 205;
				}
				return;
			}
			if (buttonId == 6201) {
				integerStackPtr -= 2;
				BrightnessPreferenceField.aShort3692 = (short) integerStack[integerStackPtr];
				if (BrightnessPreferenceField.aShort3692 <= 0) {
					BrightnessPreferenceField.aShort3692 = (short) 256;
				}
				Js5Client.clientZoom = (short) integerStack[integerStackPtr + 1];
				if (Js5Client.clientZoom <= 0) {
					Js5Client.clientZoom = (short) 320;
				}
				return;
			}
			if (buttonId == 6202) {
				integerStackPtr -= 4;
				Class284_Sub2_Sub2.aShort6201 = (short) integerStack[integerStackPtr];
				if (Class284_Sub2_Sub2.aShort6201 <= 0) {
					Class284_Sub2_Sub2.aShort6201 = (short) 1;
				}
				Class112.aShort948 = (short) integerStack[integerStackPtr + 1];
				if (Class112.aShort948 <= 0) {
					Class112.aShort948 = (short) 32767;
				} else if (Class112.aShort948 < Class284_Sub2_Sub2.aShort6201) {
					Class112.aShort948 = Class284_Sub2_Sub2.aShort6201;
				}
				Class42.aShort3231 = (short) integerStack[integerStackPtr + 2];
				if (Class42.aShort3231 <= 0) {
					Class42.aShort3231 = (short) 1;
				}
				ProceduralTextureSource.aShort3256 = (short) integerStack[integerStackPtr + 3];
				if (ProceduralTextureSource.aShort3256 <= 0) {
					ProceduralTextureSource.aShort3256 = (short) 32767;
				} else {
					if (ProceduralTextureSource.aShort3256 < Class42.aShort3231) {
						ProceduralTextureSource.aShort3256 = Class42.aShort3231;
					}
					return;
				}
				return;
			}
			if (buttonId == 6203) {
				Class151_Sub3.method2453(0, false, GraphicsBuffer.aClass293_4107.renderWidth, 100, GraphicsBuffer.aClass293_4107.renderHeight, 0);
				integerStack[integerStackPtr++] = Queue.anInt1612;
				integerStack[integerStackPtr++] = Class332_Sub2.anInt5421;
				return;
			}
			if (buttonId == 6204) {
				integerStack[integerStackPtr++] = BrightnessPreferenceField.aShort3692;
				integerStack[integerStackPtr++] = Js5Client.clientZoom;
				return;
			}
			if (buttonId == 6205) {
				integerStack[integerStackPtr++] = Class265.aShort1973;
				integerStack[integerStackPtr++] = Class98_Sub43_Sub4.aShort5934;
				return;
			}
		} else if (buttonId < 6400) {
			if (buttonId == 6300) {
				integerStack[integerStackPtr++] = (int) (TimeTools.getCurrentTime(-47) / 60000L);
				return;
			}
			if (buttonId == 6301) {
				integerStack[integerStackPtr++] = (int) (TimeTools.getCurrentTime(-47) / 86400000L) - 11745;
				return;
			}
			if (buttonId == 6302) {
				integerStackPtr -= 3;
				int i_446_ = integerStack[integerStackPtr];
				int i_447_ = integerStack[integerStackPtr + 1];
				int i_448_ = integerStack[integerStackPtr + 2];
				aCalendar1882.clear();
				aCalendar1882.set(11, 12);
				aCalendar1882.set(i_448_, i_447_, i_446_);
				int i_449_ = (int) (aCalendar1882.getTime().getTime() / 86400000L) - 11745;
				if (i_448_ < 1970) {
					i_449_--;
				}
				integerStack[integerStackPtr++] = i_449_;
				return;
			}
			if (buttonId == 6303) {
				aCalendar1882.clear();
				aCalendar1882.setTime(new Date(TimeTools.getCurrentTime(-47)));
				integerStack[integerStackPtr++] = aCalendar1882.get(1);
				return;
			}
			if (buttonId == 6304) {
				int i_450_ = integerStack[--integerStackPtr];
				boolean bool_451_ = true;
				if (i_450_ < 0) {
					bool_451_ = (i_450_ + 1) % 4 == 0;
				} else if (i_450_ < 1582) {
					bool_451_ = i_450_ % 4 == 0;
				} else if (i_450_ % 4 != 0) {
					bool_451_ = false;
				} else if (i_450_ % 100 != 0) {
					bool_451_ = true;
				} else if (i_450_ % 400 != 0) {
					bool_451_ = false;
				}
				integerStack[integerStackPtr++] = bool_451_ ? 1 : 0;
				return;
			}
		} else if (buttonId < 6500) {
			if (buttonId == 6405) {
				integerStack[integerStackPtr++] = Class195.method2662(-96) ? 1 : 0;
				return;
			}
			if (buttonId == 6406) {
				integerStack[integerStackPtr++] = Class98_Sub10_Sub18.method1054(76) ? 1 : 0;
				return;
			}
		} else if (buttonId < 6600) {
			if (buttonId == 6500) {
				if (client.clientState != 7 || MaxScreenSizePreferenceField.anInt3680 != 0 || Class21_Sub4.anInt5394 != 0) {
					integerStack[integerStackPtr++] = 1;
				} else {
					if (GraphicsLevelPreferenceField.aBoolean3671) {
						integerStack[integerStackPtr++] = 0;
					} else {
						if (StringConcatenator.aLong1998 > TimeTools.getCurrentTime(-47) - 1000L) {
							integerStack[integerStackPtr++] = 1;
						} else {
							GraphicsLevelPreferenceField.aBoolean3671 = true;
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, AwtGraphicsBuffer.aClass171_5887, Class331.aClass117_2811);
							class98_sub11.packet.writeInt(1571862888, BuildType.anInt88);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
							integerStack[integerStackPtr++] = 0;
							return;
						}
						return;
					}
					return;
				}
				return;
			}
			if (buttonId == 6501) {
				Class53_Sub1 class53_sub1 = Class155.method2494((byte) 101);
				if (class53_sub1 != null) {
					integerStack[integerStackPtr++] = class53_sub1.anInt3632;
					integerStack[integerStackPtr++] = class53_sub1.anInt427;
					stringStack[stringStackPtr++] = class53_sub1.aString3630;
					Class114 class114 = class53_sub1.method501(-1);
					integerStack[integerStackPtr++] = class114.anInt956;
					stringStack[stringStackPtr++] = class114.aString957;
					integerStack[integerStackPtr++] = class53_sub1.anInt429;
					integerStack[integerStackPtr++] = class53_sub1.anInt3631;
					stringStack[stringStackPtr++] = class53_sub1.aString3634;
				} else {
					integerStack[integerStackPtr++] = -1;
					integerStack[integerStackPtr++] = 0;
					stringStack[stringStackPtr++] = "";
					integerStack[integerStackPtr++] = 0;
					stringStack[stringStackPtr++] = "";
					integerStack[integerStackPtr++] = 0;
					integerStack[integerStackPtr++] = 0;
					stringStack[stringStackPtr++] = "";
					return;
				}
				return;
			}
			if (buttonId == 6502) {
				Class53_Sub1 class53_sub1 = Class69_Sub2.method706(200);
				if (class53_sub1 != null) {
					integerStack[integerStackPtr++] = class53_sub1.anInt3632;
					integerStack[integerStackPtr++] = class53_sub1.anInt427;
					stringStack[stringStackPtr++] = class53_sub1.aString3630;
					Class114 class114 = class53_sub1.method501(-1);
					integerStack[integerStackPtr++] = class114.anInt956;
					stringStack[stringStackPtr++] = class114.aString957;
					integerStack[integerStackPtr++] = class53_sub1.anInt429;
					integerStack[integerStackPtr++] = class53_sub1.anInt3631;
					stringStack[stringStackPtr++] = class53_sub1.aString3634;
				} else {
					integerStack[integerStackPtr++] = -1;
					integerStack[integerStackPtr++] = 0;
					stringStack[stringStackPtr++] = "";
					integerStack[integerStackPtr++] = 0;
					stringStack[stringStackPtr++] = "";
					integerStack[integerStackPtr++] = 0;
					integerStack[integerStackPtr++] = 0;
					stringStack[stringStackPtr++] = "";
					return;
				}
				return;
			}
			if (buttonId == 6503) {
				int i_452_ = integerStack[--integerStackPtr];
				String string = stringStack[--stringStackPtr];
				if (client.clientState != 7 || MaxScreenSizePreferenceField.anInt3680 != 0 || Class21_Sub4.anInt5394 != 0) {
					integerStack[integerStackPtr++] = 0;
				} else {
					integerStack[integerStackPtr++] = Class98_Sub12.method1131(-8804, i_452_, string) ? 1 : 0;
					return;
				}
				return;
			}
			if (buttonId == 6506) {
				int i_453_ = integerStack[--integerStackPtr];
				Class53_Sub1 class53_sub1 = Class275.method3283((byte) 123, i_453_);
				if (class53_sub1 != null) {
					integerStack[integerStackPtr++] = class53_sub1.anInt427;
					stringStack[stringStackPtr++] = class53_sub1.aString3630;
					Class114 class114 = class53_sub1.method501(-1);
					integerStack[integerStackPtr++] = class114.anInt956;
					stringStack[stringStackPtr++] = class114.aString957;
					integerStack[integerStackPtr++] = class53_sub1.anInt429;
					integerStack[integerStackPtr++] = class53_sub1.anInt3631;
					stringStack[stringStackPtr++] = class53_sub1.aString3634;
				} else {
					integerStack[integerStackPtr++] = -1;
					stringStack[stringStackPtr++] = "";
					integerStack[integerStackPtr++] = 0;
					stringStack[stringStackPtr++] = "";
					integerStack[integerStackPtr++] = 0;
					integerStack[integerStackPtr++] = 0;
					stringStack[stringStackPtr++] = "";
					return;
				}
				return;
			}
			if (buttonId == 6507) {
				integerStackPtr -= 4;
				int i_454_ = integerStack[integerStackPtr];
				boolean bool_455_ = integerStack[integerStackPtr + 1] == 1;
				int i_456_ = integerStack[integerStackPtr + 2];
				boolean bool_457_ = integerStack[integerStackPtr + 3] == 1;
				Class287_Sub2.method3393(bool_457_, i_456_, i_454_, (byte) 82, bool_455_);
				return;
			}
			if (buttonId == 6508) {
				Class98_Sub10_Sub25.method1080((byte) 74);
				return;
			}
			if (buttonId == 6509) {
				if (client.clientState == 7) {
					Class224_Sub3_Sub1.aBoolean6144 = integerStack[--integerStackPtr] == 1;
					return;
				}
				return;
			}
			if (buttonId == 6510) {
				integerStack[integerStackPtr++] = client.worldFlags;
				return;
			}
		} else if (buttonId >= 6700) {
			if (buttonId < 6800 && client.buildType == BuildType.WIP) {
				if (buttonId == 6700) {
					int i_458_ = Class116.attachmentMap.size((byte) -6);
					if (client.topLevelInterfaceId != -1) {
						i_458_++;
					}
					integerStack[integerStackPtr++] = i_458_;
					return;
				}
				if (buttonId == 6701) {
					int i_459_ = integerStack[--integerStackPtr];
					if (client.topLevelInterfaceId != -1) {
						if (i_459_ == 0) {
							integerStack[integerStackPtr++] = client.topLevelInterfaceId;
							return;
						}
						i_459_--;
					}
					RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(98);
					while (i_459_-- > 0) {
						class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.iterateNext(-1);
					}
					integerStack[integerStackPtr++] = class98_sub18.attachedInterfaceId;
					return;
				}
				if (buttonId == 6702) {
					int i_460_ = integerStack[--integerStackPtr];
					if (Class159.interfaceStore[i_460_] == null) {
						stringStack[stringStackPtr++] = "";
					} else {
						String string = Class159.interfaceStore[i_460_][0].aString2231;
						if (string == null) {
							stringStack[stringStackPtr++] = "";
						} else {
							stringStack[stringStackPtr++] = string.substring(0, string.indexOf(':'));
							return;
						}
						return;
					}
					return;
				}
				if (buttonId == 6703) {
					int i_461_ = integerStack[--integerStackPtr];
					if (Class159.interfaceStore[i_461_] == null) {
						integerStack[integerStackPtr++] = 0;
					} else {
						integerStack[integerStackPtr++] = Class159.interfaceStore[i_461_].length;
						return;
					}
					return;
				}
				if (buttonId == 6704) {
					integerStackPtr -= 2;
					int i_462_ = integerStack[integerStackPtr];
					int i_463_ = integerStack[integerStackPtr + 1];
					if (Class159.interfaceStore[i_462_] == null) {
						stringStack[stringStackPtr++] = "";
					} else {
						String string = Class159.interfaceStore[i_462_][i_463_].aString2231;
						if (string == null) {
							stringStack[stringStackPtr++] = "";
						} else {
							stringStack[stringStackPtr++] = string;
							return;
						}
						return;
					}
					return;
				}
				if (buttonId == 6705) {
					integerStackPtr -= 2;
					int i_464_ = integerStack[integerStackPtr];
					int i_465_ = integerStack[integerStackPtr + 1];
					if (Class159.interfaceStore[i_464_] == null) {
						integerStack[integerStackPtr++] = 0;
					} else {
						integerStack[integerStackPtr++] = Class159.interfaceStore[i_464_][i_465_].anInt2259;
						return;
					}
					return;
				}
				if (buttonId == 6706) {
					return;
				}
				if (buttonId == 6707) {
					integerStackPtr -= 3;
					int i_466_ = integerStack[integerStackPtr];
					int i_467_ = integerStack[integerStackPtr + 1];
					int i_468_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(1, i_468_, "", -121, i_466_ << 16 | i_467_);
					return;
				}
				if (buttonId == 6708) {
					integerStackPtr -= 3;
					int i_469_ = integerStack[integerStackPtr];
					int i_470_ = integerStack[integerStackPtr + 1];
					int i_471_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(2, i_471_, "", -117, i_469_ << 16 | i_470_);
					return;
				}
				if (buttonId == 6709) {
					integerStackPtr -= 3;
					int i_472_ = integerStack[integerStackPtr];
					int i_473_ = integerStack[integerStackPtr + 1];
					int i_474_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(3, i_474_, "", -126, i_472_ << 16 | i_473_);
					return;
				}
				if (buttonId == 6710) {
					integerStackPtr -= 3;
					int i_475_ = integerStack[integerStackPtr];
					int i_476_ = integerStack[integerStackPtr + 1];
					int i_477_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(4, i_477_, "", -121, i_475_ << 16 | i_476_);
					return;
				}
				if (buttonId == 6711) {
					integerStackPtr -= 3;
					int i_478_ = integerStack[integerStackPtr];
					int i_479_ = integerStack[integerStackPtr + 1];
					int i_480_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(5, i_480_, "", -117, i_478_ << 16 | i_479_);
					return;
				}
				if (buttonId == 6712) {
					integerStackPtr -= 3;
					int i_481_ = integerStack[integerStackPtr];
					int i_482_ = integerStack[integerStackPtr + 1];
					int i_483_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(6, i_483_, "", -118, i_481_ << 16 | i_482_);
					return;
				}
				if (buttonId == 6713) {
					integerStackPtr -= 3;
					int i_484_ = integerStack[integerStackPtr];
					int i_485_ = integerStack[integerStackPtr + 1];
					int i_486_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(7, i_486_, "", -125, i_484_ << 16 | i_485_);
					return;
				}
				if (buttonId == 6714) {
					integerStackPtr -= 3;
					int i_487_ = integerStack[integerStackPtr];
					int i_488_ = integerStack[integerStackPtr + 1];
					int i_489_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(8, i_489_, "", -117, i_487_ << 16 | i_488_);
					return;
				}
				if (buttonId == 6715) {
					integerStackPtr -= 3;
					int i_490_ = integerStack[integerStackPtr];
					int i_491_ = integerStack[integerStackPtr + 1];
					int i_492_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(9, i_492_, "", -115, i_490_ << 16 | i_491_);
					return;
				}
				if (buttonId == 6716) {
					integerStackPtr -= 3;
					int i_493_ = integerStack[integerStackPtr];
					int i_494_ = integerStack[integerStackPtr + 1];
					int i_495_ = integerStack[integerStackPtr + 2];
					Class303.handleKeysPressed(10, i_495_, "", -126, i_493_ << 16 | i_494_);
					return;
				}
				if (buttonId == 6717) {
					integerStackPtr -= 3;
					int i_496_ = integerStack[integerStackPtr];
					int i_497_ = integerStack[integerStackPtr + 1];
					int i_498_ = integerStack[integerStackPtr + 2];
					RtInterface class293 = Class246_Sub9.getDynamicComponent((byte) 72, i_496_ << 16 | i_497_, i_498_);
					Class98_Sub10_Sub32.method1098((byte) 117);
					Class98_Sub49 class98_sub49 = client.method116(class293);
					Class98_Sub5_Sub2.method970(class98_sub49.anInt4285, class293, class98_sub49.method1668(-1), -6838);
					return;
				}
			} else if (buttonId < 6900) {
				if (buttonId == 6800) {
					int i_499_ = integerStack[--integerStackPtr];
					WorldMapInfoDefinition class24 = Class216.worldMapInfoDefinitionList.get(-114, i_499_);
					if (class24.locationName == null) {
						stringStack[stringStackPtr++] = "";
					} else {
						stringStack[stringStackPtr++] = class24.locationName;
						return;
					}
					return;
				}
				if (buttonId == 6801) {
					int i_500_ = integerStack[--integerStackPtr];
					WorldMapInfoDefinition class24 = Class216.worldMapInfoDefinitionList.get(106, i_500_);
					integerStack[integerStackPtr++] = class24.anInt245;
					return;
				}
				if (buttonId == 6802) {
					int i_501_ = integerStack[--integerStackPtr];
					WorldMapInfoDefinition class24 = Class216.worldMapInfoDefinitionList.get(-54, i_501_);
					integerStack[integerStackPtr++] = class24.anInt264;
					return;
				}
				if (buttonId == 6803) {
					int i_502_ = integerStack[--integerStackPtr];
					WorldMapInfoDefinition class24 = Class216.worldMapInfoDefinitionList.get(-47, i_502_);
					integerStack[integerStackPtr++] = class24.anInt246;
					return;
				}
				if (buttonId == 6804) {
					integerStackPtr -= 2;
					int i_503_ = integerStack[integerStackPtr];
					int i_504_ = integerStack[integerStackPtr + 1];
					ParamDefinition class149 = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, i_504_);
					if (class149.isString(false)) {
						stringStack[stringStackPtr++] = Class216.worldMapInfoDefinitionList.get(105, i_503_).method289(-5911, class149.defaultString, i_504_);
					} else {
						integerStack[integerStackPtr++] = Class216.worldMapInfoDefinitionList.get(122, i_503_).method285(48, i_504_, class149.defaultInteger);
						return;
					}
					return;
				}
			} else if (buttonId < 7000) {
				if (buttonId == 6900) {
					integerStack[integerStackPtr++] = Js5Manager.aBoolean933 && !Class98_Sub10_Sub35.aBoolean5732 ? 1 : 0;
					return;
				}
				if (buttonId == 6901) {
					integerStack[integerStackPtr++] = Class48.anInt409;
					return;
				}
				if (buttonId == 6902) {
					integerStack[integerStackPtr++] = Class98_Sub1.anInt3814;
					return;
				}
				if (buttonId == 6903) {
					integerStack[integerStackPtr++] = DecoratedProgressBarLSEConfig.anInt5489;
					return;
				}
				if (buttonId == 6904) {
					integerStack[integerStackPtr++] = Class98_Sub10_Sub19.anInt5630;
					return;
				}
				if (buttonId == 6905) {
					String string = "";
					if (Class187.aClass143_1449 != null) {
						if (Class187.aClass143_1449.result != null) {
							string = (String) Class187.aClass143_1449.result;
						} else {
							string = Class98_Sub10_Sub39.method1122(Class187.aClass143_1449.intParameter, (byte) -63);
						}
					}
					stringStack[stringStackPtr++] = string;
					return;
				}
				if (buttonId == 6906) {
					integerStack[integerStackPtr++] = DataFs.anInt203;
					return;
				}
				if (buttonId == 6907) {
					integerStack[integerStackPtr++] = Cacheable.anInt4264;
					return;
				}
				if (buttonId == 6908) {
					integerStack[integerStackPtr++] = SpriteProgressBarLSEConfig.anInt5491;
					return;
				}
				if (buttonId == 6909) {
					integerStack[integerStackPtr++] = OpenGlGround.aBoolean5200 ? 1 : 0;
					return;
				}
				if (buttonId == 6910) {
					integerStack[integerStackPtr++] = ActionGroup.anInt6003;
					return;
				}
				if (buttonId == 6911) {
					integerStack[integerStackPtr++] = Class98_Sub43_Sub2.anInt5910;
					return;
				}
				if (buttonId == 6912) {
					integerStack[integerStackPtr++] = Class36.anInt349;
					return;
				}
			} else if (buttonId < 7100) {
				if (buttonId == 7000) {
					int i_505_ = Class98_Sub28.method1306((byte) -106);
					integerStack[integerStackPtr++] = Class98_Sub10_Sub24.anInt5671 = client.preferences.currentToolkit.getValue((byte) 120);
					integerStack[integerStackPtr++] = i_505_;
					Class98_Sub10.method999((byte) 122);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
					return;
				}
				if (buttonId == 7001) {
					GamePreferences.method1284(1);
					Class98_Sub10.method999((byte) -27);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
					return;
				}
				if (buttonId == 7002) {
					Class98_Sub50.method1672((byte) 19);
					Class98_Sub10.method999((byte) 113);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
					return;
				}
				if (buttonId == 7003) {
					Class287.setClientPreferences((byte) 27);
					Class98_Sub10.method999((byte) -126);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
					return;
				}
				if (buttonId == 7004) {
					GraphicsBuffer.method1436(-100, true);
					Class98_Sub10.method999((byte) -10);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
					return;
				}
				if (buttonId == 7005) {
					client.preferences.setPreference((byte) -13, 0, client.preferences.graphicsLevel);
					Class310.method3618(-5964);
					OpenGlGround.aBoolean5207 = false;
					return;
				}
				if (buttonId == 7006) {
					if (Class98_Sub10_Sub24.anInt5671 == 2) {
						Class98_Sub10_Sub38.aBoolean5756 = true;
					} else {
						if (Class98_Sub10_Sub24.anInt5671 == 1) {
							Class95.aBoolean798 = true;
						} else {
							if (Class98_Sub10_Sub24.anInt5671 == 3) {
								Class67.aBoolean520 = true;
							}
							return;
						}
						return;
					}
					return;
				}
				if (buttonId == 7007) {
					integerStack[integerStackPtr++] = client.preferences.graphicsLevel.getValue((byte) 123);
					return;
				}
			} else if (buttonId < 7200) {
				if (buttonId == 7100) {
					integerStackPtr -= 2;
					int i_506_ = integerStack[integerStackPtr];
					int i_507_ = integerStack[integerStackPtr + 1];
					if (i_506_ != -1) {
						if (i_507_ > 255) {
							i_507_ = 255;
						} else if (i_507_ < 0) {
							i_507_ = 0;
						}
						Class98_Sub10_Sub30.method1093(-29680, i_507_, false, i_506_);
					}
					return;
				}
				if (buttonId == 7101) {
					int i_508_ = integerStack[--integerStackPtr];
					if (i_508_ != -1) {
						OutgoingPacket.method1127((byte) 67, i_508_);
					}
					return;
				}
				if (buttonId == 7102) {
					int i_509_ = integerStack[--integerStackPtr];
					if (i_509_ != -1) {
						Class98_Sub42.method1476(256, i_509_);
					}
					return;
				}
				if (buttonId == 7103) {
					integerStack[integerStackPtr++] = Class98_Sub42.contains("jagtheora", 0) ? 1 : 0;
					return;
				}
			} else if (buttonId < 7300) {
				if (buttonId == 7201) {
					integerStack[integerStackPtr++] = client.preferences.groundDecoration.method597(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7202) {
					integerStack[integerStackPtr++] = client.preferences.characterShadows.method661(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7203) {
					integerStack[integerStackPtr++] = client.preferences.sceneryShadows.method581(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7204) {
					integerStack[integerStackPtr++] = client.preferences.waterDetail.method671(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7205) {
					integerStack[integerStackPtr++] = client.preferences.antiAliasing.method647(-1) && client.graphicsToolkit.method1823() ? 1 : 0;
					return;
				}
				if (buttonId == 7206) {
					integerStack[integerStackPtr++] = client.preferences.particles.supported(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7207) {
					integerStack[integerStackPtr++] = client.preferences.buildArea.method621(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7208) {
					integerStack[integerStackPtr++] = client.preferences.unknown3.method571(-1) && client.graphicsToolkit.method1767() ? 1 : 0;
					return;
				}
				if (buttonId == 7209) {
					integerStack[integerStackPtr++] = client.preferences.groundBlending.method657(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7210) {
					integerStack[integerStackPtr++] = client.preferences.textures.method635(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7211) {
					integerStack[integerStackPtr++] = client.preferences.maxScreenSize.method613(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7212) {
					integerStack[integerStackPtr++] = client.preferences.fog.method607(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7213) {
					integerStack[integerStackPtr++] = client.preferences.orthoZoom.method626(-1) ? 1 : 0;
					return;
				}
				if (buttonId == 7214) {
					integerStack[integerStackPtr++] = client.preferences.desiredToolkit.method587(-1) ? 1 : 0;
					return;
				}
			} else if (buttonId < 7400) {
				if (buttonId == 7301) {
					int i_510_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.groundDecoration.compatible(i_510_, 29053);
					return;
				}
				if (buttonId == 7302) {
					int i_511_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.characterShadows.compatible(i_511_, 29053);
					return;
				}
				if (buttonId == 7303) {
					int i_512_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.sceneryShadows.compatible(i_512_, 29053);
					return;
				}
				if (buttonId == 7304) {
					int i_513_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.waterDetail.compatible(i_513_, 29053);
					return;
				}
				if (buttonId == 7305) {
					int i_514_ = integerStack[--integerStackPtr];
					if (!client.graphicsToolkit.method1823()) {
						integerStack[integerStackPtr++] = 3;
					} else {
						integerStack[integerStackPtr++] = client.preferences.antiAliasing.compatible(i_514_, 29053);
						return;
					}
					return;
				}
				if (buttonId == 7306) {
					int i_515_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.particles.compatible(i_515_, 29053);
					return;
				}
				if (buttonId == 7307) {
					int i_516_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.buildArea.compatible(i_516_, 29053);
					return;
				}
				if (buttonId == 7308) {
					int i_517_ = integerStack[--integerStackPtr];
					if (!client.graphicsToolkit.method1767()) {
						integerStack[integerStackPtr++] = 3;
					} else {
						integerStack[integerStackPtr++] = client.preferences.unknown3.compatible(i_517_, 29053);
						return;
					}
					return;
				}
				if (buttonId == 7309) {
					int i_518_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.groundBlending.compatible(i_518_, 29053);
					return;
				}
				if (buttonId == 7310) {
					int i_519_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.textures.compatible(i_519_, 29053);
					return;
				}
				if (buttonId == 7311) {
					int i_520_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.maxScreenSize.compatible(i_520_, 29053);
					return;
				}
				if (buttonId == 7312) {
					int i_521_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.fog.compatible(i_521_, 29053);
					return;
				}
				if (buttonId == 7313) {
					int i_522_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.orthoZoom.compatible(i_522_, 29053);
					return;
				}
				if (buttonId == 7314) {
					int i_523_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = client.preferences.desiredToolkit.compatible(i_523_, 29053);
					return;
				}
			}
		}
		throw new IllegalStateException(String.valueOf(buttonId));
	}

	public static final void handleEvent(ClientScript2Event class98_sub21) {
		method3150(class98_sub21, 200000);
	}

	private static final void method3142(int i) {
		RtInterface class293 = RtInterface.getInterface(i, -9820);
		if (class293 != null) {
			int i_0_ = i >>> 16;
			RtInterface[] class293s = FlickeringEffectsPreferenceField.interfaceOverride[i_0_];
			if (class293s == null) {
				RtInterface[] class293s_1_ = Class159.interfaceStore[i_0_];
				int i_2_ = class293s_1_.length;
				class293s = FlickeringEffectsPreferenceField.interfaceOverride[i_0_] = new RtInterface[i_2_];
				ArrayUtils.method2892(class293s_1_, 0, class293s, 0, class293s_1_.length);
			}
			int i_3_;
			for (i_3_ = 0; i_3_ < class293s.length; i_3_++) {
				if (class293s[i_3_] == class293) {
					break;
				}
			}
			if (i_3_ < class293s.length) {
				ArrayUtils.method2892(class293s, i_3_ + 1, class293s, i_3_, class293s.length - i_3_ - 1);
				class293s[class293s.length - 1] = class293;
			}
		}
	}

	public static final void method3143(int i, boolean bool) {
		/* empty */
	}

	private static final void method3145(int i) {
		RtInterface class293 = RtInterface.getInterface(i, -9820);
		if (class293 != null) {
			int i_4_ = i >>> 16;
			RtInterface[] class293s = FlickeringEffectsPreferenceField.interfaceOverride[i_4_];
			if (class293s == null) {
				RtInterface[] class293s_5_ = Class159.interfaceStore[i_4_];
				int i_6_ = class293s_5_.length;
				class293s = FlickeringEffectsPreferenceField.interfaceOverride[i_4_] = new RtInterface[i_6_];
				ArrayUtils.method2892(class293s_5_, 0, class293s, 0, class293s_5_.length);
			}
			int i_7_;
			for (i_7_ = 0; i_7_ < class293s.length; i_7_++) {
				if (class293s[i_7_] == class293) {
					break;
				}
			}
			if (i_7_ < class293s.length) {
				ArrayUtils.method2892(class293s, 0, class293s, 1, i_7_);
				class293s[0] = class293;
			}
		}
	}

	private static final int method3146(char c) {
		if (Class184.method2627(376, c)) {
			return 1;
		}
		return 0;
	}

	public static final void method3147() {
		/* empty */
	}

	private static final void method3148(int opcode, boolean useDot) {
		if (opcode < 300) {
			if (opcode == 100) {
				integerStackPtr -= 3;
				int interfaceId = integerStack[integerStackPtr];
				int newType = integerStack[integerStackPtr + 1];
				int subInterfaceIdx = integerStack[integerStackPtr + 2];
				if (newType == 0) {
					throw new RuntimeException();
				}
				RtInterface parent = RtInterface.getInterface(interfaceId, -9820);
				if (parent.dynamicComponents == null) {
					parent.dynamicComponents = new RtInterface[subInterfaceIdx + 1];
				}
				if (parent.dynamicComponents.length <= subInterfaceIdx) {
					RtInterface[] class293s = new RtInterface[subInterfaceIdx + 1];
					for (int i_11_ = 0; i_11_ < parent.dynamicComponents.length; i_11_++) {
						class293s[i_11_] = parent.dynamicComponents[i_11_];
					}
					parent.dynamicComponents = class293s;
				}
				if (subInterfaceIdx > 0 && parent.dynamicComponents[subInterfaceIdx - 1] == null) {
					throw new RuntimeException("Gap at:" + (subInterfaceIdx - 1));
				}
				RtInterface newComponent = new RtInterface();
				newComponent.type = newType;
				newComponent.parentId = newComponent.idDword = parent.idDword;
				newComponent.componentIndex = subInterfaceIdx;
				parent.dynamicComponents[subInterfaceIdx] = newComponent;
				if (useDot) {
					aClass293_1877 = newComponent;
				} else {
					aClass293_1879 = newComponent;
				}
				WorldMapInfoDefinitionParser.setDirty(1, parent);
				return;
			}
			if (opcode == 101) {
				RtInterface class293 = useDot ? aClass293_1877 : aClass293_1879;
				if (class293.componentIndex == -1) {
					if (useDot) {
						throw new RuntimeException("Tried to .cc_delete static .active-component!");
					}
					throw new RuntimeException("Tried to cc_delete static active-component!");
				}
				RtInterface class293_13_ = RtInterface.getInterface(class293.idDword, -9820);
				class293_13_.dynamicComponents[class293.componentIndex] = null;
				WorldMapInfoDefinitionParser.setDirty(1, class293_13_);
				return;
			}
			if (opcode == 102) {
				RtInterface class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
				class293.dynamicComponents = null;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 200) {
				integerStackPtr -= 2;
				int i_14_ = integerStack[integerStackPtr];
				int i_15_ = integerStack[integerStackPtr + 1];
				RtInterface class293 = Class246_Sub9.getDynamicComponent((byte) 72, i_14_, i_15_);
				if (class293 != null && i_15_ != -1) {
					integerStack[integerStackPtr++] = 1;
					if (useDot) {
						aClass293_1877 = class293;
					} else {
						aClass293_1879 = class293;
					}
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
			if (opcode == 201) {
				int i_16_ = integerStack[--integerStackPtr];
				RtInterface class293 = RtInterface.getInterface(i_16_, -9820);
				if (class293 != null) {
					integerStack[integerStackPtr++] = 1;
					if (useDot) {
						aClass293_1877 = class293;
					} else {
						aClass293_1879 = class293;
					}
				} else {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				return;
			}
			if (opcode == 202) {
				int i_17_ = integerStack[--integerStackPtr];
				method3142(i_17_);
				return;
			}
			if (opcode == 203) {
				int i_18_ = integerStack[--integerStackPtr];
				method3145(i_18_);
				return;
			}
		} else if (opcode < 500) {
			if (opcode == 403) {
				integerStackPtr -= 2;
				int i_19_ = integerStack[integerStackPtr];
				int i_20_ = integerStack[integerStackPtr + 1];
				if (Class87.localPlayer.appearence != null) {
					for (int index = 0; index < GraphicsDefinitionParser.maleParts.length; index++) {
						if (GraphicsDefinitionParser.maleParts[index] == i_19_) {
							Class87.localPlayer.appearence.setKit(12, index, ParamDefinition.identikitDefinitionList, i_20_);
							return;
						}
					}
					for (int index = 0; index < Class49.femaleParts.length; index++) {
						if (Class49.femaleParts[index] == i_19_) {
							Class87.localPlayer.appearence.setKit(12, index, ParamDefinition.identikitDefinitionList, i_20_);
							break;
						}
					}
					return;
				}
				return;
			}
			if (opcode == 404) {
				integerStackPtr -= 2;
				int i_23_ = integerStack[integerStackPtr];
				int i_24_ = integerStack[integerStackPtr + 1];
				if (Class87.localPlayer.appearence != null) {
					Class87.localPlayer.appearence.setColour(i_24_, i_23_, -9);
					return;
				}
				return;
			}
			if (opcode == 410) {
				boolean bool_25_ = integerStack[--integerStackPtr] != 0;
				if (Class87.localPlayer.appearence != null) {
					Class87.localPlayer.appearence.setMale(bool_25_, false);
					return;
				}
				return;
			}
			if (opcode == 411) {
				integerStackPtr -= 2;
				int i_26_ = integerStack[integerStackPtr];
				int i_27_ = integerStack[integerStackPtr + 1];
				if (Class87.localPlayer.appearence != null) {
					Class87.localPlayer.appearence.method3634(i_27_, i_26_, Class98_Sub46_Sub19.itemDefinitionList, 1073741824);
					return;
				}
				return;
			}
		} else if (opcode >= 1000 && opcode < 1100 || opcode >= 2000 && opcode < 2100) {
			RtInterface class293;
			if (opcode >= 2000) {
				opcode -= 1000;
				class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
			} else {
				class293 = useDot ? aClass293_1877 : aClass293_1879;
			}
			if (opcode == 1000) {
				integerStackPtr -= 4;
				class293.xPos = integerStack[integerStackPtr];
				class293.yPos = integerStack[integerStackPtr + 1];
				int i_28_ = integerStack[integerStackPtr + 2];
				if (i_28_ < 0) {
					i_28_ = 0;
				} else if (i_28_ > 5) {
					i_28_ = 5;
				}
				int i_29_ = integerStack[integerStackPtr + 3];
				if (i_29_ < 0) {
					i_29_ = 0;
				} else if (i_29_ > 5) {
					i_29_ = 5;
				}
				class293.hPosMode = (byte) i_28_;
				class293.vPosMode = (byte) i_29_;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				RtInterface.updateLayout(class293, true);
				if (class293.componentIndex == -1) {
					Class224_Sub2_Sub1.method2838(11, class293.idDword);
				}
				return;
			}
			if (opcode == 1001) {
				integerStackPtr -= 4;
				class293.width = integerStack[integerStackPtr];
				class293.height = integerStack[integerStackPtr + 1];
				class293.viewportWidth = 0;
				class293.viewportHeight = 0;
				int i_30_ = integerStack[integerStackPtr + 2];
				if (i_30_ < 0) {
					i_30_ = 0;
				} else if (i_30_ > 4) {
					i_30_ = 4;
				}
				int i_31_ = integerStack[integerStackPtr + 3];
				if (i_31_ < 0) {
					i_31_ = 0;
				} else if (i_31_ > 4) {
					i_31_ = 4;
				}
				class293.hSizeMode = (byte) i_30_;
				class293.vSizeMode = (byte) i_31_;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				RtInterface.updateLayout(class293, true);
				if (class293.type == 0) {
					RtInterface.calculateLayout(class293, false, (byte) 118);
				}
				return;
			}
			if (opcode == 1003) {
				boolean bool_32_ = integerStack[--integerStackPtr] == 1;
				if (class293.aBoolean2295 != bool_32_) {
					class293.aBoolean2295 = bool_32_;
					WorldMapInfoDefinitionParser.setDirty(1, class293);
				}
				if (class293.componentIndex == -1) {
					Class98_Sub10_Sub18.method1056((byte) 97, class293.idDword);
				}
				return;
			}
			if (opcode == 1004) {
				integerStackPtr -= 2;
				class293.anInt2321 = integerStack[integerStackPtr];
				class293.anInt2338 = integerStack[integerStackPtr + 1];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				RtInterface.updateLayout(class293, true);
				if (class293.type == 0) {
					RtInterface.calculateLayout(class293, false, (byte) -112);
				}
				return;
			}
			if (opcode == 1005) {
				class293.aBoolean2286 = integerStack[--integerStackPtr] == 1;
				return;
			}
		} else if (opcode >= 1100 && opcode < 1200 || opcode >= 2100 && opcode < 2200) {
			RtInterface class293;
			if (opcode >= 2000) {
				opcode -= 1000;
				class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
			} else {
				class293 = useDot ? aClass293_1877 : aClass293_1879;
			}
			if (opcode == 1100) {
				integerStackPtr -= 2;
				class293.scrollX = integerStack[integerStackPtr];
				if (class293.scrollX > class293.scrollWidth - class293.renderWidth) {
					class293.scrollX = class293.scrollWidth - class293.renderWidth;
				}
				if (class293.scrollX < 0) {
					class293.scrollX = 0;
				}
				class293.scrollY = integerStack[integerStackPtr + 1];
				if (class293.scrollY > class293.scrollHeight - class293.renderHeight) {
					class293.scrollY = class293.scrollHeight - class293.renderHeight;
				}
				if (class293.scrollY < 0) {
					class293.scrollY = 0;
				}
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				if (class293.componentIndex == -1) {
					Class21_Sub4.method279(16953, class293.idDword);
				}
				return;
			}
			if (opcode == 1101) {
				class293.defaultColour = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				if (class293.componentIndex == -1) {
					Class98_Sub16.method1147(124, class293.idDword);
				}
				return;
			}
			if (opcode == 1102) {
				class293.filled = integerStack[--integerStackPtr] == 1;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1103) {
				class293.alpha = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1104) {
				class293.lineWidth = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1105) {
				int i_33_ = integerStack[--integerStackPtr];
				if (class293.defaultImage != i_33_) {
					class293.defaultImage = i_33_;
					WorldMapInfoDefinitionParser.setDirty(1, class293);
				}
				if (class293.componentIndex == -1) {
					RtKeyListener.method778(44, class293.idDword);
				}
				return;
			}
			if (opcode == 1106) {
				class293.imageRotation = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1107) {
				class293.imageRepeat = integerStack[--integerStackPtr] == 1;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1108) {
				class293.defaultMediaType = 1;
				class293.defaultMediaId = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				if (class293.componentIndex == -1) {
					AnimationDefinitionParser.method2625(false, class293.idDword);
				}
				return;
			}
			if (opcode == 1109) {
				integerStackPtr -= 6;
				class293.xOffset2D = integerStack[integerStackPtr];
				class293.yOffset2D = integerStack[integerStackPtr + 1];
				class293.rotationX = integerStack[integerStackPtr + 2];
				class293.rotationY = integerStack[integerStackPtr + 3];
				class293.rotationZ = integerStack[integerStackPtr + 4];
				class293.zoom = integerStack[integerStackPtr + 5];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				if (class293.componentIndex == -1) {
					Class290.method3413(0, class293.idDword);
					Class185.method2631(10, class293.idDword);
				}
				return;
			}
			if (opcode == 1110) {
				int i_34_ = integerStack[--integerStackPtr];
				if (class293.defaultAnimation != i_34_) {
					class293.defaultAnimation = i_34_;
					class293.anInt2303 = 0;
					class293.anInt2287 = 1;
					class293.anInt2312 = 0;
					AnimationDefinition class97 = class293.defaultAnimation == -1 ? null : Class151_Sub7.animationDefinitionList.method2623(class293.defaultAnimation, 16383);
					if (class97 != null) {
						QuickChatMessageParser.method3327(class293.anInt2303, class97, (byte) 74);
					}
					WorldMapInfoDefinitionParser.setDirty(1, class293);
				}
				if (class293.componentIndex == -1) {
					Class119_Sub3.method2185(5, class293.idDword);
				}
				return;
			}
			if (opcode == 1111) {
				class293.aBoolean2265 = integerStack[--integerStackPtr] == 1;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1112) {
				String string = stringStack[--stringStackPtr];
				if (!string.equals(class293.defaultText)) {
					class293.defaultText = string;
					WorldMapInfoDefinitionParser.setDirty(1, class293);
				}
				if (class293.componentIndex == -1) {
					VertexNormal.method3380(3, class293.idDword);
				}
				return;
			}
			if (opcode == 1113) {
				class293.fontId = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				if (class293.componentIndex == -1) {
					Class40.method361(class293.idDword, 0);
				}
				return;
			}
			if (opcode == 1114) {
				integerStackPtr -= 3;
				class293.horizontalTextAlign = integerStack[integerStackPtr];
				class293.verticalTextAlign = integerStack[integerStackPtr + 1];
				class293.lineHeight = integerStack[integerStackPtr + 2];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1115) {
				class293.shaded = integerStack[--integerStackPtr] == 1;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1116) {
				class293.rotation = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1117) {
				class293.backgroundColour = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1118) {
				class293.flipVertical = integerStack[--integerStackPtr] == 1;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1119) {
				class293.flipHorizontal = integerStack[--integerStackPtr] == 1;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1120) {
				integerStackPtr -= 2;
				class293.scrollWidth = integerStack[integerStackPtr];
				class293.scrollHeight = integerStack[integerStackPtr + 1];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				if (class293.type == 0) {
					RtInterface.calculateLayout(class293, false, (byte) 60);
				}
				return;
			}
			if (opcode == 1122) {
				class293.aBoolean2279 = integerStack[--integerStackPtr] == 1;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1123) {
				class293.zoom = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				if (class293.componentIndex == -1) {
					Class290.method3413(0, class293.idDword);
				}
				return;
			}
			if (opcode == 1124) {
				int i_35_ = integerStack[--integerStackPtr];
				class293.lineMirrored = i_35_ == 1;
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1125) {
				integerStackPtr -= 2;
				class293.viewportX = integerStack[integerStackPtr];
				class293.viewportY = integerStack[integerStackPtr + 1];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1126) {
				class293.lineCount = integerStack[--integerStackPtr];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1127) {
				integerStackPtr -= 2;
				int i_36_ = integerStack[integerStackPtr];
				int i_37_ = integerStack[integerStackPtr + 1];
				ParamDefinition class149 = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, i_36_);
				if (i_37_ != class149.defaultInteger) {
					class293.method3455(i_36_, i_37_, 16);
				} else {
					class293.removeParam((byte) 54, i_36_);
					return;
				}
				return;
			}
			if (opcode == 1128) {
				int i_38_ = integerStack[--integerStackPtr];
				String string = stringStack[--stringStackPtr];
				ParamDefinition class149 = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, i_38_);
				if (!class149.defaultString.equals(string)) {
					class293.setParam(string, 16, i_38_);
				} else {
					class293.removeParam((byte) 101, i_38_);
					return;
				}
				return;
			}
			if (opcode == 1129 || opcode == 1130) {
				int i_39_ = integerStack[--integerStackPtr];
				if ((class293.type == 5 || opcode != 1129) && (class293.type == 4 || opcode != 1130)) {
					if (class293.videoId != i_39_) {
						class293.videoId = i_39_;
						WorldMapInfoDefinitionParser.setDirty(1, class293);
					}
					if (class293.componentIndex == -1) {
						DecoratedProgressBarLoadingScreenElement.method3969(121, class293.idDword);
					}
					return;
				}
				return;
			}
		} else if (opcode >= 1200 && opcode < 1300 || opcode >= 2200 && opcode < 2300) {
			RtInterface class293;
			if (opcode >= 2000) {
				opcode -= 1000;
				class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
			} else {
				class293 = useDot ? aClass293_1877 : aClass293_1879;
			}
			WorldMapInfoDefinitionParser.setDirty(1, class293);
			if (opcode == 1200 || opcode == 1205 || opcode == 1208 || opcode == 1209 || opcode == 1212 || opcode == 1213) {
				integerStackPtr -= 2;
				int i_40_ = integerStack[integerStackPtr];
				int i_41_ = integerStack[integerStackPtr + 1];
				if (class293.componentIndex == -1) {
					Class21_Sub2.method274((byte) 83, class293.idDword);
					Class290.method3413(0, class293.idDword);
					Class185.method2631(10, class293.idDword);
				}
				if (i_40_ == -1) {
					class293.defaultMediaType = 1;
					class293.defaultMediaId = -1;
					class293.unknown = -1;
				} else {
					class293.unknown = i_40_;
					class293.itemStackSize = i_41_;
					class293.objectUsePlayerAppearence = opcode == 1208 || opcode == 1209;
					ItemDefinition class297 = Class98_Sub46_Sub19.itemDefinitionList.get(i_40_, (byte) -120);
					class293.rotationX = class297.xAngle2D;
					class293.rotationY = class297.yAngle2D;
					class293.rotationZ = class297.zAngle2D;
					class293.xOffset2D = class297.xOffset2D;
					class293.yOffset2D = class297.yOffset2D;
					class293.zoom = class297.zoom2D;
					if (opcode == 1205 || opcode == 1209) {
						class293.anInt2305 = 0;
					} else if (opcode == 1212 || opcode == 1213) {
						class293.anInt2305 = 1;
					} else {
						class293.anInt2305 = 2;
					}
					if (class293.viewportWidth > 0) {
						class293.zoom = class293.zoom * 32 / class293.viewportWidth;
					} else {
						if (class293.width > 0) {
							class293.zoom = class293.zoom * 32 / class293.width;
						}
						return;
					}
					return;
				}
				return;
			}
			if (opcode == 1201) {
				class293.defaultMediaType = 2;
				class293.defaultMediaId = integerStack[--integerStackPtr];
				if (class293.componentIndex == -1) {
					AnimationDefinitionParser.method2625(false, class293.idDword);
				}
				return;
			}
			if (opcode == 1202) {
				class293.defaultMediaType = 3;
				class293.defaultMediaId = -1;
				if (class293.componentIndex == -1) {
					AnimationDefinitionParser.method2625(false, class293.idDword);
				}
				return;
			}
			if (opcode == 1203) {
				class293.defaultMediaType = 6;
				class293.defaultMediaId = integerStack[--integerStackPtr];
				if (class293.componentIndex == -1) {
					AnimationDefinitionParser.method2625(false, class293.idDword);
				}
				return;
			}
			if (opcode == 1204) {
				class293.defaultMediaType = 5;
				class293.defaultMediaId = integerStack[--integerStackPtr];
				if (class293.componentIndex == -1) {
					AnimationDefinitionParser.method2625(false, class293.idDword);
				}
				return;
			}
			if (opcode == 1206) {
				integerStackPtr -= 4;
				class293.objId = integerStack[integerStackPtr];
				class293.anInt2306 = integerStack[integerStackPtr + 1];
				class293.anInt2260 = integerStack[integerStackPtr + 2];
				class293.anInt2334 = integerStack[integerStackPtr + 3];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1207) {
				integerStackPtr -= 2;
				class293.anInt2216 = integerStack[integerStackPtr];
				class293.anInt2261 = integerStack[integerStackPtr + 1];
				WorldMapInfoDefinitionParser.setDirty(1, class293);
				return;
			}
			if (opcode == 1210) {
				integerStackPtr -= 4;
				class293.defaultMediaId = integerStack[integerStackPtr];
				class293.anInt2210 = integerStack[integerStackPtr + 1];
				if (integerStack[integerStackPtr + 2] == 1) {
					class293.defaultMediaType = 9;
				} else {
					class293.defaultMediaType = 8;
				}
				class293.objectUsePlayerAppearence = integerStack[integerStackPtr + 3] == 1;
				if (class293.componentIndex == -1) {
					AnimationDefinitionParser.method2625(false, class293.idDword);
				}
				return;
			}
			if (opcode == 1211) {
				class293.defaultMediaType = 5;
				class293.defaultMediaId = OpenGLHeap.localPlayerIndex;
				class293.anInt2210 = 0;
				if (class293.componentIndex == -1) {
					AnimationDefinitionParser.method2625(false, class293.idDword);
				}
				return;
			}
		} else if (opcode >= 1300 && opcode < 1400 || opcode >= 2300 && opcode < 2400) {
			RtInterface class293;
			if (opcode >= 2000) {
				opcode -= 1000;
				class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
			} else {
				class293 = useDot ? aClass293_1877 : aClass293_1879;
			}
			if (opcode == 1300) {
				int i_42_ = integerStack[--integerStackPtr] - 1;
				if (i_42_ < 0 || i_42_ > 9) {
					stringStackPtr--;
				} else {
					class293.setMenuAction(stringStack[--stringStackPtr], i_42_, 1);
					return;
				}
				return;
			}
			if (opcode == 1301) {
				integerStackPtr -= 2;
				int i_43_ = integerStack[integerStackPtr];
				int i_44_ = integerStack[integerStackPtr + 1];
				if (i_43_ == -1 && i_44_ == -1) {
					class293.aClass293_2219 = null;
				} else {
					class293.aClass293_2219 = Class246_Sub9.getDynamicComponent((byte) 72, i_43_, i_44_);
					return;
				}
				return;
			}
			if (opcode == 1302) {
				int i_45_ = integerStack[--integerStackPtr];
				if (i_45_ == Class369.anInt3129 || i_45_ == AsyncCache.anInt1929 || i_45_ == Class36.anInt350) {
					class293.anInt2289 = i_45_;
					return;
				}
				return;
			}
			if (opcode == 1303) {
				class293.anInt2308 = integerStack[--integerStackPtr];
				return;
			}
			if (opcode == 1304) {
				class293.anInt2353 = integerStack[--integerStackPtr];
				return;
			}
			if (opcode == 1305) {
				class293.applyText = stringStack[--stringStackPtr];
				return;
			}
			if (opcode == 1306) {
				class293.aString2214 = stringStack[--stringStackPtr];
				return;
			}
			if (opcode == 1307) {
				class293.menuActions = null;
				return;
			}
			if (opcode == 1308) {
				class293.anInt2318 = integerStack[--integerStackPtr];
				class293.anInt2309 = integerStack[--integerStackPtr];
				return;
			}
			if (opcode == 1309) {
				int i_46_ = integerStack[--integerStackPtr];
				int i_47_ = integerStack[--integerStackPtr];
				if (i_47_ >= 1 && i_47_ <= 10) {
					class293.method3474(-17972, i_47_ - 1, i_46_);
				}
				return;
			}
			if (opcode == 1310) {
				class293.aString2333 = stringStack[--stringStackPtr];
				return;
			}
			if (opcode == 1311) {
				class293.anInt2254 = integerStack[--integerStackPtr];
				return;
			}
			if (opcode == 1312 || opcode == 1313) {
				int i_48_;
				int i_49_;
				int i_50_;
				if (opcode == 1312) {
					integerStackPtr -= 3;
					i_48_ = integerStack[integerStackPtr] - 1;
					i_49_ = integerStack[integerStackPtr + 1];
					i_50_ = integerStack[integerStackPtr + 2];
					if (i_48_ < 0 || i_48_ > 9) {
						throw new RuntimeException("IOR13121313");
					}
				} else {
					integerStackPtr -= 2;
					i_48_ = 10;
					i_49_ = integerStack[integerStackPtr];
					i_50_ = integerStack[integerStackPtr + 1];
				}
				if (class293.keyCodes == null) {
					if (i_49_ != 0) {
						class293.keyCodes = new byte[11];
						class293.keyModifiers = new byte[11];
						class293.keyRepeat = new int[11];
					} else {
						return;
					}
				}
				class293.keyCodes[i_48_] = (byte) i_49_;
				if (i_49_ != 0) {
					class293.aBoolean2222 = true;
				} else {
					class293.aBoolean2222 = false;
					for (byte element : class293.keyCodes) {
						if (element != 0) {
							class293.aBoolean2222 = true;
							break;
						}
					}
				}
				class293.keyModifiers[i_48_] = (byte) i_50_;
				return;
			}
			if (opcode == 1314) {
				class293.anInt2317 = integerStack[--integerStackPtr];
				return;
			}
		} else {
			if (opcode >= 1400 && opcode < 1500 || opcode >= 2400 && opcode < 2500) {
				RtInterface class293;
				if (opcode >= 2000) {
					opcode -= 1000;
					class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
				} else {
					class293 = useDot ? aClass293_1877 : aClass293_1879;
				}
				if (opcode == 1499) {
					class293.removeEventHandlers(-1);
				} else {
					String string = stringStack[--stringStackPtr];
					int[] is = null;
					if (string.length() > 0 && string.charAt(string.length() - 1) == 'Y') {
						int i_52_ = integerStack[--integerStackPtr];
						if (i_52_ > 0) {
							is = new int[i_52_];
							while (i_52_-- > 0) {
								is[i_52_] = integerStack[--integerStackPtr];
							}
						}
						string = string.substring(0, string.length() - 1);
					}
					Object[] objects = new Object[string.length() + 1];
					for (int i_53_ = objects.length - 1; i_53_ >= 1; i_53_--) {
						if (string.charAt(i_53_ - 1) == 's') {
							objects[i_53_] = stringStack[--stringStackPtr];
						} else {
							objects[i_53_] = new Integer(integerStack[--integerStackPtr]);
						}
					}
					int i_54_ = integerStack[--integerStackPtr];
					if (i_54_ != -1) {
						objects[0] = new Integer(i_54_);
					} else {
						objects = null;
					}
					if (opcode == 1400) {
						class293.mousePressedHandler = objects;
					} else if (opcode == 1401) {
						class293.mouseDragPassHandler = objects;
					} else if (opcode == 1402) {
						class293.mouseReleasedHandler = objects;
					} else if (opcode == 1403) {
						class293.mouseEnterHandler = objects;
					} else if (opcode == 1404) {
						class293.mouseExitHandler = objects;
					} else if (opcode == 1405) {
						class293.anObjectArray2316 = objects;
					} else if (opcode == 1406) {
						class293.anObjectArray2324 = objects;
					} else if (opcode == 1407) {
						class293.anObjectArray2269 = objects;
						class293.anIntArray2284 = is;
					} else if (opcode == 1408) {
						class293.updateHandler = objects;
					} else if (opcode == 1409) {
						class293.param = objects;
					} else if (opcode == 1410) {
						class293.anObjectArray2313 = objects;
					} else if (opcode == 1411) {
						class293.mouseDraggedHandler = objects;
					} else if (opcode == 1412) {
						class293.mouseMotionHandler = objects;
					} else if (opcode == 1414) {
						class293.inventoryUpdateHandler = objects;
						class293.anIntArray2249 = is;
					} else if (opcode == 1415) {
						class293.anObjectArray2278 = objects;
						class293.anIntArray2271 = is;
					} else if (opcode == 1416) {
						class293.anObjectArray2257 = objects;
					} else if (opcode == 1417) {
						class293.anObjectArray2277 = objects;
					} else if (opcode == 1418) {
						class293.chatboxUpdateHandler = objects;
					} else if (opcode == 1419) {
						class293.anObjectArray2274 = objects;
					} else if (opcode == 1420) {
						class293.anObjectArray2215 = objects;
					} else if (opcode == 1421) {
						class293.anObjectArray2292 = objects;
					} else if (opcode == 1422) {
						class293.anObjectArray2340 = objects;
					} else if (opcode == 1423) {
						class293.anObjectArray2330 = objects;
					} else if (opcode == 1424) {
						class293.anObjectArray2319 = objects;
					} else if (opcode == 1425) {
						class293.anObjectArray2294 = objects;
					} else if (opcode == 1426) {
						class293.anObjectArray2220 = objects;
					} else if (opcode == 1427) {
						class293.anObjectArray2266 = objects;
					} else if (opcode == 1428) {
						class293.anObjectArray2212 = objects;
						class293.anIntArray2297 = is;
					} else if (opcode == 1429) {
						class293.anObjectArray2320 = objects;
						class293.anIntArray2342 = is;
					} else if (opcode == 1430) {
						class293.anObjectArray2253 = objects;
					}
					class293.aBoolean2209 = true;
					return;
				}
				return;
			}
			if (opcode < 1600) {
				RtInterface class293 = useDot ? aClass293_1877 : aClass293_1879;
				if (opcode == 1500) {
					integerStack[integerStackPtr++] = class293.renderX;
					return;
				}
				if (opcode == 1501) {
					integerStack[integerStackPtr++] = class293.renderY;
					return;
				}
				if (opcode == 1502) {
					integerStack[integerStackPtr++] = class293.renderWidth;
					return;
				}
				if (opcode == 1503) {
					integerStack[integerStackPtr++] = class293.renderHeight;
					return;
				}
				if (opcode == 1504) {
					integerStack[integerStackPtr++] = class293.aBoolean2295 ? 1 : 0;
					return;
				}
				if (opcode == 1505) {
					integerStack[integerStackPtr++] = class293.parentId;
					return;
				}
				if (opcode == 1506) {
					RtInterface class293_55_ = RtInterface.getParent(true, class293);
					integerStack[integerStackPtr++] = class293_55_ == null ? -1 : class293_55_.idDword;
					return;
				}
			} else if (opcode < 1700) {
				RtInterface interf = useDot ? aClass293_1877 : aClass293_1879;
				if (opcode == 1600) {
					integerStack[integerStackPtr++] = interf.scrollX;
					return;
				}
				if (opcode == 1601) {
					integerStack[integerStackPtr++] = interf.scrollY;
					return;
				}
				if (opcode == 1602) {
					stringStack[stringStackPtr++] = interf.defaultText;
					return;
				}
				if (opcode == 1603) {
					integerStack[integerStackPtr++] = interf.scrollWidth;
					return;
				}
				if (opcode == 1604) {
					integerStack[integerStackPtr++] = interf.scrollHeight;
					return;
				}
				if (opcode == 1605) {
					integerStack[integerStackPtr++] = interf.zoom;
					return;
				}
				if (opcode == 1606) {
					integerStack[integerStackPtr++] = interf.rotationX;
					return;
				}
				if (opcode == 1607) {
					integerStack[integerStackPtr++] = interf.rotationZ;
					return;
				}
				if (opcode == 1608) {
					integerStack[integerStackPtr++] = interf.rotationY;
					return;
				}
				if (opcode == 1609) {
					integerStack[integerStackPtr++] = interf.alpha;
					return;
				}
				if (opcode == 1610) {
					integerStack[integerStackPtr++] = interf.xOffset2D;
					return;
				}
				if (opcode == 1611) {
					integerStack[integerStackPtr++] = interf.yOffset2D;
					return;
				}
				if (opcode == 1612) {
					integerStack[integerStackPtr++] = interf.defaultImage;
					return;
				}
				if (opcode == 1613) {
					int i_56_ = integerStack[--integerStackPtr];
					ParamDefinition class149 = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, i_56_);
					if (class149.isString(false)) {
						stringStack[stringStackPtr++] = interf.method3463(i_56_, 700, class149.defaultString);
					} else {
						integerStack[integerStackPtr++] = interf.method3472(22241, class149.defaultInteger, i_56_);
						return;
					}
					return;
				}
				if (opcode == 1614) {
					integerStack[integerStackPtr++] = interf.imageRotation;
					return;
				}
				if (opcode == 2614) {
					integerStack[integerStackPtr++] = interf.defaultMediaType == 1 ? interf.defaultMediaId : -1;
					return;
				}
			} else if (opcode < 1800) {
				RtInterface class293 = useDot ? aClass293_1877 : aClass293_1879;
				if (opcode == 1700) {
					integerStack[integerStackPtr++] = class293.unknown;
					return;
				}
				if (opcode == 1701) {
					if (class293.unknown != -1) {
						integerStack[integerStackPtr++] = class293.itemStackSize;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 1702) {
					integerStack[integerStackPtr++] = class293.componentIndex;
					return;
				}
			} else if (opcode < 1900) {
				RtInterface class293 = useDot ? aClass293_1877 : aClass293_1879;
				if (opcode == 1800) {
					integerStack[integerStackPtr++] = client.method116(class293).method1668(-1);
					return;
				}
				if (opcode == 1801) {
					int i_57_ = integerStack[--integerStackPtr];
					i_57_--;
					if (class293.menuActions == null || i_57_ >= class293.menuActions.length || class293.menuActions[i_57_] == null) {
						stringStack[stringStackPtr++] = "";
					} else {
						stringStack[stringStackPtr++] = class293.menuActions[i_57_];
						return;
					}
					return;
				}
				if (opcode == 1802) {
					if (class293.applyText == null) {
						stringStack[stringStackPtr++] = "";
					} else {
						stringStack[stringStackPtr++] = class293.applyText;
						return;
					}
					return;
				}
			} else if (opcode < 2000 || opcode >= 2900 && opcode < 3000) {
				RtInterface interf;
				if (opcode >= 2000) {
					interf = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
					opcode -= 1000;
				} else {
					interf = useDot ? aClass293_1877 : aClass293_1879;
				}
				if (anInt1893 >= 10) {
					throw new RuntimeException("C29xx-1");
				}
				if (opcode == 1927) {
					if (interf.anObjectArray2266 != null) {
						ClientScript2Event class98_sub21 = new ClientScript2Event();
						class98_sub21.component = interf;
						class98_sub21.param = interf.anObjectArray2266;
						class98_sub21.anInt3990 = anInt1893 + 1;
						Class151_Sub3.aClass148_4977.addLast(class98_sub21, -20911);
						return;
					}
					return;
				}
			} else if (opcode < 2600) {
				RtInterface class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
				if (opcode == 2500) {
					integerStack[integerStackPtr++] = class293.renderX;
					return;
				}
				if (opcode == 2501) {
					integerStack[integerStackPtr++] = class293.renderY;
					return;
				}
				if (opcode == 2502) {
					integerStack[integerStackPtr++] = class293.renderWidth;
					return;
				}
				if (opcode == 2503) {
					integerStack[integerStackPtr++] = class293.renderHeight;
					return;
				}
				if (opcode == 2504) {
					integerStack[integerStackPtr++] = class293.aBoolean2295 ? 1 : 0;
					return;
				}
				if (opcode == 2505) {
					integerStack[integerStackPtr++] = class293.parentId;
					return;
				}
				if (opcode == 1506) {
					RtInterface class293_58_ = RtInterface.getParent(true, class293);
					integerStack[integerStackPtr++] = class293_58_ == null ? -1 : class293_58_.idDword;
					return;
				}
			} else if (opcode < 2700) {
				RtInterface interf = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
				if (opcode == 2600) {
					integerStack[integerStackPtr++] = interf.scrollX;
					return;
				}
				if (opcode == 2601) {
					integerStack[integerStackPtr++] = interf.scrollY;
					return;
				}
				if (opcode == 2602) {
					stringStack[stringStackPtr++] = interf.defaultText;
					return;
				}
				if (opcode == 2603) {
					integerStack[integerStackPtr++] = interf.scrollWidth;
					return;
				}
				if (opcode == 2604) {
					integerStack[integerStackPtr++] = interf.scrollHeight;
					return;
				}
				if (opcode == 2605) {
					integerStack[integerStackPtr++] = interf.zoom;
					return;
				}
				if (opcode == 2606) {
					integerStack[integerStackPtr++] = interf.rotationX;
					return;
				}
				if (opcode == 2607) {
					integerStack[integerStackPtr++] = interf.rotationZ;
					return;
				}
				if (opcode == 2608) {
					integerStack[integerStackPtr++] = interf.rotationY;
					return;
				}
				if (opcode == 2609) {
					integerStack[integerStackPtr++] = interf.alpha;
					return;
				}
				if (opcode == 2610) {
					integerStack[integerStackPtr++] = interf.xOffset2D;
					return;
				}
				if (opcode == 2611) {
					integerStack[integerStackPtr++] = interf.yOffset2D;
					return;
				}
				if (opcode == 2612) {
					integerStack[integerStackPtr++] = interf.defaultImage;
					return;
				}
				if (opcode == 2613) {
					integerStack[integerStackPtr++] = interf.imageRotation;
					return;
				}
				if (opcode == 2614) {
					integerStack[integerStackPtr++] = interf.defaultMediaType == 1 ? interf.defaultMediaId : -1;
					return;
				}
			} else if (opcode < 2800) {
				if (opcode == 2700) {
					RtInterface class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
					integerStack[integerStackPtr++] = class293.unknown;
					return;
				}
				if (opcode == 2701) {
					RtInterface class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
					if (class293.unknown != -1) {
						integerStack[integerStackPtr++] = class293.itemStackSize;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 2702) {
					int i_59_ = integerStack[--integerStackPtr];
					RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(i_59_, -1);
					if (class98_sub18 != null) {
						integerStack[integerStackPtr++] = 1;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 2703) {
					RtInterface class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
					if (class293.dynamicComponents == null) {
						integerStack[integerStackPtr++] = 0;
					} else {
						int i_60_ = class293.dynamicComponents.length;
						for (int i_61_ = 0; i_61_ < class293.dynamicComponents.length; i_61_++) {
							if (class293.dynamicComponents[i_61_] == null) {
								i_60_ = i_61_;
								break;
							}
						}
						integerStack[integerStackPtr++] = i_60_;
						return;
					}
					return;
				}
				if (opcode == 2704 || opcode == 2705) {
					integerStackPtr -= 2;
					int i_62_ = integerStack[integerStackPtr];
					int i_63_ = integerStack[integerStackPtr + 1];
					RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(i_62_, -1);
					if (class98_sub18 != null && class98_sub18.attachedInterfaceId == i_63_) {
						integerStack[integerStackPtr++] = 1;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
			} else if (opcode < 2900) {
				RtInterface class293 = RtInterface.getInterface(integerStack[--integerStackPtr], -9820);
				if (opcode == 2800) {
					integerStack[integerStackPtr++] = client.method116(class293).method1668(-1);
					return;
				}
				if (opcode == 2801) {
					int i_64_ = integerStack[--integerStackPtr];
					i_64_--;
					if (class293.menuActions == null || i_64_ >= class293.menuActions.length || class293.menuActions[i_64_] == null) {
						stringStack[stringStackPtr++] = "";
					} else {
						stringStack[stringStackPtr++] = class293.menuActions[i_64_];
						return;
					}
					return;
				}
				if (opcode == 2802) {
					if (class293.applyText == null) {
						stringStack[stringStackPtr++] = "";
					} else {
						stringStack[stringStackPtr++] = class293.applyText;
						return;
					}
					return;
				}
			} else if (opcode < 3200) {
				if (opcode == 3100) {
					String string = stringStack[--stringStackPtr];
					Class84.method832(string, (byte) 108);
					return;
				}
				if (opcode == 3101) {
					integerStackPtr -= 2;
					PrefetchManager.method2657(Class87.localPlayer, (byte) 114, integerStack[integerStackPtr + 1], integerStack[integerStackPtr]);
					return;
				}
				if (opcode == 3103) {
					Class246_Sub3_Sub5_Sub1.method3092(-1, true);
					return;
				}
				if (opcode == 3104) {
					String string = stringStack[--stringStackPtr];
					int i_65_ = 0;
					if (AwtKeyListener.validateInteger((byte) 53, string)) {
						i_65_ = JavaNetworkWriter.parseInteger(-126, string);
					}
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class246_Sub3_Sub5.aClass171_6164, Class331.aClass117_2811);
					class98_sub11.packet.writeInt(1571862888, i_65_);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					return;
				}
				if (opcode == 3105) {
					String string = stringStack[--stringStackPtr];
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class284_Sub1_Sub2.aClass171_6191, Class331.aClass117_2811);
					class98_sub11.packet.writeByte(string.length() + 1, -61);
					class98_sub11.packet.writePJStr1(string, (byte) 113);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					return;
				}
				if (opcode == 3106) {
					String string = stringStack[--stringStackPtr];
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, FloorOverlayDefinition.aClass171_1533, Class331.aClass117_2811);
					class98_sub11.packet.writeByte(string.length() + 1, -58);
					class98_sub11.packet.writePJStr1(string, (byte) 113);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					return;
				}
				if (opcode == 3107) {
					int i_66_ = integerStack[--integerStackPtr];
					String string = stringStack[--stringStackPtr];
					Class314.method3639(i_66_, string, false);
					return;
				}
				if (opcode == 3108) {
					integerStackPtr -= 3;
					int i_67_ = integerStack[integerStackPtr];
					int i_68_ = integerStack[integerStackPtr + 1];
					int i_69_ = integerStack[integerStackPtr + 2];
					RtInterface class293 = RtInterface.getInterface(i_69_, -9820);
					NativeModelRenderer.method2405(class293, (byte) 103, i_67_, i_68_);
					return;
				}
				if (opcode == 3109) {
					integerStackPtr -= 2;
					int i_70_ = integerStack[integerStackPtr];
					int i_71_ = integerStack[integerStackPtr + 1];
					RtInterface class293 = useDot ? aClass293_1877 : aClass293_1879;
					NativeModelRenderer.method2405(class293, (byte) 118, i_70_, i_71_);
					return;
				}
				if (opcode == 3110) {
					int i_72_ = integerStack[--integerStackPtr];
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class150.aClass171_1209, Class331.aClass117_2811);
					class98_sub11.packet.writeShort(i_72_, 1571862888);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					return;
				}
				if (opcode == 3111) {
					integerStackPtr -= 2;
					int i_73_ = integerStack[integerStackPtr];
					int i_74_ = integerStack[integerStackPtr + 1];
					RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(i_73_, -1);
					if (class98_sub18 != null) {
						RtInterfaceAttachment.detachInterface(16398, true, class98_sub18, class98_sub18.attachedInterfaceId != i_74_);
					}
					RtInterfaceAttachment.attachInterface(true, -128, i_74_, i_73_, 3);
					return;
				}
				if (opcode == 3112) {
					integerStackPtr--;
					int i_75_ = integerStack[integerStackPtr];
					RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(i_75_, -1);
					if (class98_sub18 != null && class98_sub18.anInt3947 == 3) {
						RtInterfaceAttachment.detachInterface(16398, true, class98_sub18, true);
					}
					return;
				}
				if (opcode == 3113) {
					ReflectionRequest.method1165((byte) 36, stringStack[--stringStackPtr]);
					return;
				}
				if (opcode == 3114) {
					integerStackPtr -= 2;
					int i_76_ = integerStack[integerStackPtr];
					int i_77_ = integerStack[integerStackPtr + 1];
					String string = stringStack[--stringStackPtr];
					ItemDeque.addChatMessage((byte) -119, i_76_, string, i_77_, "", "", "");
					return;
				}
				if (opcode == 3115) {
					integerStackPtr -= 11;
					HorizontalAlignment[] class63s = HorizontalAlignment.getValues(123);
					VerticalAlignment[] class110s = VerticalAlignment.getValues(256);
					DummyOutputStream.method129(integerStack[integerStackPtr + 10], integerStack[integerStackPtr + 3], class63s[integerStack[integerStackPtr]], integerStack[integerStackPtr + 4], integerStack[integerStackPtr + 2], false, integerStack[integerStackPtr + 8], integerStack[integerStackPtr
							+ 9], integerStack[integerStackPtr + 6], integerStack[integerStackPtr + 7], integerStack[integerStackPtr + 5], class110s[integerStack[integerStackPtr + 1]]);
					return;
				}
			} else if (opcode < 3300) {
				if (opcode == 3200) {
					integerStackPtr -= 3;
					NPCDefinitionParser.method3537(256, (byte) 1, integerStack[integerStackPtr], integerStack[integerStackPtr + 1], integerStack[integerStackPtr + 2], 255);
					return;
				}
				if (opcode == 3201) {
					Class246_Sub3_Sub1.method2994(integerStack[--integerStackPtr], 255, (byte) -83, 50);
					return;
				}
				if (opcode == 3202) {
					integerStackPtr -= 2;
					Class228.method2861(integerStack[integerStackPtr + 1], 255, integerStack[integerStackPtr], 18596);
					return;
				}
				if (opcode == 3203) {
					integerStackPtr -= 4;
					NPCDefinitionParser.method3537(256, (byte) 1, integerStack[integerStackPtr], integerStack[integerStackPtr + 1], integerStack[integerStackPtr + 2], integerStack[integerStackPtr + 3]);
					return;
				}
				if (opcode == 3204) {
					integerStackPtr -= 3;
					Class246_Sub3_Sub1.method2994(integerStack[integerStackPtr], integerStack[integerStackPtr + 1], (byte) -83, integerStack[integerStackPtr + 2]);
					return;
				}
				if (opcode == 3205) {
					integerStackPtr -= 3;
					Class228.method2861(integerStack[integerStackPtr + 1], integerStack[integerStackPtr + 2], integerStack[integerStackPtr], 18596);
					return;
				}
				if (opcode == 3206) {
					integerStackPtr -= 4;
					Class98_Sub10_Sub9.method1036(-1962608884, integerStack[integerStackPtr + 3], integerStack[integerStackPtr + 1], integerStack[integerStackPtr], false, integerStack[integerStackPtr + 2], 256);
					return;
				}
				if (opcode == 3207) {
					integerStackPtr -= 4;
					Class98_Sub10_Sub9.method1036(-1962608884, integerStack[integerStackPtr + 3], integerStack[integerStackPtr + 1], integerStack[integerStackPtr], true, integerStack[integerStackPtr + 2], 256);
					return;
				}
				if (opcode == 3208) {
					integerStackPtr -= 5;
					NPCDefinitionParser.method3537(integerStack[integerStackPtr + 4], (byte) 1, integerStack[integerStackPtr], integerStack[integerStackPtr + 1], integerStack[integerStackPtr + 2], integerStack[integerStackPtr + 3]);
					return;
				}
				if (opcode == 3209) {
					integerStackPtr -= 5;
					Class98_Sub10_Sub9.method1036(-1962608884, integerStack[integerStackPtr + 3], integerStack[integerStackPtr + 1], integerStack[integerStackPtr], false, integerStack[integerStackPtr + 2], integerStack[integerStackPtr + 4]);
					return;
				}
			} else if (opcode < 3400) {
				if (opcode == 3300) {
					integerStack[integerStackPtr++] = Queue.timer;
					return;
				}
				if (opcode == 3301) {
					integerStackPtr -= 2;
					int i_78_ = integerStack[integerStackPtr];
					int i_79_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = Class96.method925(i_78_, -121, i_79_, false);
					return;
				}
				if (opcode == 3302) {
					integerStackPtr -= 2;
					int i_80_ = integerStack[integerStackPtr];
					int i_81_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = ClientInventory.method2212(false, (byte) -96, i_80_, i_81_);
					return;
				}
				if (opcode == 3303) {
					integerStackPtr -= 2;
					int i_82_ = integerStack[integerStackPtr];
					int i_83_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = Class249.method3161(i_82_, -122, false, i_83_);
					return;
				}
				if (opcode == 3304) {
					int i_84_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub46_Sub14.inventoriesDefinitionList.method185(9, i_84_).anInt6055;
					return;
				}
				if (opcode == 3305) {
					int i_85_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = CpuUsagePreferenceField.levels[i_85_];
					return;
				}
				if (opcode == 3306) {
					int i_86_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class256_Sub1.skillLevels[i_86_];
					return;
				}
				if (opcode == 3307) {
					int i_87_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = NewsLSEConfig.skillsExperience[i_87_];
					return;
				}
				if (opcode == 3308) {
					int i_88_ = Class87.localPlayer.plane;
					int i_89_ = (Class87.localPlayer.boundExtentsX >> 9) + Class272.gameSceneBaseX;
					int i_90_ = (Class87.localPlayer.boundExtentsZ >> 9) + aa_Sub2.gameSceneBaseY;
					integerStack[integerStackPtr++] = (i_88_ << 28) + (i_89_ << 14) + i_90_;
					return;
				}
				if (opcode == 3309) {
					int i_91_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = i_91_ >> 14 & 0x3fff;
					return;
				}
				if (opcode == 3310) {
					int i_92_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = i_92_ >> 28;
					return;
				}
				if (opcode == 3311) {
					int i_93_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = i_93_ & 0x3fff;
					return;
				}
				if (opcode == 3312) {
					integerStack[integerStackPtr++] = AdvancedMemoryCache.isMembersOnly ? 1 : 0;
					return;
				}
				if (opcode == 3313) {
					integerStackPtr -= 2;
					int i_94_ = integerStack[integerStackPtr];
					int i_95_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = Class96.method925(i_94_, 118, i_95_, true);
					return;
				}
				if (opcode == 3314) {
					integerStackPtr -= 2;
					int i_96_ = integerStack[integerStackPtr];
					int i_97_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = ClientInventory.method2212(true, (byte) -96, i_96_, i_97_);
					return;
				}
				if (opcode == 3315) {
					integerStackPtr -= 2;
					int i_98_ = integerStack[integerStackPtr];
					int i_99_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = Class249.method3161(i_98_, -121, true, i_99_);
					return;
				}
				if (opcode == 3316) {
					if (LoadingScreenSequence.rights >= 2) {
						integerStack[integerStackPtr++] = LoadingScreenSequence.rights;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3317) {
					integerStack[integerStackPtr++] = Class98_Sub10_Sub6.anInt5569;
					return;
				}
				if (opcode == 3318) {
					integerStack[integerStackPtr++] = client.server.index;
					return;
				}
				if (opcode == 3321) {
					integerStack[integerStackPtr++] = NewsItem.runEnergyLevel;
					return;
				}
				if (opcode == 3322) {
					integerStack[integerStackPtr++] = WorldMapInfoDefinition.anInt255;
					return;
				}
				if (opcode == 3323) {
					if (LoadingScreenRenderer.anInt407 >= 5 && LoadingScreenRenderer.anInt407 <= 9) {
						integerStack[integerStackPtr++] = 1;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3324) {
					if (LoadingScreenRenderer.anInt407 >= 5 && LoadingScreenRenderer.anInt407 <= 9) {
						integerStack[integerStackPtr++] = LoadingScreenRenderer.anInt407;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3325) {
					integerStack[integerStackPtr++] = client.isMembers1 ? 1 : 0;
					return;
				}
				if (opcode == 3326) {
					integerStack[integerStackPtr++] = Class87.localPlayer.combatLevel;
					return;
				}
				if (opcode == 3327) {
					integerStack[integerStackPtr++] = Class87.localPlayer.appearence != null && Class87.localPlayer.appearence.female ? 1 : 0;
					return;
				}
				if (opcode == 3329) {
					integerStack[integerStackPtr++] = BaseModel.isQuickChatWorld ? 1 : 0;
					return;
				}
				if (opcode == 3330) {
					int i_100_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = NativeOpenGlElementArrayBuffer.method2498(i_100_, (byte) 99, false);
					return;
				}
				if (opcode == 3331) {
					integerStackPtr -= 2;
					int i_101_ = integerStack[integerStackPtr];
					int i_102_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = Class98_Sub6.method978(false, false, i_101_, true, i_102_);
					return;
				}
				if (opcode == 3332) {
					integerStackPtr -= 2;
					int i_103_ = integerStack[integerStackPtr];
					int i_104_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = Class98_Sub6.method978(false, true, i_103_, true, i_104_);
					return;
				}
				if (opcode == 3333) {
					integerStack[integerStackPtr++] = OpenGlModelRenderer.anInt4855;
					return;
				}
				if (opcode == 3335) {
					integerStack[integerStackPtr++] = client.gameLanguage;
					return;
				}
				if (opcode == 3336) {
					integerStackPtr -= 4;
					int i_105_ = integerStack[integerStackPtr];
					int i_106_ = integerStack[integerStackPtr + 1];
					int i_107_ = integerStack[integerStackPtr + 2];
					int i_108_ = integerStack[integerStackPtr + 3];
					i_105_ += i_106_ << 14;
					i_105_ += i_107_ << 28;
					i_105_ += i_108_;
					integerStack[integerStackPtr++] = i_105_;
					return;
				}
				if (opcode == 3337) {
					integerStack[integerStackPtr++] = client.affiliateId;
					return;
				}
				if (opcode == 3338) {
					integerStack[integerStackPtr++] = Class278_Sub1.method3320(12);
					return;
				}
				if (opcode == 3339) {
					integerStack[integerStackPtr++] = 0;
					return;
				}
				if (opcode == 3340) {
					integerStack[integerStackPtr++] = GameShell.focus ? 1 : 0;
					return;
				}
				if (opcode == 3341) {
					integerStack[integerStackPtr++] = client.forceBilling ? 1 : 0;
					return;
				}
				if (opcode == 3342) {
					integerStack[integerStackPtr++] = client.mouseListener.getMouseX(61);
					return;
				}
				if (opcode == 3343) {
					integerStack[integerStackPtr++] = client.mouseListener.getMouseY((byte) 82);
					return;
				}
				if (opcode == 3344) {
					stringStack[stringStackPtr++] = IdentikitDefinition.method2477(29558);
					return;
				}
				if (opcode == 3345) {
					stringStack[stringStackPtr++] = CursorDefinition.method2873(0);
					return;
				}
				if (opcode == 3346) {
					integerStack[integerStackPtr++] = PrefetchObject.method1710(66);
					return;
				}
				if (opcode == 3347) {
					integerStack[integerStackPtr++] = Orientation.anInt2729;
					return;
				}
				if (opcode == 3349) {
					integerStack[integerStackPtr++] = Class87.localPlayer.yaw.value((byte) 116) >> 3;
					return;
				}
			} else if (opcode < 3500) {
				if (opcode == 3400) {
					integerStackPtr -= 2;
					int i_109_ = integerStack[integerStackPtr];
					int i_110_ = integerStack[integerStackPtr + 1];
					EnumDefinition class306 = Class98_Sub10_Sub16.enumsDefinitionList.read(i_109_, 1028602529);
					stringStack[stringStackPtr++] = class306.get(i_110_, (byte) 88);
					return;
				}
				if (opcode == 3408) {
					integerStackPtr -= 4;
					int i_111_ = integerStack[integerStackPtr];
					int i_112_ = integerStack[integerStackPtr + 1];
					int i_113_ = integerStack[integerStackPtr + 2];
					int i_114_ = integerStack[integerStackPtr + 3];
					EnumDefinition class306 = Class98_Sub10_Sub16.enumsDefinitionList.read(i_113_, 1028602529);
					if (class306.aChar2560 != i_111_ || class306.aChar2567 != i_112_) {
						throw new RuntimeException("C3408-1 " + i_113_ + "-" + i_114_);
					}
					if (i_112_ == 115) {
						stringStack[stringStackPtr++] = class306.get(i_114_, (byte) 23);
					} else {
						integerStack[integerStackPtr++] = class306.method3598(i_114_, -28629);
						return;
					}
					return;
				}
				if (opcode == 3409) {
					integerStackPtr -= 3;
					int i_115_ = integerStack[integerStackPtr];
					int i_116_ = integerStack[integerStackPtr + 1];
					int i_117_ = integerStack[integerStackPtr + 2];
					if (i_116_ == -1) {
						throw new RuntimeException("C3409-2");
					}
					EnumDefinition class306 = Class98_Sub10_Sub16.enumsDefinitionList.read(i_116_, 1028602529);
					if (class306.aChar2567 != i_115_) {
						throw new RuntimeException("C3409-1");
					}
					integerStack[integerStackPtr++] = class306.method3596(i_117_, (byte) 104) ? 1 : 0;
					return;
				}
				if (opcode == 3410) {
					int i_118_ = integerStack[--integerStackPtr];
					String string = stringStack[--stringStackPtr];
					if (i_118_ == -1) {
						throw new RuntimeException("C3410-2");
					}
					EnumDefinition class306 = Class98_Sub10_Sub16.enumsDefinitionList.read(i_118_, 1028602529);
					if (class306.aChar2567 != 's') {
						throw new RuntimeException("C3410-1");
					}
					integerStack[integerStackPtr++] = class306.method3602(string, -16972) ? 1 : 0;
					return;
				}
				if (opcode == 3411) {
					int i_119_ = integerStack[--integerStackPtr];
					EnumDefinition class306 = Class98_Sub10_Sub16.enumsDefinitionList.read(i_119_, 1028602529);
					integerStack[integerStackPtr++] = class306.aClass377_2558.size((byte) -6);
					return;
				}
			} else if (opcode < 3700) {
				if (opcode == 3600) {
					if (Class98_Sub28.totalBoxVisibility == 0) {
						integerStack[integerStackPtr++] = -2;
					} else {
						if (Class98_Sub28.totalBoxVisibility == 1) {
							integerStack[integerStackPtr++] = -1;
						} else {
							integerStack[integerStackPtr++] = Class314.friendListSize;
							return;
						}
						return;
					}
					return;
				}
				if (opcode == 3601) {
					int count = integerStack[--integerStackPtr];
					if (Class98_Sub28.totalBoxVisibility == 2 && count < Class314.friendListSize) {
						stringStack[stringStackPtr++] = Class98_Sub25.friends[count];
						if (TextLSEConfig.friendsDisplayNames[count] != null) {
							stringStack[stringStackPtr++] = TextLSEConfig.friendsDisplayNames[count];
						} else {
							stringStack[stringStackPtr++] = "";
						}
					} else {
						stringStack[stringStackPtr++] = "";
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 3602) {
					int count = integerStack[--integerStackPtr];
					if (Class98_Sub28.totalBoxVisibility == 2 && count < Class314.friendListSize) {
						integerStack[integerStackPtr++] = GroundItem.friendsOnline[count];
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3603) {
					int count = integerStack[--integerStackPtr];
					if (Class98_Sub28.totalBoxVisibility == 2 && count < Class314.friendListSize) {
						integerStack[integerStackPtr++] = Class69.anIntArray3222[count];
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3604) {
					String string = stringStack[--stringStackPtr];
					int i_123_ = integerStack[--integerStackPtr];
					System.out.println("string: " + string + "i: " + i_123_);
					SpriteProgressBarLoadingScreenElement.method3976(i_123_, (byte) 89, string);
					return;
				}
				if (opcode == 3605) {
					String string = stringStack[--stringStackPtr];
					Class176.addFriend(4, string);
					return;
				}
				if (opcode == 3606) {
					String string = stringStack[--stringStackPtr];
					OpenGLHeightMapToNormalMapConverter.removeFriend(54, string);
					return;
				}
				if (opcode == 3607) {
					String string = stringStack[--stringStackPtr];
					Class66.addIgnore(-104, string, false);
					return;
				}
				if (opcode == 3608) {
					String string = stringStack[--stringStackPtr];
					OpenGlGround.removeIgnore(string, -23995);
					return;
				}
				if (opcode == 3609) {
					String string = stringStack[--stringStackPtr];
					if (string.startsWith("<img=0>") || string.startsWith("<img=1>")) {
						string = string.substring(7);
					}
					integerStack[integerStackPtr++] = Class256_Sub1.isAdded(0, string) ? 1 : 0;
					return;
				}
				if (opcode == 3610) {
					int i_124_ = integerStack[--integerStackPtr];
					if (Class98_Sub28.totalBoxVisibility == 2 && i_124_ < Class314.friendListSize) {
						stringStack[stringStackPtr++] = Class98_Sub10_Sub17.worlds[i_124_];
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 3611) {
					if (FloorUnderlayDefinitionParser.clanChatName != null) {
						stringStack[stringStackPtr++] = Class98_Sub10_Sub2.setClanChatName(FloorUnderlayDefinitionParser.clanChatName, 46);
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 3612) {
					if (FloorUnderlayDefinitionParser.clanChatName != null) {
						integerStack[integerStackPtr++] = FloorOverlayDefinitionParser.clanChatSize;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3613) {
					int size = integerStack[--integerStackPtr];
					if (FloorUnderlayDefinitionParser.clanChatName != null && size < FloorOverlayDefinitionParser.clanChatSize) {
						stringStack[stringStackPtr++] = WhirlpoolGenerator.clanChat[size].displayName;
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 3614) {
					int i_126_ = integerStack[--integerStackPtr];
					if (FloorUnderlayDefinitionParser.clanChatName != null && i_126_ < FloorOverlayDefinitionParser.clanChatSize) {
						integerStack[integerStackPtr++] = WhirlpoolGenerator.clanChat[i_126_].worldOrLobbyColour;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3615) {
					int i_127_ = integerStack[--integerStackPtr];
					if (FloorUnderlayDefinitionParser.clanChatName != null && i_127_ < FloorOverlayDefinitionParser.clanChatSize) {
						integerStack[integerStackPtr++] = WhirlpoolGenerator.clanChat[i_127_].chatRank;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3616) {
					integerStack[integerStackPtr++] = Class232.kickRequirement;
					return;
				}
				if (opcode == 3617) {
					String string = stringStack[--stringStackPtr];
					Class76_Sub4.method756(-108, string);
					return;
				}
				if (opcode == 3618) {
					integerStack[integerStackPtr++] = Matrix.clanChatRank;
					return;
				}
				if (opcode == 3619) {
					String string = stringStack[--stringStackPtr];
					Class345.method3824(string, 2);
					return;
				}
				if (opcode == 3620) {
					Js5Archive.method3799(0);
					return;
				}
				if (opcode == 3621) {
					if (Class98_Sub28.totalBoxVisibility == 0) {
						integerStack[integerStackPtr++] = -1;
					} else {
						integerStack[integerStackPtr++] = Class248.ignoreListSize;
						return;
					}
					return;
				}
				if (opcode == 3622) {
					int count = integerStack[--integerStackPtr];
					if (Class98_Sub28.totalBoxVisibility != 0 && count < Class248.ignoreListSize) {
						stringStack[stringStackPtr++] = FriendLoginUpdate.ignores[count];
						if (ItemDeque.ignoreDisplayNames[count] != null) {
							stringStack[stringStackPtr++] = ItemDeque.ignoreDisplayNames[count];
						} else {
							stringStack[stringStackPtr++] = "";
						}
					} else {
						stringStack[stringStackPtr++] = "";
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 3623) {
					String string = stringStack[--stringStackPtr];
					if (string.startsWith("<img=0>") || string.startsWith("<img=1>")) {
						string = string.substring(7);
					}
					integerStack[integerStackPtr++] = Class14.isIgnored(string, (byte) 117) ? 1 : 0;
					return;
				}
				if (opcode == 3624) {
					int i_129_ = integerStack[--integerStackPtr];
					if (WhirlpoolGenerator.clanChat != null && i_129_ < FloorOverlayDefinitionParser.clanChatSize && WhirlpoolGenerator.clanChat[i_129_].accountName.equalsIgnoreCase(Class87.localPlayer.accountName)) {
						integerStack[integerStackPtr++] = 1;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3625) {
					if (SimpleProgressBarLSEConfig.clanChatOwnerName != null) {
						stringStack[stringStackPtr++] = SimpleProgressBarLSEConfig.clanChatOwnerName;
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 3626) {
					int i_130_ = integerStack[--integerStackPtr];
					if (FloorUnderlayDefinitionParser.clanChatName != null && i_130_ < FloorOverlayDefinitionParser.clanChatSize) {
						stringStack[stringStackPtr++] = WhirlpoolGenerator.clanChat[i_130_].world;
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 3627) {
					int i_131_ = integerStack[--integerStackPtr];
					if (Class98_Sub28.totalBoxVisibility == 2 && i_131_ >= 0 && i_131_ < Class314.friendListSize) {
						integerStack[integerStackPtr++] = aa_Sub3.aBooleanArray3575[i_131_] ? 1 : 0;
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 3628) {
					String string = stringStack[--stringStackPtr];
					if (string.startsWith("<img=0>") || string.startsWith("<img=1>")) {
						string = string.substring(7);
					}
					integerStack[integerStackPtr++] = Matrix.indexOf(string, -125);
					return;
				}
				if (opcode == 3629) {
					integerStack[integerStackPtr++] = client.countryId;
					return;
				}
				if (opcode == 3630) {
					String string = stringStack[--stringStackPtr];
					Class66.addIgnore(-59, string, true);
					return;
				}
				if (opcode == 3631) {
					int i_132_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub10_Sub38.aBooleanArray5759[i_132_] ? 1 : 0;
					return;
				}
				if (opcode == 3632) {
					int i_133_ = integerStack[--integerStackPtr];
					if (FloorUnderlayDefinitionParser.clanChatName != null && i_133_ < FloorOverlayDefinitionParser.clanChatSize) {
						stringStack[stringStackPtr++] = WhirlpoolGenerator.clanChat[i_133_].accountName;
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 3633) {
					int i_134_ = integerStack[--integerStackPtr];
					if (Class98_Sub28.totalBoxVisibility != 0 && i_134_ < Class248.ignoreListSize) {
						stringStack[stringStackPtr++] = Class255.ignoreNames[i_134_];
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
			} else if (opcode < 4000) {
				if (opcode == 3903) {
					int i_135_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub10_Sub24.offers[i_135_].method1698(true);
					return;
				}
				if (opcode == 3904) {
					int i_136_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub10_Sub24.offers[i_136_].itemId;
					return;
				}
				if (opcode == 3905) {
					int i_137_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub10_Sub24.offers[i_137_].price;
					return;
				}
				if (opcode == 3906) {
					int i_138_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub10_Sub24.offers[i_138_].offerAmount;
					return;
				}
				if (opcode == 3907) {
					int i_139_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub10_Sub24.offers[i_139_].amountComplete;
					return;
				}
				if (opcode == 3908) {
					int i_140_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub10_Sub24.offers[i_140_].sumComplete;
					return;
				}
				if (opcode == 3910) {
					int i_141_ = integerStack[--integerStackPtr];
					int i_142_ = Class98_Sub10_Sub24.offers[i_141_].method1700(7);
					integerStack[integerStackPtr++] = i_142_ == 0 ? 1 : 0;
					return;
				}
				if (opcode == 3911) {
					int i_143_ = integerStack[--integerStackPtr];
					int i_144_ = Class98_Sub10_Sub24.offers[i_143_].method1700(7);
					integerStack[integerStackPtr++] = i_144_ == 2 ? 1 : 0;
					return;
				}
				if (opcode == 3912) {
					int i_145_ = integerStack[--integerStackPtr];
					int i_146_ = Class98_Sub10_Sub24.offers[i_145_].method1700(7);
					integerStack[integerStackPtr++] = i_146_ == 5 ? 1 : 0;
					return;
				}
				if (opcode == 3913) {
					int i_147_ = integerStack[--integerStackPtr];
					int i_148_ = Class98_Sub10_Sub24.offers[i_147_].method1700(7);
					integerStack[integerStackPtr++] = i_148_ == 1 ? 1 : 0;
					return;
				}
			} else if (opcode < 4100) {
				if (opcode == 4000) {
					integerStackPtr -= 2;
					int i_149_ = integerStack[integerStackPtr];
					int i_150_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_149_ + i_150_;
					return;
				}
				if (opcode == 4001) {
					integerStackPtr -= 2;
					int i_151_ = integerStack[integerStackPtr];
					int i_152_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_151_ - i_152_;
					return;
				}
				if (opcode == 4002) {
					integerStackPtr -= 2;
					int i_153_ = integerStack[integerStackPtr];
					int i_154_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_153_ * i_154_;
					return;
				}
				if (opcode == 4003) {
					integerStackPtr -= 2;
					int i_155_ = integerStack[integerStackPtr];
					int i_156_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_155_ / i_156_;
					return;
				}
				if (opcode == 4004) {
					int i_157_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = (int) (Math.random() * i_157_);
					return;
				}
				if (opcode == 4005) {
					int i_158_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = (int) (Math.random() * (i_158_ + 1));
					return;
				}
				if (opcode == 4006) {
					integerStackPtr -= 5;
					int i_159_ = integerStack[integerStackPtr];
					int i_160_ = integerStack[integerStackPtr + 1];
					int i_161_ = integerStack[integerStackPtr + 2];
					int i_162_ = integerStack[integerStackPtr + 3];
					int i_163_ = integerStack[integerStackPtr + 4];
					integerStack[integerStackPtr++] = i_159_ + (i_160_ - i_159_) * (i_163_ - i_161_) / (i_162_ - i_161_);
					return;
				}
				if (opcode == 4007) {
					integerStackPtr -= 2;
					long l = integerStack[integerStackPtr];
					long l_164_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = (int) (l + l * l_164_ / 100L);
					return;
				}
				if (opcode == 4008) {
					integerStackPtr -= 2;
					int i_165_ = integerStack[integerStackPtr];
					int i_166_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_165_ | 1 << i_166_;
					return;
				}
				if (opcode == 4009) {
					integerStackPtr -= 2;
					int i_167_ = integerStack[integerStackPtr];
					int i_168_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_167_ & -1 - (1 << i_168_);
					return;
				}
				if (opcode == 4010) {
					integerStackPtr -= 2;
					int i_169_ = integerStack[integerStackPtr];
					int i_170_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = (i_169_ & 1 << i_170_) != 0 ? 1 : 0;
					return;
				}
				if (opcode == 4011) {
					integerStackPtr -= 2;
					int i_171_ = integerStack[integerStackPtr];
					int i_172_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_171_ % i_172_;
					return;
				}
				if (opcode == 4012) {
					integerStackPtr -= 2;
					int i_173_ = integerStack[integerStackPtr];
					int i_174_ = integerStack[integerStackPtr + 1];
					if (i_173_ == 0) {
						integerStack[integerStackPtr++] = 0;
					} else {
						integerStack[integerStackPtr++] = (int) Math.pow(i_173_, i_174_);
						return;
					}
					return;
				}
				if (opcode == 4013) {
					integerStackPtr -= 2;
					int i_175_ = integerStack[integerStackPtr];
					int i_176_ = integerStack[integerStackPtr + 1];
					if (i_175_ == 0) {
						integerStack[integerStackPtr++] = 0;
					} else {
						if (i_176_ == 0) {
							integerStack[integerStackPtr++] = 2147483647;
						} else {
							integerStack[integerStackPtr++] = (int) Math.pow(i_175_, 1.0 / i_176_);
							return;
						}
						return;
					}
					return;
				}
				if (opcode == 4014) {
					integerStackPtr -= 2;
					int i_177_ = integerStack[integerStackPtr];
					int i_178_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_177_ & i_178_;
					return;
				}
				if (opcode == 4015) {
					integerStackPtr -= 2;
					int i_179_ = integerStack[integerStackPtr];
					int i_180_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_179_ | i_180_;
					return;
				}
				if (opcode == 4016) {
					integerStackPtr -= 2;
					int i_181_ = integerStack[integerStackPtr];
					int i_182_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_181_ < i_182_ ? i_181_ : i_182_;
					return;
				}
				if (opcode == 4017) {
					integerStackPtr -= 2;
					int i_183_ = integerStack[integerStackPtr];
					int i_184_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = i_183_ > i_184_ ? i_183_ : i_184_;
					return;
				}
				if (opcode == 4018) {
					integerStackPtr -= 3;
					long l = integerStack[integerStackPtr];
					long l_185_ = integerStack[integerStackPtr + 1];
					long l_186_ = integerStack[integerStackPtr + 2];
					integerStack[integerStackPtr++] = (int) (l * l_186_ / l_185_);
					return;
				}
				if (opcode == 4019) {
					integerStackPtr -= 2;
					int i_187_ = integerStack[integerStackPtr];
					int i_188_ = integerStack[integerStackPtr + 1];
					if (i_187_ > 700 || i_188_ > 700) {
						integerStack[integerStackPtr++] = 256;
					}
					double d = (Math.random() * (i_188_ + i_187_) - i_187_ + 800.0) / 100.0;
					integerStack[integerStackPtr++] = (int) (Math.pow(2.0, d) + 0.5);
					return;
				}
			} else if (opcode < 4200) {
				if (opcode == 4100) {
					String string = stringStack[--stringStackPtr];
					int i_189_ = integerStack[--integerStackPtr];
					stringStack[stringStackPtr++] = string + i_189_;
					return;
				}
				if (opcode == 4101) {
					stringStackPtr -= 2;
					String string = stringStack[stringStackPtr];
					String string_190_ = stringStack[stringStackPtr + 1];
					stringStack[stringStackPtr++] = string + string_190_;
					return;
				}
				if (opcode == 4102) {
					String string = stringStack[--stringStackPtr];
					int i_191_ = integerStack[--integerStackPtr];
					stringStack[stringStackPtr++] = string + Class44.decimalToString(i_191_, false, true);
					return;
				}
				if (opcode == 4103) {
					String string = stringStack[--stringStackPtr];
					stringStack[stringStackPtr++] = string.toLowerCase();
					return;
				}
				if (opcode == 4104) {
					stringStack[stringStackPtr++] = method3149(integerStack[--integerStackPtr]);
					return;
				}
				if (opcode == 4105) {
					stringStackPtr -= 2;
					String string = stringStack[stringStackPtr];
					String string_192_ = stringStack[stringStackPtr + 1];
					if (Class87.localPlayer.appearence != null && Class87.localPlayer.appearence.female) {
						stringStack[stringStackPtr++] = string_192_;
					} else {
						stringStack[stringStackPtr++] = string;
						return;
					}
					return;
				}
				if (opcode == 4106) {
					int i_193_ = integerStack[--integerStackPtr];
					stringStack[stringStackPtr++] = Integer.toString(i_193_);
					return;
				}
				if (opcode == 4107) {
					stringStackPtr -= 2;
					integerStack[integerStackPtr++] = Class336.method3772(stringStack[stringStackPtr], stringStack[stringStackPtr + 1], client.gameLanguage, 1166845806);
					return;
				}
				if (opcode == 4108) {
					String string = stringStack[--stringStackPtr];
					integerStackPtr -= 2;
					int i_194_ = integerStack[integerStackPtr];
					int i_195_ = integerStack[integerStackPtr + 1];
					FontSpecifications class197 = FontSpecifications.load((byte) 121, 0, i_195_, client.fontJs5);
					integerStack[integerStackPtr++] = class197.method2669(i_194_, 0, string, OrthoZoomPreferenceField.nameIcons);
					return;
				}
				if (opcode == 4109) {
					String string = stringStack[--stringStackPtr];
					integerStackPtr -= 2;
					int i_196_ = integerStack[integerStackPtr];
					int i_197_ = integerStack[integerStackPtr + 1];
					FontSpecifications class197 = FontSpecifications.load((byte) 119, 0, i_197_, client.fontJs5);
					integerStack[integerStackPtr++] = class197.getStringWidth(i_196_, string, OrthoZoomPreferenceField.nameIcons, (byte) 5);
					return;
				}
				if (opcode == 4110) {
					stringStackPtr -= 2;
					String string = stringStack[stringStackPtr];
					String string_198_ = stringStack[stringStackPtr + 1];
					if (integerStack[--integerStackPtr] == 1) {
						stringStack[stringStackPtr++] = string;
					} else {
						stringStack[stringStackPtr++] = string_198_;
						return;
					}
					return;
				}
				if (opcode == 4111) {
					String string = stringStack[--stringStackPtr];
					stringStack[stringStackPtr++] = Class249.escapeHtml(string, 62);
					return;
				}
				if (opcode == 4112) {
					String string = stringStack[--stringStackPtr];
					int i_199_ = integerStack[--integerStackPtr];
					if (i_199_ == -1) {
						throw new RuntimeException("null char");
					}
					stringStack[stringStackPtr++] = string + (char) i_199_;
					return;
				}
				if (opcode == 4113) {
					int i_200_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = method3146((char) i_200_);
					return;
				}
				if (opcode == 4114) {
					int i_201_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class114.method2147((char) i_201_, 118) ? 1 : 0;
					return;
				}
				if (opcode == 4115) {
					int i_202_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub46_Sub15.method1611((byte) 124, (char) i_202_) ? 1 : 0;
					return;
				}
				if (opcode == 4116) {
					int i_203_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = RotatingSpriteLoadingScreenElement.method2245(18646, (char) i_203_) ? 1 : 0;
					return;
				}
				if (opcode == 4117) {
					String string = stringStack[--stringStackPtr];
					if (string != null) {
						integerStack[integerStackPtr++] = string.length();
					} else {
						integerStack[integerStackPtr++] = 0;
						return;
					}
					return;
				}
				if (opcode == 4118) {
					String string = stringStack[--stringStackPtr];
					integerStackPtr -= 2;
					int i_204_ = integerStack[integerStackPtr];
					int i_205_ = integerStack[integerStackPtr + 1];
					stringStack[stringStackPtr++] = string.substring(i_204_, i_205_);
					return;
				}
				if (opcode == 4119) {
					String string = stringStack[--stringStackPtr];
					StringBuffer stringbuffer = new StringBuffer(string.length());
					boolean bool_206_ = false;
					for (int i_207_ = 0; i_207_ < string.length(); i_207_++) {
						char c = string.charAt(i_207_);
						if (c == '<') {
							bool_206_ = true;
						} else if (c == '>') {
							bool_206_ = false;
						} else if (!bool_206_) {
							stringbuffer.append(c);
						}
					}
					stringStack[stringStackPtr++] = stringbuffer.toString();
					return;
				}
				if (opcode == 4120) {
					String string = stringStack[--stringStackPtr];
					integerStackPtr -= 2;
					int i_208_ = integerStack[integerStackPtr];
					int i_209_ = integerStack[integerStackPtr + 1];
					integerStack[integerStackPtr++] = string.indexOf(i_208_, i_209_);
					return;
				}
				if (opcode == 4121) {
					stringStackPtr -= 2;
					String string = stringStack[stringStackPtr];
					String string_210_ = stringStack[stringStackPtr + 1];
					int i_211_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = string.indexOf(string_210_, i_211_);
					return;
				}
				if (opcode == 4122) {
					int i_212_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Character.toLowerCase((char) i_212_);
					return;
				}
				if (opcode == 4123) {
					int i_213_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Character.toUpperCase((char) i_213_);
					return;
				}
				if (opcode == 4124) {
					boolean bool_214_ = integerStack[--integerStackPtr] != 0;
					int i_215_ = integerStack[--integerStackPtr];
					stringStack[stringStackPtr++] = Class39.method349(client.gameLanguage, 0, 48, i_215_, bool_214_);
					return;
				}
				if (opcode == 4125) {
					String string = stringStack[--stringStackPtr];
					int i_216_ = integerStack[--integerStackPtr];
					FontSpecifications class197 = FontSpecifications.load((byte) 115, 0, i_216_, client.fontJs5);
					integerStack[integerStackPtr++] = class197.method2676((byte) 82, OrthoZoomPreferenceField.nameIcons, string);
					return;
				}
			} else if (opcode < 4300) {
				if (opcode == 4200) {
					int itemId = integerStack[--integerStackPtr];
					stringStack[stringStackPtr++] = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -126).name;
					return;
				}
				if (opcode == 4201) {
					integerStackPtr -= 2;
					int itemId = integerStack[integerStackPtr];
					int opIdx = integerStack[integerStackPtr + 1];
					ItemDefinition definition = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -119);
					if (opIdx >= 1 && opIdx <= 5 && definition.options[opIdx - 1] != null) {
						stringStack[stringStackPtr++] = definition.options[opIdx - 1];
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 4202) {
					integerStackPtr -= 2;
					int itemId = integerStack[integerStackPtr];
					int opIdx = integerStack[integerStackPtr + 1];
					ItemDefinition class297 = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -125);
					if (opIdx >= 1 && opIdx <= 5 && class297.interfaceOptions[opIdx - 1] != null) {
						stringStack[stringStackPtr++] = class297.interfaceOptions[opIdx - 1];
					} else {
						stringStack[stringStackPtr++] = "";
						return;
					}
					return;
				}
				if (opcode == 4203) {
					int itemId = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -120).cost;
					return;
				}
				if (opcode == 4204) {
					int itemId = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -117).stackable == 1 ? 1 : 0;
					return;
				}
				if (opcode == 4205) {// Get certificate id (noted id)
					int itemId = integerStack[--integerStackPtr];
					ItemDefinition definition = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -116);
					if (definition.certTemplate == -1 && definition.certLink >= 0) {
						integerStack[integerStackPtr++] = definition.certLink;
					} else {
						integerStack[integerStackPtr++] = itemId;
						return;
					}
					return;
				}
				if (opcode == 4206) {// Get normal id (unnoted id)
					int itemId = integerStack[--integerStackPtr];
					ItemDefinition definition = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -126);
					if (definition.certTemplate >= 0 && definition.certLink >= 0) {
						integerStack[integerStackPtr++] = definition.certLink;
					} else {
						integerStack[integerStackPtr++] = itemId;
						return;
					}
					return;
				}
				if (opcode == 4207) {
					int itemId = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -124).members ? 1 : 0;
					return;
				}
				if (opcode == 4208) {
					integerStackPtr -= 2;
					int itemId = integerStack[integerStackPtr];
					int paramId = integerStack[integerStackPtr + 1];
					ParamDefinition definition = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, paramId);
					if (definition.isString(false)) {
						stringStack[stringStackPtr++] = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -126).getParam(definition.defaultString, -1, paramId);
					} else {
						integerStack[integerStackPtr++] = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -123).method3494(paramId, (byte) -86, definition.defaultInteger);
						return;
					}
					return;
				}
				if (opcode == 4209) {
					integerStackPtr -= 2;
					int itemId = integerStack[integerStackPtr];
					int opIdx = integerStack[integerStackPtr + 1] - 1;
					ItemDefinition definition = Class98_Sub46_Sub19.itemDefinitionList.get(itemId, (byte) -116);
					if (definition.cursor1InterfaceOptions == opIdx) {
						integerStack[integerStackPtr++] = definition.interfaceCursor1;
					} else {
						if (definition.cursor2InterfaceOptions == opIdx) {
							integerStack[integerStackPtr++] = definition.interfaceCursor2;
						} else {
							integerStack[integerStackPtr++] = -1;
							return;
						}
						return;
					}
					return;
				}
				if (opcode == 4210) {
					String name = stringStack[--stringStackPtr];
					int stockmarketOnly = integerStack[--integerStackPtr];
					ItemSearch.findItems(stockmarketOnly == 1, name, -97);
					integerStack[integerStackPtr++] = Class18.resultBufferSize;
					return;
				}
				if (opcode == 4211) {
					if (MaxScreenSizePreferenceField.resultIndexBuffer == null || Class85.resultBufferPtr >= Class18.resultBufferSize) {
						integerStack[integerStackPtr++] = -1;
					} else {
						integerStack[integerStackPtr++] = MaxScreenSizePreferenceField.resultIndexBuffer[Class85.resultBufferPtr++] & 0xffff;
						return;
					}
					return;
				}
				if (opcode == 4212) {
					Class85.resultBufferPtr = 0;
					return;
				}
				if (opcode == 4213) {
					int i_232_ = integerStack[--integerStackPtr];
					integerStack[integerStackPtr++] = Class98_Sub46_Sub19.itemDefinitionList.get(i_232_, (byte) -117).multiStackSize;
					return;
				}
				if (opcode == 4214) {
					String name = stringStack[--stringStackPtr];
					integerStackPtr -= 3;
					int stockMarketOnly = integerStack[integerStackPtr];
					int paramIndex = integerStack[integerStackPtr + 1];
					int paramValue = integerStack[integerStackPtr + 2];
					ItemSearch.findItems(stockMarketOnly == 1, name, paramValue, -1, paramIndex);
					integerStack[integerStackPtr++] = Class18.resultBufferSize;
					return;
				}
				if (opcode == 4215) {
					stringStackPtr -= 2;
					integerStackPtr -= 2;
					String name = stringStack[stringStackPtr];
					int stockMarketOnly = integerStack[integerStackPtr];
					int paramIndex = integerStack[integerStackPtr + 1];
					String paramValue = stringStack[stringStackPtr + 1];
					ItemSearch.findItems(name, paramValue, (byte) 34, stockMarketOnly == 1, paramIndex);
					integerStack[integerStackPtr++] = Class18.resultBufferSize;
					return;
				}
			} else if (opcode < 4400) {
				if (opcode == 4300) {
					integerStackPtr -= 2;
					int npcIndex = integerStack[integerStackPtr];
					int paramIndex = integerStack[integerStackPtr + 1];
					ParamDefinition definition = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, paramIndex);
					if (definition.isString(false)) {
						stringStack[stringStackPtr++] = Class4.npcDefinitionList.get(5, npcIndex).method2298(paramIndex, -105, definition.defaultString);
					} else {
						integerStack[integerStackPtr++] = Class4.npcDefinitionList.get(5, npcIndex).method2305(paramIndex, definition.defaultInteger, (byte) 127);
						return;
					}
					return;
				}
			} else if (opcode < 4500) {
				if (opcode == 4400) {
					integerStackPtr -= 2;
					int i_241_ = integerStack[integerStackPtr];
					int i_242_ = integerStack[integerStackPtr + 1];
					ParamDefinition class149 = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, i_242_);
					if (class149.isString(false)) {
						stringStack[stringStackPtr++] = Class130.gameObjectDefinitionList.get(i_241_, (byte) 119).method3864((byte) 109, i_242_, class149.defaultString);
					} else {
						integerStack[integerStackPtr++] = Class130.gameObjectDefinitionList.get(i_241_, (byte) 119).method3866(class149.defaultInteger, i_242_, 1);
						return;
					}
					return;
				}
			} else if (opcode < 4600) {
				if (opcode == 4500) {
					integerStackPtr -= 2;
					int i_243_ = integerStack[integerStackPtr];
					int i_244_ = integerStack[integerStackPtr + 1];
					ParamDefinition class149 = Class98_Sub43_Sub1.paramDefinitionList.list((byte) 31, i_244_);
					if (class149.isString(false)) {
						stringStack[stringStackPtr++] = Class62.structsDefinitionList.method3224(26, i_243_).method1586(i_244_, (byte) -19, class149.defaultString);
					} else {
						integerStack[integerStackPtr++] = Class62.structsDefinitionList.method3224(26, i_243_).method1585(i_244_, true, class149.defaultInteger);
						return;
					}
					return;
				}
			} else if (opcode < 4700 && opcode == 4600) {
				int i_245_ = integerStack[--integerStackPtr];
				RenderAnimDefinition class294 = Class370.renderAnimDefinitionList.method3199(false, i_245_);
				if (class294.anIntArray2395 != null && class294.anIntArray2395.length > 0) {
					int i_246_ = 0;
					int i_247_ = class294.anIntArray2386[0];
					for (int i_248_ = 1; i_248_ < class294.anIntArray2395.length; i_248_++) {
						if (class294.anIntArray2386[i_248_] > i_247_) {
							i_246_ = i_248_;
							i_247_ = class294.anIntArray2386[i_248_];
						}
					}
					integerStack[integerStackPtr++] = class294.anIntArray2395[i_246_];
				} else {
					integerStack[integerStackPtr++] = class294.anInt2396;
					return;
				}
				return;
			}
		}
		throw new IllegalStateException(String.valueOf(opcode));
	}

	private static final String method3149(int i) {
		long l = (i + 11745L) * 86400000L;
		aCalendar1882.setTime(new Date(l));
		int i_249_ = aCalendar1882.get(5);
		int i_250_ = aCalendar1882.get(2);
		int i_251_ = aCalendar1882.get(1);
		return String.valueOf(i_249_) + "-" + aStringArray1892[i_250_] + "-" + i_251_;
	}

	private static final void method3150(ClientScript2Event class98_sub21, int i) {
		Object[] objects = class98_sub21.param;
		int i_252_ = ((Integer) objects[0]).intValue();
		Class98_Sub46_Sub4 class98_sub46_sub4 = CacheFileRequest.method1601(i_252_, 100);
		if (class98_sub46_sub4 != null) {
			anIntArray1875 = new int[class98_sub46_sub4.anInt5958];
			int i_253_ = 0;
			aStringArray1886 = new String[class98_sub46_sub4.anInt5964];
			int i_254_ = 0;
			for (int i_255_ = 1; i_255_ < objects.length; i_255_++) {
				if (objects[i_255_] instanceof Integer) {
					int i_256_ = ((Integer) objects[i_255_]).intValue();
					if (i_256_ == -2147483647) {
						i_256_ = class98_sub21.mouseX;
					}
					if (i_256_ == -2147483646) {
						i_256_ = class98_sub21.mouseY;
					}
					if (i_256_ == -2147483645) {
						i_256_ = class98_sub21.component != null ? class98_sub21.component.idDword : -1;
					}
					if (i_256_ == -2147483644) {
						i_256_ = class98_sub21.code;
					}
					if (i_256_ == -2147483643) {
						i_256_ = class98_sub21.component != null ? class98_sub21.component.componentIndex : -1;
					}
					if (i_256_ == -2147483642) {
						i_256_ = class98_sub21.aClass293_3982 != null ? class98_sub21.aClass293_3982.idDword : -1;
					}
					if (i_256_ == -2147483641) {
						i_256_ = class98_sub21.aClass293_3982 != null ? class98_sub21.aClass293_3982.componentIndex : -1;
					}
					if (i_256_ == -2147483640) {
						i_256_ = class98_sub21.anInt3977;
					}
					if (i_256_ == -2147483639) {
						i_256_ = class98_sub21.anInt3976;
					}
					anIntArray1875[i_253_++] = i_256_;
				} else if (objects[i_255_] instanceof String) {
					String string = (String) objects[i_255_];
					if (string.equals("event_opbase")) {
						string = class98_sub21.opbase;
					}
					aStringArray1886[i_254_++] = string;
				}
			}
			anInt1893 = class98_sub21.anInt3990;
			method3153(class98_sub46_sub4, i);
		}
	}

	public static final void method3152(Class105 class105, int i, int i_261_) {
		Class98_Sub46_Sub4 class98_sub46_sub4 = Huffman.method2779((byte) -109, i_261_, i, class105);
		if (class98_sub46_sub4 != null) {
			anIntArray1875 = new int[class98_sub46_sub4.anInt5958];
			aStringArray1886 = new String[class98_sub46_sub4.anInt5964];
			if (class98_sub46_sub4.aClass105_5957 == VarClientDefinition.aClass105_719 || class98_sub46_sub4.aClass105_5957 == Class331.aClass105_2792 || class98_sub46_sub4.aClass105_5957 == Class98_Sub10_Sub26.aClass105_5684) {
				int i_262_ = 0;
				int i_263_ = 0;
				if (CursorDefinitionParser.aClass293_120 != null) {
					i_262_ = CursorDefinitionParser.aClass293_120.renderX;
					i_263_ = CursorDefinitionParser.aClass293_120.renderY;
				}
				anIntArray1875[0] = client.mouseListener.getMouseX(112) - i_262_;
				anIntArray1875[1] = client.mouseListener.getMouseY((byte) 72) - i_263_;
			}
			method3153(class98_sub46_sub4, 200000);
		}
	}

	private static final void method3153(Class98_Sub46_Sub4 class98_sub46_sub4, int i) {
		integerStackPtr = 0;
		stringStackPtr = 0;
		int i_264_ = -1;
		int[] is = class98_sub46_sub4.anIntArray5963;
		int[] is_265_ = class98_sub46_sub4.anIntArray5967;
		int i_266_ = -1;
		anInt1888 = 0;
		try {
			int i_267_ = 0;
			for (;;) {
				if (++i_267_ > i) {
					throw new RuntimeException("slow");
				}
				i_266_ = is[++i_264_];
				if (i_266_ < 100) {
					if (i_266_ == 0) {
						integerStack[integerStackPtr++] = is_265_[i_264_];
					} else if (i_266_ == 1) {
						int i_268_ = is_265_[i_264_];
						integerStack[integerStackPtr++] = StartupStage.varValues.values[i_268_];
					} else if (i_266_ == 2) {
						int i_269_ = is_265_[i_264_];
						StartupStage.varValues.method2291(i_269_, 98, integerStack[--integerStackPtr]);
					} else if (i_266_ == 3) {
						stringStack[stringStackPtr++] = class98_sub46_sub4.aStringArray5959[i_264_];
					} else if (i_266_ == 6) {
						i_264_ += is_265_[i_264_];
					} else if (i_266_ == 7) {
						integerStackPtr -= 2;
						if (integerStack[integerStackPtr] != integerStack[integerStackPtr + 1]) {
							i_264_ += is_265_[i_264_];
						}
					} else if (i_266_ == 8) {
						integerStackPtr -= 2;
						if (integerStack[integerStackPtr] == integerStack[integerStackPtr + 1]) {
							i_264_ += is_265_[i_264_];
						}
					} else if (i_266_ == 9) {
						integerStackPtr -= 2;
						if (integerStack[integerStackPtr] < integerStack[integerStackPtr + 1]) {
							i_264_ += is_265_[i_264_];
						}
					} else if (i_266_ == 10) {
						integerStackPtr -= 2;
						if (integerStack[integerStackPtr] > integerStack[integerStackPtr + 1]) {
							i_264_ += is_265_[i_264_];
						}
					} else if (i_266_ == 21) {
						if (anInt1888 == 0) {
							return;
						}
						Class349 class349 = aClass349Array1889[--anInt1888];
						class98_sub46_sub4 = class349.aClass98_Sub46_Sub4_2918;
						is = class98_sub46_sub4.anIntArray5963;
						is_265_ = class98_sub46_sub4.anIntArray5967;
						i_264_ = class349.anInt2919;
						anIntArray1875 = class349.anIntArray2916;
						aStringArray1886 = class349.aStringArray2917;
					} else if (i_266_ == 25) {
						int i_270_ = is_265_[i_264_];
						integerStack[integerStackPtr++] = StartupStage.varValues.getClientVarpBit(i_270_, 7373);
					} else if (i_266_ == 27) {
						int i_271_ = is_265_[i_264_];
						StartupStage.varValues.method2289(integerStack[--integerStackPtr], i_271_, 0);
					} else if (i_266_ == 31) {
						integerStackPtr -= 2;
						if (integerStack[integerStackPtr] <= integerStack[integerStackPtr + 1]) {
							i_264_ += is_265_[i_264_];
						}
					} else if (i_266_ == 32) {
						integerStackPtr -= 2;
						if (integerStack[integerStackPtr] >= integerStack[integerStackPtr + 1]) {
							i_264_ += is_265_[i_264_];
						}
					} else if (i_266_ == 33) {
						integerStack[integerStackPtr++] = anIntArray1875[is_265_[i_264_]];
					} else if (i_266_ == 34) {
						anIntArray1875[is_265_[i_264_]] = integerStack[--integerStackPtr];
					} else if (i_266_ == 35) {
						stringStack[stringStackPtr++] = aStringArray1886[is_265_[i_264_]];
					} else if (i_266_ == 36) {
						aStringArray1886[is_265_[i_264_]] = stringStack[--stringStackPtr];
					} else if (i_266_ == 37) {
						int i_272_ = is_265_[i_264_];
						stringStackPtr -= i_272_;
						String string = Class98_Sub5_Sub2.method968(i_272_, stringStack, stringStackPtr, -17120);
						stringStack[stringStackPtr++] = string;
					} else if (i_266_ == 38) {
						integerStackPtr--;
					} else if (i_266_ == 39) {
						stringStackPtr--;
					} else if (i_266_ == 40) {
						int i_273_ = is_265_[i_264_];
						Class98_Sub46_Sub4 class98_sub46_sub4_274_ = CacheFileRequest.method1601(i_273_, 100);
						if (class98_sub46_sub4_274_ == null) {
							throw new RuntimeException();
						}
						int[] is_275_ = new int[class98_sub46_sub4_274_.anInt5958];
						String[] strings = new String[class98_sub46_sub4_274_.anInt5964];
						for (int i_276_ = 0; i_276_ < class98_sub46_sub4_274_.anInt5966; i_276_++) {
							is_275_[i_276_] = integerStack[integerStackPtr - class98_sub46_sub4_274_.anInt5966 + i_276_];
						}
						for (int i_277_ = 0; i_277_ < class98_sub46_sub4_274_.anInt5965; i_277_++) {
							strings[i_277_] = stringStack[stringStackPtr - class98_sub46_sub4_274_.anInt5965 + i_277_];
						}
						integerStackPtr -= class98_sub46_sub4_274_.anInt5966;
						stringStackPtr -= class98_sub46_sub4_274_.anInt5965;
						Class349 class349 = new Class349();
						class349.aClass98_Sub46_Sub4_2918 = class98_sub46_sub4;
						class349.anInt2919 = i_264_;
						class349.anIntArray2916 = anIntArray1875;
						class349.aStringArray2917 = aStringArray1886;
						if (anInt1888 >= aClass349Array1889.length) {
							throw new RuntimeException();
						}
						aClass349Array1889[anInt1888++] = class349;
						class98_sub46_sub4 = class98_sub46_sub4_274_;
						is = class98_sub46_sub4.anIntArray5963;
						is_265_ = class98_sub46_sub4.anIntArray5967;
						i_264_ = -1;
						anIntArray1875 = is_275_;
						aStringArray1886 = strings;
					} else if (i_266_ == 42) {
						integerStack[integerStackPtr++] = Class76_Sub5.intGlobalConfigs[is_265_[i_264_]];
					} else if (i_266_ == 43) {
						int i_278_ = is_265_[i_264_];
						Class76_Sub5.intGlobalConfigs[i_278_] = integerStack[--integerStackPtr];
						Class119_Sub1.method2180(i_278_, 15233);
						Class66.aBoolean507 |= Class140.boolGlobalConfigs[i_278_];
					} else if (i_266_ == 44) {
						int i_279_ = is_265_[i_264_] >> 16;
						int i_280_ = is_265_[i_264_] & 0xffff;
						int i_281_ = integerStack[--integerStackPtr];
						if (i_281_ < 0 || i_281_ > 5000) {
							throw new RuntimeException();
						}
						anIntArray1887[i_279_] = i_281_;
						int i_282_ = -1;
						if (i_280_ == 105) {
							i_282_ = 0;
						}
						for (int i_283_ = 0; i_283_ < i_281_; i_283_++) {
							integerArrays[i_279_][i_283_] = i_282_;
						}
					} else if (i_266_ == 45) {
						int i_284_ = is_265_[i_264_];
						int i_285_ = integerStack[--integerStackPtr];
						if (i_285_ < 0 || i_285_ >= anIntArray1887[i_284_]) {
							throw new RuntimeException();
						}
						integerStack[integerStackPtr++] = integerArrays[i_284_][i_285_];
					} else if (i_266_ == 46) {
						int i_286_ = is_265_[i_264_];
						integerStackPtr -= 2;
						int i_287_ = integerStack[integerStackPtr];
						if (i_287_ < 0 || i_287_ >= anIntArray1887[i_286_]) {
							throw new RuntimeException();
						}
						integerArrays[i_286_][i_287_] = integerStack[integerStackPtr + 1];
					} else if (i_266_ == 47) {
						String string = Class151_Sub1.strGlobalConfigs[is_265_[i_264_]];
						if (string == null) {
							string = "null";
						}
						stringStack[stringStackPtr++] = string;
					} else if (i_266_ == 48) {
						int i_288_ = is_265_[i_264_];
						Class151_Sub1.strGlobalConfigs[i_288_] = stringStack[--stringStackPtr];
						Class347.method3833(i_288_, 2);
					} else if (i_266_ == 51) {
						HashTable class377 = class98_sub46_sub4.aClass377Array5956[is_265_[i_264_]];
						NodeInteger class98_sub34 = (NodeInteger) class377.get(integerStack[--integerStackPtr], -1);
						if (class98_sub34 != null) {
							i_264_ += class98_sub34.value;
						}
					} else if (i_266_ == 86) {
						if (integerStack[--integerStackPtr] == 1) {
							i_264_ += is_265_[i_264_];
						}
					} else if (i_266_ == 87 && integerStack[--integerStackPtr] == 0) {
						i_264_ += is_265_[i_264_];
					}
				} else {
					boolean bool;
					bool = is_265_[i_264_] == 1;
					if (i_266_ >= 100 && i_266_ < 5000) {
						method3148(i_266_, bool);
					} else {
						if (i_266_ < 5000 || i_266_ >= 10000) {
							break;
						}
						handleClientButtons(i_266_, bool);
					}
				}
			}
			throw new IllegalStateException("Command: " + i_266_);
		} catch (Exception exception) {
			if (class98_sub46_sub4.aString5968 != null) {
				OpenGLHeap.addChatMessage("Clientscript error in: " + class98_sub46_sub4.aString5968, 4, (byte) 68);
				StringBuffer stringbuffer = new StringBuffer(30);
				stringbuffer.append("Clientscript error in: ").append(class98_sub46_sub4.aString5968).append("\n");
				for (int i_289_ = anInt1888 - 1; i_289_ >= 0; i_289_--) {
					stringbuffer.append("via: ").append(aClass349Array1889[i_289_].aClass98_Sub46_Sub4_2918.aString5968).append("\n");
				}
				stringbuffer.append("Op: ").append(i_266_).append("\n");
				String string = exception.getMessage();
				if (string != null && string.length() > 0) {
					stringbuffer.append("Message: ").append(string).append("\n");
				}
				Class305_Sub1.reportError(exception, -125, stringbuffer.toString());
				Cacheable.write(stringbuffer.toString(), -126);
			} else {
				StringBuffer stringbuffer = new StringBuffer(30);
				stringbuffer.append("CS2: ").append(class98_sub46_sub4.hash).append(" ");
				for (int i_290_ = anInt1888 - 1; i_290_ >= 0; i_290_--) {
					stringbuffer.append("v: ").append(aClass349Array1889[i_290_].aClass98_Sub46_Sub4_2918.hash).append(" ");
				}
				stringbuffer.append("op: ").append(i_266_);
				Class305_Sub1.reportError(exception, -125, stringbuffer.toString());
			}
		}
	}

	public static void method3154() {
		anIntArray1875 = null;
		aStringArray1886 = null;
		anIntArray1887 = null;
		integerArrays = null;
		integerStack = null;
		stringStack = null;
		aClass349Array1889 = null;
		aClass293_1879 = null;
		aClass293_1877 = null;
		quickChat = null;
		aCalendar1882 = null;
		aStringArray1892 = null;
		anIntArray1891 = null;
		aClass79_1890 = null;
	}

	public static final void sendWindowPane(int paneId) {
		if (paneId != -1 && Class85.loadInterface(paneId, 73)) {
			RtInterface[] pane = Class159.interfaceStore[paneId];
			for (RtInterface interf : pane) {
				if (interf.anObjectArray2332 != null) {
					ClientScript2Event script = new ClientScript2Event();
					script.component = interf;
					script.param = interf.anObjectArray2332;
					method3150(script, 2000000);
				}
			}
		}
	}
}


/* Class42_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.model.NativeModelRenderer;

import jaggl.OpenGL;

public final class Class42_Sub2 extends Class42 {
	public static Image aClass324_5359;

	public static final void method388(boolean bool) {
		do {
			try {
				int i = 1024;
				int i_2_ = 3072;
				if (VarClientStringsDefinitionParser.aBoolean1839) {
					i_2_ = 4096;
					if (Class69.orthoCameraLock) {
						i = 2048;
					}
				}
				if (i > Class119_Sub4.aFloat4740) {
					Class119_Sub4.aFloat4740 = i;
				}
				if (Class119_Sub4.aFloat4740 > i_2_) {
					Class119_Sub4.aFloat4740 = i_2_;
				}
				for (/**/; RsFloatBuffer.aFloat5794 >= 16384.0F; RsFloatBuffer.aFloat5794 -= 16384.0F) {
					/* empty */
				}
				for (/**/; RsFloatBuffer.aFloat5794 < 0.0F; RsFloatBuffer.aFloat5794 += 16384.0F) {
					/* empty */
				}
				int i_3_ = Minimap.cameraXPosition >> 698948297;
				int i_4_ = Class224_Sub3_Sub1.cameraYPosition >> -35654487;
				int i_5_ = StrongReferenceMCNode.getHeight(Font.localPlane, Class224_Sub3_Sub1.cameraYPosition, Minimap.cameraXPosition, 24111);
				if (bool == true) {
					int i_6_ = 0;
					if ((i_3_ ^ 0xffffffff) < -4 && (i_4_ ^ 0xffffffff) < -4 && (Class165.mapWidth + -4 ^ 0xffffffff) < (i_3_ ^ 0xffffffff) && (Class98_Sub10_Sub7.mapLength + -4 ^ 0xffffffff) < (i_4_ ^ 0xffffffff)) {
						for (int i_7_ = -4 + i_3_; (4 + i_3_ ^ 0xffffffff) <= (i_7_ ^ 0xffffffff); i_7_++) {
							for (int i_8_ = i_4_ - 4; 4 + i_4_ >= i_8_; i_8_++) {
								int i_9_ = Font.localPlane;
								if (i_9_ < 3 && Class1.isBridge(i_8_, (byte) -104, i_7_)) {
									i_9_++;
								}
								int i_10_ = 0;
								if (NativeModelRenderer.aClass305_Sub1_4952.aByteArrayArrayArray2554 != null && NativeModelRenderer.aClass305_Sub1_4952.aByteArrayArrayArray2554[i_9_] != null) {
									i_10_ = (0xff & NativeModelRenderer.aClass305_Sub1_4952.aByteArrayArrayArray2554[i_9_][i_7_][i_8_]) * 8 << 976034466;
								}
								if (Class78.aSArray594 != null && Class78.aSArray594[i_9_] != null) {
									int i_11_ = i_5_ - Class78.aSArray594[i_9_].getTileHeight(i_8_, -12639, i_7_) - -i_10_;
									if ((i_11_ ^ 0xffffffff) < (i_6_ ^ 0xffffffff)) {
										i_6_ = i_11_;
									}
								}
							}
						}
					}
					int i_12_ = (i_6_ >> 142317154) * 1536;
					if (i_12_ > 786432) {
						i_12_ = 786432;
					}
					if (i_12_ < 262144) {
						i_12_ = 262144;
					}
					if (Font.anInt372 < i_12_) {
						Font.anInt372 += (-Font.anInt372 + i_12_) / 24;
					} else {
						if ((Font.anInt372 ^ 0xffffffff) >= (i_12_ ^ 0xffffffff)) {
							break;
						}
						Font.anInt372 += (i_12_ + -Font.anInt372) / 80;
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "hw.C(" + bool + ')');
			}
			break;
		} while (false);
	}

	public static final int[] method389(int i, OutgoingPacket class98_sub11) {
		try {
			RSByteBuffer class98_sub22 = new RSByteBuffer(518);
			if (i != 12206) {
				method388(true);
			}
			int[] is = new int[4];
			for (int i_19_ = 0; (i_19_ ^ 0xffffffff) > -5; i_19_++) {
				is[i_19_] = (int) (9.9999999E7 * Math.random());
			}
			class98_sub22.writeByte(10, 86);
			class98_sub22.writeInt(1571862888, is[0]);
			class98_sub22.writeInt(1571862888, is[1]);
			class98_sub22.writeInt(i ^ 0x5db096c6, is[2]);
			class98_sub22.writeInt(1571862888, is[3]);
			for (int i_20_ = 0; (i_20_ ^ 0xffffffff) > -11; i_20_++) {
				class98_sub22.writeInt(1571862888, (int) (9.9999999E7 * Math.random()));
			}
			class98_sub22.writeShort((int) (Math.random() * 9.9999999E7), i ^ 0x5db096c6);
			class98_sub22.rsaEncryption(RSByteBuffer.MODULUS, true, RSByteBuffer.PUBLIC_EXPONENT);
			class98_sub11.packet.method1217(class98_sub22.payload, class98_sub22.position, i + -12207, 0);
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hw.D(" + i + ',' + (class98_sub11 != null ? "{...}" : "null") + ')');
		}
	}

	public static void method390(boolean bool) {
		try {
			if (bool != false) {
				method389(103, null);
			}
			aClass324_5359 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hw.E(" + bool + ')');
		}
	}

	int			anInt5357;

	private int	anInt5358	= -1;

	private int	anInt5360	= -1;

	Class42_Sub2(OpenGlToolkit var_ha_Sub1, int i, int i_0_) {
		super(var_ha_Sub1, 34067, i, 6 * i_0_ * i_0_, false);
		try {
			anInt5357 = i_0_;
			this.aHa_Sub1_3227.setActiveTexture(1, this);
			for (int i_1_ = 0; i_1_ < 6; i_1_++) {
				OpenGL.glTexImage2Dub(i_1_ + 34069, 0, this.anInt3230, i_0_, i_0_, 0, Class98_Sub31_Sub2.method1339(this.anInt3230, 126), 5121, null, 0);
			}
			method372(-28003, true);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hw.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_0_ + ')');
		}
	}

	Class42_Sub2(OpenGlToolkit var_ha_Sub1, int i, int i_13_, boolean bool, byte[][] is, int i_14_) {
		super(var_ha_Sub1, 34067, i, 6 * i_13_ * i_13_, bool);
		try {
			anInt5357 = i_13_;
			this.aHa_Sub1_3227.setActiveTexture(1, this);
			for (int i_15_ = 0; (i_15_ ^ 0xffffffff) > -7; i_15_++) {
				OpenGL.glTexImage2Dub(34069 - -i_15_, 0, this.anInt3230, i_13_, i_13_, 0, i_14_, 5121, is[i_15_], 0);
			}
			method372(-28003, true);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hw.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_13_ + ',' + bool + ',' + (is != null ? "{...}" : "null") + ',' + i_14_ + ')');
		}
	}

	Class42_Sub2(OpenGlToolkit var_ha_Sub1, int i, int i_16_, boolean bool, int[][] is) {
		super(var_ha_Sub1, 34067, i, i_16_ * i_16_ * 6, bool);
		try {
			anInt5357 = i_16_;
			this.aHa_Sub1_3227.setActiveTexture(1, this);
			if (bool) {
				for (int i_17_ = 0; i_17_ < 6; i_17_++) {
					Class336.method3773(this.anInt3230, 53, this.aHa_Sub1_3227.textureType, 34069 + i_17_, i_16_, 32993, i_16_, is[i_17_]);
				}
			} else {
				for (int i_18_ = 0; (i_18_ ^ 0xffffffff) > -7; i_18_++) {
					OpenGL.glTexImage2Di(34069 - -i_18_, 0, this.anInt3230, i_16_, i_16_, 0, 32993, this.aHa_Sub1_3227.textureType, is[i_18_], 0);
				}
			}
			method372(-28003, true);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hw.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_16_ + ',' + bool + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method3(byte i) {
		try {
			if (i > -117) {
				anInt5360 = -45;
			}
			OpenGL.glFramebufferTexture2DEXT(anInt5358, anInt5360, 3553, 0, 0);
			anInt5358 = -1;
			anInt5360 = -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hw.B(" + i + ')');
		}
	}

	public final void method391(int i, int i_21_, int i_22_, int i_23_, byte i_24_) {
		try {
			OpenGL.glFramebufferTexture2DEXT(i_23_, i_21_, i_22_, this.anInt3229, i);
			anInt5360 = i_21_;
			anInt5358 = i_23_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hw.A(" + i + ',' + i_21_ + ',' + i_22_ + ',' + i_23_ + ',' + i_24_ + ')');
		}
	}
}

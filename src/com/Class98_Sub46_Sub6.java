/* Class98_Sub46_Sub6 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.preferences.ParticlesPreferenceField;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;
import com.jagex.game.client.ui.loading.impl.ConfigurableLoadingScreen;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class Class98_Sub46_Sub6 extends Cacheable {
	public static IncomingOpcode	aClass58_5975	= new IncomingOpcode(60, 4);
	public static int				anInt5979;
	public static int[]				anIntArray5980	= new int[3];

	public static final FontSpecifications loadFontMetrics(int i, int i_13_, RSToolkit var_ha) {
		Class244 class244 = Class114.method2151(i, true, var_ha, true);
		if (class244 == null) {
			return null;
		}
		return class244.aClass197_1858;
	}

	public static final String method1546(int i, int i_0_, byte i_1_, byte[] is) {
		char[] cs = new char[i];
		int i_2_ = 0;
		for (int i_3_ = 0; (i_3_ ^ 0xffffffff) > (i ^ 0xffffffff); i_3_++) {
			int i_4_ = is[i_0_ + i_3_] & 0xff;
			if ((i_4_ ^ 0xffffffff) != -1) {
				if ((i_4_ ^ 0xffffffff) <= -129 && i_4_ < 160) {
					int i_5_ = Class65.unicodeUnescapes[-128 + i_4_];
					if (i_5_ == 0) {
						i_5_ = 63;
					}
					i_4_ = i_5_;
				}
				cs[i_2_++] = (char) i_4_;
			}
		}
		return new String(cs, 0, i_2_);
	}

	public static final void method1548(int i, int i_7_, int i_8_, int i_9_, byte i_10_, int i_11_, int i_12_) {
		WaterDetailPreferenceField.anInt3717 = i_12_;
		ParticlesPreferenceField.anInt3655 = i_9_;
		Class287_Sub2.anInt3274 = i;
		FileOnDisk.anInt3025 = i_8_;
		Class137.anInt1079 = i_11_;
		Class98_Sub42.anInt4239 = i_7_;
	}

	Class35	aClass35_5971;
	Class66	aClass66_5973;
	int		anInt5972;

	int		anInt5974;

	int		anInt5976;

	int		anInt5977;

	int		anInt5978;

	Class98_Sub46_Sub6(Class35 class35, Class246_Sub5 class246_sub5) {
		aClass35_5971 = class35;
		aClass66_5973 = aClass35_5971.method331((byte) 93);
		method1547(-102);
	}

	public final void method1547(int i) {
		anInt5974 = aClass35_5971.anInt330;
		anInt5978 = aClass35_5971.anInt337;
		anInt5972 = aClass35_5971.anInt331;
		if (aClass35_5971.aClass111_334 != null) {
			aClass35_5971.aClass111_334.method2099(aClass66_5973.anInt506, aClass66_5973.anInt511, aClass66_5973.anInt505, ConfigurableLoadingScreen.anIntArray3329);
		}
		anInt5977 = ConfigurableLoadingScreen.anIntArray3329[2];
		anInt5976 = ConfigurableLoadingScreen.anIntArray3329[0];
	}
}

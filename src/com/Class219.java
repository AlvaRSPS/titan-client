/* Class219 - Decompiled by JODE
 */ package com; /*
					*/

public final class Class219 {
	public static OutgoingOpcode	aClass171_1640	= new OutgoingOpcode(7, 6);

	public int						anInt1637		= 128;
	public int						anInt1638;
	public int						anInt1639;
	public int						anInt1643;
	public int						anInt1644;

	public int						anInt1645		= 128;

	public Class219(int i) {
		anInt1643 = i;
	}

	private Class219(int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		anInt1638 = i_3_;
		anInt1644 = i_4_;
		anInt1639 = i_5_;
		anInt1643 = i;
		anInt1645 = i_2_;
		anInt1637 = i_1_;
	}

	public final void method2814(Class219 class219_0_, int i) {
		anInt1638 = class219_0_.anInt1638;
		anInt1639 = class219_0_.anInt1639;
		anInt1643 = class219_0_.anInt1643;
		anInt1645 = class219_0_.anInt1645;
		anInt1644 = class219_0_.anInt1644;
		anInt1637 = class219_0_.anInt1637;
	}

	public final Class219 method2815(int i) {
		if (i != 128) {
			method2814(null, 107);
		}
		return new Class219(anInt1643, anInt1637, anInt1645, anInt1638, anInt1644, anInt1639);
	}
}

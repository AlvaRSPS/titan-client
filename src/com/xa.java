/* xa - Decompiled by JODE
 */ package com; /*
					*/

public final class xa implements DepthBufferObject, NativeDestructor {
	long nativeid = 0L;

	xa(int i, int i_1_) {
		r(i, i_1_);
	}

	@Override
	protected final void finalize() {
		if (nativeid != 0L) {
			NativeDestructorManager.add(false, this);
		}
	}

	private final native void r(int i, int i_0_);

	private final native void va(long l, boolean bool);

	@Override
	public final void w(boolean bool) {
		va(nativeid, bool);
	}
}

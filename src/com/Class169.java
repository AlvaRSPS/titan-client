/* Class169 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.quickchat.QuickChatMessageParser;

public final class Class169 {
	public static Class169	aClass169_1294	= new Class169(0, 3, Class162.aClass162_1270);
	public static Class169	aClass169_1297	= new Class169(1, 3, Class162.aClass162_1270);
	public static Class169	aClass169_1298	= new Class169(2, 4, Class162.aClass162_1266);
	public static Class169	aClass169_1299	= new Class169(3, 1, Class162.aClass162_1270);
	public static Class169	aClass169_1301	= new Class169(4, 2, Class162.aClass162_1270);
	public static Class169	aClass169_1302	= new Class169(5, 3, Class162.aClass162_1270);
	public static Class169	aClass169_1303	= new Class169(6, 4, Class162.aClass162_1270);
	public static int		anInt1304		= Class48_Sub2_Sub1.method474(16, (byte) 31);
	public static int		anInt1305		= 2;
	public static int		scriptPointer;
	public static int		anInt1307		= -1;

	static {
		scriptPointer = -1;
	}

	public static final void method2535(int i, int i_0_, int i_1_, int i_2_, byte i_3_) {
		try {
			if (i_3_ > 101) {
				for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub37.aClass148_5748.getFirst(32); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub37.aClass148_5748.getNext(107)) {
					QuickChatMessageParser.method3328(i_1_, 256, i_0_, i_2_, i, class98_sub42);
				}
				for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class358.aClass148_3032.getFirst(32); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class358.aClass148_3032.getNext(123)) {
					int i_4_ = 1;
					RenderAnimDefinition class294 = class98_sub42.npc.method3039(1);
					if ((class98_sub42.npc.anInt6385 ^ 0xffffffff) == 0 || class98_sub42.npc.aBoolean6359) {
						i_4_ = 0;
					} else if ((class294.anInt2389 ^ 0xffffffff) == (class98_sub42.npc.anInt6385 ^ 0xffffffff) || class98_sub42.npc.anInt6385 == class294.anInt2361 || class294.anInt2402 == class98_sub42.npc.anInt6385
							|| class98_sub42.npc.anInt6385 == class294.anInt2357) {
						i_4_ = 2;
					} else if (class294.anInt2368 == class98_sub42.npc.anInt6385 || class294.anInt2394 == class98_sub42.npc.anInt6385 || class98_sub42.npc.anInt6385 == class294.anInt2403 || (class294.anInt2377 ^ 0xffffffff) == (class98_sub42.npc.anInt6385
							^ 0xffffffff)) {
						i_4_ = 3;
					}
					if ((class98_sub42.anInt4227 ^ 0xffffffff) != (i_4_ ^ 0xffffffff)) {
						int i_5_ = Class277.method3293(120, class98_sub42.npc);
						NPCDefinition class141 = class98_sub42.npc.definition;
						if (class141.anIntArray1109 != null) {
							class141 = class141.method2300(StartupStage.varValues, (byte) 29);
						}
						if (class141 == null || (i_5_ ^ 0xffffffff) == 0) {
							class98_sub42.anInt4227 = i_4_;
							class98_sub42.anInt4210 = -1;
							class98_sub42.aBoolean4215 = false;
						} else if ((class98_sub42.anInt4210 ^ 0xffffffff) != (i_5_ ^ 0xffffffff) || !class98_sub42.aBoolean4215 == class141.aBoolean1093) {
							boolean bool = false;
							if (class98_sub42.aClass98_Sub31_Sub5_4232 != null) {
								class98_sub42.anInt4236 -= 512;
								if ((class98_sub42.anInt4236 ^ 0xffffffff) >= -1) {
									Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4232);
									class98_sub42.aClass98_Sub31_Sub5_4232 = null;
									bool = true;
								}
							} else {
								bool = true;
							}
							if (bool) {
								class98_sub42.aClass98_Sub24_Sub1_4214 = null;
								class98_sub42.anInt4236 = class141.anInt1156;
								class98_sub42.aBoolean4215 = class141.aBoolean1093;
								class98_sub42.anInt4227 = i_4_;
								class98_sub42.aClass98_Sub13_4213 = null;
								class98_sub42.anInt4210 = i_5_;
							}
						} else {
							class98_sub42.anInt4236 = class141.anInt1156;
							class98_sub42.anInt4227 = i_4_;
						}
					}
					class98_sub42.anInt4229 = class98_sub42.npc.boundExtentsX;
					class98_sub42.anInt4224 = class98_sub42.npc.boundExtentsX + (class98_sub42.npc.getSize(0) << 1021778312);
					class98_sub42.anInt4225 = class98_sub42.npc.boundExtentsZ;
					class98_sub42.anInt4216 = class98_sub42.npc.boundExtentsZ + (class98_sub42.npc.getSize(0) << -1760510648);
					QuickChatMessageParser.method3328(i_1_, 256, i_0_, i_2_, i, class98_sub42);
				}
				for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub14.aClass377_5612.startIteration(107); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub14.aClass377_5612.iterateNext(-1)) {
					int i_6_ = 1;
					RenderAnimDefinition class294 = class98_sub42.player.method3039(1);
					if (class98_sub42.player.anInt6385 == -1 || class98_sub42.player.aBoolean6359) {
						i_6_ = 0;
					} else if (class294.anInt2389 != class98_sub42.player.anInt6385 && (class294.anInt2361 ^ 0xffffffff) != (class98_sub42.player.anInt6385 ^ 0xffffffff) && class98_sub42.player.anInt6385 != class294.anInt2402
							&& class98_sub42.player.anInt6385 != class294.anInt2357) {
						if (class98_sub42.player.anInt6385 == class294.anInt2368 || class98_sub42.player.anInt6385 == class294.anInt2394 || (class294.anInt2403 ^ 0xffffffff) == (class98_sub42.player.anInt6385 ^ 0xffffffff)
								|| class294.anInt2377 == class98_sub42.player.anInt6385) {
							i_6_ = 3;
						}
					} else {
						i_6_ = 2;
					}
					if (i_6_ != class98_sub42.anInt4227) {
						int i_7_ = VertexNormal.method3383(class98_sub42.player, true);
						if ((i_7_ ^ 0xffffffff) == (class98_sub42.anInt4210 ^ 0xffffffff) && !class98_sub42.aBoolean4215 == !class98_sub42.player.hasDisplayName) {
							class98_sub42.anInt4227 = i_6_;
							class98_sub42.anInt4236 = class98_sub42.player.anInt6514;
						} else {
							boolean bool = false;
							if (class98_sub42.aClass98_Sub31_Sub5_4232 == null) {
								bool = true;
							} else {
								class98_sub42.anInt4236 -= 512;
								if ((class98_sub42.anInt4236 ^ 0xffffffff) >= -1) {
									Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4232);
									class98_sub42.aClass98_Sub31_Sub5_4232 = null;
									bool = true;
								}
							}
							if (bool) {
								class98_sub42.anInt4210 = i_7_;
								class98_sub42.aClass98_Sub24_Sub1_4214 = null;
								class98_sub42.aClass98_Sub13_4213 = null;
								class98_sub42.anInt4236 = class98_sub42.player.anInt6514;
								class98_sub42.anInt4227 = i_6_;
								class98_sub42.aBoolean4215 = class98_sub42.player.hasDisplayName;
							}
						}
					}
					class98_sub42.anInt4229 = class98_sub42.player.boundExtentsX;
					class98_sub42.anInt4224 = class98_sub42.player.boundExtentsX - -(class98_sub42.player.getSize(0) << 1257463912);
					class98_sub42.anInt4225 = class98_sub42.player.boundExtentsZ;
					class98_sub42.anInt4216 = class98_sub42.player.boundExtentsZ + (class98_sub42.player.getSize(0) << -1206921304);
					QuickChatMessageParser.method3328(i_1_, 256, i_0_, i_2_, i, class98_sub42);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lf.B(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public static void method2536(int i) {
		try {
			aClass169_1294 = null;
			aClass169_1298 = null;
			aClass169_1302 = null;
			aClass169_1299 = null;
			if (i > -111) {
				aClass169_1297 = null;
			}
			aClass169_1301 = null;
			aClass169_1303 = null;
			aClass169_1297 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lf.C(" + i + ')');
		}
	}

	public static final Class169 method2537(int i, byte i_8_) {
		try {
			int i_9_ = i;
			while_110_: do {
				while_109_: do {
					while_108_: do {
						while_107_: do {
							while_106_: do {
								while_105_: do {
									do {
										if ((i_9_ ^ 0xffffffff) != -1) {
											if (i_9_ == 1) {
												break;
											}
											if ((i_9_ ^ 0xffffffff) == -3) {
												break while_105_;
											}
											if (i_9_ == 3) {
												break while_106_;
											}
											if ((i_9_ ^ 0xffffffff) == -5) {
												break while_107_;
											}
											if (i_9_ == 5) {
												break while_108_;
											}
											if ((i_9_ ^ 0xffffffff) != -7) {
												break while_110_;
											}
											if (!GameShell.cleanedStatics) {
												break while_109_;
											}
										}
										return aClass169_1294;
									} while (false);
									return aClass169_1297;
								} while (false);
								return aClass169_1298;
							} while (false);
							return aClass169_1299;
						} while (false);
						return aClass169_1301;
					} while (false);
					return aClass169_1302;
				} while (false);
				return aClass169_1303;
			} while (false);
			if (i_8_ < 5) {
				aClass169_1298 = null;
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lf.A(" + i + ',' + i_8_ + ')');
		}
	}

	private Class162	aClass162_1293;

	int					anInt1295;

	private int			anInt1296;

	int					anInt1300;

	private Class169(int i, int i_10_, Class162 class162) {
		try {
			aClass162_1293 = class162;
			anInt1296 = i_10_;
			anInt1300 = i;
			anInt1295 = aClass162_1293.anInt1263 * anInt1296;
			if (anInt1300 >= 16) {
				throw new RuntimeException();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lf.<init>(" + i + ',' + i_10_ + ',' + (class162 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final String toString() {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "lf.toString(" + ')');
		}
	}
}

/* Class98_Sub22_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.crypto.ISAACPseudoRNG;

public final class RsBitsBuffers extends RSByteBuffer {
	public static int	anInt5789;

	public static int[]	BIT_MASKS	= { 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535, 131071, 262143, 524287, 1048575, 2097151, 4194303, 8388607, 16777215, 33554431, 67108863, 134217727, 268435455, 536870911, 1073741823, 2147483647, -1 };

	public static final int method1253(boolean bool, int i) {
		if (i != -31553) {
			anInt5789 = -49;
		}
		int i_2_ = Cacheable.anInt4261;
		while_28_: do {
			while_27_: do {
				do {
					if ((i_2_ ^ 0xffffffff) != -1) {
						if ((i_2_ ^ 0xffffffff) == -2) {
							break;
						}
						if ((i_2_ ^ 0xffffffff) != -3) {
							break while_28_;
						}
						if (!GameShell.cleanedStatics) {
							break while_27_;
						}
					}
					if (!bool) {
						return CacheFileRequest.anInt6309;
					}
					return 0;
				} while (false);
				return CacheFileRequest.anInt6309;
			} while (false);
			return 0;
		} while (false);
		return 0;
	}

	private int				bitPosition;

	private ISAACPseudoRNG	cypher;

	RsBitsBuffers(int size) {
		super(size);
	}

	public final int bitDifference(int i, int i_8_) {
		return 8 * i - bitPosition;
	}

	public final void endBitwiseAccess(byte i) {
		this.position = (bitPosition - -7) / 8;
	}

	public final void initializeCypher(byte i, ISAACPseudoRNG cyph) {
		cypher = cyph;
	}

	public final void initializeCypher(int[] seed, int i) {
		if (i == 255) {
			cypher = new ISAACPseudoRNG(seed);
		}
	}

	public final void method1261(boolean bool, int i) {
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) (i + cypher.next(84));
	}

	public final int readBits(byte i, int n) {
		int bit_pos = bitPosition >> 212764035;
		int sz = 8 + -(bitPosition & 0x7);
		bitPosition += n;
		int acc = 0;
		for (/**/; (sz ^ 0xffffffff) > (n ^ 0xffffffff); sz = 8) {
			acc += (this.payload[bit_pos++] & BIT_MASKS[sz]) << n - sz;
			n -= sz;
		}
		if (n == sz) {
			acc += BIT_MASKS[sz] & this.payload[bit_pos];
		} else {
			acc += this.payload[bit_pos] >> sz - n & BIT_MASKS[n];
		}
		return acc;
	}

	public final void startBitwiseAccess(int i) {
		bitPosition = 8 * this.position;
	}

	public final void xgData(int offset, int length, byte[] buffer, boolean bool) {
		for (int p = 0; (p ^ 0xffffffff) > (length ^ 0xffffffff); p++) {
			buffer[p - -offset] = (byte) (this.payload[this.position++] + -cypher.next(123));
		}
	}

	public final int xgSmart(int i) {
		int index = this.payload[this.position++] - cypher.next(121) & 0xff;
		if ((index ^ 0xffffffff) > -129) {
			return index;
		}
		return (index - 128 << 1485705704) - -(0xff & this.payload[this.position++] - cypher.next(i ^ 0x6a));
	}

	public final boolean xzSmart(byte i) {
		int length = 0xff & this.payload[this.position] + -cypher.peek(i + -51);
		return length >= 128;
	}
}

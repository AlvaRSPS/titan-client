/* Class331 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.crypto.ISAACPseudoRNG;

public final class Class331 {
	public static Class105			aClass105_2792		= new Class105("", 17);
	public static ISAACPseudoRNG	aClass117_2811;
	public static ActionQueueEntry	aClass98_Sub46_Sub8_2803;
	public static int				anInt2800;
	public static int[]				anIntArray2810;

	boolean[]						aBooleanArray2780;
	boolean[]						aBooleanArray2782;
	byte							aByte2787;
	byte[]							aByteArray2775;
	byte[]							aByteArray2776;
	byte[]							aByteArray2783;
	byte[]							aByteArray2795;
	byte[]							aByteArray2796;
	byte[]							aByteArray2799;
	byte[][]						aByteArrayArray2804;
	int								anInt2777;
	int								anInt2778;
	int								anInt2779;
	int								anInt2785;
	int								anInt2786;
	int								anInt2788;
	int								anInt2789;
	int								anInt2793;
	int								anInt2797;
	int								anInt2798;
	int								anInt2801;
	int								anInt2802;
	int								anInt2805;
	int								anInt2808;
	int								anInt2809;
	int[]							anIntArray2781;
	int[]							anIntArray2791;
	int[]							anIntArray2794;
	int[]							anIntArray2807;
	int[][]							anIntArrayArray2784	= new int[6][258];

	int[][]							anIntArrayArray2790;

	int[][]							anIntArrayArray2806;

	Class331() {
		anIntArray2781 = new int[16];
		aBooleanArray2780 = new boolean[16];
		aByteArray2795 = new byte[256];
		anIntArray2791 = new int[6];
		aByteArray2776 = new byte[18002];
		aByteArray2796 = new byte[4096];
		aByteArray2783 = new byte[18002];
		anIntArrayArray2790 = new int[6][258];
		aBooleanArray2782 = new boolean[256];
		anIntArray2794 = new int[256];
		anIntArray2807 = new int[257];
		anInt2808 = 0;
		anInt2801 = 0;
		anIntArrayArray2806 = new int[6][258];
		aByteArrayArray2804 = new byte[6][258];
	}
}

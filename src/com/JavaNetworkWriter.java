
/* Class307 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.parser.*;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;
import com.jagex.game.client.preferences.ToolkitPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.NewsLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.input.RtKeyListener;
import com.jagex.game.input.impl.AwtMouseListener;

import java.io.IOException;
import java.io.OutputStream;

public final class JavaNetworkWriter implements Runnable {
	public static boolean aBoolean2575 = false;

	public static final void method3604(int i, byte i_1_, int i_2_, ActionQueueEntry actionEntry) {
		do {
			if (actionEntry != null && Class33.actionList.top != actionEntry) {
				int i_3_ = actionEntry.anInt5995;
				int i_4_ = actionEntry.anInt5993;
				int i_5_ = actionEntry.actionId;
				int index = (int) actionEntry.aLong5987;
				if ((i_5_ ^ 0xffffffff) <= -2001) {
					i_5_ -= 2000;
				}
				long l = actionEntry.aLong5987;
				if (i_5_ == 1002) {
					OpenGLRenderEffectManager.anInt440 = 2;
					Class98_Sub49.anInt4286 = i;
					Class98_Sub10_Sub32.anInt5720 = 0;
					BConfigDefinition.anInt3117 = i_2_;
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(index, -1);
					if (class98_sub39 != null) {
						NPC npc = class98_sub39.npc;
						NPCDefinition definition = npc.definition;
						if (definition.anIntArray1109 != null) {
							definition = definition.method2300(StartupStage.varValues, (byte) 45);
						}
						if (definition != null) {
							OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, NPC.aClass171_6506, Class331.aClass117_2811);
							frame.packet.writeShort(definition.npcId, 1571862888);
							Class98_Sub10_Sub29.sendPacket(false, frame);
						}
					}
				}
				if (i_5_ == 51) {
					Player player = Class151_Sub9.players[index];
					if (player != null) {
						Class98_Sub10_Sub32.anInt5720 = 0;
						OpenGLRenderEffectManager.anInt440 = 2;
						Class98_Sub49.anInt4286 = i;
						Class98_Sub23.anInt4001++;
						BConfigDefinition.anInt3117 = i_2_;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ ^ 0x14a, HitmarksDefinitionParser.aClass171_1001, Class331.aClass117_2811);
						class98_sub11.packet.writeLEShortA(index, 128);
						class98_sub11.packet.method1231(client.keyListener.isKeyDown(82, 5503) ? 1 : 0, (byte) 107);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, 0, player.pathZ[0], player.pathX[0], true, player.getSize(0));
					}
				}
				if (i_5_ == 45) {
					Player player = Class151_Sub9.players[index];
					if (player != null) {
						BConfigDefinition.anInt3117 = i_2_;
						Class98_Sub10_Sub32.anInt5720 = 0;
						BConfigDefinition.anInt3111++;
						OpenGLRenderEffectManager.anInt440 = 2;
						Class98_Sub49.anInt4286 = i;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class277.aClass171_2051, Class331.aClass117_2811);
						class98_sub11.packet.writeByteS(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, -102);
						class98_sub11.packet.writeShort(index, 1571862888);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, i_1_ + -78, player.pathZ[0], player.pathX[0], true, player.getSize(0));
					}
				}
				if ((i_5_ ^ 0xffffffff) == -59) {
					Class98_Sub49.anInt4286 = i;
					BConfigDefinition.anInt3117 = i_2_;
					OpenGLRenderEffectManager.anInt440 = 2;
					Class98_Sub10_Sub32.anInt5720 = 0;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Exception_Sub1.aClass171_46, Class331.aClass117_2811);
					class98_sub11.packet.method1200((byte) 127, Class187.anInt1450);
					class98_sub11.packet.writeLEShortA(aa_Sub2.gameSceneBaseY + i_4_, 128);
					class98_sub11.packet.writeLEShortA(index, 128);
					class98_sub11.packet.writeByte(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, 66);
					class98_sub11.packet.writeLEShortA(GlobalPlayer.anInt3173, 128);
					class98_sub11.packet.writeLEShort(Class272.gameSceneBaseX + i_3_, 17624);
					class98_sub11.packet.writeLEShortA(Class310.anInt2652, 128);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class44.method427(-19181, i_4_, i_3_);
				}
				if (i_5_ == 1011 || i_5_ == 1003 || i_5_ == 1001 || (i_5_ ^ 0xffffffff) == -1011 || i_5_ == 1004) {
					Class162.method2518(-1004, i_5_, i_3_, index);
				}
				if (i_5_ == 2) {
					OpenGLRenderEffectManager.anInt440 = 2;
					Class98_Sub49.anInt4286 = i;
					BConfigDefinition.anInt3117 = i_2_;
					Class98_Sub10_Sub32.anInt5720 = 0;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ + 182, Class284_Sub2_Sub2.aClass171_6198, Class331.aClass117_2811);
					class98_sub11.packet.writeLEShort(Class272.gameSceneBaseX + i_3_, 17624);
					class98_sub11.packet.writeLEShort(index, 17624);
					class98_sub11.packet.writeShortA(i_4_ + aa_Sub2.gameSceneBaseY, (byte) 126);
					class98_sub11.packet.method1244(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, (byte) 112);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class44.method427(-19181, i_4_, i_3_);
				}
				if ((i_5_ ^ 0xffffffff) == -51) {
					Class98_Sub10_Sub32.anInt5720 = 0;
					BConfigDefinition.anInt3117 = i_2_;
					Class98_Sub49.anInt4286 = i;
					OpenGLRenderEffectManager.anInt440 = 2;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, SpriteLoadingScreenElement.aClass171_3466, Class331.aClass117_2811);
					class98_sub11.packet.writeLEInt(Class187.anInt1450, 1046032984);
					class98_sub11.packet.writeLEShortA(Class310.anInt2652, 128);
					class98_sub11.packet.writeShort(GlobalPlayer.anInt3173, 1571862888);
					class98_sub11.packet.writeShort(Class272.gameSceneBaseX + i_3_, i_1_ ^ 0x5db0b926);
					class98_sub11.packet.writeByteS(client.keyListener.isKeyDown(82, 5503) ? 1 : 0, i_1_ + -112);
					class98_sub11.packet.writeShortA((int) (l >>> 1575185440) & 0x7fffffff, (byte) 126);
					class98_sub11.packet.writeLEShortA(aa_Sub2.gameSceneBaseY + i_4_, i_1_ + 50);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class67.method688(-23, l, i_3_, i_4_);
				}
				if (i_5_ == 11) {
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(index, -1);
					if (class98_sub39 != null) {
						Class98_Sub49.anInt4286 = i;
						NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
						Class98_Sub10_Sub32.anInt5720 = 0;
						BConfigDefinition.anInt3117 = i_2_;
						OpenGLRenderEffectManager.anInt440 = 2;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ + 182, ItemDeque.aClass171_4256, Class331.aClass117_2811);
						class98_sub11.packet.writeShort(index, 1571862888);
						class98_sub11.packet.method1231(client.keyListener.isKeyDown(82, 5503) ? 1 : 0, (byte) 35);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, class246_sub3_sub4_sub2_sub1.getSize(0), -2, 0, class246_sub3_sub4_sub2_sub1.pathZ[0], class246_sub3_sub4_sub2_sub1.pathX[0], true, class246_sub3_sub4_sub2_sub1.getSize(0));
					}
				}
				if ((i_5_ ^ 0xffffffff) == -21) {
					Player player = Class151_Sub9.players[index];
					if (player != null) {
						BConfigDefinition.anInt3117 = i_2_;
						Class98_Sub10_Sub32.anInt5720 = 0;
						OpenGLRenderEffectManager.anInt440 = 2;
						Class98_Sub49.anInt4286 = i;
						AnimationSkeletonSet.anInt6044++;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ + 182, ToolkitPreferenceField.aClass171_3661, Class331.aClass117_2811);
						class98_sub11.packet.writeLEShort(index, 17624);
						class98_sub11.packet.writeByteS(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, -37);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, i_1_ + -78, player.pathZ[0], player.pathX[0], true, player.getSize(i_1_ ^ 0x4e));
					}
				}
				if (i_5_ == 5) {
					Player player = Class151_Sub9.players[index];
					if (player != null) {
						OpenGLRenderEffectManager.anInt440 = 2;
						client.anInt3548++;
						BConfigDefinition.anInt3117 = i_2_;
						Class98_Sub10_Sub32.anInt5720 = 0;
						Class98_Sub49.anInt4286 = i;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class160.aClass171_1259, Class331.aClass117_2811);
						class98_sub11.packet.writeLEShortA(index, 128);
						class98_sub11.packet.writeShortA(Class310.anInt2652, (byte) 126);
						class98_sub11.packet.writeLEInt(Class187.anInt1450, 1046032984);
						class98_sub11.packet.method1231(client.keyListener.isKeyDown(82, 5503) ? 1 : 0, (byte) -128);
						class98_sub11.packet.writeShortA(GlobalPlayer.anInt3173, (byte) 126);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, 0, player.pathZ[0], player.pathX[0], true, player.getSize(i_1_ ^ 0x4e));
					}
				}
				if (i_5_ == 19) {
					Player player = Class151_Sub9.players[index];
					if (player != null) {
						Class98_Sub49.anInt4286 = i;
						Class98_Sub10_Sub32.anInt5720 = 0;
						OpenGLRenderEffectManager.anInt440 = 2;
						BConfigDefinition.anInt3117 = i_2_;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class21_Sub4.aClass171_5398, Class331.aClass117_2811);
						class98_sub11.packet.writeShortA(index, (byte) 126);
						class98_sub11.packet.method1244(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, (byte) 112);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, 0, player.pathZ[0], player.pathX[0], true, player.getSize(i_1_ + -78));
					}
				}
				if ((i_5_ ^ 0xffffffff) == -31) {
					Class98_Sub49.anInt4286 = i;
					OpenGLRenderEffectManager.anInt440 = 2;
					Class98_Sub10_Sub32.anInt5720 = 0;
					BConfigDefinition.anInt3117 = i_2_;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ ^ 0x14a, Class49.aClass171_413, Class331.aClass117_2811);
					class98_sub11.packet.writeLEShort(i_4_ + aa_Sub2.gameSceneBaseY, 17624);
					class98_sub11.packet.writeLEShortA(index, 128);
					class98_sub11.packet.writeLEShort(Class272.gameSceneBaseX + i_3_, i_1_ ^ 0x4496);
					class98_sub11.packet.method1231(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, (byte) 126);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class44.method427(i_1_ + -19259, i_4_, i_3_);
				}
				if (i_5_ == 47) {
					Player player = Class151_Sub9.players[index];
					if (player != null) {
						BConfigDefinition.anInt3117 = i_2_;
						Class65.anInt498++;
						Class98_Sub49.anInt4286 = i;
						OpenGLRenderEffectManager.anInt440 = 2;
						Class98_Sub10_Sub32.anInt5720 = 0;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, GameObjectDefinitionParser.aClass171_2520, Class331.aClass117_2811);
						class98_sub11.packet.writeByteS(client.keyListener.isKeyDown(82, i_1_ + 5425) ? 1 : 0, -52);
						class98_sub11.packet.writeShort(index, 1571862888);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, 0, player.pathZ[0], player.pathX[0], true, player.getSize(0));
					}
				}
				if ((i_5_ ^ 0xffffffff) == -17) {
					BConfigDefinition.anInt3117 = i_2_;
					Class98_Sub49.anInt4286 = i;
					Class98_Sub10_Sub32.anInt5720 = 0;
					OpenGLRenderEffectManager.anInt440 = 2;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ + 182, BuildLocation.aClass171_1508, Class331.aClass117_2811);
					class98_sub11.packet.writeLEShort(i_3_ - -Class272.gameSceneBaseX, 17624);
					class98_sub11.packet.writeLEShort(i_4_ + aa_Sub2.gameSceneBaseY, i_1_ + 17546);
					class98_sub11.packet.writeShortA((int) (l >>> -855876128) & 0x7fffffff, (byte) 126);
					class98_sub11.packet.method1231(client.keyListener.isKeyDown(82, 5503) ? 1 : 0, (byte) 74);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class67.method688(-23, l, i_3_, i_4_);
				}
				if ((i_5_ ^ 0xffffffff) == -4) {
					Player player = Class151_Sub9.players[index];
					if (player != null) {
						Class98_Sub49.anInt4286 = i;
						OpenGLRenderEffectManager.anInt440 = 2;
						BConfigDefinition.anInt3117 = i_2_;
						Class98_Sub10_Sub32.anInt5720 = 0;
						Class98_Sub43.anInt4242++;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, DummyOutputStream.aClass171_34, Class331.aClass117_2811);
						class98_sub11.packet.method1244(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, (byte) 112);
						class98_sub11.packet.writeShort(index, 1571862888);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, i_1_ + -78, player.pathZ[0], player.pathX[0], true, player.getSize(0));
					}
				}
				if (i_5_ == 8) {
					OpenGLRenderEffectManager.anInt440 = 2;
					Class98_Sub49.anInt4286 = i;
					BConfigDefinition.anInt3117 = i_2_;
					Class98_Sub10_Sub32.anInt5720 = 0;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, GZipDecompressor.aClass171_1964, Class331.aClass117_2811);
					class98_sub11.packet.writeShortA(i_3_ + Class272.gameSceneBaseX, (byte) 126);
					class98_sub11.packet.writeLEShortA(0x7fffffff & (int) (l >>> -769820512), 128);
					class98_sub11.packet.method1231(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, (byte) -124);
					class98_sub11.packet.writeLEShortA(aa_Sub2.gameSceneBaseY + i_4_, 128);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class67.method688(-23, l, i_3_, i_4_);
				}
				if (i_5_ == 17) {
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(index, -1);
					if (class98_sub39 != null) {
						NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
						BConfigDefinition.anInt3117 = i_2_;
						OpenGLRenderEffectManager.anInt440 = 2;
						Class98_Sub10_Sub32.anInt5720 = 0;
						Class98_Sub49.anInt4286 = i;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, AwtMouseListener.aClass171_5296, Class331.aClass117_2811);
						class98_sub11.packet.writeShort(index, 1571862888);
						class98_sub11.packet.method1231(!client.keyListener.isKeyDown(82, i_1_ ^ 0x1531) ? 0 : 1, (byte) 37);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, class246_sub3_sub4_sub2_sub1.getSize(0), -2, 0, class246_sub3_sub4_sub2_sub1.pathZ[0], class246_sub3_sub4_sub2_sub1.pathX[0], true, class246_sub3_sub4_sub2_sub1.getSize(0));
					}
				}
				if ((i_5_ ^ 0xffffffff) == -10 && DummyOutputStream.rtInterface == null) {
					ParamDefinition.method2435(i_3_, i_1_ ^ 0x1f, i_4_);
					DummyOutputStream.rtInterface = Class246_Sub9.getDynamicComponent((byte) 72, i_4_, i_3_);
					WorldMapInfoDefinitionParser.setDirty(1, DummyOutputStream.rtInterface);
				}
				if (i_5_ == 59) {
					RtInterface class293 = Class246_Sub9.getDynamicComponent((byte) 72, i_4_, i_3_);
					if (class293 != null) {
						Class172.method2542(false, class293);
					}
				}
				if (i_5_ == 13) {
					BConfigDefinition.anInt3117 = i_2_;
					Class98_Sub49.anInt4286 = i;
					Class98_Sub10_Sub32.anInt5720 = 0;
					OpenGLRenderEffectManager.anInt440 = 2;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, LoginOpcode.aClass171_1669, Class331.aClass117_2811);
					class98_sub11.packet.method1231(client.keyListener.isKeyDown(82, i_1_ ^ 0x1531) ? 1 : 0, (byte) 32);
					class98_sub11.packet.writeShortA(i_3_ - -Class272.gameSceneBaseX, (byte) 126);
					class98_sub11.packet.writeLEShort(index, 17624);
					class98_sub11.packet.writeLEShortA(i_4_ - -aa_Sub2.gameSceneBaseY, 128);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class44.method427(-19181, i_4_, i_3_);
				}
				if ((i_5_ ^ 0xffffffff) == -50 || i_5_ == 1006) {
					Class303.handleKeysPressed(index, i_3_, actionEntry.targetText, -124, i_4_);
				}
				if (i_5_ == 48) {
					NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(index, i_1_ + -79);
					if (class98_sub39 != null) {
						BConfigDefinition.anInt3117 = i_2_;
						OpenGLRenderEffectManager.anInt440 = 2;
						NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
						Class98_Sub49.anInt4286 = i;
						Class98_Sub10_Sub32.anInt5720 = 0;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class82.aClass171_625, Class331.aClass117_2811);
						class98_sub11.packet.method1200((byte) 125, Class187.anInt1450);
						class98_sub11.packet.writeLEShort(Class310.anInt2652, i_1_ + 17546);
						class98_sub11.packet.writeShort(index, i_1_ ^ 0x5db0b926);
						class98_sub11.packet.writeByteS(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, -53);
						class98_sub11.packet.writeLEShort(GlobalPlayer.anInt3173, i_1_ + 17546);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, class246_sub3_sub4_sub2_sub1.getSize(0), -2, i_1_ ^ 0x4e, class246_sub3_sub4_sub2_sub1.pathZ[0], class246_sub3_sub4_sub2_sub1.pathX[0], true, class246_sub3_sub4_sub2_sub1.getSize(0));
					}
				}
				if ((i_5_ ^ 0xffffffff) == -1010) {
					Class98_Sub49.anInt4286 = i;
					BConfigDefinition.anInt3117 = i_2_;
					OpenGLRenderEffectManager.anInt440 = 2;
					Class98_Sub10_Sub32.anInt5720 = 0;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, aa_Sub1.aClass171_3559, Class331.aClass117_2811);
					class98_sub11.packet.writeShort(index, i_1_ + 1571862810);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
				}
				if (i_1_ != 78) {
					method3604(-42, (byte) 33, 80, null);
				}
				if ((i_5_ ^ 0xffffffff) == -58) {
					client.anInt3548++;
					OpenGLRenderEffectManager.anInt440 = 2;
					BConfigDefinition.anInt3117 = i_2_;
					Class98_Sub49.anInt4286 = i;
					Class98_Sub10_Sub32.anInt5720 = 0;
					OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ ^ 0x14a, Class160.aClass171_1259, Class331.aClass117_2811);
					frame.packet.writeLEShortA(Class87.localPlayer.index, i_1_ ^ 0xce);
					frame.packet.writeShortA(Class310.anInt2652, (byte) 126);
					frame.packet.writeLEInt(Class187.anInt1450, 1046032984);
					frame.packet.method1231(!client.keyListener.isKeyDown(82, i_1_ ^ 0x1531) ? 0 : 1, (byte) -72);
					frame.packet.writeShortA(GlobalPlayer.anInt3173, (byte) 126);
					Class98_Sub10_Sub29.sendPacket(false, frame);
				}
				if (i_5_ == 15) {
					BConfigDefinition.anInt3117 = i_2_;
					Class98_Sub10_Sub32.anInt5720 = 0;
					OpenGLRenderEffectManager.anInt440 = 2;
					Class98_Sub49.anInt4286 = i;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class250.aClass171_1913, Class331.aClass117_2811);
					class98_sub11.packet.writeLEShort(0x7fffffff & (int) (l >>> 933731040), 17624);
					class98_sub11.packet.writeByte(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, -108);
					class98_sub11.packet.writeLEShortA(i_3_ + Class272.gameSceneBaseX, i_1_ ^ 0xce);
					class98_sub11.packet.writeShortA(i_4_ + aa_Sub2.gameSceneBaseY, (byte) 126);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class67.method688(i_1_ + -101, l, i_3_, i_4_);
				}
				if ((i_5_ ^ 0xffffffff) == -7) {
					if ((LoadingScreenSequence.rights ^ 0xffffffff) >= -1 || !client.keyListener.isKeyDown(82, i_1_ ^ 0x1531) || !client.keyListener.isKeyDown(81, i_1_ + 5425)) {
						OutgoingPacket class98_sub11 = Class50.method486(0, 0, 1, -4, i_1_ ^ 0x4e, i_4_, i_3_, true, 1);// path
						if ((index ^ 0xffffffff) != -2) {
							OpenGLRenderEffectManager.anInt440 = 1;
							Class98_Sub10_Sub32.anInt5720 = 0;
							Class98_Sub49.anInt4286 = i;
							BConfigDefinition.anInt3117 = i_2_;
						} else {
							/*
							 * ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865 .method1194(-1, -39);
							 * ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865 .method1194(-1, -90);
							 * ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865.writeShort ((int)
							 * Class98_Sub22_Sub2.aFloat5794, 1571862888);
							 * ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865 .method1194(57, -116);
							 * ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865
							 * .method1194(Class204.anInt1553, 116);
							 * ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865
							 * .method1194(Class151.anInt1213, -95);
							 * ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865 .method1194(89, 68);
							 * ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865.writeShort
							 * ((((Class246_Sub3) (Class87
							 * .aClass246_Sub3_Sub4_Sub2_Sub2_660)) .anInt5084),
							 * 1571862888); ((Class98_Sub11) class98_sub11)
							 * .aClass98_Sub22_Sub1_3865.writeShort
							 * ((((Class246_Sub3) (Class87
							 * .aClass246_Sub3_Sub4_Sub2_Sub2_660)) .anInt5079),
							 * i_1_ + 1571862810); ((Class98_Sub11)
							 * class98_sub11) .aClass98_Sub22_Sub1_3865
							 * .method1194(63, -38);
							 */
						}
						Class76_Sub2.requestFlag(0, 0, 1, -4, i_1_ ^ 0x4e, i_4_, i_3_, true, 1);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					} else {
						Class351.handleClickTeleport(aa_Sub2.gameSceneBaseY - -i_4_, Class272.gameSceneBaseX - -i_3_, Class87.localPlayer.plane, 32);
					}
				}
				if ((i_5_ ^ 0xffffffff) == -5) {
					OpenGLRenderEffectManager.anInt440 = 2;
					Class98_Sub10_Sub32.anInt5720 = 0;
					BConfigDefinition.anInt3117 = i_2_;
					Class98_Sub49.anInt4286 = i;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class283.aClass171_2146, Class331.aClass117_2811);
					System.out.println("Reachedddddddddd10");
					class98_sub11.packet.writeShort(aa_Sub2.gameSceneBaseY + i_4_, i_1_ ^ 0x5db0b926);
					class98_sub11.packet.writeLEShort((int) (l >>> 2020731040) & 0x7fffffff, 17624);
					class98_sub11.packet.writeShort(Class272.gameSceneBaseX + i_3_, 1571862888);
					class98_sub11.packet.writeByteS(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, i_1_ ^ ~0x6e);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class67.method688(-23, l, i_3_, i_4_);
				}
				if ((i_5_ ^ 0xffffffff) == -61) {
					if (LoadingScreenSequence.rights > 0 && client.keyListener.isKeyDown(82, 5503) && client.keyListener.isKeyDown(81, i_1_ + 5425)) {
						Class351.handleClickTeleport(i_4_ + aa_Sub2.gameSceneBaseY, i_3_ + Class272.gameSceneBaseX, Class87.localPlayer.plane, -67);
					} else {
						Class98_Sub49.anInt4286 = i;
						Class98_Sub10_Sub32.anInt5720 = 0;
						OpenGLRenderEffectManager.anInt440 = 1;
						BConfigDefinition.anInt3117 = i_2_;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, MapScenesDefinitionParser.aClass171_2812, Class331.aClass117_2811);
						class98_sub11.packet.writeShortA(i_4_ + aa_Sub2.gameSceneBaseY, (byte) 126);
						class98_sub11.packet.writeShortA(Class272.gameSceneBaseX + i_3_, (byte) 126);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					}
				}
				if ((i_5_ ^ 0xffffffff) == -1008) {
					Class98_Sub10_Sub32.anInt5720 = 0;
					Class98_Sub49.anInt4286 = i;
					OpenGLRenderEffectManager.anInt440 = 2;
					BConfigDefinition.anInt3117 = i_2_;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class255.aClass171_3206, Class331.aClass117_2811);
					System.out.println("Reachedddddddddd9");
					class98_sub11.packet.writeShortA(i_3_ + Class272.gameSceneBaseX, (byte) 126);
					class98_sub11.packet.writeLEShortA(0x7fffffff & (int) (l >>> -1233322400), i_1_ ^ 0xce);
					class98_sub11.packet.method1231(client.keyListener.isKeyDown(82, 5503) ? 1 : 0, (byte) -120);
					class98_sub11.packet.writeLEShortA(aa_Sub2.gameSceneBaseY + i_4_, 128);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					Class67.method688(i_1_ + -101, l, i_3_, i_4_);
				}
				if ((i_5_ ^ 0xffffffff) == -22) {
					RtInterface class293 = Class246_Sub9.getDynamicComponent((byte) 72, i_4_, i_3_);
					if (class293 != null) {
						Class98_Sub10_Sub32.method1098((byte) 96);
						Class98_Sub49 class98_sub49 = client.method116(class293);
						Class98_Sub5_Sub2.method970(class98_sub49.anInt4285, class293, class98_sub49.method1668(-1), -6838);
						Class287_Sub2.aString3272 = Class170.method2538(i_1_ + -79, class293);
						if (Class287_Sub2.aString3272 == null) {
							Class287_Sub2.aString3272 = "Null";
						}
						Class246_Sub3_Sub3.applyMenuText = class293.applyText + "<col=ffffff>";
					}
				} else {
					if (i_5_ == 10) {
						NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(index, -1);
						if (class98_sub39 != null) {
							Class98_Sub10_Sub32.anInt5720 = 0;
							BConfigDefinition.anInt3117 = i_2_;
							NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
							OpenGLRenderEffectManager.anInt440 = 2;
							Class98_Sub49.anInt4286 = i;
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, NewsLoadingScreenElement.aClass171_3470, Class331.aClass117_2811);
							class98_sub11.packet.method1231(client.keyListener.isKeyDown(82, 5503) ? 1 : 0, (byte) -109);
							class98_sub11.packet.writeLEShort(index, i_1_ + 17546);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
							Class76_Sub2.requestFlag(0, 0, class246_sub3_sub4_sub2_sub1.getSize(0), -2, 0, class246_sub3_sub4_sub2_sub1.pathZ[0], class246_sub3_sub4_sub2_sub1.pathX[0], true, class246_sub3_sub4_sub2_sub1.getSize(0));
						}
					}
					if (i_5_ == 46) {
						OpenGLRenderEffectManager.anInt440 = 1;
						BConfigDefinition.anInt3117 = i_2_;
						Class98_Sub10_Sub32.anInt5720 = 0;
						Class98_Sub49.anInt4286 = i;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, StringConcatenator.aClass171_2000, Class331.aClass117_2811);
						System.out.println("Reachedddddddddd7");
						class98_sub11.packet.method1232(Class187.anInt1450, (byte) 94);
						class98_sub11.packet.writeLEShortA(Class310.anInt2652, 128);
						class98_sub11.packet.writeShort(Class272.gameSceneBaseX + i_3_, i_1_ + 1571862810);
						class98_sub11.packet.writeShort(GlobalPlayer.anInt3173, 1571862888);
						class98_sub11.packet.writeShort(aa_Sub2.gameSceneBaseY - -i_4_, 1571862888);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class76_Sub2.requestFlag(0, 0, 1, -4, 0, i_4_, i_3_, true, 1);
					}
					if ((i_5_ ^ 0xffffffff) == -19) {
						Class98_Sub49.anInt4286 = i;
						OpenGLRenderEffectManager.anInt440 = 2;
						BConfigDefinition.anInt3117 = i_2_;
						Class98_Sub10_Sub32.anInt5720 = 0;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ + 182, RsFloatBuffer.aClass171_5792, Class331.aClass117_2811);
						System.out.println("Reachedddddddddd6");
						class98_sub11.packet.writeLEShortA(Class272.gameSceneBaseX + i_3_, 128);
						class98_sub11.packet.writeLEShortA(index, 128);
						class98_sub11.packet.writeLEShortA(aa_Sub2.gameSceneBaseY + i_4_, 128);
						class98_sub11.packet.writeByte(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, 110);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class44.method427(-19181, i_4_, i_3_);
					}
					if (i_5_ == 1008) {
						Class98_Sub10_Sub32.anInt5720 = 0;
						BConfigDefinition.anInt3117 = i_2_;
						OpenGLRenderEffectManager.anInt440 = 2;
						Class98_Sub49.anInt4286 = i;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ + 182, Class98_Sub50.aClass171_4290, Class331.aClass117_2811);
						System.out.println("Reachedddddddddd5");
						class98_sub11.packet.writeShort(index, 1571862888);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
					}
					if (i_5_ == 22) {
						Player player = Class151_Sub9.players[index];
						if (player != null) {
							Class98_Sub10_Sub32.anInt5720 = 0;
							OpenGLRenderEffectManager.anInt440 = 2;
							BConfigDefinition.anInt3117 = i_2_;
							Class98_Sub49.anInt4286 = i;
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, OpenGlPointLight.aClass171_4168, Class331.aClass117_2811);
							System.out.println("Reachedddddddddd4");
							class98_sub11.packet.writeLEShortA(index, 128);
							class98_sub11.packet.method1231(client.keyListener.isKeyDown(82, i_1_ ^ 0x1531) ? 1 : 0, (byte) 122);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
							Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, 0, player.pathZ[0], player.pathX[0], true, player.getSize(0));
						}
					}
					if (i_5_ == 44) {
						Player player = Class151_Sub9.players[index];
						if (player != null) {
							Class98_Sub10_Sub32.anInt5720 = 0;
							BConfigDefinition.anInt3117 = i_2_;
							Class98_Sub49.anInt4286 = i;
							OpenGLRenderEffectManager.anInt440 = 2;
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, BConfigDefinitionParser.outgoingOpcode, Class331.aClass117_2811);
							System.out.println("Reachedddddddddd3");
							class98_sub11.packet.writeShort(index, 1571862888);
							class98_sub11.packet.writeByte(client.keyListener.isKeyDown(82, 5503) ? 1 : 0, -91);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
							Class76_Sub2.requestFlag(0, 0, player.getSize(0), -2, 0, player.pathZ[0], player.pathX[0], true, player.getSize(0));
						}
					}
					if ((i_5_ ^ 0xffffffff) == -24) {
						Class98_Sub49.anInt4286 = i;
						Class98_Sub10_Sub32.anInt5720 = 0;
						BConfigDefinition.anInt3117 = i_2_;
						OpenGLRenderEffectManager.anInt440 = 2;
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class284_Sub2.aClass171_5180, Class331.aClass117_2811);
						System.out.println("Reachedddddddddd2");
						class98_sub11.packet.writeLEShortA(i_4_ - -aa_Sub2.gameSceneBaseY, i_1_ ^ 0xce);
						class98_sub11.packet.method1231(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, (byte) 101);
						class98_sub11.packet.writeShortA(index, (byte) 126);
						class98_sub11.packet.writeLEShortA(i_3_ - -Class272.gameSceneBaseX, 128);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class44.method427(-19181, i_4_, i_3_);
					}
					if (i_5_ == 12) {
						NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(index, -1);
						if (class98_sub39 != null) {
							Class98_Sub10_Sub32.anInt5720 = 0;
							OpenGLRenderEffectManager.anInt440 = 2;
							Class98_Sub49.anInt4286 = i;
							NPC npc = class98_sub39.npc;
							BConfigDefinition.anInt3117 = i_2_;
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(i_1_ + 182, Class41.aClass171_371, Class331.aClass117_2811);
							System.out.println("Reachedddddddddd");
							class98_sub11.packet.writeShort(index, i_1_ ^ 0x5db0b926);
							class98_sub11.packet.method1231(!client.keyListener.isKeyDown(82, i_1_ + 5425) ? 0 : 1, (byte) -99);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
							Class76_Sub2.requestFlag(0, 0, npc.getSize(0), -2, 0, npc.pathZ[0], npc.pathX[0], true, npc.getSize(i_1_ ^ 0x4e));
						}
					}
					if ((i_5_ ^ 0xffffffff) == -26) {
						NodeObject entry = (NodeObject) ProceduralTextureSource.npc.get(index, i_1_ ^ ~0x4e);
						if (entry != null) {
							BConfigDefinition.anInt3117 = i_2_;
							OpenGLRenderEffectManager.anInt440 = 2;
							Class98_Sub49.anInt4286 = i;
							Class98_Sub10_Sub32.anInt5720 = 0;
							NPC npc = entry.npc;
							OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, OpenGlArrayBufferPointer.FIRST_OPTION, Class331.aClass117_2811);
							frame.packet.writeShort(index, 1571862888);
							frame.packet.method1244(!client.keyListener.isKeyDown(82, 5503) ? 0 : 1, (byte) 112);
							Class98_Sub10_Sub29.sendPacket(false, frame);
							Class76_Sub2.requestFlag(0, 0, npc.getSize(i_1_ + -78), -2, 0, npc.pathZ[0], npc.pathX[0], true, npc.getSize(0));
						}
					}
					if (Class98_Sub10_Sub9.aBoolean5585) {
						Class98_Sub10_Sub32.method1098((byte) -30);
					}
					if (RtKeyListener.aClass293_593 == null || (Class42_Sub3.anInt5365 ^ 0xffffffff) != -1) {
						break;
					}
					WorldMapInfoDefinitionParser.setDirty(i_1_ + -77, RtKeyListener.aClass293_593);
				}
			}
			break;
		} while (false);
	}

	public static final int parseInteger(int i, String string) {
		try {
			return StoreProgressMonitor.method2859(10, true, string, -21972);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "st.C(" + i + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	private byte[]			aByteArray2568;
	private int				anInt2570;
	private int				anInt2572;
	private int				anInt2574	= 0;
	private IOException		anIOException2569;

	private OutputStream	anOutputStream2573;

	private Thread			aThread2571;

	JavaNetworkWriter(OutputStream outputstream, int i) {
		anInt2572 = 0;
		try {
			anOutputStream2573 = outputstream;
			anInt2570 = 1 + i;
			aByteArray2568 = new byte[anInt2570];
			aThread2571 = new Thread(this);
			aThread2571.setDaemon(true);
			aThread2571.start();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "st.<init>(" + (outputstream != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final void run() {
		try {
			for (;;) {
				int i;
				synchronized (this) {
					for (;;) {
						if (anIOException2569 != null) {
							return;
						}
						if ((anInt2574 ^ 0xffffffff) < (anInt2572 ^ 0xffffffff)) {
							i = -anInt2574 + anInt2570 + anInt2572;
						} else {
							i = anInt2572 - anInt2574;
						}
						if ((i ^ 0xffffffff) < -1) {
							break;
						}
						try {
							this.wait();
						} catch (InterruptedException interruptedexception) {
							/* empty */
						}
					}
				}
				try {
					if (i + anInt2574 > anInt2570) {
						int i_0_ = -anInt2574 + anInt2570;
						anOutputStream2573.write(aByteArray2568, anInt2574, i_0_);
						anOutputStream2573.write(aByteArray2568, 0, i - i_0_);
					} else {
						anOutputStream2573.write(aByteArray2568, anInt2574, i);
					}
				} catch (IOException ioexception) {
					synchronized (this) {
						anIOException2569 = ioexception;
						break;
					}
				}
				synchronized (this) {
					anInt2574 = (anInt2574 - -i) % anInt2570;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "st.run(" + ')');
		}
	}

	public final void setDummyMode(boolean bool) {
		do {
			try {
				anOutputStream2573 = new OutputStream_Sub2();
				if (bool == true) {
					break;
				}
				aByteArray2568 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "st.E(" + bool + ')');
			}
			break;
		} while (false);
	}

	public final void shutdown(byte i) {
		try {
			synchronized (this) {
				if (anIOException2569 == null) {
					anIOException2569 = new IOException("");
				}
				if (i > -113) {
					return;
				}
				notifyAll();
			}
			try {
				aThread2571.join();
			} catch (InterruptedException interruptedexception) {
				/* empty */
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "st.D(" + i + ')');
		}
	}

	public final void write(int i, int i_7_, byte[] is, int i_8_) throws IOException {
		try {
			if (i_7_ < 0 || i < 0 || i_7_ + i > is.length) {
				throw new IOException();
			}
			synchronized (this) {
				if (anIOException2569 != null) {
					throw new IOException(anIOException2569.toString());
				}
				int i_9_;
				if (anInt2574 <= anInt2572) {
					i_9_ = anInt2570 - anInt2572 - (-anInt2574 - -1);
				} else {
					i_9_ = -1 + anInt2574 + -anInt2572;
				}
				if (i_9_ < i_7_) {
					throw new IOException("");
				}
				if (i_8_ != -5) {
					setDummyMode(true);
				}
				if (i_7_ + anInt2572 > anInt2570) {
					int i_10_ = anInt2570 - anInt2572;
					ArrayUtils.method2894(is, i, aByteArray2568, anInt2572, i_10_);
					ArrayUtils.method2894(is, i_10_ + i, aByteArray2568, 0, -i_10_ + i_7_);
				} else {
					ArrayUtils.method2894(is, i, aByteArray2568, anInt2572, i_7_);
				}
				anInt2572 = (i_7_ + anInt2572) % anInt2570;
				notifyAll();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "st.B(" + i + ',' + i_7_ + ',' + (is != null ? "{...}" : "null") + ',' + i_8_ + ')');
		}
	}
}

/* Class309 - Decompiled by JODE
 */ package com; /*
					*/

public final class TextResources {
	public static OutgoingOpcode	aClass171_2650;
	public static TextResources		aClass309_2611;
	public static IncomingOpcode	aClass58_2651;
	public static TextResources		ALREADY_ADDED_ON_IGNORE_LIST;
	public static TextResources		ATTACK;
	public static TextResources		CANCEL							= new TextResources("Cancel", "Abbrechen", "Annuler", "Cancelar");
	public static TextResources		CANT_ADD_YOURSELF_TO_FRIENDS_LIST;
	public static TextResources		CANT_ADD_YOURSELF_TO_IGNORE_LIST;
	public static TextResources		CHECKING_FOR_UPDATES;
	public static TextResources		CHOOSE_OPTION;
	public static TextResources		CONNECTION_LOST;
	public static TextResources		CONTINUE;
	public static TextResources		CREATING_ARCHIVES;
	public static TextResources		CREATING_CLIP_MAPS;
	public static TextResources		CREATING_CONFIGURATION_LIST;
	public static TextResources		CYAN;
	public static TextResources		DEUTSCH;
	public static TextResources		DISCARD;
	public static TextResources		DOWNLOADING_UPDATES;
	public static TextResources		DROP;
	public static TextResources		ERROR_EXECUTING_COMMAND			= new TextResources("There was an error executing the command.", "Es gab einen Fehler beim Ausf\u00fchren des Befehls.", "Une erreur s'est produite lors de l'ex\u00e9cution de la commande.",
			"Houve um erro quando o comando foi executado.");
	public static TextResources		EXAMINE;
	public static TextResources		FACE_HERE;
	public static TextResources		FLASH1;
	public static TextResources		FLASH2;
	public static TextResources		FLASH3;
	public static TextResources		FRIEND_ALREADY_ADDED;
	public static TextResources		FRIENDS_LIST_IS_FULL;
	public static TextResources		FROM_FRIENDS_LIST_FIRST;
	public static TextResources		FROM_YOUR_IGNORE_LIST_FIRST;
	public static TextResources		GLOW1;
	public static TextResources		GLOW2;
	public static TextResources		GLOW3;
	public static TextResources		GREEN;
	public static TextResources		HAS_LOGGED_IN;
	public static TextResources		HAS_LOGGED_OUT;
	public static TextResources		IGNORE_LIST_IS_FULL;
	public static TextResources		INITIALIZING_MUSIC;
	public static TextResources		INITIALIZING_NATIVE_MANAGER;
	public static TextResources		INITIALIZING_TOOLKIT;
	public static TextResources		LEVEL;
	public static TextResources		LOADING;
	public static TextResources		LOADING_BOX;
	public static TextResources		LOADING_SCREEN_DEFINITIONS;
	public static TextResources		LOADING_SCREEN_FONTS;
	public static TextResources		LOADING_SCREEN_MEDIA;
	public static TextResources		MEMBERS_OBJECT;
	public static TextResources		MILLION;
	public static TextResources		MILLION_SHORT;
	public static TextResources		PLEASE_REMOVE;
	public static TextResources		PLEASE_REMOVE_;
	public static TextResources		PLEASE_WAIT;
	public static TextResources		PLEASE_WAIT_ATTEMPTING;
	public static TextResources		PREFETCHING;
	public static TextResources		PROFILING;
	public static TextResources		PURPLE;
	public static TextResources		RATING;
	public static TextResources		RED;
	public static TextResources		REQUESTING_LOADING_SCREEN_FONTS;
	public static TextResources		SCROLL;
	public static TextResources		SELF;
	public static TextResources		SHAKE;
	public static TextResources		SKILL;
	public static TextResources		SLIDE;
	public static TextResources		TAKE;
	public static TextResources		THIS_IS_THE_DEVELOPER_CONSOLE	= new TextResources("This is the developer console. To close, press the `, \u00b2 or \u00a7 keys on your keyboard.", "Das ist die Entwicklerkonsole. Zum Schlie\u00dfen, die Tasten `, \u00b2 or \u00a7 dr\u00fccken.",
			"Ceci est la console de d\u00e9veloppement. Pour la fermer, appuyez sur les touches `, \u00b2 ou \u00a7.", "Este \u00e9 o painel de controle do desenvolvedor. Para fechar, pressione `, \u00b2 ou \u00a7.");
	public static TextResources		THOUSAND;
	public static TextResources		THOUSAND_SHORT;
	public static TextResources		UNABLE_TO_FIND;
	public static TextResources		UNKNOWN_DEVELOPER_COMMAND		= new TextResources("Unknown developer command: ", "Unbekannter Befehl: ", "Commande inconnue : ", "Comando desconhecido: ");
	public static TextResources		WAITING_FOR_MEMORY;
	public static TextResources		WALK_HERE;
	public static TextResources		WAVE;
	public static TextResources		WAVE2;
	public static TextResources		WHITE;
	public static TextResources		WHITE_SPACE;
	public static TextResources		YELLOW;

	static {
		new TextResources("#Player", "#Spieler", "#Joueur", "#Jogador");
		MEMBERS_OBJECT = new TextResources("Members object", "Gegenstand f\u00fcr Mitglieder", "Objet d'abonn\u00e9s", "Objeto para membros");
		new TextResources("Login to a members' server to use this object.", "Du musst auf einer Mitglieder-Welt sein, um diesen Gegenstand zu benutzen.", "Connectez-vous \u00e0 un serveur d'abonn\u00e9s pour utiliser cet objet.", "Acesse um servidor para membros para usar este objeto.");
		new TextResources("Swap this note at any bank for the equivalent item.", "Dieses Zertifikat kann in einer Bank entsprechend eingetauscht werden.", "\u00c9changez ce re\u00e7u contre l'objet correspondant dans la banque de votre choix.",
				"V\u00e1 a qualquer banco para trocar esta nota pelo objeto equivalente.");
		DISCARD = new TextResources("Discard", "Ablegen", "Jeter", "Descartar");
		TAKE = new TextResources("Take", "Nehmen", "Prendre", "Pegar");
		DROP = new TextResources("Drop", "Fallen lassen", "Poser", "Largar");
		new TextResources("Ok", "Okay", "OK", "Ok");
		new TextResources("Select", "Ausw\u00e4hlen", "S\u00e9lectionner", "Selecionar");
		CONTINUE = new TextResources("Continue", "Weiter", "Continuer", "Continuar");
		new TextResources("Invalid player name.", "Unzul\u00e4ssiger Charaktername!", "Nom de joueur incorrect.", "Nome de jogador inv\u00e1lido.");
		new TextResources("You can't report yourself!", "Du kannst dich nicht selbst melden!", "Vous ne pouvez pas vous signaler vous-m\u00eame !", "Voc\u00ea n\u00e3o pode denunciar a si pr\u00f3prio!");
		new TextResources("You already sent an abuse report under 60 secs ago! Do not abuse this system!", "Du hast bereits vor weniger als 60 Sekunden einen Regelversto\u00df gemeldet!", "Vous avez d\u00e9j\u00e0 signal\u00e9 un abus il y a moins d'une minute ! N'abusez pas du syst\u00e8me !",
				"Voc\u00ea j\u00e1 enviou uma den\u00fancia de abuso h\u00e1 menos de um minuto. N\u00e3o abuse deste sistema!");
		new TextResources(null, "Dieses System darf nicht missbraucht werden!", null, null);
		new TextResources("You cannot report that person for Staff Impersonation, they are Jagex Staff.", "Diese Person ist ein Jagex-Mitarbeiter!", "Cette personne est un membre du personnel de Jagex, vous ne pouvez pas la signaler pour abus d'identit\u00e9.",
				"Voc\u00ea n\u00e3o pode denunciar essa pessoa por tentar se passar por membro da equipe Jagex, pois ela faz parte da equipe.");
		new TextResources("You can spot a Jagex moderator by the gold crown next to their name.", "Jagex-Mitarbeiter haben eine goldene Krone neben ihrem Namen.", "Vous pouvez reconna\u00eetre les mod\u00e9rateurs Jagex \u00e0 la couronne dor\u00e9e en regard de leur nom.",
				"Os moderadores da Jagex s\u00e3o identificados por uma coroa dourada pr\u00f3xima ao \u007fnome.");
		new TextResources("You can report that person under a different rule.", "Diese Person kann bez\u00fcglich einer anderen Regel gemeldet werden.", "Vous pouvez signaler cette personne pour une autre infraction aux r\u00e8gles.",
				"Voc\u00ea n\u00e3o pode denunciar essa pessoa de acordo com uma regra diferente.");
		new TextResources("Thank-you, your abuse report has been received.", "Vielen Dank, deine Meldung ist bei uns eingegangen.", "Merci, nous avons bien re\u00e7u votre rapport d'abus.", "Obrigado. Sua den\u00fancia de abuso foi recebida.");
		new TextResources("Unable to send abuse report - system busy.", "Meldung konnte nicht gesendet werden - Systeme \u00fcberlastet", "Impossible de signaler un abus - Erreur syst\u00e8me", "Sistema ocupado. N\u00e3o foi poss\u00edvel enviar sua den\u00fancia de abuso.");
		new TextResources("Invalid name", "Unzul\u00e4ssiger Name!", "Nom incorrect", "Nome inv\u00e1lido");
		new TextResources("To use this item please login to a members' server.", "Du musst auf einer Mitglieder-Welt sein, um diesen Gegenstand zu benutzen.", "Veuillez vous connecter \u00e0 un serveur d'abonn\u00e9s pour utiliser cet objet.",
				"Acesse um servidor para membros para usar este objeto.");
		new TextResources("To interact with this please login to a members' server.", "Logg dich auf einer Mitglieder-Welt ein, um damit zu interagieren.", "Veuillez vous connecter \u00e0 un serveur d'abonn\u00e9s pour cette interaction.", "Para interagir, acesse um servidor para membros.");
		new TextResources("Nothing interesting happens.", "Nichts Interessantes passiert.", "Il ne se passe rien d'int\u00e9ressant.", "Nada de interessante acontece.");
		new TextResources("You can't reach that.", "Da kommst du nicht hin.", "Vous ne pouvez pas l'atteindre.", "Voc\u00ea n\u00e3o consegue alcan\u00e7ar isso.");
		new TextResources("Invalid teleport!", "Unzul\u00e4ssiger Teleport!", "T\u00e9l\u00e9portation non valide !", "Teleporte inv\u00e1lido!");
		new TextResources("To go here you must login to a members' server.", "Du musst auf einer Mitglieder-Welt sein, um dort hinzukommen.", "Vous devez vous connecter \u00e0 un serveur d'abonn\u00e9s pour aller \u00e0 cet endroit.", "Para entrar aqui, acesse um servidor para membros.");
		new TextResources("Unable to add friend - system busy.", "Der Freund konnte nicht hinzugef\u00fcgt werden, das System ist derzeit ausgelastet.", "Impossible d'ajouter un ami - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel adicionar o amigo. O sistema est\u00e1 ocupado.");
		new TextResources("Unable to add friend - unknown player.", "Spieler konnte nicht hinzugef\u00fcgt werden - Spieler unbekannt.", "Impossible d'ajouter l'ami - joueur inconnu.", "N\u00e3o foi poss\u00edvel adicionar um amigo - jogador desconhecido.");
		new TextResources("Unable to add name - system busy.", "Der Name konnte nicht hinzugef\u00fcgt werden, das System ist derzeit ausgelastet.", "Impossible d'ajouter un nom - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel adicionar o nome. O sistema est\u00e1 ocupado.");
		new TextResources("Unable to add name - unknown player.", "Name konnte nicht hinzugef\u00fcgt werden - Spieler unbekannt.", "Impossible d'ajouter le nom - joueur inconnu.", "N\u00e3o foi poss\u00edvel adicionar um nome - jogador desconhecido.");
		FRIENDS_LIST_IS_FULL = new TextResources("Your friends list is full, max of 200 for free users, and 200 for members.", "Ihre Freunde-Liste ist voll!", "Votre liste d'amis est pleine (200 noms maximum pour la version gratuite et 200 pour les abonn\u00e9s).",
				"Sua lista de amigos est\u00e1 cheia. O limite \u00e9 200 para usu\u00e1rios n\u00e3o pagantes, e 200 para membros.");
		DEUTSCH = new TextResources(null, "Mitglieder k\u00f6nnen 200 Freunde hinzuf\u00fcgen, freie Spieler nur 200.", null, null);
		new TextResources("Unable to delete friend - system busy.", "Der Freund konnte nicht entfernt werden, das System ist derzeit ausgelastet.", "Impossible de supprimer un ami - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel excluir o amigo. O sistema est\u00e1 ocupado.");
		new TextResources("Unable to delete name - system busy.", "Name konnte nicht gel\u00f6scht werden - Systemfehler.", "Impossible d'effacer le nom - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel deletar o nome - sistema ocupado.");
		new TextResources("Unable to send message - system busy.", "Deine Nachricht konnte nicht verschickt werden, das System ist derzeit ausgelastet.", "Impossible d'envoyer un message - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel enviar a mensagem. O sistema est\u00e1 ocupado.");
		new TextResources("Unable to send message - player unavailable.", "Deine Nachricht konnte nicht verschickt werden,", "Impossible d'envoyer un message - joueur indisponible.", "N\u00e3o foi poss\u00edvel enviar a mensagem. O jogador n\u00e3o est\u00e1 dispon\u00edvel.");
		new TextResources(null, "der Spieler ist momentan nicht verf\u00fcgbar.", null, null);
		new TextResources("Unable to send message - player not on your friends list.", "Nachricht kann nicht geschickt werden,", "Impossible d'envoyer un message - joueur non inclus dans votre liste d'amis.",
				"N\u00e3o foi poss\u00edvel enviar a mensagem. O jogador n\u00e3o est\u00e1 na sua lista de amigos.");
		new TextResources(null, "Spieler nicht auf deiner Freunde-Liste.", null, null);
		new TextResources("You appear to be telling someone your password - please don't!", "Willst du jemandem dein Passwort verraten? Das darfst du nicht! Falls das", "Il semble que vous r\u00e9v\u00e9liez votre mot de passe \u00e0 quelqu'un - ne faites jamais \u00e7a !",
				"Parece que voc\u00ea est\u00e1 revelando sua senha a algu\u00e9m. N\u00e3o fa\u00e7a isso!");
		new TextResources("If you are not, please change your password to something more obscure!", "nicht der Fall ist, \u00e4ndere dein Passwort zu einem ungew\u00f6hnlicheren Begriff!", "Si ce n'est pas le cas, remplacez votre mot de passe par une formule moins \u00e9vidente !",
				"Caso n\u00e3o esteja, altere sua senha para algo mais obscuro!");
		new TextResources("Unable to send message - set your display name first by logging into the game.", "Nachricht konnte nicht gesendet werden.  Bitte richte erst deinen Charakternamen ein, ", "Impossible d'envoyer le message - enregistrez un nom de personnage en vous connectant au jeu.",
				"N\u00e3o \u00e9 poss\u00edvel enviar a mensagem. Defina um nome de personagem antes, fazendo login no jogo.");
		new TextResources(null, "indem du dich ins Spiel einloggst.", null, null);
		new TextResources("For that rule you can only report players who have spoken or traded recently.", "Mit dieser Option k\u00f6nnen nur Spieler gemeldet werden,", "Cette r\u00e8gle n'est invocable que pour les discussions ou \u00e9changes r\u00e9cents.",
				"Para essa regra, voc\u00ea s\u00f3 pode denunciar jogadores com quem tenha falado ou negociado recentemente.");
		new TextResources(null, "die k\u00fcrzlich gesprochen oder gehandelt haben.", null, null);
		new TextResources("That player is offline, or has privacy mode enabled.", "Dieser Spieler ist offline oder hat den Privatsph\u00e4ren-Modus aktiviert.", "Ce joueur est d\u00e9connect\u00e9 ou en mode priv\u00e9.",
				"O jogador est\u00e1 offline ou est\u00e1 com o modo de privacidade ativado.");
		new TextResources("You cannot send a quick chat message to a player on this world at this time.", "Einem Spieler auf dieser Welt k\u00f6nnen derzeit keine Direktchat-Nachrichten", "Impossible d'envoyer un message rapide \u00e0 un joueur de ce serveur \u00e0 l'heure actuelle.",
				"Voc\u00ea n\u00e3o pode enviar uma mensagem de papo r\u00e1pido para um jogador neste mundo neste momento.");
		new TextResources(null, "geschickt werden.", null, null);
		new TextResources("This player is on a quick chat world and cannot receive your message.", "Der Spieler kann auf einer Direktchat-Welt keine Nachrichten empfangen.", "Ce joueur est sur un serveur \u00e0 messagerie rapide et ne peut pas recevoir votre message.",
				"Este jogador n\u00e3o pode receber sua mensagem porque est\u00e1 em um mundo de papo r\u00e1pido.");
		new TextResources("Chat disabled", "Deaktiviert", "Messagerie d\u00e9sactiv\u00e9e", "Bate-papo desativado");
		new TextResources("clan_chat", "clanchat", "conversation_clan", "clan_chat");
		new TextResources("You are not currently in a clan channel.", "Du befindest dich derzeit nicht in einem Chatraum.", "Vous n'\u00eates pas dans un canal de clan.", "No momento voc\u00ea n\u00e3o est\u00e1 em um canal de cl\u00e3.");
		new TextResources("You are not allowed to talk in this clan channel.", "Du darfst in diesem Chatraum nicht reden.", "Vous n'\u00eates pas autoris\u00e9 \u00e0 parler dans ce canal de clan.", "Voc\u00ea n\u00e3o tem permiss\u00e3o para conversar neste canal de cl\u00e3.");
		new TextResources("Error sending message to clan chat - please try again later!", "Fehler beim Versenden der Nachricht - bitte versuch es sp\u00e4ter erneut.", "Erreur lors de l'envoi du message au canal de clan - veuillez r\u00e9essayer ult\u00e9rieurement.",
				"Erro ao enviar mensagem ao canal de cl\u00e3. Tente de novo depois!");
		new TextResources("Please wait until you are logged out of your previous channel.", "Bitte warte, bis du den vorherigen Chatraum verlassen hast.", "Veuillez attendre d'\u00eatre d\u00e9connect\u00e9(e) de votre canal pr\u00e9c\u00e9dent.",
				"Aguarde at\u00e9 se desconectar do canal anterior.");
		new TextResources("You are not currently in a channel.", "Du befindest dich derzeit nicht in einem Chatraum.", "Vous n'\u00eates dans aucun canal \u00e0 l'heure actuelle.", "No momento voc\u00ea n\u00e3o est\u00e1 em um canal.");
		new TextResources("Attempting to join channel...", "Chatraum wird betreten...", "Tentative de connexion au canal...", "Tentando acessar canal...");
		new TextResources("Sending request to leave channel...", "Chatraum wird verlassen...", "Envoi de la demande de sortie du canal...", "Enviando solicita\u00e7\u00e3o para deixar o canal...");
		new TextResources("Already attempting to join a channel - please wait...", "Du versuchst bereits, einem Chatraum beizutreten - bitte warte.", "Tentative de connexion au canal d\u00e9j\u00e0 en cours - veuillez patienter...", "J\u00e1 h\u00e1 uma tentativa de entrar em um canal. Aguarde...");
		new TextResources("Leave request already in progress - please wait...", "Du versuchst bereits, einen Chatraum zu verlassen - bitte warte.", "Demande de sortie d\u00e9j\u00e0 effectu\u00e9e - veuillez patienter...", "Solicita\u00e7\u00e3o de sa\u00edda j\u00e1 em andamento. Aguarde...");
		new TextResources("Invalid channel name entered!", "Ung\u00fcltiger Chatraum-Name angegeben.", "Nom de canal incorrect !", "Nome de canal inv\u00e1lido!");
		new TextResources("Unable to join clan chat at this time - please try again later!", "Chatraum kann nicht betreten werden - bitte versuch es sp\u00e4ter erneut.", "Impossible de participer \u00e0 une discussion de clan pour le moment - veuillez r\u00e9essayer ult\u00e9rieurement.",
				"N\u00e3o foi poss\u00edvel entrar no bate-papo do cl\u00e3 dessa vez. Tente de novo depois!");
		new TextResources("Now talking in clan channel ", "Chatraum: ", "Vous participez actuellement au canal de clan ", "Falando no canal do cl\u00e3 agora ");
		new TextResources("Now talking in clan channel of player: ", "Clanchat dieses Spieler beigetreten: ", "Vous participez actuellement au canal de clan du joueur : ", "Falando no canal do cl\u00e3 do jogador: ");
		new TextResources("To talk, start each line of chat with the / symbol.", "Leite eine Zeile mit / ein, um hier zu chatten.", "Pour parler, ins\u00e9rez le symbole / au d\u00e9but de chaque ligne.", "Para falar, comece cada linha de conversa com o s\u00edmbolo /.");
		new TextResources("Error joining clan channel - please try again later!", "Fehler beim Betreten des Chatraums - bitte versuch es sp\u00e4ter erneut.", "Erreur lors de la connexion au canal de clan - veuillez r\u00e9essayer ult\u00e9rieurement.",
				"Erro ao entrar no canal do cl\u00e3. Tente de novo depois!");
		new TextResources("You are temporarily blocked from joining channels - please try again later!", "Du darfst derzeit keine Chatr\u00e4ume betreten - bitte versuch es sp\u00e4ter.", "Vous \u00eates temporairement exclu des canaux - veuillez r\u00e9essayer ult\u00e9rieurement.",
				"Voc\u00ea est\u00e1 temporariamente impedido de entrar em canais. Tente de novo depois!");
		new TextResources("The channel you tried to join does not exist.", "Der von dir gew\u00fcnschte Chatraum existiert nicht.", "Le canal que vous essayez de rejoindre n'existe pas.", "O canal que voc\u00ea tentou acessar n\u00e3o existe.");
		new TextResources("The channel you tried to join is currently full.", "Der von dir gew\u00fcnschte Chatraum ist derzeit \u00fcberf\u00fcllt.", "Le canal que vous essayez de rejoindre est plein.", "O canal que voc\u00ea tentou acessar est\u00e1 cheio no momento.");
		new TextResources("You do not have a high enough rank to join this clan channel.", "Dein Rang reicht nicht aus, um diesen Chatraum zu betreten.", "Votre rang n'est pas assez \u00e9lev\u00e9 pour rejoindre ce canal de clan.",
				"Sua posi\u00e7\u00e3o n\u00e3o \u00e9 alta o suficiente para voc\u00ea entrar nesse canal de cl\u00e3.");
		new TextResources("You are temporarily banned from this clan channel.", "Du wurdest tempor\u00e4r aus diesem Chatraum verbannt.", "Vous \u00eates temporairement exclu de ce canal de clan.", "Voc\u00ea est\u00e1 temporariamente vetado de entrar nesse canal de cl\u00e3.");
		new TextResources("You are not allowed to join this user's clan channel.", "Du darfst den Chatraum dieses Benutzers nicht betreten.", "Vous n'\u00eates pas autoris\u00e9 \u00e0 rejoindre le canal de clan de cet utilisateur.",
				"Voc\u00ea n\u00e3o tem permiss\u00e3o para entrar no canal de cl\u00e3 desse usu\u00e1rio.");
		new TextResources(" joined the channel.", " hat den Chatraum betreten.", " a rejoint le canal.", " entrou no canal.");
		new TextResources(" left the channel.", " hat den Chatraum verlassen.", " a quitt\u00e9 le canal.", " deixou o canal.");
		new TextResources(" was kicked from the channel.", " wurde aus dem Chatraum rausgeworfen.", " a \u00e9t\u00e9 expuls\u00e9 du canal.", " foi expulso do canal.");
		new TextResources("You have been kicked from the channel.", "Du wurdest aus dem Chatraum rausgeworfen.", "Vous avez \u00e9t\u00e9 expuls\u00e9 du canal.", "Voc\u00ea foi expulso do canal.");
		new TextResources("You have been removed from this channel.", "Du wurdest aus dem Chatraum entfernt.", "Vous avez \u00e9t\u00e9 supprim\u00e9 de ce canal.", "Voc\u00ea foi retirado desse canal.");
		new TextResources("You have left the channel.", "Du hast den Chatraum verlassen.", "Vous avez quitt\u00e9 le canal.", "Voc\u00ea saiu do canal.");
		new TextResources("Your clan channel has now been enabled!", "Dein Chatraum ist jetzt eingeschaltet.", "Votre canal de clan est activ\u00e9 !", "Seu canal de cl\u00e3 j\u00e1 est\u00e1 ativado!");
		new TextResources("Join your channel by clicking 'Join Chat' and typing: ", "Klick auf 'Betreten' und gib ein: ", "Pour rejoindre votre canal, cliquez sur \u00ab Participer \u00bb et entrez : ", "Para entrar no seu canal, clique em \"Acessar bate-papo\" e digite: ");
		new TextResources("Your clan channel has now been disabled!", "Dein Chatraum ist jetzt ausgeschaltet.", "Votre canal de clan est d\u00e9sactiv\u00e9.", "Seu canal de cl\u00e3 foi desativado!");
		new TextResources("You do not have permission to kick users in this channel.", "Du darfst keine Benutzer aus diesem Chatraum rauswerfen.", "Vous n'\u00eates pas autoris\u00e9 \u00e0 expulser des utilisateurs de ce canal.",
				"Voc\u00ea n\u00e3o tem permiss\u00e3o para expulsar usu\u00e1rios neste canal.");
		new TextResources("You do not have permission to kick this user.", "Du darfst diesen Benutzer nicht rauswerfen.", "Vous n'\u00eates pas autoris\u00e9 \u00e0 expulser cet utilisateur.", "Voc\u00ea n\u00e3o tem permiss\u00e3o para expulsar este usu\u00e1rio.");
		new TextResources("That user is not in this channel.", "Dieser Benutzer befindet sich nicht in diesem Chatraum.", "Cet utilisateur n'est pas dans ce canal.", "Esse usu\u00e1rio n\u00e3o est\u00e1 no canal.");
		new TextResources("Your request to kick/ban this user was successful.", "Der Rauswurf/die Sperrung war erfolgreich.", "Votre demande d'exclusion de ce joueur a \u00e9t\u00e9 accept\u00e9e.", "Seu pedido para expulsar/suspender este jogador foi bem sucedido.");
		new TextResources("Your request to refresh this user's temporary ban was successful.", "Die Verl\u00e4ngerung der tempor\u00e4ren Sperrung dieses Spielers war erfolgreich.", "Le renouvellement d'exclusion temporaire de ce joueur a \u00e9t\u00e9 accept\u00e9.",
				"Seu pedido para reiniciar a suspens\u00e3o tempor\u00e1ria deste jogador foi bem sucedido.");
		new TextResources("You have been temporarily muted due to breaking a rule.", "Aufgrund eines Regelversto\u00dfes wurdest du vor\u00fcbergehend stumm geschaltet.", "La messagerie instantan\u00e9e a \u00e9t\u00e9 temporairement bloqu\u00e9e suite \u00e0 une infraction.",
				"Voc\u00ea foi temporariamente vetado por ter violado uma regra.");
		new TextResources("This mute will remain for a further ", "Diese Stummschaltung gilt f\u00fcr weitere ", "Votre acc\u00e8s restera bloqu\u00e9 encore ", "Este veto permanecer\u00e1 por mais ");
		new TextResources(" days.", " Tage.", " jours.", " dias.");
		new TextResources("You will be un-muted within 24 hours.", "Du wirst innerhalb der n\u00e4chsten 24 Stunden wieder sprechen k\u00f6nnen.", "Vous aurez \u00e0 nouveau acc\u00e8s \u00e0 la messagerie instantan\u00e9e dans 24 heures.", "O veto ser\u00e1 retirado dentro de 24 horas.");
		new TextResources("To prevent further mutes please read the rules.", "Um eine erneute Stummschaltung zu verhindern, lies bitte die Regeln.", "Pour \u00e9viter un nouveau blocage, lisez le r\u00e8glement.", "Para evitar outros vetos, leia as regras.");
		new TextResources("You have been permanently muted due to breaking a rule.", "Du wurdest permanent stumm geschaltet, da du gegen eine Regel versto\u00dfen hast.",
				"L'acc\u00e8s \u00e0 la messagerie instantan\u00e9e vous a d\u00e9finitivement \u00e9t\u00e9 retir\u00e9 suite \u00e0 une infraction.", "Voc\u00ea foi permanentemente vetado por ter violado uma regra.");
		LOADING = new TextResources("Please wait while the applet is preparing.", "Ladevorgang - bitte warte.", "Chargement en cours. Veuillez patienter.", "Carregando. Aguarde.");
		PROFILING = new TextResources("Profiling...", "Profiling...", "Profilage...", "Profiling...");
		CONNECTION_LOST = new TextResources("Connection lost.", "Verbindung abgebrochen.", "Connexion perdue.", "Conex\u00e3o perdida.");
		PLEASE_WAIT_ATTEMPTING = new TextResources("Please wait - attempting to reestablish.", "Bitte warte - es wird versucht, die Verbindung wiederherzustellen.", "Veuillez patienter - tentative de r\u00e9tablissement.", "Tentando reestabelecer conex\u00e3o. Aguarde.");
		CHECKING_FOR_UPDATES = new TextResources("Checking for updates - ", "Suche nach Updates - ", "V\u00e9rification des mises \u00e0 jour - ", "Verificando atualiza\u00e7\u00f5es - ");
		DOWNLOADING_UPDATES = new TextResources("Finishing off; almost ready - ", "Lade Update - ", "Chargement des MAJ - ", "Carregando atualiza\u00e7\u00f5es - ");
		WAITING_FOR_MEMORY = new TextResources("Waiting for memory - ", "Lade Konfiguration - ", "Chargement des fichiers config - ", "Carregando config - ");
		REQUESTING_LOADING_SCREEN_FONTS = new TextResources("Requesting loading screen fonts - ", "Konfig geladen.", "Fichiers config charg\u00e9s", "Config carregada");
		PREFETCHING = new TextResources("Prefetching - ", "Lade Sprites - ", "Chargement des sprites - ", "Carregando sprites - ");
		LOADING_SCREEN_MEDIA = new TextResources("Loaded screen media - ", "Sprites geladen.", "Sprites charg\u00e9s", "Sprites carregados");
		LOADING_SCREEN_FONTS = new TextResources("Loaded screen fonts - ", "Lade Wordpack - ", "Chargement du module texte - ", "Carregando pacote de palavras - ");
		LOADING_SCREEN_DEFINITIONS = new TextResources("Loaded screen definitions - ", "Wordpack geladen.", "Module texte charg\u00e9", "Pacote de palavras carregado");
		INITIALIZING_TOOLKIT = new TextResources("Initializing toolkit - ", "Lade Benutzeroberfl\u00e4che - ", "Chargement des interfaces - ", "Carregando interfaces - ");
		INITIALIZING_NATIVE_MANAGER = new TextResources("Initializing native manager - ", "Benutzeroberfl\u00e4che geladen.", "Interfaces charg\u00e9es", "Interfaces carregadas");
		INITIALIZING_MUSIC = new TextResources("Initializing music - ", "Lade Interface-Skripte - ", "Chargement des interfaces - ", "Carregando interfaces - ");
		CREATING_CONFIGURATION_LIST = new TextResources("Creating configuration lists - ", "Interface-Skripte geladen", "Interfaces charg\u00e9es", "Interfaces carregadas");
		CREATING_ARCHIVES = new TextResources("Creating archives - ", "Lade Zusatzschriftarten - ", "Chargement de polices secondaires - ", "Carregando fontes principais - ");
		CREATING_CLIP_MAPS = new TextResources("Creating clip maps - ", "Zusatzschriftarten geladen", "Polices secondaires charg\u00e9es", "Fontes principais carregadas");
		new TextResources("Loading world map - ", "Lade Weltkarte - ", "Chargement de la mappemonde - ", "Carregando mapa-m\u00fandi - ");
		new TextResources("Loaded world map", "Weltkarte geladen", "Mappemonde charg\u00e9e", "Mapa-m\u00fandi carregado");
		new TextResources("Loading world list data", "Lade Liste der Welten", "Chargement de la liste des serveurs", "Carregando dados da lista de mundos");
		new TextResources("Loaded world list data", "Liste der Welten geladen", "Liste des serveurs charg\u00e9e", "Dados da lista de mundos carregados");
		new TextResources("Loaded client variable data", "Client-Variablen geladen", "Variables du client charg\u00e9es", "As vari\u00e1veis do sistema foram carregadas");
		LOADING_BOX = new TextResources("Loading...", "Lade...", "Chargement en cours...", "Carregando...");
		new TextResources("Please close the interface you have open before using 'Report Abuse'.", "Bitte schlie\u00df die momentan ge\u00f6ffnete Benutzeroberfl\u00e4che,", "Fermez l'interface que vous avez ouverte avant d'utiliser le bouton \u00ab Signaler un abus \u00bb.",
				"Feche a interface aberta antes de usar o recurso \"Denunciar abuso\".");
		new TextResources(null, "bevor du die Option 'Regelversto\u00df melden' benutzt.", null, null);
		new TextResources("System update in: ", "System-Update in: ", "Mise \u00e0 jour syst\u00e8me dans : ", "Atualiza\u00e7\u00e3o do sistema em: ");
		HAS_LOGGED_IN = new TextResources(" has logged in.", " loggt sich ein.", " s'est connect\u00e9.", " entrou no jogo.");
		HAS_LOGGED_OUT = new TextResources(" has logged out.", " loggt sich aus.", " s'est d\u00e9connect\u00e9.", " saiu do jogo.");
		UNABLE_TO_FIND = new TextResources("Unable to find ", "Spieler kann nicht gefunden werden: ", "Impossible de trouver ", "N\u00e3o \u00e9 poss\u00edvel encontrar ");
		new TextResources("Use", "Benutzen", "Utiliser", "Usar");
		EXAMINE = new TextResources("Examine", "Untersuchen", "Examiner", "Examinar");
		ATTACK = new TextResources("Attack", "Angreifen", "Attaquer", "Atacar");
		CHOOSE_OPTION = new TextResources("Choose Option", "W\u00e4hl eine Option", "Choisir une option", "Selecionar op\u00e7\u00e3o");
		aClass309_2611 = new TextResources(" more options", " weitere Optionen", " autres options", " mais op\u00e7\u00f5es");
		WALK_HERE = new TextResources("Walk here", "Hierhin gehen", "Atteindre", "Caminhar para c\u00e1");
		FACE_HERE = new TextResources("Face here", "Hierhin drehen", "Regarder dans cette direction", "Virar para c\u00e1");
		LEVEL = new TextResources("level: ", "Stufe: ", "niveau ", "n\u00edvel: ");
		SKILL = new TextResources("skill: ", "Fertigkeit: ", "comp\u00e9tence ", "habilidade: ");
		RATING = new TextResources("rating: ", "Kampfstufe: ", "classement ", "qualifica\u00e7\u00e3o: ");
		PLEASE_WAIT = new TextResources("Please wait...", "Bitte warte...", "Veuillez patienter...", "Aguarde...");
		new TextResources("Close", "Bitte schlie\u00df die momentan ge\u00f6ffnete Benutzeroberfl\u00e4che,", "Fermez l'interface que vous avez ouverte avant d'utiliser le bouton \u00ab Signaler un abus \u00bb.", "Feche a interface aberta antes de usar o recurso \"Denunciar abuso\".");
		WHITE_SPACE = new TextResources(" ", ": ", " ", " ");
		MILLION = new TextResources("M", "M", "M", "M");
		MILLION_SHORT = new TextResources("M", "M", "M", "M");
		THOUSAND = new TextResources("K", "T", "K", "K");
		THOUSAND_SHORT = new TextResources("K", "T", "K", "K");
		new TextResources("From", "Von:", "De", "De");
		SELF = new TextResources("Self", "Mich", "Moi", "Eu");
		FRIEND_ALREADY_ADDED = new TextResources(" is already on your friends list.", " steht bereits auf deiner Freunde-Liste!", " est d\u00e9j\u00e0 dans votre liste d'amis.", " j\u00e1 est\u00e1 na sua lista de amigos.");
		IGNORE_LIST_IS_FULL = new TextResources("Your ignore list is full. Max of 100 users.", "Deine Ignorieren-Liste ist voll, du kannst nur 100 Spieler darauf eintragen.", "Votre liste noire est pleine (100 noms maximum).",
				"Sua lista de ignorados est\u00e1 cheia. O limite \u00e9 100 usu\u00e1rios.");
		ALREADY_ADDED_ON_IGNORE_LIST = new TextResources(" is already on your ignore list.", " steht bereits auf deiner Ignorieren-Liste!", " est d\u00e9j\u00e0 dans votre liste noire.", " j\u00e1 est\u00e1 na sua lista de ignorados.");
		CANT_ADD_YOURSELF_TO_FRIENDS_LIST = new TextResources("You can't add yourself to your own friends list.", "Du kannst dich nicht auf deine eigene Freunde-Liste setzen!", "Vous ne pouvez pas ajouter votre nom \u00e0 votre liste d'amis.",
				"Voc\u00ea n\u00e3o pode adicionar a si pr\u00f3prio \u00e0 sua lista de amigos.");
		CANT_ADD_YOURSELF_TO_IGNORE_LIST = new TextResources("You can't add yourself to your own ignore list.", "Du kannst dich nicht auf deine eigene Ignorieren-Liste setzen!", "Vous ne pouvez pas ajouter votre nom \u00e0 votre liste noire.",
				"Voc\u00ea n\u00e3o pode adicionar a si pr\u00f3prio \u00e0 sua lista de ignorados.");
		new TextResources("Changes will take effect on your clan in the next 60 seconds.", "Die \u00c4nderungen am Chatraum werden innerhalb von 60 Sekunden g\u00fcltig.", "Les modifications seront apport\u00e9es \u00e0 votre clan dans les prochaines 60 secondes.",
				"As altera\u00e7\u00f5es passar\u00e3o a valer no seu cl\u00e3 nos pr\u00f3ximos 60 segundos.");
		PLEASE_REMOVE = new TextResources("Please remove ", "Bitte entferne ", "Veuillez commencer par supprimer ", "Remova ");
		FROM_YOUR_IGNORE_LIST_FIRST = new TextResources(" from your ignore list first.", " zuerst von deiner Ignorieren-Liste!", " de votre liste noire.", " da sua lista de ignorados primeiro.");
		PLEASE_REMOVE_ = new TextResources("Please remove ", "Bitte entferne ", "Veuillez commencer par supprimer ", "Remova ");
		FROM_FRIENDS_LIST_FIRST = new TextResources(" from your friends list first.", " zuerst von deiner Freunde-Liste!", " de votre liste d'amis.", " da sua lista de amigos primeiro.");
		YELLOW = new TextResources("yellow:", "gelb:", "jaune:", "amarelo:");
		RED = new TextResources("red:", "rot:", "rouge:", "vermelho:");
		GREEN = new TextResources("green:", "gr\u00fcn:", "vert:", "verde:");
		CYAN = new TextResources("cyan:", "blaugr\u00fcn:", "cyan:", "cyan:");
		PURPLE = new TextResources("purple:", "lila:", "violet:", "roxo:");
		WHITE = new TextResources("white:", "weiss:", "blanc:", "branco:");
		FLASH1 = new TextResources("flash1:", "blinken1:", "clignotant1:", "flash1:");
		FLASH2 = new TextResources("flash2:", "blinken2:", "clignotant2:", "flash2:");
		FLASH3 = new TextResources("flash3:", "blinken3:", "clignotant3:", "brilho3:");
		GLOW1 = new TextResources("glow1:", "leuchten1:", "brillant1:", "brilho1:");
		GLOW2 = new TextResources("glow2:", "leuchten2:", "brillant2:", "brilho2:");
		GLOW3 = new TextResources("glow3:", "leuchten3:", "brillant3:", "brilho3:");
		WAVE = new TextResources("wave:", "welle:", "ondulation:", "onda:");
		WAVE2 = new TextResources("wave2:", "welle2:", "ondulation2:", "onda2:");
		SHAKE = new TextResources("shake:", "sch\u00fctteln:", "tremblement:", "tremor:");
		SCROLL = new TextResources("scroll:", "scrollen:", "d\u00e9roulement:", "rolagem:");
		SLIDE = new TextResources("slide:", "gleiten:", "glissement:", "deslizamento:");
		aClass171_2650 = new OutgoingOpcode(79, -1);
		aClass58_2651 = new IncomingOpcode(38, 3);
	}

	public static final void method3614(boolean bool, int i) {
		do {
			for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub37.aClass148_5748.getFirst(32); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub37.aClass148_5748.getNext(119)) {
				if (class98_sub42.aClass98_Sub31_Sub5_4232 != null) {
					Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4232);
					class98_sub42.aClass98_Sub31_Sub5_4232 = null;
				}
				if (class98_sub42.aClass98_Sub31_Sub5_4230 != null) {
					Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4230);
					class98_sub42.aClass98_Sub31_Sub5_4230 = null;
				}
				class98_sub42.unlink(118);
			}
			if (!bool) {
				break;
			}
			for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class358.aClass148_3032.getFirst(32); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class358.aClass148_3032.getNext(110)) {
				if (class98_sub42.aClass98_Sub31_Sub5_4232 != null) {
					Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4232);
					class98_sub42.aClass98_Sub31_Sub5_4232 = null;
				}
				class98_sub42.unlink(57);
			}
			for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub14.aClass377_5612.startIteration(118); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub14.aClass377_5612.iterateNext(-1)) {
				if (class98_sub42.aClass98_Sub31_Sub5_4232 != null) {
					Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4232);
					class98_sub42.aClass98_Sub31_Sub5_4232 = null;
				}
				class98_sub42.unlink(68);
			}
			break;
		} while (false);
	}

	public static void method3616(int i) {
		LOADING = null;
		FLASH3 = null;
		THIS_IS_THE_DEVELOPER_CONSOLE = null;
		TAKE = null;
		HAS_LOGGED_IN = null;
		SLIDE = null;
		CANT_ADD_YOURSELF_TO_IGNORE_LIST = null;
		SHAKE = null;
		WAVE = null;
		LOADING_BOX = null;
		PURPLE = null;
		WALK_HERE = null;
		GLOW3 = null;
		PLEASE_REMOVE = null;
		WHITE = null;
		PLEASE_WAIT = null;
		THOUSAND = null;
		FLASH2 = null;
		if (i == 38) {
			THOUSAND_SHORT = null;
			SCROLL = null;
			WHITE_SPACE = null;
			GREEN = null;
			RATING = null;
			FRIENDS_LIST_IS_FULL = null;
			DISCARD = null;
			FLASH1 = null;
			CHOOSE_OPTION = null;
			IGNORE_LIST_IS_FULL = null;
			CHECKING_FOR_UPDATES = null;
			CANCEL = null;
			SELF = null;
			ALREADY_ADDED_ON_IGNORE_LIST = null;
			DROP = null;
			UNABLE_TO_FIND = null;
			PLEASE_WAIT_ATTEMPTING = null;
			ERROR_EXECUTING_COMMAND = null;
			CONNECTION_LOST = null;
			MILLION_SHORT = null;
			ATTACK = null;
			LEVEL = null;
			DEUTSCH = null;
			RED = null;
			GLOW1 = null;
			YELLOW = null;
			PROFILING = null;
			aClass309_2611 = null;
			FROM_YOUR_IGNORE_LIST_FIRST = null;
			CONTINUE = null;
			aClass171_2650 = null;
			FRIEND_ALREADY_ADDED = null;
			DOWNLOADING_UPDATES = null;
			WAVE2 = null;
			EXAMINE = null;
			CANT_ADD_YOURSELF_TO_FRIENDS_LIST = null;
			aClass58_2651 = null;
			MILLION = null;
			FACE_HERE = null;
			FROM_FRIENDS_LIST_FIRST = null;
			SKILL = null;
			GLOW2 = null;
			CYAN = null;
			MEMBERS_OBJECT = null;
			PLEASE_REMOVE_ = null;
			HAS_LOGGED_OUT = null;
			UNKNOWN_DEVELOPER_COMMAND = null;
		}
	}

	private String[] translations;

	private TextResources(String english, String german, String french, String portuguese) {
		translations = new String[] { english, german, french, portuguese };
	}

	public final String getText(int languageIndex, byte i_1_) {
		return translations[languageIndex];
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

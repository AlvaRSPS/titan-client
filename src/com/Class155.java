/* Class155 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.LightningDetailPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementFactory;
import com.jagex.game.constants.BuildLocation;

public final class Class155 {
	public static final boolean method2242(int i, int i_2_, Class155 class155) {
		Js5Exception.aClass111_3203.method2103(class155.anIntArray1240[i], class155.anIntArray1237[i], class155.anIntArray1241[i], Class114.anIntArray958);
		if (i_2_ != 5541) {
			BuildLocation.WTI = null;
		}
		int i_3_ = Class114.anIntArray958[2];
		if (i_3_ < 50) {
			return false;
		}
		class155.aShortArray1244[i] = (short) (Class2.anInt69 + Class38.anInt358 * Class114.anIntArray958[0] / i_3_);
		class155.aShortArray1235[i] = (short) (Class98_Sub10_Sub23.anInt5659 + Class114.anIntArray958[1] * Class331.anInt2800 / i_3_);
		class155.aShortArray1234[i] = (short) i_3_;
		return true;
	}

	public static final Class53_Sub1 method2494(byte i) {
		VarClientStringsDefinitionParser.anInt1843 = 0;
		return Class69_Sub2.method706(200);
	}

	public static final void method2495(byte[] is, byte i) {
		do {
			RSByteBuffer buffer = new RSByteBuffer(is);
			boolean bool = false;
			if (i == -25) {
				for (;;) {
					int i_0_ = buffer.readUnsignedByte((byte) -105);
					if (i_0_ == 0) {
						break;
					}
					if ((i_0_ ^ 0xffffffff) == -2) {
						if (ItemSearch.anIntArray457 == null) {
							JavaThreadResource.anIntArray1764 = new int[4];
							LoadingScreenElementFactory.anInt3090 = 4;
							ItemSearch.anIntArray457 = new int[4];
						}
						for (int i_1_ = 0; ItemSearch.anIntArray457.length > i_1_; i_1_++) {
							ItemSearch.anIntArray457[i_1_] = buffer.readUShort(false);
							JavaThreadResource.anIntArray1764[i_1_] = buffer.readUShort(false);
						}
						bool = true;
					} else if (i_0_ == 2) {
						LightningDetailPreferenceField.anInt3666 = buffer.readShort((byte) 127);
					} else if ((i_0_ ^ 0xffffffff) == -4) {
						LoadingScreenElementFactory.anInt3090 = buffer.readUnsignedByte((byte) -109);
						ItemSearch.anIntArray457 = new int[LoadingScreenElementFactory.anInt3090];
						JavaThreadResource.anIntArray1764 = new int[LoadingScreenElementFactory.anInt3090];
					}
				}
				if (bool) {
					break;
				}
				if (ItemSearch.anIntArray457 == null) {
					LoadingScreenElementFactory.anInt3090 = 4;
					JavaThreadResource.anIntArray1764 = new int[4];
					ItemSearch.anIntArray457 = new int[4];
				}
				for (int i_2_ = 0; i_2_ < ItemSearch.anIntArray457.length; i_2_++) {
					ItemSearch.anIntArray457[i_2_] = 0;
					JavaThreadResource.anIntArray1764[i_2_] = 20 * i_2_;
				}
			}
			break;
		} while (false);
	}

	byte			aByte1238;
	byte			aByte1242;
	int[]			anIntArray1237;
	int[]			anIntArray1240;
	int[]			anIntArray1241;
	short			aShort1236;
	short			aShort1239;
	short			aShort1243;
	short			aShort1245;

	short[]			aShortArray1234;

	short[]			aShortArray1235;

	public short[]	aShortArray1244;

	public Class155(int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_) {
		aByte1242 = (byte) i;
		anIntArray1237 = new int[4];
		aByte1238 = (byte) i_3_;
		anIntArray1240 = new int[4];
		anIntArray1241 = new int[4];
		anIntArray1240[2] = i_6_;
		anIntArray1240[3] = i_7_;
		anIntArray1240[1] = i_5_;
		anIntArray1240[0] = i_4_;
		anIntArray1237[3] = i_11_;
		anIntArray1237[0] = i_8_;
		anIntArray1237[2] = i_10_;
		anIntArray1237[1] = i_9_;
		anIntArray1241[2] = i_14_;
		anIntArray1241[1] = i_13_;
		anIntArray1241[3] = i_15_;
		aShort1236 = (short) (i_4_ >> Class151_Sub8.tileScale);
		anIntArray1241[0] = i_12_;
		aShort1243 = (short) (i_6_ >> Class151_Sub8.tileScale);
		aShort1239 = (short) (i_12_ >> Class151_Sub8.tileScale);
		aShort1245 = (short) (i_14_ >> Class151_Sub8.tileScale);
		aShortArray1234 = new short[4];
		aShortArray1235 = new short[4];
		aShortArray1244 = new short[4];
	}

}

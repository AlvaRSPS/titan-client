
/* Class98_Sub43_Sub4 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Color;

import com.jagex.game.client.archive.Archive;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;
import com.jagex.game.input.RtKeyListener;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;

import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;

public final class Class98_Sub43_Sub4 extends Class98_Sub43 {
	public static float[]	aFloatArray5940		= new float[4];
	public static int		anInt5938;
	public static short		aShort5934			= 205;
	public static String[]	aStringArray5932	= new String[100];

	public static final void method1504(NPC class246_sub3_sub4_sub2_sub1, int i) {
		try {
			if (i != -16255) {
				method1508(-54, -22, null);
			}
			for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class358.aClass148_3032.getFirst(i + 16287); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class358.aClass148_3032.getNext(i ^ ~0x3f24)) {
				if (class98_sub42.npc == class246_sub3_sub4_sub2_sub1) {
					if (class98_sub42.aClass98_Sub31_Sub5_4232 != null) {
						Class81.aClass98_Sub31_Sub3_619.method1374(class98_sub42.aClass98_Sub31_Sub5_4232);
						class98_sub42.aClass98_Sub31_Sub5_4232 = null;
					}
					class98_sub42.unlink(116);
					break;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wda.D(" + (class246_sub3_sub4_sub2_sub1 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static void method1505(int i) {
		do {
			try {
				aFloatArray5940 = null;
				aStringArray5932 = null;
				if (i == 21237) {
					break;
				}
				aShort5934 = (short) -113;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wda.B(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method1506(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, ClipMap class243, int i_9_, int i_10_, int i_11_, int i_12_) {
		try {
			int i_13_ = i_6_;
			int i_14_ = i_12_;
			int i_15_ = 64;
			if (i_4_ != 14664) {
				method1504(null, -75);
			}
			int i_16_ = 64;
			int i_17_ = -i_15_ + i_6_;
			int i_18_ = i_12_ + -i_16_;
			PlayerUpdateMask.anIntArrayArray528[i_15_][i_16_] = 99;
			Archive.anIntArrayArray2846[i_15_][i_16_] = 0;
			int i_19_ = 0;
			int i_20_ = 0;
			Class359.anIntArray3060[i_19_] = i_13_;
			StartupStage.anIntArray580[i_19_++] = i_14_;
			int[][] is = class243.anIntArrayArray1853;
			while (i_19_ != i_20_) {
				i_13_ = Class359.anIntArray3060[i_20_];
				i_14_ = StartupStage.anIntArray580[i_20_];
				i_15_ = i_13_ - i_17_;
				i_16_ = -i_18_ + i_14_;
				int i_21_ = -class243.anInt1854 + i_13_;
				i_20_ = 1 + i_20_ & 0xfff;
				int i_22_ = i_14_ + -class243.anInt1855;
				int i_23_ = i;
				while_249_: do {
					while_248_: do {
						while_247_: do {
							while_246_: do {
								do {
									if ((i_23_ ^ 0xffffffff) != 3) {
										if ((i_23_ ^ 0xffffffff) != 2) {
											if (i_23_ != -2) {
												if (i_23_ != -1) {
													if ((i_23_ ^ 0xffffffff) == -1 || i_23_ == 1 || i_23_ == 2 || i_23_ == 3 || (i_23_ ^ 0xffffffff) == -10) {
														break while_247_;
													}
													break while_248_;
												}
											} else {
												break;
											}
											break while_246_;
										}
									} else {
										if ((i_8_ ^ 0xffffffff) == (i_13_ ^ 0xffffffff) && i_14_ == i_9_) {
											Class22.anInt217 = i_14_;
											FloorOverlayDefinition.anInt1539 = i_13_;
											return true;
										}
										break while_249_;
									}
									if (PointLight.method960(i_8_, i_11_, -105, i_14_, i_5_, i_13_, i_9_, 1, 1)) {
										FloorOverlayDefinition.anInt1539 = i_13_;
										Class22.anInt217 = i_14_;
										return true;
									}
									break while_249_;
								} while (false);
								if (class243.method2936(i_8_, i_5_, i_9_, -1, i_14_, i_11_, 1, i_13_, 1, i_7_)) {
									Class22.anInt217 = i_14_;
									FloorOverlayDefinition.anInt1539 = i_13_;
									return true;
								}
								break while_249_;
							} while (false);
							if (class243.method2939(i_11_, i_8_, i_14_, 14672, 1, i_9_, i_7_, i_13_, i_5_)) {
								Class22.anInt217 = i_14_;
								FloorOverlayDefinition.anInt1539 = i_13_;
								return true;
							}
							break while_249_;
						} while (false);
						if (class243.method2952(i_9_, i_13_, (byte) -110, 1, i_14_, i_10_, i_8_, i)) {
							Class22.anInt217 = i_14_;
							FloorOverlayDefinition.anInt1539 = i_13_;
							return true;
						}
						break while_249_;
					} while (false);
					if (class243.method2938(i_8_, i_10_, i_14_, i_9_, i, 17761, 1, i_13_)) {
						Class22.anInt217 = i_14_;
						FloorOverlayDefinition.anInt1539 = i_13_;
						return true;
					}
				} while (false);
				i_23_ = Archive.anIntArrayArray2846[i_15_][i_16_] + 1;
				if (i_15_ > 0 && (PlayerUpdateMask.anIntArrayArray528[i_15_ + -1][i_16_] ^ 0xffffffff) == -1 && (is[i_21_ + -1][i_22_] & 0x42240000 ^ 0xffffffff) == -1) {
					Class359.anIntArray3060[i_19_] = i_13_ - 1;
					StartupStage.anIntArray580[i_19_] = i_14_;
					PlayerUpdateMask.anIntArrayArray528[i_15_ - 1][i_16_] = 2;
					i_19_ = 0xfff & 1 + i_19_;
					Archive.anIntArrayArray2846[i_15_ + -1][i_16_] = i_23_;
				}
				if ((i_15_ ^ 0xffffffff) > -128 && PlayerUpdateMask.anIntArrayArray528[1 + i_15_][i_16_] == 0 && (0x60240000 & is[1 + i_21_][i_22_] ^ 0xffffffff) == -1) {
					Class359.anIntArray3060[i_19_] = 1 + i_13_;
					StartupStage.anIntArray580[i_19_] = i_14_;
					PlayerUpdateMask.anIntArrayArray528[1 + i_15_][i_16_] = 8;
					i_19_ = i_19_ + 1 & 0xfff;
					Archive.anIntArrayArray2846[1 + i_15_][i_16_] = i_23_;
				}
				if (i_16_ > 0 && (PlayerUpdateMask.anIntArrayArray528[i_15_][-1 + i_16_] ^ 0xffffffff) == -1 && (is[i_21_][-1 + i_22_] & 0x40a40000) == 0) {
					Class359.anIntArray3060[i_19_] = i_13_;
					StartupStage.anIntArray580[i_19_] = i_14_ - 1;
					PlayerUpdateMask.anIntArrayArray528[i_15_][i_16_ - 1] = 1;
					i_19_ = i_19_ + 1 & 0xfff;
					Archive.anIntArrayArray2846[i_15_][i_16_ + -1] = i_23_;
				}
				if (i_16_ < 127 && PlayerUpdateMask.anIntArrayArray528[i_15_][i_16_ - -1] == 0 && (is[i_21_][1 + i_22_] & 0x48240000 ^ 0xffffffff) == -1) {
					Class359.anIntArray3060[i_19_] = i_13_;
					StartupStage.anIntArray580[i_19_] = i_14_ - -1;
					i_19_ = i_19_ + 1 & 0xfff;
					PlayerUpdateMask.anIntArrayArray528[i_15_][i_16_ - -1] = 4;
					Archive.anIntArrayArray2846[i_15_][1 + i_16_] = i_23_;
				}
				if (i_15_ > 0 && (i_16_ ^ 0xffffffff) < -1 && (PlayerUpdateMask.anIntArrayArray528[-1 + i_15_][i_16_ - 1] ^ 0xffffffff) == -1 && (is[i_21_ + -1][i_22_ - 1] & 0x43a40000) == 0 && (is[-1 + i_21_][i_22_] & 0x42240000 ^ 0xffffffff) == -1 && (0x40a40000 & is[i_21_][-1 + i_22_]) == 0) {
					Class359.anIntArray3060[i_19_] = -1 + i_13_;
					StartupStage.anIntArray580[i_19_] = i_14_ + -1;
					i_19_ = 0xfff & 1 + i_19_;
					PlayerUpdateMask.anIntArrayArray528[-1 + i_15_][-1 + i_16_] = 3;
					Archive.anIntArrayArray2846[-1 + i_15_][-1 + i_16_] = i_23_;
				}
				if (i_15_ < 127 && i_16_ > 0 && PlayerUpdateMask.anIntArrayArray528[i_15_ + 1][-1 + i_16_] == 0 && (is[1 + i_21_][i_22_ - 1] & 0x60e40000 ^ 0xffffffff) == -1 && (is[1 + i_21_][i_22_] & 0x60240000) == 0 && (is[i_21_][i_22_ - 1] & 0x40a40000) == 0) {
					Class359.anIntArray3060[i_19_] = i_13_ - -1;
					StartupStage.anIntArray580[i_19_] = i_14_ - 1;
					i_19_ = 0xfff & 1 + i_19_;
					PlayerUpdateMask.anIntArrayArray528[i_15_ - -1][-1 + i_16_] = 9;
					Archive.anIntArrayArray2846[1 + i_15_][i_16_ + -1] = i_23_;
				}
				if ((i_15_ ^ 0xffffffff) < -1 && (i_16_ ^ 0xffffffff) > -128 && (PlayerUpdateMask.anIntArrayArray528[-1 + i_15_][i_16_ + 1] ^ 0xffffffff) == -1 && (is[-1 + i_21_][i_22_ + 1] & 0x4e240000) == 0 && (0x42240000 & is[i_21_ - 1][i_22_]) == 0 && (0x48240000 & is[i_21_][i_22_ + 1]) == 0) {
					Class359.anIntArray3060[i_19_] = -1 + i_13_;
					StartupStage.anIntArray580[i_19_] = i_14_ - -1;
					PlayerUpdateMask.anIntArrayArray528[-1 + i_15_][1 + i_16_] = 6;
					i_19_ = 1 + i_19_ & 0xfff;
					Archive.anIntArrayArray2846[i_15_ + -1][i_16_ + 1] = i_23_;
				}
				if ((i_15_ ^ 0xffffffff) > -128 && (i_16_ ^ 0xffffffff) > -128 && (PlayerUpdateMask.anIntArrayArray528[i_15_ - -1][i_16_ + 1] ^ 0xffffffff) == -1 && (0x78240000 & is[i_21_ + 1][1 + i_22_]) == 0 && (0x60240000 & is[1 + i_21_][i_22_]) == 0 && (0x48240000 & is[i_21_][1 + i_22_]) == 0) {
					Class359.anIntArray3060[i_19_] = 1 + i_13_;
					StartupStage.anIntArray580[i_19_] = i_14_ + 1;
					PlayerUpdateMask.anIntArrayArray528[i_15_ - -1][i_16_ - -1] = 12;
					i_19_ = 0xfff & i_19_ + 1;
					Archive.anIntArrayArray2846[1 + i_15_][1 + i_16_] = i_23_;
				}
			}
			FloorOverlayDefinition.anInt1539 = i_13_;
			Class22.anInt217 = i_14_;
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wda.A(" + i + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + (class243 != null ? "{...}" : "null") + ',' + i_9_ + ',' + i_10_ + ',' + i_11_ + ',' + i_12_ + ')');
		}
	}

	public static final int method1508(int i, int i_24_, String string) {
		try {
			if (i != 1) {
				aStringArray5932 = null;
			}
			return StoreProgressMonitor.method2859(i_24_, true, string, i ^ ~0x55d2);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wda.K(" + i + ',' + i_24_ + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	public static final void method1510(int i) {
		try {
			client.keyListener.method773((byte) -30);
			if (i != 28837) {
				anInt5938 = -111;
			}
			client.mouseListener.method3515(-119);
			client.activeClient.addCanvas(i + -28837);
			GameShell.canvas.setBackground(Color.black);
			Orientation.anInt2729 = -1;
			client.keyListener = RtKeyListener.create((byte) 10, GameShell.canvas);
			client.mouseListener = RtMouseListener.create(GameShell.canvas, true, -16777216);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wda.I(" + i + ')');
		}
	}

	private float	aFloat5935;
	private float	aFloat5939;

	private int		anInt5930;

	private int		anInt5936;

	private String	aString5931;

	private String	aString5933;

	private String	aString5937;

	Class98_Sub43_Sub4(OggStreamState oggstreamstate) {
		super(oggstreamstate);
	}

	@Override
	public final void method1482(OggPacket oggpacket, boolean bool) {
		do {
			try {
				if (this.anInt4240 <= 0 || "SUB".equals(aString5931)) {
					RSByteBuffer class98_sub22 = new RSByteBuffer(oggpacket.getData());
					if (bool != false) {
						method1510(-72);
					}
					int i = class98_sub22.readUnsignedByte((byte) 27);
					if (this.anInt4240 > 8) {
						if (i == 0) {
							long l = class98_sub22.method1189((byte) -53);
							long l_0_ = class98_sub22.method1189((byte) -63);
							long l_1_ = class98_sub22.method1189((byte) -110);
							if (l < 0L || (l_0_ ^ 0xffffffffffffffffL) > -1L || l_1_ < 0L || (l_1_ ^ 0xffffffffffffffffL) < (l ^ 0xffffffffffffffffL)) {
								throw new IllegalStateException();
							}
							aFloat5939 = (float) (anInt5936 * l) / (float) anInt5930;
							aFloat5935 = (float) (anInt5936 * (l - -l_0_)) / (float) anInt5930;
							int i_2_ = class98_sub22.method1202((byte) -75);
							if ((i_2_ ^ 0xffffffff) > -1 || i_2_ > class98_sub22.payload.length - class98_sub22.position) {
								throw new IllegalStateException();
							}
							aString5937 = SoftwareNativeHeap.method1679(class98_sub22.position, i_2_, (byte) -51, class98_sub22.payload);
						}
						if ((0x80 | i) != 0) {
							break;
						}
					} else {
						if (((i | 0x80) ^ 0xffffffff) == -1) {
							throw new IllegalStateException();
						}
						if ((this.anInt4240 ^ 0xffffffff) != -1) {
							break;
						}
						class98_sub22.position += 23;
						anInt5930 = class98_sub22.method1202((byte) -51);
						anInt5936 = class98_sub22.method1202((byte) -108);
						if (anInt5930 == 0 || anInt5936 == 0) {
							throw new IllegalStateException();
						}
						RSByteBuffer class98_sub22_3_ = new RSByteBuffer(16);
						class98_sub22.getData(class98_sub22_3_.payload, true, 16, 0);
						aString5933 = class98_sub22_3_.readString((byte) 84);
						class98_sub22_3_.position = 0;
						class98_sub22.getData(class98_sub22_3_.payload, !bool, 16, 0);
						aString5931 = class98_sub22_3_.readString((byte) 84);
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wda.J(" + (oggpacket != null ? "{...}" : "null") + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method1487(int i) {
		do {
			try {
				if (i == -1128) {
					break;
				}
				method1510(33);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wda.C(" + i + ')');
			}
			break;
		} while (false);
	}

	public final String method1503(int i) {
		try {
			if (i != 22875) {
				anInt5936 = 92;
			}
			return aString5937;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wda.G(" + i + ')');
		}
	}

	public final String method1507(boolean bool) {
		try {
			if (bool != true) {
				return null;
			}
			return aString5933;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wda.E(" + bool + ')');
		}
	}

	public final float method1509(int i) {
		try {
			if (i != -6085) {
				return 2.7172983F;
			}
			return aFloat5935;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wda.F(" + i + ')');
		}
	}

	public final float method1511(int i) {
		try {
			if (i >= -38) {
				aShort5934 = (short) 29;
			}
			return aFloat5939;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wda.H(" + i + ')');
		}
	}
}

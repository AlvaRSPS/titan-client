
/* Class98_Sub5_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.quickchat.QuickChatMessageType;

public final class Class98_Sub5_Sub1 extends PointLight {
	public static QuickChatMessageType aClass348_5534 = new QuickChatMessageType(14, 0, 4, 1);

	public static final int method964(Class162 class162, byte i) {
		try {
			if (i != 105) {
				return -27;
			}
			if (class162 != Class162.aClass162_1262) {
				if (Class162.aClass162_1264 != class162) {
					if (Class162.aClass162_1265 != class162) {
						if (Class162.aClass162_1266 == class162) {
							return 5121;
						}
						if (class162 == Class162.aClass162_1267) {
							return 5123;
						}
						if (Class162.aClass162_1268 != class162) {
							if (class162 == Class162.aClass162_1269) {
								return 5131;
							}
							if (Class162.aClass162_1270 == class162) {
								return 5126;
							}
						} else {
							return 5125;
						}
					} else {
						return 5124;
					}
				} else {
					return 5122;
				}
			} else {
				return 5120;
			}
			throw new IllegalArgumentException("");
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fi.K(" + (class162 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static final boolean method965(byte i, int i_0_) {
		try {
			return !(i_0_ != 3 && i_0_ != 7 && i_0_ != 10);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fi.N(" + i + ',' + i_0_ + ')');
		}
	}

	public static final boolean method966(int i, boolean bool) {
		try {
			boolean bool_2_ = client.graphicsToolkit.canEnableBloom();
			if (bool != !bool_2_) {
				return true;
			}
			do {
				if (bool) {
					if (client.graphicsToolkit.method1802()) {
						break;
					}
					bool = false;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				client.graphicsToolkit.method1756();
			} while (false);
			if (bool == !bool_2_) {
				client.preferences.setPreference((byte) -13, !bool ? 0 : 1, client.preferences.unknown3);
				Class310.method3618(-5964);
				return true;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fi.L(" + i + ',' + bool + ')');
		}
	}

	public static void method967(byte i) {
		try {
			if (i >= 16) {
				aClass348_5534 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fi.M(" + i + ')');
		}
	}

	Class98_Sub5_Sub1(int i, int i_6_, int i_7_, int i_8_, int i_9_, float f) {
		super(i, i_6_, i_7_, i_8_, i_9_, f);
	}

	@Override
	public final void reposition(int i, byte i_3_, int i_4_, int i_5_) {
		try {
			this._x = i_4_;
			this._z = i;
			if (i_3_ > -120) {
				aClass348_5534 = null;
			}
			this._y = i_5_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fi.A(" + i + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ')');
		}
	}

	@Override
	public final void setIntensity(float f, int i) {
		do {
			try {
				this.intensity = f;
				if (i > 12) {
					break;
				}
				reposition(55, (byte) 80, -52, 106);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fi.D(" + f + ',' + i + ')');
			}
			break;
		} while (false);
	}
}

/* Class218 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.quickchat.QuickChatMessageType;

public final class SceneGraphNodeList {
	public static QuickChatMessageType		aClass348_1630;
	public static int						anInt1635;
	public static int[]						anIntArray1631	= new int[14];
	public static String					optionText;
	public static QuickChatCategoryParser	quickChatCategoryList;

	static {
		aClass348_1630 = new QuickChatMessageType(12, 0, 1, 0);
		anInt1635 = 100;
	}

	public static final void addBefore(SceneGraphNode entry, SceneGraphNode secondEntry, byte i) {
		if (entry.previous != null) {
			entry.unlink((byte) 127);
		}
		entry.previous = secondEntry.previous;
		entry.next = secondEntry;
		entry.previous.next = entry;
		entry.next.previous = entry;
	}

	public static void method2804(byte i) {
		optionText = null;
		aClass348_1630 = null;
		quickChatCategoryList = null;
		anIntArray1631 = null;
	}

	public static final void method2806(int i, int i_2_, boolean bool) {
		if (bool != true) {
			method2806(-21, -126, true);
		}
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i, -104, 5);
		class98_sub46_sub17.method1626((byte) -103);
		class98_sub46_sub17.anInt6054 = i_2_;
	}

	public static final void method2807(int i, int i_3_, int i_4_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_3_][i_4_];
		if (class172 != null) {
			PacketBufferManager.method2227(class172.aClass246_Sub3_Sub5_1334);
			PacketBufferManager.method2227(class172.aClass246_Sub3_Sub5_1326);
			if (class172.aClass246_Sub3_Sub5_1334 != null) {
				class172.aClass246_Sub3_Sub5_1334 = null;
			}
			if (class172.aClass246_Sub3_Sub5_1326 != null) {
				class172.aClass246_Sub3_Sub5_1326 = null;
			}
		}
	}

	private SceneGraphNode	current;

	private SceneGraphNode	head	= new SceneGraphNode();

	public SceneGraphNodeList() {
		head.next = head;
		head.previous = head;
	}

	public final void addLast(boolean bool, SceneGraphNode entry) {
		if (entry.previous != null) {
			entry.unlink((byte) 127);
		}
		entry.previous = head.previous;
		entry.next = head;
		entry.previous.next = entry;
		entry.next.previous = entry;
	}

	public final void clear(int i) {
		for (;;) {
			SceneGraphNode entry = head.next;
			if (head == entry) {
				break;
			}
			entry.unlink((byte) -2);
		}
		current = null;
	}

	public final SceneGraphNode getFirst(byte i) {
		SceneGraphNode entry = head.next;
		if (entry == head) {
			current = null;
			return null;
		}
		current = entry.next;
		return entry;
	}

	public final SceneGraphNode getLast(byte i) {
		SceneGraphNode entry = head.previous;
		if (head == entry) {
			current = null;
			return null;
		}
		current = entry.previous;
		return entry;
	}

	public final SceneGraphNode getNext(boolean bool) {
		SceneGraphNode entry = current;
		if (head == entry) {
			current = null;
			return null;
		}
		current = entry.next;
		return entry;
	}

	public final boolean isEmpty(boolean bool) {
		return head.next == head;
	}

	public final SceneGraphNode removeFirst(byte i) {
		SceneGraphNode entry = head.next;
		if (head == entry) {
			return null;
		}
		entry.unlink((byte) -63);
		return entry;
	}

	public final int size(int i) {
		int count = 0;
		SceneGraphNode entry = head.next;
		while (entry != head) {
			entry = entry.next;
			count++;
		}
		return count;
	}
}

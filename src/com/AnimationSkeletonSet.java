
/* Class98_Sub46_Sub16 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.preferences.SafeModePreferenceField;

import jaggl.OpenGL;

public final class AnimationSkeletonSet extends Cacheable {
	public static AdvancedMemoryCache	aClass79_6046	= new AdvancedMemoryCache(30);
	public static int					anInt6044;

	public static final void method1613(int i, int i_0_, byte[] is, int i_1_, int i_2_, int i_3_, int i_4_) {
		if ((i ^ 0xffffffff) < i_1_ && !Class81.method815(i, 0)) {
			throw new IllegalArgumentException("");
		}
		if ((i_2_ ^ 0xffffffff) < -1 && !Class81.method815(i_2_, 0)) {
			throw new IllegalArgumentException("");
		}
		int i_5_ = Class246_Sub3_Sub3.method3014(1, i_4_);
		int i_6_ = 0;
		int i_7_ = (i ^ 0xffffffff) <= (i_2_ ^ 0xffffffff) ? i_2_ : i;
		int i_8_ = i >> 1044223969;
		int i_9_ = i_2_ >> -694038175;
		byte[] is_10_ = is;
		byte[] is_11_ = new byte[i_5_ * i_9_ * i_8_];
		for (;;) {
			OpenGL.glTexImage2Dub(i_0_, i_6_, i_3_, i, i_2_, 0, i_4_, 5121, is_10_, 0);
			if ((i_7_ ^ 0xffffffff) >= -2) {
				break;
			}
			int i_12_ = i_5_ * i;
			for (int i_13_ = 0; i_13_ < i_5_; i_13_++) {
				int i_14_ = i_13_;
				int i_15_ = i_13_;
				int i_16_ = i_15_ - -i_12_;
				for (int i_17_ = 0; (i_17_ ^ 0xffffffff) > (i_9_ ^ 0xffffffff); i_17_++) {
					for (int i_18_ = 0; (i_8_ ^ 0xffffffff) < (i_18_ ^ 0xffffffff); i_18_++) {
						int i_19_ = is_10_[i_15_];
						i_15_ += i_5_;
						i_19_ += is_10_[i_15_];
						i_15_ += i_5_;
						i_19_ += is_10_[i_16_];
						i_16_ += i_5_;
						i_19_ += is_10_[i_16_];
						i_16_ += i_5_;
						is_11_[i_14_] = (byte) (i_19_ >> 1943518018);
						i_14_ += i_5_;
					}
					i_15_ += i_12_;
					i_16_ += i_12_;
				}
			}
			byte[] is_20_ = is_11_;
			is_11_ = is_10_;
			i = i_8_;
			is_10_ = is_20_;
			i_2_ = i_9_;
			i_9_ >>= 1;
			i_7_ >>= 1;
			i_6_++;
			i_8_ >>= 1;
		}
	}

	public static final void method1618(int i, byte i_30_) {
		Class76_Sub10.anInt3794 = i;
		if (i_30_ == -85) {
			Class64_Sub5.aClass79_3650.clear(105);
		}
	}

	private byte[][]	aByteArrayArray6048;

	public Class7[]		aClass7Array6045;

	private int			anInt6047;

	public AnimationSkeletonSet(int i) {
		anInt6047 = i;
	}

	public final boolean method1614(byte i) {
		if (aClass7Array6045 != null) {
			return true;
		}
		if (aByteArrayArray6048 == null) {
			synchronized (RemoveRoofsPreferenceField.aClass207_3679) {
				if (!RemoveRoofsPreferenceField.aClass207_3679.isGroupCached(false, anInt6047)) {
					return false;
				}
				int[] is = RemoveRoofsPreferenceField.aClass207_3679.method2743(anInt6047, 6341);
				aByteArrayArray6048 = new byte[is.length][];
				for (int i_21_ = 0; i_21_ < is.length; i_21_++) {
					aByteArrayArray6048[i_21_] = RemoveRoofsPreferenceField.aClass207_3679.getFile(is[i_21_], anInt6047, false);
				}
			}
		}
		boolean bool = true;
		for (int i_22_ = 0; (aByteArrayArray6048.length ^ 0xffffffff) < (i_22_ ^ 0xffffffff); i_22_++) {
			byte[] is = aByteArrayArray6048[i_22_];
			RSByteBuffer class98_sub22 = new RSByteBuffer(is);
			class98_sub22.position = 1;
			int i_23_ = class98_sub22.readShort((byte) 127);
			synchronized (SafeModePreferenceField.aClass207_3644) {
				bool &= SafeModePreferenceField.aClass207_3644.isFileCached(-89, i_23_);
			}
		}
		if (!bool) {
			return false;
		}
		LinkedList class148 = new LinkedList();
		int[] is;
		synchronized (RemoveRoofsPreferenceField.aClass207_3679) {
			int i_24_ = RemoveRoofsPreferenceField.aClass207_3679.getFileCount(0, anInt6047);
			aClass7Array6045 = new Class7[i_24_];
			is = RemoveRoofsPreferenceField.aClass207_3679.method2743(anInt6047, 6341);
		}
		for (int i_25_ = 0; i_25_ < is.length; i_25_++) {
			byte[] is_26_ = aByteArrayArray6048[i_25_];
			RSByteBuffer class98_sub22 = new RSByteBuffer(is_26_);
			class98_sub22.position = 1;
			int i_27_ = class98_sub22.readShort((byte) 127);
			Class98_Sub1 class98_sub1 = null;
			for (Class98_Sub1 class98_sub1_28_ = (Class98_Sub1) class148.getFirst(32); class98_sub1_28_ != null; class98_sub1_28_ = (Class98_Sub1) class148.getNext(92)) {
				if (i_27_ == class98_sub1_28_.anInt3813) {
					class98_sub1 = class98_sub1_28_;
					break;
				}
			}
			if (class98_sub1 == null) {
				synchronized (SafeModePreferenceField.aClass207_3644) {
					class98_sub1 = new Class98_Sub1(i_27_, SafeModePreferenceField.aClass207_3644.getFile(i_27_, -118));
				}
				class148.addLast(class98_sub1, -20911);
			}
			aClass7Array6045[is[i_25_]] = new Class7(is_26_, class98_sub1);
		}
		aByteArrayArray6048 = null;
		return true;
	}

	public final boolean method1615(int i, boolean bool) {
		return aClass7Array6045[i].aBoolean95;
	}

	public final boolean method1617(boolean bool, int i) {
		return aClass7Array6045[i].aBoolean102;
	}

	public final boolean method1619(int i, int i_31_) {
		return aClass7Array6045[i].aBoolean104;
	}
}

/* Class246_Sub3_Sub5_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class246_Sub3_Sub5_Sub1 extends Class246_Sub3_Sub5 implements Interface19 {
	public static OutgoingOpcode aClass171_6221 = new OutgoingOpcode(6, 8);

	public static void method3090(byte i) {
		do {
			try {
				aClass171_6221 = null;
				if (i == -94) {
					break;
				}
				method3090((byte) 106);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ce.D(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method3092(int i, boolean bool) {
		do {
			try {
				if (i != -1) {
					method3090((byte) 63);
				}
				OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class15.aClass171_183, Class331.aClass117_2811);
				Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
				for (RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(94); class98_sub18 != null; class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.iterateNext(i)) {
					if (!class98_sub18.isLinked((byte) 78)) {
						class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.startIteration(95);
						if (class98_sub18 == null) {
							break;
						}
					}
					if ((class98_sub18.anInt3947 ^ 0xffffffff) == -1) {
						RtInterfaceAttachment.detachInterface(16398, bool, class98_sub18, true);
					}
				}
				if (DummyOutputStream.rtInterface == null) {
					break;
				}
				WorldMapInfoDefinitionParser.setDirty(1, DummyOutputStream.rtInterface);
				DummyOutputStream.rtInterface = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ce.F(" + i + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	private boolean		aBoolean6218	= false;
	private boolean		aBoolean6220;

	private Class228	aClass228_6217;

	Class359			aClass359_6219;

	Class246_Sub3_Sub5_Sub1(RSToolkit var_ha, GameObjectDefinition class352, int i, int i_7_, int i_8_, int i_9_, int i_10_, boolean bool, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_) {
		super(i_8_, i_9_, i_10_, i, i_7_, i_11_, i_12_);
		try {
			aClass359_6219 = new Class359(var_ha, class352, i_13_, i_14_, ((Char) this).plane, i_7_, this, bool, i_15_);
			aBoolean6220 = class352.anInt2998 != 0 && !bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.<init>(" + (var_ha != null ? "{...}" : "null") + ',' + (class352 != null ? "{...}" : "null") + ',' + i + ',' + i_7_ + ',' + i_8_ + ',' + i_9_ + ',' + i_10_ + ',' + bool + ',' + i_11_ + ',' + i_12_ + ',' + i_13_ + ',' + i_14_
					+ ',' + i_15_ + ')');
		}
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (i != -53) {
				return null;
			}
			return aClass228_6217;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = aClass359_6219.method3897(-1, false, 2048, var_ha, true);
			if (class146 == null) {
				return null;
			}
			Matrix class111 = var_ha.method1793();
			if (i >= -12) {
				aBoolean6220 = true;
			}
			class111.method2100(((Class246_Sub3_Sub5) this).aShort6165 + ((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ + ((Class246_Sub3_Sub5) this).aShort6163);
			Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, aBoolean6220);
			int i_0_ = ((Char) this).boundExtentsX >> -644618167;
			int i_1_ = ((Char) this).boundExtentsZ >> -811550487;
			aClass359_6219.method3895(class146, i_0_, i_1_, class111, true, i_0_, var_ha, false, i_1_);
			if (VarClientStringsDefinitionParser.aBoolean1839) {
				class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
			} else {
				class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
			}
			if (aClass359_6219.aClass246_Sub5_3062 != null) {
				Class242 class242 = aClass359_6219.aClass246_Sub5_3062.method3116();
				if (VarClientStringsDefinitionParser.aBoolean1839) {
					var_ha.method1785(class242, Class16.anInt197);
				} else {
					var_ha.method1820(class242);
				}
			}
			aBoolean6218 = class146.F() || aClass359_6219.aClass246_Sub5_3062 != null;
			if (aClass228_6217 != null) {
				Class283.method3350(((Char) this).anInt5089, ((Char) this).boundExtentsX, 18, ((Char) this).boundExtentsZ, class146, aClass228_6217);
			} else {
				aClass228_6217 = Class48_Sub2_Sub1.method472(((Char) this).anInt5089, ((Char) this).boundExtentsX, class146, ((Char) this).boundExtentsZ, 4);
			}
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_5_, int i_6_) {
		try {
			ModelRenderer class146 = aClass359_6219.method3897(-1, false, 131072, var_ha, false);
			if (class146 == null) {
				return false;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(((Char) this).boundExtentsX + ((Class246_Sub3_Sub5) this).aShort6165, ((Char) this).anInt5089, ((Char) this).boundExtentsZ + ((Class246_Sub3_Sub5) this).aShort6163);
			if (i_5_ < 59) {
				method64(111);
			}
			if (!VarClientStringsDefinitionParser.aBoolean1839) {
				return class146.method2339(i, i_6_, class111, false, 0);
			}
			return class146.method2333(i, i_6_, class111, false, 0, Class16.anInt197);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_5_ + ',' + i_6_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.H(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				method2978(-53);
			}
			return aClass359_6219.method3903((byte) -127);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (i != 6540) {
				aBoolean6218 = false;
			}
			return aBoolean6218;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = aClass359_6219.method3897(-1, false, 262144, var_ha, true);
			if (class146 != null) {
				int i_2_ = ((Char) this).boundExtentsX >> -2058956119;
				int i_3_ = ((Char) this).boundExtentsZ >> -1110738775;
				Matrix class111 = var_ha.method1793();
				class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
				aClass359_6219.method3895(class146, i_2_, i_3_, class111, false, i_2_, var_ha, false, i_3_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				method66(-34);
			}
			return aClass359_6219.method3899((byte) 126);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.J(" + i + ')');
		}
	}

	public final void method3091(Class185 class185, boolean bool) {
		do {
			try {
				aClass359_6219.method3901(class185, -108);
				if (bool == false) {
					break;
				}
				aBoolean6218 = true;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ce.K(" + (class185 != null ? "{...}" : "null") + ',' + bool + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method61(byte i) {
		try {
			if (i != -96) {
				method2988(null, 0);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.Q(" + i + ')');
		}
	}

	@Override
	public final void method62(RSToolkit var_ha, int i) {
		try {
			if (i != 24447) {
				method64(106);
			}
			aClass359_6219.method3892(var_ha, 103);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.G(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method63(byte i) {
		try {
			if (i != 20) {
				method2978(-38);
			}
			return aClass359_6219.anInt3038;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.B(" + i + ')');
		}
	}

	@Override
	public final int method64(int i) {
		try {
			if (i != 30472) {
				method2978(119);
			}
			return aClass359_6219.anInt3052;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.C(" + i + ')');
		}
	}

	@Override
	public final boolean method65(boolean bool) {
		try {
			if (bool != true) {
				return false;
			}
			return aClass359_6219.method3898(99);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.A(" + bool + ')');
		}
	}

	@Override
	public final int method66(int i) {
		try {
			if (i != 4657) {
				aClass171_6221 = null;
			}
			return aClass359_6219.anInt3059;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.N(" + i + ')');
		}
	}

	@Override
	public final void method67(int i, RSToolkit var_ha) {
		try {
			aClass359_6219.method3894((byte) 125, var_ha);
			if (i != -25163) {
				method2974((byte) 25, null);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ce.E(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}
}

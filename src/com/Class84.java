/* Class84 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;

public final class Class84 {
	public static String[] aStringArray636;

	public static final void loadFontIds(Js5 js5, int i) {
		HitmarksDefinitionParser.p11FullIndex = js5.getGroupId((byte) -62, "p11_full");
		HitmarksDefinitionParser.p12FullIndex = js5.getGroupId((byte) -122, "p12_full");
		HitmarksDefinitionParser.b12FullIndex = js5.getGroupId((byte) -91, "b12_full");
	}

	public static final void method832(String string, byte i) {
		ItemDeque.addChatMessage((byte) 52, 0, string, 0, "", "", "");
	}

	public boolean				aBoolean637		= false;

	public SceneGraphNodeList	aClass218_635	= new SceneGraphNodeList();

	public Class84(boolean bool) {
		aBoolean637 = bool;
	}

	public final void method833(int i) {
		for (;;) {
			Class246_Sub1 class246_sub1 = (Class246_Sub1) aClass218_635.removeFirst((byte) -72);
			if (class246_sub1 == null) {
				break;
			}
			class246_sub1.unlink((byte) -96);
			Class35.method333(class246_sub1, i ^ ~0x75);
		}
	}

	public final void method836(int i, Class246_Sub1 class246_sub1) {
		Char class246_sub3 = class246_sub1.aClass246_Sub3_5069;
		boolean bool = true;
		Class246_Sub6[] class246_sub6s = class246_sub1.aClass246_Sub6Array5067;
		for (int i_2_ = i; class246_sub6s.length > i_2_; i_2_++) {
			if (class246_sub6s[i_2_].aBoolean5114) {
				bool = false;
				break;
			}
		}
		if (!bool) {
			if (aBoolean637) {
				for (Class246_Sub1 class246_sub1_3_ = (Class246_Sub1) aClass218_635.getFirst((byte) 15); class246_sub1_3_ != null; class246_sub1_3_ = (Class246_Sub1) aClass218_635.getNext(false)) {
					if (class246_sub3 == class246_sub1_3_.aClass246_Sub3_5069) {
						class246_sub1_3_.unlink((byte) 18);
						Class35.method333(class246_sub1_3_, -120);
					}
				}
			}
			for (Class246_Sub1 class246_sub1_4_ = (Class246_Sub1) aClass218_635.getFirst((byte) 15); class246_sub1_4_ != null; class246_sub1_4_ = (Class246_Sub1) aClass218_635.getNext(false)) {
				if (class246_sub1_4_.aClass246_Sub3_5069.anInt5083 <= class246_sub3.anInt5083) {
					SceneGraphNodeList.addBefore(class246_sub1, class246_sub1_4_, (byte) 27);
					return;
				}
			}
			aClass218_635.addLast(true, class246_sub1);
		}
	}
}

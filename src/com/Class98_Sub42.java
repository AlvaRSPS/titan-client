/* Class98_Sub42 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;
import com.jagex.game.client.preferences.IdleAnimationsPreferenceField;

public final class Class98_Sub42 extends Node {
	public static boolean			aBoolean4218	= false;
	public static OutgoingOpcode	aClass171_4235	= new OutgoingOpcode(65, 2);
	public static IncomingOpcode	aClass58_4222	= new IncomingOpcode(42, 6);
	public static float				aFloat4234;
	public static long				aLong4212;
	public static long				aLong4238		= (long) (9.999999999E9 * Math.random());
	public static int				anInt4239;

	public static final void method1476(int i, int i_0_) {
		do {
			Class98_Sub36 class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.get(i_0_, -1);
			if (class98_sub36 == null) {
				break;
			}
			class98_sub36.aBoolean4153 = !class98_sub36.aBoolean4153;
			class98_sub36.aClass237_Sub1_4157.method2911(class98_sub36.aBoolean4153, (byte) 22);
			break;
		} while (false);
	}

	public static final void method1477(boolean bool) {
		do {
			if (Class241.anObject1847 == null) {
				Class284_Sub2_Sub2 class284_sub2_sub2 = new Class284_Sub2_Sub2();
				byte[] is = class284_sub2_sub2.method3379(16, 20283, 128, 128);
				Class241.anObject1847 = GroundBlendingPreferenceField.method654(2, is, false);
			}
			if (bool == false) {
				if (IdleAnimationsPreferenceField.anObject3709 != null) {
					break;
				}
				Class284_Sub1_Sub2 class284_sub1_sub2 = new Class284_Sub1_Sub2();
				byte[] is = class284_sub1_sub2.method3372(!bool, 128, 16, 128);
				IdleAnimationsPreferenceField.anObject3709 = GroundBlendingPreferenceField.method654(2, is, false);
			}
			break;
		} while (false);
	}

	public static final boolean contains(String string, int i) {
		return AdvancedMemoryCache.libraryClasses.containsKey(string);
	}

	public static final void method3446(int i) {
		for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub37.aClass148_5748.getFirst(32); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class98_Sub10_Sub37.aClass148_5748.getNext(88)) {
			if (class98_sub42.aBoolean4207) {
				class98_sub42.method1478(true);
			}
		}
		for (Class98_Sub42 class98_sub42 = (Class98_Sub42) Class358.aClass148_3032.getFirst(32); class98_sub42 != null; class98_sub42 = (Class98_Sub42) Class358.aClass148_3032.getNext(103)) {
			if (class98_sub42.aBoolean4207) {
				class98_sub42.method1478(true);
			}
		}
	}

	public boolean				aBoolean4207;
	public boolean				aBoolean4215;
	public boolean				aBoolean4226;
	public GameObjectDefinition	aClass352_4233;
	public Class98_Sub13		aClass98_Sub13_4213;
	public Class98_Sub13		aClass98_Sub13_4231;
	public Class98_Sub24_Sub1	aClass98_Sub24_Sub1_4211;
	public Class98_Sub24_Sub1	aClass98_Sub24_Sub1_4214;
	public Class98_Sub31_Sub5	aClass98_Sub31_Sub5_4230;
	public Class98_Sub31_Sub5	aClass98_Sub31_Sub5_4232;
	public int					anInt4205;
	public int					anInt4210;
	public int					anInt4216;
	public int					anInt4217;
	public int					anInt4219;
	public int					anInt4220;
	public int					anInt4221;
	public int					anInt4223;
	public int					anInt4224;
	public int					anInt4225;
	public int					anInt4227	= 0;
	public int					anInt4228;

	public int					anInt4229;

	public int					anInt4236;

	public int					anInt4237;

	public int[]				anIntArray4208;

	public NPC					npc;

	public Player				player;

	public Class98_Sub42() {
		/* empty */
	}

	public final void method1478(boolean bool) {
		do {
			try {
				if (bool != true) {
					anInt4228 = 82;
				}
				int i = anInt4210;
				boolean bool_1_ = aBoolean4215;
				if (aClass352_4233 != null) {
					GameObjectDefinition class352 = aClass352_4233.get(StartupStage.varValues, (byte) -96);
					if (class352 != null) {
						anInt4223 = class352.anInt3006;
						anInt4219 = class352.anInt2949;
						aBoolean4226 = class352.aBoolean2957;
						anInt4205 = class352.anInt2972;
						anInt4236 = class352.anInt2987;
						anInt4210 = class352.anInt2996;
						aBoolean4215 = class352.aBoolean2992;
						anIntArray4208 = class352.anIntArray2926;
						anInt4237 = class352.anInt2950;
						anInt4228 = class352.anInt2981 << -1567841911;
					} else {
						aBoolean4215 = false;
						anInt4237 = 256;
						anInt4228 = 0;
						anIntArray4208 = null;
						anInt4223 = 256;
						anInt4217 = 0;
						anInt4205 = 0;
						anInt4219 = 0;
						aBoolean4226 = false;
						anInt4236 = 0;
						anInt4210 = -1;
					}
				} else if (npc == null) {
					if (player != null) {
						anInt4210 = VertexNormal.method3383(player, bool);
						anInt4223 = 256;
						anInt4217 = 0;
						anInt4228 = player.anInt6525 << 1762065833;
						aBoolean4215 = player.hasDisplayName;
						anInt4236 = player.anInt6514;
						anInt4237 = 256;
					}
				} else {
					int i_2_ = Class277.method3293(125, npc);
					if (i != i_2_) {
						anInt4210 = i_2_;
						NPCDefinition class141 = npc.definition;
						if (class141.anIntArray1109 != null) {
							class141 = class141.method2300(StartupStage.varValues, (byte) 79);
						}
						if (class141 != null) {
							anInt4236 = class141.anInt1156;
							anInt4223 = class141.anInt1101;
							anInt4237 = class141.anInt1090;
							aBoolean4215 = class141.aBoolean1093;
							anInt4217 = class141.anInt1125 << 1720816489;
							anInt4228 = class141.anInt1128 << 958920233;
						} else {
							anInt4236 = anInt4228 = anInt4217 = 0;
							aBoolean4215 = npc.definition.aBoolean1093;
							anInt4237 = 256;
							anInt4223 = 256;
						}
					}
				}
				if ((i ^ 0xffffffff) != (anInt4210 ^ 0xffffffff) || aBoolean4215 == !bool_1_) {
					if (aClass98_Sub31_Sub5_4232 == null) {
						break;
					}
					Class81.aClass98_Sub31_Sub3_619.method1374(aClass98_Sub31_Sub5_4232);
					aClass98_Sub13_4213 = null;
					aClass98_Sub31_Sub5_4232 = null;
					aClass98_Sub24_Sub1_4214 = null;
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qfa.A(" + bool + ')');
			}
			break;
		} while (false);
	}
}

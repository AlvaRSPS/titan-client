/* Class17 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.clanchat.ClanChatMember;
import com.jagex.game.client.definition.parser.BConfigDefinitionParser;

public final class DataFs {
	public static OutgoingOpcode			aClass171_202	= new OutgoingOpcode(82, 8);
	public static int						anInt203;
	public static BConfigDefinitionParser	bConfigDefinitionList;

	public static byte[]					ioBuffer		= new byte[520];

	public static final void drawOptionWindow(int height, RSToolkit toolkit, int width, int i_12_, byte i_13_, int x, int y, String string) {
		if (NPC.aClass332_6508 == null || QuickChat.aClass332_2500 == null) {
			if (!client.spriteJs5.isFileCached(-71, Class38.anInt360) || !client.spriteJs5.isFileCached(-48, LoginOpcode.anInt1672)) {
				toolkit.fillRectangle(x, y, width, height, -Class355.anInt3017 + 255 << 544186680 | ProceduralTextureSource.anInt3261, 1);
			} else {
				NPC.aClass332_6508 = toolkit.createSprite(Image.loadImage(client.spriteJs5, Class38.anInt360, 0), true);
				Image sprite = Image.loadImage(client.spriteJs5, LoginOpcode.anInt1672, 0);
				QuickChat.aClass332_2500 = toolkit.createSprite(sprite, true);
				sprite.flipHorizontal();
				Class76_Sub11.aClass332_3795 = toolkit.createSprite(sprite, true);
			}
		}
		if (NPC.aClass332_6508 != null && QuickChat.aClass332_2500 != null) {
			int i_16_ = (width - QuickChat.aClass332_2500.getWidth() * 2) / NPC.aClass332_6508.getWidth();
			for (int i_17_ = 0; (i_16_ ^ 0xffffffff) < (i_17_ ^ 0xffffffff); i_17_++) {
				NPC.aClass332_6508.draw(QuickChat.aClass332_2500.getWidth() + x + i_17_ * NPC.aClass332_6508.getWidth(), y);
			}
			QuickChat.aClass332_2500.draw(x, y);
			Class76_Sub11.aClass332_3795.draw(x + width - Class76_Sub11.aClass332_3795.getWidth(), y);
		}
		Class98_Sub10_Sub34.p13Full.drawString((byte) 114, 14 + y, string, ClanChatMember.anInt1194 | ~0xffffff, -1, 3 + x);
		toolkit.fillRectangle(x, height + y, width, i_12_ + -height, -Class355.anInt3017 + 255 << 738306488 | ProceduralTextureSource.anInt3261, 1);
	}

	public static final void method239(int i, int i_0_) {
		Node node = Class168.aClass377_1287.startIteration(97);
		if (i <= 11) {
			anInt203 = -119;
		}
		for (/**/; node != null; node = Class168.aClass377_1287.iterateNext(-1)) {
			if ((node.hash >> -1444989456 & 0xffffL ^ 0xffffffffffffffffL) == (i_0_ ^ 0xffffffffffffffffL)) {
				node.unlink(83);
			}
		}
	}

	public static final void method243(int i, int i_27_, int i_28_, int i_29_, Mob class246_sub3_sub4_sub2, int i_30_, int i_31_) {
		Class168.method2533(i_27_, class246_sub3_sub4_sub2.boundExtentsX, i_30_, 0, (byte) 111, class246_sub3_sub4_sub2.boundExtentsZ, i_31_, class246_sub3_sub4_sub2.plane, i_29_, i);
	}

	public static void method244(int i) {
		do {
			bConfigDefinitionList = null;
			aClass171_202 = null;
			if (i == -24652) {
				break;
			}
			anInt203 = -28;
			break;
		} while (false);
	}

	private SeekableFile	dataFile	= null;

	private SeekableFile	indexFile	= null;

	private int				length		= 65000;

	private int				storeId;

	DataFs(int storeIndex, SeekableFile data_file, SeekableFile index_file, int length_) {
		indexFile = index_file;
		length = length_;
		storeId = storeIndex;
		dataFile = data_file;
	}

	public final byte[] getFile(int fileId, boolean bool) {
		if (bool != false) {
			bConfigDefinitionList = null;
		}
		synchronized (dataFile) {
			try {
				if (fileId * 6 + 6 > indexFile.getSize(!bool)) {
					return null;
				}
				indexFile.seek(6 * fileId, 0);
				indexFile.read(0, ioBuffer, -12913, 6);
				int fileSize = (0xff & ioBuffer[2]) + ((ioBuffer[1] & 0xff) << 1688022760) + ((0xff & ioBuffer[0]) << 1168448688);
				int sector = (0xff & ioBuffer[5]) + ((ioBuffer[3] & 0xff) << 2124255056) - -(0xff00 & ioBuffer[4] << 1644622216);
				if (fileSize < 0 || fileSize > length) {
					return null;
				}
				if ((sector ^ 0xffffffff) >= -1 || (dataFile.getSize(!bool) / 520L ^ 0xffffffffffffffffL) > (sector ^ 0xffffffffffffffffL)) {
					return null;
				}
				byte[] buffer = new byte[fileSize];
				int readLength = 0;
				int part = 0;
				while ((fileSize ^ 0xffffffff) < (readLength ^ 0xffffffff)) {
					if (sector == 0) {
						return null;
					}
					dataFile.seek(520 * sector, 0);
					int readRemaining = -readLength + fileSize;
					if ((readRemaining ^ 0xffffffff) < -513) {
						readRemaining = 512;
					}
					dataFile.read(0, ioBuffer, -12913, 8 + readRemaining);
					int currentFileId = ((0xff & ioBuffer[0]) << -1312835672) - -(0xff & ioBuffer[1]);
					int currentPart = (0xff00 & ioBuffer[2] << -96038520) + (ioBuffer[3] & 0xff);
					int nextSector = ((ioBuffer[5] & 0xff) << -657785976) + (ioBuffer[4] << -142719632 & 0xff0000) + (ioBuffer[6] & 0xff);
					int currentStore = ioBuffer[7] & 0xff;
					if ((fileId ^ 0xffffffff) != (currentFileId ^ 0xffffffff) || part != currentPart || storeId != currentStore) {
						return null;
					}
					if (nextSector < 0 || (dataFile.getSize(true) / 520L ^ 0xffffffffffffffffL) > (nextSector ^ 0xffffffffffffffffL)) {
						return null;
					}
					part++;
					for (int index = 0; readRemaining > index; index++) {
						buffer[readLength++] = ioBuffer[8 + index];
					}
					sector = nextSector;
				}
				return buffer;
			} catch (java.io.IOException ioexception) {
				return null;
			}
		}
	}

	public final boolean put(boolean bool, int length, byte i_18_, byte[] buffer, int fileId) {
		synchronized (dataFile) {
			try {
				int sector;
				if (!bool) {
					sector = (int) ((519L + dataFile.getSize(true)) / 520L);
					if ((sector ^ 0xffffffff) == -1) {
						sector = 1;
					}
				} else {
					if ((6 * fileId - -6 ^ 0xffffffffffffffffL) < (indexFile.getSize(true) ^ 0xffffffffffffffffL)) {
						return false;
					}
					indexFile.seek(6 * fileId, 0);
					indexFile.read(0, ioBuffer, -12913, 6);
					sector = (ioBuffer[5] & 0xff) + ((0xff & ioBuffer[4]) << -1769789464) + (ioBuffer[3] << 233682064 & 0xff0000);
					if (sector <= 0 || (dataFile.getSize(true) / 520L ^ 0xffffffffffffffffL) > (sector ^ 0xffffffffffffffffL)) {
						return false;
					}
				}
				ioBuffer[2] = (byte) length;
				ioBuffer[0] = (byte) (length >> 555589968);
				ioBuffer[3] = (byte) (sector >> -111684720);
				ioBuffer[1] = (byte) (length >> -1570816984);
				ioBuffer[5] = (byte) sector;
				ioBuffer[4] = (byte) (sector >> -423385592);
				indexFile.seek(fileId * 6, 0);
				indexFile.write(6, 0, -1, ioBuffer);
				int writePos = 0;
				int part = 0;
				int writeLength;
				for (/**/; length > writePos; writePos += writeLength) {
					int nextSector = 0;
					if (bool) {
						dataFile.seek(520 * sector, 0);
						try {
							dataFile.read(0, ioBuffer, -12913, 8);
						} catch (java.io.EOFException eofexception) {
							break;
						}
						writeLength = (0xff & ioBuffer[1]) + ((ioBuffer[0] & 0xff) << -2029480344);
						nextSector = (0xff & ioBuffer[6]) + ((ioBuffer[5] & 0xff) << -2069346712) + (0xff0000 & ioBuffer[4] << -94230768);
						int i_25_ = (ioBuffer[3] & 0xff) + (ioBuffer[2] << -1770629848 & 0xff00);
						int i_26_ = ioBuffer[7] & 0xff;
						if (writeLength != fileId || (part ^ 0xffffffff) != (i_25_ ^ 0xffffffff) || storeId != i_26_) {
							return false;
						}
						if ((nextSector ^ 0xffffffff) > -1 || nextSector > dataFile.getSize(true) / 520L) {
							return false;
						}
					}
					if (nextSector == 0) {
						bool = false;
						nextSector = (int) ((519L + dataFile.getSize(true)) / 520L);
						if ((nextSector ^ 0xffffffff) == -1) {
							nextSector++;
						}
						if (sector == nextSector) {
							nextSector++;
						}
					}
					ioBuffer[0] = (byte) (fileId >> 1054699720);
					if (length + -writePos <= 512) {
						nextSector = 0;
					}
					ioBuffer[2] = (byte) (part >> -272228248);
					ioBuffer[3] = (byte) part;
					ioBuffer[1] = (byte) fileId;
					ioBuffer[7] = (byte) storeId;
					ioBuffer[5] = (byte) (nextSector >> 1720878632);
					ioBuffer[4] = (byte) (nextSector >> -559801072);
					ioBuffer[6] = (byte) nextSector;
					dataFile.seek(sector * 520, i_18_ + 41);
					dataFile.write(8, 0, i_18_ + 40, ioBuffer);
					writeLength = -writePos + length;
					if ((writeLength ^ 0xffffffff) < -513) {
						writeLength = 512;
					}
					dataFile.write(writeLength, writePos, -1, buffer);
					sector = nextSector;
					part++;
				}
				return true;
			} catch (java.io.IOException ioexception) {
				return false;
			}
		}
	}

	public final boolean put(boolean bool, int parameterLength, int fileId, byte[] buffer) {
		synchronized (dataFile) {
			if ((parameterLength ^ 0xffffffff) > -1 || parameterLength > length) {
				throw new IllegalArgumentException();
			}
			boolean result = this.put(true, parameterLength, (byte) -41, buffer, fileId);
			if (!result) {
				result = this.put(false, parameterLength, (byte) -41, buffer, fileId);
			}
			return result;
		}
	}

	@Override
	public final String toString() {
		return "Cache:" + storeId;
	}
}

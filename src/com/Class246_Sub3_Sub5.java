/* Class246_Sub3_Sub5 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;

public abstract class Class246_Sub3_Sub5 extends Char {
	public static OutgoingOpcode aClass171_6164 = new OutgoingOpcode(34, 4);

	public static final void method1439(int i, int i_28_, int i_29_, int i_30_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_28_][i_29_];
		if (class172 != null) {
			Class246_Sub3_Sub5 class246_sub3_sub5 = class172.aClass246_Sub3_Sub5_1334;
			Class246_Sub3_Sub5 class246_sub3_sub5_31_ = class172.aClass246_Sub3_Sub5_1326;
			if (class246_sub3_sub5 != null) {
				class246_sub3_sub5.aShort6165 = (short) (class246_sub3_sub5.aShort6165 * i_30_ / (16 << Class151_Sub8.tileScale - 7));
				class246_sub3_sub5.aShort6163 = (short) (class246_sub3_sub5.aShort6163 * i_30_ / (16 << Class151_Sub8.tileScale - 7));
			}
			if (class246_sub3_sub5_31_ != null) {
				class246_sub3_sub5_31_.aShort6165 = (short) (class246_sub3_sub5_31_.aShort6165 * i_30_ / (16 << Class151_Sub8.tileScale - 7));
				class246_sub3_sub5_31_.aShort6163 = (short) (class246_sub3_sub5_31_.aShort6163 * i_30_ / (16 << Class151_Sub8.tileScale - 7));
			}
		}
	}

	public static void method3089(int i) {
		try {
			if (i != 4) {
				aClass171_6164 = null;
			}
			aClass171_6164 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wo.R(" + i + ')');
		}
	}

	public short	aShort6163;

	short			aShort6165;

	Class246_Sub3_Sub5(int i, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_) {
		try {
			this.boundExtentsX = i;
			this.collisionPlane = (byte) i_8_;
			aShort6165 = (short) i_9_;
			aShort6163 = (short) i_10_;
			this.plane = (byte) i_7_;
			this.boundExtentsZ = i_6_;
			this.anInt5089 = i_5_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wo.<init>(" + i + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + i_9_ + ',' + i_10_ + ')');
		}
	}

	@Override
	public final boolean method2977(RSToolkit var_ha, byte i) {
		try {
			if (i != 77) {
				return false;
			}
			return Class195.method2661(this.collisionPlane, this.boundExtentsZ >> Class151_Sub8.tileScale, this.boundExtentsX >> Class151_Sub8.tileScale, method2990(i + -77), (byte) -123);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wo.AA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2980(int i, PointLight[] class98_sub5s) {
		try {
			return method2989(this.boundExtentsX >> Class151_Sub8.tileScale, false, class98_sub5s, this.boundExtentsZ >> Class151_Sub8.tileScale);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wo.GA(" + i + ',' + (class98_sub5s != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_0_, RSToolkit var_ha, int i_1_, int i_2_) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wo.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_0_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i > -70) {
				aClass171_6164 = null;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wo.HA(" + i + ')');
		}
	}

	@Override
	public final boolean method2991(boolean bool) {
		try {
			if (bool != false) {
				return true;
			}
			return Class74.aBooleanArrayArray551[Class259.anInt1959 + -Class241.anInt1845 + (this.boundExtentsX >> Class151_Sub8.tileScale)][Class259.anInt1959 + (this.boundExtentsZ >> Class151_Sub8.tileScale) - CharacterShadowsPreferenceField.anInt3714];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wo.FA(" + bool + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		try {
			if (i != -73) {
				aClass171_6164 = null;
			}
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wo.DA(" + i + ')');
		}
	}
}

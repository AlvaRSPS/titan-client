/* Class98_Sub10_Sub18 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;

public final class Class98_Sub10_Sub18 extends Class98_Sub10 {
	public static int anInt5626;

	public static final boolean method1054(int i) {
		try {
			if (client.useObjectTag) {
				try {
					return !((Boolean) JavaScriptInterface.callJsMethod("showingVideoAd", GameShell.applet, -26978)).booleanValue();
				} catch (Throwable throwable) {
					/* empty */
				}
			}
			if (i <= 67) {
				anInt5626 = 88;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ll.D(" + i + ')');
		}
	}

	public static final boolean method1055(int i, int i_0_, byte i_1_) {
		try {
			return !(!(SimpleProgressBarLoadingScreenElement.method3974(i_0_, i, i_1_ + -101) | (0x70000 & i ^ 0xffffffff) != -1) && !Class76_Sub7.method763(i, i_0_, false));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ll.E(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static final void method1056(byte i, int i_2_) {
		try {
			if (i == 97) {
				Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i_2_, i ^ ~0x59, 7);
				class98_sub46_sub17.method1621(0);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ll.B(" + i + ',' + i_2_ + ')');
		}
	}

	public Class98_Sub10_Sub18() {
		super(0, true);
	}

	@Override
	public final int[] method990(int i, int i_3_) {
		try {
			return MonoOrStereoPreferenceField.anIntArray3640;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ll.G(" + i + ',' + i_3_ + ')');
		}
	}
}

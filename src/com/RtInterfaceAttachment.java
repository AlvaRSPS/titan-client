
/* Class98_Sub18 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.preferences.FlickeringEffectsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.input.impl.AwtMouseEvent;

public final class RtInterfaceAttachment extends Node {
	public static Queue				actionGroups	= new Queue();
	public static float				aFloat3948;
	public static int				anInt3951;
	public static int				anInt3952		= 0;
	public static IncomingOpcode	VARCSTR_SMALL	= new IncomingOpcode(88, -2);

	public static final RtInterfaceAttachment attachInterface(boolean bool, int dummy, int interfaceId, int slotIdDword, int i_15_) {
		RtInterfaceAttachment attachment = new RtInterfaceAttachment();
		attachment.anInt3947 = i_15_;
		attachment.attachedInterfaceId = interfaceId;
		Class116.attachmentMap.put(attachment, slotIdDword, -1);
		Class98_Sub46_Sub15.method1609(interfaceId, -12889);
		RtInterface slotInterf = RtInterface.getInterface(slotIdDword, -9820);
		if (slotInterf != null) {
			WorldMapInfoDefinitionParser.setDirty(1, slotInterf);
		}
		if (DummyOutputStream.rtInterface != null) {
			WorldMapInfoDefinitionParser.setDirty(1, DummyOutputStream.rtInterface);
			DummyOutputStream.rtInterface = null;
		}
		Class230.method2869(44);
		if (slotInterf != null) {
			RtInterface.calculateLayout(slotInterf, !bool, (byte) 66);
		}
		if (!bool) {
			ClientScript2Runtime.sendWindowPane(interfaceId);
		}
		if (!bool && client.topLevelInterfaceId != -1) {
			Js5.method2764(1, client.topLevelInterfaceId, -121);
		}
		return attachment;
	}

	public static final void detachInterface(int i, boolean bool, RtInterfaceAttachment attachment, boolean lastUsage) {
		int attachedId = attachment.attachedInterfaceId;
		int slotId = (int) attachment.hash;
		attachment.unlink(i + -16284);
		if (lastUsage) {
			uncacheInterface(false, attachedId);
		}
		DataFs.method239(i ^ 0x4016, attachedId);
		RtInterface interf = RtInterface.getInterface(slotId, -9820);
		if (interf != null) {
			WorldMapInfoDefinitionParser.setDirty(1, interf);
		}
		Class230.method2869(106);
		if (!bool && (client.topLevelInterfaceId ^ 0xffffffff) != 0) {
			Js5.method2764(1, client.topLevelInterfaceId, -44);
		}
		HashTableIterator iterator = new HashTableIterator(Class116.attachmentMap);
		for (RtInterfaceAttachment attachmentIterator = (RtInterfaceAttachment) iterator.start(0); attachmentIterator != null; attachmentIterator = (RtInterfaceAttachment) iterator.next(i + -16396)) {
			if (!attachmentIterator.isLinked((byte) 78)) {
				attachmentIterator = (RtInterfaceAttachment) iterator.start(0);
				if (attachmentIterator == null) {
					break;
				}
			}
			if ((attachmentIterator.anInt3947 ^ 0xffffffff) == -4) {
				int i_4_ = (int) attachmentIterator.hash;
				if ((attachedId ^ 0xffffffff) == (i_4_ >>> 16 ^ 0xffffffff)) {
					detachInterface(16398, bool, attachmentIterator, true);
				}
			}
		}
	}

	public static final void method1162(int i, int i_0_, int i_1_, Class var_class) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_][i_1_];
		if (class172 != null) {
			for (Class154 class154 = class172.aClass154_1325; class154 != null; class154 = class154.aClass154_1233) {
				Class246_Sub3_Sub4 class246_sub3_sub4 = class154.aClass246_Sub3_Sub4_1232;
				if (var_class.isAssignableFrom(class246_sub3_sub4.getClass()) && class246_sub3_sub4.aShort6158 == i_0_ && class246_sub3_sub4.aShort6157 == i_1_) {
					Class99.method1687(class246_sub3_sub4, false);
					break;
				}
			}
		}
	}

	public static boolean method1163(boolean bool, boolean bool_2_) {
		return bool | bool_2_;
	}

	public static final void uncacheInterface(boolean dummy, int interfaceId) {
		if (interfaceId != -1 && Class246_Sub3_Sub3_Sub1.loadedInterface[interfaceId]) {
			AwtMouseEvent.interfaceJs5.discardUnpacked(-53, interfaceId);
			Class159.interfaceStore[interfaceId] = null;
			FlickeringEffectsPreferenceField.interfaceOverride[interfaceId] = null;
			Class246_Sub3_Sub3_Sub1.loadedInterface[interfaceId] = false;
		}
	}

	public int	attachedInterfaceId;

	public int	anInt3947;

	public RtInterfaceAttachment() {
		/* empty */
	}
}

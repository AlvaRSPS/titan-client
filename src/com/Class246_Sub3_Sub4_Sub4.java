/* Class246_Sub3_Sub4_Sub4 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.GraphicsDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.preferences.SafeModePreferenceField;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class246_Sub3_Sub4_Sub4 extends Class246_Sub3_Sub4 {
	public static IncomingOpcode	aClass58_6487	= new IncomingOpcode(112, 6);
	public static int				width;

	public static final void method3079(byte i, Js5 class207, int i_9_, Js5 class207_10_) {
		try {
			RemoveRoofsPreferenceField.aClass207_3679 = class207_10_;
			SafeModePreferenceField.aClass207_3644 = class207;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.JA(" + i + ',' + (class207 != null ? "{...}" : "null") + ',' + i_9_ + ',' + (class207_10_ != null ? "{...}" : "null") + ')');
		}
	}

	public static final int method3082(int i, int i_40_, int i_41_) {
		try {
			int i_42_ = -128 + Class98_Sub5_Sub3.method971(45365 + i_41_, 91923 + i, i_40_, i_40_ + -91) - (-(Class98_Sub5_Sub3.method971(i_41_ + 10294, 37821 + i, 2, i_40_ + -98) + -128 >> 1334886049) - (Class98_Sub5_Sub3.method971(i_41_, i, 1, -48) - 128 >> -872292894));
			i_42_ = (int) (i_42_ * 0.3) - -35;
			do {
				if ((i_42_ ^ 0xffffffff) <= -11) {
					if ((i_42_ ^ 0xffffffff) >= -61) {
						break;
					}
					i_42_ = 60;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				i_42_ = 10;
			} while (false);
			return i_42_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.UA(" + i + ',' + i_40_ + ',' + i_41_ + ')');
		}
	}

	private boolean				aBoolean6471	= false;
	private boolean				aBoolean6473;
	private boolean				aBoolean6481;
	private Class246_Sub5		aClass246_Sub5_6489;
	private AnimationDefinition	aClass97_6459;
	private double				aDouble6462;
	private double				aDouble6464;
	private double				aDouble6472;
	private double				aDouble6476;
	private double				aDouble6478;
	private double				aDouble6480;
	private double				aDouble6483;
	private double				aDouble6490;
	public int					anInt6460;
	public int					anInt6461;
	public int					anInt6463;
	public int					anInt6465		= 0;
	public int					anInt6466;
	public int					anInt6467;
	public int					anInt6468;
	public int					anInt6469;
	public int					anInt6470;
	public int					anInt6474;
	public int					anInt6475;
	public int					anInt6477;
	public int					anInt6479;
	public int					anInt6482;

	public int					anInt6484;

	public int					anInt6485;

	public int					anInt6486;

	Class246_Sub3_Sub4_Sub4(int i, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_, int i_23_, int i_24_, boolean bool, int i_25_) {
		super(i_13_, i_14_, i_15_, -i_17_ + StrongReferenceMCNode.getHeight(i_13_, i_16_, i_15_, 24111), i_16_, i_15_ >> 1293743369, i_15_ >> -108544983, i_16_ >> -860505271, i_16_ >> 1727487849, false, (byte) 0);
		anInt6460 = 0;
		anInt6469 = -1;
		aBoolean6481 = false;
		anInt6477 = 0;
		anInt6484 = 0;
		try {
			anInt6461 = i_17_;
			anInt6475 = i_20_;
			anInt6467 = i_21_;
			anInt6485 = i;
			aBoolean6481 = false;
			anInt6463 = i_24_;
			anInt6479 = i_18_;
			anInt6482 = i_23_;
			anInt6466 = i_19_;
			anInt6470 = i_22_;
			aBoolean6473 = bool;
			anInt6486 = i_25_;
			int i_26_ = BuildLocation.gfxDefinitionList.method3564(2, anInt6485).animation;
			if (i_26_ != -1) {
				aClass97_6459 = Class151_Sub7.animationDefinitionList.method2623(i_26_, 16383);
			} else {
				aClass97_6459 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.<init>(" + i + ',' + i_13_ + ',' + i_14_ + ',' + i_15_ + ',' + i_16_ + ',' + i_17_ + ',' + i_18_ + ',' + i_19_ + ',' + i_20_ + ',' + i_21_ + ',' + i_22_ + ',' + i_23_ + ',' + i_24_ + ',' + bool + ',' + i_25_ + ')');
		}
	}

	@Override
	protected final void finalize() {
		try {
			if (aClass246_Sub5_6489 != null) {
				aClass246_Sub5_6489.method3114();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.finalize(" + ')');
		}
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (i != -53) {
				method3074(-62, -24, -14, (byte) -98, -63);
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = method3081((byte) -96, 2048, var_ha);
			if (class146 == null) {
				return null;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2107(anInt6474);
			class111.initRotY(anInt6468);
			class111.translate((int) aDouble6472, (int) aDouble6490, (int) aDouble6462);
			method3076(class111, var_ha, class146, (byte) -74);
			Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, false);
			if (!VarClientStringsDefinitionParser.aBoolean1839) {
				class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
			} else {
				class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
			}
			if (aClass246_Sub5_6489 != null) {
				Class242 class242 = aClass246_Sub5_6489.method3116();
				if (!VarClientStringsDefinitionParser.aBoolean1839) {
					var_ha.method1820(class242);
				} else {
					var_ha.method1785(class242, Class16.anInt197);
				}
			}
			if (i > -12) {
				aDouble6476 = 0.3831954713971479;
			}
			aBoolean6471 = class146.F();
			anInt6484 = class146.fa();
			anInt6460 = class146.ma();
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_27_, int i_28_) {
		try {
			if (i_27_ < 59) {
				aBoolean6471 = false;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_27_ + ',' + i_28_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.H(" + i + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_43_, RSToolkit var_ha, int i_44_, int i_45_) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_43_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_44_ + ',' + i_45_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i > -70) {
				method3078(-85);
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.HA(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				method2976(115, null, (byte) -18, -25);
			}
			return anInt6460;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (i != 6540) {
				anInt6470 = 62;
			}
			return aBoolean6471;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = method3081((byte) -96, 0, var_ha);
			if (class146 != null) {
				Matrix class111 = var_ha.method1793();
				class111.method2107(anInt6474);
				class111.initRotY(anInt6468);
				class111.translate((int) aDouble6472, (int) aDouble6490, (int) aDouble6462);
				anInt6484 = class146.fa();
				anInt6460 = class146.ma();
				method3076(class111, var_ha, class146, (byte) 74);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				method2988(null, 60);
			}
			return anInt6484;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.J(" + i + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		try {
			if (i != -73) {
				anInt6463 = -41;
			}
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.DA(" + i + ')');
		}
	}

	@Override
	public final void method3022(int i) {
		try {
			((Class246_Sub3_Sub4) this).aShort6157 = ((Class246_Sub3_Sub4) this).aShort6159 = (short) (int) (aDouble6462 / 512.0);
			if (i != -8675) {
				aDouble6480 = 0.42551889851769525;
			}
			((Class246_Sub3_Sub4) this).aShort6158 = ((Class246_Sub3_Sub4) this).aShort6160 = (short) (int) (aDouble6472 / 512.0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.F(" + i + ')');
		}
	}

	public final void method3074(int i, int i_1_, int i_2_, byte i_3_, int i_4_) {
		do {
			try {
				if (!aBoolean6481) {
					double d = -((Char) this).boundExtentsX + i;
					double d_5_ = i_4_ + -((Char) this).boundExtentsZ;
					double d_6_ = Math.sqrt(d * d + d_5_ * d_5_);
					aDouble6462 = ((Char) this).boundExtentsZ + anInt6467 * d_5_ / d_6_;
					aDouble6472 = ((Char) this).boundExtentsX + d * anInt6467 / d_6_;
					if (aBoolean6473) {
						aDouble6490 = StrongReferenceMCNode.getHeight(((Char) this).plane, (int) aDouble6462, (int) aDouble6472, 24111) - anInt6461;
					} else {
						aDouble6490 = ((Char) this).anInt5089;
					}
				}
				double d = -i_2_ + 1 + anInt6466;
				aDouble6483 = (i_4_ - aDouble6462) / d;
				aDouble6464 = (-aDouble6472 + i) / d;
				aDouble6478 = Math.sqrt(aDouble6483 * aDouble6483 + aDouble6464 * aDouble6464);
				if (anInt6475 == -1) {
					aDouble6480 = (i_1_ - aDouble6490) / d;
				} else {
					if (!aBoolean6481) {
						aDouble6480 = -aDouble6478 * Math.tan(anInt6475 * 0.02454369);
					}
					aDouble6476 = 2.0 * (i_1_ - aDouble6490 - aDouble6480 * d) / (d * d);
				}
				if (i_3_ == 108) {
					break;
				}
				anInt6475 = 74;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rk.NA(" + i + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ')');
			}
			break;
		} while (false);
	}

	public final void method3075(int i, int i_7_) {
		do {
			try {
				aDouble6462 += i_7_ * aDouble6483;
				aDouble6472 += aDouble6464 * i_7_;
				aBoolean6481 = true;
				if (!aBoolean6473) {
					if ((anInt6475 ^ 0xffffffff) != 0) {
						aDouble6490 += i_7_ * (i_7_ * (aDouble6476 * 0.5)) + i_7_ * aDouble6480;
						aDouble6480 += i_7_ * aDouble6476;
					} else {
						aDouble6490 += aDouble6480 * i_7_;
					}
				} else {
					aDouble6490 = StrongReferenceMCNode.getHeight(((Char) this).plane, (int) aDouble6462, (int) aDouble6472, i + 34573) + -anInt6461;
				}
				if (i == -10462) {
					anInt6468 = 8192 + (int) (2607.5945876176133 * Math.atan2(aDouble6464, aDouble6483)) & 0x3fff;
					anInt6474 = (int) (2607.5945876176133 * Math.atan2(aDouble6480, aDouble6478)) & 0x3fff;
					if (aClass97_6459 == null) {
						break;
					}
					anInt6477 += i_7_;
					while ((aClass97_6459.frameLengths[anInt6465] ^ 0xffffffff) > (anInt6477 ^ 0xffffffff)) {
						anInt6477 -= aClass97_6459.frameLengths[anInt6465];
						anInt6465++;
						if (anInt6465 >= aClass97_6459.frameIds.length) {
							anInt6465 -= aClass97_6459.frameStep;
							if (anInt6465 < 0 || anInt6465 >= aClass97_6459.frameIds.length) {
								anInt6465 = 0;
							}
						}
						anInt6469 = anInt6465 - -1;
						if (aClass97_6459.frameIds.length <= anInt6469) {
							anInt6469 -= aClass97_6459.frameStep;
							if (anInt6469 < 0 || anInt6469 >= aClass97_6459.frameIds.length) {
								anInt6469 = -1;
							}
						}
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rk.VA(" + i + ',' + i_7_ + ')');
			}
			break;
		} while (false);
	}

	private final void method3076(Matrix class111, RSToolkit var_ha, ModelRenderer class146, byte i) {
		do {
			try {
				class146.method2343(class111);
				Class87[] class87s = class146.method2320();
				Class35[] class35s = class146.method2322();
				if ((aClass246_Sub5_6489 == null || aClass246_Sub5_6489.aBoolean5099) && (class87s != null || class35s != null)) {
					aClass246_Sub5_6489 = Class246_Sub5.method3117(Queue.timer, true);
				}
				if (aClass246_Sub5_6489 == null) {
					break;
				}
				aClass246_Sub5_6489.method3120(var_ha, Queue.timer, class87s, class35s, false);
				aClass246_Sub5_6489.method3123(((Char) this).plane, ((Class246_Sub3_Sub4) this).aShort6158, ((Class246_Sub3_Sub4) this).aShort6160, ((Class246_Sub3_Sub4) this).aShort6157, ((Class246_Sub3_Sub4) this).aShort6159);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rk.RA(" + (class111 != null ? "{...}" : "null") + ',' + (var_ha != null ? "{...}" : "null") + ',' + (class146 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final void method3078(int i) {
		do {
			if (aClass246_Sub5_6489 == null) {
				break;
			}
			aClass246_Sub5_6489.method3114();
			break;
		} while (false);
	}

	public final void method3080(byte i) {
		do {
			try {
				if (!aBoolean6481) {
					if (i < 72) {
						anInt6485 = -118;
					}
					if ((anInt6470 ^ 0xffffffff) == -1) {
						break;
					}
					Mob class246_sub3_sub4_sub2 = null;
					if (anInt6470 < 0) {
						int i_29_ = -anInt6470 + -1;
						if ((OpenGLHeap.localPlayerIndex ^ 0xffffffff) == (i_29_ ^ 0xffffffff)) {
							class246_sub3_sub4_sub2 = Class87.localPlayer;
						} else {
							class246_sub3_sub4_sub2 = Class151_Sub9.players[i_29_];
						}
					} else {
						int i_30_ = -1 + anInt6470;
						NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(i_30_, -1);
						if (class98_sub39 != null) {
							class246_sub3_sub4_sub2 = class98_sub39.npc;
						}
					}
					if (class246_sub3_sub4_sub2 == null) {
						break;
					}
					((Char) this).boundExtentsX = ((Char) class246_sub3_sub4_sub2).boundExtentsX;
					((Char) this).boundExtentsZ = ((Char) class246_sub3_sub4_sub2).boundExtentsZ;
					((Char) this).anInt5089 = StrongReferenceMCNode.getHeight(((Char) this).plane, ((Char) class246_sub3_sub4_sub2).boundExtentsZ, ((Char) class246_sub3_sub4_sub2).boundExtentsX, 24111) - anInt6461;
					if (anInt6486 < 0) {
						break;
					}
					RenderAnimDefinition class294 = class246_sub3_sub4_sub2.method3039(1);
					int i_31_ = 0;
					int i_32_ = 0;
					if (class294.anIntArrayArray2366 != null && class294.anIntArrayArray2366[anInt6486] != null) {
						i_31_ += class294.anIntArrayArray2366[anInt6486][0];
						i_32_ += class294.anIntArrayArray2366[anInt6486][2];
					}
					if (class294.anIntArrayArray2364 != null && class294.anIntArrayArray2364[anInt6486] != null) {
						i_32_ += class294.anIntArrayArray2364[anInt6486][2];
						i_31_ += class294.anIntArrayArray2364[anInt6486][0];
					}
					if ((i_31_ ^ 0xffffffff) != -1 || i_32_ != 0) {
						int i_33_ = class246_sub3_sub4_sub2.yaw.value((byte) 116);
						int i_34_ = i_33_;
						if (class246_sub3_sub4_sub2.anIntArray6370 != null && class246_sub3_sub4_sub2.anIntArray6370[anInt6486] != -1) {
							i_34_ = class246_sub3_sub4_sub2.anIntArray6370[anInt6486];
						}
						int i_35_ = 0x3fff & -i_33_ + i_34_;
						int i_36_ = Class284_Sub2_Sub2.SINE[i_35_];
						int i_37_ = Class284_Sub2_Sub2.COSINE[i_35_];
						int i_38_ = i_32_ * i_36_ + i_37_ * i_31_ >> 1205179214;
						i_32_ = i_37_ * i_32_ + -(i_36_ * i_31_) >> -1374485746;
						i_31_ = i_38_;
						((Char) this).boundExtentsX += i_31_;
						((Char) this).boundExtentsZ += i_32_;
					}
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "rk.OA(" + i + ')');
			}
			break;
		} while (false);
	}

	private final ModelRenderer method3081(byte i, int i_39_, RSToolkit var_ha) {
		try {
			if (i != -96) {
				method2981(null, (byte) -125, false, -13, null, 78, -34);
			}
			GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, anInt6485);
			return class107.method1728(anInt6465, Class151_Sub7.animationDefinitionList, i_39_, anInt6477, (byte) -94, anInt6469, var_ha);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rk.LA(" + i + ',' + i_39_ + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}
}


/* Class42_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.toolkit.matrix.Matrix;

import jaggl.OpenGL;

public final class Class42_Sub3 extends Class42 {
	public static Matrix				aClass111_5364;
	public static QuickChatMessageType	aClass348_5363	= new QuickChatMessageType(6, 0, 4, 2);
	public static int					anInt5365		= 0;
	public static int					anInt5366;

	private int							anInt5362;

	Class42_Sub3(OpenGlToolkit var_ha_Sub1, int i, int i_3_, byte[] is, int i_4_) {
		super(var_ha_Sub1, 3552, i, i_3_, false);
		try {
			anInt5362 = i_3_;
			this.aHa_Sub1_3227.setActiveTexture(1, this);
			OpenGL.glPixelStorei(3317, 1);
			OpenGL.glTexImage1Dub(this.anInt3226, 0, this.anInt3230, anInt5362, 0, i_4_, 5121, is, 0);
			OpenGL.glPixelStorei(3317, 4);
			method372(-28003, true);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pea.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_3_ + ',' + (is != null ? "{...}" : "null") + ',' + i_4_ + ')');
		}
	}

	@Override
	public final void method3(byte i) {
		try {
			if (i > -117) {
				GameShell.canvas = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pea.B(" + i + ')');
		}
	}

	public final void method393(int i, boolean bool) {
		try {
			if (i == 3552) {
				this.aHa_Sub1_3227.setActiveTexture(1, this);
				OpenGL.glTexParameteri(this.anInt3226, 10242, !bool ? 33071 : 10497);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pea.C(" + i + ',' + bool + ')');
		}
	}
}

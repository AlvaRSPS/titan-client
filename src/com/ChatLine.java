
/* Class131 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;

public final class ChatLine {

	int		flags;
	int		fileIndex;
	int		somethingClanChatRelated;
	int		anInt1039;
	int		messageType;
	String	chatboxMessage;
	String	clanChatName;
	String	displayName;
	String	nameSimple;

	String	nameUnfiltered;

	ChatLine(int type, int _flags, String _displayName, String _nameUnfiltered, String _nameSimple, String _clan, int index, String message) {
		somethingClanChatRelated = Class14.method226(120);
		fileIndex = index;
		anInt1039 = Queue.timer;
		messageType = type;
		displayName = _displayName;
		chatboxMessage = message;
		clanChatName = _clan;
		nameSimple = _nameSimple;
		nameUnfiltered = _nameUnfiltered;
		flags = _flags;

	}

	public final void update(int index, String message, String _displayName, String clan, int _flags, String _nameUnfiltered, int type, String _nameSimple, int i_6_) {
		somethingClanChatRelated = Class14.method226(i_6_ ^ ~0x7970);
		clanChatName = clan;
		flags = _flags;
		chatboxMessage = message;
		nameUnfiltered = _nameUnfiltered;
		anInt1039 = Queue.timer;
		fileIndex = index;
		messageType = type;
		nameSimple = _nameSimple;
		displayName = _displayName;
	}
}


/* Class98_Sub35 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Rectangle;

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.input.impl.AwtMouseListener;

import jaclib.hardware_info.HardwareInfo;

public final class PlatformInformation extends Node {
	public static Rectangle[] aRectangleArray4144 = new Rectangle[100];

	static {
		for (int i = 0; i < 100; i++) {
			aRectangleArray4144[i] = new Rectangle();
		}
	}

	public static final void method1452(int i) {
		try {
			OpenGlPointLight.aHa4185.setAmbientIntensity(1.1523438F * (0.7F + 0.1F * client.preferences.brightnessLevel.getValue((byte) 122)));
			OpenGlPointLight.aHa4185.setSun(AwtMouseListener.anInt5298, 0.69921875F, 1.2F, -200.0F, -240.0F, -200.0F);
			OpenGlPointLight.aHa4185.applyFog(Class189.anInt1455, -1, i);
			OpenGlPointLight.aHa4185.method1775(Class246_Sub7.aClass48_5119);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pg.E(" + i + ')');
		}
	}

	public static void method1456(boolean bool) {
		try {
			if (bool != false) {
				aRectangleArray4144 = null;
			}
			aRectangleArray4144 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pg.A(" + bool + ')');
		}
	}

	private boolean	aBoolean4132;
	private boolean	aBoolean4138;
	private int		anInt4130;
	private int		anInt4133;
	private int		anInt4134;
	private int		anInt4136;
	private int		anInt4137;
	private int		anInt4139;
	private int		anInt4140;
	private int		anInt4141;
	private int		anInt4142;
	private int		anInt4145;
	private int		anInt4147;
	private int		anInt4148;
	private int		anInt4149;
	private int		anInt4150;
	private String	aString4131;
	private String	aString4135;

	private String	aString4143;

	private String	aString4146;

	int				totalPhysicMemory;

	public PlatformInformation() {
		/* empty */
	}

	PlatformInformation(boolean bool, SignLink class88) {
		try {
			if (bool) {
				if (!SignLink.osNameLowerCase.startsWith("win")) {
					if (SignLink.osNameLowerCase.startsWith("mac")) {
						anInt4147 = 2;
					} else if (SignLink.osNameLowerCase.startsWith("linux")) {
						anInt4147 = 3;
					} else {
						anInt4147 = 4;
					}
				} else {
					anInt4147 = 1;
				}
				aBoolean4132 = !(!SignLink.osArch.startsWith("amd64") && !SignLink.osArch.startsWith("x86_64"));
				if ((anInt4147 ^ 0xffffffff) == -2) {
					if (SignLink.osVersion.indexOf("4.0") == -1) {
						if ((SignLink.osVersion.indexOf("4.1") ^ 0xffffffff) != 0) {
							anInt4133 = 2;
						} else if ((SignLink.osVersion.indexOf("4.9") ^ 0xffffffff) != 0) {
							anInt4133 = 3;
						} else if (SignLink.osVersion.indexOf("5.0") == -1) {
							if (SignLink.osVersion.indexOf("5.1") == -1) {
								if (SignLink.osVersion.indexOf("6.0") == -1) {
									if (SignLink.osVersion.indexOf("6.1") != -1) {
										anInt4133 = 7;
									}
								} else {
									anInt4133 = 6;
								}
							} else {
								anInt4133 = 5;
							}
						} else {
							anInt4133 = 4;
						}
					} else {
						anInt4133 = 1;
					}
				} else if (anInt4147 == 2) {
					if ((SignLink.osVersion.indexOf("10.4") ^ 0xffffffff) != 0) {
						anInt4133 = 20;
					} else if ((SignLink.osVersion.indexOf("10.5") ^ 0xffffffff) == 0) {
						if (SignLink.osVersion.indexOf("10.6") != -1) {
							anInt4133 = 22;
						}
					} else {
						anInt4133 = 21;
					}
				}
				if ((SignLink.java_vendor.toLowerCase().indexOf("sun") ^ 0xffffffff) != 0) {
					anInt4136 = 1;
				} else if ((SignLink.java_vendor.toLowerCase().indexOf("microsoft") ^ 0xffffffff) != 0) {
					anInt4136 = 2;
				} else if (SignLink.java_vendor.toLowerCase().indexOf("apple") == -1) {
					anInt4136 = 4;
				} else {
					anInt4136 = 3;
				}
				int i = 2;
				int i_1_ = 0;
				try {
					while (i < SignLink.javaVersion.length()) {
						int i_2_ = SignLink.javaVersion.charAt(i);
						if (i_2_ < 48 || i_2_ > 57) {
							break;
						}
						i++;
						i_1_ = 10 * i_1_ + i_2_ - 48;
					}
				} catch (Exception exception) {
					/* empty */
				}
				anInt4137 = i_1_;
				i = 1 + SignLink.javaVersion.indexOf('.', 2);
				i_1_ = 0;
				try {
					while (SignLink.javaVersion.length() > i) {
						int i_3_ = SignLink.javaVersion.charAt(i);
						if ((i_3_ ^ 0xffffffff) > -49 || i_3_ > 57) {
							break;
						}
						i++;
						i_1_ = -48 - -i_3_ + 10 * i_1_;
					}
				} catch (Exception exception) {
					/* empty */
				}
				anInt4148 = i_1_;
				i = SignLink.javaVersion.indexOf('_', 4) - -1;
				i_1_ = 0;
				try {
					for (/**/; SignLink.javaVersion.length() > i; i++) {
						int i_4_ = SignLink.javaVersion.charAt(i);
						if ((i_4_ ^ 0xffffffff) > -49 || i_4_ > 57) {
							break;
						}
						i_1_ = i_4_ + -48 + i_1_ * 10;
					}
				} catch (Exception exception) {
					/* empty */
				}
				anInt4142 = i_1_;
				anInt4139 = GameShell.maxHeap;
				aBoolean4138 = !class88.is_signed;
				if (anInt4137 <= 3) {
					anInt4145 = 0;
				} else {
					anInt4145 = GameShell.availableProcessors;
				}
				try {
					int[] is = HardwareInfo.getCPUInfo();
					if (is != null && (is.length ^ 0xffffffff) == -8) {
						anInt4149 = is[4];
						anInt4140 = is[5];
						anInt4141 = is[3];
						totalPhysicMemory = is[6];
						anInt4150 = is[2];
					}
				} catch (Throwable throwable) {
					Class305_Sub1.reportError(throwable, -121, throwable.getMessage() + " (Recovered)");
					totalPhysicMemory = 0;
				}
			}
			if (aString4135 == null) {
				aString4135 = "";
			}
			if (aString4143 == null) {
				aString4143 = "";
			}
			if (aString4131 == null) {
				aString4131 = "";
			}
			if (aString4146 == null) {
				aString4146 = "";
			}
			method1454(-120);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pg.<init>(" + bool + ',' + (class88 != null ? "{...}" : "null") + ')');
		}
	}

	public final void method1453(byte i, RSByteBuffer class98_sub22) {
		try {
			class98_sub22.writeByte(5, i + -104);
			class98_sub22.writeByte(anInt4147, -115);
			class98_sub22.writeByte(!aBoolean4132 ? 0 : 1, 85);
			class98_sub22.writeByte(anInt4133, i + -137);
			if (i != 17) {
				method1452(-7);
			}
			class98_sub22.writeByte(anInt4136, 95);
			class98_sub22.writeByte(anInt4137, 114);
			class98_sub22.writeByte(anInt4148, i ^ 0x26);
			class98_sub22.writeByte(anInt4142, -36);
			class98_sub22.writeByte(!aBoolean4138 ? 0 : 1, i ^ 0x63);
			class98_sub22.writeShort(anInt4139, 1571862888);
			class98_sub22.writeByte(anInt4145, -36);
			class98_sub22.method1225(-24472, totalPhysicMemory);
			class98_sub22.writeShort(anInt4150, i + 1571862871);
			class98_sub22.writeByte(anInt4141, -101);
			class98_sub22.writeByte(anInt4149, 114);
			class98_sub22.writeByte(anInt4140, -128);
			class98_sub22.pjstr2(aString4131, -1);
			class98_sub22.pjstr2(aString4143, -1);
			class98_sub22.pjstr2(aString4146, -1);
			class98_sub22.pjstr2(aString4135, -1);
			class98_sub22.writeByte(anInt4130, -76);
			class98_sub22.writeShort(anInt4134, 1571862888);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pg.D(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	private final void method1454(int i) {
		do {
			try {
				if (i < -6) {
					if ((aString4131.length() ^ 0xffffffff) < -41) {
						aString4131 = aString4131.substring(0, 40);
					}
					if ((aString4143.length() ^ 0xffffffff) < -41) {
						aString4143 = aString4143.substring(0, 40);
					}
					if (aString4146.length() > 10) {
						aString4146 = aString4146.substring(0, 10);
					}
					if ((aString4135.length() ^ 0xffffffff) >= -11) {
						break;
					}
					aString4135 = aString4135.substring(0, 10);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "pg.C(" + i + ')');
			}
			break;
		} while (false);
	}

	public final int method1455(byte i) {
		try {
			if (i < 112) {
				return -52;
			}
			int i_0_ = 23;
			i_0_ += GroundItem.method1275(aString4131, false);
			i_0_ += GroundItem.method1275(aString4143, false);
			i_0_ += GroundItem.method1275(aString4146, false);
			i_0_ += GroundItem.method1275(aString4135, false);
			return i_0_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pg.B(" + i + ')');
		}
	}
}

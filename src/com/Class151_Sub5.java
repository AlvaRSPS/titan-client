/* Class151_Sub5 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class151_Sub5 extends Class151 {
	public static boolean			aBoolean4991	= false;
	public static IncomingOpcode	aClass58_4992	= new IncomingOpcode(87, 7);
	public static int				anInt4990;
	public static int				anInt4993;

	public static void method2461(int i) {
		do {
			try {
				aClass58_4992 = null;
				if (i == -3) {
					break;
				}
				anInt4993 = -98;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "mca.B(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method2462(int i, byte i_3_) {
		try {
			return !(i != 0 && (i ^ 0xffffffff) != -3);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mca.H(" + i + ',' + i_3_ + ')');
		}
	}

	Class151_Sub5(OpenGlToolkit var_ha_Sub1) {
		super(var_ha_Sub1);
	}

	@Override
	public final boolean method2439(int i) {
		try {
			if (i != 31565) {
				method2461(-54);
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mca.A(" + i + ')');
		}
	}

	@Override
	public final void method2440(boolean bool, boolean bool_2_) {
		try {
			if (bool != false) {
				aClass58_4992 = null;
			}
			this.aHa_Sub1_1215.method1905(true, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mca.D(" + bool + ',' + bool_2_ + ')');
		}
	}

	@Override
	public final void method2441(int i, int i_0_, int i_1_) {
		try {
			if (i_1_ > -2) {
				method2445((byte) -119);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mca.G(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	@Override
	public final void method2442(Class42 class42, boolean bool, int i) {
		try {
			if (bool != false) {
				method2440(false, false);
			}
			this.aHa_Sub1_1215.setActiveTexture(1, class42);
			this.aHa_Sub1_1215.method1896(260, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mca.F(" + (class42 != null ? "{...}" : "null") + ',' + bool + ',' + i + ')');
		}
	}

	@Override
	public final void method2443(boolean bool, int i) {
		try {
			if (i != 255) {
				return;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mca.C(" + bool + ',' + i + ')');
		}
	}

	@Override
	public final void method2445(byte i) {
		try {
			this.aHa_Sub1_1215.method1905(false, 0);
			if (i < 25) {
				aBoolean4991 = false;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "mca.E(" + i + ')');
		}
	}
}

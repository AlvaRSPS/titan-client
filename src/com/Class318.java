/* Class318 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.Class64_Sub5;

public final class Class318 {
	public static StreamHandler aClass123_2698;

	public static void method3655(boolean bool) {
		try {
			if (bool == true) {
				aClass123_2698 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tk.B(" + bool + ')');
		}
	}

	public static final void method3656(int i, int i_0_, byte i_1_, int i_2_, int i_3_) {
		try {
			if (i_3_ >= Class98_Sub10_Sub38.anInt5753 && SceneGraphNodeList.anInt1635 >= i_3_) {
				i_0_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_0_);
				i_2_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_2_);
				Class160.method2513((byte) -125, i, i_0_, i_2_, i_3_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tk.C(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public static final void method3657(byte i) {
		try {
			Class69_Sub2.aClass79_5334.clear(77);
			Class64_Sub5.aClass79_3650.clear(119);
			Class76_Sub11.aClass79_3797.clear(47);
			if (i > -20) {
				method3655(true);
			}
			Class151_Sub7.aClass79_5004.clear(108);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tk.A(" + i + ')');
		}
	}
}

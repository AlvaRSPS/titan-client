/* Class253 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.Queue;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.FileRequest;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.preferences.SafeModePreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.toolkit.font.Font;

public final class AsyncCache implements Runnable {
	public static boolean	aBoolean1930;
	public static int		anInt1929;
	public static int		anInt1934;
	public static int[]		anIntArray1928	= { 0, 4, 3, 3, 1, 1, 3, 5, 1, 5, 3, 6, 4 };
	public static int[]		anIntArray1931	= new int[8];
	public static Js5		defaultsJs5;

	static {
		anInt1929 = 1;
		aBoolean1930 = false;
	}

	public static final boolean method3175(int i, int i_0_, Class172[][][] class172s, int i_1_, byte i_2_, boolean bool) {
		try {
			byte i_3_ = bool ? (byte) 1 : (byte) (RemoveRoofsPreferenceField.anInt3676 & 0xff);
			if (OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i][i_1_] == i_3_) {
				return false;
			}
			if ((0x4 & Class281.flags[Font.localPlane][i][i_1_]) == 0) {
				return false;
			}
			int i_4_ = 0;
			int i_5_ = 0;
			Huffman.anIntArray1610[i_4_] = i;
			Class69.anIntArray3220[i_4_++] = i_1_;
			OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i][i_1_] = i_3_;
			while ((i_4_ ^ 0xffffffff) != (i_5_ ^ 0xffffffff)) {
				int i_6_ = Huffman.anIntArray1610[i_5_] & 0xffff;
				int i_7_ = Huffman.anIntArray1610[i_5_] >> 72865712 & 0xff;
				int i_8_ = Huffman.anIntArray1610[i_5_] >> 1855655704 & 0xff;
				int i_9_ = 0xffff & Class69.anIntArray3220[i_5_];
				int i_10_ = (Class69.anIntArray3220[i_5_] & 0xff7a19) >> -1279562416;
				i_5_ = 1 + i_5_ & 0xfff;
				boolean bool_11_ = false;
				if ((0x4 & Class281.flags[Font.localPlane][i_6_][i_9_]) == 0) {
					bool_11_ = true;
				}
				boolean bool_12_ = false;
				if (class172s != null) {
					int i_13_ = Font.localPlane + 1;
					while_160_: for (/**/; (i_13_ ^ 0xffffffff) >= -4; i_13_++) {
						if (class172s[i_13_] != null && (0x8 & Class281.flags[i_13_][i_6_][i_9_] ^ 0xffffffff) == -1) {
							if (bool_11_ && class172s[i_13_][i_6_][i_9_] != null) {
								if (class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1324 != null) {
									int i_14_ = SafeModePreferenceField.method561((byte) -95, i_7_);
									if (class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1324.aShort6153 == i_14_ || class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1333 != null && i_14_ == class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1333.aShort6153) {
										continue;
									}
									if ((i_8_ ^ 0xffffffff) != -1) {
										int i_15_ = SafeModePreferenceField.method561((byte) -95, i_8_);
										if ((i_15_ ^ 0xffffffff) == (class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1324.aShort6153 ^ 0xffffffff) || class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1333 != null && (class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1333.aShort6153
												^ 0xffffffff) == (i_15_ ^ 0xffffffff)) {
											continue;
										}
									}
									if (i_10_ != 0) {
										int i_16_ = SafeModePreferenceField.method561((byte) -95, i_10_);
										if (i_16_ == class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1324.aShort6153 || class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1333 != null && (i_16_ ^ 0xffffffff) == (class172s[i_13_][i_6_][i_9_].aClass246_Sub3_Sub3_1333.aShort6153 ^ 0xffffffff)) {
											continue;
										}
									}
								}
								Class172 class172 = class172s[i_13_][i_6_][i_9_];
								if (class172.aClass154_1325 != null) {
									for (Class154 class154 = class172.aClass154_1325; class154 != null; class154 = class154.aClass154_1233) {
										Class246_Sub3_Sub4 class246_sub3_sub4 = class154.aClass246_Sub3_Sub4_1232;
										if (class246_sub3_sub4 instanceof Interface19) {
											Interface19 interface19 = (Interface19) class246_sub3_sub4;
											int i_17_ = interface19.method63((byte) 20);
											int i_18_ = interface19.method66(i_2_ + 4733);
											if ((i_17_ ^ 0xffffffff) == -22) {
												i_17_ = 19;
											}
											int i_19_ = i_17_ | i_18_ << 1030705638;
											if (i_7_ == i_19_ || (i_8_ ^ 0xffffffff) != -1 && (i_19_ ^ 0xffffffff) == (i_8_ ^ 0xffffffff) || (i_10_ ^ 0xffffffff) != -1 && (i_19_ ^ 0xffffffff) == (i_10_ ^ 0xffffffff)) {
												continue while_160_;
											}
										}
									}
								}
							}
							Class172 class172 = class172s[i_13_][i_6_][i_9_];
							if (class172 != null && class172.aClass154_1325 != null) {
								for (Class154 class154 = class172.aClass154_1325; class154 != null; class154 = class154.aClass154_1233) {
									Class246_Sub3_Sub4 class246_sub3_sub4 = class154.aClass246_Sub3_Sub4_1232;
									if (class246_sub3_sub4.aShort6158 != class246_sub3_sub4.aShort6160 || (class246_sub3_sub4.aShort6159 ^ 0xffffffff) != (class246_sub3_sub4.aShort6157 ^ 0xffffffff)) {
										for (int i_20_ = class246_sub3_sub4.aShort6158; (class246_sub3_sub4.aShort6160 ^ 0xffffffff) <= (i_20_ ^ 0xffffffff); i_20_++) {
											for (int i_21_ = class246_sub3_sub4.aShort6157; (i_21_ ^ 0xffffffff) >= (class246_sub3_sub4.aShort6159 ^ 0xffffffff); i_21_++) {
												OutputStream_Sub2.aByteArrayArrayArray41[i_13_][i_20_][i_21_] = i_3_;
											}
										}
									}
								}
							}
							OutputStream_Sub2.aByteArrayArrayArray41[i_13_][i_6_][i_9_] = i_3_;
							bool_12_ = true;
						}
					}
				}
				if (bool_12_) {
					int i_22_ = Class78.aSArray594[Font.localPlane + 1].getTileHeight(i_9_, -12639, i_6_);
					if ((Class204.anIntArray1551[i_0_] ^ 0xffffffff) > (i_22_ ^ 0xffffffff)) {
						Class204.anIntArray1551[i_0_] = i_22_;
					}
					int i_23_ = i_6_ << -999054935;
					int i_24_ = i_9_ << -405497975;
					if ((Class336.anIntArray2826[i_0_] ^ 0xffffffff) >= (i_23_ ^ 0xffffffff)) {
						if (i_23_ > Class287.anIntArray2195[i_0_]) {
							Class287.anIntArray2195[i_0_] = i_23_;
						}
					} else {
						Class336.anIntArray2826[i_0_] = i_23_;
					}
					if (Class48_Sub1_Sub2.anIntArray5518[i_0_] > i_24_) {
						Class48_Sub1_Sub2.anIntArray5518[i_0_] = i_24_;
					} else if (Class295.anIntArray2409[i_0_] < i_24_) {
						Class295.anIntArray2409[i_0_] = i_24_;
					}
				}
				if (!bool_11_) {
					if (i_6_ >= 1 && OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i_6_ + -1][i_9_] != i_3_) {
						Huffman.anIntArray1610[i_4_] = Class41.or(-754974720, Class41.or(i_6_ - 1, 1179648));
						Class69.anIntArray3220[i_4_] = Class41.or(1245184, i_9_);
						i_4_ = 0xfff & i_4_ + 1;
						OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][-1 + i_6_][i_9_] = i_3_;
					}
					if ((++i_9_ ^ 0xffffffff) > (Class98_Sub10_Sub7.mapLength ^ 0xffffffff)) {
						if (i_6_ - 1 >= 0 && i_3_ != OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i_6_ - 1][i_9_] && (Class281.flags[Font.localPlane][i_6_][i_9_] & 0x4 ^ 0xffffffff) == -1 && (0x4 & Class281.flags[Font.localPlane][i_6_ + -1][i_9_ + -1]) == 0) {
							Huffman.anIntArray1610[i_4_] = Class41.or(Class41.or(1179648, i_6_ - 1), 1375731712);
							Class69.anIntArray3220[i_4_] = Class41.or(i_9_, 1245184);
							OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][-1 + i_6_][i_9_] = i_3_;
							i_4_ = 0xfff & i_4_ + 1;
						}
						if ((OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i_6_][i_9_] ^ 0xffffffff) != (i_3_ ^ 0xffffffff)) {
							Huffman.anIntArray1610[i_4_] = Class41.or(Class41.or(5373952, i_6_), 318767104);
							Class69.anIntArray3220[i_4_] = Class41.or(5439488, i_9_);
							OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i_6_][i_9_] = i_3_;
							i_4_ = 0xfff & i_4_ - -1;
						}
						if (Class165.mapWidth > i_6_ - -1 && i_3_ != OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][1 + i_6_][i_9_] && (Class281.flags[Font.localPlane][i_6_][i_9_] & 0x4 ^ 0xffffffff) == -1 && (0x4 & Class281.flags[Font.localPlane][i_6_ - -1][-1 + i_9_]) == 0) {
							Huffman.anIntArray1610[i_4_] = Class41.or(Class41.or(1 + i_6_, 5373952), -1845493760);
							Class69.anIntArray3220[i_4_] = Class41.or(5439488, i_9_);
							OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][1 + i_6_][i_9_] = i_3_;
							i_4_ = 1 + i_4_ & 0xfff;
						}
					}
					i_9_--;
					if (i_6_ + 1 < Class165.mapWidth && (OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][1 + i_6_][i_9_] ^ 0xffffffff) != (i_3_ ^ 0xffffffff)) {
						Huffman.anIntArray1610[i_4_] = Class41.or(Class41.or(1 + i_6_, 9568256), 1392508928);
						Class69.anIntArray3220[i_4_] = Class41.or(9633792, i_9_);
						i_4_ = 0xfff & 1 + i_4_;
						OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i_6_ + 1][i_9_] = i_3_;
					}
					if ((--i_9_ ^ 0xffffffff) <= -1) {
						if ((-1 + i_6_ ^ 0xffffffff) <= -1 && (OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][-1 + i_6_][i_9_] ^ 0xffffffff) != (i_3_ ^ 0xffffffff) && (Class281.flags[Font.localPlane][i_6_][i_9_] & 0x4) == 0 && (0x4 & Class281.flags[Font.localPlane][-1 + i_6_][i_9_
								- -1]) == 0) {
							Huffman.anIntArray1610[i_4_] = Class41.or(Class41.or(13762560, -1 + i_6_), 301989888);
							Class69.anIntArray3220[i_4_] = Class41.or(13828096, i_9_);
							OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][-1 + i_6_][i_9_] = i_3_;
							i_4_ = 0xfff & 1 + i_4_;
						}
						if ((OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i_6_][i_9_] ^ 0xffffffff) != (i_3_ ^ 0xffffffff)) {
							Huffman.anIntArray1610[i_4_] = Class41.or(Class41.or(i_6_, 13762560), -1828716544);
							Class69.anIntArray3220[i_4_] = Class41.or(13828096, i_9_);
							i_4_ = 0xfff & i_4_ + 1;
							OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i_6_][i_9_] = i_3_;
						}
						if (1 + i_6_ < Class165.mapWidth && i_3_ != OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][1 + i_6_][i_9_] && (Class281.flags[Font.localPlane][i_6_][i_9_] & 0x4 ^ 0xffffffff) == -1 && (Class281.flags[Font.localPlane][i_6_ + 1][1 + i_9_] & 0x4 ^ 0xffffffff) == -1) {
							Huffman.anIntArray1610[i_4_] = Class41.or(-771751936, Class41.or(i_6_ + 1, 9568256));
							Class69.anIntArray3220[i_4_] = Class41.or(i_9_, 9633792);
							i_4_ = i_4_ - -1 & 0xfff;
							OutputStream_Sub2.aByteArrayArrayArray41[Font.localPlane][i_6_ + 1][i_9_] = i_3_;
						}
					}
				}
			}
			if (Class204.anIntArray1551[i_0_] != -1000000) {
				Class204.anIntArray1551[i_0_] += 40;
				Class336.anIntArray2826[i_0_] -= 512;
				Class287.anIntArray2195[i_0_] += 512;
				Class295.anIntArray2409[i_0_] += 512;
				Class48_Sub1_Sub2.anIntArray5518[i_0_] -= 512;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pt.F(" + i + ',' + i_0_ + ',' + (class172s != null ? "{...}" : "null") + ',' + i_1_ + ',' + i_2_ + ',' + bool + ')');
		}
	}

	public static final Class246_Sub3_Sub1 method3177(int i, int i_27_, int i_28_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_27_][i_28_];
		if (class172 == null || class172.aClass246_Sub3_Sub1_1332 == null) {
			return null;
		}
		return class172.aClass246_Sub3_Sub1_1332;
	}

	public static final void method3181(Js5 class207, int i) {
		NativeToolkit.aClass207_4528 = class207;
		anInt1929 = 127;
	}

	public int		requestCount;

	private Queue	requestQueue	= new Queue();

	private boolean	shutDown		= false;

	private Thread	thread;

	AsyncCache(SignLink signLink) {
		requestCount = 0;
		SignLinkRequest request = signLink.startThread(5, this, 1);
		while (request.status == 0) {
			TimeTools.sleep(0, 10L);
		}
		if ((request.status ^ 0xffffffff) == -3) {
			throw new RuntimeException();
		}
		thread = (Thread) request.result;
	}

	public final CacheFileRequest get(int i, DataFs dataFs, int fileId) {
		CacheFileRequest newRequest = new CacheFileRequest();
		newRequest.opCode = i;
		synchronized (requestQueue) {
			for (CacheFileRequest request = (CacheFileRequest) requestQueue.getFirst(-1); request != null; request = (CacheFileRequest) requestQueue.getNext(0)) {
				if ((fileId ^ 0xffffffffffffffffL) == (request.cachedKey ^ 0xffffffffffffffffL) && dataFs == request.dataFs && (request.opCode ^ 0xffffffff) == -3) {
					newRequest.buffer = request.buffer;
					newRequest.inProgress = false;
					return newRequest;
				}
			}
		}
		newRequest.buffer = dataFs.getFile(fileId, false);
		newRequest.inProgress = false;
		newRequest.aBoolean6037 = true;
		return newRequest;
	}

	public final CacheFileRequest method3176(byte i, int fileId, DataFs dataFs) {
		CacheFileRequest request = new CacheFileRequest();
		request.cachedKey = fileId;
		request.opCode = 3;
		request.dataFs = dataFs;
		request.aBoolean6037 = false;
		postRequest(-12972, request);
		return request;
	}

	private final void postRequest(int i, CacheFileRequest request) {
		synchronized (requestQueue) {
			requestQueue.insert(request, -108);
			requestCount++;
			requestQueue.notifyAll();
		}
	}

	public final CacheFileRequest put(byte[] buffer, byte i, DataFs dataFs, int fileId) {
		CacheFileRequest request = new CacheFileRequest();
		request.cachedKey = fileId;
		request.dataFs = dataFs;
		request.buffer = buffer;
		request.aBoolean6037 = false;
		request.opCode = 2;
		postRequest(-12972, request);
		return request;
	}

	@Override
	public final void run() {
		while (!shutDown) {
			CacheFileRequest request;
			synchronized (requestQueue) {
				request = (CacheFileRequest) requestQueue.remove(-16711936);
				if (request != null) {
					requestCount--;
				} else {
					try {
						requestQueue.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
					continue;
				}
			}
			try {
				if ((request.opCode ^ 0xffffffff) != -3) {
					if (request.opCode == 3) {
						request.buffer = request.dataFs.getFile((int) request.cachedKey, false);
					}
				} else {
					request.dataFs.put(false, request.buffer.length, (int) request.cachedKey, request.buffer);
				}
			} catch (Exception exception) {
				Class305_Sub1.reportError(exception, -123, null);
			}
			request.inProgress = false;
		}
	}

	public final void shutDown(byte i) {
		shutDown = true;
		synchronized (requestQueue) {
			requestQueue.notifyAll();
		}
		try {
			thread.join();
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
		thread = null;
	}
}


/* Class265 - Decompiled by JODE
 */ package com; /*
					*/

import java.net.InetAddress;

import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;

import jagex3.jagmisc.jagmisc;

public final class Class265 implements Runnable {
	public static RtInterface	aClass293_1979;
	public static Class98_Sub49	aClass98_Sub49_1982;
	public static int			anInt1975;
	public static int			friendsChatStatus;
	public static int[]			anIntArray1981;
	public static short			aShort1973;
	public static short[]		clientPalette;

	static {
		aShort1973 = (short) 256;
		clientPalette = new short[256];
		aClass293_1979 = null;
		anIntArray1981 = new int[14];
		friendsChatStatus = 0;
		aClass98_Sub49_1982 = new Class98_Sub49(0, -1);
	}

	public static final boolean method3230(int i, int i_0_) {
		return !((i_0_ ^ 0xffffffff) != -1 && i_0_ != 1 && i_0_ != 2);
	}

	public static void method3231(int i) {
		aClass293_1979 = null;
		anIntArray1981 = null;
		aClass98_Sub49_1982 = null;
	}

	private LinkedList	aClass148_1980	= new LinkedList();

	private Thread		aThread1976;

	public Class265() {
		try {
			aThread1976 = new Thread(this);
			aThread1976.setDaemon(true);
			aThread1976.start();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qia.<init>(" + ')');
		}
	}

	private final void method3228(int i, Node class98) {
		do {
			try {
				synchronized (aClass148_1980) {
					aClass148_1980.addLast(class98, i + -20911);
					aClass148_1980.notify();
				}
				if (i == 0) {
					break;
				}
				method3231(-48);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qia.A(" + i + ',' + (class98 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final Class98_Sub4 method3229(int i, String string) {
		try {
			if (i <= 43) {
				return null;
			}
			if (aThread1976 == null) {
				throw new IllegalStateException("");
			}
			if (string == null) {
				throw new IllegalArgumentException("");
			}
			Class98_Sub4 class98_sub4 = new Class98_Sub4(string);
			method3228(0, class98_sub4);
			return class98_sub4;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qia.D(" + i + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3232(byte i) {
		try {
			if (aThread1976 != null) {
				method3228(0, new Node());
				if (i == -103) {
					try {
						aThread1976.join();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
					aThread1976 = null;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qia.B(" + i + ')');
		}
	}

	@Override
	public final void run() {
		try {
			for (;;) {
				Class98_Sub4 class98_sub4;
				synchronized (aClass148_1980) {
					Node class98;
					for (class98 = aClass148_1980.removeFirst(6494); class98 == null; class98 = aClass148_1980.removeFirst(6494)) {
						try {
							aClass148_1980.wait();
						} catch (InterruptedException interruptedexception) {
							/* empty */
						}
					}
					if (!(class98 instanceof Class98_Sub4)) {
						break;
					}
					class98_sub4 = (Class98_Sub4) class98;
				}
				int i;
				try {
					byte[] is = InetAddress.getByName(class98_sub4.aString3829).getAddress();
					i = jagmisc.ping(is[0], is[1], is[2], is[3], 1000L);
				} catch (Throwable throwable) {
					i = 1000;
				}
				class98_sub4.anInt3827 = i;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qia.run(" + ')');
		}
	}
}

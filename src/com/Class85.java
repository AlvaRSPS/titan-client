
/* Class85 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.input.impl.AwtMouseEvent;

public final class Class85 {
	/* synthetic */ static Class	aClass639;
	public static int				resultBufferPtr;

	public static final boolean loadInterface(int interfaceId, int dummy) {
		if (Class246_Sub3_Sub3_Sub1.loadedInterface[interfaceId]) {
			return true;
		}
		if (!AwtMouseEvent.interfaceJs5.isGroupCached(false, interfaceId)) {
			return false;
		}
		int componentCount = AwtMouseEvent.interfaceJs5.getFileCount(0, interfaceId);
		if (componentCount == 0) {
			Class246_Sub3_Sub3_Sub1.loadedInterface[interfaceId] = true;
			return true;
		}
		if (Class159.interfaceStore[interfaceId] == null) {
			Class159.interfaceStore[interfaceId] = new RtInterface[componentCount];
		}
		for (int componentId = 0; componentId < componentCount; componentId++) {
			if (Class159.interfaceStore[interfaceId][componentId] == null) {
				byte[] data = AwtMouseEvent.interfaceJs5.getFile(componentId, interfaceId, false);
				if (data != null) {
					RtInterface interf = Class159.interfaceStore[interfaceId][componentId] = new RtInterface();
					interf.idDword = (interfaceId << 16) - -componentId;
					if (data[0] != -1) {
						throw new IllegalStateException("Error decoding interface: Old interface format no longer supported!");
					}
					interf.decode(new RSByteBuffer(data), -947);
				}
			}
		}
		Class246_Sub3_Sub3_Sub1.loadedInterface[interfaceId] = true;
		return true;
	}

	public static final void method838(Class84 class84) {
		Class98_Sub10_Sub27.aClass84_5692 = class84;
	}

	/* synthetic */ static Class method840(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public Class85(int i, int i_3_) {
		/* empty */
	}
}

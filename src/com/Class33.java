
/* Class33 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.shadow.OpenGlShadow;
import com.jagex.game.toolkit.shadow.Shadow;

import jaggl.OpenGL;

public final class Class33 {
	public static boolean		aBoolean316	= false;
	public static LinkedList	actionList	= new LinkedList();

	public static final void method323(int[] is, Object[] objects, int i, int i_36_, int i_37_) {
		do {
			if (i != 0) {
				actionList = null;
			}
			if ((i_36_ ^ 0xffffffff) >= (i_37_ ^ 0xffffffff)) {
				break;
			}
			int i_38_ = (i_36_ + i_37_) / 2;
			int i_39_ = i_37_;
			int i_40_ = is[i_38_];
			is[i_38_] = is[i_36_];
			is[i_36_] = i_40_;
			Object object = objects[i_38_];
			objects[i_38_] = objects[i_36_];
			objects[i_36_] = object;
			int i_41_ = i_40_ != 2147483647 ? 1 : 0;
			for (int i_42_ = i_37_; i_36_ > i_42_; i_42_++) {
				if (is[i_42_] < (i_41_ & i_42_) + i_40_) {
					int i_43_ = is[i_42_];
					is[i_42_] = is[i_39_];
					is[i_39_] = i_43_;
					Object object_44_ = objects[i_42_];
					objects[i_42_] = objects[i_39_];
					objects[i_39_++] = object_44_;
				}
			}
			is[i_36_] = is[i_39_];
			is[i_39_] = i_40_;
			objects[i_36_] = objects[i_39_];
			objects[i_39_] = object;
			method323(is, objects, i, i_39_ + -1, i_37_);
			method323(is, objects, 0, i_36_, i_39_ - -1);
			break;
		} while (false);
	}

	byte[]					aByteArray321;
	private Class46[][]		aClass46ArrayArray313;
	private OpenGlToolkit	glToolkit;
	int						anInt314;
	private int				anInt317;
	private int				anInt319;
	private int				anInt322;

	private int				anInt323;

	private OpenGlGround	glGround;

	public Class33(OpenGlToolkit toolkit, OpenGlGround ground) {
		glToolkit = toolkit;
		glGround = ground;
		anInt314 = (glGround.tileUnits * glGround.width >> glToolkit.shadowScale) + 2;
		anInt322 = 2 + (glGround.length * glGround.tileUnits >> glToolkit.shadowScale);
		anInt319 = glToolkit.shadowScale + 7 + -glGround.tileScale;
		aByteArray321 = new byte[anInt314 * anInt322];
		anInt323 = glGround.width >> anInt319;
		anInt317 = glGround.length >> anInt319;
	}

	public final void method320(boolean bool, int i, int i_0_, boolean[][] bools, byte i_1_, int i_2_) {
		if (i_1_ >= 8) {
			glToolkit.method1851(false, false);
			glToolkit.method1875((byte) -127, false);
			glToolkit.method1834(104, -2);
			glToolkit.method1896(260, 1);
			glToolkit.setBlendMode((byte) -93, 1);
			float f = 1.0F / (glToolkit.anInt4318 * 128);
			if (!bool) {
				for (int i_3_ = 0; (i_3_ ^ 0xffffffff) > (anInt317 ^ 0xffffffff); i_3_++) {
					int i_4_ = i_3_ << anInt319;
					int i_5_ = i_3_ - -1 << anInt319;
					for (int i_6_ = 0; (i_6_ ^ 0xffffffff) > (anInt323 ^ 0xffffffff); i_6_++) {
						int i_7_ = 0;
						int i_8_ = i_6_ << anInt319;
						int i_9_ = 1 + i_6_ << anInt319;
						RsFloatBuffer buffer = glToolkit.dataBuffer;
						buffer.position = 0;
						for (int i_10_ = i_4_; (i_5_ ^ 0xffffffff) < (i_10_ ^ 0xffffffff); i_10_++) {
							if ((i_10_ - i_2_ ^ 0xffffffff) <= (-i ^ 0xffffffff) && i_10_ - i_2_ <= i) {
								int i_11_ = i_8_ + i_10_ * glGround.width;
								for (int i_12_ = i_8_; i_12_ < i_9_; i_12_++) {
									if (-i <= -i_0_ + i_12_ && (-i_0_ + i_12_ ^ 0xffffffff) >= (i ^ 0xffffffff) && bools[i_12_ + -i_0_ + i][i + -i_2_ + i_10_]) {
										short[] is = glGround.materialIndices[i_11_];
										if (is != null) {
											if (!glToolkit.bigEndian) {
												for (short element : is) {
													i_7_++;
													buffer.writeLEShortCorrectOne(element & 0xffff, 4);
												}
											} else {
												for (int i_14_ = 0; (i_14_ ^ 0xffffffff) > (is.length ^ 0xffffffff); i_14_++) {
													i_7_++;
													buffer.writeShort(0xffff & is[i_14_], 1571862888);
												}
											}
										}
									}
									i_11_++;
								}
							}
						}
						if ((i_7_ ^ 0xffffffff) < -1) {
							OpenGL.glMatrixMode(5890);
							OpenGL.glLoadIdentity();
							OpenGL.glScalef(f, f, 1.0F);
							OpenGL.glTranslatef(-i_6_ / f, -i_3_ / f, 1.0F);
							OpenGL.glMatrixMode(5888);
							aClass46ArrayArray313[i_6_][i_3_].method438(i_7_, 0, 5123, buffer.payload);
						}
					}
				}
			} else {
				for (int i_15_ = 0; i_15_ < anInt317; i_15_++) {
					int i_16_ = i_15_ << anInt319;
					int i_17_ = i_15_ + 1 << anInt319;
					for (int i_18_ = 0; anInt323 > i_18_; i_18_++) {
						int i_19_ = i_18_ << anInt319;
						int i_20_ = 1 + i_18_ << anInt319;
						while_38_: for (int i_21_ = i_19_; i_20_ > i_21_; i_21_++) {
							if ((-i_0_ + i_21_ ^ 0xffffffff) <= (-i ^ 0xffffffff) && (i ^ 0xffffffff) <= (i_21_ - i_0_ ^ 0xffffffff)) {
								for (int i_22_ = i_16_; i_22_ < i_17_; i_22_++) {
									if ((-i_2_ + i_22_ ^ 0xffffffff) <= (-i ^ 0xffffffff) && i >= i_22_ + -i_2_ && bools[i + i_21_ - i_0_][i + -i_2_ + i_22_]) {
										OpenGL.glMatrixMode(5890);
										OpenGL.glLoadIdentity();
										OpenGL.glScalef(f, f, 1.0F);
										OpenGL.glTranslatef(-i_18_ / f, -i_15_ / f, 1.0F);
										OpenGL.glMatrixMode(5888);
										aClass46ArrayArray313[i_18_][i_15_].method437(6401);
										break while_38_;
									}
								}
							}
						}
					}
				}
			}
			OpenGL.glMatrixMode(5890);
			OpenGL.glLoadIdentity();
			OpenGL.glMatrixMode(5888);
		}
	}

	public final void method321(int i) {
		aClass46ArrayArray313 = new Class46[anInt323][anInt317];
		for (int i_23_ = 0; (anInt317 ^ 0xffffffff) < (i_23_ ^ 0xffffffff); i_23_++) {
			for (int i_24_ = 0; (i_24_ ^ 0xffffffff) > (anInt323 ^ 0xffffffff); i_24_++) {
				aClass46ArrayArray313[i_24_][i_23_] = new Class46(glToolkit, this, glGround, i_24_, i_23_, anInt319, 1 + 128 * i_24_, 128 * i_23_ - -1);
			}
		}
	}

	public final void method322(int i, int i_25_, boolean bool, Shadow var_r) {
		OpenGlShadow var_r_Sub1 = (OpenGlShadow) var_r;
		i_25_ += 1 + var_r_Sub1.z;
		i += 1 + var_r_Sub1.x;
		int i_26_ = anInt314 * i + i_25_;
		int i_27_ = 0;
		int i_28_ = var_r_Sub1.width;
		int i_29_ = var_r_Sub1.length;
		int i_30_ = -i_29_ + anInt314;
		int i_31_ = 0;
		if (i <= 0) {
			int i_32_ = 1 + -i;
			i_28_ -= i_32_;
			i = 1;
			i_26_ += i_32_ * anInt314;
			i_27_ += i_29_ * i_32_;
		}
		if (anInt322 <= i_28_ + i) {
			int i_33_ = i - -i_28_ - (-1 - -anInt322);
			i_28_ -= i_33_;
		}
		if (i_25_ <= 0) {
			int i_34_ = -i_25_ + 1;
			i_29_ -= i_34_;
			i_26_ += i_34_;
			i_31_ += i_34_;
			i_27_ += i_34_;
			i_25_ = 1;
			i_30_ += i_34_;
		}
		if ((i_25_ + i_29_ ^ 0xffffffff) <= (anInt314 ^ 0xffffffff)) {
			int i_35_ = -anInt314 + 1 + i_25_ - -i_29_;
			i_29_ -= i_35_;
			i_30_ += i_35_;
			i_31_ += i_35_;
		}
		if ((i_29_ ^ 0xffffffff) < -1 && i_28_ > 0) {
			Class187.method2637(i_28_, i_26_, i_27_, var_r_Sub1.map, aByteArray321, -18305, i_31_, i_30_, i_29_);
			method326(i_28_, i_29_, i_25_, i, bool);
		}
	}

	public final void delete(byte i, int i_45_, int i_46_, Shadow var_r) {
		if (i <= 87) {
			aBoolean316 = true;
		}
		OpenGlShadow var_r_Sub1 = (OpenGlShadow) var_r;
		i_46_ += var_r_Sub1.z - -1;
		i_45_ += var_r_Sub1.x - -1;
		int i_47_ = i_46_ + anInt314 * i_45_;
		int i_48_ = 0;
		int i_49_ = var_r_Sub1.width;
		int i_50_ = var_r_Sub1.length;
		int i_51_ = anInt314 + -i_50_;
		int i_52_ = 0;
		if ((i_45_ ^ 0xffffffff) >= -1) {
			int i_53_ = 1 - i_45_;
			i_48_ += i_50_ * i_53_;
			i_45_ = 1;
			i_49_ -= i_53_;
			i_47_ += anInt314 * i_53_;
		}
		if ((i_49_ + i_45_ ^ 0xffffffff) <= (anInt322 ^ 0xffffffff)) {
			int i_54_ = -anInt322 + i_49_ + i_45_ + 1;
			i_49_ -= i_54_;
		}
		if (i_46_ <= 0) {
			int i_55_ = -i_46_ + 1;
			i_46_ = 1;
			i_52_ += i_55_;
			i_47_ += i_55_;
			i_50_ -= i_55_;
			i_48_ += i_55_;
			i_51_ += i_55_;
		}
		if (anInt314 <= i_46_ - -i_50_) {
			int i_56_ = i_50_ + i_46_ - (-1 + anInt314);
			i_50_ -= i_56_;
			i_51_ += i_56_;
			i_52_ += i_56_;
		}
		if ((i_50_ ^ 0xffffffff) < -1 && i_49_ > 0) {
			Class76_Sub2.method751(i_52_, aByteArray321, i_49_, var_r_Sub1.map, i_48_, i_51_, i_47_, i_50_, 0);
			method326(i_49_, i_50_, i_46_, i_45_, false);
		}
	}

	public final boolean method325(Shadow var_r, int i, int i_57_, int i_58_) {
		OpenGlShadow var_r_Sub1 = (OpenGlShadow) var_r;
		i_57_ += 1 + var_r_Sub1.z;
		i_58_ += var_r_Sub1.x - -1;
		int i_59_ = i_58_ * anInt314 + i_57_;
		int i_60_ = var_r_Sub1.width;
		int i_61_ = var_r_Sub1.length;
		int i_62_ = -i_61_ + anInt314;
		if (i_58_ <= 0) {
			int i_63_ = 1 - i_58_;
			i_58_ = 1;
			i_59_ += anInt314 * i_63_;
			i_60_ -= i_63_;
		}
		if ((i_58_ + i_60_ ^ 0xffffffff) <= (anInt322 ^ 0xffffffff)) {
			int i_64_ = i_60_ + i_58_ + 1 + -anInt322;
			i_60_ -= i_64_;
		}
		if (i_57_ <= 0) {
			int i_65_ = 1 - i_57_;
			i_61_ -= i_65_;
			i_59_ += i_65_;
			i_62_ += i_65_;
			i_57_ = 1;
		}
		if ((i_57_ - -i_61_ ^ 0xffffffff) <= (anInt314 ^ 0xffffffff)) {
			int i_66_ = i_61_ + i_57_ + 1 + -anInt314;
			i_62_ += i_66_;
			i_61_ -= i_66_;
		}
		if ((i_61_ ^ 0xffffffff) >= -1 || (i_60_ ^ 0xffffffff) >= -1) {
			return false;
		}
		int i_67_ = 8;
		if (i > -109) {
			glGround = null;
		}
		i_62_ += anInt314 * (-1 + i_67_);
		return Class130.method2230(i_67_, i_59_, 1, i_62_, i_61_, aByteArray321, i_60_);
	}

	private final void method326(int i, int i_68_, int i_69_, int i_70_, boolean bool) {
		if (bool == false && aClass46ArrayArray313 != null) {
			int i_71_ = -1 + i_69_ >> -1633322617;
			int i_72_ = i_68_ + i_69_ - 2 >> -1182142265;
			int i_73_ = -1 + i_70_ >> -1931383033;
			int i_74_ = -1 + i_70_ - (1 + -i) >> 950325127;
			for (int i_75_ = i_71_; (i_72_ ^ 0xffffffff) <= (i_75_ ^ 0xffffffff); i_75_++) {
				Class46[] class46s = aClass46ArrayArray313[i_75_];
				for (int i_76_ = i_73_; (i_76_ ^ 0xffffffff) >= (i_74_ ^ 0xffffffff); i_76_++) {
					class46s[i_76_].aBoolean392 = true;
				}
			}
		}
	}
}

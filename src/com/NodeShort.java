
/* Class98_Sub40 - Decompiled by JODE
 */ package com; /*
					*/

import java.util.Date;

import com.jagex.core.collections.Node;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;

public final class NodeShort extends Node {
	public static Class164				aClass164_4190	= new Class164(3);
	public static Class98_Sub46_Sub10	aClass98_Sub46_Sub10_4195;
	public static int					anInt4194;
	public static int					anInt4196;
	public static int					anInt4197;
	public static Js5					huffmanJs5;
	public static OutgoingOpcode		KEEP_ALIVE		= new OutgoingOpcode(12, 0);

	public static final String method1471(int i, long l) {
		ParamDefinition.GMT.setTime(new Date(l));
		int i_8_ = ParamDefinition.GMT.get(7);
		if (i != 5090) {
			method1473(null, -92, null, 85, 126, null);
		}
		int i_9_ = ParamDefinition.GMT.get(5);
		int i_10_ = ParamDefinition.GMT.get(2);
		int i_11_ = ParamDefinition.GMT.get(1);
		int i_12_ = ParamDefinition.GMT.get(11);
		int i_13_ = ParamDefinition.GMT.get(12);
		int i_14_ = ParamDefinition.GMT.get(13);
		return FloorOverlayDefinitionParser.aStringArray304[-1 + i_8_] + ", " + i_9_ / 10 + i_9_ % 10 + "-" + Class98_Sub10_Sub4.aStringArray5550[i_10_] + "-" + i_11_ + " " + i_12_ / 10 + i_12_ % 10 + ":" + i_13_ / 10 + i_13_ % 10 + ":" + i_14_ / 10 + i_14_ % 10 + " GMT";
	}

	public static final boolean method1473(WorldMapInfoDefinition class24, int i, Class98_Sub47 class98_sub47, int i_15_, int i_16_, RSToolkit var_ha) {
		int i_17_ = 2147483647;
		int i_18_ = -2147483648;
		int i_19_ = 2147483647;
		int i_20_ = -2147483648;
		if (class24.anIntArray265 != null) {
			i_17_ = (-WorldMap.anInt2086 + WorldMap.anInt2093) * (class24.anInt244 + class98_sub47.anInt4272 - WorldMap.anInt2091) / (-WorldMap.anInt2091 + WorldMap.anInt2074) + WorldMap.anInt2086;
			i_20_ = WorldMap.anInt2094 + -((class24.anInt248 + class98_sub47.anInt4267 - WorldMap.anInt2090) * (WorldMap.anInt2094 - WorldMap.anInt2077) / (WorldMap.anInt2083 + -WorldMap.anInt2090));
			i_18_ = (class98_sub47.anInt4272 + class24.anInt247 + -WorldMap.anInt2091) * (WorldMap.anInt2093 + -WorldMap.anInt2086) / (WorldMap.anInt2074 + -WorldMap.anInt2091) + WorldMap.anInt2086;
			i_19_ = -((class24.anInt262 + class98_sub47.anInt4267 - WorldMap.anInt2090) * (WorldMap.anInt2094 - WorldMap.anInt2077) / (-WorldMap.anInt2090 + WorldMap.anInt2083)) + WorldMap.anInt2094;
		}
		Sprite sprite = null;
		int i_21_ = 0;
		int i_22_ = 0;
		int i_23_ = 0;
		int i_24_ = 0;
		if (class24.anInt245 != -1) {
			if (class98_sub47.aBoolean4275 && class24.anInt225 != -1) {
				sprite = class24.getSprite((byte) 92, var_ha, true);
			} else {
				sprite = class24.getSprite((byte) 92, var_ha, false);
			}
			if (sprite != null) {
				i_21_ = class98_sub47.anInt4266 - (1 + sprite.getRenderWidth() >> -1309311967);
				if (i_21_ < i_17_) {
					i_17_ = i_21_;
				}
				i_22_ = class98_sub47.anInt4266 - -(sprite.getRenderWidth() - -1 >> -1382798303);
				if (i_18_ < i_22_) {
					i_18_ = i_22_;
				}
				i_23_ = class98_sub47.anInt4271 + -(1 + sprite.getRenderHeight() >> 682299937);
				if ((i_19_ ^ 0xffffffff) < (i_23_ ^ 0xffffffff)) {
					i_19_ = i_23_;
				}
				i_24_ = class98_sub47.anInt4271 - -(1 + sprite.getRenderHeight() >> -261887423);
				if ((i_24_ ^ 0xffffffff) < (i_20_ ^ 0xffffffff)) {
					i_20_ = i_24_;
				}
			}
		}
		RtAwtFontWrapper class326 = null;
		int i_25_ = 0;
		int i_26_ = 0;
		int i_27_ = 0;
		int i_28_ = 0;
		int i_29_ = 0;
		int i_30_ = 0;
		int i_31_ = 0;
		int i_32_ = 0;
		if (class24.locationName != null) {
			class326 = Class105.method1718(class24.anInt264, 5466);
			if (class326 != null) {
				i_25_ = StrongReferenceMCNode.p11FullMetrics.performWordWrap(class24.locationName, Class35.aStringArray335, null, null, -1);
				i_26_ = (-WorldMap.anInt2086 + WorldMap.anInt2093) * class24.anInt235 / (-WorldMap.anInt2091 + WorldMap.anInt2074) + class98_sub47.anInt4266;
				i_27_ = class98_sub47.anInt4271 + -(class24.anInt252 * (-WorldMap.anInt2077 + WorldMap.anInt2094) / (-WorldMap.anInt2090 + WorldMap.anInt2083));
				if (sprite != null) {
					i_27_ -= (sprite.getRenderHeight() >> 1059165249) + class326.getFontHeight() * i_25_;
				} else {
					i_27_ -= class326.method3704() * i_25_ / 2;
				}
				for (int i_33_ = 0; i_25_ > i_33_; i_33_++) {
					String string = Class35.aStringArray335[i_33_];
					if ((i_33_ ^ 0xffffffff) > (i_25_ + -1 ^ 0xffffffff)) {
						string = string.substring(0, -4 + string.length());
					}
					int i_34_ = class326.getStringWidth(string);
					if (i_28_ < i_34_) {
						i_28_ = i_34_;
					}
				}
				i_29_ = i_26_ + -(i_28_ / 2) + i_15_;
				if ((i_29_ ^ 0xffffffff) > (i_17_ ^ 0xffffffff)) {
					i_17_ = i_29_;
				}
				i_30_ = i_15_ + i_28_ / 2 + i_26_;
				i_31_ = i + i_27_;
				if ((i_18_ ^ 0xffffffff) > (i_30_ ^ 0xffffffff)) {
					i_18_ = i_30_;
				}
				i_32_ = i + i_25_ * class326.getFontHeight() + i_27_;
				if ((i_19_ ^ 0xffffffff) < (i_31_ ^ 0xffffffff)) {
					i_19_ = i_31_;
				}
				if ((i_32_ ^ 0xffffffff) < (i_20_ ^ 0xffffffff)) {
					i_20_ = i_32_;
				}
			}
		}
		if (WorldMap.anInt2086 > i_18_ || i_17_ > WorldMap.anInt2093 || WorldMap.anInt2077 > i_20_ || i_19_ > WorldMap.anInt2094) {
			return true;
		}
		WorldMap.method3314(var_ha, class98_sub47, class24);
		if (sprite != null) {
			if ((GroundBlendingPreferenceField.anInt3711 ^ 0xffffffff) < -1 && ((Class98_Sub5_Sub2.anInt5536 ^ 0xffffffff) != 0 && class98_sub47.anInt4268 == Class98_Sub5_Sub2.anInt5536 || (Class256.anInt1945 ^ 0xffffffff) != 0 && (class24.anInt246 ^ 0xffffffff) == (Class256.anInt1945
					^ 0xffffffff))) {
				int i_35_;
				if (Class287.anInt2186 <= 50) {
					i_35_ = Class287.anInt2186 * 2;
				} else {
					i_35_ = 200 + -(Class287.anInt2186 * 2);
				}
				int i_36_ = 0xffff00 | i_35_ << -702181928;
				var_ha.method1757(sprite.getWidth() / 2 + 7, class98_sub47.anInt4271, i_36_, 85, class98_sub47.anInt4266);
				var_ha.method1757(sprite.getWidth() / 2 - -5, class98_sub47.anInt4271, i_36_, 117, class98_sub47.anInt4266);
				var_ha.method1757(sprite.getWidth() / 2 + 3, class98_sub47.anInt4271, i_36_, 92, class98_sub47.anInt4266);
				var_ha.method1757(sprite.getWidth() / 2 - -1, class98_sub47.anInt4271, i_36_, 37, class98_sub47.anInt4266);
				var_ha.method1757(sprite.getWidth() / 2, class98_sub47.anInt4271, i_36_, 95, class98_sub47.anInt4266);
			}
			sprite.draw(class98_sub47.anInt4266 - (sprite.getRenderWidth() >> 160193697), class98_sub47.anInt4271 - (sprite.getRenderHeight() >> -277635711));
		}
		if (class24.locationName != null && class326 != null) {
			Class126.method2217((byte) 12, class98_sub47, i_28_, var_ha, class326, i_27_, class24, i_25_, i_26_);
		}
		if (class24.anInt245 != -1 || class24.locationName != null) {
			Class98_Sub23 class98_sub23 = new Class98_Sub23(class98_sub47);
			class98_sub23.anInt4000 = i_24_;
			class98_sub23.anInt4005 = i_31_;
			class98_sub23.anInt3999 = i_23_;
			class98_sub23.anInt4004 = i_32_;
			class98_sub23.anInt4003 = i_29_;
			class98_sub23.anInt4002 = i_30_;
			class98_sub23.anInt3996 = i_21_;
			class98_sub23.anInt4006 = i_22_;
			InventoriesDefinitionParser.aClass148_110.addLast(class98_sub23, i_16_ ^ ~0x6f9a);
		}
		return false;
	}

	public short aShort4191;

	public NodeShort() {
		/* empty */
	}

	public NodeShort(short i) {
		aShort4191 = i;
	}
}

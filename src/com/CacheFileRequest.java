/* Class98_Sub46_Sub13_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.FileRequest;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;

public final class CacheFileRequest extends FileRequest {
	public static int		anInt6309;
	public static int[][][]	anIntArrayArrayArray6311;

	public static final int method1598(int i, int i_0_) {
		return i >>> 1951350663;
	}

	public static final int method1600(byte i) {
		return RotatingSpriteLSEConfig.anInt5497;
	}

	public static final Class98_Sub46_Sub4 method1601(int i, int i_1_) {
		Class98_Sub46_Sub4 class98_sub46_sub4 = (Class98_Sub46_Sub4) Class38.aClass100_357.method1694((byte) 117, i);
		if (class98_sub46_sub4 != null) {
			return class98_sub46_sub4;
		}
		byte[] is = NewsLSEConfig.clientScriptJs5.getFile(0, i, false);
		if (is == null || is.length <= 1) {
			return null;
		}
		try {
			class98_sub46_sub4 = Class22.method280(is, 0);
		} catch (Exception exception) {
			throw new RuntimeException(exception.getMessage() + " S: " + i);
		}
		Class38.aClass100_357.method1695(26404, class98_sub46_sub4, i);
		return class98_sub46_sub4;
	}

	byte[]	buffer;

	DataFs	dataFs;

	int		opCode;

	public CacheFileRequest() {
		/* empty */
	}

	@Override
	public final byte[] getData(int i) {
		if (i < 5) {
			dataFs = null;
		}
		if (inProgress) {
			throw new RuntimeException();
		}
		return buffer;
	}

	@Override
	public final int getProgress(int i) {
		if (inProgress) {
			return 0;
		}
		return 100;
	}
}

/* Class76_Sub5 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.matrix.NativeMatrix;
import com.jagex.game.toolkit.shadow.NativeShadow;

public final class Class76_Sub5 extends Class76 {
	public static Matrix	aClass111_3745;
	public static double	aDouble3747;
	public static int		anInt3746;
	public static int		anInt3748	= 0;
	public static int[]		anIntArray3743;
	public static int[]		intGlobalConfigs;

	static {
		anIntArray3743 = new int[50];
	}

	public static void method757(byte i) {
		try {
			aClass111_3745 = null;
			if (i == 4) {
				intGlobalConfigs = null;
				anIntArray3743 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jh.A(" + i + ')');
		}
	}

	public static final boolean method758(byte i, int i_3_, int i_4_, int i_5_) {
		try {
			if (!RtMouseEvent.occlusion || !Js5Client.aBoolean1052) {
				return false;
			}
			if (Class4.pixelsOccludedCount < 100) {
				return false;
			}
			int i_6_ = CacheFileRequest.anIntArrayArrayArray6311[i_3_][i_5_][i_4_];
			if ((i_6_ ^ 0xffffffff) == (-FileOnDisk.anInt3020 ^ 0xffffffff)) {
				return false;
			}
			if (FileOnDisk.anInt3020 == i_6_) {
				return true;
			}
			if (Class78.aSArray594 == Class81.aSArray618) {
				return false;
			}
			int i_8_ = i_5_ << Class151_Sub8.tileScale;
			int i_9_ = i_4_ << Class151_Sub8.tileScale;
			if (Class254.method3187(Class78.aSArray594[i_3_].getTileHeight(i_4_ - -1, -12639, i_5_ + 1), 1 + i_8_, i_9_ - -1, (byte) 82, Class78.aSArray594[i_3_].getTileHeight(i_4_, -12639, i_5_), i_9_ - -NativeShadow.anInt6333 + -1, Class78.aSArray594[i_3_].getTileHeight(1 + i_4_, -12639, i_5_),
					i_9_ - (-NativeShadow.anInt6333 - -1), NativeShadow.anInt6333 + i_8_ + -1, 1 + i_8_) && Class254.method3187(Class78.aSArray594[i_3_].getTileHeight(i_4_, -12639, i_5_ + 1), NativeShadow.anInt6333 + i_8_ - 1, i_9_ + 1, (byte) 82, Class78.aSArray594[i_3_].getTileHeight(i_4_, -12639,
							i_5_), -1 + NativeShadow.anInt6333 + i_9_, Class78.aSArray594[i_3_].getTileHeight(1 + i_4_, -12639, i_5_ - -1), i_9_ - -1, -1 + i_8_ - -NativeShadow.anInt6333, 1 + i_8_)) {
				GameObjectDefinitionParser.groundOccludedCount++;
				CacheFileRequest.anIntArrayArrayArray6311[i_3_][i_5_][i_4_] = FileOnDisk.anInt3020;
				return true;
			}
			CacheFileRequest.anIntArrayArrayArray6311[i_3_][i_5_][i_4_] = -FileOnDisk.anInt3020;
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jh.G(" + i + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ')');
		}
	}

	public static final void method759(byte i, int[][] is) {
		do {
			try {
				AnimationDefinition.anIntArrayArray814 = is;
				if (i < -62) {
					break;
				}
				intGlobalConfigs = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "jh.F(" + i + ',' + (is != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	private boolean aBoolean3742 = false;

	Class76_Sub5(NativeToolkit var_ha_Sub3) {
		super(var_ha_Sub3);
	}

	@Override
	public final void method739(int i) {
		do {
			try {
				do {
					if (!aBoolean3742) {
						this.aHa_Sub3_585.method1953(i ^ 0x6b, QuickChat.aClass65_2499, 0);
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					this.aHa_Sub3_585.method1951((byte) 120, 1);
					this.aHa_Sub3_585.method1964(NativeShadow.aClass38_6334, (byte) 26);
					this.aHa_Sub3_585.method2019(Class249.aClass128_1903, Class249.aClass128_1903, 22831);
					this.aHa_Sub3_585.method2051(2, -92, MaxScreenSizePreferenceField.aClass65_3681);
					this.aHa_Sub3_585.method1953(-84, QuickChat.aClass65_2499, 0);
					this.aHa_Sub3_585.method1985(2);
					this.aHa_Sub3_585.method2005(null, i + -116);
					this.aHa_Sub3_585.method1951((byte) 120, 0);
					aBoolean3742 = false;
				} while (false);
				this.aHa_Sub3_585.method2019(Class249.aClass128_1903, Class249.aClass128_1903, 22831);
				if (i == -2) {
					break;
				}
				method757((byte) -25);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "jh.C(" + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method742(int i, int i_2_, Interface4 interface4) {
		try {
			if (i != 6) {
				method746(62, 22, -72);
			}
			this.aHa_Sub3_585.method2005(interface4, 0);
			this.aHa_Sub3_585.method2015(i_2_, (byte) 79);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jh.I(" + i + ',' + i_2_ + ',' + (interface4 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method743(int i, boolean bool) {
		try {
			if (i < 93) {
				method746(-61, 44, -112);
			}
			this.aHa_Sub3_585.method2019(Class288.aClass128_3381, Class249.aClass128_1903, 22831);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jh.D(" + i + ',' + bool + ')');
		}
	}

	@Override
	public final boolean method745(byte i) {
		try {
			return i == 27;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "jh.H(" + i + ')');
		}
	}

	@Override
	public final void method746(int i, int i_0_, int i_1_) {
		do {
			try {
				if (i_1_ < -75) {
					break;
				}
				method757((byte) 40);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "jh.E(" + i + ',' + i_0_ + ',' + i_1_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method748(int i, boolean bool) {
		do {
			try {
				Interface4_Impl3 interface4_impl3 = this.aHa_Sub3_585.method1939(-109);
				do {
					if (interface4_impl3 != null && bool) {
						this.aHa_Sub3_585.method1951((byte) 120, 1);
						this.aHa_Sub3_585.method2005(interface4_impl3, i + -6);
						this.aHa_Sub3_585.method1964(Class204.aClass38_1552, (byte) 26);
						this.aHa_Sub3_585.method1951((byte) 120, 1);
						this.aHa_Sub3_585.method2019(Class288.aClass128_3381, Class323.aClass128_2715, 22831);
						this.aHa_Sub3_585.method2026(2, true, (byte) 27, IncomingOpcode.aClass65_459, false);
						this.aHa_Sub3_585.method1953(-101, Class98_Sub43_Sub3.aClass65_5926, 0);
						NativeMatrix class111_sub3 = this.aHa_Sub3_585.method1957((byte) -101);
						class111_sub3.method2140(this.aHa_Sub3_585.method2023(1), 0);
						this.aHa_Sub3_585.method2008(Class144.aClass258_1168, (byte) 120);
						this.aHa_Sub3_585.method1951((byte) 120, 0);
						aBoolean3742 = true;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					this.aHa_Sub3_585.method1953(-127, Class98_Sub43_Sub3.aClass65_5926, 0);
				} while (false);
				if (i == 69) {
					break;
				}
				method743(-119, true);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "jh.B(" + i + ',' + bool + ')');
			}
			break;
		} while (false);
	}
}

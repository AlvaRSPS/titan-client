
/* Class263 - Decompiled by JODE
 */ package com; /*
					*/

import java.util.zip.Inflater;

public final class GZipDecompressor {
	public static OutgoingOpcode	aClass171_1964	= new OutgoingOpcode(60, 7);
	public static double			aDouble1966;
	public static int				anInt1965;
	public static int				anInt1967;

	public static final char method3217(int i, char c) {
		if ((c ^ 0xffffffff) == -199) {
			return 'E';
		}
		if ((c ^ 0xffffffff) == -231) {
			return 'e';
		}
		if (c == 223) {
			return 's';
		}
		if (c == 338) {
			return 'E';
		}
		if ((c ^ 0xffffffff) == -340) {
			return 'e';
		}
		if (i != 14561) {
			anInt1965 = 124;
		}
		return '\0';
	}

	public static final int method3219(boolean bool, int i, int i_6_, int i_7_) {
		if (bool != false) {
			aDouble1966 = 0.7636388514984377;
		}
		if ((i_6_ ^ 0xffffffff) >= (i_7_ ^ 0xffffffff)) {
			if (i >= i_7_) {
				return i_7_;
			}
			return i;
		}
		return i_6_;
	}

	public static void method3221(int i) {
		if (i > -97) {
			aDouble1966 = 0.1934401666895311;
		}
		aClass171_1964 = null;
	}

	private Inflater inflater;

	public GZipDecompressor() {
		this(-1, 1000000, 1000000);
	}

	private GZipDecompressor(int i, int i_10_, int i_11_) {
		/* empty */
	}

	public final byte[] decompress(byte i, byte[] is) {
		RSByteBuffer packet = new RSByteBuffer(is);
		packet.position = -4 + is.length;
		int i_8_ = packet.method1202((byte) -63);
		byte[] out = new byte[i_8_];
		packet.position = 0;
		this.decompress(out, packet, 18762);
		return out;
	}

	public final void decompress(byte[] data, RSByteBuffer packet, int i) {
		if (i == 18762) {
			if ((packet.payload[packet.position] ^ 0xffffffff) != -32 || (packet.payload[packet.position - -1] ^ 0xffffffff) != 116) {
				throw new RuntimeException("Invalid GZIP header!");
			}
			if (inflater == null) {
				inflater = new Inflater(true);
			}
			try {
				inflater.setInput(packet.payload, 10 + packet.position, -18 - packet.position + packet.payload.length);
				inflater.inflate(data);
			} catch (Exception exception) {
				inflater.reset();
				throw new RuntimeException("Invalid GZIP compressed data!");
			}
			inflater.reset();
		}
	}
}

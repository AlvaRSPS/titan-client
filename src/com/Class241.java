/* Class241 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.GamePreferences;

public final class Class241 {
	public static int		anInt1845;
	public static int[]		anIntArray1846	= { 19, 55, 38, 155, 255, 110, 137, 205, 76 };
	public static Object	anObject1847;

	public static final boolean method2931(int i, int i_0_, int i_1_) {
		try {
			if (i_0_ != 262144) {
				anInt1845 = 31;
			}
			return !(!(RSByteBuffer.method1241(false, i, i_1_) | (0x40000 & i_1_) != 0) && !GamePreferences.method1292(i, (byte) 116, i_1_));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pe.B(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static void method2932(int i) {
		try {
			if (i != 14441) {
				anIntArray1846 = null;
			}
			anIntArray1846 = null;
			anObject1847 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pe.A(" + i + ')');
		}
	}
}

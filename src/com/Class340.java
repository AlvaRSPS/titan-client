
/* Class340 - Decompiled by JODE
 */ package com; /*
					*/

import java.lang.reflect.Method;

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.GraphicsDefinition;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.SkyboxDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub3;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.javasw.buffer.impl.AwtGraphicsBuffer;
import com.jagex.game.toolkit.matrix.NativeMatrix;

public final class Class340 {
	public static Js5				aClass207_2847;
	/* synthetic */ static Class	aClass2850;
	/* synthetic */ static Class	aClass2851;
	public static int				anInt2849	= 0;
	public static Sprite[]			mapFlag;

	public static final void method3801(Mob class246_sub3_sub4_sub2, int i) {
		try {
			if (i != -28111) {
				mapFlag = null;
			}
			do {
				if (class246_sub3_sub4_sub2.anInt6385 != -1) {
					AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class246_sub3_sub4_sub2.anInt6385, 16383);
					if (class97 != null && class97.frameIds != null) {
						class246_sub3_sub4_sub2.anInt6409++;
						if ((class246_sub3_sub4_sub2.anInt6350 ^ 0xffffffff) > (class97.frameIds.length ^ 0xffffffff) && class246_sub3_sub4_sub2.anInt6409 > class97.frameLengths[class246_sub3_sub4_sub2.anInt6350]) {
							class246_sub3_sub4_sub2.anInt6409 = 1;
							class246_sub3_sub4_sub2.anInt6350++;
							class246_sub3_sub4_sub2.anInt6419++;
							if (!class246_sub3_sub4_sub2.aBoolean6371) {
								Class349.method3840((byte) -128, class246_sub3_sub4_sub2, class246_sub3_sub4_sub2.anInt6350, class97);
							}
						}
						if ((class97.frameIds.length ^ 0xffffffff) >= (class246_sub3_sub4_sub2.anInt6350 ^ 0xffffffff)) {
							class246_sub3_sub4_sub2.anInt6350 = 0;
							class246_sub3_sub4_sub2.anInt6409 = 0;
							if (class246_sub3_sub4_sub2.aBoolean6359) {
								class246_sub3_sub4_sub2.anInt6385 = class246_sub3_sub4_sub2.method3039(1).method3478(i + 28148);
								if ((class246_sub3_sub4_sub2.anInt6385 ^ 0xffffffff) == 0) {
									class246_sub3_sub4_sub2.aBoolean6359 = false;
									break;
								}
								class97 = Class151_Sub7.animationDefinitionList.method2623(class246_sub3_sub4_sub2.anInt6385, i + 44494);
							}
							if (!class246_sub3_sub4_sub2.aBoolean6371) {
								Class349.method3840((byte) -126, class246_sub3_sub4_sub2, class246_sub3_sub4_sub2.anInt6350, class97);
							}
						}
						class246_sub3_sub4_sub2.anInt6419 = class246_sub3_sub4_sub2.anInt6350 + 1;
						if (class97.frameIds != null) {
							if (class246_sub3_sub4_sub2.anInt6419 >= class97.frameIds.length) {
								class246_sub3_sub4_sub2.anInt6419 = 0;
							}
						} else {
							class246_sub3_sub4_sub2.aBoolean6359 = false;
							class246_sub3_sub4_sub2.anInt6385 = -1;
						}
					} else {
						class246_sub3_sub4_sub2.aBoolean6359 = false;
						class246_sub3_sub4_sub2.anInt6385 = -1;
					}
				}
			} while (false);
			do {
				if (class246_sub3_sub4_sub2.anInt6379 != -1 && class246_sub3_sub4_sub2.anInt6391 <= Queue.timer) {
					GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, class246_sub3_sub4_sub2.anInt6379);
					int i_0_ = class107.animation;
					if ((i_0_ ^ 0xffffffff) != 0) {
						AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_0_, 16383);
						if (class107.aBoolean909) {
							if (class97.resetWhenWalk == 3) {
								if (class246_sub3_sub4_sub2.anInt6436 > 0 && (class246_sub3_sub4_sub2.anInt6390 ^ 0xffffffff) >= (Queue.timer ^ 0xffffffff) && Queue.timer > class246_sub3_sub4_sub2.anInt6424) {
									class246_sub3_sub4_sub2.anInt6379 = -1;
									break;
								}
							} else if (class97.resetWhenWalk == 1 && class246_sub3_sub4_sub2.anInt6436 > 0 && (class246_sub3_sub4_sub2.anInt6390 ^ 0xffffffff) >= (Queue.timer ^ 0xffffffff) && (Queue.timer ^ 0xffffffff) < (class246_sub3_sub4_sub2.anInt6424 ^ 0xffffffff)) {
								class246_sub3_sub4_sub2.anInt6391 = Queue.timer + 1;
								break;
							}
						}
						if (class97 != null && class97.frameIds != null) {
							if (class246_sub3_sub4_sub2.anInt6376 < 0) {
								class246_sub3_sub4_sub2.anInt6376 = 0;
								if (!class246_sub3_sub4_sub2.aBoolean6371) {
									Class349.method3840((byte) -126, class246_sub3_sub4_sub2, 0, class97);
								}
							}
							class246_sub3_sub4_sub2.anInt6396++;
							if (class97.frameIds.length > class246_sub3_sub4_sub2.anInt6376 && (class246_sub3_sub4_sub2.anInt6396 ^ 0xffffffff) < (class97.frameLengths[class246_sub3_sub4_sub2.anInt6376] ^ 0xffffffff)) {
								class246_sub3_sub4_sub2.anInt6376++;
								class246_sub3_sub4_sub2.anInt6396 = 1;
								if (!class246_sub3_sub4_sub2.aBoolean6371) {
									Class349.method3840((byte) -127, class246_sub3_sub4_sub2, class246_sub3_sub4_sub2.anInt6376, class97);
								}
							}
							if ((class97.frameIds.length ^ 0xffffffff) >= (class246_sub3_sub4_sub2.anInt6376 ^ 0xffffffff)) {
								if (!class107.aBoolean909) {
									class246_sub3_sub4_sub2.anInt6379 = -1;
								} else {
									class246_sub3_sub4_sub2.anInt6356++;
									class246_sub3_sub4_sub2.anInt6376 -= class97.frameStep;
									if (class97.anInt807 > class246_sub3_sub4_sub2.anInt6356) {
										if (class246_sub3_sub4_sub2.anInt6376 < 0 || class97.frameIds.length <= class246_sub3_sub4_sub2.anInt6376) {
											class246_sub3_sub4_sub2.anInt6379 = -1;
										} else if (!class246_sub3_sub4_sub2.aBoolean6371) {
											Class349.method3840((byte) -127, class246_sub3_sub4_sub2, class246_sub3_sub4_sub2.anInt6376, class97);
										}
									} else {
										class246_sub3_sub4_sub2.anInt6379 = -1;
									}
								}
							}
							class246_sub3_sub4_sub2.anInt6367 = class246_sub3_sub4_sub2.anInt6376 - -1;
							if ((class246_sub3_sub4_sub2.anInt6367 ^ 0xffffffff) <= (class97.frameIds.length ^ 0xffffffff)) {
								if (class107.aBoolean909) {
									class246_sub3_sub4_sub2.anInt6367 -= class97.frameStep;
									if ((class97.anInt807 ^ 0xffffffff) < (class246_sub3_sub4_sub2.anInt6356 - -1 ^ 0xffffffff)) {
										if (class246_sub3_sub4_sub2.anInt6367 < 0 || (class97.frameIds.length ^ 0xffffffff) >= (class246_sub3_sub4_sub2.anInt6367 ^ 0xffffffff)) {
											class246_sub3_sub4_sub2.anInt6367 = -1;
										}
									} else {
										class246_sub3_sub4_sub2.anInt6367 = -1;
									}
								} else {
									class246_sub3_sub4_sub2.anInt6367 = -1;
								}
							}
						} else {
							class246_sub3_sub4_sub2.anInt6379 = -1;
						}
					} else {
						class246_sub3_sub4_sub2.anInt6379 = -1;
					}
				}
			} while (false);
			do {
				if (class246_sub3_sub4_sub2.anInt6365 != -1 && Queue.timer >= class246_sub3_sub4_sub2.anInt6426) {
					GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, class246_sub3_sub4_sub2.anInt6365);
					int i_1_ = class107.animation;
					if (i_1_ != -1) {
						AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_1_, 16383);
						if (class107.aBoolean909) {
							if ((class97.resetWhenWalk ^ 0xffffffff) == -4) {
								if (class246_sub3_sub4_sub2.anInt6436 > 0 && Queue.timer >= class246_sub3_sub4_sub2.anInt6390 && (Queue.timer ^ 0xffffffff) < (class246_sub3_sub4_sub2.anInt6424 ^ 0xffffffff)) {
									class246_sub3_sub4_sub2.anInt6365 = -1;
									break;
								}
							} else if (class97.resetWhenWalk == 1 && (class246_sub3_sub4_sub2.anInt6436 ^ 0xffffffff) < -1 && Queue.timer >= class246_sub3_sub4_sub2.anInt6390 && (Queue.timer ^ 0xffffffff) < (class246_sub3_sub4_sub2.anInt6424 ^ 0xffffffff)) {
								class246_sub3_sub4_sub2.anInt6426 = Queue.timer + 1;
								break;
							}
						}
						if (class97 == null || class97.frameIds == null) {
							class246_sub3_sub4_sub2.anInt6365 = -1;
						} else {
							if (class246_sub3_sub4_sub2.anInt6428 < 0) {
								class246_sub3_sub4_sub2.anInt6428 = 0;
								if (!class246_sub3_sub4_sub2.aBoolean6371) {
									Class349.method3840((byte) -127, class246_sub3_sub4_sub2, 0, class97);
								}
							}
							class246_sub3_sub4_sub2.anInt6427++;
							if ((class97.frameIds.length ^ 0xffffffff) < (class246_sub3_sub4_sub2.anInt6428 ^ 0xffffffff) && class97.frameLengths[class246_sub3_sub4_sub2.anInt6428] < class246_sub3_sub4_sub2.anInt6427) {
								class246_sub3_sub4_sub2.anInt6428++;
								class246_sub3_sub4_sub2.anInt6427 = 1;
								if (!class246_sub3_sub4_sub2.aBoolean6371) {
									Class349.method3840((byte) -126, class246_sub3_sub4_sub2, class246_sub3_sub4_sub2.anInt6428, class97);
								}
							}
							if ((class246_sub3_sub4_sub2.anInt6428 ^ 0xffffffff) <= (class97.frameIds.length ^ 0xffffffff)) {
								if (class107.aBoolean909) {
									class246_sub3_sub4_sub2.anInt6380++;
									class246_sub3_sub4_sub2.anInt6428 -= class97.frameStep;
									if ((class97.anInt807 ^ 0xffffffff) >= (class246_sub3_sub4_sub2.anInt6380 ^ 0xffffffff)) {
										class246_sub3_sub4_sub2.anInt6365 = -1;
									} else if ((class246_sub3_sub4_sub2.anInt6428 ^ 0xffffffff) > -1 || class97.frameIds.length <= class246_sub3_sub4_sub2.anInt6428) {
										class246_sub3_sub4_sub2.anInt6365 = -1;
									} else if (!class246_sub3_sub4_sub2.aBoolean6371) {
										Class349.method3840((byte) -128, class246_sub3_sub4_sub2, class246_sub3_sub4_sub2.anInt6428, class97);
									}
								} else {
									class246_sub3_sub4_sub2.anInt6365 = -1;
								}
							}
							class246_sub3_sub4_sub2.anInt6421 = class246_sub3_sub4_sub2.anInt6428 + 1;
							if (class246_sub3_sub4_sub2.anInt6421 >= class97.frameIds.length) {
								if (class107.aBoolean909) {
									class246_sub3_sub4_sub2.anInt6421 -= class97.frameStep;
									if ((class97.anInt807 ^ 0xffffffff) >= (1 + class246_sub3_sub4_sub2.anInt6380 ^ 0xffffffff)) {
										class246_sub3_sub4_sub2.anInt6421 = -1;
									} else if (class246_sub3_sub4_sub2.anInt6421 < 0 || (class97.frameIds.length ^ 0xffffffff) >= (class246_sub3_sub4_sub2.anInt6421 ^ 0xffffffff)) {
										class246_sub3_sub4_sub2.anInt6421 = -1;
									}
								} else {
									class246_sub3_sub4_sub2.anInt6421 = -1;
								}
							}
						}
					} else {
						class246_sub3_sub4_sub2.anInt6365 = -1;
					}
				}
			} while (false);
			if ((class246_sub3_sub4_sub2.anInt6413 ^ 0xffffffff) != 0 && class246_sub3_sub4_sub2.anInt6400 <= 1) {
				AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class246_sub3_sub4_sub2.anInt6413, 16383);
				if (class97.resetWhenWalk == 3) {
					if (class246_sub3_sub4_sub2.anInt6436 > 0 && Queue.timer >= class246_sub3_sub4_sub2.anInt6390 && class246_sub3_sub4_sub2.anInt6424 < Queue.timer) {
						class246_sub3_sub4_sub2.anIntArray6373 = null;
						class246_sub3_sub4_sub2.anInt6413 = -1;
					}
				} else if (class97.resetWhenWalk == 1 && class246_sub3_sub4_sub2.anInt6436 > 0 && (class246_sub3_sub4_sub2.anInt6390 ^ 0xffffffff) >= (Queue.timer ^ 0xffffffff) && (class246_sub3_sub4_sub2.anInt6424 ^ 0xffffffff) > (Queue.timer ^ 0xffffffff)) {
					class246_sub3_sub4_sub2.anInt6400 = 2;
				}
			}
			if (class246_sub3_sub4_sub2.anInt6413 != -1 && (class246_sub3_sub4_sub2.anInt6400 ^ 0xffffffff) == -1) {
				AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class246_sub3_sub4_sub2.anInt6413, 16383);
				if (class97 != null && class97.frameIds != null) {
					class246_sub3_sub4_sub2.anInt6366++;
					if ((class97.frameIds.length ^ 0xffffffff) < (class246_sub3_sub4_sub2.anInt6393 ^ 0xffffffff) && class97.frameLengths[class246_sub3_sub4_sub2.anInt6393] < class246_sub3_sub4_sub2.anInt6366) {
						class246_sub3_sub4_sub2.anInt6366 = 1;
						class246_sub3_sub4_sub2.anInt6393++;
						if (!class246_sub3_sub4_sub2.aBoolean6371) {
							Class349.method3840((byte) -126, class246_sub3_sub4_sub2, class246_sub3_sub4_sub2.anInt6393, class97);
						}
					}
					if (class246_sub3_sub4_sub2.anInt6393 >= class97.frameIds.length) {
						class246_sub3_sub4_sub2.anInt6405++;
						class246_sub3_sub4_sub2.anInt6393 -= class97.frameStep;
						if ((class97.anInt807 ^ 0xffffffff) < (class246_sub3_sub4_sub2.anInt6405 ^ 0xffffffff)) {
							if ((class246_sub3_sub4_sub2.anInt6393 ^ 0xffffffff) > -1 || (class97.frameIds.length ^ 0xffffffff) >= (class246_sub3_sub4_sub2.anInt6393 ^ 0xffffffff)) {
								class246_sub3_sub4_sub2.anInt6413 = -1;
								class246_sub3_sub4_sub2.anIntArray6373 = null;
							} else if (!class246_sub3_sub4_sub2.aBoolean6371) {
								Class349.method3840((byte) -128, class246_sub3_sub4_sub2, class246_sub3_sub4_sub2.anInt6393, class97);
							}
						} else {
							class246_sub3_sub4_sub2.anIntArray6373 = null;
							class246_sub3_sub4_sub2.anInt6413 = -1;
						}
					}
					class246_sub3_sub4_sub2.anInt6361 = 1 + class246_sub3_sub4_sub2.anInt6393;
					if (class246_sub3_sub4_sub2.anInt6361 >= class97.frameIds.length) {
						class246_sub3_sub4_sub2.anInt6361 -= class97.frameStep;
						if ((class97.anInt807 ^ 0xffffffff) < (1 + class246_sub3_sub4_sub2.anInt6405 ^ 0xffffffff)) {
							if ((class246_sub3_sub4_sub2.anInt6361 ^ 0xffffffff) > -1 || (class246_sub3_sub4_sub2.anInt6361 ^ 0xffffffff) <= (class97.frameIds.length ^ 0xffffffff)) {
								class246_sub3_sub4_sub2.anInt6361 = -1;
							}
						} else {
							class246_sub3_sub4_sub2.anInt6361 = -1;
						}
					}
				} else {
					class246_sub3_sub4_sub2.anIntArray6373 = null;
					class246_sub3_sub4_sub2.anInt6413 = -1;
				}
			}
			if ((class246_sub3_sub4_sub2.anInt6400 ^ 0xffffffff) < -1) {
				class246_sub3_sub4_sub2.anInt6400--;
			}
			for (int i_2_ = 0; i_2_ < class246_sub3_sub4_sub2.aClass226Array6387.length; i_2_++) {
				Class226 class226 = class246_sub3_sub4_sub2.aClass226Array6387[i_2_];
				if (class226 != null) {
					if (class226.anInt1703 > 0) {
						class226.anInt1703--;
					} else {
						AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class226.anInt1700, i + 44494);
						if (class97 != null && class97.frameIds != null) {
							class226.anInt1707++;
							if (class226.anInt1702 < class97.frameIds.length && class97.frameLengths[class226.anInt1702] < class226.anInt1707) {
								class226.anInt1707 = 1;
								class226.anInt1702++;
								if (!class246_sub3_sub4_sub2.aBoolean6371) {
									Class349.method3840((byte) -128, class246_sub3_sub4_sub2, class226.anInt1702, class97);
								}
							}
							if (class97.frameIds.length <= class226.anInt1702) {
								class226.anInt1704++;
								class226.anInt1702 -= class97.frameStep;
								if (class97.anInt807 <= class226.anInt1704) {
									class246_sub3_sub4_sub2.aClass226Array6387[i_2_] = null;
								} else if (class226.anInt1702 >= 0 && class97.frameIds.length > class226.anInt1702) {
									if (!class246_sub3_sub4_sub2.aBoolean6371) {
										Class349.method3840((byte) -127, class246_sub3_sub4_sub2, class226.anInt1702, class97);
									}
								} else {
									class246_sub3_sub4_sub2.aClass226Array6387[i_2_] = null;
								}
							}
							class226.anInt1701 = class226.anInt1702 - -1;
							if ((class97.frameIds.length ^ 0xffffffff) >= (class226.anInt1701 ^ 0xffffffff)) {
								class226.anInt1701 -= class97.frameStep;
								if ((1 + class226.anInt1704 ^ 0xffffffff) <= (class97.anInt807 ^ 0xffffffff)) {
									class226.anInt1701 = -1;
								} else if ((class226.anInt1701 ^ 0xffffffff) > -1 || class97.frameIds.length <= class226.anInt1701) {
									class226.anInt1701 = -1;
								}
							}
						} else {
							class246_sub3_sub4_sub2.aClass226Array6387[i_2_] = null;
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uo.D(" + (class246_sub3_sub4_sub2 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public static final void method3802(RSToolkit var_ha, int i, byte i_3_) {
		do {
			try {
				if (i_3_ != -59) {
					mapFlag = null;
				}
				if (!RtMouseEvent.occlusion || !Js5Client.aBoolean1052) {
					SkyboxDefinitionParser.activeOccludersCount = 0;
				} else {
					if (DecoratedProgressBarLSEConfig.aBoolean5479) {
						Class98_Sub42.aLong4212 = GlobalPlayer.aClass142_3174.method2308((byte) 69);
					}
					Class151_Sub7.cpsOccludedCount = 0;
					GameObjectDefinitionParser.groundOccludedCount = 0;
					Class98_Sub16.wallsOccludedCount = 0;
					int[] is = var_ha.Y();
					Class38.anInt358 = (int) (is[2] / 3.0F);
					Class331.anInt2800 = (int) (is[3] / 3.0F);
					var_ha.method1801(Class98_Sub46_Sub4.anIntArray5960);
					if (Class64_Sub3.anInt3646 != (int) (Class98_Sub46_Sub4.anIntArray5960[0] / 3.0F) || (IncomingOpcode.anInt461 ^ 0xffffffff) != ((int) (Class98_Sub46_Sub4.anIntArray5960[1] / 3.0F) ^ 0xffffffff)) {
						IncomingOpcode.anInt461 = (int) (Class98_Sub46_Sub4.anIntArray5960[1] / 3.0F);
						Class64_Sub3.anInt3646 = (int) (Class98_Sub46_Sub4.anIntArray5960[0] / 3.0F);
						NativeMatrix.anIntArray4707 = new int[IncomingOpcode.anInt461 * Class64_Sub3.anInt3646];
						Class2.anInt69 = Class64_Sub3.anInt3646 >> 427328737;
						Class98_Sub10_Sub23.anInt5659 = IncomingOpcode.anInt461 >> 942536705;
					}
					Js5Exception.aClass111_3203 = var_ha.method1752();
					SkyboxDefinitionParser.activeOccludersCount = 0;
					for (int i_4_ = 0; Class21_Sub3.anInt5389 > i_4_; i_4_++) {
						Class271.method3278(3, i, var_ha, Class258.aClass155Array1951[i_4_]);
					}
					for (int i_5_ = 0; i_5_ < Class336.anInt2820; i_5_++) {
						Class271.method3278(3, i, var_ha, Class98_Sub30.aClass155Array4099[i_5_]);
					}
					for (int i_6_ = 0; (RSToolkit.anInt936 ^ 0xffffffff) < (i_6_ ^ 0xffffffff); i_6_++) {
						Class271.method3278(3, i, var_ha, AwtGraphicsBuffer.aClass155Array5889[i_6_]);
					}
					Class4.pixelsOccludedCount = 0;
					if (SkyboxDefinitionParser.activeOccludersCount > 0) {
						int i_7_ = NativeMatrix.anIntArray4707.length;
						int i_8_ = -i_7_ + i_7_ & 0x7;
						int i_9_ = 0;
						while ((i_9_ ^ 0xffffffff) > (i_8_ ^ 0xffffffff)) {
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
						}
						while (i_7_ > i_9_) {
							NativeMatrix.anIntArray4707[i_9_++] = 2147483647;
						}
						Class287.anInt2190 = 1;
						for (int i_10_ = 0; (SkyboxDefinitionParser.activeOccludersCount ^ 0xffffffff) < (i_10_ ^ 0xffffffff); i_10_++) {
							Class155 class155 = Huffman.aClass155Array1611[i_10_];
							StringConcatenator.method3243(class155.aShortArray1244[1], class155.aShortArray1244[0], (byte) 85, class155.aShortArray1234[1], class155.aShortArray1235[3], class155.aShortArray1235[0], class155.aShortArray1244[3], class155.aShortArray1235[1], class155.aShortArray1234[0],
									class155.aShortArray1234[3]);
							StringConcatenator.method3243(class155.aShortArray1244[2], class155.aShortArray1244[1], (byte) 81, class155.aShortArray1234[2], class155.aShortArray1235[3], class155.aShortArray1235[1], class155.aShortArray1244[3], class155.aShortArray1235[2], class155.aShortArray1234[1],
									class155.aShortArray1234[3]);
						}
						Class287.anInt2190 = 2;
					}
					if (!DecoratedProgressBarLSEConfig.aBoolean5479) {
						break;
					}
					Class249.occludeCalculationTime = GlobalPlayer.aClass142_3174.method2308((byte) 69) - Class98_Sub42.aLong4212;
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "uo.B(" + (var_ha != null ? "{...}" : "null") + ',' + i + ',' + i_3_ + ')');
			}
			break;
		} while (false);
	}

	public static final synchronized void method3803(boolean bool) {
		try {
			if (bool == false && Class98_Sub10_Sub22.anObject5653 == null) {
				try {
					Class var_class = Class.forName("java.lang.management.ManagementFactory");
					Method method = var_class.getDeclaredMethod("getPlatformMBeanServer");
					Object object = method.invoke(null);
					Method method_11_ = var_class.getMethod("newPlatformMXBeanProxy", Class.forName("javax.management.MBeanServerConnection"), aClass2850 != null ? aClass2850 : (aClass2850 = method3805("java.lang.String")), aClass2851 != null ? aClass2851
							: (aClass2851 = method3805("java.lang.Class")));
					Class98_Sub10_Sub22.anObject5653 = method_11_.invoke(null, object, "com.sun.management:type=HotSpotDiagnostic", Class.forName("com.sun.management.HotSpotDiagnosticMXBean"));
				} catch (Exception exception) {
					System.out.println("HeapDump setup error:");
					exception.printStackTrace();
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uo.C(" + bool + ')');
		}
	}

	public static void method3804(byte i) {
		try {
			mapFlag = null;
			if (i == -71) {
				aClass207_2847 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uo.A(" + i + ')');
		}
	}

	/* synthetic */
	public static Class method3805(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}
}

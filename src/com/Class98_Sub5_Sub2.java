/* Class98_Sub5_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatCategory;

public final class Class98_Sub5_Sub2 extends PointLight {
	public static double	aDouble5537;
	public static int		anInt5536	= -1;

	public static final String method968(int i, String[] strings, int i_0_, int i_1_) {
		try {
			if ((i ^ 0xffffffff) == -1) {
				return "";
			}
			if (i == 1) {
				String string = strings[i_0_];
				if (string == null) {
					return "null";
				}
				return string.toString();
			}
			int i_2_ = i_0_ + i;
			int i_3_ = 0;
			for (int i_4_ = i_0_; (i_4_ ^ 0xffffffff) > (i_2_ ^ 0xffffffff); i_4_++) {
				String string = strings[i_4_];
				if (string == null) {
					i_3_ += 4;
				} else {
					i_3_ += string.length();
				}
			}
			StringBuffer stringbuffer = new StringBuffer(i_3_);
			for (int i_5_ = i_0_; i_2_ > i_5_; i_5_++) {
				String string = strings[i_5_];
				if (string == null) {
					stringbuffer.append("null");
				} else {
					stringbuffer.append(string);
				}
			}
			if (i_1_ != -17120) {
				method970(-109, null, -36, -75);
			}
			return stringbuffer.toString();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "im.M(" + i + ',' + (strings != null ? "{...}" : "null") + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static final boolean method969(int i, int i_6_, int i_7_) {
		try {
			if (i_7_ < 70) {
				aDouble5537 = -0.3046933013113084;
			}
			return (0x84080 & i_6_) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "im.K(" + i + ',' + i_6_ + ',' + i_7_ + ')');
		}
	}

	public static final void method970(int i, RtInterface class293, int i_8_, int i_9_) {
		try {
			if (class293 != null) {
				if (i_9_ != -6838) {
					method968(67, null, -92, 84);
				}
				if (class293.anObjectArray2257 != null) {
					ClientScript2Event class98_sub21 = new ClientScript2Event();
					class98_sub21.component = class293;
					class98_sub21.param = class293.anObjectArray2257;
					ClientScript2Runtime.handleEvent(class98_sub21);
				}
				Class98_Sub10_Sub9.aBoolean5585 = true;
				QuickChatCategory.anInt5945 = i;
				Class98_Sub4.anInt3826 = i_8_;
				Class187.anInt1450 = class293.idDword;
				Class21_Sub2.cursorId = class293.anInt2318;
				Class310.anInt2652 = class293.componentIndex;
				GlobalPlayer.anInt3173 = class293.unknown;
				Class336.anInt2823 = class293.anInt2309;
				WorldMapInfoDefinitionParser.setDirty(i_9_ ^ ~0x1ab4, class293);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "im.L(" + i + ',' + (class293 != null ? "{...}" : "null") + ',' + i_8_ + ',' + i_9_ + ')');
		}
	}

	Class98_Sub5_Sub2(int i, int i_10_, int i_11_, int i_12_, int i_13_, float f) {
		super(i, i_10_, i_11_, i_12_, i_13_, f);
	}

	@Override
	public final void reposition(int i, byte i_14_, int i_15_, int i_16_) {
		try {
			if (i_14_ >= -120) {
				method970(-97, null, 25, 43);
			}
			this._x = i_15_;
			this._y = i_16_;
			this._z = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "im.A(" + i + ',' + i_14_ + ',' + i_15_ + ',' + i_16_ + ')');
		}
	}

	@Override
	public final void setIntensity(float f, int i) {
		try {
			if (i < 12) {
				method970(-34, null, 53, 96);
			}
			this.intensity = f;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "im.D(" + f + ',' + i + ')');
		}
	}
}

/* Class232 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;

public final class Class232 {
	public static boolean				aBoolean1744		= false;
	public static boolean[]				aBooleanArray1741	= new boolean[100];
	private static char[]				aCharArray1745		= new char[64];
	public static AdvancedMemoryCache	aClass79_1740		= new AdvancedMemoryCache(20);
	public static byte					kickRequirement;

	public static IncomingOpcode		SEND_FRIEND			= new IncomingOpcode(10, -2);

	static {
		for (int i = 0; (i ^ 0xffffffff) > -27; i++) {
			aCharArray1745[i] = (char) (i + 65);
		}
		for (int i = 26; i < 52; i++) {
			aCharArray1745[i] = (char) (97 + i + -26);
		}
		for (int i = 52; i < 62; i++) {
			aCharArray1745[i] = (char) (48 + i + -52);
		}
		aCharArray1745[63] = '/';
		aCharArray1745[62] = '+';
	}

	public static final void method2882(int i, int i_0_, boolean bool) {
		ClientInventory class98_sub3 = WaterDetailPreferenceField.get(i_0_, bool, 6);
		if (class98_sub3 != null) {
			for (int i_1_ = 0; i_1_ < class98_sub3.anIntArray3824.length; i_1_++) {
				class98_sub3.anIntArray3824[i_1_] = -1;
				class98_sub3.anIntArray3823[i_1_] = 0;
			}
		}
	}

	public Class232() {
		/* empty */
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

/* Class87 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.WorldMapInfoDefinition;

public final class Class87 {
	public static OutgoingOpcode	CHAT_OUTGOING_PACKET;
	public static int				anInt673;
	public static Player			localPlayer;

	static {
		CHAT_OUTGOING_PACKET = new OutgoingOpcode(16, -1);
		anInt673 = 1400;
	}

	public static void method853(int i) {
		try {
			CHAT_OUTGOING_PACKET = null;
			if (i > -5) {
				method854(-66, -83, -85);
			}
			localPlayer = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fk.C(" + i + ')');
		}
	}

	public static final boolean method854(int i, int i_0_, int i_1_) {
		try {
			if (i_0_ != 28733) {
				return true;
			}
			return (i_1_ & 0x800 ^ 0xffffffff) != -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fk.B(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static final boolean method855(int i, WorldMapInfoDefinition class24) {
		try {
			if (class24 == null) {
				return false;
			}
			if (!class24.aBoolean258) {
				return false;
			}
			if (i <= 73) {
				method853(126);
			}
			if (!class24.method284(64, WorldMap.anInterface6_2060)) {
				return false;
			}
			if (Class248.aClass377_1894.get(class24.anInt228, -1) != null) {
				return false;
			}
			return BConfigDefinition.aClass377_3114.get(class24.anInt246, -1) == null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fk.E(" + i + ',' + (class24 != null ? "{...}" : "null") + ')');
		}
	}

	public byte		aByte658;
	public Class87	aClass87_657;
	public int		anInt656;
	public int		anInt659;
	public int		anInt661;
	public int		anInt662;
	public int		anInt663;
	public int		anInt664;
	public int		anInt666;
	public int		anInt668;
	public int		anInt669;

	public int		anInt670;

	public int		anInt671;

	public int		anInt672;

	public int		anInt674;

	Class87(int i, int i_2_, int i_3_, int i_4_, byte i_5_) {
		try {
			anInt661 = i_3_;
			anInt672 = i;
			aByte658 = i_5_;
			anInt674 = i_4_;
			anInt666 = i_2_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fk.<init>(" + i + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ')');
		}
	}

	public final Class92 method856(int i) {
		try {
			if (i != 0) {
				aByte658 = (byte) -8;
			}
			return ItemDeque.method1520(anInt672, i ^ 0x3a23);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fk.A(" + i + ')');
		}
	}

	public final Class87 method857(int i, boolean bool, int i_6_, int i_7_) {
		try {
			if (bool != true) {
				return null;
			}
			return new Class87(anInt672, i, i_7_, i_6_, aByte658);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fk.D(" + i + ',' + bool + ',' + i_6_ + ',' + i_7_ + ')');
		}
	}
}

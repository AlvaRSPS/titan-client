/* Class260 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.cache.MemoryCacheNode;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.quickchat.QuickChatMessage;

public final class ProceduralTextureSource implements TextureMetricsList {
	public static RtAwtFontWrapper	aClass326_3263;
	public static IncomingOpcode	aClass58_3262;
	public static float				aFloat3260;
	public static int				anInt3261;
	public static short				aShort3256	= 32767;
	public static int				headiconsPkId;
	public static HashTable			npc;

	static {
		npc = new HashTable(64);
	}

	public static final void method3207(int i, short[] is, String[] strings, int i_3_, int i_4_) {
		do {
			try {
				if ((i_4_ ^ 0xffffffff) > (i_3_ ^ 0xffffffff)) {
					int i_5_ = (i_3_ + i_4_) / 2;
					int i_6_ = i_4_;
					String string = strings[i_5_];
					strings[i_5_] = strings[i_3_];
					strings[i_3_] = string;
					short i_7_ = is[i_5_];
					is[i_5_] = is[i_3_];
					is[i_3_] = i_7_;
					for (int i_8_ = i_4_; (i_8_ ^ 0xffffffff) > (i_3_ ^ 0xffffffff); i_8_++) {
						if (string == null || strings[i_8_] != null && (i_8_ & 0x1 ^ 0xffffffff) < (strings[i_8_].compareTo(string) ^ 0xffffffff)) {
							String string_9_ = strings[i_8_];
							strings[i_8_] = strings[i_6_];
							strings[i_6_] = string_9_;
							short i_10_ = is[i_8_];
							is[i_8_] = is[i_6_];
							is[i_6_++] = i_10_;
						}
					}
					strings[i_3_] = strings[i_6_];
					strings[i_6_] = string;
					is[i_3_] = is[i_6_];
					is[i_6_] = i_7_;
					method3207(47, is, strings, -1 + i_6_, i_4_);
					method3207(47, is, strings, i_3_, 1 + i_6_);
				}
				if (i == 47) {
					break;
				}
				method3209(42);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "qf.D(" + i + ',' + (is != null ? "{...}" : "null") + ',' + (strings != null ? "{...}" : "null") + ',' + i_3_ + ',' + i_4_ + ')');
			}
			break;
		} while (false);
	}

	public static final void method3208(int i, int i_16_, int i_17_, int i_18_, int i_19_) {
		try {
			QuickChatMessage.anInt6025 = i_19_;
			if (i_16_ >= -81) {
				method3208(33, 69, -94, -55, 47);
			}
			MemoryCacheNode.anInt5952 = i;
			Class98_Sub46_Sub20.anInt6074 = i_18_;
			LoadingScreenSequence.anInt2132 = i_17_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.J(" + i + ',' + i_16_ + ',' + i_17_ + ',' + i_18_ + ',' + i_19_ + ')');
		}
	}

	public static void method3209(int i) {
		try {
			if (i == -19788) {
				aClass58_3262 = null;
				npc = null;
				aClass326_3263 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.C(" + i + ')');
		}
	}

	private Class100				aClass100_3253	= new Class100(256);

	private Js5						aClass207_3257;

	private Js5						aClass207_3258;

	private TextureMetrics[]	aClass238Array3252;

	private int						anInt3255;

	ProceduralTextureSource(Js5 class207, Js5 class207_24_, Js5 class207_25_) {
		try {
			aClass207_3258 = class207_24_;
			aClass207_3257 = class207_25_;
			RSByteBuffer class98_sub22 = new RSByteBuffer(class207.getFile(0, 0, false));
			anInt3255 = class98_sub22.readShort((byte) 127);
			aClass238Array3252 = new TextureMetrics[anInt3255];
			for (int i = 0; anInt3255 > i; i++) {
				if (class98_sub22.readUnsignedByte((byte) 72) == 1) {
					aClass238Array3252[i] = new TextureMetrics();
				}
			}
			for (int i = 0; (i ^ 0xffffffff) > (anInt3255 ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aBoolean1825 = (class98_sub22.readUnsignedByte((byte) 50) ^ 0xffffffff) == -1;
				}
			}
			for (int i = 0; (i ^ 0xffffffff) > (anInt3255 ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aBoolean1822 = (class98_sub22.readUnsignedByte((byte) 95) ^ 0xffffffff) == -2;
				}
			}
			for (int i = 0; (i ^ 0xffffffff) > (anInt3255 ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aBoolean1833 = class98_sub22.readUnsignedByte((byte) -105) == 1;
				}
			}
			for (int i = 0; i < anInt3255; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aByte1829 = class98_sub22.readSignedByte((byte) -19);
				}
			}
			for (int i = 0; anInt3255 > i; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aByte1830 = class98_sub22.readSignedByte((byte) -19);
				}
			}
			for (int i = 0; (i ^ 0xffffffff) > (anInt3255 ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aByte1820 = class98_sub22.readSignedByte((byte) -19);
				}
			}
			for (int i = 0; (i ^ 0xffffffff) > (anInt3255 ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aByte1816 = class98_sub22.readSignedByte((byte) -19);
				}
			}
			for (int i = 0; (anInt3255 ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].colour = (short) class98_sub22.readShort((byte) 127);
				}
			}
			for (int i = 0; i < anInt3255; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aByte1823 = class98_sub22.readSignedByte((byte) -19);
				}
			}
			for (int i = 0; anInt3255 > i; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aByte1837 = class98_sub22.readSignedByte((byte) -19);
				}
			}
			for (int i = 0; i < anInt3255; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aBoolean1827 = class98_sub22.readUnsignedByte((byte) -126) == 1;
				}
			}
			for (int i = 0; anInt3255 > i; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aBoolean1824 = class98_sub22.readUnsignedByte((byte) -117) == 1;
				}
			}
			for (int i = 0; anInt3255 > i; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aByte1832 = class98_sub22.readSignedByte((byte) -19);
				}
			}
			for (int i = 0; (anInt3255 ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aBoolean1826 = (class98_sub22.readUnsignedByte((byte) 46) ^ 0xffffffff) == -2;
				}
			}
			for (int i = 0; anInt3255 > i; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aBoolean1819 = class98_sub22.readUnsignedByte((byte) 31) == 1;
				}
			}
			for (int i = 0; (anInt3255 ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].aBoolean1817 = class98_sub22.readUnsignedByte((byte) 122) == 1;
				}
			}
			for (int i = 0; (anInt3255 ^ 0xffffffff) < (i ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].anInt1821 = class98_sub22.readUnsignedByte((byte) 89);
				}
			}
			for (int i = 0; anInt3255 > i; i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].anInt1835 = class98_sub22.readInt(-2);
				}
			}
			for (int i = 0; (i ^ 0xffffffff) > (anInt3255 ^ 0xffffffff); i++) {
				if (aClass238Array3252[i] != null) {
					aClass238Array3252[i].anInt1818 = class98_sub22.readUnsignedByte((byte) 35);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.<init>(" + (class207 != null ? "{...}" : "null") + ',' + (class207_24_ != null ? "{...}" : "null") + ',' + (class207_25_ != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final TextureMetrics getInfo(int i, int i_23_) {
		try {
			if (i_23_ != -28755) {
				method3206(false, 2);
			}
			return aClass238Array3252[i];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.A(" + i + ',' + i_23_ + ')');
		}
	}

	@Override
	public final int[] getPixelsArgb(int i, int i_0_, int i_1_, float f, boolean bool, int i_2_) {
		try {
			if (i < 108) {
				isCached(-35, -59);
			}
			return method3206(false, i_1_).method1633(aClass238Array3252[i_1_].aBoolean1824, f, i_2_, this, aClass207_3257, (byte) 79, i_0_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.I(" + i + ',' + i_0_ + ',' + i_1_ + ',' + f + ',' + bool + ',' + i_2_ + ')');
		}
	}

	@Override
	public final float[] getPixelsFloat(byte i, boolean bool, int i_20_, int i_21_, float f, int i_22_) {
		try {
			if (i > -116) {
				aClass238Array3252 = null;
			}
			return method3206(false, i_20_).method1630(this, aClass207_3257, (byte) -86, i_22_, i_21_, aClass238Array3252[i_20_].aBoolean1824);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.H(" + i + ',' + bool + ',' + i_20_ + ',' + i_21_ + ',' + f + ',' + i_22_ + ')');
		}
	}

	@Override
	public final int[] getPixelsRgb(int i, byte i_11_, int i_12_, float f, boolean bool, int i_13_) {
		try {
			if (i_11_ > -111) {
				method3206(true, 110);
			}
			return method3206(false, i).method1631(i_12_, bool, this, f, aClass238Array3252[i].aBoolean1824, aClass207_3257, i_13_, (byte) -34);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.E(" + i + ',' + i_11_ + ',' + i_12_ + ',' + f + ',' + bool + ',' + i_13_ + ')');
		}
	}

	@Override
	public final boolean isCached(int i, int i_14_) {
		try {
			Class98_Sub46_Sub19 class98_sub46_sub19 = method3206(false, i_14_);
			if (class98_sub46_sub19 == null || !class98_sub46_sub19.method1629(0, aClass207_3257, this)) {
				return false;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.F(" + i + ',' + i_14_ + ')');
		}
	}

	@Override
	public final int method12(boolean bool) {
		try {
			if (bool != true) {
				getPixelsRgb(-28, (byte) 95, -93, -0.31396338F, false, -83);
			}
			return anInt3255;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.G(" + bool + ')');
		}
	}

	private final Class98_Sub46_Sub19 method3206(boolean bool, int i) {
		try {
			Cacheable class98_sub46 = aClass100_3253.method1694((byte) 127, i);
			if (class98_sub46 != null) {
				return (Class98_Sub46_Sub19) class98_sub46;
			}
			byte[] is = aClass207_3258.getFile(i, -5);
			if (is == null) {
				return null;
			}
			if (bool != false) {
				return null;
			}
			Class98_Sub46_Sub19 class98_sub46_sub19 = new Class98_Sub46_Sub19(new RSByteBuffer(is));
			aClass100_3253.method1695(26404, class98_sub46_sub19, i);
			return class98_sub46_sub19;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qf.B(" + bool + ',' + i + ')');
		}
	}
}

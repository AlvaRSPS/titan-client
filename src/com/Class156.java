
/* Class156 - Decompiled by JODE
 */ package com; /*
					*/

import jaclib.memory.Buffer;

class Class156 {
	Buffer					aBuffer1247;
	private OpenGlToolkit	aHa_Sub1_1246;

	Class156(OpenGlToolkit var_ha_Sub1, Buffer buffer) {
		aHa_Sub1_1246 = var_ha_Sub1;
		aBuffer1247 = buffer;
	}

	Class156(OpenGlToolkit var_ha_Sub1, byte[] is, int i) {
		aHa_Sub1_1246 = var_ha_Sub1;
		aBuffer1247 = aHa_Sub1_1246.heap.allocate(i, false);
		if (is != null) {
			aBuffer1247.a(is, 0, 0, i);
		}
	}

	public final void method2496(byte[] is, int i) {
		if (aBuffer1247 == null || aBuffer1247.getSize() < i) {
			aBuffer1247 = aHa_Sub1_1246.heap.allocate(i, false);
		}
		aBuffer1247.a(is, 0, 0, i);
	}
}


/* Class98_Sub46_Sub14 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;
import com.jagex.game.client.definition.parser.SkyboxDefinitionParser;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.toolkit.ground.OpenGlGround;

import jaggl.OpenGL;

public final class Class98_Sub46_Sub14 extends Cacheable implements Interface3 {
	public static InventoriesDefinitionParser inventoriesDefinitionList;

	public static final void method1602(int i, int i_0_, int i_1_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_][i_1_];
		if (class172 != null) {
			PacketBufferManager.method2227(class172.aClass246_Sub3_Sub1_1332);
			if (class172.aClass246_Sub3_Sub1_1332 != null) {
				class172.aClass246_Sub3_Sub1_1332 = null;
			}
		}
	}

	public static final void method1604(boolean bool, byte i) {
		try {
			Class200.aClass111_1543.method2092(Class154.aHa1231.method1752());
			int[] is = Class154.aHa1231.Y();
			if (i != 88) {
				method1604(false, (byte) -30);
			}
			Class98_Sub10_Sub38.anInt5752 = is[3];
			Class98_Sub48.anInt4279 = is[0];
			Class96.anInt802 = is[2];
			NativeProgressMonitor.anInt3391 = is[1];
			if (!bool) {
				Class154.aHa1231.DA(Class246_Sub10.anInt5154, Class76_Sub11.anInt3798, Class138.anInt1085, SkyboxDefinitionParser.anInt466);
				Class41.method367(Class291.aDouble2199, 14794);
			} else {
				Class154.aHa1231.DA(Class224_Sub2_Sub1.anInt6143, Class98_Sub10_Sub1.anInt5543, Class370.anInt3140, NPC.anInt6509);
				Class41.method367(GZipDecompressor.aDouble1966, 14794);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "is.E(" + bool + ',' + i + ')');
		}
	}

	public static void method1606(byte i) {
		do {
			try {
				inventoriesDefinitionList = null;
				if (i > 25) {
					break;
				}
				method1607(null, (byte) 87);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "is.F(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final boolean method1607(Class228 class228, byte i) {
		try {
			if (i < 80) {
				method1607(null, (byte) 113);
			}
			if (class228 == null) {
				return false;
			}
			return OpenGlGround.method3427(class228.anInt1713, class228.anInt1714 - class228.anInt1712, class228.anInt1708 - class228.anInt1713, class228.anInt1709 - class228.anInt1715, (byte) 16, class228.anInt1715, class228.anInt1712);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "is.D(" + (class228 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private OpenGlToolkit	aHa_Sub1_5373;
	private int				anInt5374;
	private int				anInt5375	= -1;
	int						anInt5376;

	int						anInt5377;

	private int				anInt5379;

	private int				anInt5380	= -1;

	private int				anInt5381;

	Class98_Sub46_Sub14(OpenGlToolkit var_ha_Sub1, int i, int i_4_, int i_5_) {
		try {
			anInt5379 = i;
			aHa_Sub1_5373 = var_ha_Sub1;
			anInt5377 = i_5_;
			anInt5376 = i_4_;
			OpenGL.glGenRenderbuffersEXT(1, Class76_Sub9.anIntArray3785, 0);
			anInt5381 = Class76_Sub9.anIntArray3785[0];
			OpenGL.glBindRenderbufferEXT(36161, anInt5381);
			OpenGL.glRenderbufferStorageEXT(36161, anInt5379, anInt5376, anInt5377);
			anInt5374 = anInt5376 * anInt5377 * aHa_Sub1_5373.method1866(-96, anInt5379);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "is.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_4_ + ',' + i_5_ + ')');
		}
	}

	Class98_Sub46_Sub14(OpenGlToolkit var_ha_Sub1, int i, int i_6_, int i_7_, int i_8_) {
		try {
			aHa_Sub1_5373 = var_ha_Sub1;
			anInt5379 = i;
			anInt5376 = i_6_;
			anInt5377 = i_7_;
			OpenGL.glGenRenderbuffersEXT(1, Class76_Sub9.anIntArray3785, 0);
			anInt5381 = Class76_Sub9.anIntArray3785[0];
			OpenGL.glBindRenderbufferEXT(36161, anInt5381);
			OpenGL.glRenderbufferStorageMultisampleEXT(36161, i_8_, anInt5379, anInt5376, anInt5377);
			anInt5374 = anInt5377 * anInt5376 * aHa_Sub1_5373.method1866(-121, anInt5379);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "is.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			method1603(0);
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "is.finalize(" + ')');
		}
	}

	public final void method1603(int i) {
		try {
			if (i < anInt5381) {
				aHa_Sub1_5373.method1846(anInt5381, i ^ 0x4b, anInt5374);
				anInt5381 = 0;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "is.C(" + i + ')');
		}
	}

	public final void method1605(int i, int i_2_, int i_3_) {
		do {
			try {
				OpenGL.glFramebufferRenderbufferEXT(i_2_, i_3_, 36161, anInt5381);
				anInt5380 = i_3_;
				anInt5375 = i_2_;
				if (i == 0) {
					break;
				}
				method1606((byte) -93);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "is.G(" + i + ',' + i_2_ + ',' + i_3_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method3(byte i) {
		try {
			OpenGL.glFramebufferRenderbufferEXT(anInt5375, anInt5380, 36161, 0);
			if (i > -117) {
				method1605(-42, -54, 110);
			}
			anInt5380 = -1;
			anInt5375 = -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "is.B(" + i + ')');
		}
	}
}

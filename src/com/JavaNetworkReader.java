
/* Class127 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;

public final class JavaNetworkReader implements Runnable {
	public static Js5	aClass207_1019;
	public static int	anInt1018;

	public static void method2218(int i) {
		do {
			try {
				aClass207_1019 = null;
				if (i == 0) {
					break;
				}
				anInt1018 = -71;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ik.D(" + i + ')');
			}
			break;
		} while (false);
	}

	private byte[]		aByteArray1022;
	private InputStream	anInputStream1024;
	private int			anInt1016	= 0;
	private int			anInt1020	= 0;
	private int			anInt1023;
	private IOException	anIOException1017;

	private Thread		aThread1021;

	JavaNetworkReader(InputStream inputstream, int i) {
		try {
			anInt1023 = i - -1;
			anInputStream1024 = inputstream;
			aByteArray1022 = new byte[anInt1023];
			aThread1021 = new Thread(this);
			aThread1021.setDaemon(true);
			aThread1021.start();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ik.<init>(" + (inputstream != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final boolean isBuffered(int i, byte i_5_) throws IOException {
		try {
			if (i_5_ > -120) {
				aClass207_1019 = null;
			}
			if ((i ^ 0xffffffff) >= -1 || (i ^ 0xffffffff) <= (anInt1023 ^ 0xffffffff)) {
				throw new IOException();
			}
			synchronized (this) {
				int i_6_;
				do {
					if ((anInt1016 ^ 0xffffffff) < (anInt1020 ^ 0xffffffff)) {
						i_6_ = anInt1023 - (anInt1016 + -anInt1020);
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					i_6_ = anInt1020 - anInt1016;
				} while (false);
				if ((i_6_ ^ 0xffffffff) > (i ^ 0xffffffff)) {
					if (anIOException1017 != null) {
						throw new IOException(anIOException1017.toString());
					}
					return false;
				}
				return true;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ik.A(" + i + ',' + i_5_ + ')');
		}
	}

	public final int read(int i, byte[] is, int i_1_, byte i_2_) throws IOException {
		try {
			if ((i_1_ ^ 0xffffffff) > -1 || (i ^ 0xffffffff) > -1 || (is.length ^ 0xffffffff) > (i + i_1_ ^ 0xffffffff)) {
				throw new IOException();
			}
			synchronized (this) {
				int i_3_;
				if ((anInt1020 ^ 0xffffffff) > (anInt1016 ^ 0xffffffff)) {
					i_3_ = anInt1020 + anInt1023 - anInt1016;
				} else {
					i_3_ = -anInt1016 + anInt1020;
				}
				if (i_2_ != 59) {
					shutdown(-89);
				}
				if ((i_1_ ^ 0xffffffff) < (i_3_ ^ 0xffffffff)) {
					i_1_ = i_3_;
				}
				if ((i_1_ ^ 0xffffffff) == -1 && anIOException1017 != null) {
					throw new IOException(anIOException1017.toString());
				}
				if (anInt1023 >= anInt1016 - -i_1_) {
					ArrayUtils.method2894(aByteArray1022, anInt1016, is, i, i_1_);
				} else {
					int i_4_ = -anInt1016 + anInt1023;
					ArrayUtils.method2894(aByteArray1022, anInt1016, is, i, i_4_);
					ArrayUtils.method2894(aByteArray1022, 0, is, i_4_ + i, i_1_ - i_4_);
				}
				anInt1016 = (anInt1016 - -i_1_) % anInt1023;
				notifyAll();
				return i_1_;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ik.B(" + i + ',' + (is != null ? "{...}" : "null") + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	@Override
	public final void run() {
		try {
			for (;;) {
				int i;
				synchronized (this) {
					for (;;) {
						if (anIOException1017 != null) {
							return;
						}
						if ((anInt1016 ^ 0xffffffff) == -1) {
							i = -1 + -anInt1020 + anInt1023;
						} else if (anInt1016 >= anInt1020) {
							i = -anInt1020 + anInt1016 - 1;
						} else {
							i = anInt1023 + -anInt1020;
						}
						if ((i ^ 0xffffffff) < -1) {
							break;
						}
						try {
							this.wait();
						} catch (InterruptedException interruptedexception) {
							/* empty */
						}
					}
				}
				int i_0_;
				try {
					i_0_ = anInputStream1024.read(aByteArray1022, anInt1020, i);
					if ((i_0_ ^ 0xffffffff) == 0) {
						throw new EOFException();
					}
				} catch (IOException ioexception) {
					synchronized (this) {
						anIOException1017 = ioexception;
						break;
					}
				}
				synchronized (this) {
					anInt1020 = (i_0_ + anInt1020) % anInt1023;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ik.run(" + ')');
		}
	}

	public final void setDummyMode(byte i) {
		try {
			if (i <= 85) {
				run();
			}
			anInputStream1024 = new InputStream_Sub2();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ik.C(" + i + ')');
		}
	}

	public final void shutdown(int i) {
		try {
			synchronized (this) {
				if (anIOException1017 == null) {
					anIOException1017 = new IOException("");
				}
				notifyAll();
			}
			try {
				if (i <= 53) {
					anInt1018 = 123;
				}
				aThread1021.join();
			} catch (InterruptedException interruptedexception) {
				/* empty */
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ik.E(" + i + ')');
		}
	}
}

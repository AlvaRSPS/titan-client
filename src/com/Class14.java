/* Class14 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.ui.layout.VerticalAlignment;

public final class Class14 {
	public static final boolean isIgnored(String name, byte i) {
		if (name == null) {
			return false;
		}
		for (int count = 0; count < Class248.ignoreListSize; count++) {
			if (name.equalsIgnoreCase(Class255.ignoreNames[count])) {
				return true;
			}
			if (name.equalsIgnoreCase(VerticalAlignment.ignoreDisplayNames[count])) {
				return true;
			}
		}
		return false;
	}

	public static final int method226(int i) {
		return Class246_Sub3_Sub2_Sub1.anInt6345++;
	}

	public byte[]	aByteArray168;
	public short[]	aShortArray165;

	public short[]	aShortArray166;

	public short[]	aShortArray167;
}

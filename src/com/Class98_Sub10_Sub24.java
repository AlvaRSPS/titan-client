/* Class98_Sub10_Sub24 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.FloorUnderlayDefinitionParser;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.client.preferences.CpuUsagePreferenceField;
import com.jagex.game.client.preferences.OrthoZoomPreferenceField;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.preferences.UseCustomCursorPreferenceField;
import com.jagex.game.client.preferences.WaterDetailPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class98_Sub10_Sub24 extends Class98_Sub10 {
	public static GrandExchangeOffer[]	offers			= new GrandExchangeOffer[6];
	public static int					anInt5671;
	private static short[]				aShortArray5667	= { 967, 20428, -21577, 11219, -10290 };
	private static short[]				aShortArray5670	= { 957, 20418, -21587, 11209, -10300 };
	private static short[]				aShortArray5673;
	private static short[]				aShortArray5674;
	public static short[][]				aShortArrayArray5669;
	public static Js5					enumsJs5;

	static {

		aShortArray5673 = new short[] { 962, 20423, -21582, 11214, -10295 };
		aShortArray5674 = new short[] { 952, 20413, -21592, 11204, -10305 };
		aShortArrayArray5669 = new short[][] { aShortArray5667, aShortArray5673, aShortArray5670, aShortArray5674 };
	}

	public static final byte[] method1075(byte[] is, boolean bool) {
		if (is == null) {
			return null;
		}
		if (bool != true) {
			return null;
		}
		byte[] is_15_ = new byte[is.length];
		ArrayUtils.method2894(is, 0, is_15_, 0, is.length);
		return is_15_;
	}

	public static final boolean method1076(int i) {
		try {
			return PacketParser.method3967(525200579);
		} catch (java.io.IOException ioexception) {
			Canvas_Sub1.method118((byte) 104);
			return true;
		} catch (Exception exception) {
			String string = "T2 - " + (PacketParser.CURRENT_INCOMING_OPCODE != null ? PacketParser.CURRENT_INCOMING_OPCODE.method521((byte) 89) : -1) + "," + (ProceduralTextureSource.aClass58_3262 != null ? ProceduralTextureSource.aClass58_3262.method521((byte) 64) : -1) + ","
					+ (Class98_Sub10_Sub21.aClass58_5641 == null ? -1 : Class98_Sub10_Sub21.aClass58_5641.method521((byte) 97)) + " - " + Class65.currentPacketSize + "," + (Class272.gameSceneBaseX + Class87.localPlayer.pathX[0]) + "," + (aa_Sub2.gameSceneBaseY
							+ Class87.localPlayer.pathZ[0]) + " - ";
			for (int i_17_ = 0; i_17_ < Class65.currentPacketSize && (i_17_ ^ 0xffffffff) > -51; i_17_++) {
				string += PacketParser.buffer.payload[i_17_] + ",";
			}
			Class305_Sub1.reportError(exception, -124, string);
			Class98_Sub10_Sub1.handleLogout(false, false);
			return true;
		}
	}

	public static final void renderInterface(int i, boolean bool, int x, int clipLeft, int clipTop, int i_54_, RtInterface[] interfaceComponent, int clipRight, int clipBottom, boolean bool_57_, int y) {
		client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
		if (bool == true) {
			int childId = 0;
			for (/**/; (childId ^ 0xffffffff) > (interfaceComponent.length ^ 0xffffffff); childId++) {
				RtInterface comp = interfaceComponent[childId];
				if (comp != null && (i == comp.parentId || i == -1412584499 && Class255.aClass293_3208 == comp)) {
					int cLeft = x + comp.renderX;
					int cTop = comp.renderY + y;
					int cRight = 1 + comp.renderWidth + cLeft;
					int cBottom = comp.renderHeight + cTop + 1;
					int i_64_;
					if (i_54_ == -1) {
						PlatformInformation.aRectangleArray4144[Class69_Sub2.anInt5335].setBounds(x + comp.renderX, comp.renderY - -y, comp.renderWidth, comp.renderHeight);
						i_64_ = Class69_Sub2.anInt5335++;
					} else {
						i_64_ = i_54_;
					}
					comp.anInt2250 = Queue.timer;
					comp.anInt2238 = i_64_;
					if (!client.method111(comp)) {
						if ((comp.contentType ^ 0xffffffff) != -1) {
							StreamHandler.method2206(comp, (byte) 19);
						}
						int c_x = comp.renderX - -x;
						int c_y = y + comp.renderY;
						int xOffset = 0;
						int yOffset = 0;
						if (OpenGLHeap.aBoolean6079) {
							xOffset = Class189.method2642((byte) 42);
							yOffset = MapScenesDefinitionParser.method3765(false);
						}
						int alpha = comp.alpha;
						if (Class15.qaOpTest && (client.method116(comp).anInt4284 != 0 || (comp.type ^ 0xffffffff) == -1) && alpha > 127) {
							alpha = 127;
						}
						if (Class255.aClass293_3208 == comp) {
							if ((i ^ 0xffffffff) != 1412584498 && ((Class36.anInt350 ^ 0xffffffff) == (comp.anInt2289 ^ 0xffffffff) || (Class369.anInt3129 ^ 0xffffffff) == (comp.anInt2289 ^ 0xffffffff))) {
								Class65.aClass293Array500 = interfaceComponent;
								IncomingOpcode.anInt463 = x;
								Class64_Sub5.anInt3654 = y;
								continue;
							}
							if (Class15.aBoolean186 && VarClientStringsDefinitionParser.aBoolean1840) {
								int mouseX = client.mouseListener.getMouseX(67) - -xOffset;
								int mouseY = client.mouseListener.getMouseY((byte) 116) + yOffset;
								mouseY -= PlayerUpdateMask.mouseY;
								mouseX -= BaseModel.mouseX;
								if ((mouseX ^ 0xffffffff) > (NPC.anInt6500 ^ 0xffffffff)) {
									mouseX = NPC.anInt6500;
								}
								if (mouseX - -comp.renderWidth > Class189.aClass293_1457.renderWidth + NPC.anInt6500) {
									mouseX = -comp.renderWidth + Class189.aClass293_1457.renderWidth + NPC.anInt6500;
								}
								if (mouseY < LoginOpcode.anInt1670) {
									mouseY = LoginOpcode.anInt1670;
								}
								c_x = mouseX;
								if (mouseY - -comp.renderHeight > LoginOpcode.anInt1670 + Class189.aClass293_1457.renderHeight) {
									mouseY = -comp.renderHeight + Class189.aClass293_1457.renderHeight + LoginOpcode.anInt1670;
								}
								c_y = mouseY;
							}
							if ((comp.anInt2289 ^ 0xffffffff) == (Class369.anInt3129 ^ 0xffffffff)) {
								alpha = 128;
							}
						}
						int cCliptop;
						int cClipLeft;
						int cClipBottom;
						int cClipRight;
						if ((comp.type ^ 0xffffffff) != -3) {
							int cR = comp.renderWidth + c_x;
							int cB = c_y - -comp.renderHeight;
							cCliptop = c_y <= clipTop ? clipTop : c_y;
							cClipLeft = clipLeft >= c_x ? clipLeft : c_x;
							if (comp.type == 9) {
								cR++;
								cB++;
							}
							cClipRight = (clipRight ^ 0xffffffff) < (cR ^ 0xffffffff) ? cR : clipRight;
							cClipBottom = clipBottom <= cB ? clipBottom : cB;
						} else {
							cCliptop = clipTop;
							cClipLeft = clipLeft;
							cClipBottom = clipBottom;
							cClipRight = clipRight;
						}
						if ((cClipRight ^ 0xffffffff) < (cClipLeft ^ 0xffffffff) && cCliptop < cClipBottom) {
							if ((comp.contentType ^ 0xffffffff) != -1) {
								if (Class22.CONTENT_TYPE_LOGIN_BG == comp.contentType || (comp.contentType ^ 0xffffffff) == (TexturesPreferenceField.CONTENT_TYPE_GAME_VIEW ^ 0xffffffff)) {
									Class98_Sub1.method946(c_x, -121, c_y, comp);// Render
																					// the
																					// scene
									if (!OpenGLHeap.aBoolean6079) {
										Class92.method892(21337, c_x, c_y, comp.renderHeight, (comp.contentType ^ 0xffffffff) == (TexturesPreferenceField.CONTENT_TYPE_GAME_VIEW ^ 0xffffffff), comp.renderWidth);
										client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
									}
									aa_Sub3.isDirty[i_64_] = true;
									continue;
								}
								if ((comp.contentType ^ 0xffffffff) == (Orientation.CONTENT_TYPE_MINIMAP ^ 0xffffffff)) {
									if (comp.getClip(client.graphicsToolkit, 123) != null) {
										Class128.method2224(22696);
										Minimap.renderMinimap(4, comp, client.graphicsToolkit, c_x, c_y);
										Class98_Sub10_Sub20.aBooleanArray5639[i_64_] = true;
										client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
										if (OpenGLHeap.aBoolean6079) {
											if (!bool_57_) {
												Class216.method2796(cBottom, cLeft, cRight, cTop, (byte) 126);
											} else {
												AnimatedProgressBarLSEConfig.method908(cBottom, cTop, false, cLeft, cRight);
											}
										}
									}
									continue;
								}
								if (Class96.CONTENT_TYPE_COMPASS == comp.contentType) {
									if (comp.getClip(client.graphicsToolkit, 103) != null) {
										Minimap.renderCompass(4096, c_x, c_y, comp);
										Class98_Sub10_Sub20.aBooleanArray5639[i_64_] = true;
										client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
										if (OpenGLHeap.aBoolean6079) {
											if (bool_57_) {
												AnimatedProgressBarLSEConfig.method908(cBottom, cTop, !bool, cLeft, cRight);
											} else {
												Class216.method2796(cBottom, cLeft, cRight, cTop, (byte) 113);
											}
										}
									}
									continue;
								}
								if (comp.contentType == Class87.anInt673) {
									UseCustomCursorPreferenceField.drawWorldMapUnderground(client.graphicsToolkit, client.textureList, c_y, c_x, comp.renderHeight, (byte) 31, comp.renderWidth);
									aa_Sub3.isDirty[i_64_] = true;
									client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
									continue;
								}
								if (VarClientStringsDefinitionParser.anInt1841 == comp.contentType) {
									Class202.method2701(c_x, comp.renderWidth, client.graphicsToolkit, c_y, (byte) -90, comp.renderHeight);
									aa_Sub3.isDirty[i_64_] = true;
									client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
									continue;
								}
								if (client.hideInfo == false) {
									Class195.p12Full.drawStringRightAnchor(-256, 0, 370 + c_x, -1, "Position: X:" + (Class272.gameSceneBaseX - -(Class87.localPlayer.boundExtentsX >> 1513969193)) + " Y: " + (aa_Sub2.gameSceneBaseY
											- -(Class87.localPlayer.boundExtentsZ >> 1695912137)) + " Plane: " + Class87.localPlayer.plane, 175 + c_y);
								}
								if ((comp.contentType ^ 0xffffffff) == (RenderAnimDefinition.CONTENT_TYPE_FPS_BLOCK ^ 0xffffffff)) {
									if (client.displayFps || Class170.showProfiling || client.HideGUI) {
										int xPos = comp.renderWidth + c_x;
										int yPos = 15 + c_y;

										if (client.HideGUI) {// TODO:
											Class195.p12Full.drawStringRightAnchor(-15, -1, xPos, -1, "Titan", yPos);
											Class69_Sub2.p11Full.drawStringRightAnchor(+17, -26, xPos, -1, "wwww.maxgamer.org", yPos);
											aa_Sub3.isDirty[i_64_] = true;
										}

										if (OpenGLHeap.aBoolean6079) {
											if (bool_57_) {
												AnimatedProgressBarLSEConfig.method908(cBottom, cTop, false, cLeft, cRight);
											} else {
												Class216.method2796(cBottom, cLeft, cRight, cTop, (byte) 118);
											}
										}
										if (client.displayFps) {
											int color = -256;
											if (GameShell.fpsValue < 20) {
												color = -65536;
											}
											Class195.p12Full.drawStringRightAnchor(color, 0, xPos, -1, "Fps:" + GameShell.fpsValue, yPos);
											yPos += 15;
											Runtime runtime = Runtime.getRuntime();
											int memoryUsage = (int) ((runtime.totalMemory() - runtime.freeMemory()) / 1024L);
											int colour = -256;
											if ((memoryUsage ^ 0xffffffff) < -98305) {
												if (client.force64Mb) {
													Class27.method294(true);
													for (int count = 0; (count ^ 0xffffffff) > -11; count++) {
														System.gc();
													}
													memoryUsage = (int) ((runtime.totalMemory() + -runtime.freeMemory()) / 1024L);
													if ((memoryUsage ^ 0xffffffff) < -65537) {
														OpenGLHeap.addChatMessage("WARNING: Memory usage over 64MB! Please inform whoever is responsible for the content/area you are using/in.", 4, (byte) -109);
													}
												}
												colour = -65536;
											}
											Class195.p12Full.drawStringRightAnchor(colour, 0, xPos, -1, "Mem:" + memoryUsage + "k", yPos);
											yPos += 15;
											Class195.p12Full.drawStringRightAnchor(-256, 0, xPos, -1, "In:" + OpenGLHeightMapToNormalMapConverter.bytesIn + "B/s Out:" + Class98_Sub12.bytesOut + "B/s", yPos);
											yPos += 15;
											int offHeap = client.graphicsToolkit.E() / 1024;
											Class195.p12Full.drawStringRightAnchor(offHeap <= 65536 ? -256 : -65536, 0, xPos, -1, "Offheap:" + offHeap + "k", yPos);
											yPos += 15;
											int groupCount = 0;
											int i_86_ = 0;
											int i_87_ = 0;
											for (int index = 0; (index ^ 0xffffffff) > -38; index++) {
												if (Class100.js5Archives[index] != null) {
													groupCount += Class100.js5Archives[index].method3800(0);
													i_86_ += Class100.js5Archives[index].method3798(49);
													i_87_ += Class100.js5Archives[index].method3791((byte) -125);
												}
											}
											int i_89_ = i_87_ * 100 / groupCount;
											int i_90_ = i_86_ * 10000 / groupCount;
											String string = "Cache:" + Class39.method349(0, 2, 48, i_90_, true) + "% (" + i_89_ + "%)";
											Class69_Sub2.p11Full.drawStringRightAnchor(-256, 0, xPos, -1, string, yPos);
											yPos += 12;
										}
										if (ParticleManager.particleCount > 0) {
											Class69_Sub2.p11Full.drawStringRightAnchor(-256, 0, xPos, -1, "Particles: " + ParticleManager.runningSystemCount + " / " + ParticleManager.particleCount, yPos);
										}
										yPos += 12;
										if (Class170.showProfiling) {
											Class69_Sub2.p11Full.drawStringRightAnchor(-256, 0, xPos, -1, "Polys: " + client.graphicsToolkit.I() + " Models: " + client.graphicsToolkit.M(), yPos);
											yPos += 12;
											Class69_Sub2.p11Full.drawStringRightAnchor(-256, 0, xPos, -1, "Ls: " + Class191.anInt1480 + " La: " + Class76_Sub5.anInt3748 + " NPC: " + Class181.anInt1432 + " Pl: " + Class98_Sub10_Sub13.anInt5602, yPos);
											Class228.method2862(-92);
											yPos += 12;
										}
										aa_Sub3.isDirty[i_64_] = true;
									}
									continue;
								}
							}
							if (comp.type == 0) {// login screen
								if (comp.contentType == CpuUsagePreferenceField.anInt3704 && client.graphicsToolkit.canEnableBloom()) {
									client.graphicsToolkit.method1746(c_x, c_y, comp.renderWidth, comp.renderHeight);
								}
								renderInterface(comp.idDword, true, -comp.scrollX + c_x, cClipLeft, cCliptop, i_64_, interfaceComponent, cClipRight, cClipBottom, bool_57_, c_y + -comp.scrollY);
								if (comp.dynamicComponents != null) {
									renderInterface(comp.idDword, true, c_x - comp.scrollX, cClipLeft, cCliptop, i_64_, comp.dynamicComponents, cClipRight, cClipBottom, bool_57_, c_y - comp.scrollY);
								}
								RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(comp.idDword, -1);
								if (class98_sub18 != null) {
									Class246_Sub3_Sub5_Sub2.renderInterface(cClipBottom, i_64_, cCliptop, class98_sub18.attachedInterfaceId, c_y, c_x, cClipRight, (byte) -9, cClipLeft);
								}
								if ((CpuUsagePreferenceField.anInt3704 ^ 0xffffffff) == (comp.contentType ^ 0xffffffff) && client.graphicsToolkit.canEnableBloom()) {
									client.graphicsToolkit.method1814();
								}
								client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
							}
							if (Class232.aBooleanArray1741[i_64_] || (VarPlayerDefinition.anInt1282 ^ 0xffffffff) < -2) {
								if (comp.type == 3) {
									if (alpha != 0) {
										if (!comp.filled) {
											client.graphicsToolkit.drawRectangle(c_x, c_y, comp.renderWidth, comp.renderHeight, 255 + -(0xff & alpha) << 1189469208 | 0xffffff & comp.defaultColour, 1);
										} else {
											client.graphicsToolkit.fillRectangle(c_x, c_y, comp.renderWidth, comp.renderHeight, 255 + -(0xff & alpha) << -1750207592 | 0xffffff & comp.defaultColour, 1);
										}
									} else if (comp.filled) {
										client.graphicsToolkit.fillRectangle(c_x, c_y, comp.renderWidth, comp.renderHeight, comp.defaultColour, 0);// Lobby
										// rectangle
									} else {
										client.graphicsToolkit.drawRectangle(c_x, c_y, comp.renderWidth, comp.renderHeight, comp.defaultColour, 0);
									}
									if (OpenGLHeap.aBoolean6079) {
										if (bool_57_) {
											AnimatedProgressBarLSEConfig.method908(cBottom, cTop, false, cLeft, cRight);
										} else {
											Class216.method2796(cBottom, cLeft, cRight, cTop, (byte) 91);
										}
									}
								} else if (comp.type == 4) {
									Font font = comp.getFont(-69, client.graphicsToolkit);
									if (font == null) {
										if (MapRegion.aBoolean3503) {
											WorldMapInfoDefinitionParser.setDirty(1, comp);
										}
									} else {
										int colour = comp.defaultColour;
										String text = comp.defaultText;
										int textId = comp.idDword;

										/*
										 * Debugs everything related to text on
										 * interfaces. You can change the
										 * messages by hardcoding them here, but
										 * it's not recommended. Use an
										 * interface :)
										 */

										if (text.equalsIgnoreCase("Log In") && (textId == 39059507 || textId == 39059508)) {
											text = "Enter Titan";
										} else if (text.equalsIgnoreCase("Login") && textId == 39059489) {
											text = "Welcome to Titan!";
										} else if (text.equalsIgnoreCase("When you have finished playing<br>RuneScape, always use a<br>button below to exit the game and logout safely.") && textId == 11927552) {
											text = "Thank you for playing Titan!<br> We hope to see you again soon!";
										} else if (text.equalsIgnoreCase("Bank of RuneScape")) {
											text = "Bank of Titan";
										} else if (text.equalsIgnoreCase("Friends List - RuneScape 1")) {
											text = "Your friends:";
										} else if (text.equalsIgnoreCase("Ignore list")) {
											text = "Your enemies:";
										} else if (text.equalsIgnoreCase("<u=C8C8C8>Recover Your Password</u>") && textId == 39059503) {
											text = "<u=C8C8C8>I need help!</u>";
										} else if (text.equalsIgnoreCase("<u=fafafa>Recover Your Password</u>") && textId == 39059503) {
											text = "<u=C8C8C8>I need help!</u>";
										}
										//System.out.println("Parent: " + textId + " Text: " + text + " Var:");

										if (comp.unknown != -1) {
											ItemDefinition itemDefinition = Class98_Sub46_Sub19.itemDefinitionList.get(comp.unknown, (byte) -121);
											text = itemDefinition.name;
											if (text == null) {
												text = "null";
											}
											if ((itemDefinition.stackable == 1 || (comp.itemStackSize ^ 0xffffffff) != -2) && (comp.itemStackSize ^ 0xffffffff) != 0) {
												text = "<col=ff9040>" + text + "</col> x" + UseCustomCursorPreferenceField.formatItemStackSize(comp.itemStackSize, 16474);
											}
										}
										if (comp.videoId != -1) {
											text = Class48.method454(comp.videoId, bool);
											if (text == null) {
												text = "";
											}
										}
										if (DummyOutputStream.rtInterface == comp) {
											text = TextResources.PLEASE_WAIT.getText(client.gameLanguage, (byte) 25);
											colour = comp.defaultColour;
										}
										if (FloorUnderlayDefinitionParser.clipComponents) {
											client.graphicsToolkit.constrainClip(c_x, c_y, c_x + comp.renderWidth, c_y + comp.renderHeight);
										}
										font.drawString(c_x, comp.lineCount, comp.renderHeight, null, 0, comp.renderWidth, comp.shaded ? -(0xff & alpha) + 255 << 1735414296 : -1, comp.lineHeight, comp.verticalTextAlign, c_y, (byte) -80, OrthoZoomPreferenceField.nameIcons, -(alpha & 0xff)
												+ 255 << 1099433400 | colour, 0, text, comp.horizontalTextAlign, null);
										if (FloorUnderlayDefinitionParser.clipComponents) {
											client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
										}
										if ((text.trim().length() ^ 0xffffffff) < -1) {
											if (FloorUnderlayDefinitionParser.clipComponents) {
												if (OpenGLHeap.aBoolean6079) {
													if (bool_57_) {
														AnimatedProgressBarLSEConfig.method908(cBottom, cTop, false, cLeft, cRight);
													} else {
														Class216.method2796(cBottom, cLeft, cRight, cTop, (byte) 114);
													}
												}
											} else {
												FontSpecifications metrics = Class98_Sub46_Sub6.loadFontMetrics(comp.fontId, 18361, client.graphicsToolkit);
												int width = metrics.getStringWidth(comp.renderWidth, text, OrthoZoomPreferenceField.nameIcons, (byte) 79);
												int height = metrics.getStringHeight(OrthoZoomPreferenceField.nameIcons, comp.renderWidth, comp.lineHeight, text, true);
												if (OpenGLHeap.aBoolean6079) {
													if (bool_57_) {
														AnimatedProgressBarLSEConfig.method908(c_y + height, c_y, false, c_x, width + c_x);
													} else {
														Class216.method2796(height + c_y, c_x, width + c_x, c_y, (byte) -62);
													}
												}
											}
										}
									}
								} else if ((comp.type ^ 0xffffffff) == -6) {
									if (comp.objId >= 0) {
										comp.method3467(0, GrandExchangeOffer.sunDefinitionList, SimpleProgressBarLoadingScreenElement.skyboxDefinitionList).method3831(0, comp.anInt2216 << 1037126371, comp.anInt2261 << 765115811, c_y, -24446, c_x, client.graphicsToolkit, comp.renderHeight,
												comp.renderWidth, 1);
									} else {
										Sprite image;
										if ((comp.unknown ^ 0xffffffff) != 0) {
											PlayerAppearence appearence = comp.objectUsePlayerAppearence ? Class87.localPlayer.appearence : null;
											image = Class98_Sub46_Sub19.itemDefinitionList.getCachedSprite(comp.rotation, comp.unknown, client.graphicsToolkit, appearence, comp.itemStackSize, comp.anInt2305, ~0xffffff | comp.backgroundColour, 24056);
										} else if ((comp.videoId ^ 0xffffffff) != 0) {
											image = Class200.method2693(comp.videoId, (byte) -114, client.graphicsToolkit);
										} else {
											image = comp.getImage(client.graphicsToolkit, -1234042329);
										}
										if (image != null) {
											int width = image.getRenderWidth();
											int height = image.getRenderHeight();
											int i_96_ = ((comp.defaultColour ^ 0xffffffff) == -1 ? 16777215 : comp.defaultColour & 0xffffff) | 255 - (alpha & 0xff) << 638192120;
											if (comp.imageRepeat) {
												client.graphicsToolkit.constrainClip(c_x, c_y, comp.renderWidth + c_x, c_y + comp.renderHeight);
												if (comp.imageRotation != 0) {
													int i_97_ = (width + -1 + comp.renderWidth) / width;
													int i_98_ = (height - 1 + comp.renderHeight) / height;
													for (int i_99_ = 0; (i_99_ ^ 0xffffffff) > (i_97_ ^ 0xffffffff); i_99_++) {
														for (int i_100_ = 0; (i_98_ ^ 0xffffffff) < (i_100_ ^ 0xffffffff); i_100_++) {
															if (comp.defaultColour == 0) {
																image.method3730(i_99_ * width + c_x + width / 2.0F, height * i_100_ + c_y + height / 2.0F, 4096, comp.imageRotation);
															} else {
																image.method3743(c_x - -(i_99_ * width) + width / 2.0F, c_y + i_100_ * height + height / 2.0F, 4096, comp.imageRotation, 0, i_96_, 1);
															}
														}
													}
												} else if (comp.defaultColour != 0 || alpha != 0) {
													image.method3728(c_x, c_y, comp.renderWidth, comp.renderHeight, 0, i_96_, 1);
												} else {
													image.drawRepeat(c_x, c_y, comp.renderWidth, comp.renderHeight);
												}
												client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
											} else if (comp.defaultColour == 0 && alpha == 0) {
												if ((comp.imageRotation ^ 0xffffffff) != -1) {
													image.method3730(c_x + comp.renderWidth / 2.0F, c_y + comp.renderHeight / 2.0F, comp.renderWidth * 4096 / width, comp.imageRotation);
												} else if ((width ^ 0xffffffff) != (comp.renderWidth ^ 0xffffffff) || (comp.renderHeight ^ 0xffffffff) != (height ^ 0xffffffff)) {
													image.method3726(c_x, c_y, comp.renderWidth, comp.renderHeight);
												} else {
													image.draw(c_x, c_y);
												}
											} else if (comp.imageRotation == 0) {
												if ((comp.renderWidth ^ 0xffffffff) == (width ^ 0xffffffff) && comp.renderHeight == height) {
													image.draw(c_x, c_y, 0, i_96_, 1);
												} else {
													image.method3727(c_x, c_y, comp.renderWidth, comp.renderHeight, 0, i_96_, 1);
												}
											} else {
												image.method3743(comp.renderWidth / 2.0F + c_x, c_y + comp.renderHeight / 2.0F, 4096 * comp.renderWidth / width, comp.imageRotation, 0, i_96_, 1);
											}
										} else if (MapRegion.aBoolean3503) {
											WorldMapInfoDefinitionParser.setDirty(1, comp);
										}
									}
									if (OpenGLHeap.aBoolean6079) {
										if (!bool_57_) {
											Class216.method2796(cBottom, cLeft, cRight, cTop, (byte) 102);
										} else {
											AnimatedProgressBarLSEConfig.method908(cBottom, cTop, false, cLeft, cRight);
										}
									}
								} else if (comp.type == 6) {
									PlatformInformation.method1452(0);
									Class40 class40 = null;
									ModelRenderer model = null;
									int i_101_ = 0;
									if ((comp.unknown ^ 0xffffffff) != 0) {
										ItemDefinition itemDefinition = Class98_Sub46_Sub19.itemDefinitionList.get(comp.unknown, (byte) -121);
										if (itemDefinition != null) {
											itemDefinition = itemDefinition.method3493((byte) -45, comp.itemStackSize);
											AnimationDefinition definition = comp.defaultAnimation == -1 ? null : Class151_Sub7.animationDefinitionList.method2623(comp.defaultAnimation, 16383);
											PlayerAppearence appearence = comp.objectUsePlayerAppearence ? Class87.localPlayer.appearence : null;
											model = itemDefinition.method3501(comp.anInt2312, 2048, comp.anInt2303, definition, comp.anInt2287, client.graphicsToolkit, 1, 128, appearence);
											if (model == null) {
												WorldMapInfoDefinitionParser.setDirty(1, comp);
											} else {
												i_101_ = -model.fa() >> -1224511199;
											}
										}
									} else if (comp.defaultMediaType != 5) {
										if ((comp.defaultMediaType ^ 0xffffffff) == -9 || comp.defaultMediaType == 9) {
											ClientInventory inventory = WaterDetailPreferenceField.get(comp.defaultMediaId, false, 6);
											AnimationDefinition animationDefinition = (comp.defaultAnimation ^ 0xffffffff) == 0 ? null : Class151_Sub7.animationDefinitionList.method2623(comp.defaultAnimation, 16383);
											if (inventory != null) {
												PlayerAppearence appearence = !comp.objectUsePlayerAppearence ? null : Class87.localPlayer.appearence;
												model = inventory.method951(animationDefinition, comp.anInt2312, comp.anInt2303, comp.defaultMediaType == 9, appearence, comp.anInt2210, 2048, comp.anInt2287, (byte) -80, client.graphicsToolkit);
											}
										} else if ((comp.defaultAnimation ^ 0xffffffff) == 0) {
											model = comp.getModelRenderer(StartupStage.varValues, Class151_Sub7.animationDefinitionList, Class4.npcDefinitionList, ParamDefinition.identikitDefinitionList, client.graphicsToolkit, Class370.renderAnimDefinitionList, class40, (byte) -40, -1, 2048,
													-1, 0, null, Class87.localPlayer.appearence, Class98_Sub46_Sub19.itemDefinitionList);
											if (model == null && MapRegion.aBoolean3503) {
												WorldMapInfoDefinitionParser.setDirty(1, comp);
											}
										} else {
											AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(comp.defaultAnimation, 16383);
											model = comp.getModelRenderer(StartupStage.varValues, Class151_Sub7.animationDefinitionList, Class4.npcDefinitionList, ParamDefinition.identikitDefinitionList, client.graphicsToolkit, Class370.renderAnimDefinitionList, class40, (byte) -57,
													comp.anInt2287, 2048, comp.anInt2303, comp.anInt2312, class97, Class87.localPlayer.appearence, Class98_Sub46_Sub19.itemDefinitionList);
											if (model == null && MapRegion.aBoolean3503) {
												WorldMapInfoDefinitionParser.setDirty(1, comp);
											}
										}
									} else {
										int mediat5Id = comp.defaultMediaId;
										if ((mediat5Id ^ 0xffffffff) <= -1 && (mediat5Id ^ 0xffffffff) > -2049) {
											Player player = Class151_Sub9.players[mediat5Id];
											AnimationDefinition definition = comp.defaultAnimation != -1 ? Class151_Sub7.animationDefinitionList.method2623(comp.defaultAnimation, 16383) : null;
											if (player != null && (OpenGLHeap.localPlayerIndex == mediat5Id || GraphicsBuffer.method1438(player.accountName, 6243) == comp.anInt2210)) {
												model = player.appearence.method3628(Class370.renderAnimDefinitionList, definition, null, Class151_Sub7.animationDefinitionList, comp.anInt2303, ParamDefinition.identikitDefinitionList, null, Class98_Sub46_Sub19.itemDefinitionList, true, 0, null,
														Class4.npcDefinitionList, StartupStage.varValues, true, 2048, -1, comp.anInt2312, comp.anInt2287, 0, 0, client.graphicsToolkit);
											}
										}
									}
									if (model != null) {
										int cWidth;
										if (comp.viewportWidth > 0) {
											cWidth = (comp.renderWidth << -1492214583) / comp.viewportWidth;
										} else {
											cWidth = 512;
										}
										int cHeight;
										if (comp.viewportHeight > 0) {
											cHeight = (comp.renderHeight << -44986999) / comp.viewportHeight;
										} else {
											cHeight = 512;
										}
										int i_105_ = comp.renderWidth / 2 + c_x;
										int i_106_ = c_y + comp.renderHeight / 2;
										if (!comp.aBoolean2280) {
											i_105_ += comp.viewportX * cWidth >> -1439009655;
											i_106_ += cHeight * comp.viewportY >> -285380663;
										}
										SunDefinition.aClass111_1986.initIdentity();
										client.graphicsToolkit.a(SunDefinition.aClass111_1986);
										client.graphicsToolkit.DA(i_105_, i_106_, cWidth, cHeight);
										client.graphicsToolkit.ya();
										if (comp.aBoolean2325) {
											client.graphicsToolkit.setDepthWriteMask(false);
										}
										if (!comp.aBoolean2280) {
											int i_107_ = (comp.zoom << -1783646334) * Class284_Sub2_Sub2.SINE[comp.rotationX << -1188499165] >> -1207674034;
											int i_108_ = Class284_Sub2_Sub2.COSINE[comp.rotationX << -223590269] * (comp.zoom << -423051582) >> -1224356338;
											Class76_Sub5.aClass111_3745.initRotZ(-comp.rotationZ << -1988380221);
											Class76_Sub5.aClass111_3745.initRotY(comp.rotationY << 1880483235);
											Class76_Sub5.aClass111_3745.translate(comp.xOffset2D << 1838805666, i_101_ + i_107_ - -(comp.yOffset2D << -1320988254), (comp.yOffset2D << -290939422) + i_108_);
											Class76_Sub5.aClass111_3745.initRotX(comp.rotationX << -1796566077);
										} else {
											Class76_Sub5.aClass111_3745.method2107(comp.rotationX);
											Class76_Sub5.aClass111_3745.initRotY(comp.rotationY);
											Class76_Sub5.aClass111_3745.method2090(comp.rotationZ);
											Class76_Sub5.aClass111_3745.translate(comp.viewportX, comp.viewportY, comp.anInt2352);
										}
										comp.method3464(true, client.graphicsToolkit, Queue.timer, model, Class76_Sub5.aClass111_3745);
										if (FloorUnderlayDefinitionParser.clipComponents) {
											client.graphicsToolkit.constrainClip(c_x, c_y, c_x - -comp.renderWidth, comp.renderHeight + c_y);
										}
										if (!comp.aBoolean2280) {
											if (!comp.aBoolean2265) {
												model.method2325(Class76_Sub5.aClass111_3745, null, 1);
												if (comp.aClass246_Sub5_2301 != null) {
													client.graphicsToolkit.method1820(comp.aClass246_Sub5_2301.method3115());
												}
											} else {
												model.method2329(Class76_Sub5.aClass111_3745, null, comp.zoom << 1326541250, 1);
											}
										} else if (comp.aBoolean2265) {
											model.method2329(Class76_Sub5.aClass111_3745, null, comp.zoom, 1);
										} else {
											model.method2325(Class76_Sub5.aClass111_3745, null, 1);
											if (comp.aClass246_Sub5_2301 != null) {
												client.graphicsToolkit.method1820(comp.aClass246_Sub5_2301.method3115());
											}
										}
										if (FloorUnderlayDefinitionParser.clipComponents) {
											client.graphicsToolkit.setClip(clipLeft, clipTop, clipRight, clipBottom);
										}
										if (comp.aBoolean2325) {
											client.graphicsToolkit.setDepthWriteMask(true);
										}
									}
									if (OpenGLHeap.aBoolean6079) {
										if (bool_57_) {
											AnimatedProgressBarLSEConfig.method908(cBottom, cTop, false, cLeft, cRight);
										} else {
											Class216.method2796(cBottom, cLeft, cRight, cTop, (byte) 95);
										}
									}
								} else if ((comp.type ^ 0xffffffff) == -10) {
									int i_109_;
									int i_110_;
									int i_111_;
									int i_112_;
									if (comp.lineMirrored) {
										i_112_ = c_y;
										i_110_ = c_x + comp.renderWidth;
										i_111_ = c_x;
										i_109_ = c_y - -comp.renderHeight;
									} else {
										i_109_ = c_y;
										i_110_ = comp.renderWidth + c_x;
										i_111_ = c_x;
										i_112_ = comp.renderHeight + c_y;
									}
									if (comp.lineWidth == 1) {
										client.graphicsToolkit.method1795(i_111_, i_109_, i_110_, i_112_, comp.defaultColour, 0);
									} else {
										client.graphicsToolkit.method1816(i_111_, i_109_, i_110_, i_112_, comp.defaultColour, comp.lineWidth, 0);
									}
									if (OpenGLHeap.aBoolean6079) {
										if (!bool_57_) {
											Class216.method2796(cBottom, cLeft, cRight, cTop, (byte) 102);
										} else {
											AnimatedProgressBarLSEConfig.method908(cBottom, cTop, false, cLeft, cRight);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private int	anInt5665	= 1;

	private int	anInt5672	= 1;

	public Class98_Sub10_Sub24() {
		super(1, false);
	}

	@Override
	public final int[] method990(int i, int i_0_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_0_);
			if (i != 255) {
				enumsJs5 = null;
			}
			if (this.aClass16_3863.aBoolean198) {
				int i_1_ = 1 + anInt5665 - -anInt5665;
				int i_2_ = 65536 / i_1_;
				int i_3_ = 1 + anInt5672 + anInt5672;
				int i_4_ = 65536 / i_3_;
				int[][] is_5_ = new int[i_1_][];
				for (int i_6_ = i_0_ + -anInt5665; anInt5665 + i_0_ >= i_6_; i_6_++) {
					int[] is_7_ = method1000(i_6_ & SoftwareNativeHeap.anInt6075, 0, i ^ 0xff);
					int[] is_8_ = new int[Class25.anInt268];
					int i_9_ = 0;
					for (int i_10_ = -anInt5672; (anInt5672 ^ 0xffffffff) <= (i_10_ ^ 0xffffffff); i_10_++) {
						i_9_ += is_7_[Class329.anInt2761 & i_10_];
					}
					int i_11_ = 0;
					while ((i_11_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff)) {
						is_8_[i_11_] = i_9_ * i_4_ >> 880426384;
						i_9_ -= is_7_[Class329.anInt2761 & -anInt5672 + i_11_];
						i_11_++;
						i_9_ += is_7_[Class329.anInt2761 & i_11_ - -anInt5672];
					}
					is_5_[i_6_ + anInt5665 + -i_0_] = is_8_;
				}
				for (int i_12_ = 0; (Class25.anInt268 ^ 0xffffffff) < (i_12_ ^ 0xffffffff); i_12_++) {
					int i_13_ = 0;
					for (int i_14_ = 0; i_14_ < i_1_; i_14_++) {
						i_13_ += is_5_[i_14_][i_12_];
					}
					is[i_12_] = i_2_ * i_13_ >> -153979312;
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "od.G(" + i + ',' + i_0_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_18_) {
		while_135_: do {
			try {
				if (i_18_ >= -92) {
					method1075(null, true);
				}
				int i_19_ = i;
				while_134_: do {
					do {
						if (i_19_ != 0) {
							if (i_19_ == 1) {
								break;
							}
							if (i_19_ != 2) {
								break while_135_;
							}
							if (!GameShell.cleanedStatics) {
								break while_134_;
							}
						}
						anInt5672 = class98_sub22.readUnsignedByte((byte) 126);
						return;
					} while (false);
					anInt5665 = class98_sub22.readUnsignedByte((byte) -111);
					return;
				} while (false);
				this.aBoolean3861 = class98_sub22.readUnsignedByte((byte) -118) == 1;
				break;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "od.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_18_ + ')');
			}
		} while (false);
	}

	@Override
	public final int[][] method997(int i, int i_20_) {
		try {
			if (i > -76) {
				anInt5671 = -102;
			}
			int[][] is = this.aClass223_3859.method2828(i_20_, 0);
			if (this.aClass223_3859.aBoolean1683) {
				int i_21_ = anInt5665 + anInt5665 + 1;
				int i_22_ = 65536 / i_21_;
				int i_23_ = anInt5672 + anInt5672 + 1;
				int i_24_ = 65536 / i_23_;
				int[][][] is_25_ = new int[i_21_][][];
				for (int i_26_ = -anInt5665 + i_20_; (i_26_ ^ 0xffffffff) >= (i_20_ - -anInt5665 ^ 0xffffffff); i_26_++) {
					int[][] is_27_ = method994(i_26_ & SoftwareNativeHeap.anInt6075, 24431, 0);
					int[][] is_28_ = new int[3][Class25.anInt268];
					int i_29_ = 0;
					int i_30_ = 0;
					int i_31_ = 0;
					int[] is_32_ = is_27_[0];
					int[] is_33_ = is_27_[1];
					int[] is_34_ = is_27_[2];
					for (int i_35_ = -anInt5672; (anInt5672 ^ 0xffffffff) <= (i_35_ ^ 0xffffffff); i_35_++) {
						int i_36_ = Class329.anInt2761 & i_35_;
						i_29_ += is_32_[i_36_];
						i_30_ += is_33_[i_36_];
						i_31_ += is_34_[i_36_];
					}
					int[] is_37_ = is_28_[0];
					int[] is_38_ = is_28_[1];
					int[] is_39_ = is_28_[2];
					int i_40_ = 0;
					while (Class25.anInt268 > i_40_) {
						is_37_[i_40_] = i_24_ * i_29_ >> -948678224;
						is_38_[i_40_] = i_24_ * i_30_ >> -1965134224;
						is_39_[i_40_] = i_24_ * i_31_ >> 1110096464;
						int i_41_ = Class329.anInt2761 & -anInt5672 + i_40_;
						i_30_ -= is_33_[i_41_];
						i_31_ -= is_34_[i_41_];
						i_29_ -= is_32_[i_41_];
						i_40_++;
						i_41_ = Class329.anInt2761 & i_40_ - -anInt5672;
						i_29_ += is_32_[i_41_];
						i_31_ += is_34_[i_41_];
						i_30_ += is_33_[i_41_];
					}
					is_25_[i_26_ - (-anInt5665 - -i_20_)] = is_28_;
				}
				int[] is_42_ = is[0];
				int[] is_43_ = is[1];
				int[] is_44_ = is[2];
				for (int i_45_ = 0; i_45_ < Class25.anInt268; i_45_++) {
					int i_46_ = 0;
					int i_47_ = 0;
					int i_48_ = 0;
					for (int i_49_ = 0; (i_21_ ^ 0xffffffff) < (i_49_ ^ 0xffffffff); i_49_++) {
						int[][] is_50_ = is_25_[i_49_];
						i_48_ += is_50_[2][i_45_];
						i_47_ += is_50_[1][i_45_];
						i_46_ += is_50_[0][i_45_];
					}
					is_42_[i_45_] = i_46_ * i_22_ >> 710103472;
					is_43_[i_45_] = i_47_ * i_22_ >> -1587122512;
					is_44_[i_45_] = i_22_ * i_48_ >> 807012816;
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "od.C(" + i + ',' + i_20_ + ')');
		}
	}
}

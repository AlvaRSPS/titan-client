
/* Class156_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

import jaclib.memory.Buffer;

public final class Class156_Sub2 extends Class156 implements ArrayBuffer {
	public static int anInt3423 = 0;

	public static final void method2500(int i, int i_2_) {
		Class49.anInt415 = i;
	}

	public static final long stringToLong(byte i, String string) {
		long l = 0L;
		int i_3_ = string.length();
		for (int i_4_ = 0; i_4_ < i_3_; i_4_++) {
			l *= 37L;
			int i_5_ = string.charAt(i_4_);
			if (i_5_ >= 65 && i_5_ <= 90) {
				l += 1 - (-i_5_ - -65);
			} else if (i_5_ >= 97 && i_5_ <= 122) {
				l += -97 + 1 + i_5_;
			} else if (i_5_ >= 48 && i_5_ <= 57) {
				l += i_5_ + -21;
			}
			if ((l ^ 0xffffffffffffffffL) <= -177917621779460414L) {
				break;
			}
		}
		for (/**/; (l % 37L ^ 0xffffffffffffffffL) == -1L && l != 0L; l /= 37L) {
			/* empty */
		}
		return l;
	}

	private int anInt3422;

	Class156_Sub2(OpenGlToolkit var_ha_Sub1, int i, Buffer buffer) {
		super(var_ha_Sub1, buffer);
		try {
			anInt3422 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "se.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + (buffer != null ? "{...}" : "null") + ')');
		}
	}

	Class156_Sub2(OpenGlToolkit var_ha_Sub1, int i, byte[] is, int i_6_) {
		super(var_ha_Sub1, is, i_6_);
		try {
			anInt3422 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "se.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + (is != null ? "{...}" : "null") + ',' + i_6_ + ')');
		}
	}

	@Override
	public final long method52(int i) {
		try {
			if (i != 24582) {
				anInt3422 = 44;
			}
			return this.aBuffer1247.getAddress();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "se.E(" + i + ')');
		}
	}

	@Override
	public final int method53(int i) {
		try {
			if (i != -14112) {
				method55(-28);
			}
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "se.B(" + i + ')');
		}
	}

	@Override
	public final void method54(int i, int i_0_, byte[] is, int i_1_) {
		try {
			method2496(is, i);
			if (i_0_ != 7896) {
				anInt3422 = -117;
			}
			anInt3422 = i_1_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "se.F(" + i + ',' + i_0_ + ',' + (is != null ? "{...}" : "null") + ',' + i_1_ + ')');
		}
	}

	@Override
	public final int method55(int i) {
		try {
			return anInt3422;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "se.C(" + i + ')');
		}
	}
}


/* PlayerUpdateMask - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.SunDefinition;

public final class PlayerUpdateMask {
	public static OutgoingOpcode	aClass171_524;
	public static Class259[]		aClass259Array527;
	public static int				mouseY	= 0;
	public static int				anInt529;
	public static int[][]			anIntArrayArray528;
	public static Js5				graphicJs5;

	static {
		aClass171_524 = new OutgoingOpcode(51, -1);
		anIntArrayArray528 = new int[128][128];
	}

	public static final void method709(Player player, int mask, RsBitsBuffers packet, byte i_0_, int index) {
		do {
			try {
				if ((0x400 & mask) != 0) {
					int i_2_ = packet.readShort1((byte) 51);
					player.anInt6394 = packet.readByteA(true);
					player.anInt6401 = packet.readByteS(-121);
					player.anInt6420 = i_2_ & 0x7fff;
					player.aBoolean6348 = (i_2_ & 0x8000) != 0;
					player.anInt6412 = player.anInt6420 + Queue.timer - -player.anInt6394;
				}
				byte i_3_ = -1;
				if ((mask & 0x4000) != 0) {
					int i_4_ = packet.readLEShortA((byte) -28);
					int i_5_ = packet.readInt2(-74);
					if (i_4_ == 65535) {
						i_4_ = -1;
					}
					int i_6_ = packet.readUnsignedByte((byte) -112);
					int i_7_ = 0x7 & i_6_;
					int i_8_ = (0x7e & i_6_) >> -1359180605;
					if (i_8_ == 15) {
						i_8_ = -1;
					}
					player.method3032(i_8_, true, i_5_, i_7_, i_4_, -94);
				}
				if ((mask & 0x2000) != 0) {
					i_3_ = packet.readByteA(-1498293360);
				}
				if ((0x40000 & mask) != 0) {
					int i_9_ = packet.readByteS(111);
					int[] is = new int[i_9_];
					int[] is_10_ = new int[i_9_];
					for (int i_11_ = 0; i_9_ > i_11_; i_11_++) {
						int i_12_ = packet.readShort1((byte) 111);
						if ((0xc000 & i_12_ ^ 0xffffffff) != -49153) {
							is[i_11_] = i_12_;
						} else {
							int i_13_ = packet.readShort1((byte) -76);
							is[i_11_] = Class41.or(i_13_, i_12_ << -663124336);
						}
						is_10_[i_11_] = packet.readShort((byte) 127);
					}
					player.method3038(is_10_, is, true);
				}
				if ((mask & 0x80 ^ 0xffffffff) != -1) {
					int i_14_ = packet.readShort((byte) 127);
					int i_15_ = packet.readIntReverse(true);
					if ((i_14_ ^ 0xffffffff) == -65536) {
						i_14_ = -1;
					}
					int i_16_ = packet.readByteA(true);
					int i_17_ = 0x7 & i_16_;
					int i_18_ = 0xf & i_16_ >> -1521340829;
					if ((i_18_ ^ 0xffffffff) == -16) {
						i_18_ = -1;
					}
					player.method3032(i_18_, false, i_15_, i_17_, i_14_, -72);
				}
				if ((mask & 0x200 ^ 0xffffffff) != -1) {
					player.anInt6378 = packet.method1187((byte) -112);
					player.anInt6347 = packet.readByteA(-1498293360);
					player.anInt6362 = packet.method1187((byte) -112);
					player.anInt6392 = packet.method1187((byte) -112);
					player.anInt6390 = packet.readShort1((byte) 92) + Queue.timer;
					player.anInt6424 = packet.readLEShortA((byte) -53) + Queue.timer;
					player.anInt6407 = packet.readByteA(true);
					if (!player.aBoolean6532) {
						player.anInt6347 += player.pathZ[0];
						player.anInt6392 += player.pathZ[0];
						player.anInt6378 += player.pathX[0];
						player.anInt6362 += player.pathX[0];
						player.pathLength = 1;
					} else {
						player.anInt6347 += player.path_z;
						player.anInt6392 += player.path_z;
						player.anInt6378 += player.path_x;
						player.anInt6362 += player.path_x;
						player.pathLength = 0;
					}
					player.anInt6436 = 0;
				}
				if ((mask & 0x4) != 0) {
					player.nextDirection = packet.readLEShortA((byte) 91);
					if (player.pathLength == 0) {
						player.method3042(player.nextDirection, -8193);
						player.nextDirection = -1;
					}
				}
				if ((0x2 & mask ^ 0xffffffff) != -1) {
					int i_19_ = packet.readShort1((byte) -126);
					if (i_19_ == 65535) {
						i_19_ = -1;
					}
					player.anInt6364 = i_19_;
				}
				if ((mask & 0x10000 ^ 0xffffffff) != -1) {
					player.aByte6404 = packet.method1187((byte) -112);
					player.aByte6381 = packet.method1234(128);
					player.aByte6368 = packet.method1234(128);
					player.aByte6422 = (byte) packet.readUnsignedByte((byte) 107);
					player.anInt6403 = Queue.timer + packet.readLEShortA((byte) 97);
					player.anInt6349 = Queue.timer - -packet.readShortA(98);
				}
				if ((0x8 & mask) != 0) {// hit
					int i_20_ = packet.readUnsignedByte((byte) 85);
					if (i_20_ > 0) {
						for (int i_21_ = 0; i_21_ < i_20_; i_21_++) {
							int i_22_ = -1;
							int i_23_ = -1;
							int i_24_ = packet.readSmart(1689622712);
							int i_25_ = -1;
							if (i_24_ != 32767) {
								if (i_24_ == 32766) {
									i_24_ = -1;
								} else {
									i_23_ = packet.readSmart(1689622712);
								}
							} else {
								i_24_ = packet.readSmart(1689622712);
								i_23_ = packet.readSmart(1689622712);
								i_22_ = packet.readSmart(1689622712);
								i_25_ = packet.readSmart(1689622712);
							}
							int i_26_ = packet.readSmart(1689622712);
							int i_27_ = packet.readUnsignedByte((byte) -105);
							player.method3037(i_27_, false, i_23_, i_24_, Queue.timer, i_25_, i_26_, i_22_);
						}
					}
				}
				if ((mask & 0x10 ^ 0xffffffff) != -1) {
					int[] is = new int[4];
					for (int i_28_ = 0; (i_28_ ^ 0xffffffff) > -5; i_28_++) {
						is[i_28_] = packet.readLEShortA((byte) -116);
						if ((is[i_28_] ^ 0xffffffff) == -65536) {
							is[i_28_] = -1;
						}
					}
					int i_29_ = packet.readByteC((byte) 58);
					Class181.method2608(i_29_, player, is, 0);
				}
				if ((0x40 & mask ^ 0xffffffff) != -1) {
					int i_30_ = packet.readByteA(true);
					byte[] is = new byte[i_30_];
					RSByteBuffer class98_sub22 = new RSByteBuffer(is);
					packet.getData(is, true, i_30_, 0);
					Class224_Sub3_Sub1.aClass98_Sub22Array6146[index] = class98_sub22;
					player.decodeAppearance(class98_sub22, (byte) 73);
				}
				if ((0x1000 & mask ^ 0xffffffff) != -1) {
					player.clanmate = packet.readByteA(true) == 1;
				}
				if ((mask & 0x8000) != 0) {
					player.message = packet.readString((byte) 84);
					if ((player.message.charAt(0) ^ 0xffffffff) == -127) {
						player.message = player.message.substring(1);
						ItemDeque.addChatMessage((byte) -96, 2, player.message, 0, player.getName(-1, false), player.formattedName(0, true), player.displayName);
					} else if (player == Class87.localPlayer) {
						ItemDeque.addChatMessage((byte) 90, 2, player.message, 0, player.getName(-1, false), player.formattedName(0, true), player.displayName);
					}
					player.anInt6384 = 150;
					player.anInt6398 = 0;
					player.anInt6402 = 0;
				}
				if ((0x100 & mask ^ 0xffffffff) != -1) {
					int i_31_ = packet.readByteC((byte) -109);
					int[] is = new int[i_31_];
					int[] is_32_ = new int[i_31_];
					int[] is_33_ = new int[i_31_];
					for (int i_34_ = 0; i_34_ < i_31_; i_34_++) {
						int i_35_ = packet.readShortA(79);
						if (i_35_ == 65535) {
							i_35_ = -1;
						}
						is[i_34_] = i_35_;
						is_32_[i_34_] = packet.readByteC((byte) 124);
						is_33_[i_34_] = packet.readLEShortA((byte) -37);
					}
					SunDefinition.method3234(player, -3433, is_33_, is_32_, is);
				}
				if ((0x1 & mask ^ 0xffffffff) != -1) {
					Class98_Sub10_Sub21.playerMovementSpeeds[index] = packet.readByteA(-1498293360);
				}
				if (!player.aBoolean6532) {
					break;
				}
				if (i_3_ != 127) {
					byte i_36_;
					if (i_3_ == -1) {
						i_36_ = Class98_Sub10_Sub21.playerMovementSpeeds[index];
					} else {
						i_36_ = i_3_;
					}
					LoadingScreenSequence.method3334((byte) 37, i_36_, player);
					player.move(player.path_x, player.path_z, i_36_, -1);
				} else {
					player.move(player.path_z, player.path_x, 1470);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "eia.B(" + (player != null ? "{...}" : "null") + ',' + mask + ',' + (packet != null ? "{...}" : "null") + ',' + i_0_ + ',' + index + ')');
			}
			break;
		} while (false);
	}
}

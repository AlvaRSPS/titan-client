/* Class200 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.matrix.Matrix;

public final class Class200 {
	public static Matrix aClass111_1543;

	public static final int encodeJString(String string, int start, int end, byte[] data, int offset) {
		int count = end - start;
		for (int pos = 0; pos < count; pos++) {
			char c = string.charAt(start + pos);
			if (c > '\0' && c < '\u0080' || c >= '\u00a0' && c <= '\u00ff') {
				data[offset + pos] = (byte) c;
			} else {
				switch (c) {
				case '\u20ac':
					data[offset + pos] = (byte) 0xffffff80;
					break;
				case '\u201a':
					data[offset + pos] = (byte) 0xffffff82;
					break;
				case '\u0192':
					data[offset + pos] = (byte) 0xffffff83;
					break;
				case '\u201e':
					data[offset + pos] = (byte) 0xffffff84;
					break;
				case '\u2026':
					data[offset + pos] = (byte) 0xffffff85;
					break;
				case '\u2020':
					data[offset + pos] = (byte) 0xffffff86;
					break;
				case '\u2021':
					data[offset + pos] = (byte) 0xffffff87;
					break;
				case '\u02c6':
					data[offset + pos] = (byte) 0xffffff88;
					break;
				case '\u2030':
					data[offset + pos] = (byte) 0xffffff89;
					break;
				case '\u0160':
					data[offset + pos] = (byte) 0xffffff8a;
					break;
				case '\u2039':
					data[offset + pos] = (byte) 0xffffff8b;
					break;
				case '\u0152':
					data[offset + pos] = (byte) 0xffffff8c;
					break;
				case '\u017d':
					data[offset + pos] = (byte) 0xffffff8e;
					break;
				case '\u2018':
					data[offset + pos] = (byte) 0xffffff91;
					break;
				case '\u2019':
					data[offset + pos] = (byte) 0xffffff92;
					break;
				case '\u201c':
					data[offset + pos] = (byte) 0xffffff93;
					break;
				case '\u201d':
					data[offset + pos] = (byte) 0xffffff94;
					break;
				case '\u2022':
					data[offset + pos] = (byte) 0xffffff95;
					break;
				case '\u2013':
					data[offset + pos] = (byte) 0xffffff96;
					break;
				case '\u2014':
					data[offset + pos] = (byte) 0xffffff97;
					break;
				case '\u02dc':
					data[offset + pos] = (byte) 0xffffff98;
					break;
				case '\u2122':
					data[offset + pos] = (byte) 0xffffff99;
					break;
				case '\u0161':
					data[offset + pos] = (byte) 0xffffff9a;
					break;
				case '\u203a':
					data[offset + pos] = (byte) 0xffffff9b;
					break;
				case '\u0153':
					data[offset + pos] = (byte) 0xffffff9c;
					break;
				case '\u017e':
					data[offset + pos] = (byte) 0xffffff9e;
					break;
				case '\u0178':
					data[offset + pos] = (byte) 0xffffff9f;
					break;
				default:
					data[offset + pos] = (byte) '?';
					break;
				}
			}
		}
		return count;
	}

	public static final int method2692(int i) {
		int i_0_ = -1;
		for (int i_1_ = 0; i_1_ < Class18.anInt212 - 1; i_1_++) {
			if (i < Ground.anIntArray2205[i_1_] + Class15.anIntArray182[i_1_]) {
				i_0_ = i_1_;
				break;
			}
		}
		if (i_0_ == -1) {
			i_0_ = Class18.anInt212 - 1;
		}
		return i_0_;
	}

	public static final Sprite method2693(int i, byte i_2_, RSToolkit var_ha) {
		try {
			if (i_2_ > -104) {
				return null;
			}
			Class98_Sub36 class98_sub36 = (Class98_Sub36) NativeOpenGlElementArrayBuffer.aClass377_3277.get(i, -1);
			if (class98_sub36 != null) {
				Class98_Sub43_Sub3 class98_sub43_sub3 = class98_sub36.aClass237_Sub1_4157.method2908(-32675);
				class98_sub36.aBoolean4158 = true;
				if (class98_sub43_sub3 != null) {
					return class98_sub43_sub3.method1501(var_ha, 11242);
				}
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nd.A(" + i + ',' + i_2_ + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	public static void method2695(int i) {
		do {
			try {
				aClass111_1543 = null;
				if (i == -382) {
					break;
				}
				aClass111_1543 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "nd.B(" + i + ')');
			}
			break;
		} while (false);
	}

	public Class200() {
		/* empty */
	}

	@Override
	public final String toString() {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nd.toString(" + ')');
		}
	}
}

/* Class246_Sub3_Sub4_Sub2_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.GraphicsDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class NPC extends Mob {
	public static OutgoingOpcode	aClass171_6506	= new OutgoingOpcode(5, 2);
	public static Sprite			aClass332_6508;
	public static int				anInt6500		= -1;
	public static int				anInt6509;
	public static int				anInt6511;
	public static String			aString6507;

	Class40							aClass40_6502;
	int								anInt6501		= 1;
	int								anInt6503		= 1;
	int								anInt6505		= -1;
	int								anInt6510		= -1;

	public NPCDefinition			definition;

	public NPC() {
		/* empty */
	}

	@Override
	public final int getArmyIcon(int i) {
		if (definition.anIntArray1109 != null) {
			NPCDefinition def = definition.method2300(StartupStage.varValues, (byte) 82);
			if (def != null && (def.armyIcon ^ 0xffffffff) != 0) {
				return def.armyIcon;
			}
		}
		return definition.armyIcon;
	}

	@Override
	public final int getRenderId(byte i) {
		if (definition.anIntArray1109 != null) {
			NPCDefinition def = definition.method2300(StartupStage.varValues, (byte) 121);
			if (def != null && def.renderId != -1) {
				return def.renderId;
			}
		}
		return definition.renderId;
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit toolkit) {
		return null;
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit toolkit, int i) {
		try {
			if (definition == null || !method3048(toolkit, 255, 2048)) {
				return null;
			}
			Matrix class111 = toolkit.method1793();
			int i_51_ = this.yaw.value((byte) 116);
			class111.method2101(i_51_);
			Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[this.plane][this.boundExtentsX >> Class151_Sub8.tileScale][this.boundExtentsZ >> Class151_Sub8.tileScale];
			if (class172 == null || class172.aClass246_Sub3_Sub1_1332 == null) {
				this.anInt6351 -= this.anInt6351 / 10.0F;
			} else {
				int i_52_ = -class172.aClass246_Sub3_Sub1_1332.aShort6149 + this.anInt6351;
				this.anInt6351 -= i_52_ / 10.0F;
			}
			class111.translate(this.boundExtentsX, -this.anInt6351 + this.anInt5089 + -20, this.boundExtentsZ);
			RenderAnimDefinition class294 = method3039(1);
			NPCDefinition class141 = definition.anIntArray1109 != null ? definition.method2300(StartupStage.varValues, (byte) 70) : definition;
			this.aBoolean6442 = false;
			Class246_Sub1 class246_sub1 = null;
			if ((client.preferences.characterShadows.getValue((byte) 123) ^ 0xffffffff) == -2 && class141.aBoolean1130 && class294.aBoolean2400) {
				AnimationDefinition class97 = this.anInt6413 == -1 || (this.anInt6400 ^ 0xffffffff) != -1 ? null : Class151_Sub7.animationDefinitionList.method2623(this.anInt6413, 16383);
				AnimationDefinition class97_53_ = this.anInt6385 == -1 || this.aBoolean6359 && class97 != null ? null : Class151_Sub7.animationDefinitionList.method2623(this.anInt6385, 16383);
				ModelRenderer class146 = PrefetchObject.method1703(class97_53_ == null ? class97 : class97_53_, this.anInt6377, definition.boundSize, definition.aByte1134 & 0xff, this.aClass146Array6441[0], this.anInt6388, i_51_, 121, 0xff & definition.aByte1122,
						definition.aShort1135 & 0xffff, class97_53_ != null ? this.anInt6350 : this.anInt6393, toolkit, definition.aShort1094 & 0xffff, this.anInt6416);
				if (class146 != null) {
					class246_sub1 = Class94.method915(1 + this.aClass146Array6441.length, (byte) -47, method3051(2));
					this.aBoolean6442 = true;
					toolkit.setDepthWriteMask(false);
					if (VarClientStringsDefinitionParser.aBoolean1839) {
						class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[this.aClass146Array6441.length], Class16.anInt197, 0);
					} else {
						class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[this.aClass146Array6441.length], 0);
					}
					toolkit.setDepthWriteMask(true);
				}
			}
			class111.method2101(i_51_);
			class111.translate(this.boundExtentsX, this.anInt5089 - 5 + -this.anInt6351, this.boundExtentsZ);
			if (class246_sub1 == null) {
				class246_sub1 = Class94.method915(this.aClass146Array6441.length, (byte) -47, method3051(2));
			}
			method3036(toolkit, false, (byte) -124, class111, this.aClass146Array6441);
			if (VarClientStringsDefinitionParser.aBoolean1839) {
				for (int i_54_ = 0; i_54_ < this.aClass146Array6441.length; i_54_++) {
					if (this.aClass146Array6441[i_54_] != null) {
						this.aClass146Array6441[i_54_].method2329(class111, class246_sub1.aClass246_Sub6Array5067[i_54_], Class16.anInt197, 0);
					}
				}
			} else {
				for (int i_55_ = 0; (i_55_ ^ 0xffffffff) > (this.aClass146Array6441.length ^ 0xffffffff); i_55_++) {
					if (this.aClass146Array6441[i_55_] != null) {
						this.aClass146Array6441[i_55_].method2325(class111, class246_sub1.aClass246_Sub6Array5067[i_55_], 0);
					}
				}
			}
			if (this.aClass246_Sub5_6439 != null) {
				Class242 class242 = this.aClass246_Sub5_6439.method3116();
				/*if (Player.cprgbe)
					toolkit.method1820_cp(class242, Player.pDescriptor, Player.intensity, Player.ambient);
				else
					toolkit.method1820(class242);*/
				/*if (!VarClientStringsDefinitionParser.aBoolean1839) {
					toolkit.method1820(class242);
				} else {
					toolkit.method1785(class242, Class16.anInt197);
				}*/
			}
			for (ModelRenderer element : this.aClass146Array6441) {
				if (element != null) {
					this.aBoolean6442 |= element.F();
				}
			}
			((Mob) this).aClass146Array6441[0] = ((Mob) this).aClass146Array6441[1] = ((Mob) this).aClass146Array6441[2] = null;
			this.anInt6417 = RemoveRoofsPreferenceField.anInt3676;
			if (i > -12) {
				method2982((byte) -72);
			}
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.QA(" + (toolkit != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_45_, int i_46_) {
		try {
			if (definition == null || !method3048(var_ha, 255, 131072)) {
				return false;
			}
			Matrix class111 = var_ha.method1793();
			int i_47_ = this.yaw.value((byte) 116);
			class111.method2101(i_47_);
			if (i_45_ <= 59) {
				method2975(null, -17);
			}
			class111.translate(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
			boolean bool = false;
			for (ModelRenderer element : this.aClass146Array6441) {
				if (element != null) {
					boolean bool_49_ = (definition.anInt1123 ^ 0xffffffff) < -1 || ((definition.anInt1096 ^ 0xffffffff) == 0 ? definition.boundSize == 1 : (definition.anInt1096 ^ 0xffffffff) == -2);
					boolean bool_50_;
					if (!VarClientStringsDefinitionParser.aBoolean1839) {
						bool_50_ = element.method2339(i, i_46_, class111, bool_49_, definition.anInt1123);
					} else {
						bool_50_ = element.method2333(i, i_46_, class111, bool_49_, definition.anInt1123, Class16.anInt197);
					}
					if (bool_50_) {
						bool = true;
						break;
					}
				}
			}
			((Mob) this).aClass146Array6441[0] = ((Mob) this).aClass146Array6441[1] = ((Mob) this).aClass146Array6441[2] = null;
			return bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_45_ + ',' + i_46_ + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_41_, RSToolkit var_ha, int i_42_, int i_43_) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_41_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_42_ + ',' + i_43_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.HA(" + i + ')');
		}
	}

	@Override
	public final int method2986(int i) {
		try {
			if (definition == null) {
				return 0;
			}
			return definition.anInt1123;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.FB(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		try {
			if (definition != null && (this.aBoolean6440 || method3048(var_ha, 255, 0))) {
				Matrix class111 = var_ha.method1793();
				class111.method2101(this.yaw.value((byte) 116));
				class111.translate(this.boundExtentsX, this.anInt5089 - 20, this.boundExtentsZ);
				method3036(var_ha, this.aBoolean6440, (byte) -119, class111, this.aClass146Array6441);
				((Mob) this).aClass146Array6441[0] = ((Mob) this).aClass146Array6441[1] = ((Mob) this).aClass146Array6441[2] = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.DA(" + i + ')');
		}
	}

	@Override
	public final int method3044(boolean bool) {
		try {
			if (bool != false) {
				definition = null;
			}
			if (definition.anIntArray1109 != null) {
				NPCDefinition class141 = definition.method2300(StartupStage.varValues, (byte) 70);
				if (class141 != null && (class141.anInt1092 ^ 0xffffffff) != 0) {
					return class141.anInt1092;
				}
			}
			if (definition.anInt1092 == -1) {
				return super.method3044(false);
			}
			return definition.anInt1092;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.B(" + bool + ')');
		}
	}

	public final boolean method3048(RSToolkit toolkit, int i, int i_1_) {
		try {
			int i_2_ = i_1_;
			RenderAnimDefinition class294 = method3039(1);
			if (i != 255) {
				aString6507 = null;
			}
			AnimationDefinition class97 = this.anInt6413 != -1 && (this.anInt6400 ^ 0xffffffff) == -1 ? Class151_Sub7.animationDefinitionList.method2623(this.anInt6413, i ^ 0x3f00) : null;
			AnimationDefinition class97_3_ = (this.anInt6385 ^ 0xffffffff) == 0 || this.aBoolean6359 && class97 != null ? null : Class151_Sub7.animationDefinitionList.method2623(this.anInt6385, 16383);
			int i_4_ = class294.anInt2362;
			int i_5_ = class294.anInt2382;
			if ((i_4_ ^ 0xffffffff) != -1 || (i_5_ ^ 0xffffffff) != -1 || (class294.anInt2393 ^ 0xffffffff) != -1 || class294.anInt2363 != 0) {
				i_1_ |= 0x7;
			}
			boolean bool = (this.aByte6422 ^ 0xffffffff) != -1 && Queue.timer >= this.anInt6403 && (Queue.timer ^ 0xffffffff) > (this.anInt6349 ^ 0xffffffff);
			if (bool) {
				i_1_ |= 0x80000;
			}
			int i_6_ = this.yaw.value((byte) 116);
			ModelRenderer class146 = ((Mob) this).aClass146Array6441[0] = definition.method2301(this.anInt6393, i_6_, this.anIntArray6370, this.anInt6350, (byte) 100, class97, this.aClass226Array6387, toolkit, this.anInt6409, this.anInt6361,
					StartupStage.varValues, aClass40_6502, this.anInt6366, Class370.renderAnimDefinitionList, i_1_, this.anInt6419, Class151_Sub7.animationDefinitionList, class97_3_);
			if (class146 == null) {
				return false;
			}
			this.anInt6352 = class146.fa();
			this.anInt6354 = class146.ma();
			method3046(758, class146);
			if ((i_4_ ^ 0xffffffff) != -1 || i_5_ != 0) {
				method3040(false, class294.anInt2360, i_4_, i_5_, i_6_, class294.anInt2391);
				if ((this.anInt6388 ^ 0xffffffff) != -1) {
					this.aClass146Array6441[0].FA(this.anInt6388);
				}
				if (this.anInt6377 != 0) {
					this.aClass146Array6441[0].VA(this.anInt6377);
				}
				if (this.anInt6416 != 0) {
					this.aClass146Array6441[0].H(0, this.anInt6416, 0);
				}
			} else {
				method3040(false, 0, getSize(i + -255) << 188560585, getSize(0) << -1964945015, i_6_, 0);
			}
			if (bool) {
				class146.method2337(this.aByte6404, this.aByte6381, this.aByte6368, this.aByte6422 & 0xff);
			}
			if (this.anInt6379 == -1 || (this.anInt6376 ^ 0xffffffff) == 0) {
				((Mob) this).aClass146Array6441[1] = null;
			} else {
				GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(i + -253, this.anInt6379);
				boolean bool_7_ = class107.aByte923 == 3 && (i_4_ != 0 || i_5_ != 0);
				int i_8_ = i_2_;
				if (!bool_7_) {
					if (this.anInt6389 != 0) {
						i_8_ |= 0x5;
					}
					if ((this.anInt6382 ^ 0xffffffff) != -1) {
						i_8_ |= 0x2;
					}
					if (this.anInt6410 >= 0) {
						i_8_ |= 0x7;
					}
				} else {
					i_8_ |= 0x7;
				}
				ModelRenderer class146_9_ = ((Mob) this).aClass146Array6441[1] = class107.method1728(this.anInt6376, Class151_Sub7.animationDefinitionList, i_8_, this.anInt6396, (byte) 66, this.anInt6367, toolkit);
				if (class146_9_ != null) {
					if ((this.anInt6410 ^ 0xffffffff) > -1) {
						if (this.anInt6389 != 0) {
							class146_9_.rotateYaw(2048 * this.anInt6389);
						}
					} else {
						int i_10_ = 0;
						int i_11_ = 0;
						int i_12_ = 0;
						if (class294.anIntArrayArray2366 != null && class294.anIntArrayArray2366[this.anInt6410] != null) {
							i_11_ += class294.anIntArrayArray2366[this.anInt6410][1];
							i_12_ += class294.anIntArrayArray2366[this.anInt6410][2];
							i_10_ += class294.anIntArrayArray2366[this.anInt6410][0];
						}
						if (class294.anIntArrayArray2364 != null && class294.anIntArrayArray2364[this.anInt6410] != null) {
							i_10_ += class294.anIntArrayArray2364[this.anInt6410][0];
							i_11_ += class294.anIntArrayArray2364[this.anInt6410][1];
							i_12_ += class294.anIntArrayArray2364[this.anInt6410][2];
						}
						if ((i_12_ ^ 0xffffffff) != -1 || i_10_ != 0) {
							int i_13_ = i_6_;
							if (this.anIntArray6370 != null && (this.anIntArray6370[this.anInt6410] ^ 0xffffffff) != 0) {
								i_13_ = this.anIntArray6370[this.anInt6410];
							}
							int i_14_ = i_13_ + 2048 * this.anInt6389 - i_6_ & 0x3fff;
							if (i_14_ != 0) {
								class146_9_.rotateYaw(i_14_);
							}
							int i_15_ = Class284_Sub2_Sub2.SINE[i_14_];
							int i_16_ = Class284_Sub2_Sub2.COSINE[i_14_];
							int i_17_ = i_10_ * i_16_ + i_15_ * i_12_ >> -874650578;
							i_12_ = -(i_10_ * i_15_) + i_12_ * i_16_ >> 562276558;
							i_10_ = i_17_;
						}
						class146_9_.H(i_10_, i_11_, i_12_);
					}
					if (this.anInt6382 != 0) {
						class146_9_.H(0, -this.anInt6382 << -1220664798, 0);
					}
					if (bool_7_) {
						if ((this.anInt6388 ^ 0xffffffff) != -1) {
							class146_9_.FA(this.anInt6388);
						}
						if ((this.anInt6377 ^ 0xffffffff) != -1) {
							class146_9_.VA(this.anInt6377);
						}
						if ((this.anInt6416 ^ 0xffffffff) != -1) {
							class146_9_.H(0, this.anInt6416, 0);
						}
					}
				}
			}
			if ((this.anInt6365 ^ 0xffffffff) == 0 || this.anInt6428 == -1) {
				((Mob) this).aClass146Array6441[2] = null;
			} else {
				GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, this.anInt6365);
				boolean bool_18_ = (class107.aByte923 ^ 0xffffffff) == -4 && (i_4_ != 0 || i_5_ != 0);
				int i_19_ = i_2_;
				if (!bool_18_) {
					if (this.anInt6360 != 0) {
						i_19_ |= 0x5;
					}
					if ((this.anInt6363 ^ 0xffffffff) != -1) {
						i_19_ |= 0x2;
					}
					if ((this.anInt6353 ^ 0xffffffff) <= -1) {
						i_19_ |= 0x7;
					}
				} else {
					i_19_ |= 0x7;
				}
				ModelRenderer class146_20_ = ((Mob) this).aClass146Array6441[2] = class107.method1721(toolkit, this.anInt6421, 21945, i_19_, Class151_Sub7.animationDefinitionList, this.anInt6427, this.anInt6428);
				if (class146_20_ != null) {
					if ((this.anInt6353 ^ 0xffffffff) > -1 || class294.anIntArrayArray2366 == null || class294.anIntArrayArray2366[this.anInt6353] == null) {
						if (this.anInt6360 != 0) {
							class146_20_.rotateYaw(2048 * this.anInt6360);
						}
					} else {
						int i_21_ = 0;
						int i_22_ = 0;
						int i_23_ = 0;
						if (class294.anIntArrayArray2366 != null && class294.anIntArrayArray2366[this.anInt6353] != null) {
							i_21_ += class294.anIntArrayArray2366[this.anInt6353][0];
							i_22_ += class294.anIntArrayArray2366[this.anInt6353][1];
							i_23_ += class294.anIntArrayArray2366[this.anInt6353][2];
						}
						if (class294.anIntArrayArray2364 != null && class294.anIntArrayArray2364[this.anInt6353] != null) {
							i_22_ += class294.anIntArrayArray2364[this.anInt6353][1];
							i_21_ += class294.anIntArrayArray2364[this.anInt6353][0];
							i_23_ += class294.anIntArrayArray2364[this.anInt6353][2];
						}
						if (i_23_ != 0 || i_21_ != 0) {
							int i_24_ = i_6_;
							if (this.anIntArray6370 != null && this.anIntArray6370[this.anInt6353] != -1) {
								i_24_ = this.anIntArray6370[this.anInt6353];
							}
							int i_25_ = 0x3fff & i_24_ + this.anInt6360 * 2048 - i_6_;
							if ((i_25_ ^ 0xffffffff) != -1) {
								class146_20_.rotateYaw(i_25_);
							}
							int i_26_ = Class284_Sub2_Sub2.SINE[i_25_];
							int i_27_ = Class284_Sub2_Sub2.COSINE[i_25_];
							int i_28_ = i_23_ * i_26_ + i_21_ * i_27_ >> 349754830;
							i_23_ = i_27_ * i_23_ - i_21_ * i_26_ >> 1020454830;
							i_21_ = i_28_;
						}
						class146_20_.H(i_21_, i_22_, i_23_);
					}
					if (this.anInt6363 != 0) {
						class146_20_.H(0, -this.anInt6363 << 1062444738, 0);
					}
					if (bool_18_) {
						if (this.anInt6388 != 0) {
							class146_20_.FA(this.anInt6388);
						}
						if (this.anInt6377 != 0) {
							class146_20_.VA(this.anInt6377);
						}
						if (this.anInt6416 != 0) {
							class146_20_.H(0, this.anInt6416, 0);
						}
					}
				}
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.CB(" + (toolkit != null ? "{...}" : "null") + ',' + i + ',' + i_1_ + ')');
		}
	}

	public final void method3049(int i, int i_29_, boolean bool, byte i_30_, int i_31_, int i_32_) {
		do {
			try {
				this.plane = this.collisionPlane = (byte) i_32_;
				if (Class1.isBridge(i_31_, (byte) -99, i_29_)) {
					this.collisionPlane++;
				}
				if ((this.anInt6413 ^ 0xffffffff) != 0 && (Class151_Sub7.animationDefinitionList.method2623(this.anInt6413, 16383).priority ^ 0xffffffff) == -2) {
					this.anIntArray6373 = null;
					this.anInt6413 = -1;
				}
				if (this.anInt6379 != -1) {
					GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, this.anInt6379);
					if (class107.aBoolean909 && (class107.animation ^ 0xffffffff) != 0 && (Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383).priority ^ 0xffffffff) == -2) {
						this.anInt6379 = -1;
					}
				}
				if ((this.anInt6365 ^ 0xffffffff) != 0) {
					GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, this.anInt6365);
					if (class107.aBoolean909 && (class107.animation ^ 0xffffffff) != 0 && Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383).priority == 1) {
						this.anInt6365 = -1;
					}
				}
				if (!bool) {
					int i_33_ = -this.pathX[0] + i_29_;
					int i_34_ = i_31_ + -this.pathZ[0];
					if ((i_33_ ^ 0xffffffff) <= 7 && (i_33_ ^ 0xffffffff) >= -9 && (i_34_ ^ 0xffffffff) <= 7 && i_34_ <= 8) {
						if ((this.pathLength ^ 0xffffffff) > -10) {
							this.pathLength++;
						}
						for (int i_35_ = this.pathLength; (i_35_ ^ 0xffffffff) < -1; i_35_--) {
							((Mob) this).pathX[i_35_] = this.pathX[-1 + i_35_];
							((Mob) this).pathZ[i_35_] = this.pathZ[i_35_ + -1];
							((Mob) this).pathSpeed[i_35_] = this.pathSpeed[-1 + i_35_];
						}
						((Mob) this).pathX[0] = i_29_;
						((Mob) this).pathSpeed[0] = (byte) 1;
						((Mob) this).pathZ[0] = i_31_;
						break;
					}
				}
				this.pathLength = 0;
				((Mob) this).pathX[0] = i_29_;
				this.anInt6436 = 0;
				this.anInt6433 = 0;
				((Mob) this).pathZ[0] = i_31_;
				this.boundExtentsZ = (i << -869632920) + (this.pathZ[0] << 1403178185);
				this.boundExtentsX = (this.pathX[0] << 348528265) + (i << 1193276360);
				if (i_30_ >= -105) {
					anInt6501 = -54;
				}
				if (this.aClass246_Sub5_6439 == null) {
					break;
				}
				this.aClass246_Sub5_6439.method3127();
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cea.GB(" + i + ',' + i_29_ + ',' + bool + ',' + i_30_ + ',' + i_31_ + ',' + i_32_ + ')');
			}
			break;
		} while (false);
	}

	public final void method3050(int i, int i_36_, int i_37_) {
		try {
			int i_38_ = this.pathX[0];
			int i_39_ = this.pathZ[0];
			if (i_37_ == 0) {
				i_39_++;
			}
			if (i_37_ == 1) {
				i_38_++;
				i_39_++;
			}
			if (i_37_ == 2) {
				i_38_++;
			}
			if (i_37_ == 3) {
				i_38_++;
				i_39_--;
			}
			if (i_37_ == 4) {
				i_39_--;
			}
			if (i_37_ == 5) {
				i_38_--;
				i_39_--;
			}
			if (i_37_ == 6) {
				i_38_--;
			}
			if (i_37_ == 7) {
				i_39_++;
				i_38_--;
			}
			if (this.anInt6413 != -1 && Class151_Sub7.animationDefinitionList.method2623(this.anInt6413, 16383).priority == 1) {
				this.anInt6413 = -1;
				this.anIntArray6373 = null;
			}
			if ((this.anInt6379 ^ 0xffffffff) != i) {
				GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(i + 2, this.anInt6379);
				if (class107.aBoolean909 && class107.animation != -1 && Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383).priority == 1) {
					this.anInt6379 = -1;
				}
			}
			if (this.anInt6365 != -1) {
				GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(i + 2, this.anInt6365);
				if (class107.aBoolean909 && (class107.animation ^ 0xffffffff) != 0 && Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383).priority == 1) {
					this.anInt6365 = -1;
				}
			}
			if (this.pathLength < 9) {
				this.pathLength++;
			}
			for (int i_40_ = this.pathLength; (i_40_ ^ 0xffffffff) < -1; i_40_--) {
				((Mob) this).pathX[i_40_] = this.pathX[-1 + i_40_];
				((Mob) this).pathZ[i_40_] = this.pathZ[-1 + i_40_];
				((Mob) this).pathSpeed[i_40_] = this.pathSpeed[-1 + i_40_];
			}
			((Mob) this).pathX[0] = i_38_;
			((Mob) this).pathZ[0] = i_39_;
			((Mob) this).pathSpeed[0] = (byte) i_36_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.EB(" + i + ',' + i_36_ + ',' + i_37_ + ')');
		}
	}

	public final boolean method3051(int i) {
		try {
			if (i != 2) {
				anInt6503 = -16;
			}
			return definition.visibleOnMinimap;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.DB(" + i + ')');
		}
	}

	public final boolean method3052(byte i) {
		try {
			return definition != null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cea.WA(" + i + ')');
		}
	}

	public final void setDefinition(NPCDefinition class141, int i) {
		do {
			try {
				definition = class141;
				if (i != 1) {
					anInt6510 = -51;
				}
				if (this.aClass246_Sub5_6439 == null) {
					break;
				}
				this.aClass246_Sub5_6439.method3127();
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cea.AB(" + (class141 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}
}

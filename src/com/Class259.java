/* Class259 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Frame;

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5FileRequest;

public final class Class259 {
	public static int	anInt1954		= 0;
	public static int	anInt1959;
	public static int	anInt1960		= -1;
	public static int[]	anIntArray1957	= new int[3];

	public static void method3205(boolean bool) {
		try {
			if (bool == true) {
				anIntArray1957 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "qca.A(" + bool + ')');
		}
	}

	public static final Class259[] method451(int i) {
		if (PlayerUpdateMask.aClass259Array527 == null) {
			Class259[] class259s = Class259.method490(GameShell.signLink, (byte) -68);
			Class259[] class259s_9_ = new Class259[class259s.length];
			int i_10_ = 0;
			int i_11_ = client.preferences.maxScreenSize.getValue((byte) 123);
			while_55_: for (int i_12_ = 0; class259s.length > i_12_; i_12_++) {
				Class259 class259 = class259s[i_12_];
				if (((class259.anInt1955 ^ 0xffffffff) >= -1 || (class259.anInt1955 ^ 0xffffffff) <= -25) && class259.anInt1953 >= 800 && (class259.anInt1956 ^ 0xffffffff) <= -601 && ((i_11_ ^ 0xffffffff) != -3 || class259.anInt1953 <= 800 && class259.anInt1956 <= 600) && ((i_11_ ^ 0xffffffff) != -2
						|| class259.anInt1953 <= 1024 && (class259.anInt1956 ^ 0xffffffff) >= -769)) {
					for (int i_13_ = 0; i_10_ > i_13_; i_13_++) {
						Class259 class259_14_ = class259s_9_[i_13_];
						if ((class259_14_.anInt1953 ^ 0xffffffff) == (class259.anInt1953 ^ 0xffffffff) && (class259_14_.anInt1956 ^ 0xffffffff) == (class259.anInt1956 ^ 0xffffffff)) {
							if (class259.anInt1955 > class259_14_.anInt1955) {
								class259s_9_[i_13_] = class259;
							}
							continue while_55_;
						}
					}
					class259s_9_[i_10_] = class259;
					i_10_++;
				}
			}
			PlayerUpdateMask.aClass259Array527 = new Class259[i_10_];
			ArrayUtils.method2892(class259s_9_, 0, PlayerUpdateMask.aClass259Array527, 0, i_10_);
			int[] is = new int[PlayerUpdateMask.aClass259Array527.length];
			for (int i_15_ = 0; (i_15_ ^ 0xffffffff) > (PlayerUpdateMask.aClass259Array527.length ^ 0xffffffff); i_15_++) {
				Class259 class259 = PlayerUpdateMask.aClass259Array527[i_15_];
				is[i_15_] = class259.anInt1956 * class259.anInt1953;
			}
			Js5FileRequest.method1597(is, PlayerUpdateMask.aClass259Array527, 0);
		}
		return PlayerUpdateMask.aClass259Array527;
	}

	public static final Class259[] method490(SignLink class88, byte i) {
		if (!class88.canChangeResolution((byte) -112)) {
			return new Class259[0];
		}
		SignLinkRequest class143 = class88.listVideoModes(55);
		while (class143.status == 0) {
			TimeTools.sleep(0, 10L);
		}
		if ((class143.status ^ 0xffffffff) == -3) {
			return new Class259[0];
		}
		int[] is = (int[]) class143.result;
		Class259[] class259s = new Class259[is.length >> -574919422];
		int i_0_ = 0;
		for (/**/; (i_0_ ^ 0xffffffff) > (class259s.length ^ 0xffffffff); i_0_++) {
			Class259 class259 = new Class259();
			class259s[i_0_] = class259;
			class259.anInt1953 = is[i_0_ << -22199518];
			class259.anInt1956 = is[(i_0_ << -27193534) + 1];
			class259.anInt1955 = is[2 + (i_0_ << -1744757374)];
			class259.anInt1958 = is[(i_0_ << -1958187070) - -3];
		}
		return class259s;
	}

	public static final Frame method503(int i, byte i_1_, int i_2_, int i_3_, int i_4_, SignLink class88) {
		if (!class88.canChangeResolution((byte) 83)) {
			return null;
		}
		if ((i ^ 0xffffffff) == -1) {
			Class259[] class259s = Class259.method490(class88, (byte) -94);
			if (class259s == null) {
				return null;
			}
			boolean bool = false;
			for (int i_5_ = 0; (class259s.length ^ 0xffffffff) < (i_5_ ^ 0xffffffff); i_5_++) {
				if (i_2_ == class259s[i_5_].anInt1953 && i_3_ == class259s[i_5_].anInt1956 && (i_4_ == 0 || i_4_ == class259s[i_5_].anInt1958) && (!bool || (class259s[i_5_].anInt1955 ^ 0xffffffff) < (i ^ 0xffffffff))) {
					bool = true;
					i = class259s[i_5_].anInt1955;
				}
			}
			if (!bool) {
				return null;
			}
		}
		SignLinkRequest class143 = class88.enterFullScreen(-21605, i, i_2_, i_4_, i_3_);
		while ((class143.status ^ 0xffffffff) == -1) {
			TimeTools.sleep(0, 10L);
		}
		Frame frame = (Frame) class143.result;
		if (frame == null) {
			return null;
		}
		if (class143.status == 2) {
			Class281.method3331(false, frame, class88);
			return null;
		}
		return frame;
	}

	public int	anInt1953;

	public int	anInt1955;

	public int	anInt1956;

	int			anInt1958;

	public Class259() {
		/* empty */
	}
}

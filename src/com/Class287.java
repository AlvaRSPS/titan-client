
/* Class287 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.archive.Js5FileRequest;

import jaclib.memory.Buffer;
import jaggl.OpenGL;

public abstract class Class287 {
	public static IncomingOpcode	aClass58_2187	= new IncomingOpcode(111, 1);
	public static IncomingOpcode	aClass58_2194	= new IncomingOpcode(14, 2);
	public static int				anInt2186;
	public static int				anInt2190;
	public static int				anInt2196		= 0;
	public static int[]				anIntArray2195	= new int[2];
	public static int[]				regionTerrainId;

	public static final boolean method3386(int i, int i_0_, byte i_1_) {
		try {
			return !(!((0x70000 & i) != 0 | Node.method944(i, i_0_, (byte) 85)) && !Class76_Sub7.method763(i, i_0_, false));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rq.N(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	public static final void setClientPreferences(byte i) {
		client.preferences.setPreference((byte) -13, 1, client.preferences.aClass64_Sub3_4041);
		client.preferences.setPreference((byte) -13, 1, client.preferences.aClass64_Sub3_4076);
		client.preferences.setPreference((byte) -13, 1, client.preferences.removeRoofs);
		client.preferences.setPreference((byte) -13, 1, client.preferences.removeRoofsMode);
		client.preferences.setPreference((byte) -13, 1, client.preferences.groundDecoration);
		client.preferences.setPreference((byte) -13, 1, client.preferences.groundBlending);
		client.preferences.setPreference((byte) -13, 0, client.preferences.idleAnimations);
		client.preferences.setPreference((byte) -13, 0, client.preferences.flickeringEffects);
		client.preferences.setPreference((byte) -13, 0, client.preferences.characterShadows);
		client.preferences.setPreference((byte) -13, 0, client.preferences.sceneryShadows);
		client.preferences.setPreference((byte) -13, 0, client.preferences.textures);
		client.preferences.setPreference((byte) -13, 0, client.preferences.lightningDetail);
		client.preferences.setPreference((byte) -13, 0, client.preferences.waterDetail);
		client.preferences.setPreference((byte) -13, 0, client.preferences.fog);
		client.preferences.setPreference((byte) -13, 0, client.preferences.antiAliasing);
		client.preferences.setPreference((byte) -13, 0, client.preferences.multiSample);
		client.preferences.setPreference((byte) -13, 0, client.preferences.particles);
		client.preferences.setPreference((byte) -13, 0, client.preferences.buildArea);
		client.preferences.setPreference((byte) -13, 0, client.preferences.unknown3);
		Class151_Sub1.method2450((byte) 62);
		client.preferences.setPreference((byte) -13, 2, client.preferences.maxScreenSize);
		client.preferences.setPreference((byte) -13, 2, client.preferences.graphicsLevel);
		Js5FileRequest.method1593((byte) 111);
		WhirlpoolGenerator.method3980((byte) 122);
		Class33.aBoolean316 = true;
	}

	private boolean	aBoolean2192;

	OpenGlToolkit	aHa_Sub1_2185;

	private int		anInt2189;

	int				anInt2191;

	private int		anInt2193;

	Class287(OpenGlToolkit var_ha_Sub1, int i, Buffer buffer, int i_7_, boolean bool) {
		try {
			aHa_Sub1_2185 = var_ha_Sub1;
			anInt2189 = i;
			aBoolean2192 = bool;
			anInt2193 = i_7_;
			OpenGL.glGenBuffersARB(1, Class23.anIntArray222, 0);
			anInt2191 = Class23.anIntArray222[0];
			method3384(0);
			OpenGL.glBufferDataARBa(i, anInt2193, buffer.getAddress(), aBoolean2192 ? 35040 : 35044);
			aHa_Sub1_2185.anInt4336 += anInt2193;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rq.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + (buffer != null ? "{...}" : "null") + ',' + i_7_ + ',' + bool + ')');
		}
	}

	Class287(OpenGlToolkit var_ha_Sub1, int i, byte[] is, int i_6_, boolean bool) {
		try {
			anInt2189 = i;
			anInt2193 = i_6_;
			aHa_Sub1_2185 = var_ha_Sub1;
			aBoolean2192 = bool;
			OpenGL.glGenBuffersARB(1, Class23.anIntArray222, 0);
			anInt2191 = Class23.anIntArray222[0];
			method3384(0);
			OpenGL.glBufferDataARBub(i, anInt2193, is, 0, !aBoolean2192 ? 35044 : 35040);
			aHa_Sub1_2185.anInt4336 += anInt2193;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rq.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ',' + (is != null ? "{...}" : "null") + ',' + i_6_ + ',' + bool + ')');
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			aHa_Sub1_2185.method1879(anInt2193, (byte) 121, anInt2191);
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rq.finalize(" + ')');
		}
	}

	public abstract void method3384(int i);

	public final void method3389(int i, int i_5_, byte[] is) {
		try {
			method3384(i + i);
			if (i_5_ > anInt2193) {
				OpenGL.glBufferDataARBub(anInt2189, i_5_, is, 0, aBoolean2192 ? 35040 : 35044);
				aHa_Sub1_2185.anInt4336 += -anInt2193 + i_5_;
				anInt2193 = i_5_;
			} else {
				OpenGL.glBufferSubDataARBub(anInt2189, 0, i_5_, is, 0);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "rq.L(" + i + ',' + i_5_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}
}

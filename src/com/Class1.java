/* Class1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.StructsDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.input.impl.AwtMouseListener;
import com.jagex.game.toolkit.matrix.JavaMatrix;

public final class Class1 {
	public static boolean	aBoolean66;
	public static Class128	aClass128_64	= new Class128();
	public static int[]		anIntArray65	= new int[5];

	public static final boolean isBridge(int z, byte dummy, int x) {
		if ((x ^ 0xffffffff) > -1 || z < 0 || Class281.flags[1].length <= x || (z ^ 0xffffffff) <= (Class281.flags[1][x].length ^ 0xffffffff)) {
			return false;
		}
		return (0x2 & Class281.flags[1][x][z]) != 0;
	}

	public static final void loadSpriteIds(byte i, Js5 store) {
		RtInterfaceClip.hitmarksId = store.getGroupId((byte) -107, "hitmarks");
		Class140.hitbarDefaultId = store.getGroupId((byte) -106, "hitbar_default");
		Class65.timerBarDefaultId = store.getGroupId((byte) -71, "timerbar_default");
		ProceduralTextureSource.headiconsPkId = store.getGroupId((byte) -61, "headicons_pk");
		Class251.headiconPrayerId = store.getGroupId((byte) -92, "headicons_prayer");
		Class319.hintHeadiconsId = store.getGroupId((byte) -67, "hint_headicons");
		Class76_Sub2.hintMapmarkersId = store.getGroupId((byte) -68, "hint_mapmarkers");
		Class226.mapflagId = store.getGroupId((byte) -93, "mapflag");
		Class39.crossId = store.getGroupId((byte) -55, "cross");
		DummyOutputStream.mapdotsId = store.getGroupId((byte) -87, "mapdots");
		ClipMap.scrollbarId = store.getGroupId((byte) -67, "scrollbar");
		Class98_Sub31_Sub4.nameiconsId = store.getGroupId((byte) -68, "name_icons");
		StartupStage.floorshadowsId = store.getGroupId((byte) -94, "floorshadows");
		JavaMatrix.compassId = store.getGroupId((byte) -123, "compass");
		StructsDefinitionParser.otherlevelId = store.getGroupId((byte) -50, "otherlevel");
		Class76.hintMapedgeId = store.getGroupId((byte) -101, "hint_mapedge");
	}

	public static final int method160(int i, byte i_0_, int i_1_) {
		if ((i_1_ ^ 0xffffffff) == -2 || (i_1_ ^ 0xffffffff) == -4) {
			return Class98_Sub10_Sub6.anIntArray5559[i & 0x3];
		}
		return AwtMouseListener.anIntArray5301[0x3 & i];
	}

	public static final void method619(Class1 class1) {
		if (Class226.anInt1705 < 65535) {
			PointLight class98_sub5 = class1.light;
			Class98_Sub10_Sub31.aClass1Array5717[Class226.anInt1705] = class1;
			Class21_Sub4.aBooleanArray5399[Class226.anInt1705] = false;
			Class226.anInt1705++;
			int i = class1.anInt57;
			if (class1.aBoolean54) {
				i = 0;
			}
			int i_14_ = class1.anInt57;
			if (class1.aBoolean58) {
				i_14_ = OpenGLTexture2DSource.anInt3103 - 1;
			}
			for (int i_15_ = i; i_15_ <= i_14_; i_15_++) {
				int i_16_ = 0;
				int i_17_ = class98_sub5.getZ(28699) - class98_sub5.getRange(-96) + Js5.anInt1577 >> Class151_Sub8.tileScale;
				if (i_17_ < 0) {
					i_16_ -= i_17_;
					i_17_ = 0;
				}
				int i_18_ = class98_sub5.getZ(28699) + class98_sub5.getRange(127) - Js5.anInt1577 >> Class151_Sub8.tileScale;
				if (i_18_ >= Class64_Sub9.anInt3662) {
					i_18_ = Class64_Sub9.anInt3662 - 1;
				}
				for (int i_19_ = i_17_; i_19_ <= i_18_; i_19_++) {
					int i_20_ = class1.aShortArray59[i_16_++];
					int i_21_ = (class98_sub5.getX(7019) - class98_sub5.getRange(126) + Js5.anInt1577 >> Class151_Sub8.tileScale) + (i_20_ >>> 8);
					int i_22_ = i_21_ + (i_20_ & 0xff) - 1;
					if (i_21_ < 0) {
						i_21_ = 0;
					}
					if (i_22_ >= BConfigDefinition.anInt3112) {
						i_22_ = BConfigDefinition.anInt3112 - 1;
					}
					for (int i_23_ = i_21_; i_23_ <= i_22_; i_23_++) {
						long l = SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[i_15_][i_23_][i_19_];
						if ((l & 0xffffL) == 0L) {
							SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[i_15_][i_23_][i_19_] = l | Class226.anInt1705;
						} else if ((l & 0xffff0000L) == 0L) {
							SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[i_15_][i_23_][i_19_] = l | (long) Class226.anInt1705 << 16;
						} else if ((l & 0xffff00000000L) == 0L) {
							SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[i_15_][i_23_][i_19_] = l | (long) Class226.anInt1705 << 32;
						} else if ((l & ~0xffffffffffffL) == 0L) {
							SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[i_15_][i_23_][i_19_] = l | (long) Class226.anInt1705 << 48;
						}
					}
				}
			}
		}
	}

	public boolean		aBoolean54;
	public boolean		aBoolean58;
	public PointLight	light;
	private int			anInt52;
	private int			anInt53;

	private int			anInt56;

	public int			anInt57;

	private int			anInt60;

	private int			anInt61;

	int					anInt62;

	public short[]		aShortArray59;

	protected Class1() {
		if (Class287_Sub1.anIntArray3421 == null) {
			Class358.method3887(110);
		}
		method164(-48);
	}

	Class1(RSToolkit toolkit, RSByteBuffer buffer, int scale) {
		if (Class287_Sub1.anIntArray3421 == null) {
			Class358.method3887(110);
		}
		anInt57 = buffer.readUnsignedByte((byte) -105);
		aBoolean54 = (0x10 & anInt57 ^ 0xffffffff) != -1;
		aBoolean58 = (anInt57 & 0x8 ^ 0xffffffff) != -1;
		anInt57 = anInt57 & 0x7;
		int x = buffer.readShort((byte) 127) << scale;
		int z = buffer.readShort((byte) 127) << scale;
		int y = buffer.readShort((byte) 127) << scale;
		int range = buffer.readUnsignedByte((byte) 26);
		int i_22_ = 1 + 2 * range;
		aShortArray59 = new short[i_22_];
		for (int index = 0; (index ^ 0xffffffff) > (aShortArray59.length ^ 0xffffffff); index++) {
			int i_24_ = (short) buffer.readShort((byte) 127);
			int i_25_ = i_24_ >>> -502532568;
			int i_26_ = i_24_ & 0xff;
			if ((i_25_ ^ 0xffffffff) <= (i_22_ ^ 0xffffffff)) {
				i_25_ = i_22_ - 1;
			}
			if (i_26_ > -i_25_ + i_22_) {
				i_26_ = -i_25_ + i_22_;
			}
			aShortArray59[index] = (short) Class41.or(i_25_ << 1030619432, i_26_);
		}
		range = (range << Class151_Sub8.tileScale) + Js5.anInt1577;
		int hsvColour = Class208.HSL_TABLE != null ? Class208.HSL_TABLE[buffer.readShort((byte) 127)] : Class221.HSV_TABLE[HslUtils.hslToHsv(buffer.readShort((byte) 127), 61) & 0xffff];
		int attributes = buffer.readUnsignedByte((byte) -110);
		anInt62 = 0x1f & attributes;
		anInt61 = 0x700 & attributes << -997312285;
		if (anInt62 != 31) {
			method164(-31);
		}
		setLight((byte) -56, toolkit, z, hsvColour, x, range, y);
	}

	public final void method161(boolean roofsRemoved, int i, int i_2_) {
		int i_3_;
		while_3_: do {
			if (roofsRemoved) {
				i_3_ = 2048;
			} else {
				int i_4_ = 0x7ff & anInt61 + i * anInt60 / 50;
				int i_5_ = anInt53;
				while_2_: do {
					while_1_: do {
						while_0_: do {
							do {
								if (i_5_ != 1) {
									if ((i_5_ ^ 0xffffffff) != -4) {
										if ((i_5_ ^ 0xffffffff) != -5) {
											if ((i_5_ ^ 0xffffffff) != -3) {
												if (i_5_ == 5) {
													break while_1_;
												}
												break while_2_;
											}
										} else {
											break;
										}
										break while_0_;
									}
								} else {
									i_3_ = (Class284_Sub2_Sub2.SINE[i_4_ << -516328477] >> -967825788) + 1024;
									break while_3_;
								}
								i_3_ = Class287_Sub1.anIntArray3421[i_4_] >> 1875641633;
								break while_3_;
							} while (false);
							i_3_ = i_4_ >> -322029302 << 2017264779;
							break while_3_;
						} while (false);
						i_3_ = i_4_;
						break while_3_;
					} while (false);
					i_3_ = (i_4_ < 1024 ? i_4_ : -i_4_ + 2048) << 1254570465;
					break while_3_;
				} while (false);
				i_3_ = 2048;
			}
		} while (false);
		light.setIntensity((anInt56 + (i_3_ * anInt52 >> 1801072939)) / 2048.0F, 57);
	}

	private final void method164(int i) {
		do {
			int i_8_ = anInt62;
			while_17_: do {
				while_16_: do {
					while_15_: do {
						while_14_: do {
							while_13_: do {
								while_12_: do {
									while_11_: do {
										while_10_: do {
											while_9_: do {
												while_8_: do {
													while_7_: do {
														while_6_: do {
															while_5_: do {
																while_4_: do {
																	do {
																		if (i_8_ != 2) {
																			if (i_8_ != 3) {
																				if (i_8_ != 4) {
																					if ((i_8_ ^ 0xffffffff) != -6) {
																						if (i_8_ != 12) {
																							if (i_8_ != 13) {
																								if (i_8_ != 10) {
																									if (i_8_ != 11) {
																										if ((i_8_ ^ 0xffffffff) != -7) {
																											if (i_8_ != 7) {
																												if ((i_8_ ^ 0xffffffff) != -9) {
																													if (i_8_ != 9) {
																														if ((i_8_ ^ 0xffffffff) != -15) {
																															if ((i_8_ ^ 0xffffffff) != -16) {
																																if ((i_8_ ^ 0xffffffff) == -17) {
																																	break while_15_;
																																}
																																break while_16_;
																															}
																														} else {
																															break while_13_;
																														}
																														break while_14_;
																													}
																												} else {
																													break while_11_;
																												}
																												break while_12_;
																											}
																										} else {
																											break while_9_;
																										}
																										break while_10_;
																									}
																								} else {
																									break while_7_;
																								}
																								break while_8_;
																							}
																						} else {
																							break while_5_;
																						}
																						break while_6_;
																					}
																				} else {
																					break;
																				}
																				break while_4_;
																			}
																		} else {
																			anInt52 = 2048;
																			anInt60 = 2048;
																			anInt53 = 1;
																			anInt56 = 0;
																			break while_17_;
																		}
																		anInt56 = 0;
																		anInt52 = 2048;
																		anInt53 = 1;
																		anInt60 = 4096;
																		break while_17_;
																	} while (false);
																	anInt52 = 2048;
																	anInt53 = 4;
																	anInt60 = 2048;
																	anInt56 = 0;
																	break while_17_;
																} while (false);
																anInt56 = 0;
																anInt53 = 4;
																anInt52 = 2048;
																anInt60 = 8192;
																break while_17_;
															} while (false);
															anInt53 = 2;
															anInt56 = 0;
															anInt60 = 2048;
															anInt52 = 2048;
															break while_17_;
														} while (false);
														anInt53 = 2;
														anInt56 = 0;
														anInt52 = 2048;
														anInt60 = 8192;
														break while_17_;
													} while (false);
													anInt52 = 512;
													anInt56 = 1536;
													anInt53 = 3;
													anInt60 = 2048;
													break while_17_;
												} while (false);
												anInt52 = 512;
												anInt53 = 3;
												anInt56 = 1536;
												anInt60 = 4096;
												break while_17_;
											} while (false);
											anInt52 = 768;
											anInt56 = 1280;
											anInt53 = 3;
											anInt60 = 2048;
											break while_17_;
										} while (false);
										anInt56 = 1280;
										anInt53 = 3;
										anInt60 = 4096;
										anInt52 = 768;
										break while_17_;
									} while (false);
									anInt52 = 1024;
									anInt53 = 3;
									anInt56 = 1024;
									anInt60 = 2048;
									break while_17_;
								} while (false);
								anInt60 = 4096;
								anInt53 = 3;
								anInt56 = 1024;
								anInt52 = 1024;
								break while_17_;
							} while (false);
							anInt56 = 1280;
							anInt53 = 1;
							anInt60 = 2048;
							anInt52 = 768;
							break while_17_;
						} while (false);
						anInt53 = 1;
						anInt60 = 4096;
						anInt56 = 1536;
						anInt52 = 512;
						break while_17_;
					} while (false);
					anInt53 = 1;
					anInt52 = 256;
					anInt60 = 8192;
					anInt56 = 1792;
					break while_17_;
				} while (false);
				anInt52 = 2048;
				anInt53 = 0;
				anInt56 = 0;
				anInt60 = 2048;
			} while (false);

			break;
		} while (false);
	}

	public final void method166(int i, int i_9_, int i_10_, byte i_11_, int i_12_) {
		anInt53 = i_12_;
		if (i_11_ <= -81) {
			anInt56 = i_10_;
			anInt52 = i_9_;
			anInt60 = i;
		}
	}

	private final void setLight(byte dummy, RSToolkit toolkit, int z, int hsvColour, int x, int range, int y) {
		light = toolkit.createPointLight(x, y, z, range, hsvColour, 1.0F);
	}

}

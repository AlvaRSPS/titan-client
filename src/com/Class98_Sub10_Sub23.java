/* Class98_Sub10_Sub23 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.MapScenesDefinitionParser;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;

public final class Class98_Sub10_Sub23 extends Class98_Sub10 {
	public static int						anInt5659;
	public static int						anInt5661	= 0;
	public static MapScenesDefinitionParser	mapScenesDefinitionList;

	public static void method1073(int i) {
		do {
			try {
				mapScenesDefinitionList = null;
				if (i == 4096) {
					break;
				}
				method1073(45);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "nu.B(" + i + ')');
			}
			break;
		} while (false);
	}

	private int		anInt5657;
	private int		anInt5663	= 0;
	private int		anInt5664	= 10;
	private int[]	anIntArray5658;

	private int[]	anIntArray5660;

	public Class98_Sub10_Sub23() {
		super(0, true);
		anInt5657 = 2048;
	}

	@Override
	public final void method1001(byte i) {
		do {
			try {
				method1074((byte) -6);
				if (i == 66) {
					break;
				}
				mapScenesDefinitionList = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "nu.I(" + i + ')');
			}
			break;
		} while (false);
	}

	private final void method1074(byte i) {
		try {
			anIntArray5658 = new int[1 + anInt5664];
			anIntArray5660 = new int[1 + anInt5664];
			int i_2_ = 0;
			int i_3_ = 4096 / anInt5664;
			int i_4_ = i_3_ * anInt5657 >> -946071060;
			int i_5_ = 0;
			if (i != -6) {
				anInt5657 = -68;
			}
			for (/**/; i_5_ < anInt5664; i_5_++) {
				anIntArray5660[i_5_] = i_2_;
				anIntArray5658[i_5_] = i_4_ + i_2_;
				i_2_ += i_3_;
			}
			anIntArray5660[anInt5664] = 4096;
			anIntArray5658[anInt5664] = anIntArray5658[0] + 4096;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nu.D(" + i + ')');
		}
	}

	@Override
	public final int[] method990(int i, int i_6_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_6_);
			if (this.aClass16_3863.aBoolean198) {
				int i_7_ = GameObjectDefinition.anIntArray3001[i_6_];
				if ((anInt5663 ^ 0xffffffff) == -1) {
					int i_8_ = 0;
					for (int i_9_ = 0; anInt5664 > i_9_; i_9_++) {
						if ((i_7_ ^ 0xffffffff) <= (anIntArray5660[i_9_] ^ 0xffffffff) && (anIntArray5660[1 + i_9_] ^ 0xffffffff) < (i_7_ ^ 0xffffffff)) {
							if ((anIntArray5658[i_9_] ^ 0xffffffff) < (i_7_ ^ 0xffffffff)) {
								i_8_ = 4096;
							}
							break;
						}
					}
					ArrayUtils.method2896(is, 0, Class25.anInt268, i_8_);
				} else {
					for (int i_10_ = 0; i_10_ < Class25.anInt268; i_10_++) {
						int i_11_ = 0;
						int i_12_ = 0;
						int i_13_ = MonoOrStereoPreferenceField.anIntArray3640[i_10_];
						int i_14_ = anInt5663;
						while_133_: do {
							do {
								if (i_14_ != 1) {
									if ((i_14_ ^ 0xffffffff) != -3) {
										if ((i_14_ ^ 0xffffffff) == -4) {
											break;
										}
										break while_133_;
									}
								} else {
									i_11_ = i_13_;
									break while_133_;
								}
								i_11_ = 2048 + (i_13_ + -4096 + i_7_ >> -752554751);
								break while_133_;
							} while (false);
							i_11_ = (i_13_ + -i_7_ >> 2077425665) + 2048;
						} while (false);
						for (i_14_ = 0; (anInt5664 ^ 0xffffffff) < (i_14_ ^ 0xffffffff); i_14_++) {
							if ((anIntArray5660[i_14_] ^ 0xffffffff) >= (i_11_ ^ 0xffffffff) && (anIntArray5660[i_14_ - -1] ^ 0xffffffff) < (i_11_ ^ 0xffffffff)) {
								if ((anIntArray5658[i_14_] ^ 0xffffffff) < (i_11_ ^ 0xffffffff)) {
									i_12_ = 4096;
								}
								break;
							}
						}
						is[i_10_] = i_12_;
					}
				}
			}
			if (i != 255) {
				anInt5661 = -81;
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "nu.G(" + i + ',' + i_6_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_0_) {
		do {
			try {
				int i_1_ = i;
				while_132_: do {
					do {
						if (i_1_ != 0) {
							if (i_1_ != 1) {
								if ((i_1_ ^ 0xffffffff) == -3) {
									break;
								}
								break while_132_;
							}
						} else {
							anInt5664 = class98_sub22.readUnsignedByte((byte) 4);
							break while_132_;
						}
						anInt5657 = class98_sub22.readShort((byte) 127);
						break while_132_;
					} while (false);
					anInt5663 = class98_sub22.readUnsignedByte((byte) -126);
				} while (false);
				if (i_0_ < -92) {
					break;
				}
				method1001((byte) 102);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "nu.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_0_ + ')');
			}
			break;
		} while (false);
	}
}

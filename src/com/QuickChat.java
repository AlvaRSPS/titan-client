
/* Class300 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.quickchat.QuickChatMessage;

public final class QuickChat {
	public static Sprite	aClass332_2500;
	public static Class65	aClass65_2499	= new Class65();

	public static final Class119_Sub4 method3533(RSByteBuffer packet, byte i) {
		return new Class119_Sub4(packet.readUShort(false), packet.readUShort(false), packet.readUShort(false), packet.readUShort(false), packet.readMediumInt(-127), packet.readMediumInt(-128), packet.readUnsignedByte((byte) -114));
	}

	int[]				messages;

	QuickChatMessage	message;

	int					messageId;

	public QuickChat() {
		/* empty */
	}
}

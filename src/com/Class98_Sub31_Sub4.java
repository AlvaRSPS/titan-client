/* Class98_Sub31_Sub4 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.preferences.RemoveRoofsPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.ScalingSpriteLSEConfig;

public final class Class98_Sub31_Sub4 extends Class98_Sub31 {
	public static int[]	underlayFaces	= { 0, 1, 2, 2, 1, 1, 2, 3, 1, 3, 3, 4, 2, 0, 4 };
	public static int	nameiconsId;

	public static final void method1383(Player player, GameObjectDefinition class352, int i, int i_0_, int i_1_, int i_2_, int i_3_, NPC npc) {
		Class98_Sub42 class98_sub42 = new Class98_Sub42();
		class98_sub42.anInt4220 = i_3_;
		class98_sub42.anInt4225 = i_2_ << 988015593;
		class98_sub42.anInt4229 = i << -1906491031;
		if (class352 == null) {
			if (npc != null) {
				class98_sub42.npc = npc;
				NPCDefinition definition = npc.definition;
				if (definition.anIntArray1109 != null) {
					class98_sub42.aBoolean4207 = true;
					definition = definition.method2300(StartupStage.varValues, (byte) 95);
				}
				if (definition != null) {
					class98_sub42.anInt4224 = i - -definition.boundSize << -602009655;
					class98_sub42.anInt4216 = i_2_ - -definition.boundSize << 1128804969;
					class98_sub42.anInt4210 = Class277.method3293(i_1_ + 119, npc);
					class98_sub42.anInt4236 = definition.anInt1156;
					class98_sub42.aBoolean4215 = definition.aBoolean1093;
					class98_sub42.anInt4228 = definition.anInt1128 << 46162057;
					class98_sub42.anInt4223 = definition.anInt1101;
					class98_sub42.anInt4217 = definition.anInt1125 << -1887902231;
					class98_sub42.anInt4237 = definition.anInt1090;
				}
				Class358.aClass148_3032.addLast(class98_sub42, -20911);
			} else if (player != null) {
				class98_sub42.player = player;
				class98_sub42.anInt4224 = i - -player.getSize(i_1_ ^ 0x3) << 255186825;
				class98_sub42.anInt4216 = i_2_ + player.getSize(0) << -58758775;
				class98_sub42.anInt4210 = VertexNormal.method3383(player, true);
				class98_sub42.anInt4237 = 256;
				class98_sub42.anInt4228 = player.anInt6525 << -78927831;
				class98_sub42.anInt4217 = 0;
				class98_sub42.aBoolean4215 = player.hasDisplayName;
				class98_sub42.anInt4223 = 256;
				class98_sub42.anInt4236 = player.anInt6514;
				Class98_Sub10_Sub14.aClass377_5612.put(class98_sub42, player.index, -1);
			}
		} else {
			class98_sub42.aClass352_4233 = class352;
			int i_4_ = class352.sizeY;
			int i_5_ = class352.sizeX;
			if (i_0_ == 1 || (i_0_ ^ 0xffffffff) == -4) {
				i_5_ = class352.sizeY;
				i_4_ = class352.sizeX;
			}
			class98_sub42.anInt4205 = class352.anInt2972;
			class98_sub42.aBoolean4226 = class352.aBoolean2957;
			class98_sub42.anInt4219 = class352.anInt2949;
			class98_sub42.aBoolean4215 = class352.aBoolean2992;
			class98_sub42.anInt4217 = class352.anInt2970 << 1467122217;
			class98_sub42.anInt4216 = i_5_ + i_2_ << 636122729;
			class98_sub42.anInt4236 = class352.anInt2987;
			class98_sub42.anInt4224 = i_4_ + i << 531390185;
			class98_sub42.anInt4237 = class352.anInt2950;
			class98_sub42.anIntArray4208 = class352.anIntArray2926;
			class98_sub42.anInt4228 = class352.anInt2981 << 998368073;
			class98_sub42.anInt4223 = class352.anInt3006;
			class98_sub42.anInt4210 = class352.anInt2996;
			if (class352.transformIDs != null) {
				class98_sub42.aBoolean4207 = true;
				class98_sub42.method1478(true);
			}
			if (class98_sub42.anIntArray4208 != null) {
				class98_sub42.anInt4221 = (int) ((-class98_sub42.anInt4219 + class98_sub42.anInt4205) * Math.random()) + class98_sub42.anInt4219;
			}
			Class98_Sub10_Sub37.aClass148_5748.addLast(class98_sub42, -20911);
		}
	}

	public static final Class98_Sub46_Sub17 method1384(int i) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = (Class98_Sub46_Sub17) ScalingSpriteLSEConfig.aClass215_3545.getFirst(-1);
		if (class98_sub46_sub17 != null) {
			class98_sub46_sub17.unlink(45);
			class98_sub46_sub17.uncache((byte) -90);
			return class98_sub46_sub17;
		}
		do {
			class98_sub46_sub17 = (Class98_Sub46_Sub17) Class98_Sub10_Sub34.aClass215_5728.getFirst(-1);
			if (class98_sub46_sub17 == null) {
				return null;
			}
			if ((class98_sub46_sub17.method1620((byte) -108) ^ 0xffffffffffffffffL) < (TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL)) {
				return null;
			}
			class98_sub46_sub17.unlink(61);
			class98_sub46_sub17.uncache((byte) -90);
		} while ((~0x7fffffffffffffffL & class98_sub46_sub17.cachedKey ^ 0xffffffffffffffffL) == -1L);
		return class98_sub46_sub17;
	}

	public static final void initializeTrig(int dummy) {
		if (aa_Sub2.SINE == null || Class278_Sub1.COSINE == null) {
			Class278_Sub1.COSINE = new int[256];
			aa_Sub2.SINE = new int[256];
			for (int in = 0; in < 256; in++) {
				double d = 6.283185307179586 * (in / 255.0);
				aa_Sub2.SINE[in] = (int) (4096.0 * Math.sin(d));
				Class278_Sub1.COSINE[in] = (int) (4096.0 * Math.cos(d));
			}
		}
	}

	public static final int method1390(int i, int i_15_, int i_16_, int i_17_) {
		try {
			if (i_17_ != -8941) {
				return -107;
			}
			if ((0x8 & Class281.flags[i_15_][i_16_][i] ^ 0xffffffff) != -1) {
				return 0;
			}
			if ((i_15_ ^ 0xffffffff) < -1 && (Class281.flags[1][i_16_][i] & 0x2 ^ 0xffffffff) != -1) {
				return i_15_ - 1;
			}
			return i_15_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.I(" + i + ',' + i_15_ + ',' + i_16_ + ',' + i_17_ + ')');
		}
	}

	private boolean		aBoolean5864;
	private boolean		aBoolean5867;
	private LinkedList	aClass148_5859	= new LinkedList();

	private int			anInt5858;

	private int			anInt5862;

	private int			anInt5863;

	private int			anInt5865		= 256;

	private int			anInt5866;

	Class98_Sub31_Sub4(int i) {
		anInt5863 = 0;
		anInt5866 = 256;
		try {
			anInt5858 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.<init>(" + i + ')');
		}
	}

	@Override
	public final synchronized void method1321(int i) {
		try {
			if (!aBoolean5864) {
				for (;;) {
					Class98_Sub46_Sub15 class98_sub46_sub15 = method1381(-95);
					if (class98_sub46_sub15 == null) {
						if (aBoolean5867) {
							unlink(59);
							Class98_Sub49.aClass100_4283.method1690(1);
						}
						break;
					}
					if (i < -anInt5862 + class98_sub46_sub15.aShortArrayArray6040[0].length) {
						anInt5862 += i;
						break;
					}
					i -= -anInt5862 + class98_sub46_sub15.aShortArrayArray6040[0].length;
					method1388((byte) 100);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.M(" + i + ')');
		}
	}

	@Override
	public final Class98_Sub31 method1322() {
		try {
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.B(" + ')');
		}
	}

	@Override
	public final synchronized void method1325(int[] is, int i, int i_10_) {
		try {
			if (!aBoolean5864) {
				if (method1381(-67) == null) {
					if (aBoolean5867) {
						unlink(111);
						Class98_Sub49.aClass100_4283.method1690(1);
					}
				} else {
					int i_11_ = i_10_ + i;
					if (Class151_Sub7.aBoolean5007) {
						i_11_ <<= 1;
					}
					int i_12_ = 0;
					int i_13_ = 0;
					if (anInt5858 == 2) {
						i_13_ = 1;
					}
					while (i_11_ > i) {
						Class98_Sub46_Sub15 class98_sub46_sub15 = method1381(-54);
						if (class98_sub46_sub15 == null) {
							break;
						}
						short[][] is_14_;
						for (is_14_ = class98_sub46_sub15.aShortArrayArray6040; (i ^ 0xffffffff) > (i_11_ ^ 0xffffffff) && (is_14_[0].length ^ 0xffffffff) < (anInt5862 ^ 0xffffffff); anInt5862++) {
							if (!Class151_Sub7.aBoolean5007) {
								is[i++] += anInt5865 * is_14_[i_13_][anInt5862] + anInt5866 * is_14_[i_12_][anInt5862];
							} else {
								is[i++] = anInt5866 * is_14_[i_12_][anInt5862];
								is[i++] = anInt5865 * is_14_[i_13_][anInt5862];
							}
						}
						if (anInt5862 >= is_14_[0].length) {
							method1388((byte) 100);
						}
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.A(" + (is != null ? "{...}" : "null") + ',' + i + ',' + i_10_ + ')');
		}
	}

	@Override
	public final int method1326() {
		try {
			return 1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.G(" + ')');
		}
	}

	@Override
	public final Class98_Sub31 method1327() {
		try {
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.D(" + ')');
		}
	}

	public final synchronized void method1379(int i) {
		try {
			if (i != 0) {
				aBoolean5867 = false;
			}
			aBoolean5867 = true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.R(" + i + ')');
		}
	}

	public final synchronized void method1380(byte i, Class98_Sub46_Sub15 class98_sub46_sub15) {
		try {
			for (/**/; anInt5863 >= 100; anInt5863--) {
				aClass148_5859.removeFirst(6494);
			}
			if (i <= 10) {
				method1383(null, null, -103, 19, -21, 97, 127, null);
			}
			aClass148_5859.addLast(class98_sub46_sub15, -20911);
			anInt5863++;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.P(" + i + ',' + (class98_sub46_sub15 != null ? "{...}" : "null") + ')');
		}
	}

	private final synchronized Class98_Sub46_Sub15 method1381(int i) {
		try {
			if (i > -26) {
				nameiconsId = 119;
			}
			return (Class98_Sub46_Sub15) aClass148_5859.getFirst(32);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.N(" + i + ')');
		}
	}

	public final synchronized double method1382(boolean bool) {
		try {
			if ((anInt5863 ^ 0xffffffff) > -2) {
				return -1.0;
			}
			if (bool != false) {
				aBoolean5864 = true;
			}
			Class98_Sub46_Sub15 class98_sub46_sub15 = (Class98_Sub46_Sub15) aClass148_5859.getFirst(32);
			if (class98_sub46_sub15 == null) {
				return -1.0;
			}
			return class98_sub46_sub15.aDouble6042 - (float) class98_sub46_sub15.aShortArrayArray6040[0].length / (float) RemoveRoofsPreferenceField.anInt3678;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.S(" + bool + ')');
		}
	}

	public final Class98_Sub46_Sub15 method1385(double d, int i, int i_7_) {
		try {
			long l = anInt5858 << 336243488 | i;
			Class98_Sub46_Sub15 class98_sub46_sub15 = (Class98_Sub46_Sub15) Class98_Sub49.aClass100_4283.method1694((byte) 120, l);
			do {
				if (class98_sub46_sub15 == null) {
					class98_sub46_sub15 = new Class98_Sub46_Sub15(new short[anInt5858][i], d);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				class98_sub46_sub15.aDouble6042 = d;
				Class98_Sub49.aClass100_4283.method1689(l, (byte) 58);
			} while (false);
			return class98_sub46_sub15;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.E(" + d + ',' + i + ',' + i_7_ + ')');
		}
	}

	public final synchronized int method1387(boolean bool) {
		try {
			if (bool != true) {
				method1388((byte) 32);
			}
			return anInt5863;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.C(" + bool + ')');
		}
	}

	private final synchronized void method1388(byte i) {
		try {
			if (i != 100) {
				method1385(1.413056312553466, -73, -59);
			}
			Class98_Sub46_Sub15 class98_sub46_sub15 = method1381(-92);
			if (class98_sub46_sub15 != null) {
				class98_sub46_sub15.unlink(i ^ 0x56);
				anInt5862 = 0;
				anInt5863--;
				Class98_Sub49.aClass100_4283.method1695(26404, class98_sub46_sub15, class98_sub46_sub15.method1608(2));
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.F(" + i + ')');
		}
	}

	public final synchronized void method1391(boolean bool, int i) {
		try {
			if (i == -58758775) {
				aBoolean5864 = bool;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wb.L(" + bool + ',' + i + ')');
		}
	}

	public final void method1392(int i, int i_18_) {
		do {
			try {
				anInt5866 = i_18_;
				anInt5865 = i_18_;
				if (i == 255186825) {
					break;
				}
				method1326();
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wb.O(" + i + ',' + i_18_ + ')');
			}
			break;
		} while (false);
	}
}

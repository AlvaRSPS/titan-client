
/* Class98_Sub30 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.File;
import java.lang.reflect.Method;

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.impl.TextLoadingScreenElement;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.ground.NativeGround;
import com.jagex.game.toolkit.matrix.NativeMatrix;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeapBuffer;

public final class Class98_Sub30 extends Node {
	public static Class155[]		aClass155Array4099;
	public static Class164			aClass164_4088	= new Class164(1);
	/* synthetic */ static Class	aClass4100;
	public static IncomingOpcode	aClass58_4094;
	public static float[]			aFloatArray4096	= new float[16];

	public static final void method1311(boolean bool, boolean bool_0_, Player player) {
		do {
			if ((Class359.actionCount ^ 0xffffffff) > -401) {
				if (Class87.localPlayer == player) {
					if (Class98_Sub10_Sub9.aBoolean5585 && (0x10 & Class98_Sub4.anInt3826) != 0) {
						ActionQueueEntry.addAction(false, true, 0L, Class336.anInt2823, 0, Class246_Sub3_Sub3.applyMenuText + " -> <col=ffffff>" + TextResources.SELF.getText(client.gameLanguage, (byte) 25), false, 0, 57, player.index, -1, false, Class287_Sub2.aString3272);
					}
				} else if (bool_0_ == true) {
					String string;
					if (player.skillRating == 0) {
						boolean bool_1_ = true;
						if ((Class87.localPlayer.combatRange ^ 0xffffffff) != 0 && (player.combatRange ^ 0xffffffff) != 0) {
							int i = Class87.localPlayer.combatRange >= player.combatRange ? player.combatRange : Class87.localPlayer.combatRange;
							int i_2_ = Class87.localPlayer.combatLevel - player.combatLevel;
							if ((i_2_ ^ 0xffffffff) > -1) {
								i_2_ = -i_2_;
							}
							if (i_2_ > i) {
								bool_1_ = false;
							}
						}
						String string_3_ = client.game != GameDefinition.STELLAR_DAWN ? TextResources.LEVEL.getText(client.gameLanguage, (byte) 25) : TextResources.RATING.getText(client.gameLanguage, (byte) 25);
						if ((player.combatLevel ^ 0xffffffff) > (player.boosted ^ 0xffffffff)) {
							string = player.formattedName(0, true) + (bool_1_ ? Class108.getColour(Class87.localPlayer.combatLevel, player.combatLevel, 16383) : "<col=ffffff>") + " (" + string_3_ + player.combatLevel + "+" + (player.boosted + -player.combatLevel) + ")";
						} else {
							string = player.formattedName(0, true) + (!bool_1_ ? "<col=ffffff>" : Class108.getColour(Class87.localPlayer.combatLevel, player.combatLevel, 16383)) + " (" + string_3_ + player.combatLevel + ")";
						}
					} else if (player.skillRating != -1) {
						string = player.formattedName(0, true) + " (" + TextResources.SKILL.getText(client.gameLanguage, (byte) 25) + player.skillRating + ")";
					} else {
						string = player.formattedName(0, true);
					}
					if (Class98_Sub10_Sub9.aBoolean5585 && !bool && (Class98_Sub4.anInt3826 & 0x8) != 0) {
						ActionQueueEntry.addAction(false, true, player.index, Class336.anInt2823, 0, Class246_Sub3_Sub3.applyMenuText + " -> <col=ffffff>" + string, false, 0, 5, player.index, -1, false, Class287_Sub2.aString3272);
					}
					if (!bool) {
						for (int option = 7; (option ^ 0xffffffff) <= -1; option--) {
							if (LightIntensityDefinitionParser.playerOptions[option] != null) {
								short i_4_ = 0;
								if (GameDefinition.RUNESCAPE == client.game && LightIntensityDefinitionParser.playerOptions[option].equalsIgnoreCase(TextResources.ATTACK.getText(client.gameLanguage, (byte) 25))) {
									if ((Class87.localPlayer.combatLevel ^ 0xffffffff) > (player.combatLevel ^ 0xffffffff)) {
										i_4_ = (short) 2000;
									}
									if (Class87.localPlayer.team != 0 && (player.team ^ 0xffffffff) != -1) {
										if ((Class87.localPlayer.team ^ 0xffffffff) == (player.team ^ 0xffffffff)) {
											i_4_ = (short) 2000;
										} else {
											i_4_ = (short) 0;
										}
									}
								} else if (OpenGlModelRenderer.playerOptionReducedPriority[option]) {
									i_4_ = (short) 2000;
								}
								short i_5_ = (short) (i_4_ + TextLoadingScreenElement.aShortArray3447[option]);
								int i_6_ = Class151_Sub9.playerOptionCursors[option] != -1 ? Class151_Sub9.playerOptionCursors[option] : Class284_Sub2.anInt5186;
								ActionQueueEntry.addAction(false, true, player.index, i_6_, 0, "<col=ffffff>" + string, false, 0, i_5_, player.index, -1, false, LightIntensityDefinitionParser.playerOptions[option]);
							}
						}
					} else {
						ActionQueueEntry.addAction(false, false, 0L, -1, 0, "", false, 0, -1, player.index, 0, true, "<col=cccccc>" + string);
					}
					if (bool) {
						break;
					}
					for (ActionQueueEntry entry = (ActionQueueEntry) Class33.actionList.getFirst(32); entry != null; entry = (ActionQueueEntry) Class33.actionList.getNext(102)) {
						if ((entry.actionId ^ 0xffffffff) == -7) {
							entry.aString5985 = "<col=ffffff>" + string;
							break;
						}
					}
				}
			}
			break;
		} while (false);
	}

	public static final void method1319(int i, File file, boolean bool) {
		try {
			if (Class98_Sub10_Sub22.anObject5653 == null) {
				Class340.method3803(false);
			}
			try {
				Class var_class = Class.forName("sun.management.HotSpotDiagnosticMXBean");
				if (i == 0) {
					Method method = var_class.getDeclaredMethod("dumpHeap", aClass4100 != null ? aClass4100 : (aClass4100 = method1320("java.lang.String")), Boolean.TYPE);
					method.invoke(Class98_Sub10_Sub22.anObject5653, file.getAbsolutePath(), new Boolean(bool));
				}
			} catch (Exception exception) {
				System.out.println("HeapDump error:");
				exception.printStackTrace();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "li.E(" + i + ',' + (file != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	/* synthetic */ static Class method1320(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	public float				aFloat4092;
	private NativeToolkit		aHa_Sub3_4087;
	private NativeHeapBuffer	aNativeHeapBuffer4089;
	public int					anInt4084;
	public int					anInt4086;
	public int					anInt4090;
	public int					anInt4091;
	public int					anInt4098	= 0;

	private int[]				anIntArray4095;

	private Interface2_Impl1	anInterface2_Impl1_4085;

	private NativeGround		aS_Sub2_4097;

	private Stream				aStream4093;

	public Class98_Sub30(NativeGround var_s_Sub2, int i, int i_47_, int i_48_, int i_49_, int i_50_) {
		try {
			aS_Sub2_4097 = var_s_Sub2;
			anInt4091 = i_48_;
			anInt4084 = i;
			anInt4090 = i_49_;
			aHa_Sub3_4087 = aS_Sub2_4097.aHa_Sub3_5225;
			anIntArray4095 = new int[aS_Sub2_4097.length * aS_Sub2_4097.width];
			anInt4086 = i_50_;
			aFloat4092 = i_47_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "li.<init>(" + (var_s_Sub2 != null ? "{...}" : "null") + ',' + i + ',' + i_47_ + ',' + i_48_ + ',' + i_49_ + ',' + i_50_ + ')');
		}
	}

	public final void method1312(int i, boolean bool) {
		try {
			aStream4093.setBackingOffset(3 + 4 * i);
			if (bool != true) {
				method1313(108, (byte) 47);
			}
			aStream4093.writeByte(-1);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "li.D(" + i + ',' + bool + ')');
		}
	}

	public final void method1313(int i, byte i_7_) {
		try {
			aStream4093.flush();
			anInterface2_Impl1_4085 = aHa_Sub3_4087.method2060(false, 68);
			anInterface2_Impl1_4085.method73((byte) -113, i * 4, 4, aNativeHeapBuffer4089);
			aNativeHeapBuffer4089 = null;
			if (i_7_ <= 18) {
				method1311(true, false, null);
			}
			aStream4093 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "li.C(" + i + ',' + i_7_ + ')');
		}
	}

	public final void method1314(int[] is, int i, int i_8_) {
		do {
			try {
				Interface2_Impl2 interface2_impl2 = aHa_Sub3_4087.method1963(62, anInt4098 * 3);
				Buffer buffer = interface2_impl2.method78(true, -112);
				if (buffer != null) {
					Stream stream = aHa_Sub3_4087.method2043(24022, buffer);
					int i_9_ = 0;
					if (i_8_ != 32736) {
						method1315(-55, 122, 111, -45);
					}
					int i_10_ = 32767;
					int i_11_ = -32768;
					if (!Stream.a()) {
						for (int i_12_ = 0; (i_12_ ^ 0xffffffff) > (i ^ 0xffffffff); i_12_++) {
							int i_13_ = is[i_12_];
							int i_14_ = anIntArray4095[i_13_];
							short[] is_15_ = aS_Sub2_4097.aShortArrayArray5230[i_13_];
							if ((i_14_ ^ 0xffffffff) != -1 && is_15_ != null) {
								int i_16_ = 0;
								int i_17_ = 0;
								while ((i_17_ ^ 0xffffffff) > (is_15_.length ^ 0xffffffff)) {
									if ((i_14_ & 1 << i_16_++) != 0) {
										i_9_++;
										for (int i_18_ = 0; (i_18_ ^ 0xffffffff) > -4; i_18_++) {
											int i_19_ = 0xffff & is_15_[i_17_++];
											if (i_19_ > i_11_) {
												i_11_ = i_19_;
											}
											stream.d(i_19_);
											if ((i_19_ ^ 0xffffffff) > (i_10_ ^ 0xffffffff)) {
												i_10_ = i_19_;
											}
										}
									} else {
										i_17_ += 3;
									}
								}
							}
						}
					} else {
						for (int i_20_ = 0; i_20_ < i; i_20_++) {
							int i_21_ = is[i_20_];
							int i_22_ = anIntArray4095[i_21_];
							short[] is_23_ = aS_Sub2_4097.aShortArrayArray5230[i_21_];
							if (i_22_ != 0 && is_23_ != null) {
								int i_24_ = 0;
								int i_25_ = 0;
								while (i_25_ < is_23_.length) {
									if ((i_22_ & 1 << i_24_++) != 0) {
										for (int i_26_ = 0; i_26_ < 3; i_26_++) {
											int i_27_ = 0xffff & is_23_[i_25_++];
											stream.c(i_27_);
											if (i_10_ > i_27_) {
												i_10_ = i_27_;
											}
											if (i_11_ < i_27_) {
												i_11_ = i_27_;
											}
										}
										i_9_++;
									} else {
										i_25_ += 3;
									}
								}
							}
						}
					}
					stream.flush();
					if (!interface2_impl2.method79((byte) 60) || i_9_ <= 0) {
						break;
					}
					aHa_Sub3_4087.method2039((0x8 & aS_Sub2_4097.anInt5233) != 0, i_8_ + -32736, anInt4084, (aS_Sub2_4097.anInt5233 & 0x7 ^ 0xffffffff) != -1);
					if (aHa_Sub3_4087.aBoolean4563) {
						aHa_Sub3_4087.EA(2147483647, anInt4091, anInt4090, anInt4086);
					}
					NativeMatrix class111_sub3 = aHa_Sub3_4087.method1957((byte) -128);
					class111_sub3.method2137(1.0F / aFloat4092, (byte) -122, 1.0F / aFloat4092, 1.0F);
					aHa_Sub3_4087.method2008(Class246_Sub3_Sub4_Sub5.aClass258_6260, (byte) 36);
					aHa_Sub3_4087.method1971(1, true, anInterface2_Impl1_4085);
					aHa_Sub3_4087.method2042(aS_Sub2_4097.aClass256_5252, (byte) 67);
					aHa_Sub3_4087.method1973(Class336.aClass232_2822, -i_10_ + i_11_ - -1, 0, 26810, interface2_impl2, i_10_, i_9_);
					aHa_Sub3_4087.method1985(2);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "li.F(" + (is != null ? "{...}" : "null") + ',' + i + ',' + i_8_ + ')');
			}
			break;
		} while (false);
	}

	public final void method1315(int i, int i_28_, int i_29_, int i_30_) {
		try {
			anIntArray4095[i_30_ * ((Ground) aS_Sub2_4097).width + i_29_] = Class41.or(anIntArray4095[i_30_ * aS_Sub2_4097.width + i_29_], 1 << i);
			if (i_28_ != -20787) {
				method1315(22, -33, -30, -8);
			}
			anInt4098++;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "li.B(" + i + ',' + i_28_ + ',' + i_29_ + ',' + i_30_ + ')');
		}
	}

	public final void method1316(int i, int i_31_) {
		try {
			aNativeHeapBuffer4089 = aHa_Sub3_4087.method1947(i * i_31_, true, 0);
			aStream4093 = new Stream(aNativeHeapBuffer4089, 0, 4 * i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "li.A(" + i + ',' + i_31_ + ')');
		}
	}

	public final void method1317(int i, int i_32_, int i_33_, int i_34_, float f) {
		try {
			if (anInt4084 != i_33_) {
				TextureMetrics class238 = aHa_Sub3_4087.metricsList.getInfo(anInt4084, -28755);
				int i_35_ = class238.aByte1830 & 0xff;
				if (i_35_ != 0 && (class238.aByte1820 ^ 0xffffffff) != -5) {
					int i_36_;
					if ((i_32_ ^ 0xffffffff) > -1) {
						i_36_ = 0;
					} else if (i_32_ > 127) {
						i_36_ = 16777215;
					} else {
						i_36_ = i_32_ * 131586;
					}
					if ((i_35_ ^ 0xffffffff) == -257) {
						i_34_ = i_36_;
					} else {
						int i_37_ = i_35_;
						int i_38_ = 256 + -i_35_;
						i_34_ = (~0xff00ff & i_38_ * (0xff00ff & i_34_) + (i_36_ & 0xff00ff) * i_37_) + (0xff0000 & (i_36_ & 0xff00) * i_37_ + (0xff00 & i_34_) * i_38_) >> 917253800;
					}
				}
				int i_39_ = 0xff & class238.aByte1829;
				if ((i_39_ ^ 0xffffffff) != -1) {
					i_39_ += 256;
					int i_40_ = ((0xff0000 & i_34_) >> -921160400) * i_39_;
					if ((i_40_ ^ 0xffffffff) < -65536) {
						i_40_ = 65535;
					}
					int i_41_ = (i_34_ >> 1157906824 & 0xff) * i_39_;
					if ((i_41_ ^ 0xffffffff) < -65536) {
						i_41_ = 65535;
					}
					int i_42_ = (i_34_ & 0xff) * i_39_;
					if ((i_42_ ^ 0xffffffff) < -65536) {
						i_42_ = 65535;
					}
					i_34_ = (i_42_ >> -259571832) + (0xff00 & i_41_) + ((i_40_ & 0x7000ff00) << -1508903384);
				}
			}
			aStream4093.setBackingOffset(i * 4);
			if (f != 1.0F) {
				int i_43_ = (0xff9fc6 & i_34_) >> 2035833968;
				int i_44_ = 0xff & i_34_ >> 1655837352;
				i_43_ *= f;
				int i_45_ = 0xff & i_34_;
				i_44_ *= f;
				if ((i_43_ ^ 0xffffffff) > -1) {
					i_43_ = 0;
				} else if ((i_43_ ^ 0xffffffff) < -256) {
					i_43_ = 255;
				}
				i_45_ *= f;
				if (i_44_ < 0) {
					i_44_ = 0;
				} else if (i_44_ > 255) {
					i_44_ = 255;
				}
				if ((i_45_ ^ 0xffffffff) > -1) {
					i_45_ = 0;
				} else if ((i_45_ ^ 0xffffffff) < -256) {
					i_45_ = 255;
				}
				i_34_ = i_43_ << -663945424 | i_44_ << 1289794376 | i_45_;
			}
			if (aHa_Sub3_4087.anInt4580 == 0) {
				aStream4093.writeByte((byte) i_34_);
				aStream4093.writeByte((byte) (i_34_ >> 1356888040));
				aStream4093.writeByte((byte) (i_34_ >> -1014001104));
			} else {
				aStream4093.writeByte((byte) (i_34_ >> 771535760));
				aStream4093.writeByte((byte) (i_34_ >> 1569612008));
				aStream4093.writeByte((byte) i_34_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "li.I(" + i + ',' + i_32_ + ',' + i_33_ + ',' + i_34_ + ',' + f + ')');
		}
	}
}

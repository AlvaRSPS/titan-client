
/* ha_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;

import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.LinkedList;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.VarClientDefinition;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.TextLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.constants.BuildType;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.input.impl.AwtKeyListener;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.font.OpenGlFont;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.heap.Heap;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.matrix.OpenGlMatrix;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeap;
import jaggl.OpenGL;

public final class OpenGlToolkit extends RSToolkit {
	public static Class85 aClass85_4299 = new Class85(2, -1);

	public static final void method1853(int i, RSByteBuffer class98_sub22) {
		int i_116_ = class98_sub22.readSmart(i + 1689622710);
		if (i == 2) {
			Class98_Sub10_Sub36.aClass114Array5744 = new Class114[i_116_];
			for (int i_117_ = 0; (i_116_ ^ 0xffffffff) < (i_117_ ^ 0xffffffff); i_117_++) {
				Class98_Sub10_Sub36.aClass114Array5744[i_117_] = new Class114();
				Class98_Sub10_Sub36.aClass114Array5744[i_117_].anInt956 = class98_sub22.readSmart(1689622712);
				Class98_Sub10_Sub36.aClass114Array5744[i_117_].aString957 = class98_sub22.readPrefixedString(i ^ ~0x2);
			}
			Class164.anInt1274 = class98_sub22.readSmart(1689622712);
			GrandExchangeOffer.anInt854 = class98_sub22.readSmart(1689622712);
			Class42_Sub3.anInt5366 = class98_sub22.readSmart(i ^ 0x64b598ba);
			OpenGlTileMaterial.aClass53_Sub1Array3967 = new Class53_Sub1[1 + -Class164.anInt1274 + GrandExchangeOffer.anInt854];
			for (int i_118_ = 0; Class42_Sub3.anInt5366 > i_118_; i_118_++) {
				int i_119_ = class98_sub22.readSmart(1689622712);
				Class53_Sub1 class53_sub1 = OpenGlTileMaterial.aClass53_Sub1Array3967[i_119_] = new Class53_Sub1();
				class53_sub1.anInt426 = class98_sub22.readUnsignedByte((byte) -111);
				class53_sub1.anInt427 = class98_sub22.readInt(-2);
				class53_sub1.anInt3632 = i_119_ - -Class164.anInt1274;
				class53_sub1.aString3630 = class98_sub22.readPrefixedString(-1);
				class53_sub1.aString3634 = class98_sub22.readPrefixedString(-1);
			}
			BuildType.anInt88 = class98_sub22.readInt(i ^ ~0x3);
			RtMouseEvent.aBoolean3944 = true;
		}
	}

	public static final void method1871(int i, int i_196_, boolean bool, int i_197_, int i_198_, int i_199_, int i_200_) {
		Class308.anInt2580 = i_197_;
		GZipDecompressor.anInt1967 = i_199_;
		Class98_Sub10_Sub21.anInt5643 = i_198_;
		StrongReferenceMCNode.anInt6300 = i;
		NewsFetcher.anInt3098 = i_196_;
		if (bool && (GZipDecompressor.anInt1967 ^ 0xffffffff) <= -101) {
			SpriteLoadingScreenElement.yCamPosTile = 256 + Class98_Sub10_Sub21.anInt5643 * 512;
			Class98_Sub46_Sub10.xCamPosTile = 256 + NewsFetcher.anInt3098 * 512;
			AdvancedMemoryCache.anInt601 = StrongReferenceMCNode.getHeight(Font.localPlane, SpriteLoadingScreenElement.yCamPosTile, Class98_Sub46_Sub10.xCamPosTile, 24111) + -Class308.anInt2580;
		}
		Class116.anInt967 = CharacterShadowsPreferenceField.anInt3712 = -1;
		Class98_Sub46_Sub20_Sub2.cameraMode = 2;
	}

	public static final void method1895(int i, byte i_276_, String string) {
		Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i, -76, 3);
		if (i_276_ < -38) {
			class98_sub46_sub17.method1626((byte) -103);
			class98_sub46_sub17.dialogueText = string;
		}
	}

	private boolean								aBoolean4317;
	private boolean								aBoolean4345;
	private boolean								aBoolean4346;
	private boolean								aBoolean4349;
	private boolean								aBoolean4352;
	private boolean								aBoolean4360;
	boolean										underwater;
	boolean										aBoolean4367;
	boolean										aBoolean4375;
	private boolean								aBoolean4388;
	boolean										aBoolean4391;
	boolean										aBoolean4399;
	private boolean								aBoolean4405;
	boolean										aBoolean4406;
	private boolean								aBoolean4422;
	boolean										aBoolean4424;
	boolean										aBoolean4426;
	boolean										aBoolean4431;
	boolean										aBoolean4447;
	private boolean								aBoolean4448;
	private boolean								aBoolean4450;
	private boolean								aBoolean4457;
	private boolean								aBoolean4465;
	byte[]										aByteArray4469;
	public OpenGlArrayBufferPointer				aClass104_4365;
	public OpenGlArrayBufferPointer				aClass104_4436;
	private OpenGlMatrix						aClass111_Sub1_4315	= new OpenGlMatrix();
	public OpenGlMatrix							aClass111_Sub1_4316	= new OpenGlMatrix();
	public OpenGlMatrix							aClass111_Sub1_4348;
	OpenGlMatrix								aClass111_Sub1_4353;
	OpenGlMatrix								aClass111_Sub1_4354;
	public OpenGlModelRenderer					aClass146_Sub2_4355;
	public OpenGlModelRenderer					aClass146_Sub2_4361;
	public OpenGlModelRenderer					aClass146_Sub2_4369;
	public OpenGlModelRenderer					aClass146_Sub2_4382;
	public OpenGlModelRenderer					aClass146_Sub2_4386;
	public OpenGlModelRenderer					aClass146_Sub2_4393;
	public OpenGlModelRenderer					aClass146_Sub2_4428;
	public OpenGlModelRenderer					aClass146_Sub2_4456;
	public OpenGlModelRenderer					aClass146_Sub2_4461;
	public OpenGlModelRenderer					aClass146_Sub2_4462;
	private LinkedList							aClass148_4320;
	private LinkedList							aClass148_4334;
	private LinkedList							aClass148_4338;
	private LinkedList							aClass148_4339;
	private LinkedList							aClass148_4340;
	private LinkedList							aClass148_4341;
	private LinkedList							aClass148_4342;
	private LinkedList							aClass148_4343;
	private Class283							aClass283_4312;
	private Class288							aClass288_4333;
	Class288									aClass288_4363;
	private Class332_Sub1						aClass332_Sub1_4330;
	private Class360							aClass360_4313		= new Class360();
	public Class42_Sub1							aClass42_Sub1_4358;
	private Class42_Sub1_Sub1					aClass42_Sub1_Sub1_4444;
	private Class42[]							aClass42Array4396;
	private Class48_Sub1						aClass48_Sub1_4443;
	private Class98_Sub28_Sub1					aClass98_Sub28_Sub1_4311;
	private PointLight[]						aClass98_Sub5Array4408;
	private Canvas								activeCanvas;
	private long								activeCanvasHandle;
	float										aFloat4356;
	public float								aFloat4359;
	public float								aFloat4364;
	private float								aFloat4371;
	private float								aFloat4376;
	public float								aFloat4379;
	private float								aFloat4380;
	private float								aFloat4392;
	private float								aFloat4400;
	public float								aFloat4401;
	public float								aFloat4407;
	public float								ambient;
	private float								aFloat4416;
	public float								aFloat4420;
	public float								aFloat4421;
	private float								aFloat4429;
	public float								aFloat4433;
	public float								sunIntensity;
	public float								aFloat4437;
	private float								aFloat4452;
	public float								aFloat4458;
	private float[]								aFloatArray4372;
	private float[]								aFloatArray4418;
	public float[]								sunDirection;
	private float[]								aFloatArray4440;
	private float[]								aFloatArray4463;
	private Hashtable							aHashtable4306		= new Hashtable();
	private long								aLong4351;
	public NativeHeap							heap;
	private int									anInt4297;
	private int									anInt4301;
	public int									height;
	public int									width;
	public int									anInt4309			= 128;
	public int									anInt4318			= 8;
	public int									shadowScale;
	public int									anInt4321;
	private int									anInt4324;
	private int									anInt4327;
	private int									anInt4331;
	private int									anInt4335;
	public int									anInt4336;
	public int									anInt4337;
	private int									anInt4344;
	private int									anInt4347;
	private int									anInt4350;
	private int									anInt4357;
	public int									anInt4362;
	private int									anInt4368;
	private int									anInt4370;
	public int									sunProjectionZ;
	public int									anInt4381;
	private int									anInt4384;
	private int									anInt4385;
	private int									anInt4387;
	private int									anInt4389;
	private int									anInt4390;
	public int									anInt4394;
	private int									anInt4395;
	public int									sunProjectionX;
	private int									anInt4402;
	public int									anInt4404;
	private int									anInt4409;
	int											anInt4410;
	private int									anInt4411;
	private int									anInt4412;
	private int									anInt4414;
	private int									anInt4415;
	private int									anInt4417;
	public int									anInt4419;
	int											anInt4423;
	public int									anInt4427;
	private int									anInt4432;
	private int									anInt4445;
	public int									anInt4451;
	int											anInt4453;
	int											anInt4454;
	public int									anInt4455;
	private int									anInt4466;
	private int									anInt4467;
	public int[]								anIntArray4468;
	public int[]								shadowY;
	public int[]								shadowX;
	private OpenGLRenderTarget					anInterface12_4325;
	private OpenGLRenderTarget					anInterface12_4328;
	private OpenGLRenderTarget[]				anInterface12Array4326;
	private OpenGLRenderTarget[]				anInterface12Array4329;
	private OpenGLRenderTarget[]				anInterface12Array4332;
	private ArrayBuffer							anInterface16_4374;
	private ArrayBuffer							anInterface16_4464;
	int											antiAliasCount;
	public boolean								bigEndian;
	private OpenGLElementBuffer					boundElementBuffer;
	private Canvas								defaultCanvas;
	private long								defaultCanvasHandle;
	private OpenGLRenderEffectManager			effectManager;
	public int									fogDistance;
	private OpenGL								gl;
	private String								glRenderer;
	private String								glVendor;
	private boolean								hasFog;
	boolean										haveArbTextureFloat;
	boolean										haveArbTextureRectangle;
	private boolean								haveArbVbo;
	boolean										haveExt3DTexture;
	boolean										haveExtFrameBufferObject;
	public OpenGLHeightMapToNormalMapConverter	hmToNormalCvtrMap;
	private boolean								isNotS3GfxCard;

	public RsFloatBuffer						dataBuffer;

	private OpenGLTexture2DSource				texFactory;

	int											textureType;

	private boolean								useArbVbo;

	public OpenGlToolkit(Canvas defaultCanvas, TextureMetricsList textureSource, int antialiasCount) {
		super(textureSource);
		aBoolean4317 = false;
		shadowScale = 3;
		aClass148_4320 = new LinkedList();
		anInterface12Array4326 = new OpenGLRenderTarget[4];
		anInterface12Array4329 = new OpenGLRenderTarget[4];
		anInt4324 = -1;
		anInt4327 = -1;
		anInt4331 = -1;
		anInterface12Array4332 = new OpenGLRenderTarget[4];
		new Queue();
		new HashTable(16);
		aClass148_4334 = new LinkedList();
		aClass148_4338 = new LinkedList();
		aClass148_4339 = new LinkedList();
		aClass148_4340 = new LinkedList();
		aClass148_4341 = new LinkedList();
		aClass148_4342 = new LinkedList();
		aClass148_4343 = new LinkedList();
		aClass111_Sub1_4348 = new OpenGlMatrix();
		aClass111_Sub1_4353 = new OpenGlMatrix();
		aClass111_Sub1_4354 = new OpenGlMatrix();
		aFloat4376 = 0.0F;
		anInt4370 = 0;
		aFloat4392 = 1.0F;
		anInt4395 = 8448;
		anInt4387 = 8448;
		aClass98_Sub5Array4408 = new PointLight[VarClientDefinition.anInt721];
		aFloat4400 = -1.0F;
		aFloatArray4372 = new float[4];
		anInt4389 = 3584;
		anInt4414 = 0;
		aFloatArray4418 = new float[4];
		anInt4404 = 50;
		anInt4412 = -1;
		anInt4402 = 0;
		aFloat4407 = -1.0F;
		anInt4423 = -1;
		anInt4427 = 0;
		sunIntensity = -1.0F;
		aFloat4429 = -1.0F;
		anInt4381 = 512;
		aFloat4420 = 1.0F;
		aFloatArray4440 = new float[16];
		aBoolean4448 = false;
		anInt4432 = 0;
		aFloat4379 = 3584.0F;
		sunDirection = new float[4];
		anInt4455 = -1;
		anInt4454 = -1;
		fogDistance = -1;
		anInt4394 = 0;
		anInt4415 = 0;
		aFloat4452 = 1.0F;
		aFloat4458 = 1.0F;
		anInt4419 = 512;
		aFloat4401 = 3584.0F;
		aFloat4433 = 1.0F;
		anInt4451 = 0;
		aFloatArray4463 = new float[4];
		anInt4453 = 0;
		anInt4409 = 0;
		aBoolean4465 = true;
		dataBuffer = new RsFloatBuffer(8192);
		anIntArray4468 = new int[1];
		aByteArray4469 = new byte[16384];
		shadowY = new int[1];
		shadowX = new int[1];
		activeCanvas = this.defaultCanvas = defaultCanvas;
		antiAliasCount = antialiasCount;
		if (!AdvancedMemoryCache.loadNative("jaclib", (byte) -36)) {
			throw new RuntimeException("Unable to load JAGeX C library!");
		}
		if (!AdvancedMemoryCache.loadNative("jaggl", (byte) -36)) {
			throw new RuntimeException("Unable to load JAGeX OpenGL bindings!");
		}
		try {
			gl = new OpenGL();
			activeCanvasHandle = defaultCanvasHandle = gl.init(defaultCanvas, 8, 8, 8, 24, 0, antiAliasCount);
			if ((defaultCanvasHandle ^ 0xffffffffffffffffL) == -1L) {
				throw new RuntimeException("Error initializing OpenGL bindings!");
			}
			attachGl(-33);
			int rendererCaps = getRendererCaps((byte) -123);
			if ((rendererCaps ^ 0xffffffff) != -1) {
				throw new RuntimeException("Card does not support all required features");
			}
			textureType = !bigEndian ? 5121 : 33639;
			if ((glRenderer.indexOf("radeon") ^ 0xffffffff) != 0) {
				int radeonNumber = 0;
				boolean isRadeonHD = false;
				boolean bool_194_ = false;
				String[] glRendererTokens = Class112.splitString(glRenderer.replace('/', ' '), ' ', false);
				for (int index = 0; (index ^ 0xffffffff) > (glRendererTokens.length ^ 0xffffffff); index++) {
					String token = glRendererTokens[index];
					try {
						if ((token.length() ^ 0xffffffff) < -1) {
							if ((token.charAt(0) ^ 0xffffffff) == -121 && token.length() >= 3 && AwtKeyListener.validateInteger((byte) 53, token.substring(1, 3))) {
								bool_194_ = true;
								token = token.substring(1);
							}
							if (token.equals("hd")) {
								isRadeonHD = true;
							} else {
								if (token.startsWith("hd")) {
									isRadeonHD = true;
									token = token.substring(2);
								}
								if ((token.length() ^ 0xffffffff) <= -5 && AwtKeyListener.validateInteger((byte) 53, token.substring(0, 4))) {
									radeonNumber = JavaNetworkWriter.parseInteger(-94, token.substring(0, 4));
									break;
								}
							}
						}
					} catch (Exception exception) {
						/* empty */
					}
				}
				if (!bool_194_ && !isRadeonHD) {
					if ((radeonNumber ^ 0xffffffff) <= -7001 && radeonNumber <= 9250) {
						haveExt3DTexture = false;
					}
					if (radeonNumber >= 7000 && (radeonNumber ^ 0xffffffff) >= -8000) {
						haveArbVbo = false;
					}
				}
				if (!isRadeonHD || radeonNumber < 4000) {
					haveArbTextureFloat = false;
				}
				haveArbTextureRectangle &= gl.a("GL_ARB_half_float_pixel");
				useArbVbo = haveArbVbo;
				aBoolean4406 = true;
			}
			if (glVendor.indexOf("intel") != -1) {
				haveExtFrameBufferObject = false;
			}
			isNotS3GfxCard = !glVendor.equals("s3 graphics");
			if (haveArbVbo) {
				try {
					int[] is = new int[1];
					OpenGL.glGenBuffersARB(1, is, 0);
				} catch (Throwable throwable) {
					throw new RuntimeException("");
				}
			}
			GrandExchangeOffer.method1702(8, false, true);
			aBoolean4317 = true;
			texFactory = new OpenGLTexture2DSource(this, this.metricsList);
			method1903(98);
			hmToNormalCvtrMap = new OpenGLHeightMapToNormalMapConverter(this);
			aClass283_4312 = new Class283(this);
			if (aClass283_4312.method3349(true)) {
				aClass98_Sub28_Sub1_4311 = new Class98_Sub28_Sub1(this);
				if (!aClass98_Sub28_Sub1_4311.method1309((byte) 41)) {
					aClass98_Sub28_Sub1_4311.method1304((byte) -83);
					aClass98_Sub28_Sub1_4311 = null;
				}
			}
			effectManager = new OpenGLRenderEffectManager(this);
			method1864(-32);
			method1850((byte) -31);
			method1817();
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			method1773();
			throw new RuntimeException("");
		}
	}

	@Override
	public final void a(Matrix class111) {
		do {
			try {
				aClass111_Sub1_4348.method2092(class111);
				aClass111_Sub1_4353.method2092(aClass111_Sub1_4348);
				aClass111_Sub1_4353.method2111(2);
				aClass111_Sub1_4354.method2112(aClass111_Sub1_4353, (byte) -118);
				if ((anInt4385 ^ 0xffffffff) == -2) {
					break;
				}
				method1833(4);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.QE(" + (class111 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void a(int i, int i_326_, int i_327_, int i_328_, int i_329_, int i_330_, RtInterfaceClip var_aa, int i_331_, int i_332_) {
		try {
			aa_Sub3 var_aa_Sub3 = (aa_Sub3) var_aa;
			Class42_Sub1_Sub1 class42_sub1_sub1 = var_aa_Sub3.aClass42_Sub1_Sub1_3568;
			method1829((byte) -99);
			setActiveTexture(1, var_aa_Sub3.aClass42_Sub1_Sub1_3568);
			setBlendMode((byte) -109, i_330_);
			method1899(8448, 8960, 7681);
			method1840(0, 768, 90, 34167);
			float f = class42_sub1_sub1.aFloat6205 / class42_sub1_sub1.anInt6207;
			float f_333_ = class42_sub1_sub1.aFloat6209 / class42_sub1_sub1.anInt6204;
			float f_334_ = (float) i_327_ - (float) i;
			float f_335_ = (float) i_328_ - (float) i_326_;
			float f_336_ = (float) (1.0 / Math.sqrt(f_334_ * f_334_ + f_335_ * f_335_));
			OpenGL.glColor4ub((byte) (i_329_ >> 99388688), (byte) (i_329_ >> 1644719592), (byte) i_329_, (byte) (i_329_ >> -1606324584));
			f_335_ *= f_336_;
			f_334_ *= f_336_;
			OpenGL.glBegin(1);
			OpenGL.glTexCoord2f(f * (-i_331_ + i), (i_326_ + -i_332_) * f_333_);
			OpenGL.glVertex2f(0.35F + i, i_326_ + 0.35F);
			OpenGL.glTexCoord2f(f * (i_327_ + -i_331_), (-i_332_ + i_328_) * f_333_);
			OpenGL.glVertex2f(i_327_ + f_334_ + 0.35F, 0.35F + (f_335_ + i_328_));
			OpenGL.glEnd();
			method1840(0, 768, -52, 5890);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.MJ(" + i + ',' + i_326_ + ',' + i_327_ + ',' + i_328_ + ',' + i_329_ + ',' + i_330_ + ',' + (var_aa != null ? "{...}" : "null") + ',' + i_331_ + ',' + i_332_ + ')');
		}
	}

	@Override
	public final void a(int i, int i_132_, int i_133_, int i_134_, int i_135_, int i_136_, RtInterfaceClip var_aa, int i_137_, int i_138_, int i_139_, int i_140_, int i_141_) {
		try {
			if (i_133_ != i || (i_132_ ^ 0xffffffff) != (i_134_ ^ 0xffffffff)) {
				aa_Sub3 var_aa_Sub3 = (aa_Sub3) var_aa;
				Class42_Sub1_Sub1 class42_sub1_sub1 = var_aa_Sub3.aClass42_Sub1_Sub1_3568;
				method1829((byte) -119);
				setActiveTexture(1, var_aa_Sub3.aClass42_Sub1_Sub1_3568);
				setBlendMode((byte) -69, i_136_);
				method1899(8448, 8960, 7681);
				method1840(0, 768, -121, 34167);
				float f = class42_sub1_sub1.aFloat6205 / class42_sub1_sub1.anInt6207;
				float f_142_ = class42_sub1_sub1.aFloat6209 / class42_sub1_sub1.anInt6204;
				float f_143_ = (float) -i + (float) i_133_;
				float f_144_ = (float) i_134_ - (float) i_132_;
				float f_145_ = (float) (1.0 / Math.sqrt(f_144_ * f_144_ + f_143_ * f_143_));
				f_143_ *= f_145_;
				f_144_ *= f_145_;
				OpenGL.glColor4ub((byte) (i_135_ >> 1969603824), (byte) (i_135_ >> -519019928), (byte) i_135_, (byte) (i_135_ >> 1163947384));
				i_141_ %= i_140_ - -i_139_;
				float f_146_ = f_143_ * i_139_;
				float f_147_ = i_139_ * f_144_;
				float f_148_ = 0.0F;
				float f_149_ = 0.0F;
				float f_150_ = f_146_;
				float f_151_ = f_147_;
				if ((i_139_ ^ 0xffffffff) <= (i_141_ ^ 0xffffffff)) {
					f_150_ = (-i_141_ + i_139_) * f_143_;
					f_151_ = f_144_ * (i_139_ - i_141_);
				} else {
					f_148_ = (i_140_ + i_139_ - i_141_) * f_143_;
					f_149_ = f_144_ * (-i_141_ + i_140_ + i_139_);
				}
				float f_152_ = 0.35F + i + f_148_;
				float f_153_ = 0.35F + i_132_ + f_149_;
				float f_154_ = f_143_ * i_140_;
				float f_155_ = i_140_ * f_144_;
				for (;;) {
					if (i_133_ <= i) {
						if (0.35F + i_133_ > f_152_) {
							break;
						}
						if (i_133_ > f_150_ + f_152_) {
							f_150_ = -f_152_ + i_133_;
						}
					} else {
						if (f_152_ > i_133_ + 0.35F) {
							break;
						}
						if (f_152_ + f_150_ > i_133_) {
							f_150_ = i_133_ - f_152_;
						}
					}
					if ((i_134_ ^ 0xffffffff) >= (i_132_ ^ 0xffffffff)) {
						if (0.35F + i_134_ > f_153_) {
							break;
						}
						if (f_151_ + f_153_ < i_134_) {
							f_151_ = i_134_ - f_153_;
						}
					} else {
						if (i_134_ + 0.35F < f_153_) {
							break;
						}
						if (i_134_ < f_151_ + f_153_) {
							f_151_ = i_134_ - f_153_;
						}
					}
					OpenGL.glBegin(1);
					OpenGL.glTexCoord2f(f * (-i_137_ + f_152_), f_142_ * (-i_138_ + f_153_));
					OpenGL.glVertex2f(f_152_, f_153_);
					OpenGL.glTexCoord2f((-i_137_ + (f_150_ + f_152_)) * f, (-i_138_ + (f_151_ + f_153_)) * f_142_);
					OpenGL.glVertex2f(f_152_ + f_150_, f_153_ + f_151_);
					OpenGL.glEnd();
					f_152_ += f_154_ + f_150_;
					f_153_ += f_151_ + f_155_;
					f_151_ = f_147_;
					f_150_ = f_146_;
				}
				method1840(0, 768, 111, 5890);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.FJ(" + i + ',' + i_132_ + ',' + i_133_ + ',' + i_134_ + ',' + i_135_ + ',' + i_136_ + ',' + (var_aa != null ? "{...}" : "null") + ',' + i_137_ + ',' + i_138_ + ',' + i_139_ + ',' + i_140_ + ',' + i_141_ + ')');
		}
	}

	@Override
	public final Ground a(int i, int i_219_, int[][] is, int[][] is_220_, int i_221_, int i_222_, int i_223_) {
		try {
			return new OpenGlGround(this, i_222_, i_223_, i, i_219_, is, is_220_, i_221_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.DG(" + i + ',' + i_219_ + ',' + (is != null ? "{...}" : "null") + ',' + (is_220_ != null ? "{...}" : "null") + ',' + i_221_ + ',' + i_222_ + ',' + i_223_ + ')');
		}
	}

	@Override
	public final void a(Rectangle[] rectangles, int i, int i_62_, int i_63_) throws Exception_Sub1 {
		try {
			method1764(i_62_, i_63_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.DD(" + (rectangles != null ? "{...}" : "null") + ',' + i + ',' + i_62_ + ',' + i_63_ + ')');
		}
	}

	@Override
	public final void a(Heap var_za) {
		do {
			try {
				heap = ((OpenGLHeap) var_za).heap;
				if (anInterface16_4464 != null) {
					break;
				}
				RsFloatBuffer class98_sub22_sub2 = new RsFloatBuffer(80);
				do {
					if (!bigEndian) {
						class98_sub22_sub2.method1265((byte) -52, -1.0F);
						class98_sub22_sub2.method1265((byte) -52, -1.0F);
						class98_sub22_sub2.method1265((byte) -52, 0.0F);
						class98_sub22_sub2.method1265((byte) -52, 0.0F);
						class98_sub22_sub2.method1265((byte) -52, 1.0F);
						class98_sub22_sub2.method1265((byte) -52, 1.0F);
						class98_sub22_sub2.method1265((byte) -52, -1.0F);
						class98_sub22_sub2.method1265((byte) -52, 0.0F);
						class98_sub22_sub2.method1265((byte) -52, 1.0F);
						class98_sub22_sub2.method1265((byte) -52, 1.0F);
						class98_sub22_sub2.method1265((byte) -52, 1.0F);
						class98_sub22_sub2.method1265((byte) -52, 1.0F);
						class98_sub22_sub2.method1265((byte) -52, 0.0F);
						class98_sub22_sub2.method1265((byte) -52, 1.0F);
						class98_sub22_sub2.method1265((byte) -52, 0.0F);
						class98_sub22_sub2.method1265((byte) -52, -1.0F);
						class98_sub22_sub2.method1265((byte) -52, 1.0F);
						class98_sub22_sub2.method1265((byte) -52, 0.0F);
						class98_sub22_sub2.method1265((byte) -52, 0.0F);
						class98_sub22_sub2.method1265((byte) -52, 0.0F);
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					class98_sub22_sub2.writeFloat((byte) 72, -1.0F);
					class98_sub22_sub2.writeFloat((byte) -116, -1.0F);
					class98_sub22_sub2.writeFloat((byte) 74, 0.0F);
					class98_sub22_sub2.writeFloat((byte) 81, 0.0F);
					class98_sub22_sub2.writeFloat((byte) 82, 1.0F);
					class98_sub22_sub2.writeFloat((byte) 49, 1.0F);
					class98_sub22_sub2.writeFloat((byte) 103, -1.0F);
					class98_sub22_sub2.writeFloat((byte) -113, 0.0F);
					class98_sub22_sub2.writeFloat((byte) 21, 1.0F);
					class98_sub22_sub2.writeFloat((byte) -98, 1.0F);
					class98_sub22_sub2.writeFloat((byte) -116, 1.0F);
					class98_sub22_sub2.writeFloat((byte) 4, 1.0F);
					class98_sub22_sub2.writeFloat((byte) 83, 0.0F);
					class98_sub22_sub2.writeFloat((byte) -107, 1.0F);
					class98_sub22_sub2.writeFloat((byte) 22, 0.0F);
					class98_sub22_sub2.writeFloat((byte) -106, -1.0F);
					class98_sub22_sub2.writeFloat((byte) -96, 1.0F);
					class98_sub22_sub2.writeFloat((byte) 11, 0.0F);
					class98_sub22_sub2.writeFloat((byte) -105, 0.0F);
					class98_sub22_sub2.writeFloat((byte) -120, 0.0F);
				} while (false);
				anInterface16_4464 = method1878(class98_sub22_sub2.position, false, 20, -54, class98_sub22_sub2.payload);
				aClass104_4365 = new OpenGlArrayBufferPointer(anInterface16_4464, 5126, 3, 0);
				aClass104_4436 = new OpenGlArrayBufferPointer(anInterface16_4464, 5126, 2, 12);
				aClass360_4313.method3912(this, 196584);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.EH(" + (var_za != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void fillImageClip(int i, RtInterfaceClip var_aa, int i_15_, int i_16_) {
		try {
			aa_Sub3 var_aa_Sub3 = (aa_Sub3) var_aa;
			Class42_Sub1_Sub1 class42_sub1_sub1 = var_aa_Sub3.aClass42_Sub1_Sub1_3568;
			method1829((byte) -90);
			setActiveTexture(1, var_aa_Sub3.aClass42_Sub1_Sub1_3568);
			setBlendMode((byte) -50, 1);
			method1899(8448, 8960, 7681);
			method1840(0, 768, 108, 34167);
			float f = class42_sub1_sub1.aFloat6205 / class42_sub1_sub1.anInt6207;
			float f_17_ = class42_sub1_sub1.aFloat6209 / class42_sub1_sub1.anInt6204;
			OpenGL.glColor4ub((byte) (i >> 1933103952), (byte) (i >> -419860344), (byte) i, (byte) (i >> -1429152424));
			OpenGL.glBegin(7);
			OpenGL.glTexCoord2f((-i_15_ + anInt4370) * f, (anInt4402 - i_16_) * f_17_);
			OpenGL.glVertex2i(anInt4370, anInt4402);
			OpenGL.glTexCoord2f(f * (anInt4370 + -i_15_), (-i_16_ + anInt4414) * f_17_);
			OpenGL.glVertex2i(anInt4370, anInt4414);
			OpenGL.glTexCoord2f(f * (-i_15_ + anInt4432), (-i_16_ + anInt4414) * f_17_);
			OpenGL.glVertex2i(anInt4432, anInt4414);
			OpenGL.glTexCoord2f((-i_15_ + anInt4432) * f, (anInt4402 - i_16_) * f_17_);
			OpenGL.glVertex2i(anInt4432, anInt4402);
			OpenGL.glEnd();
			method1840(0, 768, -65, 5890);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.A(" + i + ',' + (var_aa != null ? "{...}" : "null") + ',' + i_15_ + ',' + i_16_ + ')');
		}
	}

	@Override
	public final void addCanvas(Canvas canvas, int i, int i_167_) {
		do {
			try {
				if (defaultCanvas == canvas) {
					throw new RuntimeException();
				}
				if (aHashtable4306.containsKey(canvas)) {
					break;
				}
				if (!canvas.isShowing()) {
					throw new RuntimeException();
				}
				try {
					Class var_class = Class.forName("java.awt.Canvas");
					Method method = var_class.getMethod("setIgnoreRepaint", Boolean.TYPE);
					method.invoke(canvas, Boolean.TRUE);
				} catch (Exception exception) {
					/* empty */
				}
				long l = gl.prepareSurface(canvas);
				if (l == -1L) {
					throw new RuntimeException();
				}
				aHashtable4306.put(canvas, new Long(l));
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.FI(" + (canvas != null ? "{...}" : "null") + ',' + i + ',' + i_167_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void applyFog(int i, int i_31_, int i_32_) {
		try {
			if ((anInt4455 ^ 0xffffffff) != (i ^ 0xffffffff) || (i_31_ ^ 0xffffffff) != (fogDistance ^ 0xffffffff) || anInt4427 != i_32_) {
				anInt4455 = i;
				anInt4427 = i_32_;
				fogDistance = i_31_;
				method1852((byte) -98);
				method1893(109);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.L(" + i + ',' + i_31_ + ',' + i_32_ + ')');
		}
	}

	@Override
	public final void attachContext(int i) {
		try {
			attachGl(-30);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.KJ(" + i + ')');
		}
	}

	private final void attachGl(int i) {
		do {
			try {
				int i_120_ = 0;
				while (!gl.b()) {
					if ((i_120_++ ^ 0xffffffff) < -6) {
						throw new RuntimeException("");
					}
					TimeTools.sleep(0, 1000L);
				}
				if (i <= -25) {
					break;
				}
				aClass98_Sub5Array4408 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.BD(" + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void b(int i, int i_313_, int i_314_, int i_315_, double d) {
		/* empty */
	}

	@Override
	public final int c(int i, int i_23_) {
		try {
			return i & i_23_ ^ i_23_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.JH(" + i + ',' + i_23_ + ')');
		}
	}

	@Override
	public final boolean canEnableBloom() {
		try {
			return !(aClass98_Sub28_Sub1_4311 == null || !aClass98_Sub28_Sub1_4311.method1300(0));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.CB(" + ')');
		}
	}

	@Override
	public final void clearClip() {
		try {
			anInt4432 = width;
			anInt4370 = 0;
			anInt4402 = 0;
			anInt4414 = height;
			OpenGL.glDisable(3089);
			method1906(4353);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.la(" + ')');
		}
	}

	@Override
	public final void clearImage(int i) {
		try {
			setBlendMode((byte) -80, 0);
			OpenGL.glClearColor((i & 0xff0000) / 1.671168E7F, (i & 0xff00) / 65280.0F, (i & 0xff) / 255.0F, (i >>> 727917496) / 255.0F);
			OpenGL.glClear(16384);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.GA(" + i + ')');
		}
	}

	@Override
	public final void constrainClip(int i, int i_103_, int i_104_, int i_105_) {
		try {
			if ((anInt4414 ^ 0xffffffff) < (i_105_ ^ 0xffffffff)) {
				anInt4414 = i_105_;
			}
			if ((i ^ 0xffffffff) < (anInt4370 ^ 0xffffffff)) {
				anInt4370 = i;
			}
			if (anInt4432 > i_104_) {
				anInt4432 = i_104_;
			}
			if ((i_103_ ^ 0xffffffff) < (anInt4402 ^ 0xffffffff)) {
				anInt4402 = i_103_;
			}
			OpenGL.glEnable(3089);
			method1906(4353);
			method1828((byte) -118);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.T(" + i + ',' + i_103_ + ',' + i_104_ + ',' + i_105_ + ')');
		}
	}

	@Override
	public final void createContexts(int i) {
		try {
			if ((i ^ 0xffffffff) != -2) {
				throw new IllegalArgumentException("");
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.OF(" + i + ')');
		}
	}

	@Override
	public final Font createFont(FontSpecifications class197, Image[] class324s, boolean bool) {
		try {
			return new OpenGlFont(this, class197, class324s, bool);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.HH(" + (class197 != null ? "{...}" : "null") + ',' + (class324s != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	@Override
	public final Matrix createMatrix() {
		try {
			return new OpenGlMatrix();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.SE(" + ')');
		}
	}

	@Override
	public final ModelRenderer createModelRenderer(BaseModel class178, int i, int i_97_, int i_98_, int i_99_) {
		try {
			return new OpenGlModelRenderer(this, class178, i, i_98_, i_99_, i_97_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.VD(" + (class178 != null ? "{...}" : "null") + ',' + i + ',' + i_97_ + ',' + i_98_ + ',' + i_99_ + ')');
		}
	}

	@Override
	public final Heap createHeap(int i) {
		try {
			OpenGLHeap var_za_Sub2 = new OpenGLHeap(i);
			aClass148_4320.addLast(var_za_Sub2, -20911);
			return var_za_Sub2;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.ME(" + i + ')');
		}
	}

	@Override
	public final PointLight createPointLight(int i, int i_317_, int i_318_, int i_319_, int i_320_, float f) {
		try {
			return new Class98_Sub5_Sub2(i, i_317_, i_318_, i_319_, i_320_, f);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.HG(" + i + ',' + i_317_ + ',' + i_318_ + ',' + i_319_ + ',' + i_320_ + ',' + f + ')');
		}
	}

	@Override
	public final RenderTarget createRenderTarget(Interface5 interface5, DepthBufferObject interface13) {
		try {
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.EC(" + (interface5 != null ? "{...}" : "null") + ',' + (interface13 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Sprite createSprite(Image class324, boolean bool) {
		try {
			int[] is = new int[class324.anInt2720 * class324.anInt2722];
			int i = 0;
			int i_212_ = 0;
			if (class324.aByteArray2723 != null) {
				for (int i_213_ = 0; class324.anInt2720 > i_213_; i_213_++) {
					for (int i_214_ = 0; (class324.anInt2722 ^ 0xffffffff) < (i_214_ ^ 0xffffffff); i_214_++) {
						is[i_212_++] = Class41.or(class324.anIntArray2718[Class202.and(255, class324.aByteArray2717[i])], class324.aByteArray2723[i] << -986422664);
						i++;
					}
				}
			} else {
				for (int i_215_ = 0; i_215_ < class324.anInt2720; i_215_++) {
					for (int i_216_ = 0; (i_216_ ^ 0xffffffff) > (class324.anInt2722 ^ 0xffffffff); i_216_++) {
						int i_217_ = class324.anIntArray2718[class324.aByteArray2717[i++] & 0xff];
						is[i_212_++] = i_217_ != 0 ? Class41.or(i_217_, -16777216) : 0;
					}
				}
			}
			Sprite class332 = this.createSprite(-7962, 0, class324.anInt2722, class324.anInt2720, is, class324.anInt2722);
			class332.method3740(class324.anInt2725, class324.anInt2721, class324.anInt2719, class324.anInt2724);
			return class332;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.IE(" + (class324 != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	@Override
	public final Sprite createSprite(int i, int i_35_, boolean bool) {
		try {
			return new Class332_Sub1(this, i, i_35_, bool);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.EG(" + i + ',' + i_35_ + ',' + bool + ')');
		}
	}

	@Override
	public final Sprite createSprite(int[] is, int i, int i_225_, int i_226_, int i_227_, boolean bool) {
		try {
			return new Class332_Sub1(this, i_226_, i_227_, is, i, i_225_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.MF(" + (is != null ? "{...}" : "null") + ',' + i + ',' + i_225_ + ',' + i_226_ + ',' + i_227_ + ',' + bool + ')');
		}
	}

	@Override
	public final void da(int i, int i_93_, int i_94_, int[] is) {
		try {
			float f = aClass111_Sub1_4348.aFloat4677 + (i_93_ * aClass111_Sub1_4348.aFloat4676 + aClass111_Sub1_4348.aFloat4684 * i + i_94_ * aClass111_Sub1_4348.aFloat4673);
			if (anInt4404 > f || f > anInt4389) {
				is[0] = is[1] = is[2] = -1;
			} else {
				int i_95_ = (int) ((aClass111_Sub1_4348.aFloat4680 * i_94_ + (aClass111_Sub1_4348.aFloat4679 * i_93_ + aClass111_Sub1_4348.aFloat4686 * i) + aClass111_Sub1_4348.aFloat4674) * anInt4419 / f);
				int i_96_ = (int) (anInt4381 * (aClass111_Sub1_4348.aFloat4675 * i_93_ + i * aClass111_Sub1_4348.aFloat4678 + i_94_ * aClass111_Sub1_4348.aFloat4687 + aClass111_Sub1_4348.aFloat4683) / f);
				if (!(i_95_ >= aFloat4421) || !(aFloat4364 >= i_95_) || !(aFloat4359 <= i_96_) || !(aFloat4437 >= i_96_)) {
					is[0] = is[1] = is[2] = -1;
				} else {
					is[0] = (int) (i_95_ - aFloat4421);
					is[1] = (int) (-aFloat4359 + i_96_);
					is[2] = (int) f;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.da(" + i + ',' + i_93_ + ',' + i_94_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void DA(int i, int i_70_, int i_71_, int i_72_) {
		do {
			try {
				anInt4394 = i_70_;
				anInt4419 = i_71_;
				anInt4451 = i;
				anInt4381 = i_72_;
				method1847(true);
				method1906(4353);
				if (anInt4385 == 3) {
					method1884((byte) 121);
				} else {
					if ((anInt4385 ^ 0xffffffff) != -3) {
						break;
					}
					method1885((byte) -127);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.DA(" + i + ',' + i_70_ + ',' + i_71_ + ',' + i_72_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void drawRectangle(int i, int i_201_, int i_202_, int i_203_, int i_204_, int i_205_) {
		do {
			try {
				float f = i + 0.35F;
				float f_206_ = 0.35F + i_201_;
				float f_207_ = f + i_202_ - 1.0F;
				float f_208_ = i_203_ + f_206_ - 1.0F;
				method1859(-68);
				setBlendMode((byte) -66, i_205_);
				OpenGL.glColor4ub((byte) (i_204_ >> 1422671184), (byte) (i_204_ >> 1460018312), (byte) i_204_, (byte) (i_204_ >> -1612786792));
				if (aBoolean4360) {
					OpenGL.glDisable(32925);
				}
				OpenGL.glBegin(2);
				OpenGL.glVertex2f(f, f_206_);
				OpenGL.glVertex2f(f, f_208_);
				OpenGL.glVertex2f(f_207_, f_208_);
				OpenGL.glVertex2f(f_207_, f_206_);
				OpenGL.glEnd();
				if (!aBoolean4360) {
					break;
				}
				OpenGL.glEnable(32925);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.OE(" + i + ',' + i_201_ + ',' + i_202_ + ',' + i_203_ + ',' + i_204_ + ',' + i_205_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int E() {
		try {
			return anInt4337 + anInt4336 + anInt4335;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.E(" + ')');
		}
	}

	@Override
	public final void EA(int i, int i_182_, int i_183_, int i_184_) {
		do {
			try {
				if (!underwater) {
					throw new RuntimeException("");
				}
				anInt4453 = i_184_;
				anInt4362 = i;
				anInt4423 = i_182_;
				anInt4454 = i_183_;
				if (!aBoolean4448) {
					break;
				}
				effectManager.aClass151_Sub9_432.method2471((byte) 34);
				effectManager.aClass151_Sub9_432.method2470(-16661);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.EA(" + i + ',' + i_182_ + ',' + i_183_ + ',' + i_184_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void F(int i, int i_33_) {
		/* empty */
	}

	@Override
	public final void fillRectangle(int i, int i_48_, int i_49_, int i_50_, int i_51_, int i_52_) {
		do {
			try {
				float f = i + 0.35F;
				float f_53_ = i_48_ + 0.35F;
				float f_54_ = f + i_49_;
				method1859(-24);
				float f_55_ = i_50_ + f_53_;
				setBlendMode((byte) -30, i_52_);
				OpenGL.glColor4ub((byte) (i_51_ >> 993788880), (byte) (i_51_ >> 848547304), (byte) i_51_, (byte) (i_51_ >> -1134524424));
				if (aBoolean4360) {
					OpenGL.glDisable(32925);
				}
				OpenGL.glBegin(7);
				OpenGL.glVertex2f(f, f_53_);
				OpenGL.glVertex2f(f, f_55_);
				OpenGL.glVertex2f(f_54_, f_55_);
				OpenGL.glVertex2f(f_54_, f_53_);
				OpenGL.glEnd();
				if (!aBoolean4360) {
					break;
				}
				OpenGL.glEnable(32925);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.aa(" + i + ',' + i_48_ + ',' + i_49_ + ',' + i_50_ + ',' + i_51_ + ',' + i_52_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void getClip(int[] is) {
		try {
			is[0] = anInt4370;
			is[3] = anInt4414;
			is[2] = anInt4432;
			is[1] = anInt4402;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.K(" + (is != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final int getFarPlane() {
		try {
			return anInt4389;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.XA(" + ')');
		}
	}

	@Override
	public final int getNearPlane() {
		try {
			return anInt4404;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.i(" + ')');
		}
	}

	@Override
	public final int[] getPixels(int i, int i_36_, int i_37_, int i_38_) {
		try {
			int[] is = new int[i_38_ * i_37_];
			for (int i_39_ = 0; i_39_ < i_38_; i_39_++) {
				OpenGL.glReadPixelsi(i, -i_39_ + -i_36_ + height, i_37_, 1, 32993, textureType, is, i_39_ * i_37_);
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.na(" + i + ',' + i_36_ + ',' + i_37_ + ',' + i_38_ + ')');
		}
	}

	private final int getRendererCaps(byte i) {
		try {
			int i_100_ = 0;
			glVendor = OpenGL.glGetString(7936).toLowerCase();
			glRenderer = OpenGL.glGetString(7937).toLowerCase();
			if (glVendor.indexOf("microsoft") != -1) {
				i_100_ |= 0x1;
			}
			if ((glVendor.indexOf("brian paul") ^ 0xffffffff) != 0 || glVendor.indexOf("mesa") != -1) {
				i_100_ |= 0x1;
			}
			String string = OpenGL.glGetString(7938);
			String[] strings = Class112.splitString(string.replace('.', ' '), ' ', false);
			do {
				if ((strings.length ^ 0xffffffff) > -3) {
					i_100_ |= 0x4;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				try {
					int i_101_ = JavaNetworkWriter.parseInteger(127, strings[0]);
					int i_102_ = JavaNetworkWriter.parseInteger(-120, strings[1]);
					anInt4390 = 10 * i_101_ + i_102_;
				} catch (NumberFormatException numberformatexception) {
					i_100_ |= 0x4;
				}
			} while (false);
			if (anInt4390 < 12) {
				i_100_ |= 0x2;
			}
			if (!gl.a("GL_ARB_multitexture")) {
				i_100_ |= 0x8;
			}
			if (!gl.a("GL_ARB_texture_env_combine")) {
				i_100_ |= 0x20;
			}
			int[] is = new int[1];
			OpenGL.glGetIntegerv(34018, is, 0);
			anInt4410 = is[0];
			OpenGL.glGetIntegerv(34929, is, 0);
			anInt4368 = is[0];
			OpenGL.glGetIntegerv(34930, is, 0);
			anInt4445 = is[0];
			if (anInt4410 < 2 || anInt4368 < 2 || (anInt4445 ^ 0xffffffff) > -3) {
				i_100_ |= 0x10;
			}
			bigEndian = Stream.a();
			aBoolean4388 = gl.arePbuffersAvailable();
			haveArbVbo = gl.a("GL_ARB_vertex_buffer_object");
			aBoolean4360 = gl.a("GL_ARB_multisample");
			aBoolean4431 = gl.a("GL_ARB_vertex_program");
			gl.a("GL_ARB_fragment_program");
			aBoolean4399 = gl.a("GL_ARB_vertex_shader");
			aBoolean4447 = gl.a("GL_ARB_fragment_shader");
			haveExt3DTexture = gl.a("GL_EXT_texture3D");
			haveArbTextureRectangle = gl.a("GL_ARB_texture_rectangle");
			aBoolean4391 = gl.a("GL_ARB_texture_cube_map");
			haveArbTextureFloat = gl.a("GL_ARB_texture_float");
			aBoolean4426 = false;
			haveExtFrameBufferObject = gl.a("GL_EXT_framebuffer_object");
			aBoolean4375 = gl.a("GL_EXT_framebuffer_blit");
			aBoolean4424 = gl.a("GL_EXT_framebuffer_multisample");
			aBoolean4450 = aBoolean4375 & aBoolean4424;
			aBoolean4367 = TextLoadingScreenElement.osName.startsWith("mac");
			OpenGL.glGetFloatv(2834, SunDefinitionParser.aFloatArray961, 0);
			aFloat4400 = SunDefinitionParser.aFloatArray961[1];
			aFloat4429 = SunDefinitionParser.aFloatArray961[0];
			if (i_100_ != 0) {
				return i_100_;
			}
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.AD(" + i + ')');
		}
	}

	@Override
	public final void H(int i, int i_24_, int i_25_, int[] is) {
		try {
			float f = aClass111_Sub1_4348.aFloat4677 + (aClass111_Sub1_4348.aFloat4673 * i_25_ + (i_24_ * aClass111_Sub1_4348.aFloat4676 + aClass111_Sub1_4348.aFloat4684 * i));
			if (f == 0.0F) {
				is[0] = is[1] = is[2] = -1;
			} else {
				int i_26_ = (int) ((i * aClass111_Sub1_4348.aFloat4686 + i_24_ * aClass111_Sub1_4348.aFloat4679 + i_25_ * aClass111_Sub1_4348.aFloat4680 + aClass111_Sub1_4348.aFloat4674) * anInt4419 / f);
				is[0] = (int) (-aFloat4421 + i_26_);
				int i_27_ = (int) (anInt4381 * (aClass111_Sub1_4348.aFloat4683 + (i_25_ * aClass111_Sub1_4348.aFloat4687 + (i * aClass111_Sub1_4348.aFloat4678 + i_24_ * aClass111_Sub1_4348.aFloat4675))) / f);
				is[1] = (int) (-aFloat4359 + i_27_);
				is[2] = (int) f;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.H(" + i + ',' + i_24_ + ',' + i_25_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void HA(int i, int i_321_, int i_322_, int i_323_, int[] is) {
		try {
			float f = aClass111_Sub1_4348.aFloat4677 + (aClass111_Sub1_4348.aFloat4673 * i_322_ + (i * aClass111_Sub1_4348.aFloat4684 + aClass111_Sub1_4348.aFloat4676 * i_321_));
			if (f < anInt4404 || f > anInt4389) {
				is[0] = is[1] = is[2] = -1;
			} else {
				int i_324_ = (int) ((aClass111_Sub1_4348.aFloat4674 + (i_322_ * aClass111_Sub1_4348.aFloat4680 + (aClass111_Sub1_4348.aFloat4686 * i + i_321_ * aClass111_Sub1_4348.aFloat4679))) * anInt4419 / i_323_);
				int i_325_ = (int) ((aClass111_Sub1_4348.aFloat4683 + (i_321_ * aClass111_Sub1_4348.aFloat4675 + i * aClass111_Sub1_4348.aFloat4678 + i_322_ * aClass111_Sub1_4348.aFloat4687)) * anInt4381 / i_323_);
				if (i_324_ >= aFloat4421 && aFloat4364 >= i_324_ && i_325_ >= aFloat4359 && i_325_ <= aFloat4437) {
					is[2] = (int) f;
					is[1] = (int) (-aFloat4359 + i_325_);
					is[0] = (int) (i_324_ - aFloat4421);
				} else {
					is[0] = is[1] = is[2] = -1;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.HA(" + i + ',' + i_321_ + ',' + i_322_ + ',' + i_323_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final int I() {
		try {
			int i = anInt4467;
			anInt4467 = 0;
			return i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.I(" + ')');
		}
	}

	@Override
	public final int JA(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		try {
			int i_9_ = 0;
			float f = aClass111_Sub1_4348.aFloat4677 + (i_4_ * aClass111_Sub1_4348.aFloat4676 + aClass111_Sub1_4348.aFloat4684 * i + i_5_ * aClass111_Sub1_4348.aFloat4673);
			if (f < 1.0F) {
				f = 1.0F;
			}
			float f_10_ = aClass111_Sub1_4348.aFloat4677 + (i_6_ * aClass111_Sub1_4348.aFloat4684 + aClass111_Sub1_4348.aFloat4676 * i_7_ + i_8_ * aClass111_Sub1_4348.aFloat4673);
			if (f_10_ < 1.0F) {
				f_10_ = 1.0F;
			}
			if (!(anInt4404 > f) || !(f_10_ < anInt4404)) {
				if (f > anInt4389 && anInt4389 < f_10_) {
					i_9_ |= 0x20;
				}
			} else {
				i_9_ |= 0x10;
			}
			int i_11_ = (int) (anInt4419 * (aClass111_Sub1_4348.aFloat4674 + (aClass111_Sub1_4348.aFloat4679 * i_4_ + aClass111_Sub1_4348.aFloat4686 * i + aClass111_Sub1_4348.aFloat4680 * i_5_)) / f);
			int i_12_ = (int) (anInt4419 * (i_8_ * aClass111_Sub1_4348.aFloat4680 + (i_6_ * aClass111_Sub1_4348.aFloat4686 + i_7_ * aClass111_Sub1_4348.aFloat4679) + aClass111_Sub1_4348.aFloat4674) / f_10_);
			if (i_11_ < aFloat4421 && aFloat4421 > i_12_) {
				i_9_ |= 0x1;
			} else if (i_11_ > aFloat4364 && aFloat4364 < i_12_) {
				i_9_ |= 0x2;
			}
			int i_13_ = (int) (anInt4381 * (aClass111_Sub1_4348.aFloat4683 + (i_5_ * aClass111_Sub1_4348.aFloat4687 + (aClass111_Sub1_4348.aFloat4678 * i + i_4_ * aClass111_Sub1_4348.aFloat4675))) / f);
			int i_14_ = (int) ((aClass111_Sub1_4348.aFloat4683 + (aClass111_Sub1_4348.aFloat4678 * i_6_ + aClass111_Sub1_4348.aFloat4675 * i_7_ + aClass111_Sub1_4348.aFloat4687 * i_8_)) * anInt4381 / f_10_);
			if (i_13_ < aFloat4359 && i_14_ < aFloat4359) {
				i_9_ |= 0x4;
			} else if (i_13_ > aFloat4437 && aFloat4437 < i_14_) {
				i_9_ |= 0x8;
			}
			return i_9_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.JA(" + i + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	@Override
	public final int M() {
		try {
			int i = anInt4466;
			anInt4466 = 0;
			return i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.M(" + ')');
		}
	}

	@Override
	public final int mergeFunctionMask(int i, int i_316_) {
		try {
			return i | i_316_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.HF(" + i + ',' + i_316_ + ')');
		}
	}

	@Override
	public final DepthBufferObject method1744(int i, int i_40_) {
		try {
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.SB(" + i + ',' + i_40_ + ')');
		}
	}

	@Override
	public final void method1746(int i, int i_238_, int i_239_, int i_240_) {
		try {
			aClass283_4312.method3345(i_239_, i_238_, i_240_, i, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.LD(" + i + ',' + i_238_ + ',' + i_239_ + ',' + i_240_ + ')');
		}
	}

	@Override
	public final boolean method1747() {
		try {
			return effectManager.method507(3, -6634);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.HJ(" + ')');
		}
	}

	@Override
	public final void method1749(boolean bool) {
		/* empty */
	}

	@Override
	public final void method1751(int i, int i_338_, int i_339_, int i_340_, int i_341_, int i_342_, int i_343_, int i_344_, int i_345_, int i_346_, int i_347_, int i_348_, int i_349_) {
		try {
			method1859(-100);
			setBlendMode((byte) -127, i_349_);
			OpenGL.glBegin(4);
			OpenGL.glColor4ub((byte) (i_346_ >> 1405670544), (byte) (i_346_ >> 847872872), (byte) i_346_, (byte) (i_346_ >> -567715176));
			OpenGL.glVertex3f(i + 0.35F, 0.35F + i_338_, i_339_);
			OpenGL.glColor4ub((byte) (i_347_ >> 1700327632), (byte) (i_347_ >> 601299240), (byte) i_347_, (byte) (i_347_ >> 840925496));
			OpenGL.glVertex3f(i_340_ + 0.35F, i_341_ + 0.35F, i_342_);
			OpenGL.glColor4ub((byte) (i_348_ >> -2126717264), (byte) (i_348_ >> 984802728), (byte) i_348_, (byte) (i_348_ >> -382114632));
			OpenGL.glVertex3f(i_343_ + 0.35F, 0.35F + i_344_, i_345_);
			OpenGL.glEnd();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.OH(" + i + ',' + i_338_ + ',' + i_339_ + ',' + i_340_ + ',' + i_341_ + ',' + i_342_ + ',' + i_343_ + ',' + i_344_ + ',' + i_345_ + ',' + i_346_ + ',' + i_347_ + ',' + i_348_ + ',' + i_349_ + ')');
		}
	}

	@Override
	public final Matrix method1752() {
		try {
			return aClass111_Sub1_4348;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.NE(" + ')');
		}
	}

	@Override
	public final void method1756() {
		do {
			try {
				if (aClass98_Sub28_Sub1_4311 == null || !aClass98_Sub28_Sub1_4311.method1300(0)) {
					break;
				}
				aClass283_4312.method3341(aClass98_Sub28_Sub1_4311, -17722);
				texFactory.method3934((byte) 100);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.PC(" + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method1761(boolean bool) {
		/* empty */
	}

	@Override
	public final void method1764(int i, int i_191_) throws Exception_Sub1 {
		try {
			try {
				gl.swapBuffers();
			} catch (Exception exception) {
				/* empty */
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.BB(" + i + ',' + i_191_ + ')');
		}
	}

	@Override
	public final boolean method1766() {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.NG(" + ')');
		}
	}

	@Override
	public final boolean method1767() {
		try {
			return !(aClass98_Sub28_Sub1_4311 == null || antiAliasCount > 1 && !aBoolean4450);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.JF(" + ')');
		}
	}

	@Override
	public final Class48 method1769(Class48 class48, Class48 class48_156_, float f, Class48 class48_157_) {
		try {
			if (class48 != null && class48_156_ != null && aBoolean4391 && haveExtFrameBufferObject) {
				Class48_Sub1_Sub2 class48_sub1_sub2 = null;
				Class48_Sub1 class48_sub1 = (Class48_Sub1) class48;
				Class48_Sub1 class48_sub1_158_ = (Class48_Sub1) class48_156_;
				Class42_Sub2 class42_sub2 = class48_sub1.method456((byte) 122);
				Class42_Sub2 class42_sub2_159_ = class48_sub1_158_.method456((byte) 123);
				if (class42_sub2 != null && class42_sub2_159_ != null) {
					int i = class42_sub2.anInt5357 <= class42_sub2_159_.anInt5357 ? class42_sub2_159_.anInt5357 : class42_sub2.anInt5357;
					if (class48_157_ != class48 && class48_156_ != class48_157_ && class48_157_ instanceof Class48_Sub1_Sub2) {
						Class48_Sub1_Sub2 class48_sub1_sub2_160_ = (Class48_Sub1_Sub2) class48_157_;
						if ((i ^ 0xffffffff) == (class48_sub1_sub2_160_.method465(-65534) ^ 0xffffffff)) {
							class48_sub1_sub2 = class48_sub1_sub2_160_;
						}
					}
					if (class48_sub1_sub2 == null) {
						class48_sub1_sub2 = new Class48_Sub1_Sub2(this, i);
					}
					if (class48_sub1_sub2.method464((byte) -21, class42_sub2, f, class42_sub2_159_)) {
						return class48_sub1_sub2;
					}
				}
			}
			if (!(f < 0.5F)) {
				return class48_156_;
			}
			return class48;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.UC(" + (class48 != null ? "{...}" : "null") + ',' + (class48_156_ != null ? "{...}" : "null") + ',' + f + ',' + (class48_157_ != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final boolean method1771() {
		try {
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.VG(" + ')');
		}
	}

	@Override
	public final RtInterfaceClip method1772(int i, int i_162_, int[] is, int[] is_163_) {
		try {
			return InputStream_Sub2.method123(i_162_, is, this, i, is_163_, (byte) 111);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.AG(" + i + ',' + i_162_ + ',' + (is != null ? "{...}" : "null") + ',' + (is_163_ != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method1773() {
		do {
			try {
				for (Node class98 = aClass148_4320.getFirst(32); class98 != null; class98 = aClass148_4320.getNext(117)) {
					((OpenGLHeap) class98).deallocate((byte) 36);
				}
				if (aClass283_4312 != null) {
					aClass283_4312.method3346((byte) -127);
				}
				if (gl != null) {
					method1836(-513);
					Enumeration enumeration = aHashtable4306.keys();
					while (enumeration.hasMoreElements()) {
						Canvas canvas = (Canvas) enumeration.nextElement();
						Long var_long = (Long) aHashtable4306.get(canvas);
						gl.releaseSurface(canvas, var_long.longValue());
					}
					gl.release();
					gl = null;
				}
				if (!aBoolean4317) {
					break;
				}
				Class18.method248(true, 75, false);
				aBoolean4317 = false;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.AB(" + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method1774(int i) {
		try {
			method1836(-513);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.IG(" + i + ')');
		}
	}

	@Override
	public final void method1775(Class48 class48) {
		try {
			aClass48_Sub1_4443 = (Class48_Sub1) class48;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.MH(" + (class48 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method1778(int i) {
		try {
			if (i < 128 || (i ^ 0xffffffff) < -1025) {
				throw new IllegalArgumentException();
			}
			anInt4309 = i;
			texFactory.method3934((byte) 100);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.CH(" + i + ')');
		}
	}

	@Override
	public final boolean method1780() {
		try {
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.FG(" + ')');
		}
	}

	@Override
	public final void method1785(Class242 class242, int i) {
		try {
			aClass360_4313.method3906(class242, -104, i, this);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.CG(" + (class242 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final void method1786(Canvas canvas) {
		do {
			try {
				if (defaultCanvas == canvas) {
					throw new RuntimeException();
				}
				if (!aHashtable4306.containsKey(canvas)) {
					break;
				}
				Long var_long = (Long) aHashtable4306.get(canvas);
				gl.releaseSurface(canvas, var_long.longValue());
				aHashtable4306.remove(canvas);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.KG(" + (canvas != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final boolean method1788() {
		try {
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.KI(" + ')');
		}
	}

	@Override
	public final void method1791(float f, float f_281_, float f_282_) {
		try {
			Class246_Sub3_Sub3_Sub1.aFloat6257 = f;
			PlayerAppearence.aFloat2680 = f_282_;
			Billboard.aFloat1378 = f_281_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.TD(" + f + ',' + f_281_ + ',' + f_282_ + ')');
		}
	}

	@Override
	public final Matrix method1793() {
		try {
			return aClass111_Sub1_4315;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.RI(" + ')');
		}
	}

	@Override
	public final void method1795(int i, int i_168_, int i_169_, int i_170_, int i_171_, int i_172_) {
		try {
			method1859(-89);
			setBlendMode((byte) -88, i_172_);
			float f = (float) -i + (float) i_169_;
			float f_173_ = (float) i_170_ - (float) i_168_;
			do {
				if (f != 0.0F || f_173_ != 0.0F) {
					float f_174_ = (float) (1.0 / Math.sqrt(f * f + f_173_ * f_173_));
					f_173_ *= f_174_;
					f *= f_174_;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				f = 1.0F;
			} while (false);
			OpenGL.glColor4ub((byte) (i_171_ >> 968665392), (byte) (i_171_ >> 493545704), (byte) i_171_, (byte) (i_171_ >> 804890104));
			OpenGL.glBegin(1);
			OpenGL.glVertex2f(i + 0.35F, 0.35F + i_168_);
			OpenGL.glVertex2f(f + i_169_ + 0.35F, 0.35F + (f_173_ + i_170_));
			OpenGL.glEnd();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.PG(" + i + ',' + i_168_ + ',' + i_169_ + ',' + i_170_ + ',' + i_171_ + ',' + i_172_ + ')');
		}
	}

	@Override
	public final Sprite method1797(int i, int i_185_, int i_186_, int i_187_, boolean bool) {
		try {
			return new Class332_Sub1(this, i, i_185_, i_186_, i_187_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.MG(" + i + ',' + i_185_ + ',' + i_186_ + ',' + i_187_ + ',' + bool + ')');
		}
	}

	@Override
	public final void method1798(int i) {
		/* empty */
	}

	@Override
	public final Class62 method1799() {
		try {
			int i = -1;
			if (glVendor.indexOf("nvidia") != -1) {
				i = 4318;
			} else if ((glVendor.indexOf("intel") ^ 0xffffffff) != 0) {
				i = 32902;
			} else if (glVendor.indexOf("ati") != -1) {
				i = 4098;
			}
			return new Class62(i, "OpenGL", anInt4390, glRenderer, 0L);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.RB(" + ')');
		}
	}

	@Override
	public final boolean method1800() {
		try {
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.RE(" + ')');
		}
	}

	@Override
	public final void method1801(int[] is) {
		try {
			is[0] = width;
			is[1] = height;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.UD(" + (is != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final boolean method1802() {
		try {
			if (aClass98_Sub28_Sub1_4311 != null) {
				do {
					if (!aClass98_Sub28_Sub1_4311.method1300(0)) {
						if (aClass283_4312.method3344(aClass98_Sub28_Sub1_4311, -47)) {
							texFactory.method3934((byte) 100);
							if (!GameShell.cleanedStatics) {
								break;
							}
						}
						return false;
					}
				} while (false);
				return true;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.FB(" + ')');
		}
	}

	@Override
	public final Class48 method1803(int i, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_) {
		try {
			if (!aBoolean4391) {
				return null;
			}
			return new Class48_Sub1_Sub1(this, i, i_18_, i_19_, i_20_, i_21_, i_22_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.II(" + i + ',' + i_18_ + ',' + i_19_ + ',' + i_20_ + ',' + i_21_ + ',' + i_22_ + ')');
		}
	}

	@Override
	public final synchronized void method1806(int i) {
		try {
			int i_112_ = 0;
			i &= 0x7fffffff;
			while (!aClass148_4338.method2420(-124)) {
				NodeInteger class98_sub34 = (NodeInteger) aClass148_4338.removeFirst(6494);
				Class255.anIntArray3210[i_112_++] = (int) class98_sub34.hash;
				anInt4336 -= class98_sub34.value;
				if (i_112_ == 1000) {
					OpenGL.glDeleteBuffersARB(i_112_, Class255.anIntArray3210, 0);
					i_112_ = 0;
				}
			}
			if ((i_112_ ^ 0xffffffff) < -1) {
				OpenGL.glDeleteBuffersARB(i_112_, Class255.anIntArray3210, 0);
				i_112_ = 0;
			}
			while (!aClass148_4339.method2420(-125)) {
				NodeInteger class98_sub34 = (NodeInteger) aClass148_4339.removeFirst(6494);
				Class255.anIntArray3210[i_112_++] = (int) class98_sub34.hash;
				anInt4337 -= class98_sub34.value;
				if ((i_112_ ^ 0xffffffff) == -1001) {
					OpenGL.glDeleteTextures(i_112_, Class255.anIntArray3210, 0);
					i_112_ = 0;
				}
			}
			if (i_112_ > 0) {
				OpenGL.glDeleteTextures(i_112_, Class255.anIntArray3210, 0);
				i_112_ = 0;
			}
			while (!aClass148_4340.method2420(-124)) {
				NodeInteger class98_sub34 = (NodeInteger) aClass148_4340.removeFirst(6494);
				Class255.anIntArray3210[i_112_++] = class98_sub34.value;
				if (i_112_ == 1000) {
					OpenGL.glDeleteFramebuffersEXT(i_112_, Class255.anIntArray3210, 0);
					i_112_ = 0;
				}
			}
			if ((i_112_ ^ 0xffffffff) < -1) {
				OpenGL.glDeleteFramebuffersEXT(i_112_, Class255.anIntArray3210, 0);
				i_112_ = 0;
			}
			while (!aClass148_4341.method2420(-125)) {
				NodeInteger class98_sub34 = (NodeInteger) aClass148_4341.removeFirst(6494);
				Class255.anIntArray3210[i_112_++] = (int) class98_sub34.hash;
				anInt4335 -= class98_sub34.value;
				if (i_112_ == 1000) {
					OpenGL.glDeleteRenderbuffersEXT(i_112_, Class255.anIntArray3210, 0);
					i_112_ = 0;
				}
			}
			if ((i_112_ ^ 0xffffffff) < -1) {
				OpenGL.glDeleteRenderbuffersEXT(i_112_, Class255.anIntArray3210, 0);
			}
			while (!aClass148_4334.method2420(-126)) {
				NodeInteger class98_sub34 = (NodeInteger) aClass148_4334.removeFirst(6494);
				OpenGL.glDeleteLists((int) class98_sub34.hash, class98_sub34.value);
			}
			while (!aClass148_4342.method2420(-126)) {
				Node class98 = aClass148_4342.removeFirst(6494);
				OpenGL.glDeleteProgramARB((int) class98.hash);
			}
			while (!aClass148_4343.method2420(-126)) {
				Node class98 = aClass148_4343.removeFirst(6494);
				OpenGL.glDeleteObjectARB(class98.hash);
			}
			while (!aClass148_4334.method2420(-125)) {
				NodeInteger class98_sub34 = (NodeInteger) aClass148_4334.removeFirst(6494);
				OpenGL.glDeleteLists((int) class98_sub34.hash, class98_sub34.value);
			}
			texFactory.method3933(0);
			if (E() > 100663296 && 60000L + aLong4351 < TimeTools.getCurrentTime(-47)) {
				System.gc();
				aLong4351 = TimeTools.getCurrentTime(-47);
			}
			anInt4321 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.UB(" + i + ')');
		}
	}

	@Override
	public final boolean method1810() {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.PH(" + ')');
		}
	}

	@Override
	public final void method1811(int i, int i_293_, int i_294_, int i_295_, int i_296_, int i_297_, int i_298_, int i_299_, int i_300_) {
		try {
			if ((i ^ 0xffffffff) != (i_294_ ^ 0xffffffff) || (i_295_ ^ 0xffffffff) != (i_293_ ^ 0xffffffff)) {
				method1859(-113);
				setBlendMode((byte) -126, i_297_);
				float f = (float) -i + (float) i_294_;
				float f_301_ = (float) -i_293_ + (float) i_295_;
				float f_302_ = (float) (1.0 / Math.sqrt(f_301_ * f_301_ + f * f));
				f_301_ *= f_302_;
				f *= f_302_;
				OpenGL.glColor4ub((byte) (i_296_ >> -84381840), (byte) (i_296_ >> -980470872), (byte) i_296_, (byte) (i_296_ >> 859356024));
				i_300_ %= i_299_ + i_298_;
				float f_303_ = f * i_298_;
				float f_304_ = f_301_ * i_298_;
				float f_305_ = 0.0F;
				float f_306_ = 0.0F;
				float f_307_ = f_303_;
				float f_308_ = f_304_;
				if (i_300_ <= i_298_) {
					f_308_ = f_301_ * (-i_300_ + i_298_);
					f_307_ = (-i_300_ + i_298_) * f;
				} else {
					f_305_ = f * (i_299_ + i_298_ + -i_300_);
					f_306_ = (-i_300_ + i_298_ + i_299_) * f_301_;
				}
				float f_309_ = i + 0.35F + f_305_;
				float f_310_ = f_306_ + (i_293_ + 0.35F);
				float f_311_ = i_299_ * f;
				float f_312_ = i_299_ * f_301_;
				for (;;) {
					if (i_294_ > i) {
						if (f_309_ > 0.35F + i_294_) {
							break;
						}
						if (i_294_ < f_309_ + f_307_) {
							f_307_ = -f_309_ + i_294_;
						}
					} else {
						if (0.35F + i_294_ > f_309_) {
							break;
						}
						if (i_294_ > f_309_ + f_307_) {
							f_307_ = -f_309_ + i_294_;
						}
					}
					if ((i_295_ ^ 0xffffffff) < (i_293_ ^ 0xffffffff)) {
						if (0.35F + i_295_ < f_310_) {
							break;
						}
						if (i_295_ < f_308_ + f_310_) {
							f_308_ = -f_310_ + i_295_;
						}
					} else {
						if (i_295_ + 0.35F > f_310_) {
							break;
						}
						if (f_310_ + f_308_ < i_295_) {
							f_308_ = -f_310_ + i_295_;
						}
					}
					OpenGL.glBegin(1);
					OpenGL.glVertex2f(f_309_, f_310_);
					OpenGL.glVertex2f(f_307_ + f_309_, f_308_ + f_310_);
					OpenGL.glEnd();
					f_310_ += f_312_ + f_308_;
					f_309_ += f_307_ + f_311_;
					f_307_ = f_303_;
					f_308_ = f_304_;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.CF(" + i + ',' + i_293_ + ',' + i_294_ + ',' + i_295_ + ',' + i_296_ + ',' + i_297_ + ',' + i_298_ + ',' + i_299_ + ',' + i_300_ + ')');
		}
	}

	@Override
	public final void method1812() {
		/* empty */
	}

	@Override
	public final Interface5 method1813(int i, int i_243_) {
		try {
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.GD(" + i + ',' + i_243_ + ')');
		}
	}

	@Override
	public final void method1814() {
		try {
			aClass283_4312.method3342(-121);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.RD(" + ')');
		}
	}

	@Override
	public final void method1816(int i, int i_64_, int i_65_, int i_66_, int i_67_, int i_68_, int i_69_) {
		try {
			OpenGL.glLineWidth(i_68_);
			method1795(i, i_64_, i_65_, i_66_, i_67_, i_69_);
			OpenGL.glLineWidth(1.0F);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.SH(" + i + ',' + i_64_ + ',' + i_65_ + ',' + i_66_ + ',' + i_67_ + ',' + i_68_ + ',' + i_69_ + ')');
		}
	}

	@Override
	public final void method1817() {
		try {
			if (isNotS3GfxCard && width > 0 && height > 0) {
				int i = anInt4370;
				int i_0_ = anInt4432;
				int i_1_ = anInt4402;
				int i_2_ = anInt4414;
				clearClip();
				OpenGL.glReadBuffer(1028);
				OpenGL.glDrawBuffer(1029);
				method1867(29458);
				method1856(false, 6914);
				method1851(false, false);
				method1881(false, false);
				method1875((byte) -124, false);
				setActiveTexture(1, null);
				method1834(115, -2);
				method1896(260, 1);
				setBlendMode((byte) -55, 0);
				OpenGL.glMatrixMode(5889);
				OpenGL.glLoadIdentity();
				OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
				OpenGL.glMatrixMode(5888);
				OpenGL.glLoadIdentity();
				OpenGL.glRasterPos2i(0, 0);
				OpenGL.glCopyPixels(0, 0, width, height, 6144);
				OpenGL.glFlush();
				OpenGL.glReadBuffer(1029);
				OpenGL.glDrawBuffer(1029);
				setClip(i, i_1_, i_0_, i_2_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.O(" + ')');
		}
	}

	@Override
	public final void method1818(int i, PointLight[] class98_sub5s) {
		do {
			try {
				for (int i_218_ = 0; i > i_218_; i_218_++) {
					aClass98_Sub5Array4408[i_218_] = class98_sub5s[i_218_];
				}
				anInt4384 = i;
				if (anInt4385 == 1) {
					break;
				}
				method1842(114);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.GF(" + i + ',' + (class98_sub5s != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final boolean method1819() {
		try {
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.KE(" + ')');
		}
	}

	@Override
	public final void method1820(Class242 class242) {
		try {
			aClass360_4313.method3906(class242, -114, -1, this);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.DE(" + (class242 != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public void method1820_cp(Class242 class242, ParticleDescriptor pDescriptor, int intensity, int ambient) {
		aClass360_4313.method3906_cp(class242, -114, -1, this, pDescriptor, intensity, ambient);
	}

	@Override
	public final int method1822() {
		try {
			return 4;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.LG(" + ')');
		}
	}

	@Override
	public final boolean method1823() {
		try {
			return !(!aBoolean4360 || canEnableBloom() && !aBoolean4450);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.AF(" + ')');
		}
	}

	@Override
	public final void method1825() {
		try {
			OpenGL.glFinish();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.TC(" + ')');
		}
	}

	private final void method1826(int i) {
		do {
			try {
				do {
					if (aBoolean4457 && !aBoolean4405) {
						OpenGL.glEnable(2896);
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					OpenGL.glDisable(2896);
				} while (false);
				if (i == 2896) {
					break;
				}
				anInt4402 = -77;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.RC(" + i + ')');
			}
			break;
		} while (false);
	}

	public final Class42_Sub2 method1827(int i) {
		try {
			if (i > -121) {
				return null;
			}
			if (aClass48_Sub1_4443 == null) {
				return null;
			}
			return aClass48_Sub1_4443.method456((byte) 121);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.OA(" + i + ')');
		}
	}

	private final void method1828(byte i) {
		try {
			do {
				if (anInt4370 > anInt4432 || (anInt4402 ^ 0xffffffff) < (anInt4414 ^ 0xffffffff)) {
					OpenGL.glScissor(0, 0, 0, 0);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				OpenGL.glScissor(anInt4415 + anInt4370, -anInt4414 + anInt4409 - -height, anInt4432 + -anInt4370, anInt4414 - anInt4402);
			} while (false);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.G(" + i + ')');
		}
	}

	public final void method1829(byte i) {
		do {
			try {
				if (i >= -78) {
					anInt4384 = -7;
				}
				if ((anInt4350 ^ 0xffffffff) == -3) {
					break;
				}
				method1877((byte) -60);
				method1856(false, 6914);
				method1851(false, false);
				method1881(false, false);
				method1875((byte) -123, false);
				method1834(-81, -2);
				anInt4350 = 2;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.OB(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1830(OpenGLElementBuffer interface8, int i) {
		do {
			try {
				if (i == 2936) {
					if (boundElementBuffer == interface8) {
						break;
					}
					if (haveArbVbo) {
						OpenGL.glBindBufferARB(34963, interface8.method19(-22132));
					}
					boundElementBuffer = interface8;
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.TA(" + (interface8 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1831(int i) {
		try {
			if (i <= 125) {
				underwater = false;
			}
			OpenGL.glLightfv(16384, 4611, sunDirection, 0);
			OpenGL.glLightfv(16385, 4611, aFloatArray4463, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.MA(" + i + ')');
		}
	}

	public final void method1832(OpenGLRenderTarget interface12, int i) {
		try {
			if (anInt4324 < 0 || interface12 != anInterface12Array4329[anInt4324]) {
				throw new RuntimeException();
			}
			anInterface12Array4329[anInt4324--] = null;
			if (i < 16) {
				anInt4390 = -56;
			}
			interface12.method38(-27095);
			if ((anInt4324 ^ 0xffffffff) <= -1) {
				anInterface12_4325 = anInterface12Array4329[anInt4324];
				anInterface12_4325.method37((byte) 77);
			} else {
				anInterface12_4325 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.CE(" + (interface12 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private final void method1833(int i) {
		do {
			try {
				OpenGL.glLoadIdentity();
				OpenGL.glMultMatrixf(aClass111_Sub1_4353.method2113(i + -114), 0);
				if (aBoolean4448) {
					effectManager.aClass151_Sub9_432.method2471((byte) 34);
				}
				method1831(127);
				method1842(103);
				if (i == 4) {
					break;
				}
				da(-81, 96, 24, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.ID(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1834(int i, int i_28_) {
		try {
			method1908(true, -109, i_28_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.HE(" + i + ',' + i_28_ + ')');
		}
	}

	public final void method1835(OpenGLRenderTarget interface12, int i) {
		try {
			if ((anInt4327 ^ 0xffffffff) <= -4) {
				throw new RuntimeException();
			}
			if (anInt4327 >= i) {
				anInterface12Array4326[anInt4327].method40((byte) -30);
			}
			anInterface12_4328 = anInterface12Array4326[++anInt4327] = interface12;
			anInterface12_4328.method36((byte) -120);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.KD(" + (interface12 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private final void method1836(int i) {
		try {
			if (i != -513) {
				r(-65, -25, -101, 48, -10, -24, -25);
			}
			gl.a();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.LA(" + i + ')');
		}
	}

	public final synchronized void method1837(byte i, int i_34_) {
		try {
			if (i <= -27) {
				NodeInteger class98_sub34 = new NodeInteger(i_34_);
				aClass148_4340.addLast(class98_sub34, -20911);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.DC(" + i + ',' + i_34_ + ')');
		}
	}

	public final OpenGLElementBuffer method1838(int i, byte[] is, int i_41_, boolean bool, int i_42_) {
		try {
			if (i_41_ != 7) {
				method1812();
			}
			if (haveArbVbo && (!bool || useArbVbo)) {
				return new Class287_Sub2(this, i, is, i_42_, bool);
			}
			return new NativeOpenGlElementArrayBuffer(this, i, is, i_42_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.PA(" + i + ',' + (is != null ? "{...}" : "null") + ',' + i_41_ + ',' + bool + ',' + i_42_ + ')');
		}
	}

	public final int method1839(int i, int i_43_) {
		try {
			if (i_43_ == (i ^ 0xffffffff) || i == 5120) {
				return 1;
			}
			if ((i ^ 0xffffffff) == -5124 || (i ^ 0xffffffff) == -5123) {
				return 2;
			}
			if ((i ^ 0xffffffff) == -5126 || i == 5124 || (i ^ 0xffffffff) == -5127) {
				return 4;
			}
			throw new IllegalArgumentException("");
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.RA(" + i + ',' + i_43_ + ')');
		}
	}

	public final void method1840(int i, int i_44_, int i_45_, int i_46_) {
		try {
			OpenGL.glTexEnvi(8960, 34176 + i, i_46_);
			OpenGL.glTexEnvi(8960, i + 34192, i_44_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.N(" + i + ',' + i_44_ + ',' + i_45_ + ',' + i_46_ + ')');
		}
	}

	public final void method1841(int i) {
		do {
			try {
				OpenGL.glPushMatrix();
				if (i == 34167) {
					break;
				}
				method1836(77);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.EB(" + i + ')');
			}
			break;
		} while (false);
	}

	private final void method1842(int i) {
		try {
			int i_56_;
			for (i_56_ = 0; anInt4384 > i_56_; i_56_++) {
				PointLight class98_sub5 = aClass98_Sub5Array4408[i_56_];
				int i_57_ = 16386 - -i_56_;
				NodeString.aFloatArray3916[0] = class98_sub5.getX(7019);
				NodeString.aFloatArray3916[1] = class98_sub5.getY((byte) 99);
				NodeString.aFloatArray3916[2] = class98_sub5.getZ(28699);
				NodeString.aFloatArray3916[3] = 1.0F;
				OpenGL.glLightfv(i_57_, 4611, NodeString.aFloatArray3916, 0);
				int i_58_ = class98_sub5.getColour((byte) -78);
				float f = class98_sub5.getIntensity(false) / 255.0F;
				NodeString.aFloatArray3916[1] = f * (Class202.and(i_58_, 65311) >> 1265447176);
				NodeString.aFloatArray3916[2] = Class202.and(i_58_, 255) * f;
				NodeString.aFloatArray3916[0] = f * Class202.and(255, i_58_ >> 411401808);
				OpenGL.glLightfv(i_57_, 4609, NodeString.aFloatArray3916, 0);
				OpenGL.glLightf(i_57_, 4617, 1.0F / (class98_sub5.getRange(-76) * class98_sub5.getRange(-105)));
				OpenGL.glEnable(i_57_);
			}
			if (i <= 13) {
				anInt4404 = -99;
			}
			for (/**/; i_56_ < anInt4411; i_56_++) {
				OpenGL.glDisable(16386 + i_56_);
			}
			anInt4411 = anInt4384;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.WB(" + i + ')');
		}
	}

	public final ArrayBuffer createArrayBuffer(int i, byte i_59_, Buffer buffer, boolean bool, int i_60_) {
		try {
			if (haveArbVbo && (!bool || useArbVbo)) {
				return new Class287_Sub1(this, i, buffer, i_60_, bool);
			}
			if (i_59_ < 49) {
				return null;
			}
			return new Class156_Sub2(this, i, buffer);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.FE(" + i + ',' + i_59_ + ',' + (buffer != null ? "{...}" : "null") + ',' + bool + ',' + i_60_ + ')');
		}
	}

	private final void method1844(byte i) {
		try {
			OpenGL.glViewport(anInt4415, anInt4409, width, height);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.WD(" + i + ')');
		}
	}

	public final void method1845(int i, int i_73_) {
		do {
			try {
				if (i_73_ != 847872872) {
					method1845(108, -11);
				}
				if (anInt4417 == i) {
					break;
				}
				OpenGL.glActiveTexture(i + 33984);
				anInt4417 = i;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.QA(" + i + ',' + i_73_ + ')');
			}
			break;
		} while (false);
	}

	public final synchronized void method1846(int i, int i_74_, int i_75_) {
		try {
			NodeInteger class98_sub34 = new NodeInteger(i_75_);
			class98_sub34.hash = i;
			if (i_74_ <= 36) {
				anInt4324 = -29;
			}
			aClass148_4341.addLast(class98_sub34, -20911);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.KC(" + i + ',' + i_74_ + ',' + i_75_ + ')');
		}
	}

	private final void method1847(boolean bool) {
		try {
			float[] fs = aFloatArray4440;
			if (bool != true) {
				method1880(42, null);
			}
			float f = (float) (-anInt4451 * anInt4404) / (float) anInt4419;
			float f_76_ = (float) (anInt4404 * (-anInt4451 + width)) / (float) anInt4419;
			float f_77_ = (float) (anInt4394 * anInt4404) / (float) anInt4381;
			float f_78_ = (float) ((-height + anInt4394) * anInt4404) / (float) anInt4381;
			do {
				if (f == f_76_ || f_78_ == f_77_) {
					fs[6] = 0.0F;
					fs[4] = 0.0F;
					fs[14] = 0.0F;
					fs[15] = 1.0F;
					fs[0] = 1.0F;
					fs[9] = 0.0F;
					fs[2] = 0.0F;
					fs[8] = 0.0F;
					fs[10] = 1.0F;
					fs[12] = 0.0F;
					fs[7] = 0.0F;
					fs[11] = 0.0F;
					fs[3] = 0.0F;
					fs[5] = 1.0F;
					fs[13] = 0.0F;
					fs[1] = 0.0F;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				float f_79_ = 2.0F * anInt4404;
				fs[5] = f_79_ / (f_77_ - f_78_);
				fs[14] = aFloat4416 = -(f_79_ * anInt4389) / (anInt4389 - anInt4404);
				fs[11] = -1.0F;
				fs[1] = 0.0F;
				fs[9] = (f_77_ + f_78_) / (-f_78_ + f_77_);
				fs[0] = f_79_ / (f_76_ - f);
				fs[8] = (f + f_76_) / (f_76_ - f);
				fs[3] = 0.0F;
				fs[4] = 0.0F;
				fs[12] = 0.0F;
				fs[7] = 0.0F;
				fs[15] = 0.0F;
				fs[10] = aFloat4371 = (float) -(anInt4404 + anInt4389) / (float) (anInt4389 - anInt4404);
				fs[2] = 0.0F;
				fs[6] = 0.0F;
				fs[13] = 0.0F;
			} while (false);
			method1894((byte) -104);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.IC(" + bool + ')');
		}
	}

	public final void method1848(float f, boolean bool, float f_92_) {
		try {
			aFloat4392 = f;
			aFloat4376 = f_92_;
			if (bool != true) {
				anInt4336 = 110;
			}
			method1852((byte) -91);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.IB(" + f + ',' + bool + ',' + f_92_ + ')');
		}
	}

	private final void method1850(byte i) {
		try {
			do {
				if (activeCanvas != null) {
					Dimension dimension = activeCanvas.getSize();
					anInt4297 = dimension.height;
					anInt4301 = dimension.width;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				anInt4301 = anInt4297 = 0;
			} while (false);
			if (anInterface12_4328 == null) {
				width = anInt4301;
				height = anInt4297;
				method1844((byte) 113);
			}
			method1867(i + 29489);
			if (i == -31) {
				clearClip();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.PB(" + i + ')');
		}
	}

	public final void method1851(boolean bool, boolean bool_111_) {
		try {
			if (bool_111_ != false) {
				this.createSprite(38, -93, true);
			}
			if (bool == !aBoolean4457) {
				aBoolean4457 = bool;
				method1826(2896);
				anInt4350 &= ~0x7;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.NC(" + bool + ',' + bool_111_ + ')');
		}
	}

	private final void method1852(byte i) {
		try {
			aFloat4380 = anInt4389 - anInt4427 - aFloat4376;
			aFloat4356 = aFloat4380 - aFloat4392 * fogDistance;
			if (aFloat4356 < anInt4404) {
				aFloat4356 = anInt4404;
			}
			if (i >= -57) {
				aBoolean4345 = true;
			}
			OpenGL.glFogf(2915, 13820);
			OpenGL.glFogf(2916, 14844);
			SunDefinitionParser.aFloatArray961[0] = Class202.and(anInt4455, 16711680) / 1.671168E7F;
			SunDefinitionParser.aFloatArray961[2] = Class202.and(255, anInt4455) / 255.0F;
			SunDefinitionParser.aFloatArray961[1] = Class202.and(anInt4455, 65280) / 65280.0F;
			OpenGL.glFogfv(2918, SunDefinitionParser.aFloatArray961, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.HD(" + i + ')');
		}
	}

	public final synchronized void method1855(boolean bool, long l) {
		try {
			if (bool != false) {
				this.setRenderTarget(null);
			}
			Node class98 = new Node();
			class98.hash = l;
			aClass148_4343.addLast(class98, -20911);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.PD(" + bool + ',' + l + ')');
		}
	}

	public final void method1856(boolean bool, int i) {
		do {
			try {
				if (i != 6914) {
					anInt4411 = -103;
				}
				if (hasFog != !bool) {
					break;
				}
				hasFog = bool;
				method1893(115);
				anInt4350 &= ~0x1f;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.BA(" + bool + ',' + i + ')');
			}
			break;
		} while (false);
	}

	private final void method1857(float f, float f_126_, byte i, float f_127_) {
		do {
			try {
				OpenGL.glMatrixMode(5890);
				if (aBoolean4422) {
					OpenGL.glLoadIdentity();
				}
				OpenGL.glTranslatef(f, f_127_, f_126_);
				OpenGL.glMatrixMode(5888);
				aBoolean4422 = true;
				if (i == 44) {
					break;
				}
				anInterface16_4374 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.FD(" + f + ',' + f_126_ + ',' + i + ',' + f_127_ + ')');
			}
			break;
		} while (false);
	}

	public final void method1858(float f, float f_128_, float f_129_, float f_130_, int i) {
		try {
			SunDefinitionParser.aFloatArray961[1] = f;
			SunDefinitionParser.aFloatArray961[3] = f_130_;
			SunDefinitionParser.aFloatArray961[2] = f_128_;
			SunDefinitionParser.aFloatArray961[0] = f_129_;
			OpenGL.glTexEnvfv(8960, 8705, SunDefinitionParser.aFloatArray961, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.GB(" + f + ',' + f_128_ + ',' + f_129_ + ',' + f_130_ + ',' + i + ')');
		}
	}

	private final void method1859(int i) {
		do {
			try {
				if (i > -5) {
					method1818(-116, null);
				}
				if (anInt4350 == 1) {
					break;
				}
				method1877((byte) -35);
				method1856(false, 6914);
				method1851(false, false);
				method1881(false, false);
				method1875((byte) -126, false);
				setActiveTexture(1, null);
				method1834(-105, -2);
				method1896(260, 1);
				anInt4350 = 1;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.LC(" + i + ')');
			}
			break;
		} while (false);
	}

	private final void method1860(byte i) {
		try {
			SunDefinitionParser.aFloatArray961[1] = aFloat4420 * ambient;
			SunDefinitionParser.aFloatArray961[0] = ambient * aFloat4433;
			SunDefinitionParser.aFloatArray961[3] = 1.0F;
			if (i != -41) {
				M();
			}
			SunDefinitionParser.aFloatArray961[2] = ambient * aFloat4458;
			OpenGL.glLightModelfv(2899, SunDefinitionParser.aFloatArray961, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.DB(" + i + ')');
		}
	}

	public final void method1861(int i) {
		do {
			try {
				if (i != 19330) {
					aClass146_Sub2_4361 = null;
				}
				if ((anInt4350 ^ 0xffffffff) == -9) {
					break;
				}
				method1904(-22);
				method1856(true, 6914);
				method1881(true, false);
				method1875((byte) 109, true);
				setBlendMode((byte) -68, 1);
				anInt4350 = 8;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.S(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void setActiveTexture(int i, Class42 class42) {
		try {
			if (i == 1) {
				Class42 class42_161_ = aClass42Array4396[anInt4417];
				if (class42 != class42_161_) {
					if (class42 != null) {
						if (class42_161_ == null) {
							OpenGL.glEnable(class42.anInt3226);
						} else if ((class42.anInt3226 ^ 0xffffffff) != (class42_161_.anInt3226 ^ 0xffffffff)) {
							OpenGL.glDisable(class42_161_.anInt3226);
							OpenGL.glEnable(class42.anInt3226);
						}
						OpenGL.glBindTexture(class42.anInt3226, class42.method369(true));
					} else {
						OpenGL.glDisable(class42_161_.anInt3226);
					}
					aClass42Array4396[anInt4417] = class42;
				}
				anInt4350 &= ~0x1;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.JE(" + i + ',' + (class42 != null ? "{...}" : "null") + ')');
		}
	}

	private final void method1864(int i) {
		try {
			method1834(102, -2);
			for (int i_164_ = -1 + anInt4410; i_164_ >= 0; i_164_--) {
				method1845(i_164_, i + 847872904);
				setActiveTexture(1, null);
				OpenGL.glTexEnvi(8960, 8704, 34160);
			}
			method1899(8448, 8960, 8448);
			method1840(2, 770, i + 128, 34168);
			method1891(370914608);
			anInt4347 = 1;
			OpenGL.glEnable(3042);
			OpenGL.glBlendFunc(770, 771);
			anInt4344 = 1;
			OpenGL.glEnable(3008);
			OpenGL.glAlphaFunc(516, 0.0F);
			aBoolean4345 = true;
			OpenGL.glColorMask(true, true, true, true);
			if (i == -32) {
				aBoolean4346 = true;
				method1856(true, 6914);
				method1851(true, false);
				method1881(true, false);
				method1875((byte) 113, true);
				method1867(i ^ ~0x730d);
				gl.setSwapInterval(0);
				OpenGL.glShadeModel(7425);
				OpenGL.glClearDepth(1.0F);
				OpenGL.glDepthFunc(515);
				OpenGL.glPolygonMode(1028, 6914);
				OpenGL.glEnable(2884);
				OpenGL.glCullFace(1029);
				OpenGL.glMatrixMode(5888);
				OpenGL.glLoadIdentity();
				OpenGL.glColorMaterial(1028, 5634);
				OpenGL.glEnable(2903);
				float[] fs = { 0.0F, 0.0F, 0.0F, 1.0F };
				for (int i_165_ = 0; i_165_ < 8; i_165_++) {
					int i_166_ = i_165_ + 16384;
					OpenGL.glLightfv(i_166_, 4608, fs, 0);
					OpenGL.glLightf(i_166_, 4615, 0.0F);
					OpenGL.glLightf(i_166_, 4616, 0.0F);
				}
				OpenGL.glEnable(16384);
				OpenGL.glEnable(16385);
				OpenGL.glFogf(2914, 0.95F);
				OpenGL.glFogi(2917, 9729);
				OpenGL.glHint(3156, 4353);
				anInt4412 = anInt4455 = -1;
				clearClip();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.FA(" + i + ')');
		}
	}

	public final void drawElements(int i, int i_175_, OpenGLElementBuffer interface8, boolean bool, int i_176_) {
		try {
			if (bool != false) {
				method1872(-128, 67);
			}
			int i_177_ = interface8.method21(5061);
			i_176_ *= method1839(i_177_, -5122);
			method1830(interface8, 2936);
			OpenGL.glDrawElements(i_175_, i, i_177_, i_176_ + interface8.method22(20260));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.VC(" + i + ',' + i_175_ + ',' + (interface8 != null ? "{...}" : "null") + ',' + bool + ',' + i_176_ + ')');
		}
	}

	public final int method1866(int i, int i_178_) {
		try {
			if (i >= -73) {
				method1822();
			}
			if (i_178_ != 6406 && (i_178_ ^ 0xffffffff) != -6410) {
				if ((i_178_ ^ 0xffffffff) != -6411 && (i_178_ ^ 0xffffffff) != -34847 && (i_178_ ^ 0xffffffff) != -34845) {
					if (i_178_ == 6407) {
						return 3;
					}
					if ((i_178_ ^ 0xffffffff) != -6409 && i_178_ != 34847) {
						if ((i_178_ ^ 0xffffffff) == -34844) {
							return 6;
						}
						if ((i_178_ ^ 0xffffffff) != -34843) {
							if ((i_178_ ^ 0xffffffff) == -6403) {
								return 3;
							}
							if (i_178_ == 6401) {
								return 1;
							}
						} else {
							return 8;
						}
					} else {
						return 4;
					}
				} else {
					return 2;
				}
			} else {
				return 1;
			}
			throw new IllegalArgumentException("");
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.J(" + i + ',' + i_178_ + ')');
		}
	}

	public final void method1867(int i) {
		try {
			if (i != 29458) {
				method1893(72);
			}
			if (anInt4385 != 0) {
				anInt4385 = 0;
				anInt4350 &= ~0x1f;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.MD(" + i + ')');
		}
	}

	public final void setPointers(OpenGlArrayBufferPointer class104, OpenGlArrayBufferPointer class104_179_, OpenGlArrayBufferPointer class104_180_, OpenGlArrayBufferPointer class104_181_, int i) {
		try {
			if (class104_181_ != null) {
				method1887(class104_181_.buffer, 34962);
				OpenGL.glVertexPointer(class104_181_.aByte900, class104_181_.type, anInterface16_4374.method55(-115), anInterface16_4374.method52(24582) - -(long) class104_181_.aByte898);
				OpenGL.glEnableClientState(32884);
			} else {
				OpenGL.glDisableClientState(32884);
			}
			if (i != 0) {
				F(-106, -93);
			}
			if (class104_179_ == null) {
				OpenGL.glDisableClientState(32885);
			} else {
				method1887(class104_179_.buffer, 34962);
				OpenGL.glNormalPointer(class104_179_.type, anInterface16_4374.method55(-122), anInterface16_4374.method52(24582) - -(long) class104_179_.aByte898);
				OpenGL.glEnableClientState(32885);
			}
			if (class104 == null) {
				OpenGL.glDisableClientState(32886);
			} else {
				method1887(class104.buffer, 34962);
				OpenGL.glColorPointer(class104.aByte900, class104.type, anInterface16_4374.method55(49), anInterface16_4374.method52(24582) - -(long) class104.aByte898);
				OpenGL.glEnableClientState(32886);
			}
			if (class104_180_ == null) {
				OpenGL.glDisableClientState(32888);
			} else {
				method1887(class104_180_.buffer, 34962);
				OpenGL.glTexCoordPointer(class104_180_.aByte900, class104_180_.type, anInterface16_4374.method55(79), anInterface16_4374.method52(24582) - -(long) class104_180_.aByte898);
				OpenGL.glEnableClientState(32888);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.SD(" + (class104 != null ? "{...}" : "null") + ',' + (class104_179_ != null ? "{...}" : "null") + ',' + (class104_180_ != null ? "{...}" : "null") + ',' + (class104_181_ != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final void method1869(int i, OpenGLRenderTarget interface12) {
		try {
			if (anInt4327 < i || interface12 != anInterface12Array4326[anInt4327]) {
				throw new RuntimeException();
			}
			anInterface12Array4326[anInt4327--] = null;
			interface12.method40((byte) -30);
			if ((anInt4327 ^ 0xffffffff) <= -1) {
				anInterface12_4328 = anInterface12Array4326[anInt4327];
				anInterface12_4328.method36((byte) -115);
			} else {
				anInterface12_4328 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.UA(" + i + ',' + (interface12 != null ? "{...}" : "null") + ')');
		}
	}

	public final void setBlendMode(byte i, int i_188_) {
		do {
			try {
				if (i > -18) {
					anInt4337 = 47;
				}
				if (i_188_ == anInt4347) {
					break;
				}
				boolean bool;
				boolean bool_189_;
				int i_190_;
				if ((i_188_ ^ 0xffffffff) == -2) {
					bool_189_ = true;
					bool = true;
					i_190_ = 1;
				} else if (i_188_ == 2) {
					bool = true;
					bool_189_ = false;
					i_190_ = 2;
				} else if (i_188_ == 128) {
					bool_189_ = true;
					bool = true;
					i_190_ = 3;
				} else {
					i_190_ = 0;
					bool = true;
					bool_189_ = false;
				}
				if (bool == !aBoolean4346) {
					OpenGL.glColorMask(bool, bool, bool, true);
					aBoolean4346 = bool;
				}
				if (aBoolean4345 != bool_189_) {
					if (!bool_189_) {
						OpenGL.glDisable(3008);
					} else {
						OpenGL.glEnable(3008);
					}
					aBoolean4345 = bool_189_;
				}
				if ((i_190_ ^ 0xffffffff) != (anInt4344 ^ 0xffffffff)) {
					if ((i_190_ ^ 0xffffffff) != -2) {
						if (i_190_ == 2) {
							OpenGL.glEnable(3042);
							OpenGL.glBlendFunc(1, 1);
						} else if (i_190_ == 3) {
							OpenGL.glEnable(3042);
							OpenGL.glBlendFunc(774, 1);
						} else {
							OpenGL.glDisable(3042);
						}
					} else {
						OpenGL.glEnable(3042);
						OpenGL.glBlendFunc(770, 771);
					}
					anInt4344 = i_190_;
				}
				anInt4347 = i_188_;
				anInt4350 &= ~0x1c;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.LE(" + i + ',' + i_188_ + ')');
			}
			break;
		} while (false);
	}

	public final synchronized void method1872(int i, int i_209_) {
		try {
			Node class98 = new Node();
			if (i_209_ != 2834) {
				method1839(59, 97);
			}
			class98.hash = i;
			aClass148_4342.addLast(class98, i_209_ ^ ~0x5abc);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.NA(" + i + ',' + i_209_ + ')');
		}
	}

	public final synchronized void method1873(int i, int i_210_, int i_211_) {
		try {
			NodeInteger class98_sub34 = new NodeInteger(i);
			class98_sub34.hash = i_211_;
			if (i_210_ != 4) {
				anInt4409 = -76;
			}
			aClass148_4339.addLast(class98_sub34, -20911);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.MB(" + i + ',' + i_210_ + ',' + i_211_ + ')');
		}
	}

	private final void method1874(int i) {
		do {
			try {
				if (i != -30) {
					method1831(-70);
				}
				if (anInt4385 == 3) {
					break;
				}
				anInt4385 = 3;
				method1884((byte) 78);
				method1833(4);
				anInt4350 &= ~0x7;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.ND(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1875(byte i, boolean bool) {
		try {
			if (!aBoolean4349 == bool) {
				aBoolean4349 = bool;
				method1876((byte) -75);
				anInt4350 &= ~0x1f;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.EE(" + i + ',' + bool + ')');
		}
	}

	private final void method1876(byte i) {
		try {
			if (i != -75) {
				useArbVbo = false;
			}
			OpenGL.glDepthMask(aBoolean4349 && aBoolean4465);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.W(" + i + ')');
		}
	}

	private final void method1877(byte i) {
		try {
			if (i < -33) {
				if ((anInt4385 ^ 0xffffffff) != -2) {
					OpenGL.glMatrixMode(5889);
					OpenGL.glLoadIdentity();
					if ((width ^ 0xffffffff) < -1 && (height ^ 0xffffffff) < -1) {
						OpenGL.glOrtho(0.0, width, height, 0.0, -1.0, 1.0);
					}
					OpenGL.glMatrixMode(5888);
					OpenGL.glLoadIdentity();
					anInt4385 = 1;
					anInt4350 &= ~0x18;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.HC(" + i + ')');
		}
	}

	public final ArrayBuffer method1878(int i, boolean bool, int i_228_, int i_229_, byte[] is) {
		try {
			if (haveArbVbo && (!bool || useArbVbo)) {
				return new Class287_Sub1(this, i_228_, is, i, bool);
			}
			if (i_229_ >= -46) {
				return null;
			}
			return new Class156_Sub2(this, i_228_, is, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.V(" + i + ',' + bool + ',' + i_228_ + ',' + i_229_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	public final synchronized void method1879(int i, byte i_230_, int i_231_) {
		try {
			NodeInteger class98_sub34 = new NodeInteger(i);
			class98_sub34.hash = i_231_;
			aClass148_4338.addLast(class98_sub34, -20911);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.BC(" + i + ',' + i_230_ + ',' + i_231_ + ')');
		}
	}

	public final void method1880(int i, OpenGLRenderTarget interface12) {
		try {
			if ((anInt4324 ^ 0xffffffff) <= -4) {
				throw new RuntimeException();
			}
			if (anInt4324 >= 0) {
				anInterface12Array4329[anInt4324].method38(-27095);
			}
			if (i >= 65) {
				anInterface12_4325 = anInterface12Array4329[++anInt4324] = interface12;
				anInterface12_4325.method37((byte) 77);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.JC(" + i + ',' + (interface12 != null ? "{...}" : "null") + ')');
		}
	}

	public final void method1881(boolean bool, boolean bool_241_) {
		do {
			try {
				if (bool == !aBoolean4352) {
					do {
						if (!bool) {
							OpenGL.glDisable(2929);
							if (!GameShell.cleanedStatics) {
								break;
							}
						}
						OpenGL.glEnable(2929);
					} while (false);
					aBoolean4352 = bool;
					anInt4350 &= ~0x1f;
				}
				if (bool_241_ == false) {
					break;
				}
				method1811(9, -63, 119, 56, 110, 39, -105, -9, -22);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.VA(" + bool + ',' + bool_241_ + ')');
			}
			break;
		} while (false);
	}

	public final void method1882(int i, int i_242_) {
		try {
			SunDefinitionParser.aFloatArray961[3] = (i >>> 911577944) / 255.0F;
			if (i_242_ > -67) {
				addCanvas(null, 84, 45);
			}
			SunDefinitionParser.aFloatArray961[0] = Class202.and(16711680, i) / 1.671168E7F;
			SunDefinitionParser.aFloatArray961[2] = Class202.and(i, 255) / 255.0F;
			SunDefinitionParser.aFloatArray961[1] = Class202.and(i, 65280) / 65280.0F;
			OpenGL.glTexEnvfv(8960, 8705, SunDefinitionParser.aFloatArray961, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.SA(" + i + ',' + i_242_ + ')');
		}
	}

	public final void method1883(OpenGlMatrix class111_sub1, byte i) {
		do {
			try {
				OpenGL.glPushMatrix();
				OpenGL.glMultMatrixf(class111_sub1.method2113(-117), 0);
				if (i <= -123) {
					break;
				}
				aClass148_4338 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.JD(" + (class111_sub1 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	private final void method1884(byte i) {
		try {
			if (i < 15) {
				gl = null;
			}
			float f = aFloat4452 * -anInt4451 / anInt4419;
			float f_244_ = aFloat4452 * -anInt4394 / anInt4381;
			float f_245_ = aFloat4452 * (width + -anInt4451) / anInt4419;
			float f_246_ = (height + -anInt4394) * aFloat4452 / anInt4381;
			OpenGL.glMatrixMode(5889);
			OpenGL.glLoadIdentity();
			if (f_245_ != f && f_244_ != f_246_) {
				OpenGL.glOrtho(f, f_245_, -f_246_, -f_244_, anInt4404, anInt4389);
			}
			OpenGL.glMatrixMode(5888);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.CC(" + i + ')');
		}
	}

	private final void method1885(byte i) {
		do {
			try {
				OpenGL.glMatrixMode(5889);
				OpenGL.glLoadMatrixf(aFloatArray4440, 0);
				OpenGL.glMatrixMode(5888);
				if (i <= -31) {
					break;
				}
				method1803(55, 77, -121, 59, -28, 57);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.CD(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1886(int i, int i_247_, int i_248_, int i_249_) {
		try {
			OpenGL.glTexEnvi(8960, i_247_ + 34184, i_249_);
			OpenGL.glTexEnvi(8960, i_248_ - -i_247_, i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.B(" + i + ',' + i_247_ + ',' + i_248_ + ',' + i_249_ + ')');
		}
	}

	public final void method1887(ArrayBuffer interface16, int i) {
		do {
			try {
				if (anInterface16_4374 != interface16) {
					if (haveArbVbo) {
						OpenGL.glBindBufferARB(34962, interface16.method53(-14112));
					}
					anInterface16_4374 = interface16;
				}
				if (i == 34962) {
					break;
				}
				method1860((byte) -89);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.MC(" + (interface16 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1888(int i, int i_250_, int i_251_) {
		try {
			anInt4409 = i;
			anInt4415 = i_251_;
			method1844((byte) 55);
			method1828((byte) 103);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.SC(" + i + ',' + i_250_ + ',' + i_251_ + ')');
		}
	}

	public final void method1889(boolean bool) {
		do {
			try {
				if ((anInt4350 ^ 0xffffffff) != -5) {
					method1877((byte) -82);
					method1856(false, 6914);
					method1851(false, false);
					method1881(false, bool);
					method1875((byte) 18, false);
					method1834(124, -2);
					setBlendMode((byte) -72, 1);
					anInt4350 = 4;
				}
				if (bool == false) {
					break;
				}
				anInt4387 = -6;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.LB(" + bool + ')');
			}
			break;
		} while (false);
	}

	public final void method1890(float f, boolean bool) {
		try {
			if (bool != true) {
				X(112);
			}
			if (aFloat4452 != f) {
				aFloat4452 = f;
				if (anInt4385 == 3) {
					method1884((byte) 87);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.AE(" + f + ',' + bool + ')');
		}
	}

	private final void method1891(int i) {
		try {
			if (i != 370914608) {
				b(96, -106, -101, 12, 0.5374041586352528);
			}
			if (aBoolean4422) {
				OpenGL.glMatrixMode(5890);
				OpenGL.glLoadIdentity();
				OpenGL.glMatrixMode(5888);
				aBoolean4422 = false;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.GC(" + i + ')');
		}
	}

	public final int method1892(int i, int i_273_) {
		try {
			if (i_273_ != 596294056) {
				return -116;
			}
			if ((i ^ 0xffffffff) == -2) {
				return 7681;
			}
			if (i == 0) {
				return 8448;
			}
			if (i != 2) {
				if ((i ^ 0xffffffff) != -4) {
					if (i == 4) {
						return 34023;
					}
				} else {
					return 260;
				}
			} else {
				return 34165;
			}
			throw new IllegalArgumentException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.TB(" + i + ',' + i_273_ + ')');
		}
	}

	private final void method1893(int i) {
		do {
			if (!hasFog || (fogDistance ^ 0xffffffff) > -1) {
				OpenGL.glDisable(2912);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			OpenGL.glEnable(2912);
		} while (false);

	}

	private final void method1894(byte i) {
		try {
			aFloatArray4440[10] = aFloat4371;
			aFloatArray4440[14] = aFloat4416;
			aFloat4401 = (-anInt4389 + aFloatArray4440[14]) / aFloatArray4440[10];
			aFloat4379 = anInt4389;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.BE(" + i + ')');
		}
	}

	public final void method1896(int i, int i_280_) {
		do {
			try {
				if ((i_280_ ^ 0xffffffff) == -2) {
					method1899(7681, 8960, 7681);
				} else if (i_280_ == 0) {
					method1899(8448, 8960, 8448);
				} else if (i_280_ != 2) {
					if ((i_280_ ^ 0xffffffff) != -4) {
						if (i_280_ == 4) {
							method1899(34023, 8960, 34023);
						}
					} else {
						method1899(8448, 8960, 260);
					}
				} else {
					method1899(7681, i + 8700, 34165);
				}
				if (i == 260) {
					break;
				}
				aClass148_4341 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.WC(" + i + ',' + i_280_ + ')');
			}
			break;
		} while (false);
	}

	public final void method1897(int i, boolean bool, boolean bool_283_, byte i_284_) {
		try {
			if ((i ^ 0xffffffff) != (anInt4357 ^ 0xffffffff) || !aBoolean4448 == underwater) {
				Class42_Sub1 class42_sub1 = null;
				int i_285_ = 0;
				byte i_286_ = 0;
				int i_287_ = 0;
				byte i_288_ = !underwater ? (byte) 0 : (byte) 3;
				if (i < 0) {
					method1891(370914608);
				} else {
					class42_sub1 = texFactory.method3931(i_284_ + 196, i);
					TextureMetrics class238 = this.metricsList.getInfo(i, -28755);
					if ((class238.aByte1823 ^ 0xffffffff) != -1 || class238.aByte1837 != 0) {
						int i_289_ = !class238.aBoolean1822 ? 128 : 64;
						int i_290_ = 50 * i_289_;
						method1857((float) (anInt4321 % i_290_ * class238.aByte1823) / (float) i_290_, 0.0F, (byte) 44, (float) (class238.aByte1837 * (anInt4321 % i_290_)) / (float) i_290_);
					} else {
						method1891(370914608);
					}
					i_285_ = class238.anInt1821;
					if (!underwater) {
						i_288_ = class238.aByte1820;
						i_287_ = class238.anInt1835;
						i_286_ = class238.aByte1816;
					}
				}
				effectManager.method508(bool_283_, bool, i_287_, i_286_, true, i_288_);
				if (!effectManager.method509(class42_sub1, false, i_285_)) {
					setActiveTexture(1, class42_sub1);
					method1896(260, i_285_);
				}
				anInt4357 = i;
				aBoolean4448 = underwater;
			}
			if (i_284_ != -70) {
				U(4, 29, -13, 16, 49);
			}
			anInt4350 &= ~0x7;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.OC(" + i + ',' + bool + ',' + bool_283_ + ',' + i_284_ + ')');
		}
	}

	public final void method1898(boolean bool, OpenGLRenderTarget interface12) {
		try {
			if (bool != true) {
				method1893(-128);
			}
			if (!aBoolean4375) {
				if ((anInt4331 ^ 0xffffffff) <= -4) {
					throw new RuntimeException();
				}
				if (anInt4331 >= 0) {
					anInterface12Array4332[anInt4331].method35((byte) 69);
				}
				anInterface12_4325 = anInterface12_4328 = anInterface12Array4332[++anInt4331] = interface12;
				anInterface12_4325.method39((byte) 122);
			} else {
				method1880(127, interface12);
				method1835(interface12, 0);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.QB(" + bool + ',' + (interface12 != null ? "{...}" : "null") + ')');
		}
	}

	public final void method1899(int i, int i_291_, int i_292_) {
		do {
			try {
				do {
					if (anInt4417 == 0) {
						boolean bool = false;
						if ((anInt4395 ^ 0xffffffff) != (i_292_ ^ 0xffffffff)) {
							OpenGL.glTexEnvi(8960, 34161, i_292_);
							bool = true;
							anInt4395 = i_292_;
						}
						if ((i ^ 0xffffffff) != (anInt4387 ^ 0xffffffff)) {
							OpenGL.glTexEnvi(8960, 34162, i);
							anInt4387 = i;
							bool = true;
						}
						if (!bool) {
							break;
						}
						anInt4350 &= ~0x1d;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					OpenGL.glTexEnvi(8960, 34161, i_292_);
					OpenGL.glTexEnvi(8960, 34162, i);
				} while (false);
				if (i_291_ == 8960) {
					break;
				}
				aClass283_4312 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.AA(" + i + ',' + i_291_ + ',' + i_292_ + ')');
			}
			break;
		} while (false);
	}

	private final void method1900(byte i) {
		try {
			SunDefinitionParser.aFloatArray961[3] = 1.0F;
			SunDefinitionParser.aFloatArray961[0] = aFloat4433 * sunIntensity;
			SunDefinitionParser.aFloatArray961[1] = aFloat4420 * sunIntensity;
			SunDefinitionParser.aFloatArray961[2] = aFloat4458 * sunIntensity;
			OpenGL.glLightfv(16384, 4609, SunDefinitionParser.aFloatArray961, 0);
			SunDefinitionParser.aFloatArray961[2] = aFloat4458 * -aFloat4407;
			SunDefinitionParser.aFloatArray961[3] = 1.0F;
			SunDefinitionParser.aFloatArray961[0] = aFloat4433 * -aFloat4407;
			SunDefinitionParser.aFloatArray961[1] = -aFloat4407 * aFloat4420;
			if (i <= 64) {
				setClipPlanes(-28, 34);
			}
			OpenGL.glLightfv(16385, 4609, SunDefinitionParser.aFloatArray961, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.IA(" + i + ')');
		}
	}

	public final void method1901(byte i) {
		do {
			try {
				if ((anInt4350 ^ 0xffffffff) != -17) {
					method1874(i + 5);
					method1856(true, 6914);
					method1881(true, false);
					method1875((byte) -123, true);
					setBlendMode((byte) -33, 1);
					anInt4350 = 16;
				}
				if (i == -35) {
					break;
				}
				aFloatArray4463 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.GE(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1902(byte i) {
		try {
			if (i != 60) {
				aBoolean4422 = true;
			}
			OpenGL.glPopMatrix();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.NB(" + i + ')');
		}
	}

	private final void method1903(int i) {
		do {
			try {
				aClass42Array4396 = new Class42[anInt4410];
				aClass42_Sub1_4358 = new Class42_Sub1(this, 3553, 6408, 1, 1);
				new Class42_Sub1(this, 3553, 6408, 1, 1);
				new Class42_Sub1(this, 3553, 6408, 1, 1);
				aClass146_Sub2_4428 = new OpenGlModelRenderer(this);
				aClass146_Sub2_4393 = new OpenGlModelRenderer(this);
				aClass146_Sub2_4462 = new OpenGlModelRenderer(this);
				aClass146_Sub2_4456 = new OpenGlModelRenderer(this);
				aClass146_Sub2_4386 = new OpenGlModelRenderer(this);
				aClass146_Sub2_4369 = new OpenGlModelRenderer(this);
				aClass146_Sub2_4461 = new OpenGlModelRenderer(this);
				if (i <= 26) {
					b(113, -33, -15, 84, -0.18123041621142552);
				}
				aClass146_Sub2_4361 = new OpenGlModelRenderer(this);
				aClass146_Sub2_4355 = new OpenGlModelRenderer(this);
				aClass146_Sub2_4382 = new OpenGlModelRenderer(this);
				if (!haveExtFrameBufferObject) {
					break;
				}
				aClass288_4363 = new Class288(this);
				new Class288(this);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.TE(" + i + ')');
			}
			break;
		} while (false);
	}

	private final void method1904(int i) {
		do {
			try {
				if (anInt4385 != 2) {
					anInt4385 = 2;
					method1885((byte) -47);
					method1833(4);
					anInt4350 &= ~0x7;
				}
				if (i <= -3) {
					break;
				}
				aFloat4371 = 0.12732868F;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.FC(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1905(boolean bool, int i) {
		try {
			if (i != 0) {
				aClass98_Sub5Array4408 = null;
			}
			if (bool == !aBoolean4405) {
				aBoolean4405 = bool;
				method1826(2896);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.AC(" + bool + ',' + i + ')');
		}
	}

	private final void method1906(int i) {
		do {
			try {
				aFloat4359 = anInt4402 - anInt4394;
				aFloat4421 = anInt4370 - anInt4451;
				aFloat4437 = anInt4414 + -anInt4394;
				aFloat4364 = anInt4432 + -anInt4451;
				if (i == 4353) {
					break;
				}
				anInterface16_4374 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.HB(" + i + ')');
			}
			break;
		} while (false);
	}

	public final void method1907(OpenGLRenderTarget interface12, int i) {
		try {
			if (i == -1) {
				if (!aBoolean4375) {
					if ((anInt4331 ^ 0xffffffff) > -1 || interface12 != anInterface12Array4332[anInt4331]) {
						throw new RuntimeException();
					}
					anInterface12Array4332[anInt4331--] = null;
					interface12.method35((byte) 69);
					if ((anInt4331 ^ 0xffffffff) > -1) {
						anInterface12_4325 = anInterface12_4328 = null;
					} else {
						anInterface12_4325 = anInterface12_4328 = anInterface12Array4332[anInt4331];
						anInterface12_4325.method39((byte) 110);
					}
				} else {
					method1832(interface12, 88);
					method1869(0, interface12);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.KB(" + (interface12 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final void method1908(boolean bool, int i, int i_337_) {
		try {
			if (i > -61) {
				X(-111);
			}
			method1897(i_337_, true, bool, (byte) -70);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.JB(" + bool + ',' + i + ',' + i_337_ + ')');
		}
	}

	public final void method1909(int i, OpenGlMatrix class111_sub1) {
		do {
			try {
				OpenGL.glLoadMatrixf(class111_sub1.method2113(-108), 0);
				if (i == -32076) {
					break;
				}
				drawRectangle(110, 43, -54, 86, -102, -54);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.CA(" + i + ',' + (class111_sub1 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final void method1910(int i, int i_350_, boolean bool, int i_351_) {
		do {
			try {
				OpenGL.glDrawArrays(i, i_351_, i_350_);
				if (bool == false) {
					break;
				}
				method1800();
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.R(" + i + ',' + i_350_ + ',' + bool + ',' + i_351_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final boolean needsNativeHeap() {
		try {
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.UF(" + ')');
		}
	}

	@Override
	public final void P(int i, int i_233_, int i_234_, int i_235_, int i_236_) {
		try {
			method1859(-67);
			setBlendMode((byte) -120, i_236_);
			float f = 0.35F + i;
			float f_237_ = i_233_ + 0.35F;
			OpenGL.glColor4ub((byte) (i_235_ >> -532968944), (byte) (i_235_ >> 1812124904), (byte) i_235_, (byte) (i_235_ >> 1778410936));
			OpenGL.glBegin(1);
			OpenGL.glVertex2f(f, f_237_);
			OpenGL.glVertex2f(f, i_234_ + f_237_);
			OpenGL.glEnd();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.P(" + i + ',' + i_233_ + ',' + i_234_ + ',' + i_235_ + ',' + i_236_ + ')');
		}
	}

	@Override
	public final void pa() {
		try {
			underwater = false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.pa(" + ')');
		}
	}

	@Override
	public final void Q(int i, int i_261_, int i_262_, int i_263_, int i_264_, int i_265_, byte[] is, int i_266_, int i_267_) {
		try {
			float f;
			float f_268_;
			do {
				if (aClass42_Sub1_Sub1_4444 != null && aClass42_Sub1_Sub1_4444.anInt5355 >= i_262_ && i_263_ <= aClass42_Sub1_Sub1_4444.anInt5352) {
					aClass42_Sub1_Sub1_4444.method378(i_262_, 6406, false, 0, is, 0, (byte) -80, 0, 0, i_263_);
					f = aClass42_Sub1_Sub1_4444.aFloat6205 * i_262_ / aClass42_Sub1_Sub1_4444.anInt5355;
					f_268_ = i_263_ * aClass42_Sub1_Sub1_4444.aFloat6209 / aClass42_Sub1_Sub1_4444.anInt5352;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				aClass42_Sub1_Sub1_4444 = Class284_Sub2.method3374(6406, i_263_, 14764, 6406, this, false, is, i_262_);
				aClass42_Sub1_Sub1_4444.method383(false, 10242, false);
				f_268_ = aClass42_Sub1_Sub1_4444.aFloat6209;
				f = aClass42_Sub1_Sub1_4444.aFloat6205;
			} while (false);
			method1829((byte) -95);
			setActiveTexture(1, aClass42_Sub1_Sub1_4444);
			setBlendMode((byte) -106, i_267_);
			OpenGL.glColor4ub((byte) (i_264_ >> -1114409264), (byte) (i_264_ >> 1074215208), (byte) i_264_, (byte) (i_264_ >> 1619670840));
			method1882(i_265_, -126);
			method1899(34165, 8960, 34165);
			method1840(0, 768, 109, 34166);
			method1840(2, 770, -72, 5890);
			method1886(770, 0, 34200, 34166);
			method1886(770, 2, 34200, 5890);
			float f_269_ = i;
			float f_270_ = i_261_;
			float f_271_ = f_269_ + i_262_;
			float f_272_ = i_263_ + f_270_;
			OpenGL.glBegin(7);
			OpenGL.glTexCoord2f(0.0F, 0.0F);
			OpenGL.glVertex2f(f_269_, f_270_);
			OpenGL.glTexCoord2f(0.0F, f);
			OpenGL.glVertex2f(f_269_, f_272_);
			OpenGL.glTexCoord2f(f_268_, f);
			OpenGL.glVertex2f(f_271_, f_272_);
			OpenGL.glTexCoord2f(f_268_, 0.0F);
			OpenGL.glVertex2f(f_271_, f_270_);
			OpenGL.glEnd();
			method1840(0, 768, 103, 5890);
			method1840(2, 770, 74, 34166);
			method1886(770, 0, 34200, 5890);
			method1886(770, 2, 34200, 34166);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.Q(" + i + ',' + i_261_ + ',' + i_262_ + ',' + i_263_ + ',' + i_264_ + ',' + i_265_ + ',' + (is != null ? "{...}" : "null") + ',' + i_266_ + ',' + i_267_ + ')');
		}
	}

	@Override
	public final int r(int i, int i_80_, int i_81_, int i_82_, int i_83_, int i_84_, int i_85_) {
		try {
			float f = aClass111_Sub1_4348.aFloat4673 * i_81_ + (aClass111_Sub1_4348.aFloat4676 * i_80_ + aClass111_Sub1_4348.aFloat4684 * i) + aClass111_Sub1_4348.aFloat4677;
			float f_86_ = aClass111_Sub1_4348.aFloat4673 * i_84_ + (aClass111_Sub1_4348.aFloat4676 * i_83_ + aClass111_Sub1_4348.aFloat4684 * i_82_) + aClass111_Sub1_4348.aFloat4677;
			int i_87_ = 0;
			if (!(f < anInt4404) || !(anInt4404 > f_86_)) {
				if (f > anInt4389 && f_86_ > anInt4389) {
					i_87_ |= 0x20;
				}
			} else {
				i_87_ |= 0x10;
			}
			int i_88_ = (int) (anInt4419 * (aClass111_Sub1_4348.aFloat4680 * i_81_ + (aClass111_Sub1_4348.aFloat4679 * i_80_ + aClass111_Sub1_4348.aFloat4686 * i) + aClass111_Sub1_4348.aFloat4674) / i_85_);
			int i_89_ = (int) ((i_83_ * aClass111_Sub1_4348.aFloat4679 + i_82_ * aClass111_Sub1_4348.aFloat4686 + aClass111_Sub1_4348.aFloat4680 * i_84_ + aClass111_Sub1_4348.aFloat4674) * anInt4419 / i_85_);
			if (i_88_ < aFloat4421 && aFloat4421 > i_89_) {
				i_87_ |= 0x1;
			} else if (aFloat4364 < i_88_ && i_89_ > aFloat4364) {
				i_87_ |= 0x2;
			}
			int i_90_ = (int) ((aClass111_Sub1_4348.aFloat4675 * i_80_ + i * aClass111_Sub1_4348.aFloat4678 + i_81_ * aClass111_Sub1_4348.aFloat4687 + aClass111_Sub1_4348.aFloat4683) * anInt4381 / i_85_);
			int i_91_ = (int) (anInt4381 * (aClass111_Sub1_4348.aFloat4683 + (i_84_ * aClass111_Sub1_4348.aFloat4687 + (i_83_ * aClass111_Sub1_4348.aFloat4675 + aClass111_Sub1_4348.aFloat4678 * i_82_))) / i_85_);
			if (aFloat4359 > i_90_ && i_91_ < aFloat4359) {
				i_87_ |= 0x4;
			} else if (i_90_ > aFloat4437 && aFloat4437 < i_91_) {
				i_87_ |= 0x8;
			}
			return i_87_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.r(" + i + ',' + i_80_ + ',' + i_81_ + ',' + i_82_ + ',' + i_83_ + ',' + i_84_ + ',' + i_85_ + ')');
		}
	}

	@Override
	public final void ra(int i, int i_277_, int i_278_, int i_279_) {
		try {
			anInt4362 = i;
			anInt4454 = i_278_;
			underwater = true;
			anInt4423 = i_277_;
			anInt4453 = i_279_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.ra(" + i + ',' + i_277_ + ',' + i_278_ + ',' + i_279_ + ')');
		}
	}

	@Override
	public final void resetRenderTarget() {
		try {
			do {
				if (!haveExtFrameBufferObject) {
					if (!aBoolean4388) {
						throw new RuntimeException("");
					}
					aClass332_Sub1_4330.method3736(0, 0, width, height, 0, 0);
					gl.setSurface(activeCanvasHandle);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if (anInterface12_4328 != aClass288_4333) {
					throw new RuntimeException();
				}
				aClass288_4333.method3401(0, true);
				aClass288_4333.method3401(8, true);
				method1907(aClass288_4333, -1);
			} while (false);
			aClass332_Sub1_4330 = null;
			height = anInt4297;
			width = anInt4301;
			method1867(29458);
			method1844((byte) 60);
			clearClip();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.QD(" + ')');
		}
	}

	@Override
	public final void resize(Canvas canvas, int i, int i_274_) {
		do {
			try {
				long l = 0L;
				do {
					if (canvas == null || defaultCanvas == canvas) {
						l = defaultCanvasHandle;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					if (aHashtable4306.containsKey(canvas)) {
						Long var_long = (Long) aHashtable4306.get(canvas);
						l = var_long.longValue();
					}
				} while (false);
				if (l == 0L) {
					throw new RuntimeException();
				}
				gl.surfaceResized(l);
				if (activeCanvas != canvas) {
					break;
				}
				method1850((byte) -31);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ac.JI(" + (canvas != null ? "{...}" : "null") + ',' + i + ',' + i_274_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void setAmbientIntensity(float f) {
		try {
			if (ambient != f) {
				ambient = f;
				method1860((byte) -41);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.xa(" + f + ')');
		}
	}

	@Override
	public final void setClip(int i, int i_113_, int i_114_, int i_115_) {
		try {
			if ((width ^ 0xffffffff) > (i_114_ ^ 0xffffffff)) {
				i_114_ = width;
			}
			if (i < 0) {
				i = 0;
			}
			if (i_113_ < 0) {
				i_113_ = 0;
			}
			if (i_115_ > height) {
				i_115_ = height;
			}
			anInt4414 = i_115_;
			anInt4402 = i_113_;
			anInt4370 = i;
			anInt4432 = i_114_;
			OpenGL.glEnable(3089);
			method1906(4353);
			method1828((byte) 107);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.KA(" + i + ',' + i_113_ + ',' + i_114_ + ',' + i_115_ + ')');
		}
	}

	@Override
	public final void setClipPlanes(int i, int i_30_) {
		try {
			if (i != anInt4404 || anInt4389 != i_30_) {
				anInt4389 = i_30_;
				anInt4404 = i;
				method1847(true);
				method1852((byte) -66);
				if ((anInt4385 ^ 0xffffffff) != -4) {
					if (anInt4385 == 2) {
						method1885((byte) -125);
					}
				} else {
					method1884((byte) 56);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.f(" + i + ',' + i_30_ + ')');
		}
	}

	@Override
	public final void setDepthWriteMask(boolean bool) {
		try {
			aBoolean4465 = bool;
			method1876((byte) -75);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.C(" + bool + ')');
		}
	}

	@Override
	public final void setRenderTarget(RenderTarget interface17) {
		/* empty */
	}

	@Override
	public final void setSun(int i, float f, float f_121_, float f_122_, float f_123_, float f_124_) {
		try {
			boolean bool = (i ^ 0xffffffff) != (anInt4412 ^ 0xffffffff);
			if (bool || sunIntensity != f || f_121_ != aFloat4407) {
				anInt4412 = i;
				aFloat4407 = f_121_;
				sunIntensity = f;
				if (bool) {
					aFloat4458 = (anInt4412 & 0xff) / 255.0F;
					aFloat4420 = (anInt4412 & 0xff00) / 65280.0F;
					aFloat4433 = (0xff0000 & anInt4412) / 1.671168E7F;
					method1860((byte) -41);
				}
				method1900((byte) 90);
			}
			if (aFloatArray4372[0] != f_122_ || aFloatArray4372[1] != f_123_ || f_124_ != aFloatArray4372[2]) {
				aFloatArray4372[0] = f_122_;
				aFloatArray4372[1] = f_123_;
				aFloatArray4372[2] = f_124_;
				aFloatArray4418[0] = -f_122_;
				aFloatArray4418[2] = -f_124_;
				aFloatArray4418[1] = -f_123_;
				float f_125_ = (float) (1.0 / Math.sqrt(f_122_ * f_122_ + f_123_ * f_123_ + f_124_ * f_124_));
				sunDirection[2] = f_124_ * f_125_;
				sunDirection[0] = f_122_ * f_125_;
				sunDirection[1] = f_125_ * f_123_;
				aFloatArray4463[1] = -sunDirection[1];
				aFloatArray4463[2] = -sunDirection[2];
				aFloatArray4463[0] = -sunDirection[0];
				method1831(127);
				sunProjectionZ = (int) (256.0F * f_124_ / f_123_);
				sunProjectionX = (int) (256.0F * f_122_ / f_123_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.ZA(" + i + ',' + f + ',' + f_121_ + ',' + f_122_ + ',' + f_123_ + ',' + f_124_ + ')');
		}
	}

	@Override
	public final void switchCanvas(Canvas canvas) {
		try {
			activeCanvas = null;
			activeCanvasHandle = 0L;
			do {
				if (canvas != null && canvas != defaultCanvas) {
					if (!aHashtable4306.containsKey(canvas)) {
						break;
					}
					Long var_long = (Long) aHashtable4306.get(canvas);
					activeCanvasHandle = var_long.longValue();
					activeCanvas = canvas;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				activeCanvasHandle = defaultCanvasHandle;
				activeCanvas = defaultCanvas;
			} while (false);
			if (activeCanvas == null || (activeCanvasHandle ^ 0xffffffffffffffffL) == -1L) {
				throw new RuntimeException();
			}
			gl.setSurface(activeCanvasHandle);
			method1850((byte) -31);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.DH(" + (canvas != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void U(int i, int i_106_, int i_107_, int i_108_, int i_109_) {
		try {
			method1859(-106);
			setBlendMode((byte) -82, i_109_);
			float f = 0.35F + i;
			OpenGL.glColor4ub((byte) (i_108_ >> -1500168624), (byte) (i_108_ >> -2018405784), (byte) i_108_, (byte) (i_108_ >> -1514779816));
			float f_110_ = 0.35F + i_106_;
			OpenGL.glBegin(1);
			OpenGL.glVertex2f(f, f_110_);
			OpenGL.glVertex2f(i_107_ + f, f_110_);
			OpenGL.glEnd();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.U(" + i + ',' + i_106_ + ',' + i_107_ + ',' + i_108_ + ',' + i_109_ + ')');
		}
	}

	@Override
	public final void X(int i) {
		try {
			shadowScale = 0;
			for (/**/; (i ^ 0xffffffff) < -2; i >>= 1) {
				shadowScale++;
			}
			anInt4318 = 1 << shadowScale;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.X(" + i + ')');
		}
	}

	@Override
	public final int[] Y() {
		try {
			return new int[] { anInt4451, anInt4394, anInt4419, anInt4381 };
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.Y(" + ')');
		}
	}

	@Override
	public final void ya() {
		try {
			method1875((byte) -121, true);
			OpenGL.glClear(256);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.ya(" + ')');
		}
	}

	@Override
	public final void za(int i, int i_253_, int i_254_, int i_255_, int i_256_) {
		try {
			if (i_254_ < 0) {
				i_254_ = -i_254_;
			}
			if ((i_254_ + i ^ 0xffffffff) <= (anInt4370 ^ 0xffffffff) && anInt4432 >= -i_254_ + i && anInt4402 <= i_253_ + i_254_ && i_253_ - i_254_ <= anInt4414) {
				method1859(-51);
				setBlendMode((byte) -23, i_256_);
				OpenGL.glColor4ub((byte) (i_255_ >> 2073758448), (byte) (i_255_ >> -1643739512), (byte) i_255_, (byte) (i_255_ >> -1884862856));
				float f = i + 0.35F;
				float f_257_ = i_253_ + 0.35F;
				int i_258_ = i_254_ << -2109983999;
				if (i_258_ < aFloat4429) {
					OpenGL.glBegin(7);
					OpenGL.glVertex2f(1.0F + f, 1.0F + f_257_);
					OpenGL.glVertex2f(f + 1.0F, -1.0F + f_257_);
					OpenGL.glVertex2f(-1.0F + f, f_257_ - 1.0F);
					OpenGL.glVertex2f(f - 1.0F, f_257_ + 1.0F);
					OpenGL.glEnd();
				} else if (!(aFloat4400 >= i_258_)) {
					OpenGL.glBegin(6);
					OpenGL.glVertex2f(f, f_257_);
					int i_259_ = 262144 / (6 * i_254_);
					if (i_259_ <= 64) {
						i_259_ = 64;
					} else if (i_259_ > 512) {
						i_259_ = 512;
					}
					i_259_ = Class23.priorPowerOf2(73, i_259_);
					OpenGL.glVertex2f(f + i_254_, f_257_);
					for (int i_260_ = -i_259_ + 16384; i_260_ > 0; i_260_ -= i_259_) {
						OpenGL.glVertex2f(f + Class98_Sub10_Sub36.aFloatArray5741[i_260_] * i_254_, Class98_Sub10_Sub36.aFloatArray5742[i_260_] * i_254_ + f_257_);
					}
					OpenGL.glVertex2f(i_254_ + f, f_257_);
					OpenGL.glEnd();
				} else {
					OpenGL.glEnable(2832);
					OpenGL.glPointSize(i_258_);
					OpenGL.glBegin(0);
					OpenGL.glVertex2f(f, f_257_);
					OpenGL.glEnd();
					OpenGL.glDisable(2832);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ac.za(" + i + ',' + i_253_ + ',' + i_254_ + ',' + i_255_ + ',' + i_256_ + ')');
		}
	}
}

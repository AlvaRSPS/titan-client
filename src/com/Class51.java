/* Class51 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.preferences.Class64_Sub9;

public final class Class51 {
	public static final void insertNode(int i, Cacheable class98_sub46, Cacheable class98_sub46_0_) {
		try {
			if (class98_sub46_0_.previousCacheable != null) {
				class98_sub46_0_.uncache((byte) -90);
			}
			if (i >= 32) {
				class98_sub46_0_.nextCacheable = class98_sub46;
				class98_sub46_0_.previousCacheable = class98_sub46.previousCacheable;
				class98_sub46_0_.previousCacheable.nextCacheable = class98_sub46_0_;
				class98_sub46_0_.nextCacheable.previousCacheable = class98_sub46_0_;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dja.A(" + i + ',' + (class98_sub46 != null ? "{...}" : "null") + ',' + (class98_sub46_0_ != null ? "{...}" : "null") + ')');
		}
	}

	boolean			aBoolean424;
	Class42_Sub1[]	aClass42_Sub1Array420	= null;
	Class42_Sub1[]	aClass42_Sub1Array423	= null;
	Class42_Sub4	aClass42_Sub4_421;
	Class42_Sub4	aClass42_Sub4_422;

	Class42_Sub4	aClass42_Sub4_425;

	Class51(OpenGlToolkit var_ha_Sub1) {
		aClass42_Sub4_421 = null;
		aClass42_Sub4_425 = null;
		aClass42_Sub4_422 = null;
		try {
			aBoolean424 = var_ha_Sub1.haveExt3DTexture;
			Class64_Sub9.method590(true, var_ha_Sub1);
			if (aBoolean424) {
				byte[] is = Class98_Sub28_Sub1.unpackDataBuffer(false, VarPlayerDefinition.anObject1285, false);
				aClass42_Sub4_422 = new Class42_Sub4(var_ha_Sub1, 6410, 128, 128, 16, is, 6410);
				is = Class98_Sub28_Sub1.unpackDataBuffer(false, Class130.anObject1030, false);
				aClass42_Sub4_425 = new Class42_Sub4(var_ha_Sub1, 6410, 128, 128, 16, is, 6410);
				OpenGLHeightMapToNormalMapConverter class118 = var_ha_Sub1.hmToNormalCvtrMap;
				if (class118.method2171(true)) {
					is = Class98_Sub28_Sub1.unpackDataBuffer(false, Class98_Sub41.anObject4203, false);
					aClass42_Sub4_421 = new Class42_Sub4(var_ha_Sub1, 6408, 128, 128, 16);
					Class42_Sub4 class42_sub4 = new Class42_Sub4(var_ha_Sub1, 6409, 128, 128, 16, is, 6409);
					if (class118.method2172(0, class42_sub4, 2.0F, aClass42_Sub4_421)) {
						aClass42_Sub4_421.method371(35);
					} else {
						aClass42_Sub4_421.method375(true);
						aClass42_Sub4_421 = null;
					}
					class42_sub4.method375(true);
				}
			} else {
				aClass42_Sub1Array423 = new Class42_Sub1[16];
				for (int i = 0; (i ^ 0xffffffff) > -17; i++) {
					byte[] is = Class98_Sub10_Sub20.method1061(2, 32768, 128 * 128 * i * 2, VarPlayerDefinition.anObject1285);
					aClass42_Sub1Array423[i] = new Class42_Sub1(var_ha_Sub1, 3553, 6410, 128, 128, true, is, 6410, false);
				}
				aClass42_Sub1Array420 = new Class42_Sub1[16];
				for (int i = 0; (i ^ 0xffffffff) > -17; i++) {
					byte[] is = Class98_Sub10_Sub20.method1061(2, 32768, 16384 * i * 2, Class130.anObject1030);
					aClass42_Sub1Array420[i] = new Class42_Sub1(var_ha_Sub1, 3553, 6410, 128, 128, true, is, 6410, false);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "dja.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ')');
		}
	}
}

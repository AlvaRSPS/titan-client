/* Class369 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;

public final class Class369 {
	public static IncomingOpcode	IGNORE_LIST		= new IncomingOpcode(37, -2);
	public static float				aFloat3131;
	public static int				anInt3129		= 0;
	public static boolean			texturesEnabled	= false;

	public static final void setLoginResponse(int responseCode, byte dummy) {
		do {
			if (Class98_Sub46_Sub20_Sub2.anInt6317 == 1) {
				RenderAnimDefinitionParser.anInt1946 = responseCode;
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			if ((Class98_Sub46_Sub20_Sub2.anInt6317 ^ 0xffffffff) == -3) {
				SocketWrapper.anInt300 = responseCode;
			}
		} while (false);

	}

	public static int method3953(int i, int i_1_) {
		try {
			return i ^ i_1_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wea.A(" + i + ',' + i_1_ + ')');
		}
	}

	public static final void method3954(int i) {
		if (Class76.aClass28ArrayArray586 != null) {
			for (int xChunk = 0; (xChunk ^ 0xffffffff) > (Class76.aClass28ArrayArray586.length ^ 0xffffffff); xChunk++) {
				for (int yChunk = 0; yChunk < Class76.aClass28ArrayArray586[xChunk].length; yChunk++) {
					Class76.aClass28ArrayArray586[xChunk][yChunk] = OpenGLDisplayList.aClass28_722;
				}
			}
		}

	}

	public static void method3955(int i) {
		do {
			try {
				IGNORE_LIST = null;
				if (i > 75) {
					break;
				}
				method3954(91);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "wea.D(" + i + ')');
			}
			break;
		} while (false);
	}
}

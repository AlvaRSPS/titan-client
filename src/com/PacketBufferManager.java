/* Class129 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.LightIntensityDefinition;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteProgressBarLoadingScreenElement;
import com.jagex.game.input.impl.AwtMouseListener;

public final class PacketBufferManager {
	public static int			anInt1026		= 0;
	public static int[]			anIntArray1027	= { 0, 2, 2, 2, 1, 1, 3, 3, 1, 3, 3, 4, 4 };
	public static byte[][]		pool100			= new byte[1000][];
	public static int			pool100Ptr		= 0;
	public static byte[][]		pool30000		= new byte[50][];
	public static int			pool30000Ptr	= 0;
	public static byte[][]		pool5000		= new byte[250][];
	public static int			pool5000Ptr		= 0;
	public static byte[][][]	poolVar;
	public static int[]			poolVarPtr;
	public static int[]			poolVarSize;

	public static final synchronized byte[] allocate(boolean bool, int size) {
		if ((size ^ 0xffffffff) == -101 && pool100Ptr > 0) {// Size 100
			byte[] buffer = pool100[--pool100Ptr];
			pool100[pool100Ptr] = null;
			return buffer;
		}
		if (bool != false) {
			anIntArray1027 = null;
		}
		if (size == 5000 && pool5000Ptr > 0) {
			byte[] buffer = pool5000[--pool5000Ptr];
			pool5000[pool5000Ptr] = null;
			return buffer;
		}
		if ((size ^ 0xffffffff) == -30001 && pool30000Ptr > 0) {// Size
																// 30000
			byte[] buffer = pool30000[--pool30000Ptr];
			pool30000[pool30000Ptr] = null;
			return buffer;
		}
		if (poolVar != null) {
			for (int index = 0; poolVarSize.length > index; index++) {
				if (size == poolVarSize[index] && (poolVarPtr[index] ^ 0xffffffff) < -1) {
					byte[] buffer = poolVar[index][--poolVarPtr[index]];
					poolVar[index][poolVarPtr[index]] = null;
					return buffer;
				}
			}
		}
		return new byte[size];
	}

	public static final synchronized void free(byte i, byte[] buffer) {
		do {
			if (buffer.length == 100 && pool100Ptr < 1000) {
				pool100[pool100Ptr++] = buffer;
			} else if (buffer.length == 5000 && pool5000Ptr < 250) {
				pool5000[pool5000Ptr++] = buffer;
			} else if ((buffer.length ^ 0xffffffff) == -30001 && (pool30000Ptr ^ 0xffffffff) > -51) {
				pool30000[pool30000Ptr++] = buffer;
			} else {
				if (poolVar == null) {
					break;
				}
				for (int index = 0; (index ^ 0xffffffff) > (poolVarSize.length ^ 0xffffffff); index++) {
					if (poolVarSize[index] == buffer.length && (poolVarPtr[index] ^ 0xffffffff) > (poolVar[index].length ^ 0xffffffff)) {
						poolVar[index][poolVarPtr[index]++] = buffer;
						break;
					}
				}
			}
		} while (false);
	}

	public static final void init(int[] sizes, int[] count, int i) {
		if (sizes == null || count == null) {
			poolVar = null;
			poolVarPtr = null;
			poolVarSize = null;
		} else {
			poolVarSize = sizes;
			poolVarPtr = new int[sizes.length];
			poolVar = new byte[sizes.length][][];
			for (int index = i; (poolVarSize.length ^ 0xffffffff) < (index ^ 0xffffffff); index++) {
				poolVar[index] = new byte[count[index]][];
			}
		}
	}

	public static void method2226(byte i) {
		anIntArray1027 = null;
	}

	public static final void method2227(Char class246_sub3) {
		if (class246_sub3 != null) {
			for (int i = 0; i < 2; i++) {
				Char class246_sub3_2_ = null;
				for (Char class246_sub3_3_ = LightIntensityDefinition.aClass246_Sub3Array3198[i]; class246_sub3_3_ != null; class246_sub3_3_ = class246_sub3_3_.animator) {
					if (class246_sub3_3_ == class246_sub3) {
						if (class246_sub3_2_ != null) {
							class246_sub3_2_.animator = class246_sub3_3_.animator;
						} else {
							LightIntensityDefinition.aClass246_Sub3Array3198[i] = class246_sub3_3_.animator;
						}
						Class358.aBoolean3033 = true;
						return;
					}
					class246_sub3_2_ = class246_sub3_3_;
				}
				class246_sub3_2_ = null;
				for (Char class246_sub3_4_ = Class359.aClass246_Sub3Array3056[i]; class246_sub3_4_ != null; class246_sub3_4_ = class246_sub3_4_.animator) {
					if (class246_sub3_4_ == class246_sub3) {
						if (class246_sub3_2_ != null) {
							class246_sub3_2_.animator = class246_sub3_4_.animator;
						} else {
							Class359.aClass246_Sub3Array3056[i] = class246_sub3_4_.animator;
						}
						Class358.aBoolean3033 = true;
						return;
					}
					class246_sub3_2_ = class246_sub3_4_;
				}
				class246_sub3_2_ = null;
				for (Char class246_sub3_5_ = Class130.aClass246_Sub3Array1029[i]; class246_sub3_5_ != null; class246_sub3_5_ = class246_sub3_5_.animator) {
					if (class246_sub3_5_ == class246_sub3) {
						if (class246_sub3_2_ != null) {
							class246_sub3_2_.animator = class246_sub3_5_.animator;
						} else {
							Class130.aClass246_Sub3Array1029[i] = class246_sub3_5_.animator;
						}
						Class358.aBoolean3033 = true;
						return;
					}
					class246_sub3_2_ = class246_sub3_5_;
				}
			}
		}
	}

	public static final void method2229() {
		if (Class246_Sub2.aClass172ArrayArrayArray5077 != null) {
			for (int i = 0; i < Class246_Sub2.aClass172ArrayArrayArray5077.length; i++) {
				for (int i_7_ = 0; i_7_ < BConfigDefinition.anInt3112; i_7_++) {
					for (int i_8_ = 0; i_8_ < Class64_Sub9.anInt3662; i_8_++) {
						if (Class246_Sub2.aClass172ArrayArrayArray5077[i][i_7_][i_8_] != null) {
							Class246_Sub2.aClass172ArrayArrayArray5077[i][i_7_][i_8_].method2544(6730);
						}
						Class246_Sub2.aClass172ArrayArrayArray5077[i][i_7_][i_8_] = null;
					}
				}
			}
		}
		Class246_Sub2.aClass172ArrayArrayArray5077 = null;
		StrongReferenceMCNode.aSArray6298 = null;
		if (Class252.aClass172ArrayArrayArray1927 != null) {
			for (int i = 0; i < Class252.aClass172ArrayArrayArray1927.length; i++) {
				for (int i_9_ = 0; i_9_ < BConfigDefinition.anInt3112; i_9_++) {
					for (int i_10_ = 0; i_10_ < Class64_Sub9.anInt3662; i_10_++) {
						if (Class252.aClass172ArrayArrayArray1927[i][i_9_][i_10_] != null) {
							Class252.aClass172ArrayArrayArray1927[i][i_9_][i_10_].method2544(6730);
						}
						Class252.aClass172ArrayArrayArray1927[i][i_9_][i_10_] = null;
					}
				}
			}
		}
		Class252.aClass172ArrayArrayArray1927 = null;
		Class81.aSArray618 = null;
		QuickChatCategory.aClass172ArrayArrayArray5948 = null;
		Class78.aSArray594 = null;
		Class74.aBooleanArrayArray551 = null;
		Class319.aBooleanArrayArray2702 = null;
		Class347.anIntArray2906 = null;
		Class34.aBooleanArrayArrayArray325 = null;
		GraphicsLevelPreferenceField.aBooleanArrayArrayArray3673 = null;
		TexturesPreferenceField.method633(0);
		if (Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273 != null) {
			for (int i = 0; i < Class347.dynamicCount; i++) {
				Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i] = null;
			}
			Class347.dynamicCount = 0;
		}
		LightIntensityDefinition.aClass246_Sub3Array3198 = null;
		Class359.aClass246_Sub3Array3056 = null;
		Class130.aClass246_Sub3Array1029 = null;
		if (FloorOverlayDefinitionParser.aClass246_Sub3Array307 != null) {
			for (int i = 0; i < FloorOverlayDefinitionParser.aClass246_Sub3Array307.length; i++) {
				FloorOverlayDefinitionParser.aClass246_Sub3Array307[i] = null;
			}
			GameObjectDefinitionParser.opaqueOnscreenCount = 0;
		}
		if (Class246_Sub4_Sub2.aClass246_Sub3Array6173 != null) {
			for (int i = 0; i < Class246_Sub4_Sub2.aClass246_Sub3Array6173.length; i++) {
				Class246_Sub4_Sub2.aClass246_Sub3Array6173[i] = null;
			}
			Class353.transOnscreenCount = 0;
		}
		if (Class98_Sub10_Sub31.aClass1Array5717 != null) {
			for (int i = 0; i < Class226.anInt1705; i++) {
				Class98_Sub10_Sub31.aClass1Array5717[i] = null;
			}
			for (int i = 0; i < OpenGLTexture2DSource.anInt3103; i++) {
				for (int i_11_ = 0; i_11_ < BConfigDefinition.anInt3112; i_11_++) {
					for (int i_12_ = 0; i_12_ < Class64_Sub9.anInt3662; i_12_++) {
						SpriteProgressBarLoadingScreenElement.aLongArrayArrayArray5476[i][i_11_][i_12_] = 0L;
					}
				}
			}
			Class226.anInt1705 = 0;
		}
		Class160.method2511(1350);
		Class98_Sub10_Sub27.aClass84_5692 = Class98_Sub10_Sub27.aClass84_5693;
		Class98_Sub10_Sub27.aClass84_5692.method833(0);
		AwtMouseListener.aByteArrayArray5291 = null;
		Class40.anIntArrayArray367 = null;
		GraphicsDefinitionParser.aShortArrayArray2534 = null;
		if (Class98_Sub46_Sub5.aClass174Array5970 != null) {
			Class249.method3162();
			Class98_Sub10_Sub30.activeToolkit.createContexts(1);
			Class98_Sub10_Sub30.activeToolkit.attachContext(0);
		}
		if (Class98_Sub43_Sub3.aClass245Array5922 != null) {
			Class98_Sub43_Sub3.aClass245Array5922 = null;
		}
		Class98_Sub10_Sub30.activeToolkit = null;
	}
}

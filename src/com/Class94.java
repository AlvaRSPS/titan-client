/* Class94 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.input.impl.AwtMouseEvent;

public final class Class94 {
	public static boolean	aBoolean797		= false;
	public static Js5		aClass207_793;
	public static long[]	aLongArray794	= new long[100];
	public static Js5		animationFramesJs5;
	public static int		anInt795;

	static {
		anInt795 = -1;
	}

	public static final Class154 method914(int i, int i_0_, int i_1_) {
		Class172 class172 = QuickChatCategory.aClass172ArrayArrayArray5948[i][i_0_][i_1_];
		if (class172 == null) {
			return null;
		}
		return class172.aClass154_1325;
	}

	public static final Class246_Sub1 method915(int i, byte i_2_, boolean bool) {
		try {
			synchronized (Class98_Sub46_Sub20_Sub2.aClass218Array6316) {
				if (i_2_ != -47) {
					return null;
				}
				Class246_Sub1 class246_sub1;
				if (Class98_Sub46_Sub20_Sub2.aClass218Array6316.length > i && !Class98_Sub46_Sub20_Sub2.aClass218Array6316[i].isEmpty(true)) {
					class246_sub1 = (Class246_Sub1) Class98_Sub46_Sub20_Sub2.aClass218Array6316[i].getLast((byte) -39);
					class246_sub1.unlink((byte) 126);
					Class1.anIntArray65[i]--;
				} else {
					class246_sub1 = new Class246_Sub1();
					class246_sub1.aClass246_Sub6Array5067 = new Class246_Sub6[i];
					for (int i_3_ = 0; i > i_3_; i_3_++) {
						class246_sub1.aClass246_Sub6Array5067[i_3_] = new Class246_Sub6();
					}
				}
				class246_sub1.aBoolean5070 = bool;
				return class246_sub1;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fr.C(" + i + ',' + i_2_ + ',' + bool + ')');
		}
	}

	public static final void method916(int i, Js5 class207, Js5 class207_4_, Js5 class207_5_, Js5 class207_6_) {
		try {
			AwtMouseEvent.interfaceJs5 = class207;
			Class340.aClass207_2847 = class207_6_;
			if (i >= -74) {
				method914(109, 98, -81);
			}
			Class166.imageJs5 = class207_5_;
			Class159.interfaceStore = new RtInterface[AwtMouseEvent.interfaceJs5.getGroupCount((byte) -11)][];
			Class246_Sub3_Sub3_Sub1.loadedInterface = new boolean[AwtMouseEvent.interfaceJs5.getGroupCount((byte) -11)];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fr.A(" + i + ',' + (class207 != null ? "{...}" : "null") + ',' + (class207_4_ != null ? "{...}" : "null") + ',' + (class207_5_ != null ? "{...}" : "null") + ',' + (class207_6_ != null ? "{...}" : "null") + ')');
		}
	}

	public static final boolean method917(byte i, int i_7_, int i_8_) {
		try {
			if (i > -90) {
				return false;
			}
			return !(!(Class195.method2663(i_8_, i_7_, false) | (i_8_ & 0x70000 ^ 0xffffffff) != -1) && !Class76_Sub7.method763(i_8_, i_7_, false));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fr.E(" + i + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	public static final void setVarcstr(String name, int id, int i_9_) {
		Class98_Sub46_Sub17 dialogue = Class185.method2628(id, -78, 2);
		dialogue.method1626((byte) -103);
		dialogue.dialogueText = name;
	}
}

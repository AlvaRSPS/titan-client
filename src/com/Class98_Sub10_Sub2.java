/* Class98_Sub10_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;

public final class Class98_Sub10_Sub2 extends Class98_Sub10 {
	public static final void method1008(int i) {
		try {
			if (i < -32) {
				Js5Exception.aClass79_3204.clear(77);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aha.F(" + i + ')');
		}
	}

	public static final void method1009(int i) {
		do {
			try {
				try {
					if ((RenderAnimDefinitionParser.anInt1948 ^ 0xffffffff) == -2) {
						int i_30_ = BConfigDefinition.aClass98_Sub31_Sub2_3122.method1360(-16257);
						if ((i_30_ ^ 0xffffffff) < -1 && BConfigDefinition.aClass98_Sub31_Sub2_3122.method1354(-3619)) {
							i_30_ -= QuickChatMessageType.anInt2911;
							if (i_30_ < 0) {
								i_30_ = 0;
							}
							BConfigDefinition.aClass98_Sub31_Sub2_3122.method1366(i_30_, (byte) 48);
							break;
						}
						BConfigDefinition.aClass98_Sub31_Sub2_3122.method1364(85);
						BConfigDefinition.aClass98_Sub31_Sub2_3122.method1349(-1);
						Class202.aClass308_1550 = null;
						do {
							if (LightIntensityDefinitionParser.aClass207_2025 == null) {
								RenderAnimDefinitionParser.anInt1948 = 0;
								if (!GameShell.cleanedStatics) {
									break;
								}
							}
							RenderAnimDefinitionParser.anInt1948 = 2;
						} while (false);
						Class81.aClass98_Sub7_620 = null;
					}
					if ((RenderAnimDefinitionParser.anInt1948 ^ 0xffffffff) != -4) {
						break;
					}
					int i_31_ = BConfigDefinition.aClass98_Sub31_Sub2_3122.method1360(-16257);
					if ((Class224_Sub3.anInt5037 ^ 0xffffffff) < (i_31_ ^ 0xffffffff) && BConfigDefinition.aClass98_Sub31_Sub2_3122.method1354(-3619)) {
						i_31_ += Class22.anInt219;
						if ((Class224_Sub3.anInt5037 ^ 0xffffffff) > (i_31_ ^ 0xffffffff)) {
							i_31_ = Class224_Sub3.anInt5037;
						}
						BConfigDefinition.aClass98_Sub31_Sub2_3122.method1366(i_31_, (byte) 72);
					} else {
						RenderAnimDefinitionParser.anInt1948 = 0;
						Class22.anInt219 = 0;
					}
				} catch (Exception exception) {
					exception.printStackTrace();
					BConfigDefinition.aClass98_Sub31_Sub2_3122.method1364(105);
					Class202.aClass308_1550 = null;
					LightIntensityDefinitionParser.aClass207_2025 = null;
					Class81.aClass98_Sub7_620 = null;
					Class116.aClass98_Sub31_Sub2_965 = null;
					RenderAnimDefinitionParser.anInt1948 = 0;
				}
				break;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "aha.E(" + i + ')');
			}
		} while (false);
	}

	public static final String setClanChatName(String string, int i) {
		String text = Class18.longToString(Class156_Sub2.stringToLong((byte) -125, string), -120);
		if (text == null) {
			text = "";
		}
		return text;
	}

	private int anInt5545 = 32768;

	public Class98_Sub10_Sub2() {
		super(3, false);
	}

	@Override
	public final void method1001(byte i) {
		try {
			if (i != 66) {
				method991(122, null, (byte) 17);
			}
			Class98_Sub31_Sub4.initializeTrig(0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aha.I(" + i + ')');
		}
	}

	@Override
	public final int[] method990(int i, int i_16_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_16_);
			if (this.aClass16_3863.aBoolean198) {
				int[] is_17_ = method1000(i_16_, 1, 0);
				int[] is_18_ = method1000(i_16_, 2, 0);
				for (int i_19_ = 0; (Class25.anInt268 ^ 0xffffffff) < (i_19_ ^ 0xffffffff); i_19_++) {
					int i_20_ = is_17_[i_19_] >> -2049795036 & 0xff;
					int i_21_ = anInt5545 * is_18_[i_19_] >> 329150252;
					int i_22_ = i_21_ * Class278_Sub1.COSINE[i_20_] >> -862649204;
					int i_23_ = aa_Sub2.SINE[i_20_] * i_21_ >> -1243350708;
					int i_24_ = Class329.anInt2761 & (i_22_ >> 820741420) + i_19_;
					int i_25_ = SoftwareNativeHeap.anInt6075 & (i_23_ >> 1296780) + i_16_;
					int[] is_26_ = method1000(i_25_, 0, 0);
					is[i_19_] = is_26_[i_24_];
				}
			}
			if (i != 255) {
				method1008(16);
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aha.G(" + i + ',' + i_16_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_27_) {
		try {
			int i_28_ = i;
			do {
				if ((i_28_ ^ 0xffffffff) != -1) {
					if ((i_28_ ^ 0xffffffff) != -2) {
						break;
					}
				} else {
					anInt5545 = class98_sub22.readShort((byte) 127) << 568932100;
					break;
				}
				this.aBoolean3861 = (class98_sub22.readUnsignedByte((byte) -126) ^ 0xffffffff) == -2;
			} while (false);
			if (i_27_ >= -92) {
				anInt5545 = 33;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aha.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_27_ + ')');
		}
	}

	@Override
	public final int[][] method997(int i, int i_0_) {
		try {
			if (i > -76) {
				anInt5545 = -101;
			}
			int[][] is = this.aClass223_3859.method2828(i_0_, 0);
			if (this.aClass223_3859.aBoolean1683) {
				int[] is_1_ = method1000(i_0_, 1, 0);
				int[] is_2_ = method1000(i_0_, 2, 0);
				int[] is_3_ = is[0];
				int[] is_4_ = is[1];
				int[] is_5_ = is[2];
				for (int i_6_ = 0; (Class25.anInt268 ^ 0xffffffff) < (i_6_ ^ 0xffffffff); i_6_++) {
					int i_7_ = 255 * is_1_[i_6_] >> 328402860 & 0xff;
					int i_8_ = is_2_[i_6_] * anInt5545 >> 299114828;
					int i_9_ = Class278_Sub1.COSINE[i_7_] * i_8_ >> -1083056916;
					int i_10_ = aa_Sub2.SINE[i_7_] * i_8_ >> -1641503572;
					int i_11_ = Class329.anInt2761 & (i_9_ >> 1756603788) + i_6_;
					int i_12_ = SoftwareNativeHeap.anInt6075 & (i_10_ >> -1134552788) + i_0_;
					int[][] is_13_ = method994(i_12_, 24431, 0);
					is_3_[i_6_] = is_13_[0][i_11_];
					is_4_[i_6_] = is_13_[1][i_11_];
					is_5_[i_6_] = is_13_[2][i_11_];
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "aha.C(" + i + ',' + i_0_ + ')');
		}
	}
}

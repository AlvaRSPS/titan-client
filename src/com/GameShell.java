
/* Applet_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Method;
import java.net.URL;

import com.jagex.Launcher;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.core.timetools.timebase.FrameTimeBase;
import com.jagex.game.client.archive.Archive;
import com.jagex.game.client.definition.FloorOverlayDefinition;
import com.jagex.game.toolkit.model.OpenGlModelRenderer;

import jagex3.jagmisc.jagmisc;

public abstract class GameShell extends Applet implements Runnable, FocusListener, WindowListener {
	public static boolean			aBoolean12;
	public static boolean			aBoolean16;
	public static boolean			aBoolean17;
	public static boolean			aBoolean5;
	public static boolean			aBoolean6;
	public static boolean			aBoolean8;
	public static GameShell			activeGameShell			= null;
	public static int				anInt10;
	public static int				anInt11;
	public static int				anInt13;
	public static int				anInt14;
	public static int				anInt15;
	public static int				anInt18;
	public static int				anInt19;
	public static int				anInt2;
	public static int				anInt20;
	public static int				anInt21;
	public static int				anInt22;
	public static int				anInt4;
	public static int				anInt7;
	public static int				anInt9;
	public static Applet			applet;
	public static int				availableProcessors		= 1;
	public static Canvas			canvas;
	public static boolean			cleanedStatics;
	public static SignLink			errorSignLink;
	public static boolean			focus;
	public static volatile boolean	focusIn					= true;
	public static int				fpsValue				= 0;
	public static Frame				frame;
	public static int				frameHeight;
	public static FrameTimeBase		frameTimeBase;
	public static int				frameWidth;
	public static volatile boolean	fullRedraw				= true;
	public static Frame				fullScreenFrame;
	public static Applet			gameApplet;
	public static long				graphicsUpdateInterval	= 20000000L;
	public static boolean			haveJava5OrBetter		= false;
	public static int				height;
	public static volatile long		lastCanvasReplace		= 0L;
	public static int				leftMargin				= 0;
	public static int				loadCount				= 0;
	public static long[]			logicTimeTracker		= new long[32];
	public static int				logicTimeTrackerPtr;
	public static int				maxHeap;
	public static int				missedLogicSteps;
	public static volatile boolean	recommendCanvasReplace	= false;
	public static int				renderTimer;
	public static long[]			renderTimeTracker		= new long[32];
	public static int				renderTimeTrackerPtr;
	public static int				revision;
	public static boolean			shuttingDown			= false;
	public static SignLink			signLink;
	public static long				stopTimer;
	public static int				topMargin				= 0;
	public static int				width;

	public static final void createDummyActionEvent(SignLink signLink, int i, Object object) {
		do {
			if (signLink.systemEventQueue != null) {
				for (int sleepCtr = 0; (sleepCtr ^ 0xffffffff) > -51 && signLink.systemEventQueue.peekEvent() != null; sleepCtr++) {
					TimeTools.sleep(0, 1L);
				}
				if (i == 31668) {
					if (object == null) {
						break;
					}
					signLink.systemEventQueue.postEvent(new ActionEvent(object, 1001, "dummy"));
				}
				break;
			}
			break;
		} while (false);
	}

	public static final int method81(int i, byte i_0_, int i_1_) {
		if (i_0_ != -9) {
			method96(null, -42, null, -65, 42, 54, -75, 7, null, false, -21, 124, 62, -66, 123);
		}
		int i_2_ = 0;
		for (/**/; (i ^ 0xffffffff) < -1; i--) {
			i_2_ = i_2_ << -2140156575 | 0x1 & i_1_;
			i_1_ >>>= 1;
		}
		return i_2_;
	}

	public static final int method96(ClipMap class243, int i, int[] is, int i_19_, int i_20_, int i_21_, int i_22_, int i_23_, int[] is_24_, boolean bool, int i_25_, int i_26_, int i_27_, int i_28_, int i_29_) {
		for (int i_30_ = 0; i_30_ < 128; i_30_++) {
			for (int i_31_ = 0; i_31_ < 128; i_31_++) {
				PlayerUpdateMask.anIntArrayArray528[i_30_][i_31_] = 0;
				Archive.anIntArrayArray2846[i_30_][i_31_] = 99999999;
			}
		}
		boolean bool_32_;
		if (i != 1) {
			if ((i ^ 0xffffffff) == -3) {
				bool_32_ = OpenGlModelRenderer.method2384(class243, i_22_, i_29_, i_19_, i_26_, i_20_, i_28_, i_27_ ^ 0x29ddf632, i_21_, i_25_, i_23_);
			} else {
				bool_32_ = Class98_Sub46_Sub10.method1568(i, i_22_, i_28_, i_20_, i_23_, (byte) 20, i_26_, i_19_, i_29_, i_25_, i_21_, class243);
			}
		} else {
			bool_32_ = Class98_Sub43_Sub4.method1506(i_26_, 14664, i_22_, i_28_, i_19_, i_25_, class243, i_21_, i_20_, i_29_, i_23_);
		}
		int i_33_ = i_28_ + -64;
		int i_34_ = i_23_ + -64;
		if (i_27_ != 48) {
			anInt2 = -57;
		}
		int i_35_ = FloorOverlayDefinition.anInt1539;
		int i_36_ = Class22.anInt217;
		if (!bool_32_) {
			if (bool) {
				int i_37_ = 2147483647;
				int i_38_ = 2147483647;
				int i_39_ = 10;
				for (int i_40_ = -i_39_ + i_25_; (i_40_ ^ 0xffffffff) >= (i_25_ + i_39_ ^ 0xffffffff); i_40_++) {
					for (int i_41_ = -i_39_ + i_21_; i_39_ + i_21_ >= i_41_; i_41_++) {
						int i_42_ = i_40_ - i_33_;
						int i_43_ = i_41_ - i_34_;
						if (i_42_ >= 0 && (i_43_ ^ 0xffffffff) <= -1 && i_42_ < 128 && i_43_ < 128 && Archive.anIntArrayArray2846[i_42_][i_43_] < 100) {
							int i_44_ = 0;
							if (i_25_ > i_40_) {
								i_44_ = -i_40_ + i_25_;
							} else if (i_25_ + i_29_ + -1 < i_40_) {
								i_44_ = -i_29_ + -i_25_ - -1 + i_40_;
							}
							int i_45_ = 0;
							if ((i_21_ ^ 0xffffffff) >= (i_41_ ^ 0xffffffff)) {
								if (i_41_ > i_22_ + i_21_ - 1) {
									i_45_ = i_41_ + 1 + -i_22_ + -i_21_;
								}
							} else {
								i_45_ = -i_41_ + i_21_;
							}
							int i_46_ = i_44_ * i_44_ + i_45_ * i_45_;
							if (i_37_ > i_46_ || (i_37_ ^ 0xffffffff) == (i_46_ ^ 0xffffffff) && Archive.anIntArrayArray2846[i_42_][i_43_] < i_38_) {
								i_36_ = i_41_;
								i_35_ = i_40_;
								i_37_ = i_46_;
								i_38_ = Archive.anIntArrayArray2846[i_42_][i_43_];
							}
						}
					}
				}
				if (i_37_ == 2147483647) {
					return -1;
				}
			} else {
				return -1;
			}
		}
		if ((i_28_ ^ 0xffffffff) == (i_35_ ^ 0xffffffff) && (i_36_ ^ 0xffffffff) == (i_23_ ^ 0xffffffff)) {
			return 0;
		}
		int i_47_ = 0;
		Class359.anIntArray3060[i_47_] = i_35_;
		StartupStage.anIntArray580[i_47_++] = i_36_;
		int i_49_;
		int i_48_ = i_49_ = PlayerUpdateMask.anIntArrayArray528[-i_33_ + i_35_][-i_34_ + i_36_];
		while ((i_28_ ^ 0xffffffff) != (i_35_ ^ 0xffffffff) || (i_36_ ^ 0xffffffff) != (i_23_ ^ 0xffffffff)) {
			if ((i_49_ ^ 0xffffffff) != (i_48_ ^ 0xffffffff)) {
				i_49_ = i_48_;
				Class359.anIntArray3060[i_47_] = i_35_;
				StartupStage.anIntArray580[i_47_++] = i_36_;
			}
			if ((i_48_ & 0x2) == 0) {
				if ((i_48_ & 0x8) != 0) {
					i_35_--;
				}
			} else {
				i_35_++;
			}
			if ((i_48_ & 0x1 ^ 0xffffffff) == -1) {
				if ((0x4 & i_48_ ^ 0xffffffff) != -1) {
					i_36_--;
				}
			} else {
				i_36_++;
			}
			i_48_ = PlayerUpdateMask.anIntArrayArray528[-i_33_ + i_35_][-i_34_ + i_36_];
		}
		int i_50_ = 0;
		while ((i_47_-- ^ 0xffffffff) < -1) {
			is[i_50_] = Class359.anIntArray3060[i_47_];
			is_24_[i_50_++] = StartupStage.anIntArray580[i_47_];
			if ((is.length ^ 0xffffffff) >= (i_50_ ^ 0xffffffff)) {
				break;
			}
		}
		return i_50_;
	}

	public static final void pollAvailableProcessors(int i) {
		do {
			try {
				Method getAvailableProcessors = Runtime.class.getMethod("availableProcessors");
				if (getAvailableProcessors != null) {
					Runtime runtime = Runtime.getRuntime();
					Integer integer = (Integer) getAvailableProcessors.invoke(runtime);
					availableProcessors = integer.intValue();
				}
			} catch (Exception exception) {
				/* empty */
			}
			break;
		} while (false);
	}

	public static final void pollHeapSize(int i) {
		do {
			if (!signLink.msJava) {
				try {
					Method getMaxMemory = Runtime.class.getMethod("maxMemory");
					if (getMaxMemory != null) {
						Runtime runtime = Runtime.getRuntime();
						Long maxMemory = (Long) getMaxMemory.invoke(runtime);
						maxHeap = 1 + (int) (maxMemory.longValue() / 1048576L);
					}
					break;
				} catch (Exception exception) {
					break;
				}
			}
			maxHeap = 96;
		} while (false);
	}

	public static final void provideLoaderApplet(Applet applet) {
		GameShell.applet = applet;
	}

	private boolean	crashed	= false;

	private boolean	jagMiscClean;

	public GameShell() {
		jagMiscClean = false;
	}

	synchronized void addCanvas(int i) {
		if (canvas != null) {
			canvas.removeFocusListener(this);
			canvas.getParent().setBackground(Color.black);
			canvas.getParent().remove(canvas);
		}
		Container container;
		if (fullScreenFrame != null) {
			container = fullScreenFrame;
		} else if (frame != null) {
			container = frame;
		} else if (applet != null) {
			container = applet;
		} else {
			container = activeGameShell;
		}
		container.setLayout(null);
		canvas = new Canvas_Sub1(this);
		container.add(canvas);
		canvas.setSize(frameWidth, frameHeight);
		canvas.setVisible(true);
		if (frame != container) {
			canvas.setLocation(leftMargin, topMargin);
		} else {
			Insets insets = GameShell.frame.getInsets();
			canvas.setLocation(leftMargin + insets.left, topMargin + insets.top);
		}
		canvas.addFocusListener(this);
		canvas.requestFocus();
		focus = true;
		focusIn = true;
		fullRedraw = true;
		recommendCanvasReplace = false;
		if (i != 0) {
			crashed = true;
		}
		lastCanvasReplace = TimeTools.getCurrentTime(-47);
	}

	@Override
	public final void destroy() {
		if (activeGameShell == this && !shuttingDown) {
			stopTimer = TimeTools.getCurrentTime(-47);
			TimeTools.sleep(0, 5000L);
			errorSignLink = null;
			shutDown(false, 18192);
		}
	}

	public final void error(byte i, String string) {
		if (!crashed) {
			crashed = true;
			System.out.println("error_game_" + string);
			try {
				JavaScriptInterface.callJsMethod("loggedout", applet, -26978);
			} catch (Throwable throwable) {
				/* empty */
			}
			try {
				getAppletContext().showDocument(new URL(getCodeBase(), "error_game_" + string + ".ws"), "_top");
			} catch (Exception exception) {
				/* empty */
			}
		}
	}

	@Override
	public final void focusGained(FocusEvent focusevent) {
		focusIn = true;
		fullRedraw = true;
	}

	@Override
	public final void focusLost(FocusEvent focusevent) {
		focusIn = false;
	}

	String generateDebugInfoString(int i) {
		return null;
	}

	@Override
	public final AppletContext getAppletContext() {
		if (frame != null) {
			return null;
		}
		if (applet != null && applet != this) {
			return applet.getAppletContext();
		}
		return super.getAppletContext();
	}

	@Override
	public final URL getCodeBase() {
		if (frame != null) {
			return null;
		}
		if (applet != null && applet != this) {
			return applet.getCodeBase();
		}
		return super.getCodeBase();
	}

	@Override
	public final URL getDocumentBase() {
		if (frame != null) {
			return null;
		}
		if (applet != null && applet != this) {
			return applet.getDocumentBase();
		}
		return super.getDocumentBase();
	}

	@Override
	public final String getParameter(String string) {
		if (frame != null) {
			return null;
		}
		if (applet != null && applet != this) {
			return applet.getParameter(string);
		}
		return super.getParameter(string);
	}

	@Override
	public abstract void init();

	public final boolean loadJacLib(int i) {
		return AdvancedMemoryCache.loadNative("jaclib", (byte) -36);
	}

	public final boolean loadJagMisc(int i) {
		return AdvancedMemoryCache.loadNative("jagmisc", (byte) -36);
	}

	public final boolean loadJagTheora(boolean bool) {
		return AdvancedMemoryCache.loadNative("jagtheora", (byte) -36);
	}

	public abstract void mainInit(int i);

	public abstract void mainLoop(byte i);

	private final void mainLoopWrapper(int i) {
		if (i != 6341) {
			anInt2 = -22;
		}
		long currentTime = TimeTools.getCurrentTime(-47);
		logicTimeTracker[logicTimeTrackerPtr] = currentTime;
		logicTimeTrackerPtr = 0x1f & 1 + logicTimeTrackerPtr;
		synchronized (this) {
			focus = focusIn;
		}
		mainLoop((byte) -6);
	}

	public abstract void mainQuit(boolean bool);

	public abstract void mainRedraw(int i);

	private final void mainRedrawWrapper(int i) {
		long currentTime = TimeTools.getCurrentTime(i + -784382384);
		long lastTime = renderTimeTracker[renderTimeTrackerPtr];
		renderTimeTracker[renderTimeTrackerPtr] = currentTime;
		renderTimeTrackerPtr = 0x1f & renderTimeTrackerPtr - -1;
		if ((lastTime ^ 0xffffffffffffffffL) != -1L && currentTime > lastTime) {
			int deltaTime = (int) (currentTime + -lastTime);
			fpsValue = ((deltaTime >> 784382337) + 32000) / deltaTime;
		}
		do {
			if ((renderTimer++ ^ 0xffffffff) < -51) {
				fullRedraw = true;
				renderTimer -= 50;
				canvas.setSize(frameWidth, frameHeight);
				canvas.setVisible(true);
				if (frame == null || fullScreenFrame != null) {// WindowMode.fullScreenFrame
																// != null ?
					canvas.setLocation(leftMargin, topMargin);
					if (!cleanedStatics) {
						break;
					}
				}
				Insets insets = frame.getInsets();
				canvas.setLocation(insets.left + leftMargin, topMargin + insets.top);
			}
		} while (false);
		mainRedraw(i + -784382227);
	}

	public final boolean method89(int i) {
		String string = getDocumentBase().getHost().toLowerCase();
		if (string.equals("jagex.com") || string.endsWith(".jagex.com")) {
			return true;
		}
		if (string.equals("runescape.com") || string.endsWith(".runescape.com")) {
			return true;
		}
		if (string.equals("stellardawn.com") || string.endsWith(".stellardawn.com")) {
			return true;
		}
		if (string.endsWith("127.0.0.1")) {
			return true;
		}
		if (string.equalsIgnoreCase(Launcher.mainurl)) {
			return true;
		}
		for (/**/; string.length() > 0 && string.charAt(-1 + string.length()) >= '0' && (string.charAt(-1 + string.length()) ^ 0xffffffff) >= -58; string = string.substring(0, -1 + string.length())) {
			/* empty */
		}
		if (string.endsWith("192.168.1.")) {
			return true;
		}
		error((byte) 112, "invalidhost");
		return false;
	}

	@Override
	public final synchronized void paint(Graphics graphics) {
		do {
			if (activeGameShell == this && !shuttingDown) {
				fullRedraw = true;
				if (!haveJava5OrBetter || -lastCanvasReplace + TimeTools.getCurrentTime(-47) <= 1000L) {
					break;
				}
				Rectangle rectangle = graphics.getClipBounds();
				if (rectangle == null || (rectangle.width ^ 0xffffffff) <= (GameShell.width ^ 0xffffffff) && (rectangle.height ^ 0xffffffff) <= (height ^ 0xffffffff)) {
					recommendCanvasReplace = true;
				}
			}
			break;
		} while (false);
	}

	@Override
	public final void run() {
		do {
			try {
				if (SignLink.java_vendor != null) {
					String javaVendor = SignLink.java_vendor.toLowerCase();
					if (javaVendor.indexOf("sun") == -1 && (javaVendor.indexOf("apple") ^ 0xffffffff) == 0) {
						if ((javaVendor.indexOf("ibm") ^ 0xffffffff) != 0 && (SignLink.javaVersion == null || SignLink.javaVersion.equals("1.4.2"))) {
							error((byte) 87, "wrongjava");
							break;
						}
					} else {
						String javaVersion = SignLink.javaVersion;
						if (javaVersion.equals("1.1") || javaVersion.startsWith("1.1.") || javaVersion.equals("1.2") || javaVersion.startsWith("1.2.")) {
							error((byte) 88, "wrongjava");
							break;
						}
					}
				}
				if (SignLink.javaVersion != null && SignLink.javaVersion.startsWith("1.")) {
					int index = 2;
					int minorVersion = 0;
					for (/**/; (index ^ 0xffffffff) > (SignLink.javaVersion.length() ^ 0xffffffff); index++) {
						int currentChar = SignLink.javaVersion.charAt(index);
						if (currentChar < 48 || (currentChar ^ 0xffffffff) < -58) {
							break;
						}
						minorVersion = 10 * minorVersion - (48 + -currentChar);
					}
					if (minorVersion >= 5) {
						haveJava5OrBetter = true;
					}
				}
				Applet applet = activeGameShell;
				if (GameShell.applet != null) {
					applet = GameShell.applet;
				}
				Method setFocusCycleRoot = SignLink.setFocusCycleRoot;
				if (setFocusCycleRoot != null) {
					setFocusCycleRoot.invoke(applet, Boolean.TRUE);
				}
				pollHeapSize(107);
				pollAvailableProcessors(12345);
				addCanvas(0);
				mainInit(-13395);
				frameTimeBase = FrameTimeBase.create(72);
				while ((stopTimer ^ 0xffffffffffffffffL) == -1L || (TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL) > (stopTimer ^ 0xffffffffffffffffL)) {
					missedLogicSteps = frameTimeBase.waitForNextFrame(0, graphicsUpdateInterval);
					for (int i = 0; missedLogicSteps > i; i++) {
						mainLoopWrapper(6341);
					}
					mainRedrawWrapper(784382337);
					createDummyActionEvent(signLink, 31668, canvas);
				}
			} catch (ThreadDeath threaddeath) {
				throw threaddeath;
			} catch (Throwable throwable) {
				Class305_Sub1.reportError(throwable, -123, generateDebugInfoString(0));
				error((byte) -88, "crash");
			} finally {
				shutDown(true, 18192);
			}
		} while (false);
	}

	private final void shutDown(boolean bool, int i) {
		synchronized (this) {
			if (shuttingDown) {
				return;
			}
			shuttingDown = true;
		}
		System.out.println("Shutdown start - clean:" + bool);
		if (applet != null) {
			applet.destroy();
		}
		try {
			mainQuit(false);
		} catch (Exception exception) {
			/* empty */
		}
		if (jagMiscClean) {
			try {
				jagmisc.quit();
			} catch (Throwable throwable) {
				/* empty */
			}
			jagMiscClean = false;
		}
		NativeDestructorManager.setAddAllowed(-87, true);
		AdvancedMemoryCache.unloadNatives(-91);
		if (canvas != null) {
			try {
				canvas.removeFocusListener(this);
				canvas.getParent().remove(canvas);
			} catch (Exception exception) {
				/* empty */
			}
		}
		if (signLink != null) {
			try {
				signLink.shutDown(103);
			} catch (Exception exception) {
				/* empty */
			}
		}
		if (frame != null) {
			frame.setVisible(false);
			frame.dispose();
			frame = null;
		}
		System.out.println("Shutdown complete - clean:" + bool);
	}

	@Override
	public final void start() {
		if (activeGameShell == this && !shuttingDown) {
			stopTimer = 0L;
		}
	}

	public final void startApplet(int rev, int w, int i_4_, int indexCount, String gameName, int cacheId, int h) {
		try {
			if (activeGameShell != null) {
				loadCount++;
				if ((loadCount ^ 0xffffffff) <= -4) {
					error((byte) 74, "alreadyloaded");
				} else {
					getAppletContext().showDocument(getDocumentBase(), "_self");
				}
			} else {
				revision = rev;
				gameApplet = applet;
				width = frameWidth = h;
				height = frameHeight = w;
				leftMargin = 0;
				if (i_4_ == 4) {
					topMargin = 0;
					activeGameShell = this;
					errorSignLink = signLink = new SignLink(cacheId, gameName, indexCount, applet != null);
					SignLinkRequest signLinkRequest = signLink.startThread(1, this, i_4_ + -3);
					while ((signLinkRequest.status ^ 0xffffffff) == -1) {
						TimeTools.sleep(0, 10L);
					}
				}
			}
		} catch (Throwable throwable) {
			Class305_Sub1.reportError(throwable, -128, null);
			error((byte) -50, "crash");
		}
	}

	public final void startWindowed(boolean bool, String string, int i, int width, int i_10_, boolean resizeable, int revision, int height) {
		try {
			GameShell.activeGameShell = this;
			GameShell.leftMargin = 0;
			GameShell.height = GameShell.frameHeight = width;
			GameShell.width = GameShell.frameWidth = height;
			GameShell.gameApplet = null;
			GameShell.topMargin = 0;
			GameShell.revision = revision;
			GameShell.frame = new Frame();
			GameShell.frame.setTitle("Titan");
			GameShell.frame.setResizable(true);
			GameShell.frame.addWindowListener(this);
			GameShell.frame.setVisible(true);
			GameShell.frame.toFront();
			Insets insets = GameShell.frame.getInsets();
			GameShell.frame.setSize(insets.left + GameShell.width + insets.right, insets.bottom + GameShell.height - -insets.top);
			GameShell.errorSignLink = GameShell.signLink = new SignLink(i_10_, string, i, resizeable);
			SignLinkRequest request = GameShell.signLink.startThread(1, this, 1);
			while ((request.status ^ 0xffffffff) == -1) {
				TimeTools.sleep(0, 10L);
			}
		} catch (Exception exception) {
			Class305_Sub1.reportError(exception, -128, "Error starting windowed GameShell!");
		}
	}

	@Override
	public final void stop() {
		if (this == activeGameShell && !shuttingDown) {
			stopTimer = TimeTools.getCurrentTime(-47) + 4000L;
		}
	}

	@Override
	public final void update(Graphics graphics) {
		paint(graphics);
	}

	@Override
	public final void windowActivated(WindowEvent windowevent) {
		/* empty */
	}

	@Override
	public final void windowClosed(WindowEvent windowevent) {
		/* empty */
	}

	@Override
	public final void windowClosing(WindowEvent windowevent) {
		destroy();
	}

	@Override
	public final void windowDeactivated(WindowEvent windowevent) {
		/* empty */
	}

	@Override
	public final void windowDeiconified(WindowEvent windowevent) {
		/* empty */
	}

	@Override
	public final void windowIconified(WindowEvent windowevent) {
		/* empty */
	}

	@Override
	public final void windowOpened(WindowEvent windowevent) {
		/* empty */
	}
}

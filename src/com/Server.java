/* Class354 - Decompiled by JODE
 */ package com; /*
					*/

public final class Server {
	public static Class98_Sub9 aClass98_Sub9_3014 = new Class98_Sub9(0, 0);

	public static final Class277 method3872(byte i) {
		if (i != 83) {
			aClass98_Sub9_3014 = null;
		}
		try {
			return (Class277) Class.forName("com.Class277_Sub1").newInstance();
		} catch (Throwable throwable) {
			return null;
		}

	}

	public String	host;
	public int		index;
	public int		initialPort;
	int				secondPort;

	private boolean	useInitialPort	= true;

	private boolean	useProxy		= false;

	public Server() {
		secondPort = 43594;
		initialPort = 43594;
	}

	public final boolean equals(int i, Server server) {
		if (server != null) {
			return !(index != server.index || !host.equals(server.host));
		}
		return false;
	}

	public final SignLinkRequest openSocket(int i, SignLink signLink) {
		return signLink.openSocket(host, false, !useInitialPort ? initialPort : secondPort, useProxy);
	}

	public final void rotateConnectionMethod(int i) {
		do {
			if (!useInitialPort) {
				useInitialPort = true;
				useProxy = true;
			} else if (!useProxy) {
				useInitialPort = false;
			} else {
				useProxy = false;
			}
			break;
		} while (false);
	}
}

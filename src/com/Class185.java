/* Class185 - Decompiled by JODE
 * Visit http: jode.sourceforge.net/
 */
package com;

import com.jagex.core.collections.LinkedList;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.anticheat.ReflectionAntiCheat;
import com.jagex.game.client.archive.Archive;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.WorldMapInfoDefinition;
import com.jagex.game.client.definition.parser.AnimationDefinitionParser;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;
import com.jagex.game.client.definition.parser.GraphicsDefinitionParser;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.ParamDefinitionParser;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;
import com.jagex.game.client.preferences.CpuUsagePreferenceField;
import com.jagex.game.client.preferences.ParticlesPreferenceField;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatCategoryParser;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.client.ui.loading.impl.AwtLoadingScreen;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementType;
import com.jagex.game.client.ui.loading.impl.elements.config.DecoratedProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.RotatingSpriteLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.input.RtKeyEvent;
import com.jagex.game.input.RtKeyListener;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.font.FontSpecifications;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;
import com.jagex.game.toolkit.matrix.NativeMatrix;
import com.jagex.game.toolkit.matrix.OpenGlMatrix;
import com.jagex.game.toolkit.shadow.OpenGlShadow;

public final class Class185 {
	public static Class246_Sub4_Sub2_Sub1[] aClass246_Sub4_Sub2_Sub1Array1445;

	public static final Class98_Sub46_Sub17 method2628(int id, int i_0_, int i_1_) {
		Class98_Sub46_Sub17 dialogue = (Class98_Sub46_Sub17) Class76_Sub4.aClass377_3738.get((long) i_1_ << 1046340512 | id, -1);
		if (dialogue == null) {
			dialogue = new Class98_Sub46_Sub17(i_1_, id);
			Class76_Sub4.aClass377_3738.put(dialogue, dialogue.hash, -1);
		}
		return dialogue;
	}

	public static final void method2629(byte i) {
		if ((Class98_Sub10_Sub6.anInt5569 ^ 0xffffffff) < -2) {
			Class98_Sub36.anInt4161 = WorldMapInfoDefinition.logicTimer;
			Class98_Sub10_Sub6.anInt5569--;
		}
		if (Class76_Sub9.aBoolean3788) {
			Class76_Sub9.aBoolean3788 = false;
			Canvas_Sub1.method118((byte) 104);
		} else {
			if (!Player.menuOpen) {
				Class46.method435((byte) 85);
			}
			for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > -101; i_2_++) {
				if (!Class98_Sub10_Sub24.method1076(i + -58)) {
					break;
				}
			}
			if (client.clientState == 10) {
				while (Class92.method893(118)) {
					OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(i + 313, Class98_Sub10_Sub7.aClass171_5571, Class331.aClass117_2811);
					frame.packet.writeByte(0, i + 138);
					int i_3_ = frame.packet.position;
					ReflectionAntiCheat.process((byte) -61, frame.packet);
					frame.packet.method1211((byte) 91, -i_3_ + frame.packet.position);
					Class98_Sub10_Sub29.sendPacket(false, frame);
				}
				if (Class284.aClass98_Sub4_2167 != null) {
					if (Class284.aClass98_Sub4_2167.anInt3827 != -1) {
						OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class98_Sub10_Sub22.aClass171_5652, Class331.aClass117_2811);
						class98_sub11.packet.writeShort(Class284.aClass98_Sub4_2167.anInt3827, 1571862888);
						Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
						Class284.aClass98_Sub4_2167 = null;
						CursorDefinitionParser.aLong121 = 30000L + TimeTools.getCurrentTime(-47);
					}
				} else if ((TimeTools.getCurrentTime(i ^ 0x1a) ^ 0xffffffffffffffffL) <= (CursorDefinitionParser.aLong121 ^ 0xffffffffffffffffL)) {
					Class284.aClass98_Sub4_2167 = Class48_Sub2_Sub1.aClass265_5531.method3229(i ^ ~0xb, client.server.host);
				}
				RtMouseEvent class98_sub17 = (RtMouseEvent) VarPlayerDefinition.aClass148_1284.getFirst(32);
				if (class98_sub17 != null || -2000L + TimeTools.getCurrentTime(-47) > LoadingScreenElementType.aLong954) {
					OutgoingPacket frame = null;
					int i_4_ = 0;
					for (RtMouseEvent class98_sub17_5_ = (RtMouseEvent) CpuUsagePreferenceField.aClass148_3703.getFirst(32); class98_sub17_5_ != null; class98_sub17_5_ = (RtMouseEvent) CpuUsagePreferenceField.aClass148_3703.getNext(107)) {
						if (frame != null && (frame.packet.position - i_4_ ^ 0xffffffff) <= -241) {
							break;
						}
						class98_sub17_5_.unlink(126);
						int i_6_ = class98_sub17_5_.getMouseY(48);
						if (i_6_ < -1) {
							i_6_ = -1;
						} else if (i_6_ > 65534) {
							i_6_ = 65534;
						}
						int i_7_ = class98_sub17_5_.getMouseX(123);
						if ((i_7_ ^ 0xffffffff) > 0) {
							i_7_ = -1;
						} else if (i_7_ > 65534) {
							i_7_ = 65534;
						}
						if (i_7_ != OpenGLHeightMapToNormalMapConverter.anInt980 || (SunDefinitionParser.anInt960 ^ 0xffffffff) != (i_6_ ^ 0xffffffff)) {
							if (frame == null) {
								frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, WorldMapInfoDefinitionParser.aClass171_2853, Class331.aClass117_2811);
								frame.packet.writeByte(0, i ^ 0x4f);
								i_4_ = frame.packet.position;
							}
							int i_8_ = -OpenGLHeightMapToNormalMapConverter.anInt980 + i_7_;
							OpenGLHeightMapToNormalMapConverter.anInt980 = i_7_;
							int i_9_ = -SunDefinitionParser.anInt960 + i_6_;
							SunDefinitionParser.anInt960 = i_6_;
							int i_10_ = (int) ((class98_sub17_5_.method1154(true) - LoadingScreenElementType.aLong954) / 20L);
							if (i_10_ >= 8 || i_8_ < -32 || (i_8_ ^ 0xffffffff) < -32 || (i_9_ ^ 0xffffffff) > 31 || (i_9_ ^ 0xffffffff) < -32) {
								if ((i_10_ ^ 0xffffffff) <= -33 || (i_8_ ^ 0xffffffff) > 127 || (i_8_ ^ 0xffffffff) < -128 || i_9_ < -128 || i_9_ > 127) {
									if (i_10_ >= 32) {
										frame.packet.writeShort(i_10_ + 57344, 1571862888);
										if (i_7_ == 1 || i_6_ == -1) {
											frame.packet.writeInt(1571862888, -2147483648);
										} else {
											frame.packet.writeInt(1571862888, i_7_ | i_6_ << 1539798800);
										}
									} else {
										frame.packet.writeByte(192 + i_10_, 85);
										if ((i_7_ ^ 0xffffffff) == -2 || i_6_ == -1) {
											frame.packet.writeInt(1571862888, -2147483648);
										} else {
											frame.packet.writeInt(i ^ ~0x5db0b95c, i_7_ | i_6_ << -422182576);
										}
									}
								} else {
									i_8_ += 128;
									frame.packet.writeByte(128 + i_10_, -98);
									i_9_ += 128;
									frame.packet.writeShort(i_9_ + (i_8_ << 1431182856), 1571862888);
								}
							} else {
								i_8_ += 32;
								i_9_ += 32;
								frame.packet.writeShort(i_9_ + (i_10_ << 1379774828) - -(i_8_ << -540290298), 1571862888);
							}
							LoadingScreenElementType.aLong954 = class98_sub17_5_.method1154(true);
						}
					}
					if (frame != null) {
						frame.packet.method1211((byte) 123, -i_4_ + frame.packet.position);
						Class98_Sub10_Sub29.sendPacket(false, frame);
					}
				}
				if (class98_sub17 != null) {
					long l = (class98_sub17.method1154(true) - Class98_Sub36.aLong4159) / 50L;
					Class98_Sub36.aLong4159 = class98_sub17.method1154(true);
					if (l > 32767L) {
						l = 32767L;
					}
					int i_11_ = class98_sub17.getMouseY(89);
					if ((i_11_ ^ 0xffffffff) > -1) {
						i_11_ = 0;
					} else if ((i_11_ ^ 0xffffffff) < -65536) {
						i_11_ = 65535;
					}
					int i_12_ = class98_sub17.getMouseX(122);
					if (i_12_ >= 0) {
						if (i_12_ > 65535) {
							i_12_ = 65535;
						}
					} else {
						i_12_ = 0;
					}
					int i_13_ = 0;
					if (class98_sub17.getEventType(-5) == 2) {
						i_13_ = 1;
					}
					int i_14_ = (int) l;
					OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class287_Sub2.aClass171_3270, Class331.aClass117_2811);
					class98_sub11.packet.writeShort(i_13_ << 391207055 | i_14_, i + 1571862941);
					class98_sub11.packet.writeInt(i + 1571862941, i_12_ | i_11_ << -1879594384);
					Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
				}
				if ((QuickChatCategory.keyReleaseCount ^ 0xffffffff) < -1) {
					OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, GraphicsBuffer.aClass171_4106, Class331.aClass117_2811);
					frame.packet.writeByte(3 * QuickChatCategory.keyReleaseCount, 78);
					for (int i_15_ = 0; (QuickChatCategory.keyReleaseCount ^ 0xffffffff) < (i_15_ ^ 0xffffffff); i_15_++) {
						RtKeyEvent interface7 = Archive.anInterface7Array2845[i_15_];
						long l = (interface7.getTimeStamp(-121) - AwtLoadingScreen.aLong3356) / 50L;
						AwtLoadingScreen.aLong3356 = interface7.getTimeStamp(-84);
						if ((l ^ 0xffffffffffffffffL) < -65536L) {
							l = 65535L;
						}
						frame.packet.writeByte(interface7.getKeyCode(true), 89);
						frame.packet.writeShort((int) l, i + 1571862941);
					}
					Class98_Sub10_Sub29.sendPacket(false, frame);
				}
				if ((Class42_Sub1_Sub1.anInt6210 ^ 0xffffffff) < -1) {
					Class42_Sub1_Sub1.anInt6210--;
				}
				if (ParticlesPreferenceField.aBoolean3656 && (Class42_Sub1_Sub1.anInt6210 ^ 0xffffffff) >= -1) {
					ParticlesPreferenceField.aBoolean3656 = false;
					Class42_Sub1_Sub1.anInt6210 = 20;
					OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(i + 313, Class246_Sub7.aClass171_5115, Class331.aClass117_2811);
					frame.packet.writeLEShort((int) Class119_Sub4.aFloat4740 >> -52235901, 17624);
					frame.packet.writeShort((int) RsFloatBuffer.aFloat5794 >> -449746077, i + 1571862941);
					Class98_Sub10_Sub29.sendPacket(false, frame);
				}
				if (GameShell.focus == !GrandExchangeOffer.aBoolean856) {
					GrandExchangeOffer.aBoolean856 = GameShell.focus;
					OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class230.aClass171_1727, Class331.aClass117_2811);
					frame.packet.writeByte(!GameShell.focus ? 0 : 1, -93);
					Class98_Sub10_Sub29.sendPacket(false, frame);
				}
				if (!OpenGlGround.aBoolean5207) {
					OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(i + 313, Class246_Sub1.aClass171_5068, Class331.aClass117_2811);
					frame.packet.writeByte(0, i + -27);
					int i_16_ = frame.packet.position;
					RSByteBuffer class98_sub22 = client.preferences.encode(true);
					frame.packet.method1217(class98_sub22.payload, class98_sub22.position, -1, 0);
					frame.packet.method1211((byte) 107, frame.packet.position - i_16_);
					Class98_Sub10_Sub29.sendPacket(false, frame);
					OpenGlGround.aBoolean5207 = true;
				}
				if (QuickChatCategory.aClass172ArrayArrayArray5948 != null) {
					if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) == -3) {
						Class98_Sub46_Sub3.method1541(73);
					} else if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) == -4) {
						AnimationDefinitionParser.method2620(0);
					}
				}
				if (Class4.aBoolean79) {
					Class4.aBoolean79 = false;
				} else {
					FileProgressMonitor.aFloat3405 /= 2.0F;
				}
				if (i != -53) {
					aClass246_Sub4_Sub2_Sub1Array1445 = null;
				}
				if (JavaNetworkWriter.aBoolean2575) {
					JavaNetworkWriter.aBoolean2575 = false;
				} else {
					MapRegion.aFloat2545 /= 2.0F;
				}
				NPCDefinition.method2294(0);
				if ((client.clientState ^ 0xffffffff) == -11) {
					Class329.method3708(-1);
					LightIntensityDefinitionParser.method3273(true);
					Animable.method3003(i ^ 0x54bc);
					Class224_Sub1.anInt5031++;
					if (Class224_Sub1.anInt5031 > 750) {
						Canvas_Sub1.method118((byte) 104);
					} else {
						Class50.method485(37);
						Class216.method2794((byte) -85);
						Class76_Sub10.method770(-256);
						for (int i_17_ = StartupStage.varValues.method2290(-115, true); i_17_ != -1; i_17_ = StartupStage.varValues.method2290(-87, false)) {
							Class349.method3838(i_17_, 1864);
							OpenGlMatrix.anIntArray4682[Class202.and(31, VarClientStringsDefinitionParser.anInt1844++)] = i_17_;
						}
						for (Class98_Sub46_Sub17 dialogue = Class98_Sub31_Sub4.method1384(i ^ 0x4b); dialogue != null; dialogue = Class98_Sub31_Sub4.method1384(-46)) {
							int i_18_ = dialogue.method1625((byte) -108);
							int i_19_ = dialogue.method1623(-101);
							if (i_18_ == 1) {
								Class76_Sub5.intGlobalConfigs[i_19_] = dialogue.anInt6054;
								Class66.aBoolean507 |= Class140.boolGlobalConfigs[i_19_];
								ItemDeque.anIntArray4257[Class202.and(Class246_Sub4_Sub2.anInt6181++, 31)] = i_19_;
							} else if (i_18_ != 2) {
								if ((i_18_ ^ 0xffffffff) != -4) {
									if (i_18_ == 4) {
										RtInterface interf = RtInterface.getInterface(i_19_, -9820);
										int i_20_ = dialogue.anInt6054;
										int i_21_ = dialogue.headId;
										int i_22_ = dialogue.anInt6053;
										if (interf.defaultMediaType != i_20_ || (i_21_ ^ 0xffffffff) != (interf.defaultMediaId ^ 0xffffffff) || i_22_ != interf.anInt2210) {
											interf.defaultMediaType = i_20_;
											interf.defaultMediaId = i_21_;
											interf.anInt2210 = i_22_;
											WorldMapInfoDefinitionParser.setDirty(1, interf);
										}
									} else if ((i_18_ ^ 0xffffffff) != -6) {
										if ((i_18_ ^ 0xffffffff) != -7) {
											if ((i_18_ ^ 0xffffffff) != -8) {
												if (i_18_ == 8) {
													RtInterface interf = RtInterface.getInterface(i_19_, i + -9767);
													if (dialogue.anInt6054 != interf.rotationX || (interf.rotationY ^ 0xffffffff) != (dialogue.headId ^ 0xffffffff) || dialogue.anInt6053 != interf.zoom) {
														interf.rotationX = dialogue.anInt6054;
														interf.zoom = dialogue.anInt6053;
														interf.rotationY = dialogue.headId;
														if (interf.unknown != -1) {
															if (interf.viewportWidth <= 0) {
																if (interf.width > 0) {
																	interf.zoom = 32 * interf.zoom / interf.width;
																}
															} else {
																interf.zoom = 32 * interf.zoom / interf.viewportWidth;
															}
														}
														WorldMapInfoDefinitionParser.setDirty(1, interf);
													}
												} else if ((i_18_ ^ 0xffffffff) != -10) {
													if ((i_18_ ^ 0xffffffff) != -11) {
														if (i_18_ == 11) {
															RtInterface interf = RtInterface.getInterface(i_19_, -9820);
															interf.vPosMode = (byte) 0;
															interf.hPosMode = (byte) 0;
															interf.renderX = interf.xPos = dialogue.anInt6054;
															interf.renderY = interf.yPos = dialogue.headId;
															WorldMapInfoDefinitionParser.setDirty(1, interf);
														} else if (i_18_ != 12) {
															if ((i_18_ ^ 0xffffffff) == -15) {
																RtInterface class293 = RtInterface.getInterface(i_19_, i ^ 0x266f);
																class293.defaultImage = dialogue.anInt6054;
															} else if ((i_18_ ^ 0xffffffff) != -16) {
																if (i_18_ != 16) {
																	if (i_18_ == 17) {
																		RtInterface class293 = RtInterface.getInterface(i_19_, -9820);
																		class293.videoId = dialogue.anInt6054;
																	}
																} else {
																	RtInterface class293 = RtInterface.getInterface(i_19_, -9820);
																	class293.fontId = dialogue.anInt6054;
																}
															} else {
																LightIntensityDefinitionParser.anInt2024 = dialogue.anInt6054;
																Class246_Sub3_Sub1_Sub2.anInt6251 = dialogue.headId;
																ParamDefinitionParser.aBoolean3110 = true;
															}
														} else {
															RtInterface class293 = RtInterface.getInterface(i_19_, -9820);
															int i_23_ = dialogue.anInt6054;
															if (class293 != null && (class293.type ^ 0xffffffff) == -1) {
																if (-class293.renderHeight + class293.scrollHeight < i_23_) {
																	i_23_ = class293.scrollHeight + -class293.renderHeight;
																}
																if ((i_23_ ^ 0xffffffff) > -1) {
																	i_23_ = 0;
																}
																if ((i_23_ ^ 0xffffffff) != (class293.scrollY ^ 0xffffffff)) {
																	class293.scrollY = i_23_;
																	WorldMapInfoDefinitionParser.setDirty(1, class293);
																}
															}
														}
													} else {
														RtInterface class293 = RtInterface.getInterface(i_19_, -9820);
														if ((dialogue.anInt6054 ^ 0xffffffff) != (class293.xOffset2D ^ 0xffffffff) || class293.yOffset2D != dialogue.headId || (class293.rotationZ ^ 0xffffffff) != (dialogue.anInt6053 ^ 0xffffffff)) {
															class293.rotationZ = dialogue.anInt6053;
															class293.xOffset2D = dialogue.anInt6054;
															class293.yOffset2D = dialogue.headId;
															WorldMapInfoDefinitionParser.setDirty(1, class293);
														}
													}
												} else {
													RtInterface class293 = RtInterface.getInterface(i_19_, -9820);
													if ((dialogue.anInt6054 ^ 0xffffffff) != (class293.unknown ^ 0xffffffff) || (class293.itemStackSize ^ 0xffffffff) != (dialogue.headId ^ 0xffffffff)) {
														class293.itemStackSize = dialogue.headId;
														class293.unknown = dialogue.anInt6054;
														WorldMapInfoDefinitionParser.setDirty(1, class293);
													}
												}
											} else {
												RtInterface class293 = RtInterface.getInterface(i_19_, i ^ 0x266f);
												boolean bool = (dialogue.anInt6054 ^ 0xffffffff) == -2;
												if (!class293.aBoolean2295 != !bool) {
													class293.aBoolean2295 = bool;
													WorldMapInfoDefinitionParser.setDirty(i + 54, class293);
												}
											}
										} else {
											int i_24_ = dialogue.anInt6054;
											int i_25_ = (0x7d21 & i_24_) >> -1879977206;
											int i_26_ = i_24_ >> 504954597 & 0x1f;
											int i_27_ = 0x1f & i_24_;
											int i_28_ = (i_26_ << -116011253) + (i_25_ << 1244792819) + (i_27_ << -1908460573);
											RtInterface class293 = RtInterface.getInterface(i_19_, -9820);
											if ((i_28_ ^ 0xffffffff) != (class293.defaultColour ^ 0xffffffff)) {
												class293.defaultColour = i_28_;
												WorldMapInfoDefinitionParser.setDirty(1, class293);
											}
										}
									} else {
										RtInterface interf = RtInterface.getInterface(i_19_, -9820);
										if (interf.defaultAnimation != dialogue.anInt6054 || (dialogue.anInt6054 ^ 0xffffffff) == 0) {
											interf.defaultAnimation = dialogue.anInt6054;
											interf.anInt2303 = 0;
											interf.anInt2312 = 0;
											interf.anInt2287 = 1;
											AnimationDefinition class97 = (interf.defaultAnimation ^ 0xffffffff) != 0 ? Class151_Sub7.animationDefinitionList.method2623(interf.defaultAnimation, i + 16436) : null;
											if (class97 != null) {
												QuickChatMessageParser.method3327(interf.anInt2303, class97, (byte) 118);
											}
											WorldMapInfoDefinitionParser.setDirty(i + 54, interf);
										}
									}
								} else {
									RtInterface interf = RtInterface.getInterface(i_19_, i + -9767);
									if (!dialogue.dialogueText.equals(interf.defaultText)) {
										interf.defaultText = dialogue.dialogueText;
										WorldMapInfoDefinitionParser.setDirty(i + 54, interf);
									}
								}
							} else {
								Class151_Sub1.strGlobalConfigs[i_19_] = dialogue.dialogueText;
								// System.out.println(Arrays.toString(Class151_Sub1.strGlobalConfigs)
								// + "" + i_19_);
								LinkedList.anIntArray1196[Class202.and(31, Class96.anInt803++)] = i_19_;
							}
						}
						GameDefinition.anInt2099++;
						if ((OpenGLRenderEffectManager.anInt440 ^ 0xffffffff) != -1) {
							Class98_Sub10_Sub32.anInt5720 += 20;
							if ((Class98_Sub10_Sub32.anInt5720 ^ 0xffffffff) <= -401) {
								OpenGLRenderEffectManager.anInt440 = 0;
							}
						}
						if (RtKeyListener.aClass293_593 != null) {
							Class42_Sub3.anInt5365++;
							if ((Class42_Sub3.anInt5365 ^ 0xffffffff) <= -16) {
								WorldMapInfoDefinitionParser.setDirty(1, RtKeyListener.aClass293_593);
								RtKeyListener.aClass293_593 = null;
							}
						}
						Class162.aClass293_1272 = null;
						VarClientStringsDefinitionParser.aBoolean1840 = false;
						CursorDefinitionParser.aClass293_120 = null;
						Class166.aBoolean1278 = false;
						Class98_Sub1.method946(-1, -123, -1, null);
						GraphicsDefinitionParser.method3563(-1, null, -1, i ^ ~0x8);
						if (!Class98_Sub10_Sub9.aBoolean5585) {
							Class21_Sub2.cursorId = -1;
						}
						Class3.method172(98);
						WorldMapInfoDefinition.logicTimer++;
						if (PrefetchObject.aBoolean889) {
							OutgoingPacket class98_sub11 = Class246_Sub3_Sub4.prepareOutgoingPacket(260, Class98_Sub43_Sub2.aClass171_5906, Class331.aClass117_2811);
							class98_sub11.packet.method1232(Class375.planeTele | NameHashTable.yTele << 514171598 | Class333.xTele << -509241764, (byte) 106);
							Class98_Sub10_Sub29.sendPacket(false, class98_sub11);
							PrefetchObject.aBoolean889 = false;
						}
						for (;;) {
							ClientScript2Event class98_sub21 = (ClientScript2Event) Class181.aClass148_1430.removeFirst(i ^ ~0x196a);
							if (class98_sub21 == null) {
								break;
							}
							RtInterface class293 = class98_sub21.component;
							if ((class293.componentIndex ^ 0xffffffff) <= -1) {
								RtInterface class293_29_ = RtInterface.getInterface(class293.parentId, i ^ 0x266f);
								if (class293_29_ == null || class293_29_.dynamicComponents == null || (class293.componentIndex ^ 0xffffffff) <= (class293_29_.dynamicComponents.length ^ 0xffffffff) || class293_29_.dynamicComponents[class293.componentIndex] != class293) {
									continue;
								}
							}
							ClientScript2Runtime.handleEvent(class98_sub21);
						}
						for (;;) {
							ClientScript2Event class98_sub21 = (ClientScript2Event) Class98_Sub46_Sub10.aClass148_6018.removeFirst(i ^ ~0x196a);
							if (class98_sub21 == null) {
								break;
							}
							RtInterface interf = class98_sub21.component;
							if ((interf.componentIndex ^ 0xffffffff) <= -1) {
								RtInterface interff = RtInterface.getInterface(interf.parentId, -9820);
								if (interff == null || interff.dynamicComponents == null || interf.componentIndex >= interff.dynamicComponents.length || interff.dynamicComponents[interf.componentIndex] != interf) {
									continue;
								}
							}
							ClientScript2Runtime.handleEvent(class98_sub21);
						}
						for (;;) {
							ClientScript2Event class98_sub21 = (ClientScript2Event) Class151_Sub3.aClass148_4977.removeFirst(6494);
							if (class98_sub21 == null) {
								break;
							}
							RtInterface class293 = class98_sub21.component;
							if (class293.componentIndex >= 0) {
								RtInterface class293_31_ = RtInterface.getInterface(class293.parentId, -9820);
								if (class293_31_ == null || class293_31_.dynamicComponents == null || (class293_31_.dynamicComponents.length ^ 0xffffffff) >= (class293.componentIndex ^ 0xffffffff) || class293 != class293_31_.dynamicComponents[class293.componentIndex]) {
									continue;
								}
							}
							ClientScript2Runtime.handleEvent(class98_sub21);
						}
						if (CursorDefinitionParser.aClass293_120 == null) {
							Class156_Sub2.anInt3423 = 0;
						}
						if (Class255.aClass293_3208 != null) {
							NativeMatrix.method2118(19653);
						}
						if (LoadingScreenSequence.rights > 0 && client.keyListener.isKeyDown(82, 5503) && client.keyListener.isKeyDown(81, 5503) && (Class319.mouseScrollDelta ^ 0xffffffff) != -1) {
							int plane = Class87.localPlayer.plane - Class319.mouseScrollDelta;
							/*if (plane >= 0) {
								if (plane > 3) {
									plane = 3;
								}
							} else {
								plane = 0;
							}*/
							Class351.handleClickTeleport(Class87.localPlayer.pathZ[0] + aa_Sub2.gameSceneBaseY, Class272.gameSceneBaseX + Class87.localPlayer.pathX[0], plane, 52);
						}
						Class204.method2709((byte) 49);
						for (int i_33_ = 0; i_33_ < 5; i_33_++) {
							QuickChatCategoryParser.anIntArray1597[i_33_]++;
						}
						if (Class66.aBoolean507 && (OpenGlShadow.aLong6322 ^ 0xffffffffffffffffL) > (-60000L + TimeTools.getCurrentTime(-47) ^ 0xffffffffffffffffL)) {
							Class23.method283((byte) 118);
						}
						for (FriendLoginUpdate update = (FriendLoginUpdate) Class119.friendLogins.getFirst((byte) 15); update != null; update = (FriendLoginUpdate) Class119.friendLogins.getNext(false)) {
							if ((-5L + TimeTools.getCurrentTime(i ^ 0x1a) / 1000L ^ 0xffffffffffffffffL) < (update.time ^ 0xffffffffffffffffL)) {
								if (update.onlineStatus > 0) {
									ItemDeque.addChatMessage((byte) 125, 5, update.friendName + TextResources.HAS_LOGGED_IN.getText(client.gameLanguage, (byte) 25), 0, "", "", "");
								}
								if ((update.onlineStatus ^ 0xffffffff) == -1) {
									ItemDeque.addChatMessage((byte) -120, 5, update.friendName + TextResources.HAS_LOGGED_OUT.getText(client.gameLanguage, (byte) 25), 0, "", "", "");
								}
								update.unlink((byte) 126);
							}
						}
						RotatingSpriteLoadingScreenElement.anInt5459++;
						if (RotatingSpriteLoadingScreenElement.anInt5459 > 500) {
							RotatingSpriteLoadingScreenElement.anInt5459 = 0;
							int i_34_ = (int) (Math.random() * 8.0);
							if ((i_34_ & 0x1) == 1) {
								GraphicsDefinitionParser.anInt2533 += FontSpecifications.anInt1520;
							}
							if ((i_34_ & 0x2) == 2) {
								RtMouseEvent.anInt3943 += DecoratedProgressBarLSEConfig.anInt5488;
							}
							if ((i_34_ & 0x4) == 4) {
								Class98_Sub10_Sub9.anInt5581 += Class98_Sub46_Sub4.anInt5961;
							}
						}
						if (GraphicsDefinitionParser.anInt2533 < -50) {
							FontSpecifications.anInt1520 = 2;
						}
						if (GraphicsDefinitionParser.anInt2533 > 50) {
							FontSpecifications.anInt1520 = -2;
						}
						if ((RtMouseEvent.anInt3943 ^ 0xffffffff) > 54) {
							DecoratedProgressBarLSEConfig.anInt5488 = 2;
						}
						if (Class98_Sub10_Sub9.anInt5581 < -40) {
							Class98_Sub46_Sub4.anInt5961 = 1;
						}
						if (RtMouseEvent.anInt3943 > 55) {
							DecoratedProgressBarLSEConfig.anInt5488 = -2;
						}
						Class278_Sub1.anInt5170++;
						if ((Class98_Sub10_Sub9.anInt5581 ^ 0xffffffff) < -41) {
							Class98_Sub46_Sub4.anInt5961 = -1;
						}
						if (Class278_Sub1.anInt5170 > 500) {
							Class278_Sub1.anInt5170 = 0;
							int i_35_ = (int) (8.0 * Math.random());
							if ((0x2 & i_35_ ^ 0xffffffff) == -3) {
								Class151.anInt1213 += Class76_Sub9.anInt3786;
							}
							if ((0x1 & i_35_) == 1) {
								Class204.anInt1553 += Class169.anInt1305;
							}
						}
						if ((Class204.anInt1553 ^ 0xffffffff) > 59) {
							Class169.anInt1305 = 2;
						}
						if (Class151.anInt1213 < -20) {
							Class76_Sub9.anInt3786 = 1;
						}
						if (Class204.anInt1553 > 60) {
							Class169.anInt1305 = -2;
						}
						BuildLocation.anInt1511++;
						if (Class151.anInt1213 > 10) {
							Class76_Sub9.anInt3786 = -1;
						}
						if (BuildLocation.anInt1511 > 50) {
							Class76_Sub5.anInt3746++;
							OutgoingPacket frame = Class246_Sub3_Sub4.prepareOutgoingPacket(260, NodeShort.KEEP_ALIVE, Class331.aClass117_2811);
							Class98_Sub10_Sub29.sendPacket(false, frame);
						}
						if (Class76_Sub8.aBoolean3771) {
							Huffman.method2781(i ^ ~0x3c);
							Class76_Sub8.aBoolean3771 = false;
						}
						try {
							Class95.method920((byte) 115);
						} catch (java.io.IOException ioexception) {
							Canvas_Sub1.method118((byte) 104);
						}
					}
				}
			}
		}
	}

	public static final void method2630(int yChunk, int i_36_, int xChunk, int i_38_) {
		if (i_36_ <= -102) {
			Class28 class28 = Class76.aClass28ArrayArray586[xChunk][yChunk];
			// System.out.println(xChunk + " " + yChunk);
			Class21_Sub3.method275(false, i_38_, class28 == null ? OpenGLDisplayList.aClass28_722 : class28);
		}
	}

	public static final void method2631(int i, int i_39_) {
		Class98_Sub46_Sub17 dialogue = method2628(i_39_, -29, i);
		dialogue.method1621(0);
	}

	public long		aLong1448;

	public int[]	anIntArray1446;

	public short[]	aShortArray1444;

	public short[]	aShortArray1447;

	protected Class185() {
		/* empty */
	}

	Class185(long l, int[] is, short[] is_40_, short[] is_41_) {
		aShortArray1444 = is_41_;
		aLong1448 = l;
		anIntArray1446 = is;
		aShortArray1447 = is_40_;
	}
}

/* Class98_Sub46_Sub8 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;

public final class ActionQueueEntry extends Cacheable {
	public static IncomingOpcode RECIEVE_PRIVATE_MESSAGE = new IncomingOpcode(30, -1);

	public static final void addAction(boolean bool, boolean bool_56_, long objectId, int cursorId, int i_57_, String target, boolean bool_58_, int i_59_, int actionId, long groupHash, int i_62_, boolean otherLevel, String actionText) {
		if (!Player.menuOpen && Class359.actionCount < 500) {
			cursorId = cursorId == -1 ? Class21_Sub2.cursorId : cursorId;
			if (bool == false) {
				ActionQueueEntry entry = new ActionQueueEntry(actionText, target, cursorId, actionId, i_62_, objectId, i_57_, i_59_, bool_56_, otherLevel, groupHash, bool_58_);
				ActionQueueEntry.addAction(126, entry);
			}
		}
	}

	public static final void addAction(int dummy, ActionQueueEntry entry) {
		do {
			if (entry != null && dummy > 117) {
				Class33.actionList.addLast(entry, -20911);
				Class359.actionCount++;
				ActionGroup group;
				if (!entry.notGroupable && !"".equals(entry.targetText)) {
					long hash = entry.groupHash;
					for (group = (ActionGroup) Class98_Sub47.groupMap.get(hash, -1); group != null; group = (ActionGroup) Class98_Sub47.groupMap.nextHashCollision(122)) {
						if (group.actionText.equals(entry.targetText)) {
							break;
						}
					}
					if (group == null) {
						group = (ActionGroup) AnimationSkeletonSet.aClass79_6046.get(-126, hash);
						if (group != null && !group.actionText.equals(entry.targetText)) {
							group = null;
						}
						if (group == null) {
							group = new ActionGroup(entry.targetText);
						}
						Class98_Sub47.groupMap.put(group, hash, -1);
						GraphicsLevelPreferenceField.groupCount++;
					}
				} else {
					group = new ActionGroup(entry.targetText);
					GraphicsLevelPreferenceField.groupCount++;
				}
				if (!group.addAction(90, entry)) {
					break;
				}
				ActionGroup.addGroup(group, (byte) 87);
			}
			break;
		} while (false);
	}

	public boolean	aBoolean5983;
	public boolean	aBoolean5984;
	public boolean	notGroupable;
	public String	actionText;
	public long		aLong5987;
	public long		groupHash;
	public int		anInt5986;
	public int		anInt5988;
	public int		actionId;
	public int		anInt5993;

	public int		anInt5995;

	public String	aString5985;

	public String	targetText;

	ActionQueueEntry(String actionText, String targetText, int i, int i_1_, int i_2_, long l, int i_3_, int i_4_, boolean bool, boolean bool_5_, long l_6_, boolean bool_7_) {
		groupHash = l_6_;
		anInt5988 = i_2_;
		anInt5986 = i;
		anInt5993 = i_4_;
		this.targetText = targetText;
		actionId = i_1_;
		aBoolean5983 = bool_5_;
		aBoolean5984 = bool;
		notGroupable = bool_7_;
		aLong5987 = l;
		anInt5995 = i_3_;
		this.actionText = actionText;
	}
}

/* Class246_Sub3_Sub3_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.input.RtKeyListener;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;
import com.jagex.game.toolkit.shadow.Shadow;

public final class Class246_Sub3_Sub3_Sub2 extends Class246_Sub3_Sub3 implements Interface19 {

	public static final float[] method3021(int i, int i_22_, float[] fs) {
		try {
			float[] fs_23_ = new float[i_22_];
			ArrayUtils.method2897(fs, 0, fs_23_, 0, i_22_);
			if (i != -65537) {
				return null;
			}
			return fs_23_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.P(" + i + ',' + i_22_ + ',' + (fs != null ? "{...}" : "null") + ')');
		}
	}

	private boolean		aBoolean6280;
	private boolean		aBoolean6281;
	private boolean		aBoolean6284;
	private boolean		aBoolean6287;
	private byte		aByte6279;
	private byte		aByte6288;
	ModelRenderer		aClass146_6285;
	private Class228	aClass228_6282;

	private Shadow		aR6286;

	private short		aShort6283;

	Class246_Sub3_Sub3_Sub2(RSToolkit var_ha, GameObjectDefinition class352, int i, int i_12_, int i_13_, int i_14_, int i_15_, boolean bool, int i_16_, int i_17_, boolean bool_18_) {
		super(i_13_, i_14_, i_15_, i, i_12_, RtKeyListener.method780(i_16_, (byte) -72, i_17_));
		do {
			try {
				aByte6288 = (byte) i_17_;
				aBoolean6284 = bool_18_;
				aByte6279 = (byte) i_16_;
				aShort6283 = (short) class352.id;
				aBoolean6280 = class352.anInt2998 != 0 && !bool;
				aBoolean6287 = bool;
				((Char) this).boundExtentsX = i_13_;
				((Char) this).boundExtentsZ = i_15_;
				aBoolean6281 = var_ha.method1771() && class352.aBoolean2935 && !aBoolean6287 && client.preferences.sceneryShadows.getValue((byte) 124) != 0;
				int i_19_ = 2048;
				if (aBoolean6284) {
					i_19_ |= 0x10000;
				}
				Class298 class298 = method3018(var_ha, -99, i_19_, aBoolean6281);
				if (class298 == null) {
					break;
				}
				aClass146_6285 = class298.aClass146_2477;
				aR6286 = class298.aR2479;
				if (!aBoolean6284) {
					break;
				}
				aClass146_6285 = aClass146_6285.method2341((byte) 0, i_19_, false);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vr.<init>(" + (var_ha != null ? "{...}" : "null") + ',' + (class352 != null ? "{...}" : "null") + ',' + i + ',' + i_12_ + ',' + i_13_ + ',' + i_14_ + ',' + i_15_ + ',' + bool + ',' + i_16_ + ',' + i_17_ + ',' + bool_18_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (aClass228_6282 == null) {
				aClass228_6282 = Class48_Sub2_Sub1.method472(((Char) this).anInt5089, ((Char) this).boundExtentsX, method3019(0, (byte) 58, var_ha), ((Char) this).boundExtentsZ, 4);
			}
			if (i != -53) {
				method3019(89, (byte) -78, null);
			}
			return aClass228_6282;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			if (aClass146_6285 == null) {
				return null;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
			Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, aBoolean6280);
			do {
				if (!VarClientStringsDefinitionParser.aBoolean1839) {
					aClass146_6285.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				aClass146_6285.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
			} while (false);
			if (i >= -12) {
				return null;
			}
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_2_, int i_3_) {
		try {
			ModelRenderer class146 = method3019(131072, (byte) 61, var_ha);
			if (class146 != null) {
				Matrix class111 = var_ha.method1793();
				class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
				if (VarClientStringsDefinitionParser.aBoolean1839) {
					return class146.method2333(i, i_3_, class111, false, 0, Class16.anInt197);
				}
				return class146.method2339(i, i_3_, class111, false, 0);
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			if (aClass146_6285 == null) {
				return true;
			}
			if (aClass146_6285.r()) {
				return false;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.H(" + i + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_5_, RSToolkit var_ha, int i_6_, int i_7_) {
		do {
			try {
				if (!(class246_sub3 instanceof Class246_Sub3_Sub3_Sub2)) {
					if (class246_sub3 instanceof Class246_Sub3_Sub4_Sub1) {
						Class246_Sub3_Sub4_Sub1 class246_sub3_sub4_sub1 = (Class246_Sub3_Sub4_Sub1) class246_sub3;
						if (aClass146_6285 != null && class246_sub3_sub4_sub1.aClass146_6243 != null) {
							aClass146_6285.method2332(class246_sub3_sub4_sub1.aClass146_6243, i_5_, i_6_, i_7_, bool);
						}
					}
				} else {
					Class246_Sub3_Sub3_Sub2 class246_sub3_sub3_sub2_9_ = (Class246_Sub3_Sub3_Sub2) class246_sub3;
					if (aClass146_6285 == null || class246_sub3_sub3_sub2_9_.aClass146_6285 == null) {
						break;
					}
					aClass146_6285.method2332(class246_sub3_sub3_sub2_9_.aClass146_6285, i_5_, i_6_, i_7_, bool);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vr.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_5_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_6_ + ',' + i_7_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i > -70) {
				method3021(-106, 41, null);
			}
			return aBoolean6284;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.HA(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				return 118;
			}
			if (aClass146_6285 != null) {
				return aClass146_6285.ma();
			}
			return 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (i != 6540) {
				return true;
			}
			if (aClass146_6285 != null) {
				return aClass146_6285.F();
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		try {
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				return 127;
			}
			if (aClass146_6285 == null) {
				return 0;
			}
			return aClass146_6285.fa();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.J(" + i + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		do {
			try {
				aBoolean6284 = false;
				if (aClass146_6285 != null) {
					aClass146_6285.updateFunctionMask(~0x10000 & aClass146_6285.functionMask());
				}
				if (i == -73) {
					break;
				}
				method2975(null, -1);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vr.DA(" + i + ')');
			}
			break;
		} while (false);
	}

	private final Class298 method3018(RSToolkit var_ha, int i, int i_0_, boolean bool) {
		try {
			if (i >= -20) {
				return null;
			}
			GameObjectDefinition class352 = Class130.gameObjectDefinitionList.get(0xffff & aShort6283, (byte) 119);
			Ground var_s;
			Ground var_s_1_;
			if (!aBoolean6287) {
				if ((((Char) this).collisionPlane ^ 0xffffffff) > -4) {
					var_s_1_ = StrongReferenceMCNode.aSArray6298[((Char) this).collisionPlane - -1];
				} else {
					var_s_1_ = null;
				}
				var_s = StrongReferenceMCNode.aSArray6298[((Char) this).collisionPlane];
			} else {
				var_s = Class81.aSArray618[((Char) this).collisionPlane];
				var_s_1_ = StrongReferenceMCNode.aSArray6298[0];
			}
			return class352.method3851(((Char) this).boundExtentsZ, false, var_s_1_, aByte6288, ((Char) this).anInt5089, bool, ((Char) this).boundExtentsX, i_0_, null, var_s, var_ha, aByte6279);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.T(" + (var_ha != null ? "{...}" : "null") + ',' + i + ',' + i_0_ + ',' + bool + ')');
		}
	}

	private final ModelRenderer method3019(int i, byte i_11_, RSToolkit var_ha) {
		try {
			if (aClass146_6285 != null && var_ha.c(aClass146_6285.functionMask(), i) == 0) {
				return aClass146_6285;
			}
			if (i_11_ < 31) {
				aClass228_6282 = null;
			}
			Class298 class298 = method3018(var_ha, -56, i, false);
			if (class298 == null) {
				return null;
			}
			return class298.aClass146_2477;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.R(" + i + ',' + i_11_ + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final void method61(byte i) {
		try {
			if (aClass146_6285 != null) {
				aClass146_6285.method2326();
			}
			if (i != -96) {
				method2992((byte) -58);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.Q(" + i + ')');
		}
	}

	@Override
	public final void method62(RSToolkit var_ha, int i) {
		do {
			try {
				if (i != 24447) {
					aShort6283 = (short) -43;
				}
				Shadow var_r;
				if (aR6286 == null && aBoolean6281) {
					Class298 class298 = method3018(var_ha, -49, 262144, true);
					var_r = class298 == null ? null : class298.aR2479;
				} else {
					var_r = aR6286;
					aR6286 = null;
				}
				if (var_r == null) {
					break;
				}
				Class184.method2626(var_r, ((Char) this).collisionPlane, ((Char) this).boundExtentsX, ((Char) this).boundExtentsZ, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vr.G(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int method63(byte i) {
		try {
			if (i != 20) {
				method64(-101);
			}
			return aByte6279;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.B(" + i + ')');
		}
	}

	@Override
	public final int method64(int i) {
		try {
			if (i != 30472) {
				aBoolean6284 = false;
			}
			return aShort6283 & 0xffff;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.C(" + i + ')');
		}
	}

	@Override
	public final boolean method65(boolean bool) {
		try {
			if (bool != true) {
				return false;
			}
			return aBoolean6281;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.A(" + bool + ')');
		}
	}

	@Override
	public final int method66(int i) {
		try {
			if (i != 4657) {
				aBoolean6284 = true;
			}
			return aByte6288;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vr.N(" + i + ')');
		}
	}

	@Override
	public final void method67(int i, RSToolkit var_ha) {
		do {
			try {
				if (i == -25163) {
					Shadow var_r;
					if (aR6286 == null && aBoolean6281) {
						Class298 class298 = method3018(var_ha, i + 25108, 262144, true);
						var_r = class298 != null ? class298.aR2479 : null;
					} else {
						var_r = aR6286;
						aR6286 = null;
					}
					if (var_r == null) {
						break;
					}
					Class268.method3254(var_r, ((Char) this).collisionPlane, ((Char) this).boundExtentsX, ((Char) this).boundExtentsZ, null);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vr.E(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}
}

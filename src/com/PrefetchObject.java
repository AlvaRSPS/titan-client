/* Class102 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.loading.monitor.ProgressMonitor;
import com.jagex.game.input.RtMouseListener;
import com.jagex.game.input.impl.AwtMouseListener;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class PrefetchObject {
	public static boolean			aBoolean889			= false;
	public static ChatStatus		OFF					= new ChatStatus(2);
	public static PrefetchObject	ANIMATIONS			= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	BCONFIGS			= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	BILLBOARDS			= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	CLIENT_SCRIPTS		= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	DEFAULTS			= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	ENUMS				= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	FONTS				= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	GRAPHICS			= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	HUFFMAN				= new PrefetchObject(Class191.JS5_FILE);
	public static PrefetchObject	HW3D				= new PrefetchObject(Class191.NATIVE_LIBRARY);
	public static PrefetchObject	INTERFACES			= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	ITEMS				= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	JACLIB				= new PrefetchObject(Class191.NATIVE_LIBRARY);
	public static PrefetchObject	JAGDX				= new PrefetchObject(Class191.NATIVE_LIBRARY);
	public static PrefetchObject	JAGGL				= new PrefetchObject(Class191.NATIVE_LIBRARY);
	public static PrefetchObject	JAGMISC				= new PrefetchObject(Class191.NATIVE_LIBRARY);
	public static PrefetchObject	JAGTHEORA			= new PrefetchObject(Class191.NATIVE_LIBRARY);
	public static PrefetchObject	MATERIALS			= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	MISCELLANEOUS		= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	NPCS				= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	OBJECTS				= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	PARTICLES			= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	QUICKCHAT_MENUS		= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	QUICKCHAT_MESSAGES	= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	SHADERS				= new PrefetchObject(Class191.JS5_STORE);
	public static PrefetchObject	SW3D				= new PrefetchObject(Class191.NATIVE_LIBRARY);
	public static PrefetchObject	WORLD_MAP_DETAILS	= new PrefetchObject(Class191.JS5_GROUP);

	public static final PrefetchObject[] getAll(int i) {
		return new PrefetchObject[] { DEFAULTS, JACLIB, JAGGL, JAGDX, JAGMISC, SW3D, HW3D, JAGTHEORA, SHADERS, MATERIALS, MISCELLANEOUS, OBJECTS, ENUMS, NPCS, ITEMS, ANIMATIONS, GRAPHICS, BCONFIGS, QUICKCHAT_MESSAGES, QUICKCHAT_MENUS, PARTICLES, BILLBOARDS, HUFFMAN, INTERFACES, CLIENT_SCRIPTS,
				FONTS, WORLD_MAP_DETAILS };
	}

	public static final ModelRenderer method1703(AnimationDefinition class97, int i, int i_0_, int i_1_, ModelRenderer class146, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, RSToolkit var_ha, int i_8_, int i_9_) {
		if (class146 == null) {
			return null;
		}
		int i_10_ = 2055;
		if (i_4_ < 113) {
			return null;
		}
		if (class97 != null) {
			i_10_ |= class97.method932(false, i_7_, true, -1);
			i_10_ &= ~0x200;
		}
		long l = i_0_ + (i_5_ << -903417392) - -(i_1_ << -423490568) - -((long) i_8_ << 142401248) + ((long) i_6_ << 21507696);
		ModelRenderer class146_11_;
		synchronized (RtMouseListener.aClass79_2493) {
			class146_11_ = (ModelRenderer) RtMouseListener.aClass79_2493.get(-119, l);
		}
		if (class146_11_ == null || (var_ha.c(class146_11_.functionMask(), i_10_) ^ 0xffffffff) != -1) {
			if (class146_11_ != null) {
				i_10_ = var_ha.mergeFunctionMask(i_10_, class146_11_.functionMask());
			}
			int i_12_;
			if ((i_0_ ^ 0xffffffff) != -2) {
				if (i_0_ != 2) {
					if (i_0_ != 3) {
						if (i_0_ == 4) {
							i_12_ = 18;
						} else {
							i_12_ = 21;
						}
					} else {
						i_12_ = 15;
					}
				} else {
					i_12_ = 12;
				}
			} else {
				i_12_ = 9;
			}
			int i_13_ = 3;
			int[] is = { 64, 96, 128 };
			BaseModel class178 = new BaseModel(i_13_ * i_12_ + 1, i_13_ * i_12_ * 2 - i_12_, 0);
			int i_14_ = class178.method2599(14418, 0, 0, 0);
			int[][] is_15_ = new int[i_13_][i_12_];
			for (int i_16_ = 0; i_16_ < i_13_; i_16_++) {
				int i_17_ = is[i_16_];
				int i_18_ = is[i_16_];
				for (int i_19_ = 0; i_12_ > i_19_; i_19_++) {
					int i_20_ = (i_19_ << 1158136782) / i_12_;
					int i_21_ = Class284_Sub2_Sub2.SINE[i_20_] * i_17_ >> 1774149838;
					int i_22_ = Class284_Sub2_Sub2.COSINE[i_20_] * i_18_ >> 1320391086;
					is_15_[i_16_][i_19_] = class178.method2599(14418, i_21_, 0, i_22_);
				}
			}
			for (int i_23_ = 0; i_23_ < i_13_; i_23_++) {
				int i_24_ = (i_23_ * 256 + 128) / i_13_;
				int i_25_ = -i_24_ + 256;
				byte i_26_ = (byte) (i_5_ * i_25_ + i_24_ * i_1_ >> -266203096);
				short i_27_ = (short) ((0x7f00 & i_25_ * (0x7f & i_8_) + (0x7f & i_6_) * i_24_) + (0xfc0000 & i_24_ * (0xfc00 & i_6_) + i_25_ * (0xfc00 & i_8_)) + ((i_8_ & 0x380) * i_25_ + (0x380 & i_6_) * i_24_ & 0x38000) >> -887272664);
				for (int i_28_ = 0; (i_28_ ^ 0xffffffff) > (i_12_ ^ 0xffffffff); i_28_++) {
					if ((i_23_ ^ 0xffffffff) != -1) {
						class178.method2594((byte) 1, i_27_, (short) -1, i_26_, false, is_15_[i_23_][(1 + i_28_) % i_12_], is_15_[-1 + i_23_][i_28_], (byte) -1, is_15_[i_23_ - 1][(i_28_ + 1) % i_12_]);
						class178.method2594((byte) 1, i_27_, (short) -1, i_26_, false, is_15_[i_23_][i_28_], is_15_[-1 + i_23_][i_28_], (byte) -1, is_15_[i_23_][(1 + i_28_) % i_12_]);
					} else {
						class178.method2594((byte) 1, i_27_, (short) -1, i_26_, false, is_15_[0][i_28_], i_14_, (byte) -1, is_15_[0][(i_28_ - -1) % i_12_]);
					}
				}
			}
			class146_11_ = var_ha.createModelRenderer(class178, i_10_, SoftwareNativeHeap.anInt6076, 64, 768);
			synchronized (RtMouseListener.aClass79_2493) {
				RtMouseListener.aClass79_2493.put(l, class146_11_, (byte) -80);
			}
		}
		int i_29_ = class146.V();
		int i_30_ = class146.RA();
		int i_31_ = class146.HA();
		int i_32_ = class146.G();
		AnimationSkeletonSet class98_sub46_sub16 = null;
		if (class97 != null) {
			i_7_ = class97.frameIds[i_7_];
			class98_sub46_sub16 = Class151_Sub7.animationDefinitionList.method2624(2, i_7_ >> 448240368);
			i_7_ &= 0xffff;
		}
		if (class98_sub46_sub16 != null) {
			class146_11_ = class146_11_.method2341((byte) 3, i_10_, true);
			class146_11_.scale(-i_29_ + i_30_ >> 1914721025, 128, -i_31_ + i_32_ >> 1598124673);
			class146_11_.H(i_30_ + i_29_ >> 217245921, 0, i_31_ - -i_32_ >> 1082256225);
			class146_11_.method2340(class98_sub46_sub16, 0, i_7_);
		} else {
			class146_11_ = class146_11_.method2341((byte) 3, i_10_, true);
			class146_11_.scale(-i_29_ + i_30_ >> -953095807, 128, -i_31_ + i_32_ >> 226658273);
			class146_11_.H(i_29_ + i_30_ >> -379781503, 0, i_31_ - -i_32_ >> -1170469311);
		}
		if (i_2_ != 0) {
			class146_11_.FA(i_2_);
		}
		if ((i ^ 0xffffffff) != -1) {
			class146_11_.VA(i);
		}
		if (i_9_ != 0) {
			class146_11_.H(0, i_9_, 0);
		}
		return class146_11_;
	}

	public static final int method1710(int i) {
		if (i < 45) {
			PARTICLES = null;
		}
		if (Player.menuOpen) {
			return 6;
		}
		if (SunDefinition.aClass98_Sub46_Sub8_1994 == null) {
			return 0;
		}
		int i_35_ = SunDefinition.aClass98_Sub46_Sub8_1994.actionId;
		if (ClientScript2Event.method1179(i_35_, 255)) {
			return 1;
		}
		if (AwtMouseListener.method3526(123, i_35_)) {
			return 2;
		}
		if (Class98_Sub10_Sub21.method1064(i_35_, false)) {
			return 3;
		}
		if (Class36.method340(i_35_, (byte) -91)) {
			return 4;
		}
		return 5;
	}

	public ProgressMonitor	progressMonitor;

	private Class191		type;

	private int				weight;

	public PrefetchObject(Class191 loadingObjType) {
		type = loadingObjType;
		weight = 1;
	}

	public final ProgressMonitor getProgressMonitor(int i) {
		return progressMonitor;
	}

	public final int getWeight(int i) {
		return weight;
	}

	public final void setProgressMonitor(byte i, ProgressMonitor monitor) {
		if (monitor.getType(15763) != type) {
			throw new IllegalArgumentException();
		}
		progressMonitor = monitor;
	}

	public final void setWeight(int weightValue, int i_33_) {
		weight = weightValue;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

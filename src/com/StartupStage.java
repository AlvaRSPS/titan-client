/* Class75 - Decompiled by JODE
 */ package com; /*
					*/

public final class StartupStage {
	public static Class140 varValues;
	public static StartupStage	aClass75_567					= new StartupStage(8, TextResources.CHECKING_FOR_UPDATES, TextResources.CHECKING_FOR_UPDATES, 5, 98, true, true);
	private static StartupStage	aClass75_572					= new StartupStage(13, TextResources.DOWNLOADING_UPDATES, TextResources.DOWNLOADING_UPDATES, 92, 93);
	public static StartupStage	aClass75_573					= new StartupStage(14, TextResources.DOWNLOADING_UPDATES, TextResources.DOWNLOADING_UPDATES, 94, 95);
	public static StartupStage	aClass75_574					= new StartupStage(15, TextResources.DOWNLOADING_UPDATES, TextResources.DOWNLOADING_UPDATES, 96, 97);
	public static StartupStage	aClass75_575					= new StartupStage(16, TextResources.DOWNLOADING_UPDATES, 97);
	public static StartupStage	LOAD_LOGIN_INTERFACE					= new StartupStage(17, TextResources.DOWNLOADING_UPDATES, 97);
	public static StartupStage	aClass75_577					= new StartupStage(18, TextResources.DOWNLOADING_UPDATES, 100);
	public static StartupStage	aClass75_578					= new StartupStage(19, TextResources.DOWNLOADING_UPDATES, 100);
	public static StartupStage	aClass75_579					= new StartupStage(20, TextResources.DOWNLOADING_UPDATES, 100);
	public static int			anInt581						= 0;
	public static int[]			anIntArray580					= new int[4096];
	public static int[]			anIntArray582					= { 4, 2, 1, 1, 2, 2, 3, 1, 3, 3, 3, 2, 0 };
	public static StartupStage	CREATE_CLIP_MAPS				= new StartupStage(6, TextResources.CREATING_CLIP_MAPS, 4);
	public static StartupStage	CREATE_CONFIG_LISTS				= new StartupStage(12, TextResources.CREATING_CONFIGURATION_LIST, TextResources.CREATING_CONFIGURATION_LIST, 92, 92);
	public static StartupStage	CREATE_JS_ARCHIVES				= new StartupStage(7, TextResources.CREATING_ARCHIVES, TextResources.CREATING_ARCHIVES, 4, 5);
	public static int			floorshadowsId;
	public static StartupStage	INITIALIZE_MUSIC				= new StartupStage(9, TextResources.INITIALIZING_MUSIC, 99);
	public static StartupStage	INITIALIZE_NATIVE_MANAGER		= new StartupStage(10, TextResources.INITIALIZING_NATIVE_MANAGER, 100);
	public static StartupStage	INITIALIZE_TOOLKIT				= new StartupStage(1, TextResources.INITIALIZING_TOOLKIT, 2);
	public static StartupStage	LOAD_LOADING_SCREEN_DEFINITIONS	= new StartupStage(2, TextResources.LOADING_SCREEN_DEFINITIONS, TextResources.LOADING_SCREEN_DEFINITIONS, 2, 3);
	public static StartupStage	LOAD_LOADING_SCREEN_FONTS		= new StartupStage(4, TextResources.LOADING_SCREEN_FONTS, TextResources.LOADING_SCREEN_FONTS, 3, 4);
	public static StartupStage	LOAD_LOADING_SCREEN_MEDIA		= new StartupStage(5, TextResources.LOADING_SCREEN_MEDIA, 4);
	public static StartupStage	PREFETCH						= new StartupStage(11, TextResources.PREFETCHING, TextResources.PREFETCHING, 0, 92, true, true);
	public static StartupStage	REQUEST_LOADING_SCREEN_FONTS	= new StartupStage(3, TextResources.REQUESTING_LOADING_SCREEN_FONTS, 3);
	public static StartupStage	WAIT_FOR_MEMORY					= new StartupStage(0, TextResources.WAITING_FOR_MEMORY, TextResources.WAITING_FOR_MEMORY, 0, 1);

	public static final StartupStage[] getStartupStages(int i) {
		return new StartupStage[] { WAIT_FOR_MEMORY, INITIALIZE_TOOLKIT, LOAD_LOADING_SCREEN_DEFINITIONS, REQUEST_LOADING_SCREEN_FONTS, LOAD_LOADING_SCREEN_FONTS, LOAD_LOADING_SCREEN_MEDIA, CREATE_CLIP_MAPS, CREATE_JS_ARCHIVES, aClass75_567, INITIALIZE_MUSIC, INITIALIZE_NATIVE_MANAGER, PREFETCH,
				CREATE_CONFIG_LISTS, aClass75_572, aClass75_573, aClass75_574, aClass75_575, LOAD_LOGIN_INTERFACE, aClass75_577, aClass75_578, aClass75_579 };
	}

	boolean					aBoolean554;
	TextResources			checkingMessages;
	public TextResources	fetchingMessages;
	public boolean			gradualProgress;
	private int				id;

	public int				percentageStart;

	public int				percentageStop;

	private StartupStage(int i, TextResources class309, int i_0_) {
		this(i, class309, class309, i_0_, i_0_, true, false);
	}

	private StartupStage(int i, TextResources class309, TextResources class309_2_, int i_3_, int i_4_) {
		this(i, class309, class309_2_, i_3_, i_4_, true, false);
	}

	private StartupStage(int i, TextResources class309, TextResources class309_5_, int i_6_, int i_7_, boolean bool, boolean bool_8_) {
		aBoolean554 = bool;
		gradualProgress = bool_8_;
		checkingMessages = class309_5_;
		fetchingMessages = class309;
		id = i;
		percentageStart = i_6_;
		percentageStop = i_7_;
	}

	public final int getId(byte i) {
		return id;
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}
}

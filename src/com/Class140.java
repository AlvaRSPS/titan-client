/* Class140 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.ui.loading.LoadingScreenRenderer;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;

public final class Class140 implements VarValues {
	public static OutgoingOpcode		aClass171_3248	= new OutgoingOpcode(42, 4);
	public static Class48				aClass48_3245;
	public static boolean[]				boolGlobalConfigs;
	public static int					hitbarDefaultId;
	public static LoadingScreenRenderer	loadingScreenRenderer;

	public static final boolean method2287(int i, int i_9_, int i_10_) {
		if (i_10_ != 2048) {
			return false;
		}
		return (0x800 & i ^ 0xffffffff) != -1;
	}

	private HashTable	aClass377_3242	= new HashTable(128);

	int[] values;

	private int[]		configs;

	public Class140() {
		values = new int[SpriteLoadingScreenElement.varPlayerDefinitionList.anInt1086];
		configs = new int[SpriteLoadingScreenElement.varPlayerDefinitionList.anInt1086];
	}

	@Override
	public final int getClientVarp(int i, int i_2_) {
		return values[i];
	}

	@Override
	public final int getClientVarpBit(int bitConfigId, int i_4_) {
		BConfigDefinition definition = DataFs.bConfigDefinitionList.list(bitConfigId, (byte) 40);
		int id = definition.bConfigId;
		int start = definition.start;
		int end = definition.end;
		if (i_4_ != 7373) {
			return -48;
		}
		int bits = Class98_Sub46_Sub20.bitmasks[end - start];
		return values[id] >> start & bits;
	}

	public final void method2288(byte i) {
		int i_11_ = 0;
		for (/**/; (SpriteLoadingScreenElement.varPlayerDefinitionList.anInt1086 ^ 0xffffffff) < (i_11_ ^ 0xffffffff); i_11_++) {
			VarPlayerDefinition class167 = SpriteLoadingScreenElement.varPlayerDefinitionList.method2282(i_11_, 16);
			if (class167 != null && class167.anInt1283 == 0) {
				configs[i_11_] = 0;
				values[i_11_] = 0;
			}
		}
		aClass377_3242 = new HashTable(128);
	}

	public final void method2289(int i, int i_13_, int i_14_) {
		BConfigDefinition class366 = DataFs.bConfigDefinitionList.list(i_13_, (byte) 68);
		int i_15_ = class366.bConfigId;
		int i_16_ = class366.start;
		int i_17_ = class366.end;
		int i_18_ = Class98_Sub46_Sub20.bitmasks[i_17_ - i_16_];
		if (i < i_14_ || (i ^ 0xffffffff) < (i_18_ ^ 0xffffffff)) {
			i = 0;
		}
		i_18_ <<= i_16_;
		method2291(i_15_, 86, i_18_ & i << i_16_ | (i_18_ ^ 0xffffffff) & values[i_15_]);
	}

	public final int method2290(int i, boolean bool) {
		long l = TimeTools.getCurrentTime(-47);
		Class98_Sub50 class98_sub50 = !bool ? (Class98_Sub50) aClass377_3242.iterateNext(-1) : (Class98_Sub50) aClass377_3242.startIteration(117);
		for (/**/; class98_sub50 != null; class98_sub50 = (Class98_Sub50) aClass377_3242.iterateNext(-1)) {
			if ((0x3fffffffffffffffL & class98_sub50.aLong4288 ^ 0xffffffffffffffffL) > (l ^ 0xffffffffffffffffL)) {
				if ((0x4000000000000000L & class98_sub50.aLong4288) == 0L) {
					class98_sub50.unlink(59);
				} else {
					int i_20_ = (int) class98_sub50.hash;
					values[i_20_] = configs[i_20_];
					class98_sub50.unlink(45);
					return i_20_;
				}
			}
		}
		return -1;
	}

	public final void method2291(int i, int i_21_, int i_22_) {
		values[i] = i_22_;
		Class98_Sub50 class98_sub50 = (Class98_Sub50) aClass377_3242.get(i, -1);
		do {
			if (class98_sub50 == null) {
				class98_sub50 = new Class98_Sub50(500L + TimeTools.getCurrentTime(-47));
				aClass377_3242.put(class98_sub50, i, -1);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			class98_sub50.aLong4288 = TimeTools.getCurrentTime(-47) - -500L;
		} while (false);
	}

	public final void method2292(int i, int i_23_, int i_24_) {
		BConfigDefinition class366 = DataFs.bConfigDefinitionList.list(i, (byte) 98);
		int i_25_ = class366.bConfigId;
		if (i_24_ == -32727) {
			int i_26_ = class366.start;
			int i_27_ = class366.end;
			int i_28_ = Class98_Sub46_Sub20.bitmasks[i_27_ + -i_26_];
			if ((i_23_ ^ 0xffffffff) > -1 || i_28_ < i_23_) {
				i_23_ = 0;
			}
			i_28_ <<= i_26_;
			setConfig(i_24_ + 25258, i_23_ << i_26_ & i_28_ | configs[i_25_] & (i_28_ ^ 0xffffffff), i_25_);
		}
	}

	public final void setConfig(int i, int value, int id) {
		configs[id] = value;
		// System.out.println("Array configs: " +
		// Arrays.toString(this.configs));
		Class98_Sub50 class98_sub50 = (Class98_Sub50) aClass377_3242.get(id, -1);
		if (class98_sub50 != null) {
			if ((class98_sub50.aLong4288 ^ 0xffffffffffffffffL) != -4611686018427387906L) {
				class98_sub50.aLong4288 = 0x4000000000000000L | TimeTools.getCurrentTime(-47) - -500L;
			}
		} else {
			class98_sub50 = new Class98_Sub50(4611686018427387905L);
			aClass377_3242.put(class98_sub50, id, -1);
		}
	}
}

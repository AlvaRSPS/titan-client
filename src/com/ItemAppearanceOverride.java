package com;

import com.jagex.game.client.definition.ItemDefinition;

public class ItemAppearanceOverride {

	public static int and(int a, int b) {
		return a & b;
	}

	public static final ItemAppearanceOverride create(RSByteBuffer buffer, ItemDefinition def) {
		ItemAppearanceOverride override = new ItemAppearanceOverride(def);
		int attributes = buffer.readUnsignedByte((byte) -114);
		System.out.println("Attributes: " + attributes);
		boolean models = (0x1 & attributes) != 0;
		boolean headModels = (attributes & 0x2) != 0;
		boolean replacementColours = (0x4 & attributes) != 0;
		boolean replacementTextures = (0x8 & attributes) != 0;
		if (models) {
			override.wear[0] = buffer.readSmart32();
			System.out.println("Override wear 0: " + override.wear[0]);
			override.wear2[0] = buffer.readSmart32();
			if ((def.manWear2 ^ 0xffffffff) != 0 || (def.womanWear2 ^ 0xffffffff) != 0) {
				override.wear[1] = buffer.readSmart32();
				override.wear2[1] = buffer.readSmart32();
			}
			if (def.colourEquip1 != -1 || (def.womanWear3 ^ 0xffffffff) != 0) {
				override.wear[2] = buffer.readSmart32();
				override.wear2[2] = buffer.readSmart32();
			}
		}
		if (headModels) {
			override.manhead[0] = buffer.readSmart32();
			override.womanhead[0] = buffer.readSmart32();
			if (def.manHead2 != -1 || (def.womanHead2 ^ 0xffffffff) != 0) {
				override.manhead[1] = buffer.readSmart32();
				override.womanhead[1] = buffer.readSmart32();
			}
		}
		if (replacementColours) {
			int packed = buffer.readShort((byte) 127);
			int[] parts = new int[4];
			parts[3] = and(15, packed >> 12);
			parts[2] = and(15, packed >> 8);
			parts[0] = and(15, packed);
			parts[1] = and(packed, 255) >> 4;
			for (int index = 0; index < 4; index++) {
				if (parts[index] != 15)
					override.recol_d[parts[index]] = (short) buffer.readShort((byte) 127);
			}
		}
		if (replacementTextures) {
			int packed = buffer.readUnsignedByte((byte) -114);
			int[] parts = new int[2];
			parts[0] = and(packed, 15);
			parts[1] = and(255, packed) >> 4;
			for (int index = 0; index < 2; index++) {
				if (parts[index] != 15)
					override.retex_d[parts[index]] = (short) buffer.readShort((byte) 127);
			}
		}
		return override;
	}

	public int[]	wear		= new int[3];
	public int[]	wear2		= new int[3];
	public int[]	manhead		= new int[2];
	public int[]	womanhead	= new int[2];

	public short[]	recol_d;

	public short[]	retex_d;

	public ItemAppearanceOverride(ItemDefinition objtype) {
		wear[0] = objtype.manWear;
		wear[1] = objtype.manWear2;
		wear[2] = objtype.colourEquip1;
		wear2[2] = objtype.womanWear3;
		wear2[0] = objtype.womanWear;
		wear2[1] = objtype.womanWear2;
		manhead[1] = objtype.manHead2;
		manhead[0] = objtype.manHead;
		womanhead[0] = objtype.womanHead;
		womanhead[1] = objtype.womanHead2;
		if (objtype.recolD != null) {
			recol_d = new short[objtype.recolD.length];
			ArrayUtils.arrayCopy(objtype.recolD, 0, recol_d, 0, recol_d.length);
		}
		if (objtype.retexD != null) {
			retex_d = new short[objtype.retexD.length];
			ArrayUtils.arrayCopy(objtype.retexD, 0, retex_d, 0, retex_d.length);
		}
	}

}
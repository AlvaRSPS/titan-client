/* Class184 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.shadow.Shadow;

public final class Class184 {
	public static final boolean method2626(Shadow var_r, int i, int i_0_, int i_1_, boolean[] bools) {
		boolean bool = false;
		if (Class78.aSArray594 != Class81.aSArray618) {
			int i_2_ = StrongReferenceMCNode.aSArray6298[i].averageHeight(i_0_, i_1_, true);
			int i_3_ = 0;
			for (/**/; i_3_ <= i; i_3_++) {
				Ground var_s = StrongReferenceMCNode.aSArray6298[i_3_];
				if (var_s != null) {
					int i_4_ = i_2_ - var_s.averageHeight(i_0_, i_1_, true);
					if (bools != null) {
						bools[i_3_] = var_s.method3418(var_r, i_0_, i_4_, i_1_, 0, false);
						if (!bools[i_3_]) {
							continue;
						}
					}
					var_s.deleteShadow(var_r, i_0_, i_4_, i_1_, 0, false);
					bool = true;
				}
			}
		}
		return bool;
	}

	public static final boolean method2627(int i, char c) {
		if (c >= 32 && c <= 126) {
			return true;
		}
		if ((c ^ 0xffffffff) <= -161 && (c ^ 0xffffffff) >= -256) {
			return true;
		}
		if (i != 376) {
			return false;
		}
		return c == 8364 || c == 338 || c == 8212 || c == 339 || c == 376;
	}
}


/* Class246_Sub3_Sub4_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Rectangle;

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.GraphicsDefinition;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.ui.loading.impl.elements.config.AnimatedProgressBarLSEConfig;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.input.RtMouseEvent;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class246_Sub3_Sub4_Sub3 extends Class246_Sub3_Sub4 {
	public static OutgoingOpcode	aClass171_6446;
	public static IncomingOpcode	aClass58_6457;
	public static int[]				anIntArray6451	= new int[1000];

	static {
		aClass171_6446 = new OutgoingOpcode(2, -1);
		aClass58_6457 = new IncomingOpcode(55, -1);
	}

	public static final void method3070(int i) {
		try {
			if (i > -2) {
				aClass171_6446 = null;
			}
			RtMouseEvent class98_sub17 = (RtMouseEvent) VarPlayerDefinition.aClass148_1284.getFirst(32);
			boolean bool = Class255.aClass293_3208 != null || (Class156_Sub2.anInt3423 ^ 0xffffffff) < -1;
			int i_20_ = class98_sub17.getMouseX(-115);
			int i_21_ = class98_sub17.getMouseY(112);
			if (bool) {
				Class21_Sub4.anInt5396 = 1;
			}
			if (bool) {
				Class347.aClass98_Sub46_Sub8_2908 = SunDefinition.aClass98_Sub46_Sub8_1994;
			} else {
				JavaNetworkWriter.method3604(i_21_, (byte) 78, i_20_, SunDefinition.aClass98_Sub46_Sub8_1994);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.D(" + i + ')');
		}
	}

	public static final void method3071(int i, int i_22_, int i_23_, int i_24_, int i_25_) {
		try {
			if (i_22_ == -1) {
				for (int i_26_ = 0; (Class69_Sub2.anInt5335 ^ 0xffffffff) < (i_26_ ^ 0xffffffff); i_26_++) {
					Rectangle rectangle = PlatformInformation.aRectangleArray4144[i_26_];
					if (i_24_ < rectangle.width + rectangle.x && (rectangle.x ^ 0xffffffff) > (i_23_ + i_24_ ^ 0xffffffff) && (i_25_ ^ 0xffffffff) > (rectangle.y - -rectangle.height ^ 0xffffffff) && i + i_25_ > rectangle.y) {
						aa_Sub3.isDirty[i_26_] = true;
					}
				}
				AnimatedProgressBarLSEConfig.method908(i + i_25_, i_25_, false, i_24_, i_23_ + i_24_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.A(" + i + ',' + i_22_ + ',' + i_23_ + ',' + i_24_ + ',' + i_25_ + ')');
		}
	}

	public static void method3072(int i) {
		do {
			try {
				aClass171_6446 = null;
				anIntArray6451 = null;
				aClass58_6457 = null;
				if (i == 0) {
					break;
				}
				aClass58_6457 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "or.K(" + i + ')');
			}
			break;
		} while (false);
	}

	private boolean				aBoolean6449	= true;
	boolean						aBoolean6450;
	private Class246_Sub5		aClass246_Sub5_6448;
	private AnimationDefinition	aClass97_6458;
	private int					anInt6444;
	int							anInt6445;
	private int					anInt6447		= 0;
	private int					anInt6452;

	private int					anInt6453;

	private int					anInt6454;

	private int					anInt6455;

	private int					anInt6456;

	Class246_Sub3_Sub4_Sub3(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_) {
		super(i_2_, i_3_, i_4_, i_5_, i_6_, i_7_, i_8_, i_9_, i_10_, false, (byte) 0);
		anInt6444 = 0;
		anInt6454 = -1;
		anInt6456 = 0;
		aBoolean6450 = false;
		anInt6452 = 0;
		anInt6453 = 0;
		do {
			try {
				anInt6453 = i_11_;
				anInt6445 = i_1_ - -i_0_;
				anInt6455 = i;
				GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, anInt6455);
				int i_12_ = class107.animation;
				do {
					if (i_12_ != -1) {
						aClass97_6458 = Class151_Sub7.animationDefinitionList.method2623(i_12_, 16383);
						aBoolean6450 = false;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					aBoolean6450 = true;
				} while (false);
				if ((i_1_ ^ 0xffffffff) != (anInt6445 ^ 0xffffffff)) {
					break;
				}
				Class349.method3840((byte) -127, this, anInt6456, aClass97_6458);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "or.<init>(" + i + ',' + i_0_ + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ',' + i_9_ + ',' + i_10_ + ',' + i_11_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	protected final void finalize() {
		do {
			try {
				if (aClass246_Sub5_6448 == null) {
					break;
				}
				aClass246_Sub5_6448.method3114();
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "or.finalize(" + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (i != -53) {
				anIntArray6451 = null;
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = method3069(var_ha, 0x800 | ((anInt6453 ^ 0xffffffff) != -1 ? 5 : 0), -18981, anInt6455);
			if (class146 == null) {
				return null;
			}
			if ((anInt6453 ^ 0xffffffff) != -1) {
				class146.rotateYaw(2048 * anInt6453);
			}
			if (i > -12) {
				anInt6452 = -121;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
			method3068(class146, var_ha, -17770, class111);
			Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, false);
			if (VarClientStringsDefinitionParser.aBoolean1839) {
				class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
			} else {
				class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
			}
			if (aClass246_Sub5_6448 != null) {
				Class242 class242 = aClass246_Sub5_6448.method3116();
				if (!VarClientStringsDefinitionParser.aBoolean1839) {
					var_ha.method1820(class242);
				} else {
					var_ha.method1785(class242, Class16.anInt197);
				}
			}
			aBoolean6449 = class146.F();
			anInt6444 = class146.fa();
			anInt6447 = class146.ma();
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_27_, int i_28_) {
		try {
			if (i_27_ < 59) {
				return true;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_27_ + ',' + i_28_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.H(" + i + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_16_, RSToolkit var_ha, int i_17_, int i_18_) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_16_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_17_ + ',' + i_18_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i >= -70) {
				anInt6453 = -72;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.HA(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				anInt6447 = 45;
			}
			return anInt6447;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			return aBoolean6449;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		do {
			try {
				ModelRenderer class146 = method3069(var_ha, 0, -18981, anInt6455);
				if (class146 == null) {
					break;
				}
				Matrix class111 = var_ha.method1793();
				class111.method2100(((Char) this).boundExtentsX, ((Char) this).anInt5089, ((Char) this).boundExtentsZ);
				method3068(class146, var_ha, -17770, class111);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "or.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				return -58;
			}
			return anInt6444;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.J(" + i + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		try {
			if (i == -73) {
				throw new IllegalStateException();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.DA(" + i + ')');
		}
	}

	public final void method3067(int i) {
		try {
			if (aClass246_Sub5_6448 != null) {
				aClass246_Sub5_6448.method3114();
			}
			if (i <= 71) {
				method2981(null, (byte) -50, false, 126, null, 99, 56);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.B(" + i + ')');
		}
	}

	private final void method3068(ModelRenderer class146, RSToolkit var_ha, int i, Matrix class111) {
		do {
			try {
				class146.method2343(class111);
				Class87[] class87s = class146.method2320();
				Class35[] class35s = class146.method2322();
				if ((aClass246_Sub5_6448 == null || aClass246_Sub5_6448.aBoolean5099) && (class87s != null || class35s != null)) {
					aClass246_Sub5_6448 = Class246_Sub5.method3117(Queue.timer, true);
				}
				if (i != -17770) {
					method2975(null, 84);
				}
				if (aClass246_Sub5_6448 == null) {
					break;
				}
				aClass246_Sub5_6448.method3120(var_ha, Queue.timer, class87s, class35s, false);
				aClass246_Sub5_6448.method3123(((Char) this).plane, ((Class246_Sub3_Sub4) this).aShort6158, ((Class246_Sub3_Sub4) this).aShort6160, ((Class246_Sub3_Sub4) this).aShort6157, ((Class246_Sub3_Sub4) this).aShort6159);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "or.E(" + (class146 != null ? "{...}" : "null") + ',' + (var_ha != null ? "{...}" : "null") + ',' + i + ',' + (class111 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	private final ModelRenderer method3069(RSToolkit var_ha, int i, int i_13_, int i_14_) {
		try {
			GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, i_14_);
			Ground var_s = StrongReferenceMCNode.aSArray6298[((Char) this).plane];
			Ground var_s_15_ = ((Char) this).collisionPlane < 3 ? StrongReferenceMCNode.aSArray6298[((Char) this).collisionPlane + 1] : null;
			if (i_13_ != -18981) {
				method3067(92);
			}
			if (aBoolean6450) {
				return class107.method1722(var_ha, Class151_Sub7.animationDefinitionList, -1, i, ((Char) this).boundExtentsX, true, var_s_15_, ((Char) this).anInt5089, 0, var_s, ((Char) this).boundExtentsZ, -1, (byte) 2);
			}
			return class107.method1722(var_ha, Class151_Sub7.animationDefinitionList, anInt6456, i, ((Char) this).boundExtentsX, true, var_s_15_, ((Char) this).anInt5089, anInt6452, var_s, ((Char) this).boundExtentsZ, anInt6454, (byte) 2);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "or.C(" + (var_ha != null ? "{...}" : "null") + ',' + i + ',' + i_13_ + ',' + i_14_ + ')');
		}
	}

	public final void method3073(byte i, int i_29_) {
		do {
			try {
				if (!aBoolean6450) {
					anInt6452 += i_29_;
					while ((anInt6452 ^ 0xffffffff) < (aClass97_6458.frameLengths[anInt6456] ^ 0xffffffff)) {
						anInt6452 -= aClass97_6458.frameLengths[anInt6456];
						anInt6456++;
						if (anInt6456 >= aClass97_6458.frameIds.length) {
							aBoolean6450 = true;
							break;
						}
					}
					if (aBoolean6450) {
						break;
					}
					Class349.method3840((byte) -126, this, anInt6456, aClass97_6458);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "or.G(" + i + ',' + i_29_ + ')');
			}
			break;
		} while (false);
	}
}

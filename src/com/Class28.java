/* Class28 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.QuestDefinitionParser;
import com.jagex.game.input.impl.AwtMouseListener;

public final class Class28 {
	public static Class128	aClass128_286	= new Class128();
	public static float[]	aFloatArray294	= new float[2];

	public Class48			aClass48_287;
	public float			aFloat281;
	public float			aFloat288;
	public float			aFloat289;
	public float			aFloat291		= 0.25F;
	public float			aFloat293		= 1.0F;
	public float			aFloat295		= 1.0F;
	public int				anInt282;
	public int				anInt283;
	public int				anInt284;
	public int				anInt285;
	public int				anInt290;

	int						anInt292;

	public Class28() {
		aFloat288 = 0.69921875F;
		anInt282 = -50;
		aClass48_287 = Class246_Sub7.aClass48_5119;
		anInt292 = -50;
		anInt285 = Class189.anInt1455;
		anInt284 = -60;
		anInt290 = AwtMouseListener.anInt5298;
		anInt283 = 0;
		aFloat281 = 1.1523438F;
		aFloat289 = 1.2F;
	}

	/*
	 * @Override public String toString() { // System.out.println("field1: " +
	 * anInt290); // System.out.println("field2: " + anInt285); return null;
	 *
	 * }
	 */

	Class28(RSByteBuffer class98_sub22) {
		try {
			int i = class98_sub22.readUnsignedByte((byte) -100);
			if ((client.preferences.lightningDetail.getValue((byte) 126) ^ 0xffffffff) == -2 && OpenGlPointLight.aHa4185.method1822() > 0) {
				if ((0x1 & i ^ 0xffffffff) != -1) {
					anInt290 = class98_sub22.readInt(-2);
				} else {
					anInt290 = AwtMouseListener.anInt5298;
				}
				if ((0x2 & i) == 0) {
					aFloat281 = 1.1523438F;
				} else {
					aFloat281 = class98_sub22.readShort((byte) 127) / 256.0F;
				}
				if ((0x4 & i) != 0) {
					aFloat288 = class98_sub22.readShort((byte) 127) / 256.0F;
				} else {
					aFloat288 = 0.69921875F;
				}
				if ((0x8 & i ^ 0xffffffff) != -1) {
					aFloat289 = class98_sub22.readShort((byte) 127) / 256.0F;
				} else {
					aFloat289 = 1.2F;
				}
			} else {
				if ((0x1 & i ^ 0xffffffff) != -1) {
					class98_sub22.readInt(-2);
				}
				if ((0x2 & i ^ 0xffffffff) != -1) {
					class98_sub22.readShort((byte) 127);
				}
				if ((0x4 & i) != 0) {
					class98_sub22.readShort((byte) 127);
				}
				if ((i & 0x8) != 0) {
					class98_sub22.readShort((byte) 127);
				}
				aFloat281 = 1.1523438F;
				aFloat289 = 1.2F;
				anInt290 = AwtMouseListener.anInt5298;
				aFloat288 = 0.69921875F;
			}
			if ((0x10 & i) != 0) {
				anInt282 = class98_sub22.readUShort(false);
				anInt284 = class98_sub22.readUShort(false);
				anInt292 = class98_sub22.readUShort(false);
			} else {
				anInt284 = -60;
				anInt292 = -50;
				anInt282 = -50;
			}
			if ((0x20 & i ^ 0xffffffff) == -1) {
				anInt285 = Class189.anInt1455;
			} else {
				anInt285 = class98_sub22.readInt(-2);
			}
			if ((i & 0x40) == 0) {
				anInt283 = 0;
			} else {
				anInt283 = class98_sub22.readShort((byte) 127);
			}
			if ((i & 0x80) == 0) {
				aClass48_287 = Class246_Sub7.aClass48_5119;
			} else {
				int i_1_ = class98_sub22.readShort((byte) 127);
				int i_2_ = class98_sub22.readShort((byte) 127);
				int i_3_ = class98_sub22.readShort((byte) 127);
				int i_4_ = class98_sub22.readShort((byte) 127);
				int i_5_ = class98_sub22.readShort((byte) 127);
				int i_6_ = class98_sub22.readShort((byte) 127);
				aClass48_287 = QuestDefinitionParser.method217(5, i_4_, i_6_, i_5_, i_3_, i_1_, i_2_);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ca.<init>(" + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	public final void method297(int i, RSByteBuffer class98_sub22) {
		try {
			aFloat295 = 8 * class98_sub22.readUnsignedByte((byte) 96) / 255.0F;
			aFloat291 = class98_sub22.readUnsignedByte((byte) -122) * 8 / 255.0F;
			aFloat293 = 8 * class98_sub22.readUnsignedByte((byte) 1) / 255.0F;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ca.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ')');
		}
	}

	public final boolean method299(boolean bool, Class28 class28_0_) {
		try {
			if (bool != true) {
				return false;
			}
			return !((anInt290 ^ 0xffffffff) != (class28_0_.anInt290 ^ 0xffffffff) || aFloat281 != class28_0_.aFloat281 || aFloat288 != class28_0_.aFloat288 || aFloat289 != class28_0_.aFloat289 || class28_0_.aFloat291 != aFloat291 || class28_0_.aFloat295 != aFloat295 || class28_0_.aFloat293 != aFloat293
					|| (class28_0_.anInt285 ^ 0xffffffff) != (anInt285 ^ 0xffffffff) || anInt283 != class28_0_.anInt283 || aClass48_287 != class28_0_.aClass48_287);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ca.C(" + bool + ',' + (class28_0_ != null ? "{...}" : "null") + ')');
		}
	}
}

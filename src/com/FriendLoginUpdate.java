/* Class246_Sub4_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;

public final class FriendLoginUpdate extends Entity {
	public static Class258				aClass258_6169	= new Class258();
	public static IncomingOpcode		aClass58_6166	= new IncomingOpcode(98, -2);
	public static AdvancedMemoryCache	aClass79_6170	= new AdvancedMemoryCache(4);
	public static String[]				ignores			= new String[100];

	public static final void method3103(byte i) {
		do {
			if ((MaxScreenSizePreferenceField.anInt3680 ^ 0xffffffff) == -6) {
				MaxScreenSizePreferenceField.anInt3680 = 6;
				if (i == -38) {
					break;
				}
				method3104(52);
			}
			break;
		} while (false);
	}

	public static final void method3104(int i) {
		Class248.aClass377_1894.clear(-124);
		if (i == 5134) {
			BConfigDefinition.aClass377_3114.clear(i ^ ~0x1433);
		}
	}

	public static final void method3105(byte i, Class172[][][] class172s) {
		if (i >= -30) {
			aClass258_6169 = null;
		}
		for (int i_0_ = 0; (i_0_ ^ 0xffffffff) > (class172s.length ^ 0xffffffff); i_0_++) {
			Class172[][] class172s_1_ = class172s[i_0_];
			for (Class172[] element : class172s_1_) {
				for (int i_3_ = 0; (element.length ^ 0xffffffff) < (i_3_ ^ 0xffffffff); i_3_++) {
					Class172 class172 = element[i_3_];
					if (class172 != null) {
						if (class172.aClass246_Sub3_Sub1_1332 instanceof Interface19) {
							((Interface19) class172.aClass246_Sub3_Sub1_1332).method61((byte) -96);
						}
						if (class172.aClass246_Sub3_Sub5_1334 instanceof Interface19) {
							((Interface19) class172.aClass246_Sub3_Sub5_1334).method61((byte) -96);
						}
						if (class172.aClass246_Sub3_Sub5_1326 instanceof Interface19) {
							((Interface19) class172.aClass246_Sub3_Sub5_1326).method61((byte) -96);
						}
						if (class172.aClass246_Sub3_Sub3_1324 instanceof Interface19) {
							((Interface19) class172.aClass246_Sub3_Sub3_1324).method61((byte) -96);
						}
						if (class172.aClass246_Sub3_Sub3_1333 instanceof Interface19) {
							((Interface19) class172.aClass246_Sub3_Sub3_1333).method61((byte) -96);
						}
						for (Class154 class154 = class172.aClass154_1325; class154 != null; class154 = class154.aClass154_1233) {
							Class246_Sub3_Sub4 class246_sub3_sub4 = class154.aClass246_Sub3_Sub4_1232;
							if (class246_sub3_sub4 instanceof Interface19) {
								((Interface19) class246_sub3_sub4).method61((byte) -96);
							}
						}
					}
				}
			}
		}
	}

	String	friendName;

	short	onlineStatus;

	int		time;

	FriendLoginUpdate(String string, int i) {
		time = (int) (TimeTools.getCurrentTime(-47) / 1000L);
		friendName = string;
		onlineStatus = (short) i;
	}
}

/* Class34 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.SunDefinition;

public final class Class34 {
	public static boolean		aBoolean324		= false;
	public static boolean[][][]	aBooleanArrayArrayArray325;
	public static float[]		aFloatArray326	= new float[4];

	public static final void method328(int i) {
		do {
			try {
				Class232.aClass79_1740.freeSoftReferences((byte) 60);
				if (i == 0) {
					break;
				}
				method330((byte) -71, true);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cha.C(" + i + ')');
			}
			break;
		} while (false);
	}

	public static void method329(int i) {
		try {
			if (i != 0) {
				method330((byte) -103, false);
			}
			aBooleanArrayArrayArray325 = null;
			aFloatArray326 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cha.B(" + i + ')');
		}
	}

	public static final void method330(byte i, boolean bool) {
		do {
			try {
				if (Class140.loadingScreenRenderer == null) {
					SunDefinition.method3238(0);
				}
				if (!bool) {
					break;
				}
				Class140.loadingScreenRenderer.refresh(-7423);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cha.A(" + i + ',' + bool + ')');
			}
			break;
		} while (false);
	}
}

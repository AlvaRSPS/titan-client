/* Class246_Sub3_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.LightIntensityDefinition;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.definition.parser.InventoriesDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.UseCustomCursorPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.RotatingSpriteLSEConfig;

public abstract class Class246_Sub3_Sub1 extends Char {
	public static OutgoingOpcode			SEND_QUICK_CHAT_MESSAGE	= new OutgoingOpcode(61, -1);
	public static HitmarksDefinitionParser	hitmarksDefinitionList;

	public static final void method2994(int i, int i_1_, byte i_2_, int i_3_) {
		try {
			i_1_ = i_1_ * client.preferences.musicVolume.getValue((byte) 126) >> 288450632;
			if (i_2_ != -83) {
				method2995(-90, 54, -126, null, null);
			}
			do {
				if ((i ^ 0xffffffff) == 0 && !Class151_Sub5.aBoolean4991) {
					RotatingSpriteLSEConfig.method3777(31585);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if ((i ^ 0xffffffff) != 0 && ((Class144.anInt1169 ^ 0xffffffff) != (i ^ 0xffffffff) || !InventoriesDefinitionParser.method188(false)) && i_1_ != 0 && !Class151_Sub5.aBoolean4991) {
					UseCustomCursorPreferenceField.method674(i_1_, 0, i, Class98_Sub10_Sub1.musicJs5, i_3_, false, (byte) -86);
					Class233.method2883((byte) 111);
				}
			} while (false);
			if ((Class144.anInt1169 ^ 0xffffffff) != (i ^ 0xffffffff)) {
				Class151_Sub8.aClass98_Sub31_Sub2_5013 = null;
			}
			Class144.anInt1169 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hc.D(" + i + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}

	public static final void method2995(int i, int i_9_, int i_10_, Class246_Sub3_Sub5 class246_sub3_sub5, Class246_Sub3_Sub5 class246_sub3_sub5_11_) {
		Class172 class172 = Class100.method1693(i, i_9_, i_10_);
		if (class172 != null) {
			class172.aClass246_Sub3_Sub5_1334 = class246_sub3_sub5;
			class172.aClass246_Sub3_Sub5_1326 = class246_sub3_sub5_11_;
			int i_12_ = Class78.aSArray594 == Class81.aSArray618 ? 1 : 0;
			if (class246_sub3_sub5.method2978(-124)) {
				if (class246_sub3_sub5.method2987(6540)) {
					class246_sub3_sub5.animator = Class359.aClass246_Sub3Array3056[i_12_];
					Class359.aClass246_Sub3Array3056[i_12_] = class246_sub3_sub5;
				} else {
					class246_sub3_sub5.animator = LightIntensityDefinition.aClass246_Sub3Array3198[i_12_];
					LightIntensityDefinition.aClass246_Sub3Array3198[i_12_] = class246_sub3_sub5;
					Class358.aBoolean3033 = true;
				}
			} else {
				class246_sub3_sub5.animator = Class130.aClass246_Sub3Array1029[i_12_];
				Class130.aClass246_Sub3Array1029[i_12_] = class246_sub3_sub5;
			}
			if (class246_sub3_sub5_11_ != null) {
				if (class246_sub3_sub5_11_.method2978(-7)) {
					if (class246_sub3_sub5_11_.method2987(6540)) {
						class246_sub3_sub5_11_.animator = Class359.aClass246_Sub3Array3056[i_12_];
						Class359.aClass246_Sub3Array3056[i_12_] = class246_sub3_sub5_11_;
					} else {
						class246_sub3_sub5_11_.animator = LightIntensityDefinition.aClass246_Sub3Array3198[i_12_];
						LightIntensityDefinition.aClass246_Sub3Array3198[i_12_] = class246_sub3_sub5_11_;
						Class358.aBoolean3033 = true;
					}
				} else {
					class246_sub3_sub5_11_.animator = Class130.aClass246_Sub3Array1029[i_12_];
					Class130.aClass246_Sub3Array1029[i_12_] = class246_sub3_sub5_11_;
				}
			}
		}
	}

	short aShort6149;

	Class246_Sub3_Sub1(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		try {
			aShort6149 = (short) i_8_;
			this.boundExtentsX = i;
			this.collisionPlane = (byte) i_7_;
			this.plane = (byte) i_6_;
			this.anInt5089 = i_4_;
			this.boundExtentsZ = i_5_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hc.<init>(" + i + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	@Override
	public final boolean method2977(RSToolkit var_ha, byte i) {
		try {
			if (i != 77) {
				return true;
			}
			return Class76_Sub5.method758((byte) 62, this.collisionPlane, this.boundExtentsZ >> Class151_Sub8.tileScale, this.boundExtentsX >> Class151_Sub8.tileScale);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hc.AA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method2980(int i, PointLight[] class98_sub5s) {
		try {
			return method2989(this.boundExtentsX >> Class151_Sub8.tileScale, false, class98_sub5s, this.boundExtentsZ >> Class151_Sub8.tileScale);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hc.GA(" + i + ',' + (class98_sub5s != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final boolean method2991(boolean bool) {
		return Class74.aBooleanArrayArray551[Class259.anInt1959 + (this.boundExtentsX >> Class151_Sub8.tileScale) - Class241.anInt1845][-CharacterShadowsPreferenceField.anInt3714 + (this.boundExtentsZ >> Class151_Sub8.tileScale) + Class259.anInt1959];
	}
}

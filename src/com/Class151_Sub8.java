
/* Class151_Sub8 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

import jaggl.OpenGL;

public final class Class151_Sub8 extends Class151 {
	public static IncomingOpcode		VARCSTR_LARGE	= new IncomingOpcode(43, -1);
	public static Class98_Sub31_Sub2	aClass98_Sub31_Sub2_5013;
	public static int					tileScale;
	public static int					anInt5016;
	public static int[]					anIntArray5014	= new int[4096];

	static {
		for (int i = 0; (i ^ 0xffffffff) > -4097; i++) {
			anIntArray5014[i] = LoginOpcode.method2825(-83, i);
		}
		anInt5016 = 0;
	}

	public static void method2468(byte i) {
		try {
			aClass98_Sub31_Sub2_5013 = null;
			anIntArray5014 = null;
			VARCSTR_LARGE = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vj.B(" + i + ')');
		}
	}

	private boolean				aBoolean5010	= false;

	private OpenGLDisplayList	aClass91_5012;

	Class151_Sub8(OpenGlToolkit var_ha_Sub1) {
		super(var_ha_Sub1);
		do {
			try {
				if (!var_ha_Sub1.aBoolean4391) {
					break;
				}
				aClass91_5012 = new OpenGLDisplayList(var_ha_Sub1, 2);
				aClass91_5012.newList(0, -30389);
				this.aHa_Sub1_1215.method1845(1, 847872872);
				this.aHa_Sub1_1215.method1899(7681, 8960, 34165);
				this.aHa_Sub1_1215.method1840(2, 770, 86, 34168);
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 34167);
				OpenGL.glTexGeni(8192, 9472, 34066);
				OpenGL.glTexGeni(8193, 9472, 34066);
				OpenGL.glTexGeni(8194, 9472, 34066);
				OpenGL.glEnable(3168);
				OpenGL.glEnable(3169);
				OpenGL.glEnable(3170);
				this.aHa_Sub1_1215.method1845(0, 847872872);
				aClass91_5012.endList((byte) 100);
				aClass91_5012.newList(1, -30389);
				this.aHa_Sub1_1215.method1845(1, 847872872);
				this.aHa_Sub1_1215.method1899(8448, 8960, 8448);
				this.aHa_Sub1_1215.method1840(2, 770, -62, 34166);
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 5890);
				OpenGL.glDisable(3168);
				OpenGL.glDisable(3169);
				OpenGL.glDisable(3170);
				OpenGL.glMatrixMode(5890);
				OpenGL.glLoadIdentity();
				OpenGL.glMatrixMode(5888);
				this.aHa_Sub1_1215.method1845(0, 847872872);
				aClass91_5012.endList((byte) -125);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vj.<init>(" + (var_ha_Sub1 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final boolean method2439(int i) {
		try {
			if (i != 31565) {
				aClass98_Sub31_Sub2_5013 = null;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vj.A(" + i + ')');
		}
	}

	@Override
	public final void method2440(boolean bool, boolean bool_3_) {
		try {
			Class42_Sub2 class42_sub2 = this.aHa_Sub1_1215.method1827(-126);
			if (bool != false) {
				aClass98_Sub31_Sub2_5013 = null;
			}
			if (aClass91_5012 != null && class42_sub2 != null && bool_3_) {
				aClass91_5012.callList('\0', bool);
				this.aHa_Sub1_1215.method1845(1, 847872872);
				this.aHa_Sub1_1215.setActiveTexture(1, class42_sub2);
				OpenGL.glMatrixMode(5890);
				OpenGL.glLoadMatrixf(this.aHa_Sub1_1215.aClass111_Sub1_4354.method2116(54), 0);
				OpenGL.glMatrixMode(5888);
				this.aHa_Sub1_1215.method1845(0, 847872872);
				aBoolean5010 = true;
			} else {
				this.aHa_Sub1_1215.method1886(770, 0, 34200, 34168);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vj.D(" + bool + ',' + bool_3_ + ')');
		}
	}

	@Override
	public final void method2441(int i, int i_1_, int i_2_) {
		try {
			if (i_2_ > -2) {
				method2445((byte) 108);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vj.G(" + i + ',' + i_1_ + ',' + i_2_ + ')');
		}
	}

	@Override
	public final void method2442(Class42 class42, boolean bool, int i) {
		try {
			if (bool == false) {
				this.aHa_Sub1_1215.setActiveTexture(1, class42);
				this.aHa_Sub1_1215.method1896(260, i);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vj.F(" + (class42 != null ? "{...}" : "null") + ',' + bool + ',' + i + ')');
		}
	}

	@Override
	public final void method2443(boolean bool, int i) {
		do {
			try {
				this.aHa_Sub1_1215.method1899(7681, i + 8705, 8448);
				if (i == 255) {
					break;
				}
				VARCSTR_LARGE = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vj.C(" + bool + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final void method2445(byte i) {
		try {
			do {
				if (!aBoolean5010) {
					this.aHa_Sub1_1215.method1886(770, 0, 34200, 5890);
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				aClass91_5012.callList('\001', false);
				this.aHa_Sub1_1215.method1845(1, 847872872);
				this.aHa_Sub1_1215.setActiveTexture(1, null);
				this.aHa_Sub1_1215.method1845(0, 847872872);
			} while (false);
			this.aHa_Sub1_1215.method1899(8448, 8960, 8448);
			if (i <= 25) {
				aClass98_Sub31_Sub2_5013 = null;
			}
			aBoolean5010 = false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vj.E(" + i + ')');
		}
	}
}

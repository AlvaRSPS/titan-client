
/* Class138 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.toolkit.ground.Ground;

public final class Class138 {
	public static SceneGraphNodeList	aClass218_1084	= new SceneGraphNodeList();
	public static int					anInt1085;
	public static int[]					hueBuffer;

	public static final ChatLine getChatMessageByIndex(byte i, int messageIndex) {
		if (messageIndex < 0 || messageIndex >= 100) {
			return null;
		}
		return Class98_Sub46_Sub3.buffer[messageIndex];
	}

	public static final void method2278(int i, Ground var_s) {
		Class78.aSArray594[i] = var_s;
	}
}

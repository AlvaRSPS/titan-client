/* Class30 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class Class30 {
	public static final boolean method304(int i, byte[] is) {
		try {
			RSByteBuffer class98_sub22 = new RSByteBuffer(is);
			int i_0_ = class98_sub22.readUnsignedByte((byte) -100);
			if (i_0_ != 2) {
				return false;
			}
			boolean bool = (class98_sub22.readUnsignedByte((byte) -123) ^ 0xffffffff) == -2;
			if (bool) {
				OpenGlToolkit.method1853(2, class98_sub22);
			}
			Class291.method3415(104, class98_sub22);
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cca.A(" + i + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}
}

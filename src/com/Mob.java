/* Class246_Sub3_Sub4_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.GraphicsDefinition;
import com.jagex.game.client.definition.HitmarksDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.preferences.ScreenSizePreferenceField;
import com.jagex.game.client.preferences.VolumePreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.LoadingScreenElementFactory;
import com.jagex.game.client.ui.loading.impl.elements.impl.SimpleProgressBarLoadingScreenElement;
import com.jagex.game.constants.BuildLocation;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.heap.OpenGLHeap;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public abstract class Mob extends Class246_Sub3_Sub4 {
	public static int anInt6357;

	public static final void method3964(RsBitsBuffers packet, int i) {
		try {
			packet.startBitwiseAccess(0);
			int localPlayerId = OpenGLHeap.localPlayerIndex;
			Player player = Class87.localPlayer = Class151_Sub9.players[localPlayerId] = new Player();
			player.index = localPlayerId;
			int i_10_ = packet.readBits((byte) -108, 30);
			byte i_11_ = (byte) (i_10_ >> -638775876);
			int i_12_ = (0xffff6b1 & i_10_) >> -190559890;
			((Mob) player).pathX[0] = i_12_ - Class272.gameSceneBaseX;
			int i_13_ = 0x3fff & i_10_;
			player.boundExtentsX = (player.pathX[0] << 152480009) - -(player.getSize(i ^ i) << 15811816);
			((Mob) player).pathZ[0] = -aa_Sub2.gameSceneBaseY + i_13_;
			player.boundExtentsZ = (player.pathZ[0] << 652406601) + (player.getSize(0) << 2125038824);
			Font.localPlane = player.plane = player.collisionPlane = i_11_;
			if (Class1.isBridge(player.pathZ[0], (byte) -117, player.pathX[0])) {
				player.collisionPlane++;
			}
			if (Class224_Sub3_Sub1.aClass98_Sub22Array6146[localPlayerId] != null) {
				player.decodeAppearance(Class224_Sub3_Sub1.aClass98_Sub22Array6146[localPlayerId], (byte) 73);
			}
			Class2.playerCount = 0;
			Class319.playerIndices[Class2.playerCount++] = localPlayerId;
			GamePreferences.aByteArray4075[localPlayerId] = (byte) 0;
			SimpleProgressBarLoadingScreenElement.anInt5473 = 0;
			for (int i_14_ = 1; (i_14_ ^ 0xffffffff) > -2049; i_14_++) {
				if ((i_14_ ^ 0xffffffff) != (localPlayerId ^ 0xffffffff)) {
					int i_15_ = packet.readBits((byte) -20, 18);
					int i_16_ = i_15_ >> 1414608112;
					int i_17_ = (i_15_ & 0xffe4) >> 251705128;
					int i_18_ = i_15_ & 0xff;
					GlobalPlayer class376 = EnumDefinition.aClass376Array2562[i_14_] = new GlobalPlayer();
					class376.positionHash = (i_16_ << -148957092) - (-(i_17_ << -430930482) - i_18_);
					class376.clanmate = false;
					class376.target = -1;
					class376.nextDirection = 0;
					Class76_Sub9.anIntArray3791[SimpleProgressBarLoadingScreenElement.anInt5473++] = i_14_;
					GamePreferences.aByteArray4075[i_14_] = (byte) 0;
				}
			}
			packet.endBitwiseAccess((byte) 120);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "wl.J(" + (packet != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	boolean					aBoolean6348	= false;
	boolean					aBoolean6359;
	public boolean			aBoolean6371;
	boolean					aBoolean6440;
	boolean					aBoolean6442;
	private byte			aByte6355;
	byte					aByte6368;
	byte					aByte6381;
	byte					aByte6404;
	byte					aByte6422;
	byte[]					pathSpeed;
	public ModelRenderer[]	aClass146Array6441;
	public Class226[]		aClass226Array6387;
	public Class246_Sub5	aClass246_Sub5_6439;
	public Orientation		yaw;
	private Orientation		roll;
	private Orientation		pitch;
	int						anInt6347;
	int						anInt6349;
	int						anInt6350;
	int						anInt6351;
	int						anInt6352;
	int						anInt6353		= -1;
	int						anInt6354;
	int						anInt6356;
	int						anInt6358		= 0;
	int						anInt6360;
	int						anInt6361		= -1;
	int						anInt6362;
	int						anInt6363;
	public int				anInt6364;
	int						anInt6365;
	int						anInt6366		= 0;
	int						anInt6367;
	int						anInt6372;
	int						anInt6376;
	int						anInt6377;
	int						anInt6378;
	int						anInt6379;
	int						anInt6380;
	int						anInt6382;
	int						anInt6384;
	int						anInt6385;
	int						anInt6388;
	int						anInt6389;
	int						anInt6390;
	int						anInt6391;
	int						anInt6392;
	int						anInt6393;
	int						anInt6394;
	private int				anInt6395;
	int						anInt6396;
	int						anInt6398;
	int						anInt6400;
	int						anInt6401;
	int						anInt6402;
	int						anInt6403;
	int						anInt6405;
	public int				anInt6406;
	int						anInt6407;
	int						anInt6408;
	int						anInt6409;
	int						anInt6410;
	int						anInt6411;
	int						anInt6412;
	int						anInt6413;
	public int				anInt6414;
	int						anInt6415;
	int						anInt6416;
	int						anInt6417;
	int						anInt6418;
	int						anInt6419;
	int						anInt6420;
	int						anInt6421;
	int						anInt6424;
	int						anInt6426;
	int						anInt6427;
	int						anInt6428;
	int						anInt6429;
	int						anInt6433;
	int						pathLength;
	int						anInt6435;
	int						anInt6436;
	int[]					anIntArray6370;
	int[]					anIntArray6373;
	int[]					anIntArray6375;
	int[]					anIntArray6383;
	int[]					anIntArray6386;
	int[]					anIntArray6397;
	int[]					anIntArray6423;
	int[]					anIntArray6425;
	int[]					anIntArray6430;
	public int				index;
	String					message;

	public int[]			pathX;

	public int[]			pathZ;

	public Mob() {
		super(0, 0, 0, 0, 0, 0, 0, 0, 0, false, (byte) 0);
		aBoolean6359 = false;
		anInt6356 = 0;
		anInt6351 = 0;
		anInt6354 = 0;
		anInt6379 = -1;
		anInt6352 = -32768;
		anInt6385 = -1;
		anInt6367 = -1;
		aByte6355 = (byte) 0;
		anInt6380 = 0;
		aClass226Array6387 = new Class226[12];
		anInt6396 = 0;
		anInt6349 = -1;
		anIntArray6397 = new int[LoadingScreenElementFactory.anInt3090];
		anInt6350 = 0;
		message = null;
		anIntArray6375 = new int[LoadingScreenElementFactory.anInt3090];
		anInt6364 = -1;
		aBoolean6371 = true;
		anInt6406 = 0;
		anInt6403 = -1;
		anInt6405 = 0;
		anInt6376 = 0;
		anInt6410 = -1;
		anInt6414 = 256;
		anIntArray6373 = null;
		anInt6402 = 0;
		anInt6384 = 100;
		anIntArray6386 = new int[LoadingScreenElementFactory.anInt3090];
		anInt6398 = 0;
		anInt6412 = -1000;
		anInt6409 = 0;
		anInt6419 = -1;
		anInt6413 = -1;
		anInt6418 = -1000;
		anInt6408 = 0;
		anIntArray6423 = new int[LoadingScreenElementFactory.anInt3090];
		anInt6372 = 0;
		anInt6427 = 0;
		anInt6365 = -1;
		anInt6400 = 0;
		anInt6421 = -1;
		anIntArray6425 = new int[LoadingScreenElementFactory.anInt3090];
		aByte6422 = (byte) 0;
		anInt6411 = -1;
		anInt6393 = 0;
		anInt6428 = 0;
		anInt6395 = 1;
		anIntArray6430 = new int[LoadingScreenElementFactory.anInt3090];
		yaw = new Orientation();
		roll = new Orientation();
		pitch = new Orientation();
		pathLength = 0;
		pathX = new int[10];
		pathZ = new int[10];
		anInt6433 = 0;
		aClass146Array6441 = new ModelRenderer[3];
		anInt6435 = 0;
		anInt6436 = 0;
		aBoolean6442 = false;
		aBoolean6440 = false;
		pathSpeed = new byte[10];
	}

	@Override
	protected final void finalize() {
		try {
			if (aClass246_Sub5_6439 != null) {
				aClass246_Sub5_6439.method3114();
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.finalize(" + ')');
		}
	}

	abstract int getArmyIcon(int i);

	abstract int getRenderId(byte i);

	public int getSize(int i) {
		try {
			if (i != 0) {
				return -77;
			}
			return anInt6395;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.A(" + i + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.H(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				anInt6362 = -77;
			}
			return anInt6354;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (i != 6540) {
				setSize((byte) -59, 60);
			}
			return aBoolean6442;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.I(" + i + ')');
		}
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				return 30;
			}
			if ((anInt6352 ^ 0xffffffff) == 32767) {
				return 0;
			}
			return anInt6352;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.J(" + i + ')');
		}
	}

	@Override
	public final void method3022(int i) {
		try {
			if (i != -8675) {
				method3037(-16, true, -64, 100, -58, -61, -88, 24);
			}
			int i_0_ = (-1 + anInt6395 << 1914403080) + 240;
			this.aShort6159 = (short) (i_0_ + this.boundExtentsZ >> 1380990889);
			this.aShort6160 = (short) (this.boundExtentsX + i_0_ >> 1794948489);
			this.aShort6157 = (short) (this.boundExtentsZ + -i_0_ >> 956113769);
			this.aShort6158 = (short) (-i_0_ + this.boundExtentsX >> 12223625);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.F(" + i + ')');
		}
	}

	public final void method3031(int i) {
		try {
			anInt6436 = i;
			pathLength = 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.R(" + i + ')');
		}
	}

	public final void method3032(int i, boolean bool, int i_1_, int i_2_, int i_3_, int i_4_) {
		do {
			try {
				int i_5_ = bool ? anInt6365 : anInt6379;
				if ((i_3_ ^ 0xffffffff) != 0 && i_5_ != -1) {
					if (i_5_ != i_3_) {
						GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, i_3_);
						GraphicsDefinition class107_6_ = BuildLocation.gfxDefinitionList.method3564(2, i_5_);
						if (class107.animation != -1 && (class107_6_.animation ^ 0xffffffff) != 0) {
							AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383);
							AnimationDefinition class97_7_ = Class151_Sub7.animationDefinitionList.method2623(class107_6_.animation, 16383);
							if (class97_7_.anInt829 > class97.anInt829) {
								break;
							}
						}
					} else {
						GraphicsDefinition class107 = BuildLocation.gfxDefinitionList.method3564(2, i_3_);
						if (class107.aBoolean909 && class107.animation != -1) {
							AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(class107.animation, 16383);
							int i_8_ = class97.anInt819;
							if ((i_8_ ^ 0xffffffff) == -1) {
								break;
							}
							if (i_8_ == 2) {
								if (!bool) {
									anInt6356 = 0;
								} else {
									anInt6380 = 0;
									break;
								}
								break;
							}
						}
					}
				}
				if (i_4_ >= -69) {
					getArmyIcon(-115);
				}
				int i_9_ = Queue.timer;
				if (bool) {
					anInt6421 = 1;
					anInt6426 = i_9_ - -(i_1_ & 0xffff);
					anInt6427 = 0;
					anInt6365 = i_3_;
					anInt6363 = i_1_ >> 813121872;
					anInt6428 = 0;
					anInt6353 = i;
					anInt6360 = i_2_;
					if (i_9_ < anInt6426) {
						anInt6428 = -1;
					}
					if ((anInt6365 ^ 0xffffffff) != 0 && i_9_ == anInt6426) {
						int i_10_ = BuildLocation.gfxDefinitionList.method3564(2, anInt6365).animation;
						if (i_10_ != -1) {
							AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_10_, 16383);
							if (class97 != null && class97.frameIds != null && !aBoolean6371) {
								Class349.method3840((byte) -128, this, 0, class97);
							}
						}
					}
				} else {
					anInt6396 = 0;
					anInt6367 = 1;
					anInt6379 = i_3_;
					anInt6410 = i;
					anInt6389 = i_2_;
					anInt6391 = i_9_ + (i_1_ & 0xffff);
					anInt6382 = i_1_ >> -1127488624;
					anInt6376 = 0;
					if ((i_9_ ^ 0xffffffff) > (anInt6391 ^ 0xffffffff)) {
						anInt6376 = -1;
					}
					if ((anInt6379 ^ 0xffffffff) == 0 || anInt6391 != i_9_) {
						break;
					}
					int i_11_ = BuildLocation.gfxDefinitionList.method3564(2, anInt6379).animation;
					if ((i_11_ ^ 0xffffffff) == 0) {
						break;
					}
					AnimationDefinition class97 = Class151_Sub7.animationDefinitionList.method2623(i_11_, 16383);
					if (class97 == null || class97.frameIds == null || aBoolean6371) {
						break;
					}
					Class349.method3840((byte) -128, this, 0, class97);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "oi.S(" + i + ',' + bool + ',' + i_1_ + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ')');
			}
			break;
		} while (false);
	}

	public final int method3033(byte i) {
		try {
			RenderAnimDefinition class294 = method3039(1);
			int i_13_ = yaw.value;
			boolean bool;
			if (class294.anInt2398 == 0) {
				bool = yaw.pulse(4201, anInt6414, anInt6415, anInt6414);
			} else {
				bool = yaw.pulse(4201, class294.anInt2383, anInt6415, class294.anInt2398);
			}
			int i_14_ = -i_13_ + yaw.value;
			if (i_14_ != 0) {
				anInt6408++;
			} else {
				anInt6408 = 0;
				yaw.update(true, anInt6415);
			}
			if (bool) {
				if ((class294.anInt2390 ^ 0xffffffff) != -1) {
					if ((i_14_ ^ 0xffffffff) < -1) {
						roll.pulse(4201, class294.anInt2392, class294.anInt2393, class294.anInt2390);
					} else {
						roll.pulse(4201, class294.anInt2392, -class294.anInt2393, class294.anInt2390);
					}
				}
				if (class294.anInt2375 != 0) {
					pitch.pulse(4201, class294.anInt2380, class294.anInt2363, class294.anInt2375);
				}
			} else {
				if ((class294.anInt2390 ^ 0xffffffff) == -1) {
					roll.update(true, 0);
				} else {
					roll.pulse(4201, class294.anInt2392, 0, class294.anInt2390);
				}
				if (class294.anInt2375 != 0) {
					pitch.pulse(4201, class294.anInt2380, 0, class294.anInt2375);
				} else {
					pitch.update(true, 0);
				}
			}
			return i_14_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.L(" + i + ')');
		}
	}

	public final void method3036(RSToolkit var_ha, boolean bool, byte i, Matrix class111, ModelRenderer[] class146s) {
		do {
			try {
				if (i <= -118) {
					do {
						if (bool) {
							if (aClass246_Sub5_6439 == null) {
								break;
							}
							aClass246_Sub5_6439.method3126(Queue.timer);
							if (!GameShell.cleanedStatics) {
								break;
							}
						}
						ModelRenderer class146 = class146s[0];
						ModelRenderer class146_15_ = class146s[1];
						ModelRenderer class146_16_ = class146s[2];
						if (class146 != null) {
							class146.method2343(class111);
							int i_17_ = 0;
							int i_18_ = 0;
							int i_19_ = 0;
							int i_20_ = 0;
							Class87[] class87s = class146.method2320();
							Class35[] class35s = class146.method2322();
							if (class35s != null) {
								i_20_++;
								i_19_ += class35s.length;
							}
							if (class87s != null) {
								i_18_++;
								i_17_ += class87s.length;
							}
							Class87[] class87s_21_ = null;
							Class35[] class35s_22_ = null;
							if (class146_15_ != null) {
								class146_15_.method2343(class111);
								class87s_21_ = class146_15_.method2320();
								class35s_22_ = class146_15_.method2322();
							}
							if (class35s_22_ != null) {
								i_19_ += class35s_22_.length;
								i_20_++;
							}
							if (class87s_21_ != null) {
								i_17_ += class87s_21_.length;
								i_18_++;
							}
							Class87[] class87s_23_ = null;
							Class35[] class35s_24_ = null;
							if (class146_16_ != null) {
								class146_16_.method2343(class111);
								class87s_23_ = class146_16_.method2320();
								class35s_24_ = class146_16_.method2322();
							}
							if (class35s_24_ != null) {
								i_19_ += class35s_24_.length;
								i_20_++;
							}
							if (class87s_23_ != null) {
								i_18_++;
								i_17_ += class87s_23_.length;
							}
							if ((aClass246_Sub5_6439 == null || aClass246_Sub5_6439.aBoolean5099) && ((i_18_ ^ 0xffffffff) < -1 || (i_20_ ^ 0xffffffff) < -1)) {
								aClass246_Sub5_6439 = Class246_Sub5.method3117(Queue.timer, true);
							}
							if (aClass246_Sub5_6439 != null) {
								Class87[] class87s_25_;
								if (i_18_ == 1) {
									if (class87s_21_ == null) {
										if (class87s_23_ != null) {
											class87s_25_ = class87s_23_;
										} else {
											class87s_25_ = class87s;
										}
									} else {
										class87s_25_ = class87s_21_;
									}
								} else {
									class87s_25_ = new Class87[i_17_];
									int i_26_ = 0;
									if (class87s != null) {
										ArrayUtils.method2892(class87s, 0, class87s_25_, i_26_, class87s.length);
										i_26_ += class87s.length;
									}
									if (class87s_21_ != null) {
										ArrayUtils.method2892(class87s_21_, 0, class87s_25_, i_26_, class87s_21_.length);
										i_26_ += class87s_21_.length;
									}
									if (class87s_23_ != null) {
										ArrayUtils.method2892(class87s_23_, 0, class87s_25_, i_26_, class87s_23_.length);
									}
								}
								Class35[] class35s_28_;
								if (i_20_ != 1) {
									class35s_28_ = new Class35[i_19_];
									int i_29_ = 0;
									if (class35s != null) {
										ArrayUtils.method2892(class35s, 0, class35s_28_, i_29_, class35s.length);
										i_29_ += class35s.length;
									}
									if (class35s_22_ != null) {
										ArrayUtils.method2892(class35s_22_, 0, class35s_28_, i_29_, class35s_22_.length);
										i_29_ += class35s_22_.length;
									}
									if (class35s_24_ != null) {
										ArrayUtils.method2892(class35s_24_, 0, class35s_28_, i_29_, class35s_24_.length);
									}
								} else if (class35s_22_ != null) {
									class35s_28_ = class35s_22_;
								} else if (class35s_24_ == null) {
									class35s_28_ = class35s;
								} else {
									class35s_28_ = class35s_24_;
								}
								aClass246_Sub5_6439.method3120(var_ha, Queue.timer, class87s_25_, class35s_28_, false);
							}
							aBoolean6440 = true;
						}
					} while (false);
					if (aClass246_Sub5_6439 == null) {
						break;
					}
					aClass246_Sub5_6439.method3123(this.plane, this.aShort6158, this.aShort6160, this.aShort6157, this.aShort6159);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "oi.N(" + (var_ha != null ? "{...}" : "null") + ',' + bool + ',' + i + ',' + (class111 != null ? "{...}" : "null") + ',' + (class146s != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final void method3037(int i, boolean bool, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_) {
		try {
			boolean bool_36_ = true;
			boolean bool_37_ = true;
			for (int i_38_ = 0; (LoadingScreenElementFactory.anInt3090 ^ 0xffffffff) < (i_38_ ^ 0xffffffff); i_38_++) {
				if ((anIntArray6375[i_38_] ^ 0xffffffff) < (i_32_ ^ 0xffffffff)) {
					bool_36_ = false;
				} else {
					bool_37_ = false;
				}
			}
			int i_39_ = -1;
			int i_40_ = -1;
			int i_41_ = 0;
			if ((i_31_ ^ 0xffffffff) <= -1) {
				HitmarksDefinition class86 = Class246_Sub3_Sub1.hitmarksDefinitionList.method2194(-62, i_31_);
				i_40_ = class86.comparisonType;
				i_41_ = class86.duration;
			}
			if (bool == false) {
				if (bool_37_) {
					if ((i_40_ ^ 0xffffffff) == 0) {
						return;
					}
					i_39_ = 0;
					int i_42_ = 0;
					if ((i_40_ ^ 0xffffffff) != -1) {
						if ((i_40_ ^ 0xffffffff) == -2) {
							i_42_ = anIntArray6425[0];
						}
					} else {
						i_42_ = anIntArray6375[0];
					}
					for (int i_43_ = 1; (LoadingScreenElementFactory.anInt3090 ^ 0xffffffff) < (i_43_ ^ 0xffffffff); i_43_++) {
						if ((i_40_ ^ 0xffffffff) == -1) {
							if ((i_42_ ^ 0xffffffff) < (anIntArray6375[i_43_] ^ 0xffffffff)) {
								i_39_ = i_43_;
								i_42_ = anIntArray6375[i_43_];
							}
						} else if ((i_40_ ^ 0xffffffff) == -2 && i_42_ > anIntArray6425[i_43_]) {
							i_39_ = i_43_;
							i_42_ = anIntArray6425[i_43_];
						}
					}
					if ((i_40_ ^ 0xffffffff) == -2 && (i_30_ ^ 0xffffffff) >= (i_42_ ^ 0xffffffff)) {
						return;
					}
				} else {
					if (bool_36_) {
						aByte6355 = (byte) 0;
					}
					for (int i_44_ = 0; (i_44_ ^ 0xffffffff) > (LoadingScreenElementFactory.anInt3090 ^ 0xffffffff); i_44_++) {
						int i_45_ = aByte6355;
						aByte6355 = (byte) ((aByte6355 + 1) % LoadingScreenElementFactory.anInt3090);
						if ((i_32_ ^ 0xffffffff) <= (anIntArray6375[i_45_] ^ 0xffffffff)) {
							i_39_ = i_45_;
							break;
						}
					}
				}
				if (i_39_ >= 0) {
					anIntArray6430[i_39_] = i_31_;
					anIntArray6425[i_39_] = i_30_;
					anIntArray6386[i_39_] = i_35_;
					anIntArray6423[i_39_] = i_33_;
					anIntArray6375[i_39_] = i_41_ + i_32_ + i_34_;
					anIntArray6397[i_39_] = i;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.V(" + i + ',' + bool + ',' + i_30_ + ',' + i_31_ + ',' + i_32_ + ',' + i_33_ + ',' + i_34_ + ',' + i_35_ + ')');
		}
	}

	public final void method3038(int[] is, int[] is_46_, boolean bool) {
		try {
			if (bool != true) {
				anInt6352 = -44;
			}
			if (anIntArray6383 != null || is_46_ == null) {
				if (is_46_ == null) {
					anIntArray6383 = null;
					return;
				}
			} else {
				anIntArray6383 = new int[12];
			}
			for (int i = 0; anIntArray6383.length > i; i++) {
				anIntArray6383[i] = -1;
			}
			for (int i = 0; (i ^ 0xffffffff) > (is_46_.length ^ 0xffffffff); i++) {
				int i_47_ = is[i];
				int i_48_ = 0;
				while ((anIntArray6383.length ^ 0xffffffff) < (i_48_ ^ 0xffffffff)) {
					if ((i_47_ & 0x1 ^ 0xffffffff) != -1) {
						anIntArray6383[i_48_] = is_46_[i];
					}
					i_48_++;
					i_47_ >>= 1;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.M(" + (is != null ? "{...}" : "null") + ',' + (is_46_ != null ? "{...}" : "null") + ',' + bool + ')');
		}
	}

	public final RenderAnimDefinition method3039(int i) {
		try {
			if (i != 1) {
				method3032(125, false, -5, 97, 58, -63);
			}
			int i_49_ = getRenderId((byte) 70);
			if (i_49_ != -1) {
				return Class370.renderAnimDefinitionList.method3199(false, i_49_);
			}
			return VolumePreferenceField.aClass294_3706;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.U(" + i + ')');
		}
	}

	public final void method3040(boolean bool, int i, int i_50_, int i_51_, int i_52_, int i_53_) {
		try {
			int i_54_ = this.aShort6158 + this.aShort6160 >> -1012169567;
			int i_55_ = this.aShort6157 + this.aShort6159 >> -988754175;
			int i_56_ = Class284_Sub2_Sub2.SINE[i_52_];
			int i_57_ = Class284_Sub2_Sub2.COSINE[i_52_];
			int i_58_ = -i_50_ / 2;
			int i_59_ = -i_51_ / 2;
			int i_60_ = i_58_ * i_57_ + i_59_ * i_56_ >> -1025229234;
			int i_61_ = i_57_ * i_59_ - i_56_ * i_58_ >> -184895538;
			if (bool != false) {
				method3039(126);
			}
			int i_62_ = ScreenSizePreferenceField.method664(this.boundExtentsZ + i_61_, i_55_, 109, this.plane, this.boundExtentsX + i_60_, i_54_);
			int i_63_ = i_50_ / 2;
			int i_64_ = -i_51_ / 2;
			int i_65_ = i_64_ * i_56_ - -(i_57_ * i_63_) >> 755643662;
			int i_66_ = i_57_ * i_64_ - i_63_ * i_56_ >> -1527044626;
			int i_67_ = ScreenSizePreferenceField.method664(this.boundExtentsZ - -i_66_, i_55_, 125, this.plane, i_65_ + this.boundExtentsX, i_54_);
			int i_68_ = -i_50_ / 2;
			int i_69_ = i_51_ / 2;
			int i_70_ = i_56_ * i_69_ + i_68_ * i_57_ >> 726596686;
			int i_71_ = i_69_ * i_57_ - i_56_ * i_68_ >> -1452830226;
			int i_72_ = ScreenSizePreferenceField.method664(i_71_ + this.boundExtentsZ, i_55_, 89, this.plane, i_70_ + this.boundExtentsX, i_54_);
			int i_73_ = i_50_ / 2;
			int i_74_ = i_51_ / 2;
			int i_75_ = i_73_ * i_57_ + i_56_ * i_74_ >> -26644050;
			int i_76_ = -(i_56_ * i_73_) + i_57_ * i_74_ >> 352446958;
			int i_77_ = ScreenSizePreferenceField.method664(i_76_ + this.boundExtentsZ, i_55_, 111, this.plane, i_75_ + this.boundExtentsX, i_54_);
			int i_78_ = i_67_ > i_62_ ? i_62_ : i_67_;
			int i_79_ = (i_72_ ^ 0xffffffff) <= (i_77_ ^ 0xffffffff) ? i_77_ : i_72_;
			int i_80_ = i_77_ <= i_67_ ? i_77_ : i_67_;
			int i_81_ = i_72_ > i_62_ ? i_62_ : i_72_;
			anInt6388 = 0x3fff & (int) (2607.5945876176133 * Math.atan2(-i_79_ + i_78_, i_51_));
			anInt6377 = 0x3fff & (int) (Math.atan2(i_81_ + -i_80_, i_50_) * 2607.5945876176133);
			if ((anInt6388 ^ 0xffffffff) != -1 && (i ^ 0xffffffff) != -1) {
				int i_82_ = 16384 - i;
				if (anInt6388 <= 8192) {
					if ((anInt6388 ^ 0xffffffff) < (i ^ 0xffffffff)) {
						anInt6388 = i;
					}
				} else if (anInt6388 < i_82_) {
					anInt6388 = i_82_;
				}
			}
			if (anInt6377 != 0 && i_53_ != 0) {
				int i_83_ = -i_53_ + 16384;
				if (anInt6377 > 8192) {
					if (i_83_ > anInt6377) {
						anInt6377 = i_83_;
					}
				} else if (anInt6377 > i_53_) {
					anInt6377 = i_53_;
				}
			}
			anInt6416 = i_62_ - -i_77_;
			if (anInt6416 > i_72_ + i_67_) {
				anInt6416 = i_67_ - -i_72_;
			}
			anInt6416 = -this.anInt5089 + (anInt6416 >> 1518450497);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.C(" + bool + ',' + i + ',' + i_50_ + ',' + i_51_ + ',' + i_52_ + ',' + i_53_ + ')');
		}
	}

	public final void method3042(int i, int i_96_) {
		try {
			RenderAnimDefinition class294 = method3039(1);
			if ((class294.anInt2398 ^ 0xffffffff) != -1 || (anInt6414 ^ 0xffffffff) != -1) {
				yaw.normalize(i_96_ + 452);
				int i_97_ = -yaw.value + i & 0x3fff;
				if ((i_97_ ^ 0xffffffff) < i_96_) {
					anInt6415 = -16384 - (-i_97_ - yaw.value);
				} else {
					anInt6415 = yaw.value - -i_97_;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.P(" + i + ',' + i_96_ + ')');
		}
	}

	public final boolean method3043(int i, int i_98_, int i_99_) {
		try {
			if (anIntArray6370 == null) {
				if (i_99_ == -1) {
					return true;
				}
				anIntArray6370 = new int[12];
				for (int i_100_ = 0; (i_100_ ^ 0xffffffff) > -13; i_100_++) {
					anIntArray6370[i_100_] = -1;
				}
			}
			RenderAnimDefinition class294 = method3039(1);
			if (i != 12) {
				setSize((byte) 34, 119);
			}
			int i_101_ = 256;
			if (class294.anIntArray2373 != null && class294.anIntArray2373[i_98_] > 0) {
				i_101_ = class294.anIntArray2373[i_98_];
			}
			if (i_99_ == -1) {
				if (anIntArray6370[i_98_] == -1) {
					return true;
				}
				int i_102_ = yaw.value((byte) 116);
				int i_103_ = anIntArray6370[i_98_];
				int i_104_ = -i_103_ + i_102_;
				if (-i_101_ <= i_104_ && (i_101_ ^ 0xffffffff) <= (i_104_ ^ 0xffffffff)) {
					anIntArray6370[i_98_] = -1;
					for (int i_105_ = 0; i_105_ < 12; i_105_++) {
						if ((anIntArray6370[i_105_] ^ 0xffffffff) != 0) {
							return true;
						}
					}
					anIntArray6370 = null;
					return true;
				}
				if ((i_104_ <= 0 || i_104_ > 8192) && (i_104_ ^ 0xffffffff) < 8191) {
					anIntArray6370[i_98_] = Class202.and(16383, -i_101_ + i_103_);
				} else {
					anIntArray6370[i_98_] = Class202.and(i_101_ + i_103_, 16383);
				}
				return false;
			}
			if (anIntArray6370[i_98_] == -1) {
				anIntArray6370[i_98_] = yaw.value((byte) 116);
			}
			int i_106_ = anIntArray6370[i_98_];
			int i_107_ = i_99_ + -i_106_;
			if ((i_107_ ^ 0xffffffff) <= (-i_101_ ^ 0xffffffff) && i_107_ <= i_101_) {
				anIntArray6370[i_98_] = i_99_;
				return true;
			}
			if (((i_107_ ^ 0xffffffff) >= -1 || (i_107_ ^ 0xffffffff) < -8193) && i_107_ > -8192) {
				anIntArray6370[i_98_] = Class202.and(16383, i_106_ - i_101_);
			} else {
				anIntArray6370[i_98_] = Class202.and(i_101_ + i_106_, 16383);
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.K(" + i + ',' + i_98_ + ',' + i_99_ + ')');
		}
	}

	int method3044(boolean bool) {
		try {
			RenderAnimDefinition class294 = method3039(1);
			if (class294.anInt2385 != -1) {
				return class294.anInt2385;
			}
			if (anInt6352 == -32768) {
				return 200;
			}
			if (bool != false) {
				return 122;
			}
			return -anInt6352;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.B(" + bool + ')');
		}
	}

	public final void method3046(int i, ModelRenderer class146) {
		try {
			if (i != 758) {
				anInt6384 = -63;
			}
			int i_109_ = roll.value;
			int i_110_ = pitch.value;
			if ((i_109_ ^ 0xffffffff) != -1 || (i_110_ ^ 0xffffffff) != -1) {
				int i_111_ = class146.fa() / 2;
				class146.H(0, -i_111_, 0);
				class146.VA(i_109_ & 0x3fff);
				class146.FA(i_110_ & 0x3fff);
				class146.H(0, i_111_, 0);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.Q(" + i + ',' + (class146 != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3047(int i, boolean bool, int i_112_) {
		try {
			RenderAnimDefinition class294 = method3039(1);
			if (bool || (class294.anInt2398 ^ 0xffffffff) != -1 || (anInt6414 ^ 0xffffffff) != -1) {
				if (i_112_ < 9) {
					anInt6391 = 107;
				}
				anInt6415 = i & 0x3fff;
				yaw.update(true, anInt6415);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.W(" + i + ',' + bool + ',' + i_112_ + ')');
		}
	}

	public final void setSize(byte i, int i_108_) {
		try {
			if (i > 66) {
				anInt6395 = i_108_;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "oi.E(" + i + ',' + i_108_ + ')');
		}
	}
}

/* Class98_Sub10_Sub16 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.EnumDefinitionParser;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class98_Sub10_Sub16 extends Class98_Sub10 {
	public static EnumDefinitionParser	enumsDefinitionList;
	public static Class85				REMOVE_GROUND_ITEM	= new Class85(6, 3);

	public static void method1051(boolean bool) {
		try {
			if (bool != false) {
				method1052(-69, -23, -110, null, -114, 47, 46);
			}
			REMOVE_GROUND_ITEM = null;
			enumsDefinitionList = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ir.D(" + bool + ')');
		}
	}

	public static final ModelRenderer method1052(int i, int i_0_, int i_1_, RSToolkit var_ha, int i_2_, int i_3_, int i_4_) {
		try {
			long l = i_2_;
			ModelRenderer class146 = (ModelRenderer) FriendLoginUpdate.aClass79_6170.get(-126, l);
			int i_5_ = 2055;
			if (class146 == null) {
				BaseModel class178 = Class98_Sub6.fromFile(0, -9252, Class76_Sub9.modelsJs5, i_2_);
				if (class178 == null) {
					return null;
				}
				if ((class178.version ^ 0xffffffff) > -14) {
					class178.scaleLog2(13746, 2);
				}
				class146 = var_ha.createModelRenderer(class178, i_5_, Class98_Sub10_Sub13.anInt5600, 64, 768);
				FriendLoginUpdate.aClass79_6170.put(l, class146, (byte) -80);
			}
			class146 = class146.method2341((byte) 2, i_5_, true);
			if (i_0_ != 0) {
				class146.rotateYaw(i_0_);
			}
			if (i_1_ != 0) {
				class146.FA(i_1_);
			}
			if ((i ^ 0xffffffff) != -1) {
				class146.VA(i);
			}
			if (i_3_ < 68) {
				method1052(80, 14, 63, null, -118, 89, 104);
			}
			if (i_4_ != 0) {
				class146.H(0, i_4_, 0);
			}
			return class146;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ir.B(" + i + ',' + i_0_ + ',' + i_1_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_2_ + ',' + i_3_ + ',' + i_4_ + ')');
		}
	}

	public Class98_Sub10_Sub16() {
		super(1, true);
	}

	@Override
	public final int[] method990(int i, int i_6_) {
		try {
			int[] is = this.aClass16_3863.method237((byte) 98, i_6_);
			if (i != 255) {
				REMOVE_GROUND_ITEM = null;
			}
			if (this.aClass16_3863.aBoolean198) {
				int[][] is_7_ = method994(i_6_, 24431, 0);
				int[] is_8_ = is_7_[0];
				int[] is_9_ = is_7_[1];
				int[] is_10_ = is_7_[2];
				for (int i_11_ = 0; (i_11_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_11_++) {
					is[i_11_] = (is_8_[i_11_] + is_9_[i_11_] - -is_10_[i_11_]) / 3;
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ir.G(" + i + ',' + i_6_ + ')');
		}
	}
}

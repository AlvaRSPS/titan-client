/* Class338 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Client;

public final class Sound {
	public static short[] clientPalette = new short[256];

	public static final void clearCache(byte i) {
		Js5Client.titleCache.clear(15);
	}

	public static final void method3778(byte i) {
		if ((RsBitsBuffers.anInt5789 ^ 0xffffffff) != 0) {
			Class119.method2176(-1, false, RsBitsBuffers.anInt5789, (byte) 89, -1);
			RsBitsBuffers.anInt5789 = -1;
		}
	}

	byte				aByte2840;
	Char				aClass246_Sub3_2834;
	Class37				aClass37_2835;
	Class98_Sub13		aClass98_Sub13_2838;
	Class98_Sub24_Sub1	aClass98_Sub24_Sub1_2839;
	Class98_Sub31_Sub5	aClass98_Sub31_Sub5_2836;
	int					anInt2837;
	int					delay;

	int					soundId;

	int					speed;

	int					timesPlayed;

	int					volume;

	public Sound(byte i, int soundId, int timesPlayed, int delay, int volume, int i_12_, int speed, Char animable) {
		this.delay = delay;
		this.speed = speed;
		this.soundId = soundId;
		this.volume = volume;
		anInt2837 = i_12_;
		aClass246_Sub3_2834 = animable;
		this.timesPlayed = timesPlayed;
		aByte2840 = i;
	}

	public final boolean method3782(int i) {
		return !(aByte2840 != 2 && (aByte2840 ^ 0xffffffff) != -4);
	}
}

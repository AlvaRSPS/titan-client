/* Interface17 - Decompiled by JODE
 */ package com; /*
					*/

public interface RenderTarget {
	void copyFromRenderer(int i, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, boolean bool, boolean bool_11_);

	void copyToRenderer(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, boolean colourMask, boolean depthMask);
}

/* Class114 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.BuildAreaPreferenceField;
import com.jagex.game.client.preferences.MaxScreenSizePreferenceField;
import com.jagex.game.toolkit.font.FontSpecifications;

public final class Class114 {
	public static int[] anIntArray958 = new int[3];

	public static final boolean method2147(char c, int i) {
		try {
			if (i <= 104) {
				return true;
			}
			return !(((c ^ 0xffffffff) > -49 || c > 57) && (c < 65 || c > 90) && (c < 97 || c > 122));
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hga.A(" + c + ',' + i + ')');
		}
	}

	public static final void method2148() {
		for (int i = 0; i < Class347.dynamicCount; i++) {
			Class246_Sub3_Sub4 class246_sub3_sub4 = Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i];
			Class99.method1687(class246_sub3_sub4, true);
			Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i] = null;
		}
		Class347.dynamicCount = 0;
	}

	public static final Class244 method2151(int i, boolean bool, RSToolkit var_ha, boolean bool_2_) {
		try {
			if ((i ^ 0xffffffff) == 0) {
				return null;
			}
			if (Class2.fontIds != null) {
				for (int i_3_ = 0; (i_3_ ^ 0xffffffff) > (Class2.fontIds.length ^ 0xffffffff); i_3_++) {
					if (Class2.fontIds[i_3_] == i) {
						return Class242.fonts[i_3_];
					}
				}
			}
			Class244 class244 = (Class244) Class232.aClass79_1740.get(-123, i);
			if (class244 != null) {
				if (bool && class244.aClass197_1858 == null) {
					FontSpecifications class197 = FontSpecifications.load(BuildAreaPreferenceField.glyphsStore, true, i);
					if (class197 == null) {
						return null;
					}
					class244.aClass197_1858 = class197;
				}
				return class244;
			}
			Image[] class324s = Image.loadImages(MaxScreenSizePreferenceField.fontmetricsStore, i);
			if (class324s == null) {
				return null;
			}
			FontSpecifications class197 = FontSpecifications.load(BuildAreaPreferenceField.glyphsStore, true, i);
			if (class197 == null) {
				return null;
			}
			if (!bool) {
				class244 = new Class244(var_ha.createFont(class197, class324s, true));
			} else {
				class244 = new Class244(var_ha.createFont(class197, class324s, true), class197);
			}
			Class232.aClass79_1740.put(i, class244, (byte) -80);
			return class244;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "hga.B(" + i + ',' + bool + ',' + (var_ha != null ? "{...}" : "null") + ',' + bool_2_ + ')');
		}
	}

	int		anInt956;

	String	aString957;
}

/* Class310 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;

public final class Class310 {
	public static boolean[][]	aBooleanArrayArray2653	= { { true, true, true, true, true, true, true, true, true, true, true, true, true }, { true, true, true, false, false, false, true, true, false, false, false, false, true }, { true, false, false, false, false, true, true, true, false, false,
			false, false, false }, { false, false, true, true, true, true, false, false, false, false, false, false, false }, { true, true, true, true, true, true, false, false, false, false, false, false, false }, { true, true, true, false, false, true, true, true, false, false, false, false,
					false }, { true, true, false, false, false, true, true, true, false, false, false, false, true }, { true, true, false, false, false, false, false, true, false, false, false, false, false }, { false, true, true, true, true, true, true, true, false, false, false, false, false }, {
							true, false, false, false, true, true, true, true, true, true, false, false, false }, { true, true, true, true, true, false, false, false, true, true, false, false, false }, { true, true, true, false, false, false, false, false, false, false, true, true, false },
			new boolean[13], { true, true, true, true, true, true, true, true, true, true, true, true, true }, new boolean[13] };
	public static int			anInt2652				= -1;

	public static void method3617(byte i) {
		try {
			if (i >= -11) {
				method3617((byte) -111);
			}
			aBooleanArrayArray2653 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "tc.A(" + i + ')');
		}
	}

	public static final void method3618(int i) {
		do {
			try {
				FileOnDisk class356 = null;
				try {
					SignLinkRequest class143 = GameShell.signLink.openPreferences("", true, i + 27480);
					if (i != -5964) {
						break;
					}
					while (class143.status == 0) {
						TimeTools.sleep(i + 5964, 1L);
					}
					if ((class143.status ^ 0xffffffff) == -2) {
						class356 = (FileOnDisk) class143.result;
						RSByteBuffer class98_sub22 = client.preferences.encode(true);
						class356.method3882(class98_sub22.payload, 4657, 0, class98_sub22.position);
					}
				} catch (Exception exception) {
					/* empty */
				}
				try {
					if (class356 == null) {
						break;
					}
					class356.close(true);
				} catch (Exception exception) {
					/* empty */
				}
				break;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "tc.B(" + i + ')');
			}
		} while (false);
	}
}

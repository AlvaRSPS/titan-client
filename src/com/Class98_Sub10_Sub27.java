/* Class98_Sub10_Sub27 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.preferences.MonoOrStereoPreferenceField;
import com.jagex.game.client.ui.layout.HorizontalAlignment;
import com.jagex.game.client.ui.loading.impl.elements.impl.RectangleLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;

public final class Class98_Sub10_Sub27 extends Class98_Sub10 {
	public static Class84	aClass84_5692;
	public static Class84	aClass84_5693;

	static {
		aClass84_5692 = aClass84_5693 = new Class84(false);
	}

	public static final int getTotalChatLines(byte i) {
		if (i != -4) {
			return 72;
		}
		return RectangleLoadingScreenElement.chatLinesCount;
	}

	public static void method1085(int i) {
		try {
			aClass84_5693 = null;
			if (i < 40) {
				method1085(-125);
			}
			aClass84_5692 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ow.B(" + i + ')');
		}
	}

	public Class98_Sub10_Sub27() {
		super(1, false);
	}

	private final void method1087(int i, int i_10_, int i_11_) {
		try {
			int i_12_ = MonoOrStereoPreferenceField.anIntArray3640[i_11_];
			int i_13_ = GameObjectDefinition.anIntArray3001[i_10_];
			float f = (float) Math.atan2(i_12_ + i, i_13_ + -2048);
			if (f >= -3.141592653589793 && f <= -2.356194490192345) {
				SpriteLoadingScreenElement.anInt3464 = i_10_;
				Class98_Sub10_Sub23.anInt5661 = i_11_;
			} else if (!(f <= -1.5707963267948966) || !(f >= -2.356194490192345)) {
				if (f <= -0.7853981633974483 && f >= -1.5707963267948966) {
					SpriteLoadingScreenElement.anInt3464 = i_11_;
					Class98_Sub10_Sub23.anInt5661 = -i_10_ + Class25.anInt268;
				} else if (!(f <= 0.0F) || !(f >= -0.7853981633974483)) {
					if (!(f >= 0.0F) || !(f <= 0.7853981633974483)) {
						if (!(f >= 0.7853981633974483) || !(f <= 1.5707963267948966)) {
							if (!(f >= 1.5707963267948966) || !(f <= 2.356194490192345)) {
								if (f >= 2.356194490192345 && f <= 3.141592653589793) {
									Class98_Sub10_Sub23.anInt5661 = -i_11_ + Class25.anInt268;
									SpriteLoadingScreenElement.anInt3464 = i_10_;
								}
							} else {
								SpriteLoadingScreenElement.anInt3464 = HorizontalAlignment.anInt492 - i_11_;
								Class98_Sub10_Sub23.anInt5661 = i_10_;
							}
						} else {
							Class98_Sub10_Sub23.anInt5661 = -i_10_ + Class25.anInt268;
							SpriteLoadingScreenElement.anInt3464 = HorizontalAlignment.anInt492 + -i_11_;
						}
					} else {
						Class98_Sub10_Sub23.anInt5661 = Class25.anInt268 + -i_11_;
						SpriteLoadingScreenElement.anInt3464 = -i_10_ + HorizontalAlignment.anInt492;
					}
				} else {
					Class98_Sub10_Sub23.anInt5661 = i_11_;
					SpriteLoadingScreenElement.anInt3464 = -i_10_ + HorizontalAlignment.anInt492;
				}
			} else {
				SpriteLoadingScreenElement.anInt3464 = i_11_;
				Class98_Sub10_Sub23.anInt5661 = i_10_;
			}
			SpriteLoadingScreenElement.anInt3464 &= SoftwareNativeHeap.anInt6075;
			Class98_Sub10_Sub23.anInt5661 &= Class329.anInt2761;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ow.D(" + i + ',' + i_10_ + ',' + i_11_ + ')');
		}
	}

	@Override
	public final int[] method990(int i, int i_6_) {
		try {
			if (i != 255) {
				return null;
			}
			int[] is = this.aClass16_3863.method237((byte) 98, i_6_);
			if (this.aClass16_3863.aBoolean198) {
				for (int i_7_ = 0; (i_7_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_7_++) {
					method1087(-2048, i_6_, i_7_);
					int[] is_8_ = method1000(SpriteLoadingScreenElement.anInt3464, 0, i ^ 0xff);
					is[i_7_] = is_8_[Class98_Sub10_Sub23.anInt5661];
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ow.G(" + i + ',' + i_6_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_9_) {
		do {
			try {
				if (i_9_ > -92) {
					method990(-69, 63);
				}
				if ((i ^ 0xffffffff) != -1) {
					break;
				}
				this.aBoolean3861 = (class98_sub22.readUnsignedByte((byte) 14) ^ 0xffffffff) == -2;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ow.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_9_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int[][] method997(int i, int i_0_) {
		try {
			if (i > -76) {
				return null;
			}
			int[][] is = this.aClass223_3859.method2828(i_0_, 0);
			if (this.aClass223_3859.aBoolean1683) {
				int[] is_1_ = is[0];
				int[] is_2_ = is[1];
				int[] is_3_ = is[2];
				for (int i_4_ = 0; i_4_ < Class25.anInt268; i_4_++) {
					method1087(-2048, i_0_, i_4_);
					int[][] is_5_ = method994(SpriteLoadingScreenElement.anInt3464, 24431, 0);
					is_1_[i_4_] = is_5_[0][Class98_Sub10_Sub23.anInt5661];
					is_2_[i_4_] = is_5_[1][Class98_Sub10_Sub23.anInt5661];
					is_3_[i_4_] = is_5_[2][Class98_Sub10_Sub23.anInt5661];
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ow.C(" + i + ',' + i_0_ + ')');
		}
	}
}

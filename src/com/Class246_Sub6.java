/* Class246_Sub6 - Decompiled by JODE
 */ package com; /*
					*/

public final class Class246_Sub6 extends SceneGraphNode {
	public boolean	aBoolean5114	= false;
	public int		anInt5109;
	public int		anInt5110;
	public int		anInt5111;
	public int		anInt5112;
	public int		anInt5113;

	public Class246_Sub6() {
		/* empty */
	}

	public final boolean method3130(int i, int i_0_) {
		if (!aBoolean5114) {
			return false;
		}
		int i_1_ = anInt5110 - anInt5111;
		int i_2_ = anInt5112 - anInt5113;
		int i_3_ = i_1_ * i_1_ + i_2_ * i_2_;
		int i_4_ = i * i_1_ + i_0_ * i_2_ - (anInt5111 * i_1_ + anInt5113 * i_2_);
		if (i_4_ <= 0) {
			int i_5_ = anInt5111 - i;
			int i_6_ = anInt5113 - i_0_;
			return i_5_ * i_5_ + i_6_ * i_6_ < anInt5109 * anInt5109;
		}
		if (i_4_ > i_3_) {
			int i_7_ = anInt5110 - i;
			int i_8_ = anInt5112 - i_0_;
			return i_7_ * i_7_ + i_8_ * i_8_ < anInt5109 * anInt5109;
		}
		i_4_ = (i_4_ << 10) / i_3_;
		int i_9_ = anInt5111 + (i_1_ * i_4_ >> 10) - i;
		int i_10_ = anInt5113 + (i_2_ * i_4_ >> 10) - i_0_;
		return i_9_ * i_9_ + i_10_ * i_10_ < anInt5109 * anInt5109;
	}
}

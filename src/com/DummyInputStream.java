
/* InputStream_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.InputStream;

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;

public final class DummyInputStream extends InputStream {
	public static IncomingOpcode	aClass58_28					= new IncomingOpcode(22, 6);
	public static int[][][]			animatedBackgroundCoords	= new int[2][][];

	public static final boolean method121(Char class246_sub3, boolean bool, byte[][][] is, int i, byte i_0_) {
		if (!GameObjectDefinitionParser.aBoolean2526) {
			return false;
		}
		int i_1_ = class246_sub3.boundExtentsX >> Class151_Sub8.tileScale;
		int i_2_ = i_1_;
		int i_3_ = class246_sub3.boundExtentsZ >> Class151_Sub8.tileScale;
		int i_4_ = i_3_;
		if (class246_sub3 instanceof Class246_Sub3_Sub4) {
			i_2_ = ((Class246_Sub3_Sub4) class246_sub3).aShort6160;
			i_4_ = ((Class246_Sub3_Sub4) class246_sub3).aShort6159;
			i_1_ = ((Class246_Sub3_Sub4) class246_sub3).aShort6158;
			i_3_ = ((Class246_Sub3_Sub4) class246_sub3).aShort6157;
		}
		for (int i_5_ = i_1_; i_5_ <= i_2_; i_5_++) {
			for (int i_6_ = i_3_; i_6_ <= i_4_; i_6_++) {
				if (class246_sub3.collisionPlane < OpenGLTexture2DSource.anInt3103 && i_5_ >= EnumDefinition.anInt2561 && i_5_ < Class21_Sub2.anInt5388 && i_6_ >= OutgoingOpcode.anInt1318 && i_6_ < FloorOverlayDefinitionParser.anInt303) {
					if (is != null && class246_sub3.plane >= i && is[class246_sub3.plane][i_5_][i_6_] == i_0_ || !class246_sub3.method2991(false) || class246_sub3.method2977(Class98_Sub10_Sub30.activeToolkit, (byte) 77)) {
						if (!bool && i_5_ >= Class241.anInt1845 - 16 && i_5_ <= Class241.anInt1845 + 16 && i_6_ >= CharacterShadowsPreferenceField.anInt3714 - 16 && i_6_ <= CharacterShadowsPreferenceField.anInt3714 + 16) {
							if (Class375.aBoolean3170) {
								Class98_Sub43_Sub3.aClass245Array5922[Class252.anInt1923++].method2958((byte) 115, class246_sub3);
								Class252.anInt1923 %= Class18.anInt212;
							} else {
								class246_sub3.method2988(Class98_Sub10_Sub30.activeToolkit, -51);
							}
						}
					} else {
						return false;
					}
				}
			}
		}
		return true;
	}

	public static void method122(boolean bool) {
		if (bool != false) {
			method122(true);
		}
		aClass58_28 = null;
		animatedBackgroundCoords = null;
	}

	@Override
	public final int read() {
		TimeTools.sleep(0, 30000L);
		return -1;
	}
}

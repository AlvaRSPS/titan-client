
/* Class321 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Component;
import java.awt.Point;
import java.awt.Robot;
import java.awt.image.BufferedImage;

public final class SunCursor {
	private Component	comp;
	private Robot		robot;

	SunCursor() throws Exception {
		try {
			robot = new Robot();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final void moveMouse(int width, int height) {
		try {
			robot.mouseMove(width, height);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final void setCustomCursor(Component component, int[] pixel, int width, int height, Point hotSpot) {
		try {
			if (pixel != null) {
				BufferedImage bufferedImage = new BufferedImage(width, height, 2);
				bufferedImage.setRGB(0, 0, width, height, pixel, 0, width);
				component.setCursor(component.getToolkit().createCustomCursor(bufferedImage, hotSpot, null));
			} else {
				component.setCursor(null);
			}
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final void showCursor(Component component, boolean bool) {
		do {
			try {
				if (!bool) {
					if (component == null) {
						throw new NullPointerException();
					}
				} else {
					component = null;
				}
				if (component != comp) {
					if (null != comp) {
						comp.setCursor(null);
						comp = null;
					}
					if (component == null) {
						break;
					}
					component.setCursor(component.getToolkit().createCustomCursor(new BufferedImage(1, 1, 2), new Point(0, 0), null));
					comp = component;
				}
			} catch (RuntimeException runtimeException) {
				throw runtimeException;
			}
			break;
		} while (false);
	}
}

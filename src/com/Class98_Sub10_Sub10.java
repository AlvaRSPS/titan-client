/* Class98_Sub10_Sub10 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.preferences.AntialiasPreferenceField;

public final class Class98_Sub10_Sub10 extends Class98_Sub10 {
	public static int[]	anIntArray5589;
	public static int[]	archiveProgressWeight	= { 4, 4, 1, 2, 6, 4, 2, 44, 2, 2, 2, 2, 2, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1 };

	public static final void method1038(Mob class246_sub3_sub4_sub2, int i, int i_4_) {
		try {
			int i_5_ = -1;
			int i_6_ = 0;
			if ((Queue.timer ^ 0xffffffff) <= (class246_sub3_sub4_sub2.anInt6390 ^ 0xffffffff)) {
				if (class246_sub3_sub4_sub2.anInt6424 >= Queue.timer) {
					RtInterface.method3466((byte) 20, class246_sub3_sub4_sub2);
				} else {
					Class333.method3762((byte) 38, false, class246_sub3_sub4_sub2);
					i_6_ = BConfigDefinition.anInt3121;
					i_5_ = AntialiasPreferenceField.anInt3708;
				}
			} else {
				Class165.method2523(i_4_ + 24500, class246_sub3_sub4_sub2);
			}
			if (class246_sub3_sub4_sub2.boundExtentsX < 512 || class246_sub3_sub4_sub2.boundExtentsZ < 512 || class246_sub3_sub4_sub2.boundExtentsX >= -512 + 512 * Class165.mapWidth || (class246_sub3_sub4_sub2.boundExtentsZ ^ 0xffffffff) <= (512
					* Class98_Sub10_Sub7.mapLength - 512 ^ 0xffffffff)) {
				class246_sub3_sub4_sub2.anInt6379 = -1;
				class246_sub3_sub4_sub2.anIntArray6373 = null;
				class246_sub3_sub4_sub2.anInt6365 = -1;
				i_5_ = -1;
				i_6_ = 0;
				class246_sub3_sub4_sub2.anInt6424 = 0;
				class246_sub3_sub4_sub2.anInt6413 = -1;
				class246_sub3_sub4_sub2.anInt6390 = 0;
				class246_sub3_sub4_sub2.boundExtentsX = class246_sub3_sub4_sub2.pathX[0] * 512 - -(256 * class246_sub3_sub4_sub2.getSize(0));
				class246_sub3_sub4_sub2.boundExtentsZ = class246_sub3_sub4_sub2.pathZ[0] * 512 + 256 * class246_sub3_sub4_sub2.getSize(0);
				class246_sub3_sub4_sub2.method3031(0);
			}
			if (i_4_ != -12212) {
				anIntArray5589 = null;
			}
			if (class246_sub3_sub4_sub2 == Class87.localPlayer && ((class246_sub3_sub4_sub2.boundExtentsX ^ 0xffffffff) > -6145 || (class246_sub3_sub4_sub2.boundExtentsZ ^ 0xffffffff) > -6145 || class246_sub3_sub4_sub2.boundExtentsX >= (-12 + Class165.mapWidth) * 512
					|| (-12 + Class98_Sub10_Sub7.mapLength) * 512 <= class246_sub3_sub4_sub2.boundExtentsZ)) {
				class246_sub3_sub4_sub2.anIntArray6373 = null;
				class246_sub3_sub4_sub2.anInt6424 = 0;
				i_5_ = -1;
				class246_sub3_sub4_sub2.anInt6413 = -1;
				class246_sub3_sub4_sub2.anInt6379 = -1;
				class246_sub3_sub4_sub2.anInt6365 = -1;
				i_6_ = 0;
				class246_sub3_sub4_sub2.anInt6390 = 0;
				class246_sub3_sub4_sub2.boundExtentsX = 512 * class246_sub3_sub4_sub2.pathX[0] + 256 * class246_sub3_sub4_sub2.getSize(i_4_ ^ ~0x2fb3);
				class246_sub3_sub4_sub2.boundExtentsZ = 512 * class246_sub3_sub4_sub2.pathZ[0] + 256 * class246_sub3_sub4_sub2.getSize(0);
				class246_sub3_sub4_sub2.method3031(0);
			}
			int i_7_ = Class98_Sub10_Sub13.method1041(class246_sub3_sub4_sub2, 0);
			Class108.method1729(i_4_ + 12114, class246_sub3_sub4_sub2);
			Class284_Sub1_Sub2.method3370(i_6_, i_4_ + 18356, class246_sub3_sub4_sub2, i_5_, i_7_);
			LoadingScreenSequence.method3334((byte) 37, i_5_, class246_sub3_sub4_sub2);
			Class340.method3801(class246_sub3_sub4_sub2, -28111);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ev.D(" + (class246_sub3_sub4_sub2 != null ? "{...}" : "null") + ',' + i + ',' + i_4_ + ')');
		}
	}

	public static void method1039(int i) {
		try {
			anIntArray5589 = null;
			client.ssKey = null;
			archiveProgressWeight = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ev.B(" + i + ')');
		}
	}

	private int	anInt5588;

	private int	anInt5591	= 1024;

	private int	anInt5592;

	public Class98_Sub10_Sub10() {
		super(1, false);
		anInt5588 = 2048;
		anInt5592 = 3072;
	}

	@Override
	public final void method1001(byte i) {
		do {
			try {
				anInt5588 = -anInt5591 + anInt5592;
				if (i == 66) {
					break;
				}
				method990(-42, 80);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ev.I(" + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int[] method990(int i, int i_0_) {
		try {
			if (i != 255) {
				method1001((byte) -46);
			}
			int[] is = this.aClass16_3863.method237((byte) 98, i_0_);
			if (this.aClass16_3863.aBoolean198) {
				int[] is_1_ = method1000(i_0_, 0, 0);
				for (int i_2_ = 0; i_2_ < Class25.anInt268; i_2_++) {
					is[i_2_] = anInt5591 + (is_1_[i_2_] * anInt5588 >> 806237676);
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ev.G(" + i + ',' + i_0_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_8_) {
		while_79_: do {
			try {
				if (i_8_ > -92) {
					method997(64, -121);
				}
				int i_9_ = i;
				do {
					if (i_9_ != 0) {
						if (i_9_ != 1) {
							if ((i_9_ ^ 0xffffffff) == -3) {
								break;
							}
							break while_79_;
						}
					} else {
						anInt5591 = class98_sub22.readShort((byte) 127);
						break while_79_;
					}
					anInt5592 = class98_sub22.readShort((byte) 127);
					break while_79_;
				} while (false);
				this.aBoolean3861 = class98_sub22.readUnsignedByte((byte) 40) == 1;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ev.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_8_ + ')');
			}
		} while (false);
	}

	@Override
	public final int[][] method997(int i, int i_11_) {
		try {
			int[][] is = this.aClass223_3859.method2828(i_11_, 0);
			if (this.aClass223_3859.aBoolean1683) {
				int[][] is_12_ = method994(i_11_, 24431, 0);
				int[] is_13_ = is_12_[0];
				int[] is_14_ = is_12_[1];
				int[] is_15_ = is_12_[2];
				int[] is_16_ = is[0];
				int[] is_17_ = is[1];
				int[] is_18_ = is[2];
				for (int i_19_ = 0; (i_19_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_19_++) {
					is_16_[i_19_] = anInt5591 - -(is_13_[i_19_] * anInt5588 >> 1994769356);
					is_17_[i_19_] = (anInt5588 * is_14_[i_19_] >> -1205225780) + anInt5591;
					is_18_[i_19_] = (is_15_[i_19_] * anInt5588 >> 859422188) + anInt5591;
				}
			}
			if (i >= -76) {
				method1038(null, 10, -90);
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ev.C(" + i + ',' + i_11_ + ')');
		}
	}
}

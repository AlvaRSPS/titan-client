/* Class22 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.game.client.archive.Js5Exception;

public final class Class22 {
	public static int	anInt216				= 0;
	public static int	anInt217;
	public static int	anInt219;
	public static int	CONTENT_TYPE_LOGIN_BG	= 1337;

	public static final Class98_Sub46_Sub4 method280(byte[] is, int i) {
		try {
			Class98_Sub46_Sub4 class98_sub46_sub4 = new Class98_Sub46_Sub4();
			RSByteBuffer class98_sub22 = new RSByteBuffer(is);
			class98_sub22.position = -2 + class98_sub22.payload.length;
			int i_0_ = class98_sub22.readShort((byte) 127);
			int i_1_ = -12 + -i_0_ + class98_sub22.payload.length + -2;
			class98_sub22.position = i_1_;
			int i_2_ = class98_sub22.readInt(-2);
			class98_sub46_sub4.anInt5958 = class98_sub22.readShort((byte) 127);
			class98_sub46_sub4.anInt5964 = class98_sub22.readShort((byte) 127);
			class98_sub46_sub4.anInt5966 = class98_sub22.readShort((byte) 127);
			class98_sub46_sub4.anInt5965 = class98_sub22.readShort((byte) 127);
			int i_3_ = class98_sub22.readUnsignedByte((byte) 39);
			if ((i_3_ ^ 0xffffffff) < -1) {
				class98_sub46_sub4.aClass377Array5956 = new HashTable[i_3_];
				for (int i_4_ = 0; i_3_ > i_4_; i_4_++) {
					int i_5_ = class98_sub22.readShort((byte) 127);
					HashTable class377 = new HashTable(Class48.findNextGreaterPwr2(423660257, i_5_));
					class98_sub46_sub4.aClass377Array5956[i_4_] = class377;
					while (i_5_-- > 0) {
						int i_6_ = class98_sub22.readInt(-2);
						int i_7_ = class98_sub22.readInt(-2);
						class377.put(new NodeInteger(i_7_), i_6_, -1);
					}
				}
			}
			class98_sub22.position = 0;
			class98_sub46_sub4.aString5968 = class98_sub22.method1222(-1);
			class98_sub46_sub4.aStringArray5959 = new String[i_2_];
			class98_sub46_sub4.anIntArray5963 = new int[i_2_];
			class98_sub46_sub4.anIntArray5967 = new int[i_2_];
			int i_8_ = i;
			while ((i_1_ ^ 0xffffffff) < (class98_sub22.position ^ 0xffffffff)) {
				int i_9_ = class98_sub22.readShort((byte) 127);
				if ((i_9_ ^ 0xffffffff) == -4) {
					class98_sub46_sub4.aStringArray5959[i_8_] = class98_sub22.readString((byte) 84).intern();
				} else if ((i_9_ ^ 0xffffffff) > -101 && i_9_ != 21 && (i_9_ ^ 0xffffffff) != -39 && i_9_ != 39) {
					class98_sub46_sub4.anIntArray5967[i_8_] = class98_sub22.readInt(i + -2);
				} else {
					class98_sub46_sub4.anIntArray5967[i_8_] = class98_sub22.readUnsignedByte((byte) 111);
				}
				class98_sub46_sub4.anIntArray5963[i_8_++] = i_9_;
			}
			return class98_sub46_sub4;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bk.A(" + (is != null ? "{...}" : "null") + ',' + i + ')');
		}
	}
}

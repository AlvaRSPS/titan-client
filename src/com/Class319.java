/* Class319 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.cache.AdvancedMemoryCache;

public final class Class319 {
	public static boolean		aBoolean2700			= false;
	public static boolean[][]	aBooleanArrayArray2702;
	public static boolean		groundBlendingEnabled	= false;
	public static int			hintHeadiconsId;
	public static int			mouseScrollDelta		= 0;
	public static int[]			playerIndices			= new int[2048];

	public static final boolean loadedNativeLibs(boolean bool) {
		if (!AdvancedMemoryCache.loadNative("jaclib", (byte) -36)) {
			return false;
		}

		return AdvancedMemoryCache.loadNative("hw3d", (byte) -36);
	}

	private AdvancedMemoryCache	aClass79_2703	= new AdvancedMemoryCache(256);

	private TextureMetricsList	aD2701;

	private NativeToolkit		aHa_Sub3_2704;

	Class319(NativeToolkit var_ha_Sub3, TextureMetricsList var_d) {
		aHa_Sub3_2704 = var_ha_Sub3;
		aD2701 = var_d;
	}

	public final void method3659(int i) {
		aClass79_2703.clear(i + 80);
	}

	public final Interface4_Impl2 method3661(int i, int i_0_) {
		Object object = aClass79_2703.get(i + -124, i_0_);
		if (object != null) {
			return (Interface4_Impl2) object;
		}
		if (!aD2701.isCached(-5, i_0_)) {
			return null;
		}
		if (i != 0) {
			return null;
		}
		TextureMetrics class238 = aD2701.getInfo(i_0_, i + -28755);
		int i_1_ = !class238.aBoolean1822 ? aHa_Sub3_2704.anInt4607 : 64;
		Interface4_Impl2 interface4_impl2;
		if (class238.aBoolean1817 && aHa_Sub3_2704.canEnableBloom()) {
			float[] fs = aD2701.getPixelsFloat((byte) -117, false, i_0_, i_1_, 0.7F, i_1_);
			interface4_impl2 = aHa_Sub3_2704.method2066(Class62.aClass164_486, (class238.aByte1832 ^ 0xffffffff) != -1, fs, false, i_1_, i_1_);
		} else {
			int[] is;
			if ((class238.anInt1818 ^ 0xffffffff) == -3 || !Class98_Sub10_Sub7.method1023(i ^ 0x1, class238.aByte1820)) {
				is = aD2701.getPixelsArgb(i + 115, i_1_, i_0_, 0.7F, false, i_1_);
			} else {
				is = aD2701.getPixelsRgb(i_0_, (byte) -116, i_1_, 0.7F, true, i_1_);
			}
			interface4_impl2 = aHa_Sub3_2704.method2012(i_1_, i_1_, (byte) 31, is, class238.aByte1832 != 0);
		}
		interface4_impl2.method46(class238.aBoolean1826, class238.aBoolean1819, -97);
		aClass79_2703.put(i_0_, interface4_impl2, (byte) -80);
		return interface4_impl2;
	}

	public final void method3662(int i) {
		aClass79_2703.makeSoftReferences((byte) 62, 5);
		if (i >= -112) {
			return;
		}
	}
}

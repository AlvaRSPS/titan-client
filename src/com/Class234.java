/* Class234 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;

public final class Class234 {
	public static boolean			aBoolean1750	= false;
	public static OutgoingOpcode	aClass171_1747	= new OutgoingOpcode(15, 8);
	public static float				aFloat1749;
	public static Js5				mapsJs5;
	public static Js5				npcJs5;

	public static void method2885(byte i) {
		try {
			npcJs5 = null;
			mapsJs5 = null;
			aClass171_1747 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ot.C(" + i + ')');
		}
	}

	public static final int method2886(int i, int i_1_) {
		try {
			if (i_1_ > -101) {
				return -25;
			}
			return i >>> 10;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ot.B(" + i + ',' + i_1_ + ')');
		}
	}

	public static final boolean method2887(int i, byte i_2_, int i_3_) {
		try {
			if (i_2_ != 46) {
				method2886(36, 120);
			}
			return (i_3_ & 0xc580) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ot.A(" + i + ',' + i_2_ + ',' + i_3_ + ')');
		}
	}
}

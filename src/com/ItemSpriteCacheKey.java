/* Class73 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;

public final class ItemSpriteCacheKey implements CacheKey {
	public static IncomingOpcode SET_PLAYER_OPTION = new IncomingOpcode(120, -1);

	public static final boolean method719(int i, int i_0_, int i_1_, int i_2_) {
		Js5Exception.aClass111_3203.method2103(i_0_, i_1_, i, Class114.anIntArray958);
		int i_3_ = Class114.anIntArray958[2];
		if ((i_3_ ^ 0xffffffff) > -51) {
			return false;
		}
		Class114.anIntArray958[1] = Class331.anInt2800 * Class114.anIntArray958[1] / i_3_ + Class98_Sub10_Sub23.anInt5659;
		Class114.anIntArray958[2] = i_3_;
		Class114.anIntArray958[i_2_] = Class38.anInt358 * Class114.anIntArray958[0] / i_3_ + Class2.anInt69;
		return true;
	}

	public static final int method721(String string, int i, char c) {
		int i_6_ = 0;
		int i_7_ = string.length();
		for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
			if ((c ^ 0xffffffff) == (string.charAt(i_8_) ^ 0xffffffff)) {
				i_6_++;
			}
		}
		return i_6_;
	}

	public boolean	appearence;
	public int		e;
	public int		itemId;
	public int		outlineColour;

	public int		stackMode;
	public int		stackSize;

	public int		toolkitId;

	@Override
	public final boolean equals(int i, CacheKey cacheKey) {
		if (!(cacheKey instanceof ItemSpriteCacheKey)) {
			return false;
		}
		ItemSpriteCacheKey other = (ItemSpriteCacheKey) cacheKey;
		if ((other.toolkitId ^ 0xffffffff) != (toolkitId ^ 0xffffffff)) {
			return false;
		}
		if ((itemId ^ 0xffffffff) != (other.itemId ^ 0xffffffff)) {
			return false;
		}
		if ((stackSize ^ 0xffffffff) != (other.stackSize ^ 0xffffffff)) {
			return false;
		}
		if (e != other.e) {
			return false;
		}
		if (other.outlineColour != outlineColour) {
			return false;
		}
		if (stackMode != other.stackMode) {
			return false;
		}
		return !appearence != other.appearence;
	}

	@Override
	public final long getHash(boolean bool) {
		long[] crc = WhirlpoolGenerator.CRC_64;
		long hash = -1L;
		hash = hash >>> 104055176 ^ crc[(int) (0xffL & (toolkitId ^ hash))];
		hash = hash >>> -1169160440 ^ crc[(int) ((hash ^ itemId >> 333698888) & 0xffL)];
		hash = crc[(int) ((itemId ^ hash) & 0xffL)] ^ hash >>> 2082866632;
		hash = hash >>> 785508872 ^ crc[(int) (0xffL & (hash ^ stackSize >> 1900552600))];
		hash = crc[(int) ((hash ^ stackSize >> -944629936) & 0xffL)] ^ hash >>> -784712120;
		hash = hash >>> -982760568 ^ crc[(int) (0xffL & (stackSize >> -780870712 ^ hash))];
		hash = crc[(int) ((hash ^ stackSize) & 0xffL)] ^ hash >>> -723616056;
		hash = hash >>> 546854024 ^ crc[(int) (0xffL & (e ^ hash))];
		hash = crc[(int) ((outlineColour >> 19611320 ^ hash) & 0xffL)] ^ hash >>> -1652365816;
		hash = crc[(int) (0xffL & (outlineColour >> -1593786384 ^ hash))] ^ hash >>> 172527176;
		hash = hash >>> -2082785080 ^ crc[(int) (0xffL & (hash ^ outlineColour >> -970562392))];
		hash = hash >>> 298776520 ^ crc[(int) ((outlineColour ^ hash) & 0xffL)];
		hash = crc[(int) ((hash ^ stackMode) & 0xffL)] ^ hash >>> -1877289208;
		hash = hash >>> 1282797832 ^ crc[(int) (0xffL & ((!appearence ? 0 : 1) ^ hash))];
		return hash;
	}
}

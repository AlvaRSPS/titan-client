/* Class98_Sub10_Sub11 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.LinkedList;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.toolkit.heap.SoftwareNativeHeap;

public final class Class98_Sub10_Sub11 extends Class98_Sub10 {
	public static LinkedList	animatedObjects	= new LinkedList();
	public static short[][]		aShortArrayArray5597;

	public static void method1040(int i) {
		try {
			aShortArrayArray5597 = null;
			animatedObjects = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fia.B(" + i + ')');
		}
	}

	private boolean	aBoolean5594	= true;

	private boolean	aBoolean5595	= true;

	public Class98_Sub10_Sub11() {
		super(1, false);
	}

	@Override
	public final int[] method990(int i, int i_13_) {
		try {
			if (i != 255) {
				aBoolean5594 = true;
			}
			int[] is = this.aClass16_3863.method237((byte) 98, i_13_);
			if (this.aClass16_3863.aBoolean198) {
				int[] is_14_ = method1000(!aBoolean5595 ? i_13_ : SoftwareNativeHeap.anInt6075 - i_13_, 0, i ^ 0xff);
				if (aBoolean5594) {
					for (int i_15_ = 0; (i_15_ ^ 0xffffffff) > (Class25.anInt268 ^ 0xffffffff); i_15_++) {
						is[i_15_] = is_14_[-i_15_ + Class329.anInt2761];
					}
				} else {
					ArrayUtils.arrayCopy(is_14_, 0, is, 0, Class25.anInt268);
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fia.G(" + i + ',' + i_13_ + ')');
		}
	}

	@Override
	public final void method991(int i, RSByteBuffer class98_sub22, byte i_11_) {
		do {
			try {
				int i_12_ = i;
				while_80_: do {
					do {
						if (i_12_ != 0) {
							if ((i_12_ ^ 0xffffffff) != -2) {
								if ((i_12_ ^ 0xffffffff) == -3) {
									break;
								}
								break while_80_;
							}
						} else {
							aBoolean5594 = class98_sub22.readUnsignedByte((byte) 43) == 1;
							break while_80_;
						}
						aBoolean5595 = class98_sub22.readUnsignedByte((byte) -116) == 1;
						break while_80_;
					} while (false);
					this.aBoolean3861 = class98_sub22.readUnsignedByte((byte) -107) == 1;
				} while (false);
				if (i_11_ <= -92) {
					break;
				}
				method991(7, null, (byte) -105);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fia.A(" + i + ',' + (class98_sub22 != null ? "{...}" : "null") + ',' + i_11_ + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int[][] method997(int i, int i_1_) {
		try {
			int[][] is = this.aClass223_3859.method2828(i_1_, 0);
			if (i > -76) {
				return null;
			}
			if (this.aClass223_3859.aBoolean1683) {
				int[][] is_2_ = method994(!aBoolean5595 ? i_1_ : SoftwareNativeHeap.anInt6075 - i_1_, 24431, 0);
				int[] is_3_ = is_2_[0];
				int[] is_4_ = is_2_[1];
				int[] is_5_ = is_2_[2];
				int[] is_6_ = is[0];
				int[] is_7_ = is[1];
				int[] is_8_ = is[2];
				if (!aBoolean5594) {
					for (int i_9_ = 0; i_9_ < Class25.anInt268; i_9_++) {
						is_6_[i_9_] = is_3_[i_9_];
						is_7_[i_9_] = is_4_[i_9_];
						is_8_[i_9_] = is_5_[i_9_];
					}
				} else {
					for (int i_10_ = 0; (Class25.anInt268 ^ 0xffffffff) < (i_10_ ^ 0xffffffff); i_10_++) {
						is_6_[i_10_] = is_3_[Class329.anInt2761 - i_10_];
						is_7_[i_10_] = is_4_[-i_10_ + Class329.anInt2761];
						is_8_[i_10_] = is_5_[-i_10_ + Class329.anInt2761];
					}
				}
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "fia.C(" + i + ',' + i_1_ + ')');
		}
	}
}

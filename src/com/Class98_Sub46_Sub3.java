/* Class98_Sub46_Sub3 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.ui.loading.impl.elements.impl.SpriteLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.newsfeed.NewsFetcher;
import com.jagex.game.toolkit.font.Font;

public final class Class98_Sub46_Sub3 extends Cacheable {
	public static ChatLine[] buffer = new ChatLine[100];

	public static final void method1541(int i) {
		do {
			try {
				int i_0_ = 256 + 512 * NewsFetcher.anInt3098;
				int i_1_ = Class98_Sub10_Sub21.anInt5643 * 512 + 256;
				int i_2_ = StrongReferenceMCNode.getHeight(Font.localPlane, i_1_, i_0_, 24111) - Class308.anInt2580;
				do {
					if ((GZipDecompressor.anInt1967 ^ 0xffffffff) <= -101) {
						SpriteLoadingScreenElement.yCamPosTile = 256 + 512 * Class98_Sub10_Sub21.anInt5643;
						Class98_Sub46_Sub10.xCamPosTile = NewsFetcher.anInt3098 * 512 + 256;
						AdvancedMemoryCache.anInt601 = StrongReferenceMCNode.getHeight(Font.localPlane, SpriteLoadingScreenElement.yCamPosTile, Class98_Sub46_Sub10.xCamPosTile, 24111) - Class308.anInt2580;
						if (!GameShell.cleanedStatics) {
							break;
						}
					}
					if (Class98_Sub46_Sub10.xCamPosTile < i_0_) {
						Class98_Sub46_Sub10.xCamPosTile += StrongReferenceMCNode.anInt6300 - -((-Class98_Sub46_Sub10.xCamPosTile + i_0_) * GZipDecompressor.anInt1967 / 1000);
						if ((Class98_Sub46_Sub10.xCamPosTile ^ 0xffffffff) < (i_0_ ^ 0xffffffff)) {
							Class98_Sub46_Sub10.xCamPosTile = i_0_;
						}
					}
					if (i_0_ < Class98_Sub46_Sub10.xCamPosTile) {
						Class98_Sub46_Sub10.xCamPosTile -= StrongReferenceMCNode.anInt6300 + (-i_0_ + Class98_Sub46_Sub10.xCamPosTile) * GZipDecompressor.anInt1967 / 1000;
						if (Class98_Sub46_Sub10.xCamPosTile < i_0_) {
							Class98_Sub46_Sub10.xCamPosTile = i_0_;
						}
					}
					if ((AdvancedMemoryCache.anInt601 ^ 0xffffffff) > (i_2_ ^ 0xffffffff)) {
						AdvancedMemoryCache.anInt601 += (-AdvancedMemoryCache.anInt601 + i_2_) * GZipDecompressor.anInt1967 / 1000 + StrongReferenceMCNode.anInt6300;
						if (AdvancedMemoryCache.anInt601 > i_2_) {
							AdvancedMemoryCache.anInt601 = i_2_;
						}
					}
					if (i_1_ > SpriteLoadingScreenElement.yCamPosTile) {
						SpriteLoadingScreenElement.yCamPosTile += GZipDecompressor.anInt1967 * (i_1_ + -SpriteLoadingScreenElement.yCamPosTile) / 1000 + StrongReferenceMCNode.anInt6300;
						if ((SpriteLoadingScreenElement.yCamPosTile ^ 0xffffffff) < (i_1_ ^ 0xffffffff)) {
							SpriteLoadingScreenElement.yCamPosTile = i_1_;
						}
					}
					if ((AdvancedMemoryCache.anInt601 ^ 0xffffffff) < (i_2_ ^ 0xffffffff)) {
						AdvancedMemoryCache.anInt601 -= StrongReferenceMCNode.anInt6300 - -((AdvancedMemoryCache.anInt601 - i_2_) * GZipDecompressor.anInt1967 / 1000);
						if (i_2_ > AdvancedMemoryCache.anInt601) {
							AdvancedMemoryCache.anInt601 = i_2_;
						}
					}
					if ((i_1_ ^ 0xffffffff) > (SpriteLoadingScreenElement.yCamPosTile ^ 0xffffffff)) {
						SpriteLoadingScreenElement.yCamPosTile -= StrongReferenceMCNode.anInt6300 + GZipDecompressor.anInt1967 * (-i_1_ + SpriteLoadingScreenElement.yCamPosTile) / 1000;
						if ((i_1_ ^ 0xffffffff) < (SpriteLoadingScreenElement.yCamPosTile ^ 0xffffffff)) {
							SpriteLoadingScreenElement.yCamPosTile = i_1_;
						}
					}
				} while (false);
				i_1_ = 512 * NodeString.anInt3915 + 256;
				i_0_ = 256 + 512 * Exception_Sub1.anInt44;
				i_2_ = StrongReferenceMCNode.getHeight(Font.localPlane, i_1_, i_0_, 24111) - Class303.anInt2530;
				int i_3_ = -Class98_Sub46_Sub10.xCamPosTile + i_0_;
				int i_4_ = -AdvancedMemoryCache.anInt601 + i_2_;
				int i_5_ = -SpriteLoadingScreenElement.yCamPosTile + i_1_;
				int i_6_ = (int) Math.sqrt(i_3_ * i_3_ - -(i_5_ * i_5_));
				int i_7_ = 0x3fff & (int) (Math.atan2(i_4_, i_6_) * 2607.5945876176133);
				int i_8_ = (int) (-2607.5945876176133 * Math.atan2(i_3_, i_5_)) & 0x3fff;
				if (i_7_ < 1024) {
					i_7_ = 1024;
				}
				if ((i_7_ ^ 0xffffffff) < -3073) {
					i_7_ = 3072;
				}
				if (Mob.anInt6357 < i_7_) {
					Mob.anInt6357 += (i_7_ - Mob.anInt6357 >> -1117924989) * Class98_Sub4.anInt3828 / 1000 + Class98_Sub41.anInt4202 << -1739945821;
					if (Mob.anInt6357 > i_7_) {
						Mob.anInt6357 = i_7_;
					}
				}
				if ((i_7_ ^ 0xffffffff) > (Mob.anInt6357 ^ 0xffffffff)) {
					Mob.anInt6357 -= Class98_Sub41.anInt4202 + Class98_Sub4.anInt3828 * (-i_7_ + Mob.anInt6357 >> 1278679235) / 1000 << -1425824541;
					if (i_7_ > Mob.anInt6357) {
						Mob.anInt6357 = i_7_;
					}
				}
				int i_9_ = i_8_ + -Class186.cameraX;
				if ((i_9_ ^ 0xffffffff) < -8193) {
					i_9_ -= 16384;
				}
				if ((i_9_ ^ 0xffffffff) > 8191) {
					i_9_ += 16384;
				}
				i_9_ >>= 3;
				if ((i_9_ ^ 0xffffffff) < -1) {
					Class186.cameraX += Class98_Sub4.anInt3828 * i_9_ / 1000 + Class98_Sub41.anInt4202 << 1507566883;
					Class186.cameraX &= 0x3fff;
				}
				if (i_9_ < 0) {
					Class186.cameraX -= Class98_Sub4.anInt3828 * -i_9_ / 1000 + Class98_Sub41.anInt4202 << 1406205123;
					Class186.cameraX &= 0x3fff;
				}
				int i_10_ = -Class186.cameraX + i_8_;
				if (i > 69) {
					if ((i_10_ ^ 0xffffffff) < -8193) {
						i_10_ -= 16384;
					}
					if (i_10_ < -8192) {
						i_10_ += 16384;
					}
					Class308.anInt2584 = 0;
					if (((i_10_ ^ 0xffffffff) <= -1 || i_9_ <= 0) && (i_10_ <= 0 || i_9_ >= 0)) {
						break;
					}
					Class186.cameraX = i_8_;
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "bv.A(" + i + ')');
			}
			break;
		} while (false);
	}

	Class246_Sub3_Sub4_Sub3 aClass246_Sub3_Sub4_Sub3_5954;

	Class98_Sub46_Sub3(Class246_Sub3_Sub4_Sub3 class246_sub3_sub4_sub3) {
		try {
			aClass246_Sub3_Sub4_Sub3_5954 = class246_sub3_sub4_sub3;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bv.<init>(" + (class246_sub3_sub4_sub3 != null ? "{...}" : "null") + ')');
		}
	}
}

/* Exception_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.preferences.ParticlesPreferenceField;

public final class Exception_Sub1 extends Exception {
	public static OutgoingOpcode		aClass171_46	= new OutgoingOpcode(45, 15);
	public static IncomingOpcode		aClass58_43		= new IncomingOpcode(117, 0);
	public static int					anInt44;
	public static PlatformInformation	platformInformation;

	public static final void method134(byte i) {
		if (!JavaNetworkWriter.aBoolean2575) {
			ParticlesPreferenceField.aBoolean3656 = true;
			MapRegion.aFloat2545 += (-MapRegion.aFloat2545 + 12.0F) / 2.0F;
			JavaNetworkWriter.aBoolean2575 = true;
		}
	}

	public Exception_Sub1() {
		/* empty */
	}
}

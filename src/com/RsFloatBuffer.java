
/* Class98_Sub22_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import jaclib.memory.Stream;

public final class RsFloatBuffer extends RSByteBuffer {
	public static OutgoingOpcode	aClass171_5792	= new OutgoingOpcode(71, 7);
	public static IncomingOpcode	aClass58_5793	= new IncomingOpcode(17, 8);
	public static float				aFloat5794		= 0.0F;

	public static final int method1262(int i, int i_0_) {
		if (i_0_ < 96) {
			return 0;
		}
		if (i_0_ < 128) {
			return 2;
		}
		return 3;
	}

	public RsFloatBuffer(int i) {
		super(i);
	}

	public final void method1265(byte i, float f) {
		int i_3_ = Stream.floatToRawIntBits(f);
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) i_3_;
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) (i_3_ >> 8);
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) (i_3_ >> 16);
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) (i_3_ >> 24);
	}

	public final void writeFloat(byte i, float f) {
		int i_2_ = Stream.floatToRawIntBits(f);
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) (i_2_ >> 24);
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) (i_2_ >> 16);
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) (i_2_ >> 8);
		((RSByteBuffer) this).payload[((RSByteBuffer) this).position++] = (byte) i_2_;
	}
}

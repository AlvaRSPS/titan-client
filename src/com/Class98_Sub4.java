/* Class98_Sub4 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.GraphicsLevelPreferenceField;

public final class Class98_Sub4 extends Node {
	public static int	anInt3826;
	public static int	anInt3828;

	public static final void method953(int i, int i_0_, boolean bool) {
		try {
			int i_1_ = Class42_Sub1.p13FullMetrics.method2674(TextResources.CHOOSE_OPTION.getText(client.gameLanguage, (byte) 25), 99);
			int i_2_;
			if (Class248.aBoolean1896) {
				for (ActionGroup class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getFirst(-1); class98_sub46_sub9 != null; class98_sub46_sub9 = (ActionGroup) RtInterfaceAttachment.actionGroups.getNext(0)) {
					int i_3_;
					if ((class98_sub46_sub9.actionCount ^ 0xffffffff) != -2) {
						i_3_ = Class98_Sub10_Sub25.method1079(class98_sub46_sub9, 21);
					} else {
						i_3_ = LoginOpcode.method2824((byte) 81, (ActionQueueEntry) class98_sub46_sub9.actions.head.nextCacheable);
					}
					if ((i_3_ ^ 0xffffffff) < (i_1_ ^ 0xffffffff)) {
						i_1_ = i_3_;
					}
				}
				i_1_ += 8;
				i_2_ = 16 * GraphicsLevelPreferenceField.groupCount + 21;
				Class15.anInt172 = GraphicsLevelPreferenceField.groupCount * 16 + (Class98_Sub5_Sub3.aBoolean5539 ? 26 : 22);
			} else {
				for (ActionQueueEntry class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getFirst(32); class98_sub46_sub8 != null; class98_sub46_sub8 = (ActionQueueEntry) Class33.actionList.getNext(119)) {
					int i_4_ = LoginOpcode.method2824((byte) 75, class98_sub46_sub8);
					if ((i_1_ ^ 0xffffffff) > (i_4_ ^ 0xffffffff)) {
						i_1_ = i_4_;
					}
				}
				i_1_ += 8;
				Class15.anInt172 = Class359.actionCount * 16 + (Class98_Sub5_Sub3.aBoolean5539 ? 26 : 22);
				i_2_ = 21 + Class359.actionCount * 16;
			}
			int i_5_ = i_0_ + -(i_1_ / 2);
			if ((GameShell.frameWidth ^ 0xffffffff) > (i_5_ - -i_1_ ^ 0xffffffff)) {
				i_5_ = GameShell.frameWidth - i_1_;
			}
			if ((i_5_ ^ 0xffffffff) > -1) {
				i_5_ = 0;
			}
			int i_6_ = i;
			if (i_2_ + i_6_ > GameShell.frameHeight) {
				i_6_ = -i_2_ + GameShell.frameHeight;
			}
			Class38.anInt355 = i_5_;
			if ((i_6_ ^ 0xffffffff) > -1) {
				i_6_ = 0;
			}
			OpenGlArrayBufferPointer.anInt897 = i_6_;
			Player.menuOpen = bool;
			Class246_Sub3_Sub4_Sub4.width = i_1_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bfa.A(" + i + ',' + i_0_ + ',' + bool + ')');
		}
	}

	volatile int	anInt3827	= -1;

	volatile String	aString3829;

	Class98_Sub4(String string) {
		try {
			aString3829 = string;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "bfa.<init>(" + (string != null ? "{...}" : "null") + ')');
		}
	}
}

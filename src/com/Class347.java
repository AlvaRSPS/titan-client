
/* Class347 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.parser.CursorDefinitionParser;

import jaggl.OpenGL;

public final class Class347 {
	public static ActionQueueEntry	aClass98_Sub46_Sub8_2908;
	public static int[]				anIntArray2906;
	public static int				dynamicCount	= 0;

	public static void method3832(byte i) {
		try {
			aClass98_Sub46_Sub8_2908 = null;
			anIntArray2906 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vc.B(" + i + ')');
		}
	}

	public static final void method3833(int i, int i_1_) {
		try {
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i, -70, i_1_);
			class98_sub46_sub17.method1621(0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vc.A(" + i + ',' + i_1_ + ')');
		}
	}

	public static final int method3834(int i, char c, byte i_2_) {
		try {
			if (i_2_ > -69) {
				return -103;
			}
			int i_3_ = c << -1402494108;
			if (Character.isUpperCase(c) || Character.isTitleCase(c)) {
				c = Character.toLowerCase(c);
				i_3_ = 1 + (c << -1894868316);
			}
			return i_3_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vc.D(" + i + ',' + c + ',' + i_2_ + ')');
		}
	}

	public static final Class202 method3835(int i, OpenGlToolkit var_ha_Sub1, int i_4_, String string) {
		try {
			int i_5_ = OpenGL.glGenProgramARB();
			OpenGL.glBindProgramARB(i_4_, i_5_);
			OpenGL.glProgramStringARB(i_4_, 34933, string);
			OpenGL.glGetIntegerv(34379, CursorDefinitionParser.anIntArray123, 0);
			if (CursorDefinitionParser.anIntArray123[0] != -1) {
				OpenGL.glBindProgramARB(i_4_, 0);
				return null;
			}
			OpenGL.glBindProgramARB(i_4_, i);
			return new Class202(var_ha_Sub1, i_4_, i_5_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vc.C(" + i + ',' + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i_4_ + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}
}

/* Class246_Sub3_Sub1_Sub2 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Class246_Sub3_Sub1_Sub2 extends Class246_Sub3_Sub1 implements Interface19 {
	public static Class204	aClass204_6247;
	public static int		anInt6251		= -1;
	public static int[]		anIntArray6245	= new int[8];

	static {
		aClass204_6247 = new Class204();
	}

	public static void method3001(int i) {
		try {
			if (i != -22408) {
				method3001(77);
			}
			aClass204_6247 = null;
			anIntArray6245 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.L(" + i + ')');
		}
	}

	private boolean		aBoolean6246;
	private boolean		aBoolean6248	= false;

	private Class228	aClass228_6250;

	Class359			aClass359_6249;

	Class246_Sub3_Sub1_Sub2(RSToolkit var_ha, GameObjectDefinition class352, int i, int i_7_, int i_8_, int i_9_, int i_10_, boolean bool, int i_11_, int i_12_) {
		super(i_8_, i_9_, i_10_, i, i_7_, class352.anInt2945);
		try {
			aClass359_6249 = new Class359(var_ha, class352, 22, i_11_, i, i_7_, this, bool, i_12_);
			aBoolean6246 = class352.anInt2998 != 0 && !bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.<init>(" + (var_ha != null ? "{...}" : "null") + ',' + (class352 != null ? "{...}" : "null") + ',' + i + ',' + i_7_ + ',' + i_8_ + ',' + i_9_ + ',' + i_10_ + ',' + bool + ',' + i_11_ + ',' + i_12_ + ')');
		}
	}

	@Override
	public final Class228 method2974(byte i, RSToolkit var_ha) {
		try {
			if (i != -53) {
				aClass204_6247 = null;
			}
			return aClass228_6250;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.KA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	@Override
	public final Class246_Sub1 method2975(RSToolkit var_ha, int i) {
		try {
			ModelRenderer class146 = aClass359_6249.method3897(-1, false, 2048, var_ha, true);
			if (class146 == null) {
				return null;
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
			Class246_Sub1 class246_sub1 = Class94.method915(1, (byte) -47, aBoolean6246);
			int i_17_ = this.boundExtentsX >> 1484733673;
			if (i >= -12) {
				method2975(null, -86);
			}
			int i_18_ = this.boundExtentsZ >> 560688297;
			aClass359_6249.method3895(class146, i_17_, i_18_, class111, true, i_17_, var_ha, false, i_18_);
			if (!VarClientStringsDefinitionParser.aBoolean1839) {
				class146.method2325(class111, class246_sub1.aClass246_Sub6Array5067[0], 0);
			} else {
				class146.method2329(class111, class246_sub1.aClass246_Sub6Array5067[0], Class16.anInt197, 0);
			}
			if (aClass359_6249.aClass246_Sub5_3062 != null) {
				Class242 class242 = aClass359_6249.aClass246_Sub5_3062.method3116();
				if (VarClientStringsDefinitionParser.aBoolean1839) {
					var_ha.method1785(class242, Class16.anInt197);
				} else {
					var_ha.method1820(class242);
				}
			}
			aBoolean6248 = class146.F() || aClass359_6249.aClass246_Sub5_3062 != null;
			if (aClass228_6250 != null) {
				Class283.method3350(this.anInt5089, this.boundExtentsX, 18, this.boundExtentsZ, class146, aClass228_6250);
			} else {
				aClass228_6250 = Class48_Sub2_Sub1.method472(this.anInt5089, this.boundExtentsX, class146, this.boundExtentsZ, 4);
			}
			return class246_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.QA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final boolean method2976(int i, RSToolkit var_ha, byte i_0_, int i_1_) {
		try {
			ModelRenderer class146 = aClass359_6249.method3897(-1, false, 131072, var_ha, false);
			if (class146 == null) {
				return false;
			}
			if (i_0_ < 59) {
				method2990(-80);
			}
			Matrix class111 = var_ha.method1793();
			class111.method2100(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
			if (VarClientStringsDefinitionParser.aBoolean1839) {
				return class146.method2333(i, i_1_, class111, false, 0, Class16.anInt197);
			}
			return class146.method2339(i, i_1_, class111, false, 0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.TA(" + i + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	@Override
	public final boolean method2978(int i) {
		try {
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.H(" + i + ')');
		}
	}

	@Override
	public final void method2981(Char class246_sub3, byte i, boolean bool, int i_13_, RSToolkit var_ha, int i_14_, int i_15_) {
		try {
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.CA(" + (class246_sub3 != null ? "{...}" : "null") + ',' + i + ',' + bool + ',' + i_13_ + ',' + (var_ha != null ? "{...}" : "null") + ',' + i_14_ + ',' + i_15_ + ')');
		}
	}

	@Override
	public final boolean method2982(byte i) {
		try {
			if (i >= -70) {
				anIntArray6245 = null;
			}
			return false;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.HA(" + i + ')');
		}
	}

	@Override
	public final int method2985(boolean bool) {
		try {
			if (bool != false) {
				method66(82);
			}
			return aClass359_6249.method3903((byte) -123);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.O(" + bool + ')');
		}
	}

	@Override
	public final boolean method2987(int i) {
		try {
			if (i != 6540) {
				method3002(null, (byte) -110);
			}
			return aBoolean6248;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.I(" + i + ')');
		}
	}

	@Override
	public final void method2988(RSToolkit var_ha, int i) {
		do {
			try {
				ModelRenderer class146 = aClass359_6249.method3897(-1, true, 262144, var_ha, true);
				if (class146 == null) {
					break;
				}
				int i_4_ = this.boundExtentsX >> 118087081;
				int i_5_ = this.boundExtentsZ >> -1339126199;
				Matrix class111 = var_ha.method1793();
				class111.method2100(this.boundExtentsX, this.anInt5089, this.boundExtentsZ);
				aClass359_6249.method3895(class146, i_4_, i_5_, class111, false, i_4_, var_ha, false, i_5_);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ps.MA(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	@Override
	public final int method2990(int i) {
		try {
			if (i != 0) {
				anInt6251 = -108;
			}
			return aClass359_6249.method3899((byte) 125);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.J(" + i + ')');
		}
	}

	@Override
	public final void method2992(byte i) {
		try {
			if (i != -73) {
				anInt6251 = -80;
			}
			throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.DA(" + i + ')');
		}
	}

	public final void method3002(Class185 class185, byte i) {
		try {
			aClass359_6249.method3901(class185, -118);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.M(" + (class185 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final void method61(byte i) {
		try {
			if (i != -96) {
				aClass228_6250 = null;
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.Q(" + i + ')');
		}
	}

	@Override
	public final void method62(RSToolkit var_ha, int i) {
		try {
			if (i != 24447) {
				anIntArray6245 = null;
			}
			aClass359_6249.method3892(var_ha, 105);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.G(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	@Override
	public final int method63(byte i) {
		try {
			if (i != 20) {
				anInt6251 = -77;
			}
			return aClass359_6249.anInt3038;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.B(" + i + ')');
		}
	}

	@Override
	public final int method64(int i) {
		try {
			if (i != 30472) {
				method3001(55);
			}
			return aClass359_6249.anInt3052;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.C(" + i + ')');
		}
	}

	@Override
	public final boolean method65(boolean bool) {
		try {
			if (bool != true) {
				return true;
			}
			return aClass359_6249.method3898(21);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.A(" + bool + ')');
		}
	}

	@Override
	public final int method66(int i) {
		try {
			if (i != 4657) {
				method2976(108, null, (byte) 101, -1);
			}
			return aClass359_6249.anInt3059;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ps.N(" + i + ')');
		}
	}

	@Override
	public final void method67(int i, RSToolkit var_ha) {
		do {
			try {
				aClass359_6249.method3894((byte) -85, var_ha);
				if (i == -25163) {
					break;
				}
				method2981(null, (byte) -78, false, 22, null, 107, 28);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ps.E(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}
}


/* Class98_Sub22 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.Launcher;
import com.jagex.core.collections.Node;
import com.jagex.core.crypto.CRC32;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;

import java.math.BigInteger;

public class RSByteBuffer extends Node {

	public static IncomingOpcode	SEND_CHUNK_UPDATE;
	public static int				anInt3994		= -1;
	public static Js5				texturesJs5;
	public static BigInteger		MODULUS			= new BigInteger(
			"AAA40215470339B49D4C67C1F3F2654DD5C6EEFCB232ADE21AB39238634477E5314B0F7455266A119991B320B82C35CCE37703C942B11EB24CBC5F49CCAD501FF918455AD83B69842A6F94AC2A7424BAA292067174EFE9617A939D683FB185669D443DF2B57B6041F917ACFCF7B3A0A9EDC2F1F48147FD7357F6A165ABEBDB1F", 16);
	public static BigInteger		PUBLIC_EXPONENT	= new BigInteger("10001", 16);

	static {
		SEND_CHUNK_UPDATE = new IncomingOpcode(114, 3);
	}

	public static final void method1216(int i) {
		try {
			if (i != -17470) {
				method1216(-14);
			}
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(0, -42, 15);
			class98_sub46_sub17.method1621(0);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.HC(" + i + ')');
		}
	}

	public static final boolean method1241(boolean bool, int i, int i_78_) {
		try {
			if (bool != false) {
				return false;
			}
			return (0x100 & i_78_) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.L(" + bool + ',' + i + ',' + i_78_ + ')');
		}
	}

	public static void method1243(int i) {
		try {
			texturesJs5 = null;
			if (i <= 79) {
				anInt3994 = -43;
			}
			SEND_CHUNK_UPDATE = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.JC(" + i + ')');
		}
	}

	public byte[]	payload;
	public int		position;

	public RSByteBuffer(byte[] buffer) {
		position = 0;
		payload = buffer;
	}

	public RSByteBuffer(int size) {
		payload = PacketBufferManager.allocate(false, size);
		position = 0;
	}

	public final int addCrc(int i, byte i_11_) {
		int i_12_ = CRC32.calculate(position, payload, i, false);
		writeInt(1571862888, i_12_);
		return i_12_;
	}

	public final void getData(byte[] is, boolean bool, int i, int i_6_) {
		for (int i_7_ = i_6_; i_7_ < i_6_ + i; i_7_++) {
			is[i_7_] = payload[position++];
		}
	}

	public void injectByte(int i, int pos) {
		payload[pos] = (byte) i;
	}

	public void injectShort(int i, int pos) {
		payload[pos] = (byte) (i >> 8);
		payload[pos + 1] = (byte) i;

	}

	public final int method1183(int i) {
		position += 2;
		int i_2_ = (payload[-2 + position] & 0xff) + (payload[position - 1] << 1553290568 & 0xff00);
		if ((i_2_ ^ 0xffffffff) < -32768) {
			i_2_ -= 65536;
		}
		return i_2_;
	}

	public final byte method1187(byte i) {
		return (byte) -payload[position++];
	}

	public final long method1189(byte i) {
		long l = method1202((byte) -58) & 0xffffffffL;
		long l_5_ = 0xffffffffL & method1202((byte) -68);
		return l + (l_5_ << 1898790944);
	}

	public final int method1192(byte i) {
		position += 3;
		return (0xff & payload[-3 + position]) + ((0xff & payload[-2 + position]) << -1177345560) + (0xff0000 & payload[position - 1] << 587636880);
	}

	public final int method1198(int i) {
		position += 2; // writeLEShortA
		int i_14_ = (-128 + payload[position - 2] & 0xff) + (payload[position + -1] << -1508488440 & 0xff00);
		if ((i_14_ ^ 0xffffffff) < -32768) {
			i_14_ -= 65536;
		}
		return i_14_;
	}

	public final void method1199(int i, boolean bool) {
		if (bool != false) {
			readInt1(true);
		}
		if ((~0x7f & i) != 0) {
			if ((~0x3fff & i) != 0) {
				if ((~0x1fffff & i ^ 0xffffffff) != -1) {
					if ((~0xfffffff & i) != 0) {
						writeByte(i >>> -342844612 | 0x80, 94);
					}
					writeByte((i | 0x1001c695) >>> 651642005, -37);
				}
				writeByte((0x201d9a | i) >>> -33880754, 107);
			}
			writeByte(0x80 | i >>> -1419157497, -56);
		}
		writeByte(i & 0x7f, 56);
	}

	public final void method1200(byte i, int i_16_) {
		payload[position++] = (byte) (i_16_ >> -968007800);
		payload[position++] = (byte) i_16_;
		payload[position++] = (byte) (i_16_ >> -922569928);
		payload[position++] = (byte) (i_16_ >> -944199472);
	}

	public final void method1201(int i) {
		do {
			if (payload != null) {
				PacketBufferManager.free((byte) 75, payload);
			}
			payload = null;
			if (i == 0) {
				break;
			}
			method1216(-7);
			break;
		} while (false);
	}

	public final int method1202(byte i) {
		position += 4;
		if (i >= -45) {
			anInt3994 = 37;
		}
		return (payload[-3 + position] << -401730296 & 0xff00) + ((payload[position + -2] & 0xff) << 178467952) + ((0xff & payload[-1 + position]) << -688793032) + (0xff & payload[-4 + position]);
	}

	public final void method1207(byte i, int i_25_) {
		payload[position - i_25_ + -2] = (byte) (i_25_ >> 1804543080);
		if (i != 90) {
			readUnsignedByte((byte) -108);
		}
		payload[-1 + -i_25_ + position] = (byte) i_25_;
	}

	public final int method1208(int i) {
		int i_26_ = 0;
		if (i != 3893) {
			return 116;
		}
		int i_27_;
		for (i_27_ = readSmart(i + 1689618819); i_27_ == 32767; i_27_ = readSmart(1689622712)) {
			i_26_ += 32767;
		}
		i_26_ += i_27_;
		return i_26_;
	}

	public final boolean method1210(int i) {
		position -= 4;
		int i_29_ = CRC32.calculate(position, payload, 0, false);
		int i_30_ = readInt(-2);
		return i_29_ == i_30_;
	}

	public final void method1211(byte i, int i_31_) {
		if (i > 79) {
			payload[-i_31_ + position - 1] = (byte) i_31_;
		}
	}

	public final void method1213(int i, long l, int i_32_) {
		if (--i_32_ < 0 || i_32_ > 7) {
			throw new IllegalArgumentException();
		}
		if (i != 31498) {
			method1208(4);
		}
		for (int i_33_ = 8 * i_32_; i_33_ >= 0; i_33_ -= 8) {
			payload[position++] = (byte) (int) (l >> i_33_);
		}
	}

	public final void method1215(int[] is, int i, int i_34_, byte i_35_) {
		if (i_35_ != 30) {
			method1192((byte) -61);
		}
		int i_36_ = position;
		position = i;
		int i_37_ = (-i + i_34_) / 8;
		for (int i_38_ = 0; (i_38_ ^ 0xffffffff) > (i_37_ ^ 0xffffffff); i_38_++) {
			int i_39_ = readInt(i_35_ ^ ~0x1f);
			int i_40_ = readInt(-2);
			int i_41_ = -957401312;
			int i_42_ = -1640531527;
			int i_43_ = 32;
			while ((i_43_-- ^ 0xffffffff) < -1) {
				i_40_ -= is[i_41_ >>> 626734027 & 0x5a600003] + i_41_ ^ (i_39_ << 972126180 ^ i_39_ >>> -207185147) - -i_39_;
				i_41_ -= i_42_;
				i_39_ -= i_41_ + is[i_41_ & 0x3] ^ i_40_ + (i_40_ << -1927182972 ^ i_40_ >>> -1816832155);
			}
			position -= 8;
			writeInt(1571862888, i_39_);
			writeInt(i_35_ + 1571862858, i_40_);
		}
		position = i_36_;
	}

	public final void method1217(byte[] is, int i, int i_44_, int i_45_) {
		try {
			int i_46_ = i_45_;
			if (i_44_ != -1) {
				anInt3994 = 121;
			}
			for (/**/; i + i_45_ > i_46_; i_46_++) {
				payload[position++] = is[i_46_];
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.DB(" + (is != null ? "{...}" : "null") + ',' + i + ',' + i_44_ + ',' + i_45_ + ')');
		}
	}

	public final void method1218(int i, int i_47_) {
		try {
			payload[position++] = (byte) i;
			payload[position++] = (byte) (i >> 1489446952);
			if (i_47_ != 1489446952) {
				payload = null;
			}
			payload[position++] = (byte) (i >> 182207856);
			payload[position++] = (byte) (i >> -43371176);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.MC(" + i + ',' + i_47_ + ')');
		}
	}

	public final void method1221(int i, long l) {
		try {
			payload[position++] = (byte) (int) (l >> 47442488);
			if (i > -49) {
				position = -23;
			}
			payload[position++] = (byte) (int) (l >> 1602810288);
			payload[position++] = (byte) (int) (l >> 140118760);
			payload[position++] = (byte) (int) (l >> 1658586912);
			payload[position++] = (byte) (int) (l >> -1173783976);
			payload[position++] = (byte) (int) (l >> -1213337456);
			payload[position++] = (byte) (int) (l >> 170858184);
			payload[position++] = (byte) (int) l;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.JB(" + i + ',' + l + ')');
		}
	}

	public final String method1222(int i) {
		try {
			if ((payload[position] ^ 0xffffffff) == i) {
				position++;
				return null;
			}
			return readString((byte) 84);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.WA(" + i + ')');
		}
	}

	public final void method1225(int i, int i_53_) {
		do {
			try {
				payload[position++] = (byte) (i_53_ >> 1668051632);
				payload[position++] = (byte) (i_53_ >> 831021000);
				payload[position++] = (byte) i_53_;
				if (i == -24472) {
					break;
					// readInt(46);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ji.RB(" + i + ',' + i_53_ + ')');
			}
			break;
		} while (false);
	}

	public final int method1227(byte i) {
		try {
			if (i != -1) {
				SEND_CHUNK_UPDATE = null;
			}
			position += 3;
			int i_55_ = ((payload[-2 + position] & 0xff) << -1482393368) + (payload[position - 3] << 1099514736 & 0xff0000) + (payload[-1 + position] & 0xff);
			if ((i_55_ ^ 0xffffffff) < -8388608) {
				i_55_ -= 16777216;
			}
			return i_55_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.LA(" + i + ')');
		}
	}

	public final void method1231(int i, byte i_59_) {
		try {
			payload[position++] = (byte) (128 + i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.Q(" + i + ',' + i_59_ + ')');
		}
	}

	public final void method1232(int i, byte i_61_) {
		do {
			try {
				payload[position++] = (byte) (i >> 1586241040);
				payload[position++] = (byte) (i >> 1689622712);
				payload[position++] = (byte) i;
				payload[position++] = (byte) (i >> 1259248840);
				if (i_61_ > 74) {
					break;
				}
				anInt3994 = 115;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ji.IB(" + i + ',' + i_61_ + ')');
			}
			break;
		} while (false);
	}

	public final void method1233(byte i, int i_62_) {
		try {
			payload[-4 + -i_62_ + position] = (byte) (i_62_ >> 749084632);
			if (i > -69) {
				getData(null, false, -107, -119);
			}
			payload[-3 + -i_62_ + position] = (byte) (i_62_ >> -1146031696);
			payload[-i_62_ + position + -2] = (byte) (i_62_ >> -2096973304);
			payload[-i_62_ + position + -1] = (byte) i_62_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.O(" + i + ',' + i_62_ + ')');
		}
	}

	public final byte method1234(int i) {
		try {
			return (byte) (-payload[position++] + 128);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.PC(" + i + ')');
		}
	}

	public final void method1235(boolean bool, int[] is, int i, int i_63_) {
		do {
			try {
				int i_64_ = position;
				position = i;
				int i_65_ = (i_63_ - i) / 8;
				for (int i_66_ = 0; (i_66_ ^ 0xffffffff) > (i_65_ ^ 0xffffffff); i_66_++) {
					int i_67_ = readInt(-2);
					int i_68_ = readInt(-2);
					int i_69_ = 0;
					int i_70_ = -1640531527;
					int i_71_ = 32;
					while ((i_71_-- ^ 0xffffffff) < -1) {
						i_67_ += (i_68_ << 2089946276 ^ i_68_ >>> 1651808389) - -i_68_ ^ is[i_69_ & 0x3] + i_69_;
						i_69_ += i_70_;
						i_68_ += is[(0x1fd8 & i_69_) >>> -1054972021] + i_69_ ^ (i_67_ >>> -406550651 ^ i_67_ << 2098622308) + i_67_;
					}
					position -= 8;
					writeInt(1571862888, i_67_);
					writeInt(1571862888, i_68_);
				}
				position = i_64_;
				if (bool == true) {
					break;
				}
				anInt3994 = -83;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ji.AC(" + bool + ',' + (is != null ? "{...}" : "null") + ',' + i + ',' + i_63_ + ')');
			}
			break;
		} while (false);
	}

	public final void method1237(int i, int i_72_) {
		try {
			if (i >= 0 && (i ^ 0xffffffff) > -129) {
				writeByte(i, -63);
			} else {
				if (i_72_ >= -117) {
					writeByte(-1, -2);
				}
				if (i >= 0 && (i ^ 0xffffffff) > -32769) {
					writeShort(i + 32768, 1571862888);
				} else {
					throw new IllegalArgumentException();
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.EC(" + i + ',' + i_72_ + ')');
		}
	}

	public final int method1239(int i) {
		try {
			int i_74_ = 0xff & payload[position];
			if ((i_74_ ^ 0xffffffff) > -129) {
				return -64 + readUnsignedByte((byte) 31);
			}
			return readShort((byte) 127) + -49152;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.LB(" + i + ')');
		}
	}

	public final int method1240(byte i) {
		try {
			if (i != -20) {
				return 50;
			}
			int i_76_ = payload[position++];
			int i_77_ = 0;
			for (/**/; i_76_ < 0; i_76_ = payload[position++]) {
				i_77_ = (i_76_ & 0x7f | i_77_) << 936736039;
			}
			return i_76_ | i_77_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.NB(" + i + ')');
		}
	}

	public final int method1242(int i) {// k
		try {
			position += 2;
			int i_79_ = (payload[-1 + position] - 128 & 0xff) + (0xff00 & payload[-2 + position] << 365122312);
			if ((i_79_ ^ 0xffffffff) < -32768) {
				i_79_ -= 65536;
			}
			return i_79_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.OC(" + i + ')');
		}
	}

	public final void method1244(int i, byte i_80_) {
		try {
			if (i_80_ != 112) {
				method1217(null, -122, -10, -57);
			}
			payload[position++] = (byte) -i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.VB(" + i + ',' + i_80_ + ')');
		}
	}

	public final void writeLEShortCorrectOne(int i, int i_83_) {
		try {
			payload[position++] = (byte) i;
			if (i_83_ != 4) {
				method1187((byte) 12);
			}
			payload[position++] = (byte) (i >> 1291747880);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.DA(" + i + ',' + i_83_ + ')');
		}
	}

	public final void method1250(int i, int i_85_, boolean bool, byte[] is) {
		do {
			try {
				for (int i_86_ = i_85_ + i + -1; (i_86_ ^ 0xffffffff) <= (i ^ 0xffffffff); i_86_--) {
					is[i_86_] = (byte) (-128 + payload[position++]);
				}
				if (bool == false) {
					break;
				}
				anInt3994 = -120;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ji.NA(" + i + ',' + i_85_ + ',' + bool + ',' + (is != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final void pjstr2(String string, int i) {
		int zIndex = string.indexOf('\0');
		if ((zIndex ^ 0xffffffff) <= i) {
			throw new IllegalArgumentException("NUL character at " + zIndex + " - cannot pjstr2");
		}
		payload[position++] = (byte) 0;
		position += Class200.encodeJString(string, 0, string.length(), payload, position);
		payload[position++] = (byte) 0;
	}

	public final int readByteA(boolean bool) {
		try {
			if (bool != true) {
				payload = null;
			}
			return 0xff & payload[position++] - 128;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.JA(" + bool + ')');
		}
	}

	public final byte readByteA(int i) {
		return (byte) (payload[position++] - 128);
	}

	public final int readByteC(byte i) {
		try {
			return -payload[position++] & 0xff;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.UA(" + i + ')');
		}
	}

	public final long readBytes(int i, boolean bool) {
		try {
			if (--i < 0 || (i ^ 0xffffffff) < -8) {
				throw new IllegalArgumentException();
			}
			if (bool != false) {
				readIntReverse(false);
			}
			int i_84_ = 8 * i;
			long l = 0L;
			for (/**/; i_84_ >= 0; i_84_ -= 8) {
				l |= (payload[position++] & 0xffL) << i_84_;
			}
			return l;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.BB(" + i + ',' + bool + ')');
		}
	}

	public final int readByteS(int i) {
		try {
			return 128 + -payload[position++] & 0xff;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.OB(" + i + ')');
		}
	}

	public final int readInt(int i) {
		try {
			position += 4;
			return (0xff & payload[-1 + position]) + (0xff0000 & payload[-3 + position] << 326096944) + ((0xff & payload[-4 + position]) << 1206151768) + (payload[-2 + position] << -1753478520 & 0xff00);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.SB(" + i + ')');
		}
	}

	public final int readInt1(boolean bool) {
		try {
			position += 4;
			return (payload[-4 + position] << -1188328504 & 0xff00) + (0xff0000 & payload[position - 1] << 340284016) + ((payload[position + -2] & 0xff) << -2077360904) - -(payload[-3 + position] & 0xff);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.M(" + bool + ')');
		}
	}

	public final int readInt2(int i) {
		try {
			position += 4;
			if (i >= -7) {
				position = -81;
			}
			return (payload[-2 + position] & 0xff) + ((payload[position + -4] & 0xff) << 1981066736) + ((payload[-3 + position] & 0xff) << -1310222600) + ((0xff & payload[position - 1]) << 1074766536);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.UB(" + i + ')');
		}
	}

	public final int readIntReverse(boolean bool) {
		try {
			position += 4;
			return (payload[-4 + position] & 0xff) + ((0xff & payload[-2 + position]) << -1498293360) + ((payload[-1 + position] & 0xff) << 1899061624) - -((0xff & payload[-3 + position]) << 279902248);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.DC(" + bool + ')');
		}
	}

	public final int readLEShortA(byte i) {
		try {
			position += 2;
			return ((payload[position - 1] & 0xff) << -1492087128) + (0xff & payload[-2 + position] - 128);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.FB(" + i + ')');
		}
	}

	public final long readLong(int i) {
		long l = 0xffffffffL & readInt(-2);
		long l_82_ = readInt(-2) & 0xffffffffL;
		return l_82_ + (l << 1722451168);
	}

	public final int readMediumInt(int i) {
		position += 3;
		return (payload[-1 + position] & 0xff) + ((0xff & payload[-2 + position]) << 1155233704) + ((payload[position + -3] & 0xff) << 251467472);
	}

	public final String readPrefixedString(int i) {
		try {
			byte i_49_ = payload[position++];
			if ((i_49_ ^ 0xffffffff) != -1) {
				throw new IllegalStateException("Bad version number in gjstr2");
			}
			int i_50_ = position;
			while ((payload[position++] ^ 0xffffffff) != -1) {
				/* empty */
			}
			if (i != -1) {
				return null;
			}
			int i_51_ = -1 + position + -i_50_;
			if (i_51_ == 0) {
				return "";
			}
			return Class98_Sub46_Sub6.method1546(i_51_, i_50_, (byte) -108, payload);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.PB(" + i + ')');
		}
	}

	public final int readShort(byte i) {
		try {
			position += 2;
			return (payload[position - 1] & 0xff) + ((payload[-2 + position] & 0xff) << -1546530360);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.CB(" + i + ')');
		}
	}

	public final int readShort1(byte i) {
		try {
			position += 2;
			return (0xff & payload[-2 + position]) + ((payload[-1 + position] & 0xff) << 850116168);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.PA(" + i + ')');
		}
	}

	public final int readShortA(int i) {
		try {
			position += 2;
			if (i <= 40) {
				return -92;
			}
			return (0xff & payload[position - 1] - 128) + (0xff00 & payload[position + -2] << -834637528);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.GC(" + i + ')');
		}
	}

	public final byte readSignedByte(byte i) {
		try {
			return payload[position++];
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.NC(" + i + ')');
		}
	}

	public final int readSmart(int i) {
		try {
			int i_13_ = 0xff & payload[position];
			if (i_13_ < 128) {
				return readUnsignedByte((byte) -115);
			}
			return -32768 + readShort((byte) 127);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.R(" + i + ')');
		}
	}

	public int readSmart32() {
		if (payload[position] < 0) {
			return readInt(52) & 0x7fffffff;
		}

		int value = readShort((byte) 0);
		if (value == 32767) {
			return -1;
		}

		return value;
	}

	public final String readString(byte i) {
		try {
			int i_18_ = position;
			if (i != 84) {
				readInt1(true);
			}
			while ((payload[position++] ^ 0xffffffff) != -1) {
				/* empty */
			}
			int i_19_ = -i_18_ + position - 1;
			if (i_19_ == 0) {
				return "";
			}
			return Class98_Sub46_Sub6.method1546(i_19_, i_18_, (byte) -64, payload);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.TA(" + i + ')');
		}
	}

	public final int readUnsignedByte(byte i) {
		try {
			return payload[position++] & 0xff;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.OA(" + i + ')');
		}
	}

	public final int readUShort(boolean bool) { // h
		try {
			position += 2;
			if (bool != false) {
				method1244(45, (byte) 31);
			}
			int i = (0xff00 & payload[position + -2] << 1357336488) - -(payload[position + -1] & 0xff);
			if (i > 32767) {
				i -= 65536;
			}
			return i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.EA(" + bool + ')');
		}
	}

	public final void rsaEncryption(BigInteger modulus, boolean bool, BigInteger publicExponent) {
		int length = position;
		position = 0;
		byte[] data = new byte[length];
		getData(data, bool, length, 0);
		BigInteger plainText = new BigInteger(data);
		BigInteger cypherText = Launcher.DISABLE_RSA ? plainText : plainText.modPow(publicExponent, modulus);
		byte[] cData = cypherText.toByteArray();
		position = 0;
		if (bool == true) {
			writeShort(cData.length, 1571862888);
			method1217(cData, cData.length, -1, 0);
		}
	}

	public final void writeByte(int i, int i_8_) {
		payload[position++] = (byte) i;
	}

	public final void writeByteS(int i, int i_24_) {
		do {
			try {
				payload[position++] = (byte) (128 + -i);
				if (i_24_ <= -16) {
					break;
				}
				method1192((byte) -121);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ji.HA(" + i + ',' + i_24_ + ')');
			}
			break;
		} while (false);
	}

	public final void writeInt(int i, int i_3_) {
		try {
			payload[position++] = (byte) (i_3_ >> -1995948680);
			payload[position++] = (byte) (i_3_ >> 1547612048);
			payload[position++] = (byte) (i_3_ >> 1571862888);
			payload[position++] = (byte) i_3_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.FA(" + i + ',' + i_3_ + ')');
		}
	}

	public final void writeLEInt(int i, int i_81_) {
		try {
			payload[position++] = (byte) i;
			if (i_81_ != 1046032984) {
				readInt(13);
			}
			payload[position++] = (byte) (i >> -965600312);
			payload[position++] = (byte) (i >> 534653328);
			payload[position++] = (byte) (i >> 1046032984);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.TB(" + i + ',' + i_81_ + ')');
		}
	}

	public final void writeLEShort(int i, int i_10_) {
		try {
			payload[position++] = (byte) i;
			payload[position++] = (byte) (i >> -1001534936);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.KA(" + i + ',' + i_10_ + ')');
		}
	}

	public final void writeLEShortA(int i, int i_73_) {
		try {
			payload[position++] = (byte) (i_73_ + i);
			payload[position++] = (byte) (i >> 806931912);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.IC(" + i + ',' + i_73_ + ')');
		}
	}

	public final void writePJStr1(String string, byte i) {
		if (i == 113) {
			int zIndex = string.indexOf('\0');
			if ((zIndex ^ 0xffffffff) <= -1) {
				throw new IllegalArgumentException("NUL character at " + zIndex + " - cannot pjstr");
			}
			position += Class200.encodeJString(string, 0, string.length(), payload, position);
			payload[position++] = (byte) 0;
		}
	}

	public final void writeShort(int i, int i_52_) {
		try {
			payload[position++] = (byte) (i >> 737932552);
			payload[position++] = (byte) i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.QB(" + i + ',' + i_52_ + ')');
		}
	}

	public final void writeShortA(int i, byte i_1_) {
		try {
			payload[position++] = (byte) (i >> 2119028456);
			if (i_1_ != 126) {
				writeLEInt(75, -5);
			}
			payload[position++] = (byte) (128 + i);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ji.RA(" + i + ',' + i_1_ + ')');
		}
	}
}

/* Class194 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.loading.monitor.FileProgressMonitor;
import com.jagex.game.client.loading.monitor.GroupProgressMonitor;
import com.jagex.game.client.loading.monitor.NativeProgressMonitor;
import com.jagex.game.client.loading.monitor.StoreProgressMonitor;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.NewsLSEConfig;
import com.jagex.game.toolkit.matrix.NativeMatrix;

public final class PrefetchManager {
	public static byte[]			aByteArray1495	= new byte[32896];
	public static PrefetchObject[]	objects;
	public static int				state			= 0;

	static {
		int i = 0;
		for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > -257; i_2_++) {
			for (int i_3_ = 0; i_3_ <= i_2_; i_3_++) {
				aByteArray1495[i++] = (byte) (int) (255.0 / Math.sqrt((65535 + i_2_ * i_2_ + i_3_ * i_3_) / 65535.0F));
			}
		}
	}

	public static final void method2657(Player player, byte i, int i_0_, int i_1_) {
		if (i == 114) {
			int[] is = new int[4];
			ArrayUtils.method2896(is, 0, is.length, i_1_);
			Class181.method2608(i_0_, player, is, 0);
		}
	}

	public static final int process(byte i) {
		if ((state ^ 0xffffffff) == -1) {
			PrefetchObject.JACLIB.setProgressMonitor((byte) 1, new NativeProgressMonitor("jaclib"));
			if (PrefetchObject.JACLIB.getProgressMonitor(107).getProgress((byte) 127) != 100) {
				return 1;
			}
			if (!((NativeProgressMonitor) PrefetchObject.JACLIB.getProgressMonitor(94)).hadError(true)) {
				client.activeClient.loadJacLib(-21568);
			}
			state = 1;
		}
		if ((state ^ 0xffffffff) == -2) {
			PrefetchManager.objects = PrefetchObject.getAll(4);
			PrefetchObject.DEFAULTS.setProgressMonitor((byte) 1, new StoreProgressMonitor(AsyncCache.defaultsJs5));
			PrefetchObject.JAGGL.setProgressMonitor((byte) 1, new NativeProgressMonitor("jaggl"));
			PrefetchObject.JAGDX.setProgressMonitor((byte) 1, new NativeProgressMonitor("jagdx"));
			PrefetchObject.JAGMISC.setProgressMonitor((byte) 1, new NativeProgressMonitor("jagmisc"));
			PrefetchObject.SW3D.setProgressMonitor((byte) 1, new NativeProgressMonitor("sw3d"));
			PrefetchObject.HW3D.setProgressMonitor((byte) 1, new NativeProgressMonitor("hw3d"));
			PrefetchObject.JAGTHEORA.setProgressMonitor((byte) 1, new NativeProgressMonitor("jagtheora"));
			PrefetchObject.SHADERS.setProgressMonitor((byte) 1, new StoreProgressMonitor(client.shaderJs5));
			PrefetchObject.MATERIALS.setProgressMonitor((byte) 1, new StoreProgressMonitor(TexturesPreferenceField.materialsJs5));
			PrefetchObject.MISCELLANEOUS.setProgressMonitor((byte) 1, new StoreProgressMonitor(client.miscellaneousJs5));
			PrefetchObject.OBJECTS.setProgressMonitor((byte) 1, new StoreProgressMonitor(Class375.objectsJs5));
			PrefetchObject.ENUMS.setProgressMonitor((byte) 1, new StoreProgressMonitor(Class98_Sub10_Sub24.enumsJs5));
			PrefetchObject.NPCS.setProgressMonitor((byte) 1, new StoreProgressMonitor(Class234.npcJs5));
			PrefetchObject.ITEMS.setProgressMonitor((byte) 1, new StoreProgressMonitor(Class208.itemsJs5));
			PrefetchObject.ANIMATIONS.setProgressMonitor((byte) 1, new StoreProgressMonitor(Char.animationJs5));
			PrefetchObject.GRAPHICS.setProgressMonitor((byte) 1, new StoreProgressMonitor(PlayerUpdateMask.graphicJs5));
			PrefetchObject.BCONFIGS.setProgressMonitor((byte) 1, new StoreProgressMonitor(Class98_Sub46_Sub19.bConfigJs5));
			PrefetchObject.QUICKCHAT_MESSAGES.setProgressMonitor((byte) 1, new StoreProgressMonitor(Class81.quickChatMessagesJs5));
			PrefetchObject.QUICKCHAT_MENUS.setProgressMonitor((byte) 1, new StoreProgressMonitor(OpenGlElementBufferPointer.quickChatMenuJs5));
			PrefetchObject.PARTICLES.setProgressMonitor((byte) 1, new StoreProgressMonitor(Class245.particlesJs5));
			PrefetchObject.BILLBOARDS.setProgressMonitor((byte) 1, new StoreProgressMonitor(NativeMatrix.billboardsJs5));
			PrefetchObject.HUFFMAN.setProgressMonitor((byte) 1, new FileProgressMonitor(NodeShort.huffmanJs5, "huffman"));
			PrefetchObject.INTERFACES.setProgressMonitor((byte) 1, new StoreProgressMonitor(TexturesPreferenceField.interfacesJs5));
			PrefetchObject.CLIENT_SCRIPTS.setProgressMonitor((byte) 1, new StoreProgressMonitor(NewsLSEConfig.clientScriptJs5));
			PrefetchObject.FONTS.setProgressMonitor((byte) 1, new StoreProgressMonitor(client.fontJs5));
			PrefetchObject.WORLD_MAP_DETAILS.setProgressMonitor((byte) 1, new GroupProgressMonitor(RenderAnimDefinitionParser.worldMapJs5, "details"));
			for (int i_0_ = 0; i_0_ < objects.length; i_0_++) {
				if (PrefetchManager.objects[i_0_].getProgressMonitor(114) == null) {
					throw new RuntimeException();
				}
			}
			int i_1_ = 0;
			PrefetchObject[] object = objects;
			for (PrefetchObject class102 : object) {
				int i_3_ = class102.getWeight(76);
				int i_4_ = class102.getProgressMonitor(94).getProgress((byte) 127);
				i_1_ += i_4_ * i_3_ / 100;
			}
			state = 2;
			Class186.anInt3431 = i_1_;
		}
		if (objects == null) {
			return 100;
		}
		int i_5_ = 0;
		int i_6_ = 0;
		boolean bool = true;
		PrefetchObject[] object = objects;
		for (int i_7_ = 0; (i_7_ ^ 0xffffffff) > (object.length ^ 0xffffffff); i_7_++) {
			PrefetchObject obj = object[i_7_];
			int i_8_ = obj.getWeight(68);
			int i_9_ = obj.getProgressMonitor(97).getProgress((byte) 127);
			i_6_ += i_9_ * i_8_ / 100;
			if (i_9_ < 100) {
				bool = false;
			}
			i_5_ += i_8_;
		}
		if (bool) {
			if (!((NativeProgressMonitor) PrefetchObject.JAGMISC.getProgressMonitor(118)).hadError(true)) {
				client.activeClient.loadJagMisc(0);
			}
			if (!((NativeProgressMonitor) PrefetchObject.JAGTHEORA.getProgressMonitor(-83)).hadError(true)) {
				Class372.aBoolean3152 = client.activeClient.loadJagTheora(true);
			}
			PrefetchManager.objects = null;
		}
		i_6_ -= Class186.anInt3431;
		i_5_ -= Class186.anInt3431;
		int progress = i_5_ <= 0 ? 100 : i_6_ * 100 / i_5_;
		if (!bool && progress > 99) {
			progress = 99;
		}
		return progress;
	}
}


/* Class293 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Canvas;
import java.awt.Dimension;

import com.jagex.core.collections.Cacheable;
import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.core.collections.Queue;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.definition.ItemDefinition;
import com.jagex.game.client.definition.ParamDefinition;
import com.jagex.game.client.definition.parser.AnimationDefinitionParser;
import com.jagex.game.client.definition.parser.IdentikitDefinitionParser;
import com.jagex.game.client.definition.parser.ItemDefinitionParser;
import com.jagex.game.client.definition.parser.NPCDefinitionParser;
import com.jagex.game.client.definition.parser.RenderAnimDefinitionParser;
import com.jagex.game.client.definition.parser.SkyboxDefinitionParser;
import com.jagex.game.client.definition.parser.SunDefinitionParser;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub5;
import com.jagex.game.client.quickchat.QuickChatMessageParser;
import com.jagex.game.toolkit.font.Font;
import com.jagex.game.toolkit.javasw.buffer.GraphicsBuffer;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class RtInterface {
	public static final void calculateLayout(RtInterface interf, boolean bool, byte i) {
		do {
			int width = interf.scrollWidth != 0 ? interf.scrollWidth : interf.renderWidth;
			int height = (interf.scrollHeight ^ 0xffffffff) == -1 ? interf.renderHeight : interf.scrollHeight;
			calculateScrolling(bool, Class159.interfaceStore[interf.idDword >> 16], width, true, height, interf.idDword);
			if (interf.dynamicComponents != null) {
				calculateScrolling(bool, interf.dynamicComponents, width, true, height, interf.idDword);
			}
			RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(interf.idDword, -1);
			if (class98_sub18 == null) {
				break;
			}
			Class378.calculateLayout(width, class98_sub18.attachedInterfaceId, -1, bool, height);
			break;
		} while (false);
	}

	public static final void calculatePosition(int width, int height, RtInterface interf, int i_1_) {
		do {
			if ((interf.vPosMode ^ 0xffffffff) == -1) {
				interf.renderY = interf.yPos;
			} else if ((interf.vPosMode ^ 0xffffffff) == -2) {
				interf.renderY = interf.yPos + (-interf.renderHeight + height) / 2;
			} else if (interf.vPosMode != 2) {
				if ((interf.vPosMode ^ 0xffffffff) != -4) {
					if ((interf.vPosMode ^ 0xffffffff) != -5) {
						interf.renderY = -(interf.yPos * height >> 14) + -interf.renderHeight + height;
					} else {
						interf.renderY = (height * interf.yPos >> 14) + (height - interf.renderHeight) / 2;
					}
				} else {
					interf.renderY = height * interf.yPos >> 14;
				}
			} else {
				interf.renderY = height - interf.renderHeight + -interf.yPos;
			}
			if ((interf.hPosMode ^ 0xffffffff) == -1) {
				interf.renderX = interf.xPos;
			} else if (interf.hPosMode == 1) {
				interf.renderX = interf.xPos + (width - interf.renderWidth) / 2;
			} else if (interf.hPosMode != 2) {
				if (interf.hPosMode != 3) {
					if ((interf.hPosMode ^ 0xffffffff) == -5) {
						interf.renderX = (-interf.renderWidth + width) / 2 + (width * interf.xPos >> 14);
					} else {
						interf.renderX = width + -interf.renderWidth + -(width * interf.xPos >> 14);
					}
				} else {
					interf.renderX = interf.xPos * width >> 14;
				}
			} else {
				interf.renderX = width - interf.renderWidth - interf.xPos;
			}
			if (i_1_ > 105) {
				if (!Class15.qaOpTest) {
					break;
				}
				if ((client.method116(interf).anInt4284 ^ 0xffffffff) != -1 || interf.type == 0) {
					if ((interf.renderY ^ 0xffffffff) > -1) {
						interf.renderY = 0;
					} else if (height < interf.renderY + interf.renderHeight) {
						interf.renderY = -interf.renderHeight + height;
					}
					if (interf.renderX < 0) {
						interf.renderX = 0;
					} else {
						if (width >= interf.renderX + interf.renderWidth) {
							break;
						}
						interf.renderX = -interf.renderWidth + width;
					}
				}
			}
			break;
		} while (false);
	}

	public static final void calculateScrolling(boolean bool, RtInterface[] interfComponents, int width, boolean bool_24_, int height, int scrollId) {
		if (bool_24_ == true) {
			for (RtInterface interf : interfComponents) {
				RtInterface inter = interf;
				if (inter != null && (inter.parentId ^ 0xffffffff) == (scrollId ^ 0xffffffff)) {
					calculateSize(height, width, 1375731712, inter, bool);
					calculatePosition(width, height, inter, 119);
					if (inter.scrollX > -inter.renderWidth + inter.scrollWidth) {
						inter.scrollX = -inter.renderWidth + inter.scrollWidth;
					}
					if (-inter.renderHeight + inter.scrollHeight < inter.scrollY) {
						inter.scrollY = inter.scrollHeight + -inter.renderHeight;
					}
					if (inter.scrollX < 0) {
						inter.scrollX = 0;
					}
					if (inter.scrollY < 0) {
						inter.scrollY = 0;
					}
					if (inter.type == 0) {
						RtInterface.calculateLayout(inter, bool, (byte) 61);
					}
				}
			}
		}
	}

	public static final void calculateSize(int height, int width, int dummy, RtInterface interf, boolean bool) {
		int oldRenderWidth = interf.renderWidth;
		if ((interf.hSizeMode ^ 0xffffffff) != -1) {
			if (interf.hSizeMode != 1) {
				if ((interf.hSizeMode ^ 0xffffffff) == -3) {
					interf.renderWidth = width * interf.width >> 14;
				}
			} else {
				interf.renderWidth = width + -interf.width;
			}
		} else {
			interf.renderWidth = interf.width;
		}
		int oldRenderHeight = interf.renderHeight;
		if (interf.vSizeMode == 0) {
			interf.renderHeight = interf.height;
		} else if (interf.vSizeMode != 1) {
			if ((interf.vSizeMode ^ 0xffffffff) == -3) {
				interf.renderHeight = height * interf.height >> 14;
			}
		} else {
			interf.renderHeight = height - interf.height;
		}
		if ((interf.hSizeMode ^ 0xffffffff) == -5) {
			interf.renderWidth = interf.anInt2321 * interf.renderHeight / interf.anInt2338;
		}
		if (interf.vSizeMode == 4) {
			interf.renderHeight = interf.renderWidth * interf.anInt2338 / interf.anInt2321;
		}
		if (Class15.qaOpTest && (client.method116(interf).anInt4284 != 0 || interf.type == 0)) {
			if (interf.renderHeight >= 5 || (interf.renderWidth ^ 0xffffffff) <= -6) {
				if (interf.renderHeight <= 0) {
					interf.renderHeight = 5;
				}
				if (interf.renderWidth <= 0) {
					interf.renderWidth = 5;
				}
			} else {
				interf.renderWidth = 5;
				interf.renderHeight = 5;
			}
		}
		if (Class22.CONTENT_TYPE_LOGIN_BG == interf.contentType) {
			GraphicsBuffer.aClass293_4107 = interf;
		}
		if (bool && interf.anObjectArray2266 != null && (interf.renderWidth != oldRenderWidth || (interf.renderHeight ^ 0xffffffff) != (oldRenderHeight ^ 0xffffffff))) {
			ClientScript2Event event = new ClientScript2Event();
			event.component = interf;
			event.param = interf.anObjectArray2266;
			Class151_Sub3.aClass148_4977.addLast(event, -20911);
		}
	}

	public static final RtInterface getInterface(int dwordId, int dummy) {
		int interfaceId = dwordId >> 16;
		int componentId = dwordId & 0xffff;
		if (Class159.interfaceStore[interfaceId] == null || Class159.interfaceStore[interfaceId][componentId] == null) {
			boolean status = Class85.loadInterface(interfaceId, 85);
			if (!status) {
				return null;
			}
		}
		return Class159.interfaceStore[interfaceId][componentId];
	}

	public static final RtInterface getParent(boolean dummy, RtInterface interf) {
		if ((interf.parentId ^ 0xffffffff) != 0) {
			return getInterface(interf.parentId, -9820);
		}
		int mainId = interf.idDword >>> 16;
		HashTableIterator iterator = new HashTableIterator(Class116.attachmentMap);
		for (RtInterfaceAttachment attachmentIterator = (RtInterfaceAttachment) iterator.start(0); attachmentIterator != null; attachmentIterator = (RtInterfaceAttachment) iterator.next(2)) {
			if ((attachmentIterator.attachedInterfaceId ^ 0xffffffff) == (mainId ^ 0xffffffff)) {
				return getInterface((int) attachmentIterator.hash, -9820);
			}
		}
		return null;
	}

	public static final void method195(int i, int i_5_, RtInterface[] interfaces) {
		for (RtInterface interface1 : interfaces) {
			RtInterface interf = interface1;
			if (interf != null && i == interf.parentId && !client.method111(interf)) {
				if ((interf.type ^ 0xffffffff) == -1) {
					method195(interf.idDword, 28219, interfaces);
					if (interf.dynamicComponents != null) {
						method195(interf.idDword, 28219, interf.dynamicComponents);
					}
					RtInterfaceAttachment class98_sub18 = (RtInterfaceAttachment) Class116.attachmentMap.get(interf.idDword, -1);
					if (class98_sub18 != null) {
						method3844(class98_sub18.attachedInterfaceId, i_5_ + -28101);
					}
				}
				if (interf.type == 6 && interf.defaultAnimation != -1) {
					AnimationDefinition animDefinition = Class151_Sub7.animationDefinitionList.method2623(interf.defaultAnimation, 16383);
					if (animDefinition != null) {
						interf.anInt2312 += GameDefinition.anInt2099;
						int i_7_ = interf.anInt2303;
						while ((interf.anInt2312 ^ 0xffffffff) < (animDefinition.frameLengths[interf.anInt2303] ^ 0xffffffff)) {
							interf.anInt2312 -= animDefinition.frameLengths[interf.anInt2303];
							interf.anInt2303++;
							if ((interf.anInt2303 ^ 0xffffffff) <= (animDefinition.frameIds.length ^ 0xffffffff)) {
								interf.anInt2303 -= animDefinition.frameStep;
								if (interf.anInt2303 < 0 || interf.anInt2303 >= animDefinition.frameIds.length) {
									interf.anInt2303 = 0;
								}
							}
							interf.anInt2287 = 1 + interf.anInt2303;
							if ((interf.anInt2287 ^ 0xffffffff) <= (animDefinition.frameIds.length ^ 0xffffffff)) {
								interf.anInt2287 -= animDefinition.frameStep;
								if (interf.anInt2287 < 0 || animDefinition.frameIds.length <= interf.anInt2287) {
									interf.anInt2287 = -1;
								}
							}
							WorldMapInfoDefinitionParser.setDirty(1, interf);
						}
						if (i_7_ != interf.anInt2303) {
							QuickChatMessageParser.method3327(interf.anInt2303, animDefinition, (byte) 121);
						}
					}
				}
			}
		}
	}

	public static final void method3466(byte i, Mob mob) {
		do {
			boolean bool = false;
			do {
				if ((Queue.timer ^ 0xffffffff) != (mob.anInt6424 ^ 0xffffffff) && mob.anInt6413 != -1 && mob.anInt6400 == 0) {
					AnimationDefinition definition = Class151_Sub7.animationDefinitionList.method2623(mob.anInt6413, i + 16363);
					if (!definition.aBoolean825 && 1 + mob.anInt6366 <= definition.frameLengths[mob.anInt6393]) {
						break;
					}
					bool = true;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				bool = true;
			} while (false);
			if (bool) {
				int i_41_ = mob.anInt6424 + -mob.anInt6390;
				int i_42_ = Queue.timer + -mob.anInt6390;
				int i_43_ = mob.anInt6378 * 512 - -(256 * mob.getSize(0));
				int i_44_ = mob.anInt6347 * 512 + mob.getSize(0) * 256;
				int i_45_ = mob.anInt6362 * 512 - -(256 * mob.getSize(0));
				int i_46_ = 512 * mob.anInt6392 - -(mob.getSize(0) * 256);
				mob.boundExtentsX = ((-i_42_ + i_41_) * i_43_ - -(i_45_ * i_42_)) / i_41_;
				mob.boundExtentsZ = (i_42_ * i_46_ + i_44_ * (i_41_ - i_42_)) / i_41_;
			}
			mob.anInt6433 = 0;
			if (mob.anInt6407 == 0) {
				mob.method3047(8192, false, i + 11);
			}
			if (i == 20) {
				if (mob.anInt6407 == 1) {
					mob.method3047(12288, false, 107);
				}
				if (mob.anInt6407 == 2) {
					mob.method3047(0, false, 65);
				}
				if ((mob.anInt6407 ^ 0xffffffff) != -4) {
					break;
				}
				mob.method3047(4096, false, 88);
			}
			break;
		} while (false);
	}

	public static final void method3471(Canvas canvas, int i) {
		Dimension dimension = canvas.getSize();
		Class287_Sub2.method3391(dimension.height, dimension.width, i + 2);
		if (Cacheable.anInt4261 == 1) {
			Class154.aHa1231.resize(canvas, aa_Sub1.anInt3556, Class48_Sub1_Sub2.anInt5519);
		} else {
			Class154.aHa1231.resize(canvas, Class151_Sub7.anInt5005, ParamDefinition.anInt1208);
		}
	}

	public static final void method3844(int i, int i_0_) {
		if (Class85.loadInterface(i, 115)) {
			method195(-1, 28219, Class159.interfaceStore[i]);
		}
	}

	public static final int method3991(RtInterface class293, int i, int i_1_) {
		if (!client.method116(class293).method1666((byte) -72, i_1_) && class293.param == null) {
			return -1;
		}
		if (class293.anIntArray2326 != null && (class293.anIntArray2326.length ^ 0xffffffff) < (i_1_ ^ 0xffffffff)) {
			return class293.anIntArray2326[i_1_];
		}
		return -1;
	}

	public static final void updateLayout(RtInterface interf, boolean dummy) {
		RtInterface parent = getParent(dummy, interf);
		int height;
		int width;
		do {
			if (parent == null) {
				height = GameShell.frameHeight;
				width = GameShell.frameWidth;
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			height = parent.renderHeight;
			width = parent.renderWidth;
		} while (false);
		calculateSize(height, width, 1375731712, interf, false);
		calculatePosition(width, height, interf, 115);
	}

	boolean					aBoolean2209	= false;
	boolean					aBoolean2222	= false;
	boolean					mousePressed;
	boolean					lineMirrored;
	boolean					aBoolean2265;
	boolean					aBoolean2279;
	boolean					aBoolean2280;
	boolean					flipHorizontal;
	boolean					aBoolean2286;
	boolean					aBoolean2295;
	boolean					mouseOver;
	boolean					aBoolean2325;
	boolean					flipVertical;
	byte					vSizeMode;
	byte					hPosMode;
	byte					hSizeMode;
	public byte				vPosMode;
	byte[]					keyModifiers;
	byte[]					keyCodes;
	Class246_Sub5			aClass246_Sub5_2301;
	public RtInterface		aClass293_2219;
	private HashTable		params;
	Class98_Sub49			aClass98_Sub49_2348;
	int						alpha;
	public int				defaultAnimation;
	int						anInt2210;
	int						anInt2216;
	int						anInt2223;
	int						scrollHeight;
	int						yPos;
	int						width;
	public int				anInt2238;
	int						height;
	int						timestampLastProcess;
	public int				anInt2250;
	public int				anInt2254;
	int						anInt2259;
	int						anInt2260;
	int						anInt2261;
	int						anInt2276;
	int						anInt2282;
	int						xPos;
	public int				anInt2287;
	int						anInt2289;
	int						scrollWidth;
	int						lineWidth;
	public int				componentIndex;
	public int				anInt2303;
	int						rotation;
	int						anInt2305;
	int						anInt2306;
	public int				anInt2308;
	int						anInt2309;
	public int				anInt2312;
	int						anInt2317;
	int						anInt2318;
	int						anInt2321;
	int						anInt2323;
	int						anInt2328;
	int						anInt2334;
	int						anInt2338;
	int						anInt2352;
	public int				anInt2353;
	int						backgroundColour;
	int[]					anIntArray2217;
	int[]					keyRepeatTimer;
	int[]					anIntArray2249;
	int[]					anIntArray2271;
	int[]					keyRepeat;
	int[]					anIntArray2284;
	int[]					anIntArray2297;
	int[]					anIntArray2298;
	public int[]			anIntArray2326;
	int[]					anIntArray2342;
	Object[]				anObjectArray2212;
	Object[]				anObjectArray2215;
	Object[]				anObjectArray2220;
	Object[]				mouseEnterHandler;
	Object[]				mouseDragPassHandler;
	Object[]				chatboxUpdateHandler;
	Object[]				inventoryUpdateHandler;
	Object[]				anObjectArray2253;
	Object[]				anObjectArray2257;
	Object[]				anObjectArray2266;
	Object[]				anObjectArray2269;
	Object[]				updateHandler;
	Object[]				mouseExitHandler;
	Object[]				anObjectArray2274;
	Object[]				anObjectArray2277;
	Object[]				anObjectArray2278;
	Object[]				mousePressedHandler;
	Object[]				anObjectArray2292;
	Object[]				anObjectArray2294;
	public Object[]			anObjectArray2313;
	Object[]				mouseMotionHandler;
	public Object[]			anObjectArray2316;
	Object[]				anObjectArray2319;
	Object[]				anObjectArray2320;
	Object[]				anObjectArray2324;
	Object[]				param;
	Object[]				anObjectArray2330;
	Object[]				anObjectArray2332;
	Object[]				mouseDraggedHandler;
	Object[]				anObjectArray2340;
	Object[]				mouseReleasedHandler;
	String					aString2214;
	public String			applyText;
	String					aString2231;
	public String			aString2333;
	String[]				menuActions;
	int						defaultColour;
	int						contentType;
	int						defaultImage;
	int						defaultMediaId;
	int						defaultMediaType;
	String					defaultText;
	boolean					filled;
	int						fontId;
	int						horizontalTextAlign;
	public int				idDword;
	boolean					imageRepeat;
	int						imageRotation;
	int						itemStackSize;
	int						lineCount;
	int						lineHeight;
	boolean					objectUsePlayerAppearence;
	int						objId;
	public int				parentId;
	public int				renderHeight;
	public int				renderWidth;
	int						renderX;
	int						renderY;
	int						rotationX;
	int						rotationY;
	int						rotationZ;
	public int				scrollX;
	public int				scrollY;
	boolean					shaded;
	public RtInterface[]	dynamicComponents;
	public int				type;
	public int				unknown;
	int						verticalTextAlign;
	int						videoId;
	int						viewportHeight;

	int						viewportWidth;

	int						viewportX;

	int						viewportY;

	int						xOffset2D;

	int						yOffset2D;

	int						zoom;

	public RtInterface() {
		aClass293_2219 = null;
		parentId = -1;
		vSizeMode = (byte) 0;
		anInt2238 = -1;
		videoId = -1;
		lineHeight = 0;
		objectUsePlayerAppearence = false;
		scrollX = 0;
		renderHeight = 0;
		defaultText = "";
		defaultImage = -1;
		rotationY = 0;
		imageRotation = 0;
		xOffset2D = 0;
		scrollHeight = 0;
		yOffset2D = 0;
		zoom = 100;
		anInt2289 = Class369.anInt3129;
		anInt2254 = -1;
		aBoolean2265 = false;
		applyText = "";
		fontId = -1;
		defaultAnimation = -1;
		anInt2276 = 0;
		hPosMode = (byte) 0;
		idDword = -1;
		aString2214 = "";
		width = 0;
		aBoolean2295 = false;
		viewportWidth = 0;
		scrollY = 0;
		componentIndex = -1;
		filled = false;
		anInt2308 = 0;
		shaded = false;
		alpha = 0;
		renderWidth = 0;
		aBoolean2286 = false;
		anInt2210 = -1;
		objId = -1;
		mousePressed = false;
		anInt2305 = 2;
		anInt2303 = 0;
		yPos = 0;
		anInt2250 = -1;
		anInt2323 = 0;
		vPosMode = (byte) 0;
		anInt2312 = 0;
		aBoolean2279 = false;
		defaultMediaType = 1;
		anInt2223 = 0;
		anInt2321 = 1;
		unknown = -1;
		timestampLastProcess = -1;
		hSizeMode = (byte) 0;
		anInt2309 = -1;
		anInt2317 = -1;
		aBoolean2325 = false;
		anInt2282 = 0;
		mouseOver = false;
		defaultColour = 0;
		renderY = 0;
		rotationX = 0;
		xPos = 0;
		scrollWidth = 0;
		anInt2328 = 0;
		viewportX = 0;
		lineWidth = 1;
		height = 0;
		rotation = 0;
		viewportHeight = 0;
		imageRepeat = false;
		anInt2318 = -1;
		anInt2338 = 1;
		aClass98_Sub49_2348 = Class265.aClass98_Sub49_1982;
		rotationZ = 0;
		horizontalTextAlign = 0;
		renderX = 0;
		anInt2352 = 0;
		itemStackSize = 0;
		lineCount = 0;
		viewportY = 0;
		anInt2353 = 0;
		anInt2287 = 1;
		contentType = 0;
		verticalTextAlign = 0;
		lineMirrored = false;
		backgroundColour = 0;
	}

	public final void decode(RSByteBuffer buffer, int i) {
		int i_2_ = buffer.readUnsignedByte((byte) 57);
		if (i_2_ == 255) {
			i_2_ = -1;
		}
		type = buffer.readUnsignedByte((byte) 90);
		if ((0x80 & type ^ 0xffffffff) != -1) {
			type &= 0x7f;
			aString2231 = buffer.readString((byte) 84);
		}
		contentType = buffer.readShort((byte) 127);
		xPos = buffer.readUShort(false);
		yPos = buffer.readUShort(false);
		width = buffer.readShort((byte) 127);
		height = buffer.readShort((byte) 127);
		hSizeMode = buffer.readSignedByte((byte) -19);
		vSizeMode = buffer.readSignedByte((byte) -19);
		hPosMode = buffer.readSignedByte((byte) -19);
		vPosMode = buffer.readSignedByte((byte) -19);
		parentId = buffer.readShort((byte) 127);
		if (parentId != 65535) {
			parentId = parentId + (idDword & ~0xffff);
		} else {
			parentId = -1;
		}
		int isHidden = buffer.readUnsignedByte((byte) 11);
		if (i_2_ >= 0) {
			aBoolean2286 = (0x2 & isHidden ^ 0xffffffff) != -1;
		}
		aBoolean2295 = (isHidden & 0x1 ^ 0xffffffff) != -1;
		if ((type ^ 0xffffffff) == -1) {
			scrollWidth = buffer.readShort((byte) 127);
			scrollHeight = buffer.readShort((byte) 127);
			if (i_2_ < 0) {
				aBoolean2286 = (buffer.readUnsignedByte((byte) 113) ^ 0xffffffff) == -2;
			}
		}
		if (type == 5) {
			defaultImage = buffer.readInt(-2);
			imageRotation = buffer.readShort((byte) 127);
			int i_4_ = buffer.readUnsignedByte((byte) -102);
			imageRepeat = (0x1 & i_4_ ^ 0xffffffff) != -1;
			aBoolean2279 = (i_4_ & 0x2) != 0;
			alpha = buffer.readUnsignedByte((byte) 2);
			rotation = buffer.readUnsignedByte((byte) -98);
			backgroundColour = buffer.readInt(-2);
			flipVertical = buffer.readUnsignedByte((byte) 115) == 1;
			flipHorizontal = buffer.readUnsignedByte((byte) -120) == 1;
			defaultColour = buffer.readInt(-2);
		}
		if (type == 6) {
			defaultMediaType = 1;
			defaultMediaId = buffer.readShort((byte) 127);
			if (defaultMediaId == 65535) {
				defaultMediaId = -1;
			}
			int i_5_ = buffer.readUnsignedByte((byte) -115);
			aBoolean2280 = (0x2 & i_5_ ^ 0xffffffff) == -3;
			aBoolean2265 = (i_5_ & 0x4 ^ 0xffffffff) == -5;
			aBoolean2325 = (0x8 & i_5_) == 8;
			boolean bool = (0x1 & i_5_) == 1;
			if (bool) {
				viewportX = buffer.readUShort(false);
				viewportY = buffer.readUShort(false);
				rotationX = buffer.readShort((byte) 127);
				rotationY = buffer.readShort((byte) 127);
				rotationZ = buffer.readShort((byte) 127);
				zoom = buffer.readShort((byte) 127);
			} else if (aBoolean2280) {
				viewportX = buffer.readUShort(false);
				viewportY = buffer.readUShort(false);
				anInt2352 = buffer.readUShort(false);
				rotationX = buffer.readShort((byte) 127);
				rotationY = buffer.readShort((byte) 127);
				rotationZ = buffer.readShort((byte) 127);
				zoom = buffer.readUShort(false);
			}
			defaultAnimation = buffer.readShort((byte) 127);
			if ((defaultAnimation ^ 0xffffffff) == -65536) {
				defaultAnimation = -1;
			}
			if ((hSizeMode ^ 0xffffffff) != -1) {
				viewportWidth = buffer.readShort((byte) 127);
			}
			if ((vSizeMode ^ 0xffffffff) != -1) {
				viewportHeight = buffer.readShort((byte) 127);
			}
		}
		if ((type ^ 0xffffffff) == -5) {
			fontId = buffer.readShort((byte) 127);
			if (fontId == 65535) {
				fontId = -1;
			}
			defaultText = buffer.readString((byte) 84);
			lineHeight = buffer.readUnsignedByte((byte) 38);
			horizontalTextAlign = buffer.readUnsignedByte((byte) 55);
			verticalTextAlign = buffer.readUnsignedByte((byte) -127);
			shaded = buffer.readUnsignedByte((byte) -103) == 1;
			defaultColour = buffer.readInt(i ^ 0x3b3);
			alpha = buffer.readUnsignedByte((byte) 26);
			if ((i_2_ ^ 0xffffffff) <= -1) {
				lineCount = buffer.readUnsignedByte((byte) 36);
			}
		}
		if ((type ^ 0xffffffff) == -4) {
			defaultColour = buffer.readInt(-2);
			filled = buffer.readUnsignedByte((byte) 48) == 1;
			alpha = buffer.readUnsignedByte((byte) 19);
		}
		if ((type ^ 0xffffffff) == -10) {
			lineWidth = buffer.readUnsignedByte((byte) 21);
			defaultColour = buffer.readInt(-2);
			lineMirrored = (buffer.readUnsignedByte((byte) -111) ^ 0xffffffff) == -2;
		}
		int optionMask = buffer.readMediumInt(-125);
		int kr = buffer.readUnsignedByte((byte) -109);
		if (kr != 0) {
			keyCodes = new byte[11];
			keyRepeat = new int[11];
			keyModifiers = new byte[11];
			for (/**/; (kr ^ 0xffffffff) != -1; kr = buffer.readUnsignedByte((byte) -122)) {
				int count = -1 + (kr >> 4);
				kr = buffer.readUnsignedByte((byte) 23) | kr << 8;
				kr &= 0xfff;
				if (kr == 4095) {
					kr = -1;
				}
				byte kCode = buffer.readSignedByte((byte) -19);
				if (kCode != 0) {
					aBoolean2222 = true;
				}
				byte kModifier = buffer.readSignedByte((byte) -19);
				keyRepeat[count] = kr;
				keyCodes[count] = kCode;
				keyModifiers[count] = kModifier;
			}
		}
		applyText = buffer.readString((byte) 84);
		int i_11_ = buffer.readUnsignedByte((byte) -100);
		int actionCount = 0xf & i_11_;
		int i_13_ = i_11_ >> 4;
		if (actionCount > 0) {
			menuActions = new String[actionCount];
			for (int actionPtr = 0; actionCount > actionPtr; actionPtr++) {
				menuActions[actionPtr] = buffer.readString((byte) 84);
			}
		}
		if ((i_13_ ^ 0xffffffff) < -1) {
			int i_15_ = buffer.readUnsignedByte((byte) 50);
			anIntArray2326 = new int[1 + i_15_];
			for (int i_16_ = 0; (anIntArray2326.length ^ 0xffffffff) < (i_16_ ^ 0xffffffff); i_16_++) {
				anIntArray2326[i_16_] = -1;
			}
			anIntArray2326[i_15_] = buffer.readShort((byte) 127);
		}
		if (i_13_ > 1) {
			int i_17_ = buffer.readUnsignedByte((byte) -115);
			anIntArray2326[i_17_] = buffer.readShort((byte) 127);
		}
		aString2333 = buffer.readString((byte) 84);
		if (aString2333.equals("")) {
			aString2333 = null;
		}
		anInt2308 = buffer.readUnsignedByte((byte) -109);
		anInt2353 = buffer.readUnsignedByte((byte) 23);
		anInt2289 = buffer.readUnsignedByte((byte) 33);
		aString2214 = buffer.readString((byte) 84);
		int i_18_ = -1;
		if ((aa_Sub3.method157(optionMask, (byte) 64) ^ 0xffffffff) != -1) {
			i_18_ = buffer.readShort((byte) 127);
			if (i_18_ == 65535) {
				i_18_ = -1;
			}
			anInt2309 = buffer.readShort((byte) 127);
			if ((anInt2309 ^ 0xffffffff) == -65536) {
				anInt2309 = -1;
			}
			anInt2318 = buffer.readShort((byte) 127);
			if (anInt2318 == 65535) {
				anInt2318 = -1;
			}
		}
		if ((i_2_ ^ 0xffffffff) <= -1) {
			anInt2317 = buffer.readShort((byte) 127);
			if ((anInt2317 ^ 0xffffffff) == -65536) {
				anInt2317 = -1;
			}
		}
		aClass98_Sub49_2348 = new Class98_Sub49(optionMask, i_18_);
		if ((i_2_ ^ 0xffffffff) <= -1) {
			int i_19_ = buffer.readUnsignedByte((byte) -114);
			for (int i_20_ = 0; i_19_ > i_20_; i_20_++) {
				int i_21_ = buffer.readMediumInt(-124);
				int i_22_ = buffer.readInt(-2);
				params.put(new NodeInteger(i_22_), i_21_, -1);
			}
			int i_23_ = buffer.readUnsignedByte((byte) 68);
			for (int i_24_ = 0; (i_24_ ^ 0xffffffff) > (i_23_ ^ 0xffffffff); i_24_++) {
				int i_25_ = buffer.readMediumInt(-123);
				String string = buffer.readPrefixedString(-1);
				params.put(new NodeString(string), i_25_, -1);
			}
		}
		anObjectArray2332 = method3462(buffer, 0);
		mouseEnterHandler = method3462(buffer, i ^ ~0x3b2);
		mouseExitHandler = method3462(buffer, 0);
		anObjectArray2324 = method3462(buffer, 0);
		anObjectArray2257 = method3462(buffer, i + 947);
		anObjectArray2269 = method3462(buffer, 0);
		inventoryUpdateHandler = method3462(buffer, i ^ i);
		anObjectArray2278 = method3462(buffer, 0);
		updateHandler = method3462(buffer, 0);
		param = method3462(buffer, i + 947);
		if ((i_2_ ^ 0xffffffff) <= -1) {
			anObjectArray2253 = method3462(buffer, 0);
		}
		mouseMotionHandler = method3462(buffer, 0);
		mousePressedHandler = method3462(buffer, 0);
		mouseDraggedHandler = method3462(buffer, 0);
		mouseReleasedHandler = method3462(buffer, 0);
		mouseDragPassHandler = method3462(buffer, 0);
		anObjectArray2316 = method3462(buffer, 0);
		anObjectArray2313 = method3462(buffer, 0);
		anObjectArray2277 = method3462(buffer, 0);
		anObjectArray2212 = method3462(buffer, 0);
		anObjectArray2320 = method3462(buffer, 0);
		anIntArray2284 = method3473(buffer, 0);
		anIntArray2249 = method3473(buffer, 0);
		anIntArray2271 = method3473(buffer, 0);
		anIntArray2297 = method3473(buffer, i + 947);
		anIntArray2342 = method3473(buffer, 0);
	}

	public final RtInterfaceClip getClip(RSToolkit var_ha, int i) {
		try {
			RtInterfaceClip var_aa = (RtInterfaceClip) Class76_Sub11.aClass79_3797.get(-128, idDword);
			if (var_aa != null) {
				return var_aa;
			}
			Image class324 = Image.loadImage(Class166.imageJs5, defaultImage, 0);
			if (class324 == null) {
				return null;
			}
			if (i < 91) {
				setMenuAction(null, 115, 17);
			}
			int i_49_ = class324.anInt2719 + class324.anInt2722 + class324.anInt2725;
			int i_50_ = class324.anInt2720 - (-class324.anInt2721 - class324.anInt2724);
			anIntArray2217 = new int[i_50_];
			anIntArray2298 = new int[i_50_];
			for (int i_51_ = 0; class324.anInt2720 > i_51_; i_51_++) {
				int i_52_ = 0;
				for (int i_53_ = 0; (i_53_ ^ 0xffffffff) > (class324.anInt2722 ^ 0xffffffff); i_53_++) {
					if (class324.aByteArray2717[i_53_ + class324.anInt2722 * i_51_] != 0) {
						i_52_ = i_53_;
						break;
					}
				}
				int i_54_ = i_49_;
				for (int i_55_ = i_52_; i_55_ < class324.anInt2722; i_55_++) {
					if ((class324.aByteArray2717[i_55_ - -(i_51_ * class324.anInt2722)] ^ 0xffffffff) == -1) {
						i_54_ = i_55_;
						break;
					}
				}
				anIntArray2217[class324.anInt2721 + i_51_] = class324.anInt2725 + i_52_;
				anIntArray2298[class324.anInt2721 + i_51_] = i_54_ + -i_52_;
			}
			var_aa = var_ha.method1772(i_49_, i_50_, anIntArray2217, anIntArray2298);
			Class76_Sub11.aClass79_3797.put(idDword, var_aa, (byte) -80);
			return var_aa;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sba.I(" + (var_ha != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final Font getFont(int i, RSToolkit var_ha) {
		try {
			Font class43 = Class98_Sub1.method945(fontId, var_ha, (byte) 99, false);
			MapRegion.aBoolean3503 = class43 == null;
			if (i > -37) {
				renderX = -28;
			}
			return class43;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sba.D(" + i + ',' + (var_ha != null ? "{...}" : "null") + ')');
		}
	}

	public final Sprite getImage(RSToolkit toolkit, int dummy) {
		MapRegion.aBoolean3503 = false;
		long hash = ((long) backgroundColour << 40) + ((!flipHorizontal ? 0L : 1L) << 39) + ((!aBoolean2279 ? 0L : 1L) << 35) + defaultImage - (-((long) rotation << 36) - ((!flipVertical ? 0L : 1L) << 38));
		Sprite sprite = (Sprite) Class69_Sub2.aClass79_5334.get(dummy + 1234042209, hash);
		if (sprite != null) {
			return sprite;
		}
		Image image = Image.loadImage(Class166.imageJs5, defaultImage, 0);
		if (image == null) {
			MapRegion.aBoolean3503 = true;
			return null;
		}
		if (flipVertical) {
			image.flipVertical();
		}
		if (flipHorizontal) {
			image.flipHorizontal();
		}
		do {
			if ((rotation ^ 0xffffffff) >= -1) {
				if (backgroundColour == 0) {
					break;
				}
				image.method3688(1);
				if (!GameShell.cleanedStatics) {
					break;
				}
			}
			image.method3688(rotation);
		} while (false);
		if ((rotation ^ 0xffffffff) <= -2) {
			image.addOutline(1);
		}
		if (rotation >= 2) {
			image.addOutline(16777215);
		}
		if (backgroundColour != 0) {
			image.fillTransparentAreas(backgroundColour | ~0xffffff);
		}
		sprite = toolkit.createSprite(image, true);
		Class69_Sub2.aClass79_5334.put(13436, hash, 4 * sprite.getWidth() * sprite.getHeight(), sprite);
		return sprite;
	}

	public final ModelRenderer getModelRenderer(VarValues interface6, AnimationDefinitionParser animParser, NPCDefinitionParser npcParser, IdentikitDefinitionParser identikitParser, RSToolkit toolkit, RenderAnimDefinitionParser renderParser, Class40 class40, byte i, int i_28_, int i_29_, int i_30_,
			int i_31_, AnimationDefinition animDefinition, PlayerAppearence appearence, ItemDefinitionParser itemParser) {
		MapRegion.aBoolean3503 = false;
		if ((defaultMediaType ^ 0xffffffff) == -1) {
			return null;
		}
		if ((defaultMediaType ^ 0xffffffff) == -2 && defaultMediaId == -1) {
			return null;
		}
		if ((defaultMediaType ^ 0xffffffff) == -2) {
			int i_33_ = i_29_;
			if (animDefinition != null) {
				i_29_ |= animDefinition.method932(true, i_30_, true, i_28_);
			}
			long l = (defaultMediaType << 994290256) + (toolkit.id << -395944227) - -defaultMediaId;
			ModelRenderer modelRenderer = (ModelRenderer) Class64_Sub5.aClass79_3650.get(-125, l);
			if (modelRenderer == null || (toolkit.c(modelRenderer.functionMask(), i_29_) ^ 0xffffffff) != -1) {
				if (modelRenderer != null) {
					i_29_ = toolkit.mergeFunctionMask(i_29_, modelRenderer.functionMask());
				}
				BaseModel model = Class98_Sub6.fromFile(0, -9252, Class340.aClass207_2847, defaultMediaId);
				if (model == null) {
					MapRegion.aBoolean3503 = true;
					return null;
				}
				if (model.version < 13) {
					model.scaleLog2(13746, 2);
				}
				modelRenderer = toolkit.createModelRenderer(model, i_29_, Class76_Sub10.anInt3794, 64, 768);
				Class64_Sub5.aClass79_3650.put(l, modelRenderer, (byte) -80);
			}
			if (animDefinition != null) {
				modelRenderer = animDefinition.method937(i_28_, i_31_, i_29_, 121, modelRenderer, i_30_);
			}
			modelRenderer.updateFunctionMask(i_33_);
			return modelRenderer;
		}
		if ((defaultMediaType ^ 0xffffffff) == -3) {
			ModelRenderer class146 = npcParser.get(5, defaultMediaId).method2299(animDefinition, false, interface6, i_31_, i_30_, animParser, i_29_, toolkit, class40, i_28_);
			if (class146 == null) {
				MapRegion.aBoolean3503 = true;
				return null;
			}
			return class146;
		}
		if (defaultMediaType == 3) {
			if (appearence == null) {
				return null;
			}
			ModelRenderer class146 = appearence.method3624((byte) 107, i_31_, identikitParser, itemParser, toolkit, i_30_, interface6, animDefinition, npcParser, i_28_, animParser, i_29_);
			if (class146 == null) {
				MapRegion.aBoolean3503 = true;
				return null;
			}
			return class146;
		}
		if (defaultMediaType == 4) {
			ItemDefinition class297 = itemParser.get(defaultMediaId, (byte) -117);
			ModelRenderer class146 = class297.method3501(i_31_, i_29_, i_30_, animDefinition, i_28_, toolkit, 10, 128, appearence);
			if (class146 == null) {
				MapRegion.aBoolean3503 = true;
				return null;
			}
			return class146;
		}
		if (defaultMediaType == 6) {
			ModelRenderer class146 = npcParser.get(5, defaultMediaId).method2301(i_30_, 0, null, 0, (byte) 120, animDefinition, null, toolkit, 0, i_28_, interface6, class40, i_31_, renderParser, i_29_, 0, animParser, null);
			if (class146 == null) {
				MapRegion.aBoolean3503 = true;
				return null;
			}
			return class146;
		}
		if (defaultMediaType == 7) {
			if (appearence == null) {
				return null;
			}
			int i_34_ = defaultMediaId >>> -1684107024;
			int i_35_ = defaultMediaId & 0xffff;
			int i_36_ = anInt2210;
			ModelRenderer class146 = appearence.method3626(i_30_, animDefinition, i_31_, i_36_, i_34_, i_35_, i_28_, i_29_, identikitParser, toolkit, 256, animParser);
			if (class146 == null) {
				MapRegion.aBoolean3503 = true;
				return null;
			}
			return class146;
		}
		return null;
	}

	public final void method3455(int i, int i_0_, int i_1_) {
		try {
			if (params == null) {
				params = new HashTable(16);
				params.put(new NodeInteger(i_0_), i, -1);
			} else if (i_1_ == 16) {
				NodeInteger class98_sub34 = (NodeInteger) params.get(i, -1);
				if (class98_sub34 != null) {
					class98_sub34.value = i_0_;
				} else {
					params.put(new NodeInteger(i_0_), i, i_1_ ^ ~0x10);
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sba.B(" + i + ',' + i_0_ + ',' + i_1_ + ')');
		}
	}

	private final Object[] method3462(RSByteBuffer class98_sub22, int i) {
		try {
			int i_37_ = class98_sub22.readUnsignedByte((byte) -123);
			if ((i_37_ ^ 0xffffffff) == -1) {
				return null;
			}
			Object[] objects = new Object[i_37_];
			for (int i_38_ = i; (i_38_ ^ 0xffffffff) > (i_37_ ^ 0xffffffff); i_38_++) {
				int i_39_ = class98_sub22.readUnsignedByte((byte) -125);
				if (i_39_ != 0) {
					if (i_39_ == 1) {
						objects[i_38_] = class98_sub22.readString((byte) 84);
					}
				} else {
					objects[i_38_] = new Integer(class98_sub22.readInt(-2));
				}
			}
			aBoolean2209 = true;
			return objects;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sba.M(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final String method3463(int i, int i_40_, String string) {
		try {
			if (i_40_ != 700) {
				filled = false;
			}
			if (params == null) {
				return string;
			}
			NodeString class98_sub15 = (NodeString) params.get(i, -1);
			if (class98_sub15 == null) {
				return string;
			}
			return class98_sub15.value;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sba.R(" + i + ',' + i_40_ + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3464(boolean bool, RSToolkit var_ha, int i, ModelRenderer class146, Matrix class111) {
		do {
			try {
				class146.method2343(class111);
				Class87[] class87s = class146.method2320();
				if (bool == true) {
					Class35[] class35s = class146.method2322();
					if ((aClass246_Sub5_2301 == null || aClass246_Sub5_2301.aBoolean5099) && (class87s != null || class35s != null)) {
						aClass246_Sub5_2301 = Class246_Sub5.method3117(i, false);
					}
					if (aClass246_Sub5_2301 == null) {
						break;
					}
					aClass246_Sub5_2301.method3120(var_ha, i, class87s, class35s, false);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "sba.A(" + bool + ',' + (var_ha != null ? "{...}" : "null") + ',' + i + ',' + (class146 != null ? "{...}" : "null") + ',' + (class111 != null ? "{...}" : "null") + ')');
			}
			break;
		} while (false);
	}

	public final Class346 method3467(int i, SunDefinitionParser class115, SkyboxDefinitionParser skyboxParser) {
		if (i == (objId ^ 0xffffffff)) {
			return null;
		}
		long l = (0xffffL & anInt2306) << -1299272208 | (long) anInt2260 << 1934691488 & 65535L << -565839264 | 65535L << 1514706192 & (long) anInt2334 << 1170488080 | objId & 0xffffL;
		Class346 class346 = (Class346) Class151_Sub7.aClass79_5004.get(i + -123, l);
		if (class346 == null) {
			class346 = skyboxParser.method528(0, objId, anInt2260, anInt2306, class115, anInt2334);
			Class151_Sub7.aClass79_5004.put(l, class346, (byte) -80);
		}
		return class346;
	}

	public final int method3472(int dummy, int i_65_, int i_66_) {
		if (params == null) {
			return i_65_;
		}
		NodeInteger nodeInteger = (NodeInteger) params.get(i_66_, -1);
		if (nodeInteger == null) {
			return i_65_;
		}
		return nodeInteger.value;
	}

	private final int[] method3473(RSByteBuffer class98_sub22, int i) {
		try {
			int i_67_ = class98_sub22.readUnsignedByte((byte) -104);
			if (i_67_ == 0) {
				return null;
			}
			int[] is = new int[i_67_];
			for (int i_68_ = i; i_68_ < i_67_; i_68_++) {
				is[i_68_] = class98_sub22.readInt(-2);
			}
			return is;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sba.J(" + (class98_sub22 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final void method3474(int i, int i_69_, int i_70_) {
		try {
			if (anIntArray2326 == null || (i_69_ ^ 0xffffffff) <= (anIntArray2326.length ^ 0xffffffff)) {
				int[] is = new int[i_69_ + 1];
				if (anIntArray2326 != null) {
					for (int i_71_ = 0; anIntArray2326.length > i_71_; i_71_++) {
						is[i_71_] = anIntArray2326[i_71_];
					}
					for (int i_72_ = anIntArray2326.length; (i_72_ ^ 0xffffffff) > (i_69_ ^ 0xffffffff); i_72_++) {
						is[i_72_] = -1;
					}
				}
				anIntArray2326 = is;
			}
			if (i != -17972) {
				imageRepeat = false;
			}
			anIntArray2326[i_69_] = i_70_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sba.E(" + i + ',' + i_69_ + ',' + i_70_ + ')');
		}
	}

	public final void removeEventHandlers(int dummy) {
		anObjectArray2316 = null;
		anObjectArray2215 = null;
		anObjectArray2292 = null;
		mouseExitHandler = null;
		inventoryUpdateHandler = null;
		anObjectArray2340 = null;
		anIntArray2297 = null;
		anIntArray2249 = null;
		anObjectArray2320 = null;
		anObjectArray2277 = null;
		anObjectArray2266 = null;
		mousePressedHandler = null;
		anObjectArray2220 = null;
		anObjectArray2274 = null;
		anObjectArray2278 = null;
		anObjectArray2313 = null;
		anObjectArray2324 = null;
		mouseDraggedHandler = null;
		mouseReleasedHandler = null;
		anObjectArray2253 = null;
		anIntArray2342 = null;
		mouseMotionHandler = null;
		anIntArray2271 = null;
		anObjectArray2294 = null;
		chatboxUpdateHandler = null;
		anIntArray2284 = null;
		anObjectArray2269 = null;
		updateHandler = null;
		anObjectArray2332 = null;
		param = null;
		anObjectArray2257 = null;
		anObjectArray2330 = null;
		mouseEnterHandler = null;
		mouseDragPassHandler = null;
		anObjectArray2212 = null;
		anObjectArray2319 = null;
	}

	public final void removeParam(byte dummy, int id) {
		if (params != null) {
			Node node = params.get(id, -1);
			if (node != null) {
				node.unlink(120);
			}
		}
	}

	public final void setMenuAction(String action, int id, int dummy) {
		if (menuActions == null || id >= menuActions.length) {
			String[] actions = new String[1 + id];
			if (menuActions != null) {
				for (int ptr = 0; (ptr ^ 0xffffffff) > (menuActions.length ^ 0xffffffff); ptr++) {
					actions[ptr] = menuActions[ptr];
				}
			}
			menuActions = actions;
		}
		menuActions[id] = action;
	}

	public final void setParam(String string, int i, int id) {
		if (params == null) {
			params = new HashTable(16);
			params.put(new NodeString(string), id, i ^ ~0x10);
		} else {
			NodeString nodeString = (NodeString) params.get(id, i ^ ~0x10);
			if (nodeString == null) {
				params.put(new NodeString(string), id, i ^ ~0x10);
			} else {
				nodeString.value = string;
			}
		}
	}
}

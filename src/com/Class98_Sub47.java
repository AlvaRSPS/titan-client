/* Class98_Sub47 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.BuildAreaPreferenceField;

public final class Class98_Sub47 extends Node {
	public static Sprite			aClass332_4273;
	public static HashTable			groupMap		= new HashTable(16);
	public static IncomingOpcode	aClass58_4270	= new IncomingOpcode(118, 1);
	public static int				anInt4276		= 0;

	public static final void method1658(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		if (i_3_ == i_1_) {
			BuildAreaPreferenceField.method620(i, i_1_, true, i_0_, i_4_);
		} else {
			if ((Class76_Sub8.anInt3778 ^ 0xffffffff) >= (-i_1_ + i_0_ ^ 0xffffffff) && i_0_ + i_1_ <= Class3.anInt77 && -i_3_ + i >= Class98_Sub10_Sub38.anInt5753 && (SceneGraphNodeList.anInt1635 ^ 0xffffffff) <= (i + i_3_ ^ 0xffffffff)) {
				Class284.method3362(i, i_1_, i_4_, (byte) -119, i_0_, i_3_);
			} else {
				Class40.method364(i_3_, i_0_, i_1_, i, i_2_ + -17066, i_4_);
			}
		}
	}

	boolean		aBoolean4275	= false;
	int			anInt4266;
	int			anInt4267;
	public int	anInt4268		= -1;
	int			anInt4269;

	int			anInt4271;

	int			anInt4272;

	Class98_Sub47(int i) {
		try {
			// i = sprite id or something, yet to figure it out lelele
			anInt4268 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "uca.<init>(" + i + ')');
		}
	}
}


/* Class78 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;

import com.jagex.core.timetools.general.TimeTools;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.LightIntensityDefinition;
import com.jagex.game.toolkit.ground.Ground;

public final class Class78 {
	public static int		anInt596;
	public static int[]		anIntArray595	= { -1, -1, 1, 1 };
	public static int[]		anIntArray597	= new int[32];
	public static Ground[]	aSArray594;

	public static final void method790(int i, int i_0_, int i_1_, Class246_Sub3_Sub3 class246_sub3_sub3, Class246_Sub3_Sub3 class246_sub3_sub3_2_) {
		Class172 class172 = Class100.method1693(i, i_0_, i_1_);
		if (class172 != null) {
			class172.aClass246_Sub3_Sub3_1324 = class246_sub3_sub3;
			class172.aClass246_Sub3_Sub3_1333 = class246_sub3_sub3_2_;
			int i_3_ = aSArray594 == Class81.aSArray618 ? 1 : 0;
			if (class246_sub3_sub3.method2978(-124)) {
				if (class246_sub3_sub3.method2987(6540)) {
					class246_sub3_sub3.animator = Class359.aClass246_Sub3Array3056[i_3_];
					Class359.aClass246_Sub3Array3056[i_3_] = class246_sub3_sub3;
				} else {
					class246_sub3_sub3.animator = LightIntensityDefinition.aClass246_Sub3Array3198[i_3_];
					LightIntensityDefinition.aClass246_Sub3Array3198[i_3_] = class246_sub3_sub3;
					Class358.aBoolean3033 = true;
				}
			} else {
				class246_sub3_sub3.animator = Class130.aClass246_Sub3Array1029[i_3_];
				Class130.aClass246_Sub3Array1029[i_3_] = class246_sub3_sub3;
			}
			if (class246_sub3_sub3_2_ != null) {
				if (class246_sub3_sub3_2_.method2978(-126)) {
					if (class246_sub3_sub3_2_.method2987(6540)) {
						class246_sub3_sub3_2_.animator = Class359.aClass246_Sub3Array3056[i_3_];
						Class359.aClass246_Sub3Array3056[i_3_] = class246_sub3_sub3_2_;
					} else {
						class246_sub3_sub3_2_.animator = LightIntensityDefinition.aClass246_Sub3Array3198[i_3_];
						LightIntensityDefinition.aClass246_Sub3Array3198[i_3_] = class246_sub3_sub3_2_;
						Class358.aBoolean3033 = true;
					}
				} else {
					class246_sub3_sub3_2_.animator = Class130.aClass246_Sub3Array1029[i_3_];
					Class130.aClass246_Sub3Array1029[i_3_] = class246_sub3_sub3_2_;
				}
			}
		}
	}

	public static final void method791(byte i) {
		do {
			try {
				if (i == 102) {
					FileOnDisk class356 = null;
					try {
						SignLinkRequest class143 = GameShell.signLink.openPreferences("2", true, 21516);
						while ((class143.status ^ 0xffffffff) == -1) {
							TimeTools.sleep(0, 1L);
						}
						if (class143.status == 1) {
							class356 = (FileOnDisk) class143.result;
							byte[] is = new byte[(int) class356.method3878((byte) -60)];
							int i_4_;
							for (int i_5_ = 0; i_5_ < is.length; i_5_ += i_4_) {
								i_4_ = class356.method3879(-i_5_ + is.length, (byte) -26, i_5_, is);
								if (i_4_ == -1) {
									throw new IOException("EOF");
								}
							}
							SocketWrapper.method306(i ^ 0x270c, new RSByteBuffer(is));
						}
					} catch (Exception exception) {
						/* empty */
					}
					try {
						if (class356 == null) {
							break;
						}
						class356.close(true);
					} catch (Exception exception) {
						/* empty */
					}
					break;
				}
				break;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fba.B(" + i + ')');
			}
		} while (false);
	}

	public static void method792(int i) {
		do {
			try {
				anIntArray597 = null;
				aSArray594 = null;
				anIntArray595 = null;
				if (i == -17344) {
					break;
				}
				method792(-125);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "fba.A(" + i + ')');
			}
			break;
		} while (false);
	}
}

/* Class208 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.SkyboxDefinitionParser;
import com.jagex.game.client.preferences.Class64_Sub3;
import com.jagex.game.client.preferences.CpuUsagePreferenceField;
import com.jagex.game.toolkit.ground.Ground;
import com.jagex.game.toolkit.matrix.NativeMatrix;

public final class Class208 {
	public static int[]	HSL_TABLE;
	public static int[]	anIntArray1580	= new int[8];
	public static Js5	itemsJs5;

	public static final void method2767() {
		int i = 10;
		int i_0_ = 30;
		if (CpuUsagePreferenceField.anInt3702 != 0 && Class284_Sub1.font != null) {
			Class98_Sub10_Sub30.activeToolkit.getClip(Class98_Sub16.anIntArray3933);
			for (int i_1_ = 0; i_1_ < Ground.anIntArray2205.length; i_1_++) {
				Class98_Sub10_Sub30.activeToolkit.method1755(8479, Class98_Sub16.anIntArray3933[1], Ground.anIntArray2205[i_1_] + Class15.anIntArray182[i_1_], -256, Class98_Sub16.anIntArray3933[3] - Class98_Sub16.anIntArray3933[1]);
			}
			for (int i_2_ = 0; i_2_ < SkyboxDefinitionParser.activeOccludersCount; i_2_++) {
				Class155 class155 = Huffman.aClass155Array1611[i_2_];
				Class98_Sub10_Sub30.activeToolkit.H(class155.anIntArray1240[0], class155.anIntArray1237[0], class155.anIntArray1241[0], Class237.anIntArray1809);
				Class98_Sub10_Sub30.activeToolkit.H(class155.anIntArray1240[1], class155.anIntArray1237[1], class155.anIntArray1241[1], Class314.anIntArray2691);
				Class98_Sub10_Sub30.activeToolkit.H(class155.anIntArray1240[2], class155.anIntArray1237[2], class155.anIntArray1241[2], Class98_Sub46_Sub6.anIntArray5980);
				Class98_Sub10_Sub30.activeToolkit.H(class155.anIntArray1240[3], class155.anIntArray1237[3], class155.anIntArray1241[3], Class262.anIntArray1962);
				if (Class237.anIntArray1809[2] != -1 && Class314.anIntArray2691[2] != -1 && Class98_Sub46_Sub6.anIntArray5980[2] != -1 && Class262.anIntArray1962[2] != -1) {
					int i_3_ = -65536;
					if (class155.aByte1242 == 4) {
						i_3_ = -16776961;
					}
					Class98_Sub10_Sub30.activeToolkit.method1789(Class237.anIntArray1809[1], i_3_, Class314.anIntArray2691[1], Class314.anIntArray2691[0], -10550, Class237.anIntArray1809[0]);
					Class98_Sub10_Sub30.activeToolkit.method1789(Class314.anIntArray2691[1], i_3_, Class98_Sub46_Sub6.anIntArray5980[1], Class98_Sub46_Sub6.anIntArray5980[0], -10550, Class314.anIntArray2691[0]);
					Class98_Sub10_Sub30.activeToolkit.method1789(Class98_Sub46_Sub6.anIntArray5980[1], i_3_, Class262.anIntArray1962[1], Class262.anIntArray1962[0], -10550, Class98_Sub46_Sub6.anIntArray5980[0]);
					Class98_Sub10_Sub30.activeToolkit.method1789(Class262.anIntArray1962[1], i_3_, Class237.anIntArray1809[1], Class237.anIntArray1809[0], -10550, Class262.anIntArray1962[0]);
					Class98_Sub10_Sub30.activeToolkit.method1789(Class237.anIntArray1809[1], i_3_, Class98_Sub46_Sub6.anIntArray5980[1], Class98_Sub46_Sub6.anIntArray5980[0], -10550, Class237.anIntArray1809[0]);
				}
			}
			Class284_Sub1.font.drawString((byte) -118, i_0_ + 45, "Dynamic: " + Class347.dynamicCount + "/" + 5000, -256, -16777216, i);
			Class284_Sub1.font.drawString((byte) 62, i_0_ + 60, "Total Opaque Onscreen: " + GameObjectDefinitionParser.opaqueOnscreenCount + "/" + 10000, -256, -16777216, i);
			Class284_Sub1.font.drawString((byte) 46, i_0_ + 75, "Total Trans Onscreen: " + Class353.transOnscreenCount + "/" + 5000, -256, -16777216, i);
			Class284_Sub1.font.drawString((byte) 78, i_0_ + 90, "Occluders: " + (Class21_Sub3.anInt5389 + Class336.anInt2820) + " Active: " + SkyboxDefinitionParser.activeOccludersCount, -256, -16777216, i);
			Class284_Sub1.font.drawString((byte) 84, i_0_ + 105, "Occluded: Ground:" + GameObjectDefinitionParser.groundOccludedCount + " Walls: " + Class98_Sub16.wallsOccludedCount + " CPs: " + Class151_Sub7.cpsOccludedCount + " Pixels: " + Class4.pixelsOccludedCount, -256, -16777216, i);
			Class284_Sub1.font.drawString((byte) 78, i_0_ + 120, "Occlude Calc Took: " + Class249.occludeCalculationTime / 1000L + "us", -256, -16777216, i);
			if (CpuUsagePreferenceField.anInt3702 == 2 && NativeMatrix.anIntArray4707 != null) {
				for (int i_4_ = 0; i_4_ < NativeMatrix.anIntArray4707.length; i_4_++) {
					float f = NativeMatrix.anIntArray4707[i_4_];
					f /= 4194304.0F;
					if (f > 1.0F) {
						f = 1.0F;
					}
					f *= 255.0F;
					f = 255.0F - f;
					int i_5_ = (int) f;
					NativeMatrix.anIntArray4707[i_4_] = i_5_ | i_5_ << 8 | i_5_ << 16 | ~0xffffff;
				}
				Sprite class332 = Class98_Sub10_Sub30.activeToolkit.createSprite(-7962, 0, Class64_Sub3.anInt3646, IncomingOpcode.anInt461, NativeMatrix.anIntArray4707, Class64_Sub3.anInt3646);
				class332.draw(i, 170, 1, 0, 0);
			}
		}
	}

}

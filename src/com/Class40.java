/* Class40 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;
import com.jagex.game.client.preferences.GroundBlendingPreferenceField;

public final class Class40 {
	public static IncomingOpcode	aClass58_369	= new IncomingOpcode(11, 28);
	public static int[][]			anIntArrayArray367;

	public static final void calculateLayout(int i, boolean bool) {
		Class378.calculateLayout(GameShell.frameWidth, client.topLevelInterfaceId, -1, bool, GameShell.frameHeight);
	}

	public static final void method360(byte i) {
		do {
			try {
				Class98_Sub10_Sub2.method1009(-63);
				Class128.method2224(22696);
				if (i == 79) {
					break;
				}
				anIntArrayArray367 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "cr.F(" + i + ')');
			}
			break;
		} while (false);
	}

	public static final void method361(int i, int i_1_) {
		try {
			Class98_Sub46_Sub17 class98_sub46_sub17 = Class185.method2628(i, -30, 16);
			class98_sub46_sub17.method1621(i_1_ + i_1_);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cr.E(" + i + ',' + i_1_ + ')');
		}
	}

	public static final void method362(byte i, int i_2_) {
		try {
			GroundBlendingPreferenceField.anInt3711 = 3;
			Class287.anInt2186 = 100;
			Class98_Sub5_Sub2.anInt5536 = i_2_;
			Class256.anInt1945 = -1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cr.C(" + i + ',' + i_2_ + ')');
		}
	}

	public static void method363(int i) {
		try {
			if (i >= -55) {
				anIntArrayArray367 = null;
			}
			anIntArrayArray367 = null;
			aClass58_369 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cr.D(" + i + ')');
		}
	}

	public static final void method364(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		try {
			int i_9_ = 0;
			int i_10_ = i;
			int i_11_ = i_5_ * i_5_;
			int i_13_ = i * i;
			int i_14_ = i_13_ << -124913215;
			int i_15_ = i_11_ << -1990005311;
			int i_16_ = i << -1924555039;
			int i_17_ = i_11_ * (1 + -i_16_) - -i_14_;
			int i_18_ = i_13_ - i_15_ * (i_16_ + -1);
			int i_19_ = i_11_ << 1567622306;
			int i_20_ = i_13_ << 2060901122;
			int i_21_ = ((i_9_ << -27167295) + 3) * i_14_;
			int i_22_ = i_15_ * (-3 + (i_10_ << 500964065));
			int i_23_ = (1 + i_9_) * i_20_;
			if (Class98_Sub10_Sub38.anInt5753 <= i_6_ && (SceneGraphNodeList.anInt1635 ^ 0xffffffff) <= (i_6_ ^ 0xffffffff)) {
				int i_24_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_4_ + i_5_);
				int i_25_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, -i_5_ + i_4_);
				Class333.method3761(i_8_, AnimationDefinition.anIntArrayArray814[i_6_], i_25_, i_24_, (byte) -125);
			}
			int i_26_ = (-1 + i_10_) * i_19_;
			while (i_10_ > 0) {
				if ((i_17_ ^ 0xffffffff) > -1) {
					while (i_17_ < 0) {
						i_18_ += i_23_;
						i_17_ += i_21_;
						i_21_ += i_20_;
						i_9_++;
						i_23_ += i_20_;
					}
				}
				if (i_18_ < 0) {
					i_18_ += i_23_;
					i_17_ += i_21_;
					i_21_ += i_20_;
					i_9_++;
					i_23_ += i_20_;
				}
				i_17_ += -i_26_;
				i_18_ += -i_22_;
				i_26_ -= i_19_;
				i_22_ -= i_19_;
				i_10_--;
				int i_27_ = i_6_ - i_10_;
				int i_28_ = i_10_ + i_6_;
				if ((i_28_ ^ 0xffffffff) <= (Class98_Sub10_Sub38.anInt5753 ^ 0xffffffff) && (i_27_ ^ 0xffffffff) >= (SceneGraphNodeList.anInt1635 ^ 0xffffffff)) {
					int i_29_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, i_4_ + i_9_);
					int i_30_ = GZipDecompressor.method3219(false, Class3.anInt77, Class76_Sub8.anInt3778, -i_9_ + i_4_);
					if ((i_27_ ^ 0xffffffff) <= (Class98_Sub10_Sub38.anInt5753 ^ 0xffffffff)) {
						Class333.method3761(i_8_, AnimationDefinition.anIntArrayArray814[i_27_], i_30_, i_29_, (byte) 90);
					}
					if (SceneGraphNodeList.anInt1635 >= i_28_) {
						Class333.method3761(i_8_, AnimationDefinition.anIntArrayArray814[i_28_], i_30_, i_29_, (byte) 2);
					}
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "cr.A(" + i + ',' + i_4_ + ',' + i_5_ + ',' + i_6_ + ',' + i_7_ + ',' + i_8_ + ')');
		}
	}

	public long		aLong365;

	public int[]	anIntArray366;

	public short[]	aShortArray368;

	public short[]	aShortArray370;

	protected Class40() {
		/* empty */
	}

	Class40(long l, int[] is, short[] is_31_, short[] is_32_) {
		aShortArray368 = is_32_;
		aLong365 = l;
		aShortArray370 = is_31_;
		anIntArray366 = is;
	}
}

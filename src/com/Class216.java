/* Class216 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Manager;
import com.jagex.game.client.definition.parser.WorldMapInfoDefinitionParser;
import com.jagex.game.client.preferences.GroundDecorationPreferenceField;
import com.jagex.game.client.preferences.ScreenSizePreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.ScalingSpriteLSEConfig;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class216 {
	public static WorldMapInfoDefinitionParser worldMapInfoDefinitionList;

	public static final boolean method2793(int i, byte i_0_, int i_1_) {
		return Class161.method2514(i_1_, 16, i) & OpenGlElementBufferPointer.method3672(i, i_1_, 2048);
	}

	public static final void method2794(byte i) {
		if (i <= -74) {
			for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > (Class150.npcCount ^ 0xffffffff); i_2_++) {
				int i_3_ = Orientation.npcIndices[i_2_];
				NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(i_3_, -1);
				if (class98_sub39 != null) {
					NPC class246_sub3_sub4_sub2_sub1 = class98_sub39.npc;
					Class98_Sub10_Sub10.method1038(class246_sub3_sub4_sub2_sub1, class246_sub3_sub4_sub2_sub1.definition.boundSize, -12212);
				}
			}
		}
	}

	public static final void method2796(int i, int i_5_, int i_6_, int i_7_, byte i_8_) {
		while_131_: do {
			int i_10_ = Class98_Sub10_Sub5.anInt5554;
			while_130_: do {
				do {
					if (i_10_ != 0) {
						if ((i_10_ ^ 0xffffffff) == -2) {
							break;
						}
						if ((i_10_ ^ 0xffffffff) != -3) {
							break while_131_;
						}
						if (!GameShell.cleanedStatics) {
							break while_130_;
						}
					}
					return;
				} while (false);
				Class98_Sub10_Sub5.anInt5554 = 2;
				ScreenSizePreferenceField.anInt3716 = i_5_;
				Class82.anInt629 = i;
				ReflectionRequest.anInt3956 = i_6_;
				GameShell.anInt2 = i_7_;
				return;
			} while (false);
			if (i_6_ > ReflectionRequest.anInt3956) {
				ReflectionRequest.anInt3956 = i_6_;
			}
			if ((i ^ 0xffffffff) < (Class82.anInt629 ^ 0xffffffff)) {
				Class82.anInt629 = i;
			}
			if (i_7_ < GameShell.anInt2) {
				GameShell.anInt2 = i_7_;
			}
			if ((ScreenSizePreferenceField.anInt3716 ^ 0xffffffff) < (i_5_ ^ 0xffffffff)) {
				ScreenSizePreferenceField.anInt3716 = i_5_;
			}
			break;
		} while (false);
	}

	public static final void method2797(int i, int i_11_, int i_12_, int i_13_) {
		if ((i ^ 0xffffffff) != (Class224_Sub2_Sub1.anInt6141 ^ 0xffffffff) || (Js5Manager.anInt926 ^ 0xffffffff) != (i_12_ ^ 0xffffffff) || (aa_Sub1.anInt3558 ^ 0xffffffff) != (i_11_ ^ 0xffffffff)) {
			Class224_Sub2_Sub1.anInt6141 = i;
			Js5Manager.anInt926 = i_12_;
			Class358.aBoolean3033 = true;
			aa_Sub1.anInt3558 = i_11_;
			double d = -(2 * i * 3.141592653589793) / 16384.0;
			double d_14_ = -(i_12_ * 2 * 3.141592653589793) / 16384.0;
			double d_15_ = Math.cos(d_14_);
			double d_16_ = Math.sin(d_14_);
			double d_17_ = Math.cos(d);
			double d_18_ = Math.sin(d);
			Class224_Sub3.aDouble5038 = -d_16_ * d_17_;
			Class98_Sub5_Sub2.aDouble5537 = d_18_;
			GroundDecorationPreferenceField.aDouble3669 = d_16_ * d_18_;
			ScalingSpriteLSEConfig.aDouble3543 = d_15_ * d_17_;
			GameDefinition.aDouble2100 = 0.0;
			if (i_13_ == 25980) {
				Class76_Sub5.aDouble3747 = d_16_;
				Class98_Sub10_Sub25.aDouble5675 = d_17_;
				OpenGLHeap.aDouble6081 = d_15_;
				Class283.aDouble2145 = d_18_ * -d_15_;
			}
		}
	}

	int	anInt1617;
	int	anInt1618;
	int	anInt1619;
	int	anInt1620;
	int	anInt1621;
	int	anInt1623;
	int	anInt1624;

	int	anInt1625;

	int	anInt1626;

	int	anInt1627;

	int	anInt1628;

	int	anInt1629;

	public Class216() {
		/* empty */
	}

	public final boolean method2795(Class216 class216_4_, boolean bool) {
		return (anInt1617 ^ 0xffffffff) == (class216_4_.anInt1617 ^ 0xffffffff) && (anInt1618 ^ 0xffffffff) == (class216_4_.anInt1618 ^ 0xffffffff) && anInt1621 == class216_4_.anInt1621;
	}
}

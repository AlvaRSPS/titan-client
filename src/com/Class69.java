
/* Class69 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.BConfigDefinition;
import com.jagex.game.client.definition.EnumDefinition;
import com.jagex.game.client.definition.LightIntensityDefinition;
import com.jagex.game.client.definition.RenderAnimDefinition;
import com.jagex.game.client.definition.VarPlayerDefinition;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.preferences.CharacterShadowsPreferenceField;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.quickchat.QuickChatCategory;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.toolkit.matrix.Matrix;
import com.jagex.game.toolkit.shadow.NativeShadow;

import jaclib.memory.Buffer;
import jaclib.memory.Source;
import jaclib.memory.heap.NativeHeapBuffer;
import jaggl.MapBuffer;
import jaggl.OpenGL;

public abstract class Class69 implements Interface2 {

	public static int[]		anIntArray3220	= new int[4096];
	public static int[]		anIntArray3222	= new int[200];
	public static boolean	orthoCameraLock	= true;

	public static final int method696(float f, float f_0_, byte i, float f_1_) {
		try {
			float f_2_ = !(f < 0.0F) ? f : -f;
			float f_3_ = f_1_ < 0.0F ? -f_1_ : f_1_;
			float f_4_ = !(f_0_ < 0.0F) ? f_0_ : -f_0_;
			if (!(f_3_ > f_2_) || !(f_4_ < f_3_)) {
				if (!(f_2_ < f_4_) || !(f_3_ < f_4_)) {
					if (!(f > 0.0F)) {
						return 5;
					}
					return 4;
				}
				if (f_0_ > 0.0F) {
					return 2;
				}
				return 3;
			}
			if (f_1_ > 0.0F) {
				return 0;
			}
			return 1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.B(" + f + ',' + f_0_ + ',' + i + ',' + f_1_ + ')');
		}
	}

	public static final void method700(boolean bool, byte[][][] is, int i, byte i_7_, int i_8_, int i_9_, boolean bool_10_) {
		int i_11_ = bool ? 1 : 0;
		GameObjectDefinitionParser.opaqueOnscreenCount = 0;
		Class353.transOnscreenCount = 0;
		FileOnDisk.anInt3020++;
		if ((i_9_ & 0x2) == 0) {
			for (Char class246_sub3 = LightIntensityDefinition.aClass246_Sub3Array3198[i_11_]; class246_sub3 != null; class246_sub3 = class246_sub3.animator) {
				if (!DummyInputStream.method121(class246_sub3, bool, is, i, i_7_)) {
					Matrix.method2098(class246_sub3);
					if (class246_sub3.anInt5083 != -1) {
						FloorOverlayDefinitionParser.aClass246_Sub3Array307[GameObjectDefinitionParser.opaqueOnscreenCount++] = class246_sub3;
					}
				}
			}
		}
		if ((i_9_ & 0x1) == 0) {
			for (Char class246_sub3 = Class359.aClass246_Sub3Array3056[i_11_]; class246_sub3 != null; class246_sub3 = class246_sub3.animator) {
				if (!DummyInputStream.method121(class246_sub3, bool, is, i, i_7_)) {
					Matrix.method2098(class246_sub3);
					if (class246_sub3.anInt5083 != -1) {
						Class246_Sub4_Sub2.aClass246_Sub3Array6173[Class353.transOnscreenCount++] = class246_sub3;
					}
				}
			}
			for (Char class246_sub3 = Class130.aClass246_Sub3Array1029[i_11_]; class246_sub3 != null; class246_sub3 = class246_sub3.animator) {
				if (!DummyInputStream.method121(class246_sub3, bool, is, i, i_7_)) {
					if (class246_sub3.method2987(6540)) {
						Matrix.method2098(class246_sub3);
						if (class246_sub3.anInt5083 != -1) {
							Class246_Sub4_Sub2.aClass246_Sub3Array6173[Class353.transOnscreenCount++] = class246_sub3;
						}
					} else {
						Matrix.method2098(class246_sub3);
						if (class246_sub3.anInt5083 != -1) {
							FloorOverlayDefinitionParser.aClass246_Sub3Array307[GameObjectDefinitionParser.opaqueOnscreenCount++] = class246_sub3;
						}
					}
				}
			}
			if (!bool) {
				for (int i_12_ = 0; i_12_ < Class347.dynamicCount; i_12_++) {
					if (!DummyInputStream.method121(Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i_12_], bool, is, i, i_7_)) {
						Matrix.method2098(Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i_12_]);
						if (Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i_12_].anInt5083 != -1) {
							if (Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i_12_].method2987(6540)) {
								Class246_Sub4_Sub2.aClass246_Sub3Array6173[Class353.transOnscreenCount++] = Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i_12_];
							} else {
								FloorOverlayDefinitionParser.aClass246_Sub3Array307[GameObjectDefinitionParser.opaqueOnscreenCount++] = Class246_Sub3_Sub5_Sub2.aClass246_Sub3_Sub4Array6273[i_12_];
							}
						}
					}
				}
			}
		}
		if (GameObjectDefinitionParser.opaqueOnscreenCount > 0) {
			Class283.method3343(FloorOverlayDefinitionParser.aClass246_Sub3Array307, 0, GameObjectDefinitionParser.opaqueOnscreenCount - 1);
			for (int i_13_ = 0; i_13_ < GameObjectDefinitionParser.opaqueOnscreenCount; i_13_++) {
				VarPlayerDefinition.method2529(FloorOverlayDefinitionParser.aClass246_Sub3Array307[i_13_], true, bool_10_);
			}
		}
		if (QuickChatMessageType.aBoolean2914) {
			Class98_Sub10_Sub30.activeToolkit.method1818(0, null);
		}
		if ((i_9_ & 0x2) == 0) {
			for (int i_14_ = FloorOverlayDefinitionParser.anInt305; i_14_ < OpenGLTexture2DSource.anInt3103; i_14_++) {
				if (i_14_ >= i && is != null) {
					int i_15_ = Class74.aBooleanArrayArray551.length;
					if (EnumDefinition.anInt2561 + Class74.aBooleanArrayArray551.length > BConfigDefinition.anInt3112) {
						i_15_ -= EnumDefinition.anInt2561 + Class74.aBooleanArrayArray551.length - BConfigDefinition.anInt3112;
					}
					int i_16_ = Class74.aBooleanArrayArray551[0].length;
					if (OutgoingOpcode.anInt1318 + Class74.aBooleanArrayArray551[0].length > Class64_Sub9.anInt3662) {
						i_16_ -= OutgoingOpcode.anInt1318 + Class74.aBooleanArrayArray551[0].length - Class64_Sub9.anInt3662;
					}
					boolean[][] bools = Class319.aBooleanArrayArray2702;
					if (GameObjectDefinitionParser.aBoolean2526) {
						if (Class375.aBoolean3170) {
							bools = Class34.aBooleanArrayArrayArray325[i_14_];
						}
						for (int i_17_ = Class67.anInt521; i_17_ < i_15_; i_17_++) {
							int i_18_ = i_17_ + EnumDefinition.anInt2561 - Class67.anInt521;
							for (int i_19_ = OpenGlPointLight.anInt4184; i_19_ < i_16_; i_19_++) {
								bools[i_17_][i_19_] = false;
								if (Class74.aBooleanArrayArray551[i_17_][i_19_]) {
									int i_20_ = i_19_ + OutgoingOpcode.anInt1318 - OpenGlPointLight.anInt4184;
									for (int i_21_ = i_14_; i_21_ >= 0; i_21_--) {
										if (QuickChatCategory.aClass172ArrayArrayArray5948[i_21_][i_18_][i_20_] != null && QuickChatCategory.aClass172ArrayArrayArray5948[i_21_][i_18_][i_20_].aByte1322 == i_14_) {
											bools[i_17_][i_19_] = !(i_21_ >= i && is[i_21_][i_18_][i_20_] == i_7_ || Class76_Sub5.method758((byte) 95, i_14_, i_20_, i_18_));
											break;
										}
									}
								}
							}
						}
					}
					if (Class375.aBoolean3170) {
						if (i_8_ >= 0) {
							Class78.aSArray594[i_14_].method3416(0, 0, 0, null, false, i_8_, i_9_);
						} else {
							Class78.aSArray594[i_14_].method3426(0, 0, 0, null, false, i_9_);
						}
						for (int i_22_ = 0; i_22_ < RenderAnimDefinition.anInt2407; i_22_++) {
							Class98_Sub43_Sub3.aClass245Array5922[i_22_].method2961(true, new Class246_Sub10(i_14_ + 1));
						}
					} else if (i_8_ >= 0) {
						Class78.aSArray594[i_14_].method3416(Class241.anInt1845, CharacterShadowsPreferenceField.anInt3714, Class259.anInt1959, Class319.aBooleanArrayArray2702, false, i_8_, i_9_);
					} else {
						Class78.aSArray594[i_14_].method3426(Class241.anInt1845, CharacterShadowsPreferenceField.anInt3714, Class259.anInt1959, Class319.aBooleanArrayArray2702, false, i_9_);
					}
				} else {
					int i_23_ = Class74.aBooleanArrayArray551.length;
					if (EnumDefinition.anInt2561 + Class74.aBooleanArrayArray551.length > BConfigDefinition.anInt3112) {
						i_23_ -= EnumDefinition.anInt2561 + Class74.aBooleanArrayArray551.length - BConfigDefinition.anInt3112;
					}
					int i_24_ = Class74.aBooleanArrayArray551[0].length;
					if (OutgoingOpcode.anInt1318 + Class74.aBooleanArrayArray551[0].length > Class64_Sub9.anInt3662) {
						i_24_ -= OutgoingOpcode.anInt1318 + Class74.aBooleanArrayArray551[0].length - Class64_Sub9.anInt3662;
					}
					boolean[][] bools = Class319.aBooleanArrayArray2702;
					if (GameObjectDefinitionParser.aBoolean2526) {
						if (Class375.aBoolean3170) {
							bools = Class34.aBooleanArrayArrayArray325[i_14_];
						}
						for (int i_25_ = Class67.anInt521; i_25_ < i_23_; i_25_++) {
							int i_26_ = i_25_ + EnumDefinition.anInt2561 - Class67.anInt521;
							for (int i_27_ = OpenGlPointLight.anInt4184; i_27_ < i_24_; i_27_++) {
								bools[i_25_][i_27_] = Class74.aBooleanArrayArray551[i_25_][i_27_] && !Class76_Sub5.method758((byte) 110, i_14_, i_27_ + OutgoingOpcode.anInt1318 - OpenGlPointLight.anInt4184, i_26_);
							}
						}
					}
					if (Class375.aBoolean3170) {
						if (i_8_ >= 0) {
							Class78.aSArray594[i_14_].method3416(0, 0, 0, null, false, i_8_, i_9_);
						} else {
							Class78.aSArray594[i_14_].method3426(0, 0, 0, null, false, i_9_);
						}
						for (int i_28_ = 0; i_28_ < RenderAnimDefinition.anInt2407; i_28_++) {
							Class98_Sub43_Sub3.aClass245Array5922[i_28_].method2961(true, new Class246_Sub10(i_14_ + 1));
						}
					} else if (i_8_ >= 0) {
						Class78.aSArray594[i_14_].method3416(Class241.anInt1845, CharacterShadowsPreferenceField.anInt3714, Class259.anInt1959, Class319.aBooleanArrayArray2702, true, i_8_, i_9_);
					} else {
						Class78.aSArray594[i_14_].method3426(Class241.anInt1845, CharacterShadowsPreferenceField.anInt3714, Class259.anInt1959, Class319.aBooleanArrayArray2702, true, i_9_);
					}
				}
			}
		}
		if (Class353.transOnscreenCount > 0) {
			NativeShadow.method1656(Class246_Sub4_Sub2.aClass246_Sub3Array6173, 0, Class353.transOnscreenCount - 1);
			for (int i_29_ = 0; i_29_ < Class353.transOnscreenCount; i_29_++) {
				VarPlayerDefinition.method2529(Class246_Sub4_Sub2.aClass246_Sub3Array6173[i_29_], true, bool_10_);
			}
		}
	}

	public static final void method701(int i) {
		if (i == 0) {
			if (RenderAnimDefinition.anInt2407 == 2) {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[0]);
				Class98_Sub46_Sub5.aClass174Array5970[1].method2564(Class98_Sub43_Sub3.aClass245Array5922[1]);
			} else if (RenderAnimDefinition.anInt2407 == 3) {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[0]);
				Class98_Sub46_Sub5.aClass174Array5970[1].method2564(Class98_Sub43_Sub3.aClass245Array5922[1]);
				Class98_Sub46_Sub5.aClass174Array5970[2].method2564(Class98_Sub43_Sub3.aClass245Array5922[2]);
			} else {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[0]);
				Class98_Sub46_Sub5.aClass174Array5970[1].method2564(Class98_Sub43_Sub3.aClass245Array5922[1]);
				Class98_Sub46_Sub5.aClass174Array5970[2].method2564(Class98_Sub43_Sub3.aClass245Array5922[2]);
				Class98_Sub46_Sub5.aClass174Array5970[3].method2564(Class98_Sub43_Sub3.aClass245Array5922[3]);
			}
		} else if (i == 1) {
			if (RenderAnimDefinition.anInt2407 == 2) {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[2]);
			} else if (RenderAnimDefinition.anInt2407 == 3) {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[3]);
				Class98_Sub46_Sub5.aClass174Array5970[1].method2564(Class98_Sub43_Sub3.aClass245Array5922[4]);
			} else {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[4]);
				Class98_Sub46_Sub5.aClass174Array5970[1].method2564(Class98_Sub43_Sub3.aClass245Array5922[5]);
				Class98_Sub46_Sub5.aClass174Array5970[2].method2564(Class98_Sub43_Sub3.aClass245Array5922[6]);
			}
		} else if (i == 2) {
			if (RenderAnimDefinition.anInt2407 == 2) {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[3]);
			} else if (RenderAnimDefinition.anInt2407 == 3) {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[5]);
			} else {
				Class98_Sub46_Sub5.aClass174Array5970[0].method2564(Class98_Sub43_Sub3.aClass245Array5922[7]);
			}
		}
	}

	private boolean				aBoolean3221;
	OpenGLXToolkit				aHa_Sub3_Sub2_3217;
	private NativeHeapBuffer	aNativeHeapBuffer3215;
	private int					anInt3212;

	private int					anInt3214;

	private int					anInt3216	= 0;

	private int					anInt3218;

	private int					anInt3219	= -1;

	Class69(OpenGLXToolkit var_ha_Sub3_Sub2, int i, boolean bool) {
		try {
			aBoolean3221 = bool;
			aHa_Sub3_Sub2_3217 = var_ha_Sub3_Sub2;
			anInt3212 = i;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.<init>(" + (var_ha_Sub3_Sub2 != null ? "{...}" : "null") + ',' + i + ',' + bool + ')');
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			method72(false);
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.finalize(" + ')');
		}
	}

	@Override
	public int method2(int i) {
		try {
			if (i != 200) {
				orthoCameraLock = true;
			}
			return anInt3218;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.E(" + i + ')');
		}
	}

	public final Buffer method694(boolean bool, MapBuffer mapbuffer, int i) {
		try {
			if (anInt3216 == 0) {
				method702((byte) -72);
				if ((anInt3219 ^ 0xffffffff) < -1) {
					OpenGL.glBindBufferARB(anInt3212, anInt3219);
					if (bool) {
						OpenGL.glBufferDataARBub(anInt3212, anInt3214, null, 0, aBoolean3221 ? 35040 : 35044);
						if (anInt3218 <= aHa_Sub3_Sub2_3217.aNativeHeapBuffer4521.c) {
							anInt3216 = 1;
							return aHa_Sub3_Sub2_3217.aNativeHeapBuffer4521;
						}
					}
					if (!mapbuffer.a() && mapbuffer.a(anInt3212, anInt3218, 35001)) {
						anInt3216 = 2;
						return mapbuffer;
					}
				} else {
					anInt3216 = 2;
					return aNativeHeapBuffer3215;
				}
			}
			if (i != -15793) {
				method698(-18);
			}
			return null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.M(" + bool + ',' + (mapbuffer != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	public final long method695(int i) {
		try {
			if (i != -30277) {
				aHa_Sub3_Sub2_3217 = null;
			}
			if ((anInt3219 ^ 0xffffffff) != -1) {
				return 0L;
			}
			return aNativeHeapBuffer3215.getAddress();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.H(" + i + ')');
		}
	}

	public final boolean method697(Source source, int i, int i_5_) {
		try {
			do {
				if (i_5_ > anInt3214) {
					method702((byte) -76);
					if ((anInt3219 ^ 0xffffffff) >= -1) {
						throw new RuntimeException("ARGH!");
					}
					OpenGL.glBindBufferARB(anInt3212, anInt3219);
					OpenGL.glBufferDataARBa(anInt3212, i_5_, source.getAddress(), aBoolean3221 ? 35040 : 35044);
					aHa_Sub3_Sub2_3217.anInt4538 += i_5_ - anInt3218;
					anInt3214 = i_5_;
					if (!GameShell.cleanedStatics) {
						break;
					}
				}
				if (anInt3219 <= 0) {
					throw new RuntimeException("ARGH!");
				}
				OpenGL.glBindBufferARB(anInt3212, anInt3219);
				OpenGL.glBufferSubDataARBa(anInt3212, 0, anInt3218, source.getAddress());
				aHa_Sub3_Sub2_3217.anInt4538 += -anInt3218 + i_5_;
			} while (false);
			anInt3218 = i_5_;
			if (i != 1) {
				return true;
			}
			return true;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.C(" + (source != null ? "{...}" : "null") + ',' + i + ',' + i_5_ + ')');
		}
	}

	public final void method698(int i) {
		do {
			try {
				if (i != 18569) {
					anInt3214 = -80;
				}
				if (!aHa_Sub3_Sub2_3217.aBoolean6137) {
					break;
				}
				OpenGL.glBindBufferARB(anInt3212, anInt3219);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ei.A(" + i + ')');
			}
			break;
		} while (false);
	}

	private final void method702(byte i) {
		try {
			if (i < -53 && anInt3219 < 0) {
				if (aHa_Sub3_Sub2_3217.aBoolean6137) {
					OpenGL.glGenBuffersARB(1, Class190.anIntArray1463, 0);
					anInt3219 = Class190.anIntArray1463[0];
					OpenGL.glBindBufferARB(anInt3212, anInt3219);
				} else {
					anInt3219 = 0;
				}
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.J(" + i + ')');
		}
	}

	public final boolean method703(byte i, MapBuffer mapbuffer) {
		try {
			boolean bool = true;
			if (i != -68) {
				method696(-2.5029087F, 1.1315566F, (byte) -51, -0.1177615F);
			}
			if (anInt3216 != 0) {
				do {
					if (anInt3219 > 0) {
						OpenGL.glBindBufferARB(anInt3212, anInt3219);
						if (anInt3216 != 1) {
							bool = mapbuffer.b();
							if (!GameShell.cleanedStatics) {
								break;
							}
						}
						OpenGL.glBufferSubDataARBa(anInt3212, 0, anInt3214, aHa_Sub3_Sub2_3217.aNativeHeapBuffer4521.getAddress());
					}
				} while (false);
				anInt3216 = 0;
			}
			return bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "ei.F(" + i + ',' + (mapbuffer != null ? "{...}" : "null") + ')');
		}
	}

	void method72(boolean bool) {
		do {
			try {
				if ((anInt3219 ^ 0xffffffff) < -1) {
					aHa_Sub3_Sub2_3217.method2084(1, anInt3218, anInt3219);
					anInt3219 = -1;
				}
				if (bool == false) {
					break;
				}
				method703((byte) -53, null);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ei.L(" + bool + ')');
			}
			break;
		} while (false);
	}

	void method76(int i, int i_6_) {
		do {
			try {
				if ((i ^ 0xffffffff) < (anInt3214 ^ 0xffffffff)) {
					method702((byte) -85);
					do {
						if ((anInt3219 ^ 0xffffffff) < -1) {
							OpenGL.glBindBufferARB(anInt3212, anInt3219);
							OpenGL.glBufferDataARBub(anInt3212, i, null, 0, !aBoolean3221 ? 35044 : 35040);
							aHa_Sub3_Sub2_3217.anInt4538 += -anInt3214 + i;
							if (!GameShell.cleanedStatics) {
								break;
							}
						}
						aNativeHeapBuffer3215 = aHa_Sub3_Sub2_3217.method1947(i, false, i_6_ ^ 0x512b);
					} while (false);
					anInt3214 = i;
				}
				anInt3218 = i;
				if (i_6_ == 20779) {
					break;
				}
				anIntArray3222 = null;
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "ei.G(" + i + ',' + i_6_ + ')');
			}
			break;
		} while (false);
	}
}

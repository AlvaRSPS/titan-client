/* Class201 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.Queue;
import com.jagex.core.collections.cache.AdvancedMemoryCache;
import com.jagex.core.collections.cache.StrongReferenceMCNode;
import com.jagex.game.client.archive.Js5Client;
import com.jagex.game.client.definition.GameObjectDefinition;
import com.jagex.game.client.definition.NPCDefinition;
import com.jagex.game.client.definition.parser.FloorOverlayDefinitionParser;
import com.jagex.game.client.definition.parser.LightIntensityDefinitionParser;
import com.jagex.game.client.definition.parser.ParamDefinitionParser;
import com.jagex.game.client.definition.parser.VarClientStringsDefinitionParser;
import com.jagex.game.client.preferences.AntialiasPreferenceField;
import com.jagex.game.client.preferences.Class64_Sub9;
import com.jagex.game.client.preferences.TexturesPreferenceField;
import com.jagex.game.client.preferences.ToolkitPreferenceField;
import com.jagex.game.client.ui.loading.impl.elements.config.ProgressBarLSEConfig;
import com.jagex.game.client.ui.loading.impl.elements.impl.NewsLoadingScreenElement;
import com.jagex.game.client.ui.loading.impl.elements.impl.TextLoadingScreenElement;
import com.jagex.game.constants.BuildType;
import com.jagex.game.toolkit.model.ModelRenderer;

public final class Minimap {
	public static int	anInt1544	= 0;
	public static int	cameraXPosition;

	public static final void clipPlanes(int i, boolean bool) {
		if (i == -546) {
			int i_41_ = Class246_Sub4_Sub2.anInt6184;
			int i_42_ = TexturesPreferenceField.anInt3696;
			if (bool && VarClientStringsDefinitionParser.aBoolean1839) {
				i_41_ <<= 1;
				i_42_ = -i_41_;
			}
			client.graphicsToolkit.setClipPlanes(i_42_, i_41_);
		}
	}

	public static final void renderCompass(int i, int x, int y, RtInterface interf) {
		RtInterfaceClip clip = interf.getClip(client.graphicsToolkit, i + -3999);
		if (clip != null && i == 4096) {
			client.graphicsToolkit.setClip(x, y, x - -interf.renderWidth, interf.renderHeight + y);
			if (Class333.anInt3386 < 3) {
				NewsLoadingScreenElement.aClass332_3471.method3739(x + interf.renderWidth / 2.0F, y + interf.renderHeight / 2.0F, 4096, ((int) -RsFloatBuffer.aFloat5794 & 0x3fff) << -1645068286, clip, x, y);
			} else {
				client.graphicsToolkit.fillImageClip(-16777216, clip, x, y);
			}
		}
	}

	public static final void renderMinimap(int dummy, RtInterface interf, RSToolkit toolkit, int gameScreenWidth, int gameScreenHeight) {
		RtInterfaceClip clip = interf.getClip(toolkit, dummy + 117);
		if (clip != null) {
			toolkit.setClip(gameScreenWidth, gameScreenHeight, interf.renderWidth + gameScreenWidth, gameScreenHeight + interf.renderHeight);
			if ((Class333.anInt3386 ^ 0xffffffff) == -3 || Class333.anInt3386 == 5 || Class64_Sub9.aClass332_3663 == null) {
				toolkit.fillImageClip(-16777216, clip, gameScreenWidth, gameScreenHeight);
			} else {
				int zoom;
				int xPos;
				int i_4_;
				int yPos;
				if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) == -5) {
					xPos = StrongReferenceMCNode.anInt6295;
					yPos = Js5Client.anInt1051;
					zoom = 4096;
					i_4_ = 0x3fff & (int) -RsFloatBuffer.aFloat5794;
				} else {
					zoom = -(16 * Class151.anInt1213) + 4096;
					xPos = Class87.localPlayer.boundExtentsX;
					i_4_ = 0x3fff & Class204.anInt1553 + (int) -RsFloatBuffer.aFloat5794;
					yPos = Class87.localPlayer.boundExtentsZ;
				}
				int i_6_ = -(2 * (-104 + Class165.mapWidth)) + xPos / 128 + 48;
				int i_7_ = -(yPos / 128) + 48 - (-(Class98_Sub10_Sub7.mapLength * 4) - -(2 * Class98_Sub10_Sub7.mapLength) - 208);
				Class64_Sub9.aClass332_3663.drawRotated(gameScreenWidth + interf.renderWidth / 2.0F, gameScreenHeight + interf.renderHeight / 2.0F, i_6_, i_7_, zoom, i_4_ << 1401446114, clip, gameScreenWidth, gameScreenHeight);
				for (NodeInteger node = (NodeInteger) ToolkitPreferenceField.aClass148_3659.getFirst(32); node != null; node = (NodeInteger) ToolkitPreferenceField.aClass148_3659.getNext(dummy ^ 0x64)) {
					int i_8_ = node.value;
					int i_9_ = (0x3fff & AntialiasPreferenceField.aClass370_3707.anIntArray3133[i_8_] >> -86148882) + -Class272.gameSceneBaseX;
					int i_10_ = -aa_Sub2.gameSceneBaseY + (AntialiasPreferenceField.aClass370_3707.anIntArray3133[i_8_] & 0x3fff);
					int i_11_ = -(xPos / 128) + 2 + 4 * i_9_;
					int i_12_ = 2 + i_10_ * 4 - yPos / 128;
					Class164.drawLocationSprite(i_11_, AntialiasPreferenceField.aClass370_3707.anIntArray3138[i_8_], gameScreenHeight, clip, toolkit, dummy ^ ~0x7997ff75, interf, gameScreenWidth, i_12_);
				}
				for (int index = 0; (index ^ 0xffffffff) > (Class98_Sub10_Sub7.locSpriteCount ^ 0xffffffff); index++) {
					int locSpriteX = -(xPos / 128) + 4 * TextLoadingScreenElement.anIntArray3451[index] + 2;
					int locSpriteY = 4 * BuildType.anIntArray91[index] + 2 + -(yPos / 128);
					GameObjectDefinition definition = Class130.gameObjectDefinitionList.get(AdvancedMemoryCache.anIntArray603[index], (byte) 119);
					if (definition.transformIDs != null) {
						definition = definition.get(StartupStage.varValues, (byte) -95);
						if (definition == null || definition.mapFunction == -1) {
							continue;
						}
					}
					Class164.drawLocationSprite(locSpriteX, definition.mapFunction, gameScreenHeight, clip, toolkit, -2040004466, interf, gameScreenWidth, locSpriteY);
				}
				for (ItemDeque deque = (ItemDeque) ModelRenderer.groundItems.startIteration(97); deque != null; deque = (ItemDeque) ModelRenderer.groundItems.iterateNext(-1)) {
					int i_16_ = (int) (0x3L & deque.hash >> 1749334492);
					if ((i_16_ ^ 0xffffffff) == (Canvas_Sub1.anInt26 ^ 0xffffffff)) {
						int i_17_ = (int) (0x3fffL & deque.hash) + -Class272.gameSceneBaseX;
						int i_18_ = (int) (0x3fffL & deque.hash >> 65890382) + -aa_Sub2.gameSceneBaseY;
						int i_19_ = -(xPos / 128) + 2 + 4 * i_17_;
						int i_20_ = -(yPos / 128) + 2 + 4 * i_18_;
						Class4.drawMapDot(gameScreenHeight, i_20_, gameScreenWidth, interf, clip, i_19_, (byte) -24, ProgressBarLSEConfig.mapDots[0]);
					}
				}
				for (int i_21_ = 0; Class150.npcCount > i_21_; i_21_++) {
					NodeObject linkable = (NodeObject) ProceduralTextureSource.npc.get(Orientation.npcIndices[i_21_], dummy + -5);
					if (linkable != null) {
						NPC npc = linkable.npc;
						if (npc.method3052((byte) 106) && (npc.plane ^ 0xffffffff) == (Class87.localPlayer.plane ^ 0xffffffff)) {
							NPCDefinition npcDefinition = npc.definition;
							if (npcDefinition != null && npcDefinition.anIntArray1109 != null) {
								npcDefinition = npcDefinition.method2300(StartupStage.varValues, (byte) 78);
							}
							if (npcDefinition != null && npcDefinition.drawMapDot && npcDefinition.visibleOnMinimap) {
								int x = npc.boundExtentsX / 128 - xPos / 128;
								int y = npc.boundExtentsZ / 128 + -(yPos / 128);
								if (npcDefinition.anInt1118 == -1) {
									Class4.drawMapDot(gameScreenHeight, y, gameScreenWidth, interf, clip, x, (byte) -24, ProgressBarLSEConfig.mapDots[1]);
								} else {
									Class164.drawLocationSprite(x, npcDefinition.anInt1118, gameScreenHeight, clip, toolkit, -2040004466, interf, gameScreenWidth, y);
								}
							}
						}
					}
				}
				int count = Class2.playerCount;
				int[] indices = Class319.playerIndices;
				for (int index = 0; (index ^ 0xffffffff) > (count ^ 0xffffffff); index++) {
					Player player = Class151_Sub9.players[indices[index]];
					if (player != null && player.hasAppearance((byte) 106) && !player.invisible && player != Class87.localPlayer && (Class87.localPlayer.plane ^ 0xffffffff) == (player.plane ^ 0xffffffff)) {
						int x = -(xPos / 128) + player.boundExtentsX / 128;
						int y = -(yPos / 128) + player.boundExtentsZ / 128;
						boolean isFriend = false;
						for (int friendIndex = 0; (Class314.friendListSize ^ 0xffffffff) < (friendIndex ^ 0xffffffff); friendIndex++) {
							if (player.accountName.equals(Class98_Sub25.friends[friendIndex]) && (GroundItem.friendsOnline[friendIndex] ^ 0xffffffff) != -1) {
								isFriend = true;
								break;
							}
						}
						boolean isFriendsChatMembers = false;
						for (int friendsChatIndex = 0; FloorOverlayDefinitionParser.clanChatSize > friendsChatIndex; friendsChatIndex++) {
							if (player.accountName.equals(WhirlpoolGenerator.clanChat[friendsChatIndex].accountName)) {
								isFriendsChatMembers = true;
								break;
							}
						}
						boolean isTeamMember = false;
						if (Class87.localPlayer.team != 0 && (player.team ^ 0xffffffff) != -1 && (Class87.localPlayer.team ^ 0xffffffff) == (player.team ^ 0xffffffff)) {
							isTeamMember = true;
						}
						if (player.clanmate) {
							Class4.drawMapDot(gameScreenHeight, y, gameScreenWidth, interf, clip, x, (byte) -24, ProgressBarLSEConfig.mapDots[6]);
						} else if (isFriend) {
							Class4.drawMapDot(gameScreenHeight, y, gameScreenWidth, interf, clip, x, (byte) -24, ProgressBarLSEConfig.mapDots[3]);
						} else if (isFriendsChatMembers) {
							Class4.drawMapDot(gameScreenHeight, y, gameScreenWidth, interf, clip, x, (byte) -24, ProgressBarLSEConfig.mapDots[5]);
						} else if (isTeamMember) {
							Class4.drawMapDot(gameScreenHeight, y, gameScreenWidth, interf, clip, x, (byte) -24, ProgressBarLSEConfig.mapDots[4]);
						} else {
							Class4.drawMapDot(gameScreenHeight, y, gameScreenWidth, interf, clip, x, (byte) -24, ProgressBarLSEConfig.mapDots[2]);
						}
					}
				}
				Class36[] class36s = OpenGlArrayBufferPointer.aClass36Array903;
				for (int i_32_ = 0; (i_32_ ^ 0xffffffff) > (class36s.length ^ 0xffffffff); i_32_++) {
					Class36 class36 = class36s[i_32_];
					if (class36 != null && class36.anInt346 != 0 && (Queue.timer % 20 ^ 0xffffffff) > -11) {
						if ((class36.anInt346 ^ 0xffffffff) == -2) {
							NodeObject class98_sub39 = (NodeObject) ProceduralTextureSource.npc.get(class36.anInt345, -1);
							if (class98_sub39 != null) {
								NPC npc = class98_sub39.npc;
								int x = npc.boundExtentsX / 128 - xPos / 128;
								int y = npc.boundExtentsZ / 128 - yPos / 128;
								Class98_Sub10_Sub29.method1090(class36.anInt341, clip, y, x, interf, gameScreenHeight, 360000L, gameScreenWidth, 4);
							}
						}
						if (class36.anInt346 == 2) {
							int i_35_ = -(xPos / 128) + class36.anInt338 / 128;
							int i_36_ = -(yPos / 128) + class36.anInt347 / 128;
							long l = class36.anInt340 << 546406215;
							l *= l;
							Class98_Sub10_Sub29.method1090(class36.anInt341, clip, i_36_, i_35_, interf, gameScreenHeight, l, gameScreenWidth, 4);
						}
						if (class36.anInt346 == 10 && (class36.anInt345 ^ 0xffffffff) <= -1 && class36.anInt345 < Class151_Sub9.players.length) {
							Player player = Class151_Sub9.players[class36.anInt345];
							if (player != null) {
								int i_37_ = player.boundExtentsX / 128 - xPos / 128;
								int i_38_ = -(yPos / 128) + player.boundExtentsZ / 128;
								Class98_Sub10_Sub29.method1090(class36.anInt341, clip, i_38_, i_37_, interf, gameScreenHeight, 360000L, gameScreenWidth, 4);
								System.out.println("b: " + class36.anInt346);

							}
						}
					}

				}
				if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) != -5) {
					if ((LightIntensityDefinitionParser.anInt2024 ^ 0xffffffff) != -1) {
						int i_39_ = -(xPos / 128) + 2 + 4 * LightIntensityDefinitionParser.anInt2024 + (Class87.localPlayer.getSize(0) + -1) * 2;
						int i_40_ = Class246_Sub3_Sub1_Sub2.anInt6251 * 4 + 2 - yPos / 128 - -(2 * (Class87.localPlayer.getSize(dummy ^ 0x4) - 1));
						Class4.drawMapDot(gameScreenHeight, i_40_, gameScreenWidth, interf, clip, i_39_, (byte) -24, Class340.mapFlag[ParamDefinitionParser.aBoolean3110 ? 1 : 0]);
					}
					if (!Class87.localPlayer.invisible) {
						toolkit.drawPlayerSquareDot(3, 3, -1 + interf.renderHeight / 2 + gameScreenHeight, -1, (byte) -66, gameScreenWidth - (-(interf.renderWidth / 2) - -1));
					}
				}
			}
		}
	}

}

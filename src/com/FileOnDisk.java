
/* Class356 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.AnimationDefinition;

public final class FileOnDisk {
	public static AnimationDefinition[]	aClass97Array3023	= new AnimationDefinition[14];
	public static int					anInt3018			= 100;
	public static int					anInt3020;
	public static int					anInt3025;

	public static void method3881(int i) {
		try {
			if (i != -14445) {
				method3881(107);
			}
			aClass97Array3023 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vl.D(" + i + ')');
		}
	}

	private File				aFile3022;
	private long				aLong3019;
	private long				aLong3024;

	private RandomAccessFile	aRandomAccessFile3021;

	FileOnDisk(File file, String string, long l) throws IOException {
		try {
			if (l == -1L) {
				l = 9223372036854775807L;
			}
			if ((file.length() ^ 0xffffffffffffffffL) < (l ^ 0xffffffffffffffffL)) {
				file.delete();
			}
			aRandomAccessFile3021 = new RandomAccessFile(file, string);
			aLong3024 = l;
			aLong3019 = 0L;
			aFile3022 = file;
			int i = aRandomAccessFile3021.read();
			if (i != -1 && !string.equals("r")) {
				aRandomAccessFile3021.seek(0L);
				aRandomAccessFile3021.write(i);
			}
			aRandomAccessFile3021.seek(0L);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vl.<init>(" + (file != null ? "{...}" : "null") + ',' + (string != null ? "{...}" : "null") + ',' + l + ')');
		}
	}

	public final void close(boolean bool) throws IOException {
		do {
			try {
				if (aRandomAccessFile3021 != null) {
					aRandomAccessFile3021.close();
					aRandomAccessFile3021 = null;
				}
				if (bool == true) {
					break;
				}
				method3876((byte) -68);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vl.E(" + bool + ')');
			}
			break;
		} while (false);
	}

	@Override
	protected final void finalize() throws Throwable {
		try {
			if (aRandomAccessFile3021 != null) {
				System.out.println("Warning! fileondisk " + aFile3022 + " not closed correctly using close(). Auto-closing instead. ");
				close(true);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vl.finalize(" + ')');
		}
	}

	public final File method3876(byte i) {
		try {
			if (i != 3) {
				return null;
			}
			return aFile3022;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vl.G(" + i + ')');
		}
	}

	public final void method3877(byte i, long l) throws IOException {
		try {
			aRandomAccessFile3021.seek(l);
			aLong3019 = l;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vl.A(" + i + ',' + l + ')');
		}
	}

	public final long method3878(byte i) throws IOException {
		try {
			if (i > -17) {
				method3876((byte) 94);
			}
			return aRandomAccessFile3021.length();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vl.F(" + i + ')');
		}
	}

	public final int method3879(int i, byte i_1_, int i_2_, byte[] is) throws IOException {
		try {
			if (i_1_ != -26) {
				aLong3024 = -32L;
			}
			int i_3_ = aRandomAccessFile3021.read(is, i_2_, i);
			if ((i_3_ ^ 0xffffffff) < -1) {
				aLong3019 += i_3_;
			}
			return i_3_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vl.C(" + i + ',' + i_1_ + ',' + i_2_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3882(byte[] is, int i, int i_4_, int i_5_) throws IOException {
		try {
			if ((i_5_ - -aLong3019 ^ 0xffffffffffffffffL) < (aLong3024 ^ 0xffffffffffffffffL)) {
				aRandomAccessFile3021.seek(aLong3024);
				aRandomAccessFile3021.write(1);
				throw new EOFException();
			}
			aRandomAccessFile3021.write(is, i_4_, i_5_);
			if (i != 4657) {
				method3876((byte) 49);
			}
			aLong3019 += i_5_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vl.B(" + (is != null ? "{...}" : "null") + ',' + i + ',' + i_4_ + ',' + i_5_ + ')');
		}
	}
}

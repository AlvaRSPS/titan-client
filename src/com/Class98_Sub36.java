/* Class98_Sub36 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.core.collections.Node;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.definition.SunDefinition;
import com.jagex.game.client.definition.parser.GameObjectDefinitionParser;
import com.jagex.game.client.definition.parser.HitmarksDefinitionParser;
import com.jagex.game.client.preferences.GamePreferences;
import com.jagex.game.client.quickchat.QuickChatMessageType;
import com.jagex.game.client.ui.loading.impl.elements.config.BackgroundColourLSEConfig;
import com.jagex.game.toolkit.ground.OpenGlGround;
import com.jagex.game.toolkit.heap.OpenGLHeap;

public final class Class98_Sub36 extends Node {
	public static QuickChatMessageType	aClass348_4156;
	public static long					aLong4159	= -1L;
	public static int					anInt4161;
	public static int[]					regionObjectMapId;

	static {
		aClass348_4156 = new QuickChatMessageType(7, 0, 1, 1);
		anInt4161 = 0;
	}

	public static void method1457(int i) {
		try {
			regionObjectMapId = null;
			if (i != -2496) {
				method1458(-13);
			}
			aClass348_4156 = null;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pm.A(" + i + ')');
		}
	}

	public static final void method1458(int i) {
		try {
			if (client.graphicsToolkit != null) {
				if (OpenGLHeap.aBoolean6079) {
					HashTableIterator.method540((byte) -51);
				}
				Class98_Sub10_Sub27.aClass84_5692.method833(0);
				PacketBufferManager.method2229();
				InputStream_Sub2.method124(i ^ 0x6068);
				Class48_Sub1_Sub2.method466(true);
				Class160.method2511(i ^ ~0x6545);
				Class39_Sub1.method355(i ^ 0x606a);
				if (OpenGlGround.aClass346_5202 != null) {
					OpenGlGround.aClass346_5202.method3827((byte) -87);
				}
				Class151_Sub7.method2466(-32346);
				Class98_Sub10_Sub15.method1050((byte) 114);
				Class176.method2582((byte) 99);
				SunDefinition.method3235((byte) -25);
				OpenGLHeightMapToNormalMapConverter.method2173(false, 123);
				for (int i_0_ = 0; i_0_ < 2048; i_0_++) {
					Player player = Class151_Sub9.players[i_0_];
					if (player != null) {
						for (int i_1_ = 0; player.aClass146Array6441.length > i_1_; i_1_++) {
							((Mob) player).aClass146Array6441[i_1_] = null;
						}
					}
				}
				for (int i_2_ = 0; (Class98_Sub10_Sub20.anInt5640 ^ 0xffffffff) < (i_2_ ^ 0xffffffff); i_2_++) {
					NPC class246_sub3_sub4_sub2_sub1 = BackgroundColourLSEConfig.aClass98_Sub39Array3516[i_2_].npc;
					if (class246_sub3_sub4_sub2_sub1 != null) {
						for (int i_3_ = 0; i_3_ < class246_sub3_sub4_sub2_sub1.aClass146Array6441.length; i_3_++) {
							((Mob) class246_sub3_sub4_sub2_sub1).aClass146Array6441[i_3_] = null;
						}
					}
				}
				Class76_Sub5.aClass111_3745 = null;
				SunDefinition.aClass111_1986 = null;
				client.graphicsToolkit.destroy(-1);
				client.graphicsToolkit = null;
			}
			if (i != -24580) {
				method1459(-123);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pm.B(" + i + ')');
		}
	}

	public static final void method1459(int i) {
		Class151_Sub9.anInt5028 = PacketParser.buffer.readByteA(true);
		int size = PacketParser.buffer.readByteA(true);
		int centreY = PacketParser.buffer.readShortA(i ^ ~0x3e777627);
		int centreX = PacketParser.buffer.readShort1((byte) -76);
		boolean force = PacketParser.buffer.readByteA(true) == 1;
		Class98_Sub10_Sub13.method1043((byte) -103);
		GamePreferences.setSize((byte) 8, size);
		PacketParser.buffer.startBitwiseAccess(0);
		for (int type = 0; (type ^ 0xffffffff) > -5; type++) {
			for (int x = 0; x < Class165.mapWidth >> -1040274845; x++) {
				for (int y = 0; y < Class98_Sub10_Sub7.mapLength >> -1712093149; y++) {
					int i_10_ = PacketParser.buffer.readBits((byte) -45, 1);
					if (i_10_ != 1) {
						Class170.regionData[type][x][y] = -1;
					} else {
						Class170.regionData[type][x][y] = PacketParser.buffer.readBits((byte) -86, 26);
					}
				}
			}
		}
		PacketParser.buffer.endBitwiseAccess((byte) 120);
		int i_11_ = (-PacketParser.buffer.position + Class65.currentPacketSize) / 16;
		Class98_Sub46_Sub17.regionXteaKey = new int[i_11_][4];
		for (int i_12_ = 0; i_11_ > i_12_; i_12_++) {
			for (int i_13_ = 0; i_13_ < 4; i_13_++) {
				Class98_Sub46_Sub17.regionXteaKey[i_12_][i_13_] = PacketParser.buffer.readInt(-2);
			}
		}
		Class76_Sub7.anIntArray3765 = null;
		if (i != -1048016408) {
			regionObjectMapId = null;
		}
		HashTable.aByteArrayArray3182 = new byte[i_11_][];
		Class105.regionNpcMapData = null;
		client.unknown = new byte[i_11_][];
		GameObjectDefinitionParser.regionUwTerrainId = new int[i_11_];
		HitmarksDefinitionParser.regionPositionHash = new int[i_11_];
		regionObjectMapId = new int[i_11_];
		Class287.regionTerrainId = new int[i_11_];
		HashTable.regionUwObjectMapId = new int[i_11_];
		Player.aByteArrayArray6533 = new byte[i_11_][];
		Class255.aByteArrayArray3211 = new byte[i_11_][];
		i_11_ = 0;
		for (int i_14_ = 0; i_14_ < 4; i_14_++) {
			for (int i_15_ = 0; Class165.mapWidth >> 1588990627 > i_15_; i_15_++) {
				for (int i_16_ = 0; Class98_Sub10_Sub7.mapLength >> 1681357539 > i_16_; i_16_++) {
					int i_17_ = Class170.regionData[i_14_][i_15_][i_16_];
					if ((i_17_ ^ 0xffffffff) != 0) {
						int i_18_ = (i_17_ & 0xffe88a) >> 1970993198;
						int i_19_ = 0x7ff & i_17_ >> -1128997053;
						int i_20_ = i_19_ / 8 + (i_18_ / 8 << -1281050872);
						for (int i_21_ = 0; (i_21_ ^ 0xffffffff) > (i_11_ ^ 0xffffffff); i_21_++) {
							if (i_20_ == HitmarksDefinitionParser.regionPositionHash[i_21_]) {
								i_20_ = -1;
								break;
							}
						}
						if ((i_20_ ^ 0xffffffff) != 0) {
							HitmarksDefinitionParser.regionPositionHash[i_11_] = i_20_;
							int i_22_ = i_20_ >> -1048016408 & 0xff;
							int i_23_ = i_20_ & 0xff;
							Class287.regionTerrainId[i_11_] = Class234.mapsJs5.getGroupId((byte) -71, "m" + i_22_ + "_" + i_23_);
							regionObjectMapId[i_11_] = Class234.mapsJs5.getGroupId((byte) -118, "l" + i_22_ + "_" + i_23_);
							GameObjectDefinitionParser.regionUwTerrainId[i_11_] = Class234.mapsJs5.getGroupId((byte) -126, "um" + i_22_ + "_" + i_23_);
							HashTable.regionUwObjectMapId[i_11_] = Class234.mapsJs5.getGroupId((byte) -85, "ul" + i_22_ + "_" + i_23_);
							i_11_++;
						}
					}
				}
			}
		}
		Class251.updateMapArea(-6547, centreY, force, centreX, 11);
	}

	public boolean			aBoolean4153;
	boolean					aBoolean4154;
	public boolean			aBoolean4155;

	boolean					aBoolean4158;

	public Class237_Sub1	aClass237_Sub1_4157;

	public int				anInt4152;

	public int				anInt4160;

	Class98_Sub36(int i, Class237_Sub1 class237_sub1, int i_24_, boolean bool) {
		try {
			anInt4152 = i_24_;
			aClass237_Sub1_4157 = class237_sub1;
			anInt4160 = i;
			aBoolean4154 = bool;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "pm.<init>(" + i + ',' + (class237_sub1 != null ? "{...}" : "null") + ',' + i_24_ + ',' + bool + ')');
		}
	}
}

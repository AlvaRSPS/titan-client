
/* Class360 - Decompiled by JODE
 */ package com; /*
					*/

import java.awt.Color;

import com.jagex.game.client.archive.Js5Exception;

import jaggl.OpenGL;

public final class Class360 {
	public static String password = "";

	public static final boolean method3905(int i, int i_61_, int i_62_) {
		try {
			return (i_61_ & 0x8000) != 0;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vq.E(" + i + ',' + i_61_ + ',' + i_62_ + ')');
		}
	}

	public static final boolean method3909(int i, int i_81_) {
		try {
			if (i != -4) {
				password = null;
			}
			return !((i_81_ < 0 || (i_81_ ^ 0xffffffff) < -4) && i_81_ != 9);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vq.H(" + i + ',' + i_81_ + ')');
		}
	}

	private OpenGlArrayBufferPointer	aClass104_3067;
	private OpenGlArrayBufferPointer	aClass104_3068;
	private OpenGlArrayBufferPointer	aClass104_3069;
	private Class246_Sub4_Sub2[][]		aClass246_Sub4_Sub2ArrayArray3072;
	private Class246_Sub4_Sub2[][]		aClass246_Sub4_Sub2ArrayArray3073;
	private RsFloatBuffer				stream;
	private float[]						aFloatArray3065	= new float[16];
	private int							anInt3071;
	private int							anInt3076;

	private int[]						anIntArray3074;

	private int[]						anIntArray3075;

	private int[]						anIntArray3077;

	private ArrayBuffer					anInterface16_3066;

	Class360() {
		stream = new RsFloatBuffer(786336);
		anInt3071 = Class48_Sub2_Sub1.method474(1600, (byte) 31);
		aClass246_Sub4_Sub2ArrayArray3072 = new Class246_Sub4_Sub2[1600][64];
		anIntArray3075 = new int[1600];
		anIntArray3077 = new int[64];
		anIntArray3074 = new int[8191];
		aClass246_Sub4_Sub2ArrayArray3073 = new Class246_Sub4_Sub2[64][768];
		anInt3076 = 0;
	}

	private final void method3904(int i, OpenGlToolkit var_ha_Sub1, byte i_0_) {//Used for changing particle colour
		do {
			try {
				OpenGL.glGetFloatv(2982, aFloatArray3065, 0);
				float f = aFloatArray3065[0];
				float f_1_ = aFloatArray3065[4];
				float f_2_ = aFloatArray3065[8];
				float f_3_ = aFloatArray3065[1];
				float f_4_ = aFloatArray3065[5];
				float f_5_ = aFloatArray3065[9];
				float f_6_ = f + f_3_;
				float f_7_ = f_4_ + f_1_;
				float f_8_ = f_5_ + f_2_;
				float f_9_ = f - f_3_;
				float f_10_ = -f_4_ + f_1_;
				if (i_0_ == 52) {
					float f_11_ = f_2_ - f_5_;
					float f_12_ = f_3_ - f;
					float f_13_ = f_4_ - f_1_;
					float f_14_ = -f_2_ + f_5_;
					stream.position = 0;
					if (var_ha_Sub1.bigEndian) {
						for (int i_15_ = -1 + i; (i_15_ ^ 0xffffffff) <= -1; i_15_--) {
							int i_16_ = anIntArray3075[i_15_] <= 64 ? anIntArray3075[i_15_] : 64;
							if (i_16_ > 0) {
								for (int i_17_ = -1 + i_16_; (i_17_ ^ 0xffffffff) <= -1; i_17_--) {
									Class246_Sub4_Sub2 class246_sub4_sub2 = aClass246_Sub4_Sub2ArrayArray3072[i_15_][i_17_];
									int i_18_ = class246_sub4_sub2.anInt6178;
									byte i_19_ = (byte) (i_18_ >> 1656669520);
									byte i_20_ = (byte) (i_18_ >> -1272463480);
									byte i_21_ = (byte) i_18_;
									byte i_22_ = (byte) (i_18_ >>> 1975843128);
									float f_23_ = class246_sub4_sub2.anInt6176 >> -454584212;
									float f_24_ = class246_sub4_sub2.anInt6177 >> -649195284;
									float f_25_ = class246_sub4_sub2.anInt6175 >> 78882380;
									int i_26_ = class246_sub4_sub2.anInt6179 >> -2029434900;
									stream.writeFloat((byte) -118, 0.0F);
									stream.writeFloat((byte) -123, 0.0F);
									stream.writeFloat((byte) -96, -i_26_ * f_6_ + f_23_);
									stream.writeFloat((byte) -1, f_7_ * -i_26_ + f_24_);
									stream.writeFloat((byte) -96, f_25_ + f_8_ * -i_26_);
									stream.writeByte(i_19_, -115);
									stream.writeByte(i_20_, -36);
									stream.writeByte(i_21_, -90);
									stream.writeByte(i_22_, 118);
									stream.writeFloat((byte) 81, 1.0F);
									stream.writeFloat((byte) -121, 0.0F);
									stream.writeFloat((byte) 73, i_26_ * f_9_ + f_23_);
									stream.writeFloat((byte) 81, f_10_ * i_26_ + f_24_);
									stream.writeFloat((byte) -102, f_11_ * i_26_ + f_25_);
									stream.writeByte(i_19_, 73);
									stream.writeByte(i_20_, i_0_ + -127);
									stream.writeByte(i_21_, i_0_ ^ ~0x7a);
									stream.writeByte(i_22_, 106);
									stream.writeFloat((byte) -104, 1.0F);
									stream.writeFloat((byte) -98, 1.0F);
									stream.writeFloat((byte) 66, i_26_ * f_6_ + f_23_);
									stream.writeFloat((byte) 45, f_7_ * i_26_ + f_24_);
									stream.writeFloat((byte) 126, f_25_ + f_8_ * i_26_);
									stream.writeByte(i_19_, i_0_ ^ ~0x52);
									stream.writeByte(i_20_, -99);
									stream.writeByte(i_21_, 103);
									stream.writeByte(i_22_, -40);
									stream.writeFloat((byte) -111, 0.0F);
									stream.writeFloat((byte) -126, 1.0F);
									stream.writeFloat((byte) 88, i_26_ * f_12_ + f_23_);
									stream.writeFloat((byte) 86, i_26_ * f_13_ + f_24_);
									stream.writeFloat((byte) 103, i_26_ * f_14_ + f_25_);
									stream.writeByte(i_19_, -110);
									stream.writeByte(i_20_, 50);
									stream.writeByte(i_21_, 126);
									stream.writeByte(i_22_, 124);
								}
								if ((anIntArray3075[i_15_] ^ 0xffffffff) < -65) {
									int i_27_ = -65 + anIntArray3075[i_15_];
									for (int i_28_ = anIntArray3077[i_27_] - 1; (i_28_ ^ 0xffffffff) <= -1; i_28_--) {
										Class246_Sub4_Sub2 class246_sub4_sub2 = aClass246_Sub4_Sub2ArrayArray3073[i_27_][i_28_];
										int i_29_ = class246_sub4_sub2.anInt6178;
										byte i_30_ = (byte) (i_29_ >> -1694259824);
										byte i_31_ = (byte) (i_29_ >> 451522632);
										byte i_32_ = (byte) i_29_;
										byte i_33_ = (byte) (i_29_ >>> -310568296);
										float f_34_ = class246_sub4_sub2.anInt6176 >> -901688052;
										float f_35_ = class246_sub4_sub2.anInt6177 >> 2000215468;
										float f_36_ = class246_sub4_sub2.anInt6175 >> 1165920652;
										int i_37_ = class246_sub4_sub2.anInt6179 >> -627874868;
										stream.writeFloat((byte) 95, 0.0F);
										stream.writeFloat((byte) 30, 0.0F);
										stream.writeFloat((byte) -113, f_34_ + f_6_ * -i_37_);
										stream.writeFloat((byte) 43, -i_37_ * f_7_ + f_35_);
										stream.writeFloat((byte) 32, f_8_ * -i_37_ + f_36_);
										stream.writeByte(i_30_, -46);
										stream.writeByte(i_31_, -59);
										stream.writeByte(i_32_, i_0_ ^ 0x9);
										stream.writeByte(i_33_, 78);
										stream.writeFloat((byte) 44, 1.0F);
										stream.writeFloat((byte) -113, 0.0F);
										stream.writeFloat((byte) -125, f_34_ + f_9_ * i_37_);
										stream.writeFloat((byte) 29, f_10_ * i_37_ + f_35_);
										stream.writeFloat((byte) -100, f_11_ * i_37_ + f_36_);
										stream.writeByte(i_30_, 66);
										stream.writeByte(i_31_, i_0_ ^ ~0x58);
										stream.writeByte(i_32_, -117);
										stream.writeByte(i_33_, i_0_ + 44);
										stream.writeFloat((byte) -117, 1.0F);
										stream.writeFloat((byte) 115, 1.0F);
										stream.writeFloat((byte) 9, f_34_ + f_6_ * i_37_);
										stream.writeFloat((byte) -103, f_35_ + i_37_ * f_7_);
										stream.writeFloat((byte) 57, f_36_ + i_37_ * f_8_);
										stream.writeByte(i_30_, 118);
										stream.writeByte(i_31_, 90);
										stream.writeByte(i_32_, -61);
										stream.writeByte(i_33_, i_0_ + 71);
										stream.writeFloat((byte) 30, 0.0F);
										stream.writeFloat((byte) -127, 1.0F);
										stream.writeFloat((byte) 51, f_34_ + f_12_ * i_37_);
										stream.writeFloat((byte) 42, i_37_ * f_13_ + f_35_);
										stream.writeFloat((byte) 80, f_36_ + f_14_ * i_37_);
										stream.writeByte(i_30_, 43);
										stream.writeByte(i_31_, i_0_ ^ ~0x8);
										stream.writeByte(i_32_, 85);
										stream.writeByte(i_33_, i_0_ ^ 0xa);
									}
								}
							}
						}
					} else {
						for (int i_38_ = i - 1; (i_38_ ^ 0xffffffff) <= -1; i_38_--) {
							int i_39_ = anIntArray3075[i_38_] > 64 ? 64 : anIntArray3075[i_38_];
							if ((i_39_ ^ 0xffffffff) < -1) {
								for (int i_40_ = i_39_ - 1; (i_40_ ^ 0xffffffff) <= -1; i_40_--) {
									Class246_Sub4_Sub2 class246_sub4_sub2 = aClass246_Sub4_Sub2ArrayArray3072[i_38_][i_40_];
									int i_41_ = class246_sub4_sub2.anInt6178;
									byte i_42_ = (byte) (i_41_ >> 394032304);
									byte i_43_ = (byte) (i_41_ >> -1148711800);
									byte i_44_ = (byte) i_41_;
									byte i_45_ = (byte) (i_41_ >>> -634651496);
									float f_46_ = class246_sub4_sub2.anInt6176 >> 1288887212;
									float f_47_ = class246_sub4_sub2.anInt6177 >> -1848026004;
									float f_48_ = class246_sub4_sub2.anInt6175 >> -638538612;
									int i_49_ = class246_sub4_sub2.anInt6179 >> -1389801652;
									stream.method1265((byte) -52, 0.0F);
									stream.method1265((byte) -52, 0.0F);
									stream.method1265((byte) -52, f_46_ + f_6_ * -i_49_);
									stream.method1265((byte) -52, f_47_ + f_7_ * -i_49_);
									stream.method1265((byte) -52, f_48_ + -i_49_ * f_8_);
									stream.writeByte(i_42_, i_0_ + -94);
									stream.writeByte(i_43_, i_0_ ^ 0x65);
									stream.writeByte(i_44_, 120);
									stream.writeByte(i_45_, -102);
									stream.method1265((byte) -52, 1.0F);
									stream.method1265((byte) -52, 0.0F);
									stream.method1265((byte) -52, i_49_ * f_9_ + f_46_);
									stream.method1265((byte) -52, i_49_ * f_10_ + f_47_);
									stream.method1265((byte) -52, i_49_ * f_11_ + f_48_);
									stream.writeByte(i_42_, i_0_ ^ ~0x6f);
									stream.writeByte(i_43_, i_0_ ^ ~0x7c);
									stream.writeByte(i_44_, i_0_ + -164);
									stream.writeByte(i_45_, -124);
									stream.method1265((byte) -52, 1.0F);
									stream.method1265((byte) -52, 1.0F);
									stream.method1265((byte) -52, f_46_ + f_6_ * i_49_);
									stream.method1265((byte) -52, f_7_ * i_49_ + f_47_);
									stream.method1265((byte) -52, f_48_ + i_49_ * f_8_);
									stream.writeByte(i_42_, i_0_ ^ 0x4e);
									stream.writeByte(i_43_, 64);
									stream.writeByte(i_44_, 57);
									stream.writeByte(i_45_, -41);
									stream.method1265((byte) -52, 0.0F);
									stream.method1265((byte) -52, 1.0F);
									stream.method1265((byte) -52, f_46_ + f_12_ * i_49_);
									stream.method1265((byte) -52, f_13_ * i_49_ + f_47_);
									stream.method1265((byte) -52, f_14_ * i_49_ + f_48_);
									stream.writeByte(i_42_, 121);
									stream.writeByte(i_43_, i_0_ + -115);
									stream.writeByte(i_44_, -52);
									stream.writeByte(i_45_, -35);
								}
								if ((anIntArray3075[i_38_] ^ 0xffffffff) < -65) {
									int i_50_ = -64 + anIntArray3075[i_38_] + -1;
									for (int i_51_ = anIntArray3077[i_50_] + -1; i_51_ >= 0; i_51_--) {
										Class246_Sub4_Sub2 class246_sub4_sub2 = aClass246_Sub4_Sub2ArrayArray3073[i_50_][i_51_];
										int i_52_ = class246_sub4_sub2.anInt6178;
										byte i_53_ = (byte) (i_52_ >> 117536784);
										byte i_54_ = (byte) (i_52_ >> 1635536040);
										byte i_55_ = (byte) i_52_;
										byte i_56_ = (byte) (i_52_ >>> -100916008);
										float f_57_ = class246_sub4_sub2.anInt6176 >> -1764237876;
										float f_58_ = class246_sub4_sub2.anInt6177 >> 1759716140;
										float f_59_ = class246_sub4_sub2.anInt6175 >> 88626700;
										int i_60_ = class246_sub4_sub2.anInt6179 >> -1925490324;
										stream.method1265((byte) -52, 0.0F);
										stream.method1265((byte) -52, 0.0F);
										stream.method1265((byte) -52, f_6_ * -i_60_ + f_57_);
										stream.method1265((byte) -52, f_7_ * -i_60_ + f_58_);
										stream.method1265((byte) -52, f_8_ * -i_60_ + f_59_);
										stream.writeByte(i_53_, 79);
										stream.writeByte(i_54_, -56);
										stream.writeByte(i_55_, -106);
										stream.writeByte(i_56_, -118);
										stream.method1265((byte) -52, 1.0F);
										stream.method1265((byte) -52, 0.0F);
										stream.method1265((byte) -52, i_60_ * f_9_ + f_57_);
										stream.method1265((byte) -52, f_10_ * i_60_ + f_58_);
										stream.method1265((byte) -52, f_59_ + i_60_ * f_11_);
										stream.writeByte(i_53_, i_0_ ^ 0x6f);
										stream.writeByte(i_54_, -55);
										stream.writeByte(i_55_, i_0_ ^ 0x1a);
										stream.writeByte(i_56_, -41);
										stream.method1265((byte) -52, 1.0F);
										stream.method1265((byte) -52, 1.0F);
										stream.method1265((byte) -52, f_57_ + i_60_ * f_6_);
										stream.method1265((byte) -52, f_58_ + f_7_ * i_60_);
										stream.method1265((byte) -52, f_8_ * i_60_ + f_59_);
										stream.writeByte(i_53_, 43);
										stream.writeByte(i_54_, 49);
										stream.writeByte(i_55_, -51);
										stream.writeByte(i_56_, i_0_ + -155);
										stream.method1265((byte) -52, 0.0F);
										stream.method1265((byte) -52, 1.0F);
										stream.method1265((byte) -52, f_57_ + i_60_ * f_12_);
										stream.method1265((byte) -52, f_58_ + f_13_ * i_60_);
										stream.method1265((byte) -52, i_60_ * f_14_ + f_59_);
										stream.writeByte(i_53_, -102);
										stream.writeByte(i_54_, i_0_ ^ ~0x1b);
										stream.writeByte(i_55_, i_0_ + -136);
										stream.writeByte(i_56_, 124);
									}
								}
							}
						}
					}
					if ((stream.position ^ 0xffffffff) == -1) {
						break;
					}
					anInterface16_3066.method54(stream.position, 7896, stream.payload, 24);
					var_ha_Sub1.setPointers(aClass104_3068, null, aClass104_3067, aClass104_3069, 0);
					var_ha_Sub1.method1910(7, stream.position / 24, false, 0);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vq.B(" + i + ',' + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i_0_ + ')');
			}
			break;
		} while (false);
	}

	private final void method3904_cp(int i, OpenGlToolkit var_ha_Sub1, byte i_0_, ParticleDescriptor pDescriptor, int intensity, int ambient) {//Used for changing particle colour
		do {
			try {
				OpenGL.glGetFloatv(2982, aFloatArray3065, 0);
				float f = aFloatArray3065[0];
				float f_1_ = aFloatArray3065[4];
				float f_2_ = aFloatArray3065[8];
				float f_3_ = aFloatArray3065[1];
				float f_4_ = aFloatArray3065[5];
				float f_5_ = aFloatArray3065[9];
				float f_6_ = f + f_3_;
				float f_7_ = f_4_ + f_1_;
				float f_8_ = f_5_ + f_2_;
				float f_9_ = f - f_3_;
				float f_10_ = -f_4_ + f_1_;
				if (i_0_ == 52) {
					float f_11_ = f_2_ - f_5_;
					float f_12_ = f_3_ - f;
					float f_13_ = f_4_ - f_1_;
					float f_14_ = -f_2_ + f_5_;
					if (pDescriptor.m) {
						Color col = pDescriptor.inc();
						pDescriptor.red = (byte) col.getRed();
						pDescriptor.green = (byte) col.getGreen();
						pDescriptor.blue = (byte) col.getBlue();
					}
					stream.position = 0;
					if (var_ha_Sub1.bigEndian) {
						for (int i_15_ = -1 + i; (i_15_ ^ 0xffffffff) <= -1; i_15_--) {
							int i_16_ = anIntArray3075[i_15_] <= 64 ? anIntArray3075[i_15_] : 64;
							if (i_16_ > 0) {
								for (int i_17_ = -1 + i_16_; (i_17_ ^ 0xffffffff) <= -1; i_17_--) {
									Class246_Sub4_Sub2 class246_sub4_sub2 = aClass246_Sub4_Sub2ArrayArray3072[i_15_][i_17_];
									int i_18_ = class246_sub4_sub2.anInt6178;
									byte i_19_ = (byte) (i_18_ >> 1656669520);
									byte i_20_ = (byte) (i_18_ >> -1272463480);
									byte i_21_ = (byte) i_18_;
									/*Color col = ccat.inc();
									i_19_ = (byte) col.getRed();
									i_20_ = (byte) col.getGreen();
									i_21_ = (byte) col.getBlue();*/
									i_19_ = pDescriptor.red;
									i_20_ = pDescriptor.green;
									i_21_ = pDescriptor.blue;
									byte i_22_ = (byte) (i_18_ >>> 1975843128);
									float f_23_ = class246_sub4_sub2.anInt6176 >> -454584212;
									float f_24_ = class246_sub4_sub2.anInt6177 >> -649195284;
									float f_25_ = class246_sub4_sub2.anInt6175 >> 78882380;
									int i_26_ = class246_sub4_sub2.anInt6179 >> -2029434900;
									if (ambient > 0)
										i_22_ = (byte) ambient;
									if (intensity > 0) {
										i_26_ = intensity;
									}
									stream.writeFloat((byte) -118, 0.0F);
									stream.writeFloat((byte) -123, 0.0F);
									stream.writeFloat((byte) -96, -i_26_ * f_6_ + f_23_);
									stream.writeFloat((byte) -1, f_7_ * -i_26_ + f_24_);
									stream.writeFloat((byte) -96, f_25_ + f_8_ * -i_26_);
									stream.writeByte(i_19_, -115);
									stream.writeByte(i_20_, -36);
									stream.writeByte(i_21_, -90);
									stream.writeByte(i_22_, 118);
									stream.writeFloat((byte) 81, 1.0F);
									stream.writeFloat((byte) -121, 0.0F);
									stream.writeFloat((byte) 73, i_26_ * f_9_ + f_23_);
									stream.writeFloat((byte) 81, f_10_ * i_26_ + f_24_);
									stream.writeFloat((byte) -102, f_11_ * i_26_ + f_25_);
									stream.writeByte(i_19_, 73);
									stream.writeByte(i_20_, i_0_ + -127);
									stream.writeByte(i_21_, i_0_ ^ ~0x7a);
									stream.writeByte(i_22_, 106);
									stream.writeFloat((byte) -104, 1.0F);
									stream.writeFloat((byte) -98, 1.0F);
									stream.writeFloat((byte) 66, i_26_ * f_6_ + f_23_);
									stream.writeFloat((byte) 45, f_7_ * i_26_ + f_24_);
									stream.writeFloat((byte) 126, f_25_ + f_8_ * i_26_);
									stream.writeByte(i_19_, i_0_ ^ ~0x52);
									stream.writeByte(i_20_, -99);
									stream.writeByte(i_21_, 103);
									stream.writeByte(i_22_, -40);
									stream.writeFloat((byte) -111, 0.0F);
									stream.writeFloat((byte) -126, 1.0F);
									stream.writeFloat((byte) 88, i_26_ * f_12_ + f_23_);
									stream.writeFloat((byte) 86, i_26_ * f_13_ + f_24_);
									stream.writeFloat((byte) 103, i_26_ * f_14_ + f_25_);
									stream.writeByte(i_19_, -110);
									stream.writeByte(i_20_, 50);
									stream.writeByte(i_21_, 126);
									stream.writeByte(i_22_, 124);
								}
								if ((anIntArray3075[i_15_] ^ 0xffffffff) < -65) {
									int i_27_ = -65 + anIntArray3075[i_15_];
									for (int i_28_ = anIntArray3077[i_27_] - 1; (i_28_ ^ 0xffffffff) <= -1; i_28_--) {
										Class246_Sub4_Sub2 class246_sub4_sub2 = aClass246_Sub4_Sub2ArrayArray3073[i_27_][i_28_];
										int i_29_ = class246_sub4_sub2.anInt6178;
										byte i_30_ = (byte) (i_29_ >> -1694259824);
										byte i_31_ = (byte) (i_29_ >> 451522632);
										byte i_32_ = (byte) i_29_;
										/*Color col = ccat.inc();
										i_30_ = (byte) col.getRed();
										i_31_ = (byte) col.getGreen();
										i_32_ = (byte) col.getBlue();*/
										i_30_ = pDescriptor.red;
										i_31_ = pDescriptor.green;
										i_32_ = pDescriptor.blue;

										byte i_33_ = (byte) (i_29_ >>> -310568296);
										float f_34_ = class246_sub4_sub2.anInt6176 >> -901688052;
										float f_35_ = class246_sub4_sub2.anInt6177 >> 2000215468;
										float f_36_ = class246_sub4_sub2.anInt6175 >> 1165920652;
										int i_37_ = class246_sub4_sub2.anInt6179 >> -627874868;

										if (ambient > 0)
											i_33_ = (byte) ambient;
										if (intensity > 0) {
											i_37_ = intensity;
										}
										stream.writeFloat((byte) 95, 0.0F);
										stream.writeFloat((byte) 30, 0.0F);
										stream.writeFloat((byte) -113, f_34_ + f_6_ * -i_37_);
										stream.writeFloat((byte) 43, -i_37_ * f_7_ + f_35_);
										stream.writeFloat((byte) 32, f_8_ * -i_37_ + f_36_);
										stream.writeByte(i_30_, -46);
										stream.writeByte(i_31_, -59);
										stream.writeByte(i_32_, i_0_ ^ 0x9);
										stream.writeByte(i_33_, 78);
										stream.writeFloat((byte) 44, 1.0F);
										stream.writeFloat((byte) -113, 0.0F);
										stream.writeFloat((byte) -125, f_34_ + f_9_ * i_37_);
										stream.writeFloat((byte) 29, f_10_ * i_37_ + f_35_);
										stream.writeFloat((byte) -100, f_11_ * i_37_ + f_36_);
										stream.writeByte(i_30_, 66);
										stream.writeByte(i_31_, i_0_ ^ ~0x58);
										stream.writeByte(i_32_, -117);
										stream.writeByte(i_33_, i_0_ + 44);
										stream.writeFloat((byte) -117, 1.0F);
										stream.writeFloat((byte) 115, 1.0F);
										stream.writeFloat((byte) 9, f_34_ + f_6_ * i_37_);
										stream.writeFloat((byte) -103, f_35_ + i_37_ * f_7_);
										stream.writeFloat((byte) 57, f_36_ + i_37_ * f_8_);
										stream.writeByte(i_30_, 118);
										stream.writeByte(i_31_, 90);
										stream.writeByte(i_32_, -61);
										stream.writeByte(i_33_, i_0_ + 71);
										stream.writeFloat((byte) 30, 0.0F);
										stream.writeFloat((byte) -127, 1.0F);
										stream.writeFloat((byte) 51, f_34_ + f_12_ * i_37_);
										stream.writeFloat((byte) 42, i_37_ * f_13_ + f_35_);
										stream.writeFloat((byte) 80, f_36_ + f_14_ * i_37_);
										stream.writeByte(i_30_, 43);
										stream.writeByte(i_31_, i_0_ ^ ~0x8);
										stream.writeByte(i_32_, 85);
										stream.writeByte(i_33_, i_0_ ^ 0xa);
									}
								}
							}
						}
					} else {
						for (int i_38_ = i - 1; (i_38_ ^ 0xffffffff) <= -1; i_38_--) {
							int i_39_ = anIntArray3075[i_38_] > 64 ? 64 : anIntArray3075[i_38_];
							if ((i_39_ ^ 0xffffffff) < -1) {
								for (int i_40_ = i_39_ - 1; (i_40_ ^ 0xffffffff) <= -1; i_40_--) {
									Class246_Sub4_Sub2 class246_sub4_sub2 = aClass246_Sub4_Sub2ArrayArray3072[i_38_][i_40_];
									int i_41_ = class246_sub4_sub2.anInt6178;
									byte red = (byte) (i_41_ >> 394032304);
									byte green = (byte) (i_41_ >> -1148711800);
									byte blue = (byte) i_41_;
									red = pDescriptor.red;
									green = pDescriptor.green;
									blue = pDescriptor.blue;
									//System.out.println("Red: " + red + " Green: " + green + " Blue:" + blue);

									byte amb = (byte) (i_41_ >>> -634651496);
									float f_46_ = class246_sub4_sub2.anInt6176 >> 1288887212;
									float f_47_ = class246_sub4_sub2.anInt6177 >> -1848026004;
									float f_48_ = class246_sub4_sub2.anInt6175 >> -638538612;
									int intens = class246_sub4_sub2.anInt6179 >> -1389801652;
									if (ambient > 0)
										amb = (byte) ambient;
									if (intensity > 0) {
										intens = intensity;
									}
									//System.out.println("Ambient: " + amb + " Intensity: " + intens);

									stream.method1265((byte) -52, 0.0F);
									stream.method1265((byte) -52, 0.0F);
									stream.method1265((byte) -52, f_46_ + f_6_ * -intens);
									stream.method1265((byte) -52, f_47_ + f_7_ * -intens);
									stream.method1265((byte) -52, f_48_ + -intens * f_8_);
									stream.writeByte(red, i_0_ + -94);
									stream.writeByte(green, i_0_ ^ 0x65);
									stream.writeByte(blue, 120);
									stream.writeByte(amb, -102);
									stream.method1265((byte) -52, 1.0F);
									stream.method1265((byte) -52, 0.0F);
									stream.method1265((byte) -52, intens * f_9_ + f_46_);
									stream.method1265((byte) -52, intens * f_10_ + f_47_);
									stream.method1265((byte) -52, intens * f_11_ + f_48_);
									stream.writeByte(red, i_0_ ^ ~0x6f);
									stream.writeByte(green, i_0_ ^ ~0x7c);
									stream.writeByte(blue, i_0_ + -164);
									stream.writeByte(amb, -124);
									stream.method1265((byte) -52, 1.0F);
									stream.method1265((byte) -52, 1.0F);
									stream.method1265((byte) -52, f_46_ + f_6_ * intens);
									stream.method1265((byte) -52, f_7_ * intens + f_47_);
									stream.method1265((byte) -52, f_48_ + intens * f_8_);
									stream.writeByte(red, i_0_ ^ 0x4e);
									stream.writeByte(green, 64);
									stream.writeByte(blue, 57);
									stream.writeByte(amb, -41);
									stream.method1265((byte) -52, 0.0F);
									stream.method1265((byte) -52, 1.0F);
									stream.method1265((byte) -52, f_46_ + f_12_ * intens);
									stream.method1265((byte) -52, f_13_ * intens + f_47_);
									stream.method1265((byte) -52, f_14_ * intens + f_48_);
									stream.writeByte(red, 121);
									stream.writeByte(green, i_0_ + -115);
									stream.writeByte(blue, -52);
									stream.writeByte(amb, -35);
								}
								if ((anIntArray3075[i_38_] ^ 0xffffffff) < -65) {
									int i_50_ = -64 + anIntArray3075[i_38_] + -1;
									for (int i_51_ = anIntArray3077[i_50_] + -1; i_51_ >= 0; i_51_--) {
										Class246_Sub4_Sub2 class246_sub4_sub2 = aClass246_Sub4_Sub2ArrayArray3073[i_50_][i_51_];
										int i_52_ = class246_sub4_sub2.anInt6178;
										byte i_53_ = (byte) (i_52_ >> 117536784);
										byte i_54_ = (byte) (i_52_ >> 1635536040);
										byte i_55_ = (byte) i_52_;
										i_53_ = pDescriptor.red;
										i_54_ = pDescriptor.green;
										i_55_ = pDescriptor.blue;
										byte i_56_ = (byte) (i_52_ >>> -100916008);
										float f_57_ = class246_sub4_sub2.anInt6176 >> -1764237876;
										float f_58_ = class246_sub4_sub2.anInt6177 >> 1759716140;
										float f_59_ = class246_sub4_sub2.anInt6175 >> 88626700;
										int i_60_ = class246_sub4_sub2.anInt6179 >> -1925490324;
										if (ambient > 0)
											i_56_ = (byte) ambient;
										if (intensity > 0) {
											i_60_ = intensity;
										}
										stream.method1265((byte) -52, 0.0F);
										stream.method1265((byte) -52, 0.0F);
										stream.method1265((byte) -52, f_6_ * -i_60_ + f_57_);
										stream.method1265((byte) -52, f_7_ * -i_60_ + f_58_);
										stream.method1265((byte) -52, f_8_ * -i_60_ + f_59_);
										stream.writeByte(i_53_, 79);
										stream.writeByte(i_54_, -56);
										stream.writeByte(i_55_, -106);
										stream.writeByte(i_56_, -118);
										stream.method1265((byte) -52, 1.0F);
										stream.method1265((byte) -52, 0.0F);
										stream.method1265((byte) -52, i_60_ * f_9_ + f_57_);
										stream.method1265((byte) -52, f_10_ * i_60_ + f_58_);
										stream.method1265((byte) -52, f_59_ + i_60_ * f_11_);
										stream.writeByte(i_53_, i_0_ ^ 0x6f);
										stream.writeByte(i_54_, -55);
										stream.writeByte(i_55_, i_0_ ^ 0x1a);
										stream.writeByte(i_56_, -41);
										stream.method1265((byte) -52, 1.0F);
										stream.method1265((byte) -52, 1.0F);
										stream.method1265((byte) -52, f_57_ + i_60_ * f_6_);
										stream.method1265((byte) -52, f_58_ + f_7_ * i_60_);
										stream.method1265((byte) -52, f_8_ * i_60_ + f_59_);
										stream.writeByte(i_53_, 43);
										stream.writeByte(i_54_, 49);
										stream.writeByte(i_55_, -51);
										stream.writeByte(i_56_, i_0_ + -155);
										stream.method1265((byte) -52, 0.0F);
										stream.method1265((byte) -52, 1.0F);
										stream.method1265((byte) -52, f_57_ + i_60_ * f_12_);
										stream.method1265((byte) -52, f_58_ + f_13_ * i_60_);
										stream.method1265((byte) -52, i_60_ * f_14_ + f_59_);
										stream.writeByte(i_53_, -102);
										stream.writeByte(i_54_, i_0_ ^ ~0x1b);
										stream.writeByte(i_55_, i_0_ + -136);
										stream.writeByte(i_56_, 124);
									}
								}
							}
						}
					}
					if ((stream.position ^ 0xffffffff) == -1) {
						break;
					}
					anInterface16_3066.method54(stream.position, 7896, stream.payload, 24);
					var_ha_Sub1.setPointers(aClass104_3068, null, aClass104_3067, aClass104_3069, 0);
					var_ha_Sub1.method1910(7, stream.position / 24, false, 0);
				}
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vq.B(" + i + ',' + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i_0_ + ')');
			}
			break;
		} while (false);
	}

	public final void method3906(Class242 class242, int i, int i_64_, OpenGlToolkit var_ha_Sub1) {
		try {
			if (var_ha_Sub1.aClass111_Sub1_4348 != null) {
				if ((i_64_ ^ 0xffffffff) > -1) {
					method3908(var_ha_Sub1, 5228);
				} else {
					method3911(var_ha_Sub1, true, i_64_);
				}
				float f = var_ha_Sub1.aClass111_Sub1_4348.aFloat4684;
				float f_65_ = var_ha_Sub1.aClass111_Sub1_4348.aFloat4676;
				float f_66_ = var_ha_Sub1.aClass111_Sub1_4348.aFloat4673;
				float f_67_ = var_ha_Sub1.aClass111_Sub1_4348.aFloat4677;
				try {
					int i_68_ = 0;
					int i_69_ = 2147483647;
					int i_70_ = 0;
					Entity class246_sub4 = class242.aClass358_1850.aClass246_Sub4_3028;
					if (i > -40) {
						return;
					}
					for (Entity class246_sub4_71_ = class246_sub4.next; class246_sub4_71_ != class246_sub4; class246_sub4_71_ = class246_sub4_71_.next) {
						Class246_Sub4_Sub2 class246_sub4_sub2 = (Class246_Sub4_Sub2) class246_sub4_71_;
						int i_72_ = (int) ((class246_sub4_sub2.anInt6175 >> 1098990956) * f_66_ + ((class246_sub4_sub2.anInt6176 >> -1531981396) * f + (class246_sub4_sub2.anInt6177 >> -572896980) * f_65_) + f_67_);
						anIntArray3074[i_68_++] = i_72_;
						if ((i_69_ ^ 0xffffffff) < (i_72_ ^ 0xffffffff)) {
							i_69_ = i_72_;
						}
						if (i_72_ > i_70_) {
							i_70_ = i_72_;
						}
					}
					int i_73_ = -i_69_ + i_70_;
					int i_74_;
					if (i_73_ + 2 > 1600) {
						i_74_ = 1 - (-Class48_Sub2_Sub1.method474(i_73_, (byte) 31) + anInt3071);
						i_73_ = (i_73_ >> i_74_) - -2;
					} else {
						i_73_ += 2;
						i_74_ = 0;
					}
					i_68_ = 0;
					Entity class246_sub4_75_ = class246_sub4.next;
					int i_76_ = -2;
					boolean bool = true;
					boolean bool_77_ = true;
					while (class246_sub4_75_ != class246_sub4) {
						anInt3076 = 0;
						for (int i_78_ = 0; i_73_ > i_78_; i_78_++) {
							anIntArray3075[i_78_] = 0;
						}
						for (int i_79_ = 0; i_79_ < 64; i_79_++) {
							anIntArray3077[i_79_] = 0;
						}
						for (/**/; class246_sub4 != class246_sub4_75_; class246_sub4_75_ = class246_sub4_75_.next) {
							Class246_Sub4_Sub2 class246_sub4_sub2 = (Class246_Sub4_Sub2) class246_sub4_75_;
							if (bool_77_) {
								i_76_ = class246_sub4_sub2.anInt6180;
								bool = class246_sub4_sub2.aBoolean6174;
								bool_77_ = false;
							}
							if ((i_68_ ^ 0xffffffff) < -1 && ((class246_sub4_sub2.anInt6180 ^ 0xffffffff) != (i_76_ ^ 0xffffffff) || !bool == class246_sub4_sub2.aBoolean6174)) {
								bool_77_ = true;
								break;
							}
							int i_80_ = -i_69_ + anIntArray3074[i_68_++] >> i_74_;
							if (i_80_ < 1600) {
								if (anIntArray3075[i_80_] >= 64) {
									if (anIntArray3075[i_80_] == 64) {
										if (anInt3076 == 64) {
											continue;
										}
										anIntArray3075[i_80_] += 1 - -anInt3076++;
									}
									aClass246_Sub4_Sub2ArrayArray3073[anIntArray3075[i_80_] - 65][anIntArray3077[-1 + anIntArray3075[i_80_] + -64]++] = class246_sub4_sub2;
								} else {
									aClass246_Sub4_Sub2ArrayArray3072[i_80_][anIntArray3075[i_80_]++] = class246_sub4_sub2;
								}
							}
						}
						if ((i_76_ ^ 0xffffffff) > -1) {
							var_ha_Sub1.method1834(-43, -1);
						} else {
							var_ha_Sub1.method1834(-84, i_76_);
						}
						if (bool && Class49.aFloat416 != var_ha_Sub1.ambient) {
							var_ha_Sub1.setAmbientIntensity(Class49.aFloat416);
						} else if (var_ha_Sub1.ambient != 1.0F) {
							var_ha_Sub1.setAmbientIntensity(1.0F);
						}
						method3904(i_73_, var_ha_Sub1, (byte) 52);
					}
				} catch (Exception exception) {
					/* empty */
				}
				method3907(var_ha_Sub1, -124);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vq.J(" + (class242 != null ? "{...}" : "null") + ',' + i + ',' + i_64_ + ',' + (var_ha_Sub1 != null ? "{...}" : "null") + ')');
		}
	}

	public final void method3906_cp(Class242 class242, int i, int i_64_, OpenGlToolkit toolkit, ParticleDescriptor ccat, int ci, int ca) {
		try {
			if (toolkit.aClass111_Sub1_4348 != null) {
				if ((i_64_ ^ 0xffffffff) > -1) {
					method3908(toolkit, 5228);
				} else {
					method3911(toolkit, true, i_64_);
				}
				float f = toolkit.aClass111_Sub1_4348.aFloat4684;
				float f_65_ = toolkit.aClass111_Sub1_4348.aFloat4676;
				float f_66_ = toolkit.aClass111_Sub1_4348.aFloat4673;
				float f_67_ = toolkit.aClass111_Sub1_4348.aFloat4677;
				try {
					int i_68_ = 0;
					int i_69_ = 2147483647;
					int i_70_ = 0;
					Entity class246_sub4 = class242.aClass358_1850.aClass246_Sub4_3028;
					if (i > -40) {
						return;
					}
					for (Entity class246_sub4_71_ = class246_sub4.next; class246_sub4_71_ != class246_sub4; class246_sub4_71_ = class246_sub4_71_.next) {
						Class246_Sub4_Sub2 class246_sub4_sub2 = (Class246_Sub4_Sub2) class246_sub4_71_;
						int i_72_ = (int) ((class246_sub4_sub2.anInt6175 >> 1098990956) * f_66_ + ((class246_sub4_sub2.anInt6176 >> -1531981396) * f + (class246_sub4_sub2.anInt6177 >> -572896980) * f_65_) + f_67_);
						anIntArray3074[i_68_++] = i_72_;
						if ((i_69_ ^ 0xffffffff) < (i_72_ ^ 0xffffffff)) {
							i_69_ = i_72_;
						}
						if (i_72_ > i_70_) {
							i_70_ = i_72_;
						}
					}
					int i_73_ = -i_69_ + i_70_;
					int i_74_;
					if (i_73_ + 2 > 1600) {
						i_74_ = 1 - (-Class48_Sub2_Sub1.method474(i_73_, (byte) 31) + anInt3071);
						i_73_ = (i_73_ >> i_74_) - -2;
					} else {
						i_73_ += 2;
						i_74_ = 0;
					}
					i_68_ = 0;
					Entity class246_sub4_75_ = class246_sub4.next;
					int i_76_ = -2;
					boolean bool = true;
					boolean bool_77_ = true;
					while (class246_sub4_75_ != class246_sub4) {
						anInt3076 = 0;
						for (int i_78_ = 0; i_73_ > i_78_; i_78_++) {
							anIntArray3075[i_78_] = 0;
						}
						for (int i_79_ = 0; i_79_ < 64; i_79_++) {
							anIntArray3077[i_79_] = 0;
						}
						for (/**/; class246_sub4 != class246_sub4_75_; class246_sub4_75_ = class246_sub4_75_.next) {
							Class246_Sub4_Sub2 class246_sub4_sub2 = (Class246_Sub4_Sub2) class246_sub4_75_;
							if (bool_77_) {
								i_76_ = class246_sub4_sub2.anInt6180;
								bool = class246_sub4_sub2.aBoolean6174;
								bool_77_ = false;
							}
							if ((i_68_ ^ 0xffffffff) < -1 && ((class246_sub4_sub2.anInt6180 ^ 0xffffffff) != (i_76_ ^ 0xffffffff) || !bool == class246_sub4_sub2.aBoolean6174)) {
								bool_77_ = true;
								break;
							}
							int i_80_ = -i_69_ + anIntArray3074[i_68_++] >> i_74_;
							if (i_80_ < 1600) {
								if (anIntArray3075[i_80_] >= 64) {
									if (anIntArray3075[i_80_] == 64) {
										if (anInt3076 == 64) {
											continue;
										}
										anIntArray3075[i_80_] += 1 - -anInt3076++;
									}
									aClass246_Sub4_Sub2ArrayArray3073[anIntArray3075[i_80_] - 65][anIntArray3077[-1 + anIntArray3075[i_80_] + -64]++] = class246_sub4_sub2;
								} else {
									aClass246_Sub4_Sub2ArrayArray3072[i_80_][anIntArray3075[i_80_]++] = class246_sub4_sub2;
								}
							}
						}
						if ((i_76_ ^ 0xffffffff) > -1) {
							toolkit.method1834(-43, -1);
						} else {
							toolkit.method1834(-84, i_76_);
						}
						if (bool && Class49.aFloat416 != toolkit.ambient) {
							toolkit.setAmbientIntensity(Class49.aFloat416);
						} else if (toolkit.ambient != 1.0F) {
							toolkit.setAmbientIntensity(1.0F);
						}
						method3904_cp(i_73_, toolkit, (byte) 52, ccat, ci, ca);
					}
				} catch (Exception exception) {
					/* empty */
				}
				method3907(toolkit, -124);
			}
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vq.J(" + (class242 != null ? "{...}" : "null") + ',' + i + ',' + i_64_ + ',' + (toolkit != null ? "{...}" : "null") + ')');
		}
	}

	private final void method3907(OpenGlToolkit var_ha_Sub1, int i) {
		do {
			try {
				var_ha_Sub1.method1875((byte) -127, true);
				OpenGL.glEnable(16384);
				OpenGL.glEnable(16385);
				if (var_ha_Sub1.ambient == Class49.aFloat416) {
					break;
				}
				var_ha_Sub1.setAmbientIntensity(Class49.aFloat416);
			} catch (RuntimeException runtimeException) {
				throw Js5Exception.createJs5Exception(runtimeException, "vq.C(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ')');
			}
			break;
		} while (false);
	}

	private final void method3908(OpenGlToolkit var_ha_Sub1, int i) {
		try {
			Class49.aFloat416 = var_ha_Sub1.ambient;
			var_ha_Sub1.method1861(19330);
			if (i != 5228) {
				aClass246_Sub4_Sub2ArrayArray3072 = null;
			}
			OpenGL.glDisable(16384);
			OpenGL.glDisable(16385);
			var_ha_Sub1.method1875((byte) -127, false);
			OpenGL.glNormal3f(0.0F, -1.0F, 0.0F);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vq.D(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	private final void method3911(OpenGlToolkit var_ha_Sub1, boolean bool, int i) {
		try {
			Class49.aFloat416 = var_ha_Sub1.ambient;
			var_ha_Sub1.method1890(i, bool);
			var_ha_Sub1.method1901((byte) -35);
			OpenGL.glDisable(16384);
			OpenGL.glDisable(16385);
			var_ha_Sub1.method1875((byte) -125, false);
			OpenGL.glNormal3f(0.0F, -1.0F, 0.0F);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vq.G(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + bool + ',' + i + ')');
		}
	}

	public final void method3912(OpenGlToolkit var_ha_Sub1, int i) {
		try {
			anInterface16_3066 = var_ha_Sub1.method1878(i, true, 24, -51, null);
			aClass104_3067 = new OpenGlArrayBufferPointer(anInterface16_3066, 5126, 2, 0);
			aClass104_3069 = new OpenGlArrayBufferPointer(anInterface16_3066, 5126, 3, 8);
			aClass104_3068 = new OpenGlArrayBufferPointer(anInterface16_3066, 5121, 4, 20);
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "vq.F(" + (var_ha_Sub1 != null ? "{...}" : "null") + ',' + i + ')');
		}
	}
}

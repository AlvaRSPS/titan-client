/* Class308 - Decompiled by JODE
 */ package com; /*
					*/

import com.jagex.core.collections.HashTable;
import com.jagex.game.client.archive.Js5;
import com.jagex.game.client.archive.Js5Exception;
import com.jagex.game.client.preferences.ParticlesPreferenceField;

public final class Class308 {
	public static Class105			aClass105_2576				= new Class105("", 14);
	public static IncomingOpcode	aClass58_2581				= new IncomingOpcode(96, 10);
	public static ActionGroup		aClass98_Sub46_Sub9_2583	= null;
	public static int				anInt2580;
	public static int				anInt2584;

	public static final void handleCameraClick(int i, int i_0_, int i_1_, int i_2_) {
		i <<= 3;
		i_2_ <<= 3;
		i_0_ <<= 3;
		RsFloatBuffer.aFloat5794 = i;
		if ((Class98_Sub46_Sub20_Sub2.cameraMode ^ 0xffffffff) == -3) {
			Mob.anInt6357 = i_2_;
			Class186.cameraX = i;
			anInt2584 = i_0_;
		}
		Class119_Sub4.aFloat4740 = i_2_;
		Class42_Sub2.method388(true);
		ParticlesPreferenceField.aBoolean3656 = true;
	}

	private Js5			aClass207_2577;
	private Js5			aClass207_2578;

	private HashTable	aClass377_2579	= new HashTable(256);

	private HashTable	aClass377_2585	= new HashTable(256);

	Class308(Js5 class207, Js5 class207_11_) {
		try {
			aClass207_2578 = class207;
			aClass207_2577 = class207_11_;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sv.<init>(" + (class207 != null ? "{...}" : "null") + ',' + (class207_11_ != null ? "{...}" : "null") + ')');
		}
	}

	private final Class98_Sub24_Sub1 method3609(int i, int i_4_, int i_5_, int[] is) {
		try {
			int i_6_ = (0xfff8 & i_4_ << -42388412 | i_4_ >>> 885159052) ^ i_5_;
			i_6_ |= i_4_ << -1252543472;
			long l = i_6_ ^ 0x100000000L;
			Class98_Sub24_Sub1 class98_sub24_sub1 = (Class98_Sub24_Sub1) aClass377_2585.get(l, i ^ ~0x5177);
			if (class98_sub24_sub1 != null) {
				return class98_sub24_sub1;
			}
			if (is != null && (is[0] ^ 0xffffffff) >= -1) {
				return null;
			}
			Class98_Sub13 class98_sub13 = (Class98_Sub13) aClass377_2579.get(l, -1);
			if (class98_sub13 == null) {
				class98_sub13 = Class98_Sub13.method1140(aClass207_2577, i_4_, i_5_);
				if (class98_sub13 == null) {
					return null;
				}
				aClass377_2579.put(class98_sub13, l, i + -20856);
			}
			class98_sub24_sub1 = class98_sub13.method1132(is);
			if (class98_sub24_sub1 == null) {
				return null;
			}
			class98_sub13.unlink(98);
			aClass377_2585.put(class98_sub24_sub1, l, i + -20856);
			return class98_sub24_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sv.A(" + i + ',' + i_4_ + ',' + i_5_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	public final Class98_Sub24_Sub1 method3611(int i, int i_7_, int[] is) {
		try {
			if (i == (aClass207_2578.getGroupCount((byte) -11) ^ 0xffffffff)) {
				return method3612(14913, is, i_7_, 0);
			}
			if ((aClass207_2578.getFileCount(0, i_7_) ^ 0xffffffff) == -2) {
				return method3612(i + 14915, is, 0, i_7_);
			}
			throw new RuntimeException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sv.E(" + i + ',' + i_7_ + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}

	private final Class98_Sub24_Sub1 method3612(int i, int[] is, int i_8_, int i_9_) {
		try {
			int i_10_ = i_8_ ^ ((~0x5ffff000 & i_9_) << -746085692 | i_9_ >>> -2070738644);
			i_10_ |= i_9_ << 965219344;
			long l = i_10_;
			Class98_Sub24_Sub1 class98_sub24_sub1 = (Class98_Sub24_Sub1) aClass377_2585.get(l, -1);
			if (class98_sub24_sub1 != null) {
				return class98_sub24_sub1;
			}
			if (is != null && (is[0] ^ 0xffffffff) >= -1) {
				return null;
			}
			Class37 class37 = Class37.method342(aClass207_2578, i_9_, i_8_);
			if (class37 == null) {
				return null;
			}
			if (i != 14913) {
				method3609(96, 36, 96, null);
			}
			class98_sub24_sub1 = class37.method344();
			aClass377_2585.put(class98_sub24_sub1, l, -1);
			if (is != null) {
				is[0] -= class98_sub24_sub1.aByteArray5799.length;
			}
			return class98_sub24_sub1;
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sv.B(" + i + ',' + (is != null ? "{...}" : "null") + ',' + i_8_ + ',' + i_9_ + ')');
		}
	}

	public final Class98_Sub24_Sub1 method3613(int i, boolean bool, int[] is) {
		try {
			if ((aClass207_2577.getGroupCount((byte) -11) ^ 0xffffffff) == -2) {
				return method3609(20855, 0, i, is);
			}
			if ((aClass207_2577.getFileCount(0, i) ^ 0xffffffff) == -2) {
				return method3609(20855, i, 0, is);
			}
			if (bool != true) {
				return null;
			}
			throw new RuntimeException();
		} catch (RuntimeException runtimeException) {
			throw Js5Exception.createJs5Exception(runtimeException, "sv.F(" + i + ',' + bool + ',' + (is != null ? "{...}" : "null") + ')');
		}
	}
}

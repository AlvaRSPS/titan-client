/* d - Decompiled by JODE
 */ package com; /*
					*/

public interface TextureMetricsList {
	TextureMetrics getInfo(int i, int i_7_);

	int[] getPixelsArgb(int i, int i_8_, int i_9_, float f, boolean bool, int i_10_);

	float[] getPixelsFloat(byte i, boolean bool, int width, int height, float brightness, int i_6_);

	int[] getPixelsRgb(int i, byte i_1_, int i_2_, float f, boolean bool, int i_3_);

	boolean isCached(int i, int i_0_);

	int method12(boolean bool);
}


/* Class123_Sub1 - Decompiled by JODE
 */ package com; /*
					*/

import java.io.IOException;
import java.net.Socket;

public final class JavaNetworkHandler extends StreamHandler {

	public static final void method2211(byte i) {
		Class38.aClass100_357.method1690(1);
	}

	private JavaNetworkReader	inputStream;
	private JavaNetworkWriter	outputStream;

	private Socket				socket;

	public JavaNetworkHandler(Socket socket, int bufferSize) throws IOException {
		this.socket = socket;
		this.socket.setSoTimeout(30000);
		this.socket.setTcpNoDelay(true);
		inputStream = new JavaNetworkReader(this.socket.getInputStream(), bufferSize);
		outputStream = new JavaNetworkWriter(this.socket.getOutputStream(), bufferSize);
	}

	@Override
	public final void close(int i) {
		try {
			socket.close();
		} catch (IOException ioexception) {
			/* empty */
		}
		inputStream.shutdown(85);
		outputStream.shutdown((byte) -117);
	}

	@Override
	protected final void finalize() {
		close(-17);
	}

	@Override
	public final boolean isBuffered(int i, int nBytes) throws IOException {
		return inputStream.isBuffered(nBytes, (byte) -124);
	}

	@Override
	public final int read(byte[] buffer, int offset, int dummy, int length) throws IOException {
		return inputStream.read(offset, buffer, length, (byte) 59);
	}

	@Override
	public final void setDummyMode(int i) {
		inputStream.setDummyMode((byte) 126);
		outputStream.setDummyMode(true);
	}

	@Override
	public final void write(int dummy, int length, byte[] buffer, int offset) throws IOException {
		outputStream.write(offset, length, buffer, dummy ^ 0x5ef4);
	}
}

/* Peer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.peer;

abstract class Peer {
	/*synthetic*/ static Class a;

	static {
		init(a == null ? a = a("jaclib.peer.PeerReference") : a);
	}

	/*synthetic*/ static Class a(String string) {
		Class var_class;
		try {
			var_class = Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
		return var_class;
	}

	private static final native void init(Class var_class);

	protected PeerReference reference;

	protected Peer() {
		/* empty */
	}

	protected long a() {
		long l;
		try {
			l = reference.a(0);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return l;
	}

	public final boolean a(int i) {
		boolean bool;
		try {
			if (i != 25759)
				a();
			bool = reference.b(i + -25881);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
		return bool;
	}
}

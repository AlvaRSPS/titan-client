/* IUnknownReference - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.peer;

class IUnknownReference extends PeerReference {
	IUnknownReference(IUnknown iunknown, jaa var_jaa) {
		super(iunknown, var_jaa);
	}

	@Override
	protected final native long releasePeer(long l);
}

/* NativeHeap - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.memory.heap;

public final class NativeHeap {
	private int		a;
	private boolean	b;
	long			peer;

	public NativeHeap(int i) {
		try {
			a = i;
			allocateHeap(a);
			b = true;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	final synchronized boolean a() {
		try {
			return b;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final NativeHeapBuffer allocate(int i, boolean bool) {
		try {
			if (!b)
				throw new IllegalStateException();
			return new NativeHeapBuffer(this, allocateBuffer(i, bool), i);
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	final synchronized native int allocateBuffer(int i, boolean bool);

	private final native void allocateHeap(int i);

	public final synchronized void deallocate() {
		try {
			if (b)
				deallocateHeap();
			b = false;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	final synchronized native void deallocateBuffer(int i);

	private final native void deallocateHeap();

	@Override
	protected final synchronized void finalize() throws Throwable {
		try {
			super.finalize();
			deallocate();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	final synchronized native void get(int i, byte[] is, int i_0_, int i_1_, int i_2_);

	final synchronized native long getBufferAddress(int i);

	final synchronized native void put(int i, byte[] is, int i_3_, int i_4_, int i_5_);
}

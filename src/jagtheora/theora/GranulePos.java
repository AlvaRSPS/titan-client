/* GranulePos - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.theora;

import jagtheora.misc.SimplePeer;

public class GranulePos extends SimplePeer {
	static {
		init();
	}

	private static final native void init();

	public long position;

	@Override
	protected final native void clear();
}

/* TheoraComment - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.theora;

import jagtheora.misc.SimplePeer;

public class TheoraComment extends SimplePeer {
	public TheoraComment() {
		try {
			init();
			if (b())
				throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	protected final native void clear();

	private final native void init();
}

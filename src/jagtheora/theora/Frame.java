/* Frame - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.theora;

import jagtheora.misc.SimplePeer;

public class Frame extends SimplePeer {
	static {
		init();
	}

	private static final native void init();

	public int[]	pixels;

	public int		a;

	public int		b;

	public Frame(int i, int i_0_) {
		try {
			b = i_0_;
			a = i;
			pixels = new int[a * b];
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	protected final native void clear();
}

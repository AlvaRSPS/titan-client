/* OggSyncState - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.ogg;

import jagtheora.misc.SimplePeer;

public class OggSyncState extends SimplePeer {
	public OggSyncState() {
		try {
			init();
			if (b())
				throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	protected final native void clear();

	private final native void init();

	public final native int pageOut(OggPage oggpage);

	public final native long pageSeek(OggPage oggpage);

	public final native boolean reset();

	public final native boolean write(byte[] is, int i);
}

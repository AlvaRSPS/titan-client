/* OggStreamState - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.ogg;

import jagtheora.misc.SimplePeer;

public class OggStreamState extends SimplePeer {
	public OggStreamState(int i) {
		try {
			if (!init(i))
				throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	protected final native void clear();

	private final native boolean init(int i);

	public final native boolean isEOS();

	public final native int packetOut();

	public final native int packetOut(OggPacket oggpacket);

	public final native int packetPeek();

	public final native int packetPeek(OggPacket oggpacket);

	public final native boolean pageIn(OggPage oggpage);

	public final native boolean reset();

	public final native boolean resetSerialNo(int i);
}

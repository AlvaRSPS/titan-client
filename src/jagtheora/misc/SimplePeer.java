/* SimplePeer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.misc;

public abstract class SimplePeer {
	static {
		init();
	}

	private static final native void init();

	private long peer;

	public final void a() {
		do {
			try {
				if (b())
					break;
				clear();
			} catch (RuntimeException runtimeException) {
				throw runtimeException;
			}
			break;
		} while (false);
	}

	public final boolean b() {
		try {
			if ((peer ^ 0xffffffffffffffffL) != -1L)
				return false;
			return true;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	protected abstract void clear();

	@Override
	protected final void finalize() throws Throwable {
		try {
			if (!b())
				a();
			super.finalize();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	private final void setPeer(long l) {
		try {
			peer = l;
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}
}

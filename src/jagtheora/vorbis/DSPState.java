/* DSPState - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.vorbis;

import jagtheora.misc.SimplePeer;

public class DSPState extends SimplePeer {
	public DSPState(VorbisInfo vorbisinfo) {
		try {
			init(vorbisinfo);
			if (b())
				throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	public final native void blockIn(VorbisBlock vorbisblock);

	@Override
	protected final native void clear();

	public final native double granuleTime();

	private final native void init(VorbisInfo vorbisinfo);

	public final native float[][] pcmOut(int i);

	public final native void read(int i);
}

/* VorbisInfo - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.vorbis;

import jagtheora.misc.SimplePeer;
import jagtheora.ogg.OggPacket;

public class VorbisInfo extends SimplePeer {
	static {
		initFields();
	}

	private static final native void initFields();

	public int	channels;

	public int	rate;

	public VorbisInfo() {
		try {
			init();
			if (b())
				throw new IllegalStateException();
		} catch (RuntimeException runtimeException) {
			throw runtimeException;
		}
	}

	@Override
	protected final native void clear();

	public final native int headerIn(VorbisComment vorbiscomment, OggPacket oggpacket);

	private final native void init();
}
